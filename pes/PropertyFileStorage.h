/**
 *******************************************************************************
 * 
 * @file    PropertyFileStorage.h
 * @date    03-10-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For handling storage of a property file, especially on disc/in memory,
 *    and related actions such as look-up.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef PROPERTYFILESTORAGE_H_INCLUDED
#define PROPERTYFILESTORAGE_H_INCLUDED

#include <string>
#include <map>
#include <tuple>
#include <unordered_map>
#include <utility>

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"

/**
 * @brief
 *    Handles storage of _property files_, especially whether they are on disc
 *    or in memory, and how to perform actions such as look-up accordingly.
 **/
class PropertyFileStorage
{
   public:
      /*************************************************************************
       * TYPES, ETC.
       ************************************************************************/
      //! Calc. codes are strings like `*#1_-21/256#2_5/64*`.
      using calc_code_t = std::string;

      //! Property values are regular floating point numbers.
      using value_t = Nb;

      //! Calc. nums. are integers.
      using calc_num_t = In;

      //! For combining all property information associated with a calc. code.
      using property_t = std::tuple<value_t, calc_num_t>;

      //! Tuple access using midas::util::ToUType. Order must match property_t.
      enum class property_fields {value, calc_num};

      //! Internal in-memory storage type, preferably with _fast_ search properties.
      using container_t = std::unordered_map<calc_code_t, property_t>;


      /*************************************************************************
       * CONSTRUCTORS, ETC.
       ************************************************************************/
      //@{
      //! Deleted since we don't want more than one object per file.
      PropertyFileStorage(const PropertyFileStorage&) = delete;
      PropertyFileStorage& operator=(const PropertyFileStorage&) = delete;
      //@}

      //@{
      //! Default moves are okay.
      PropertyFileStorage(PropertyFileStorage&&) = default;
      //@}

      //! Destructor takes care of writing properties stored in memory, if any.
      ~PropertyFileStorage();

      //! Constructor from file name.
      PropertyFileStorage(const std::string& arFileName);

      /*************************************************************************
       * PUBLIC FUNCTIONS
       ************************************************************************/
      //! String describing how file is currently internally stored.
      std::string StorageMode() const;

      //! Reads file and stores it in memory.
      void ReadIntoMemory();

      //! Clears file contents from memory.
      void ClearFromMemory();

      //! Append a new calc. code and corresponding property.
      bool Append(calc_code_t, property_t);

      //! Returns property contents corresponding to argument calc. code.
      std::pair<property_t, bool> Search(const calc_code_t&) const;

   private:
      /*************************************************************************
       * PRIVATE MEMBERS, ETC.
       ************************************************************************/
      //! Scoped `enum` for internally keeping track of file storage.
      enum class Storage: int
         {  on_disc = 0
         ,  in_memory
         };
      //! Map from `enum` to string for returning sensible information.
      const std::map<Storage, std::string> mStorageEnumToString =
         {  {Storage::on_disc, "on disc"}
         ,  {Storage::in_memory, "in memory"}
         };

      //! The file name with which the object is associated.
      std::string mFileName;

      //! The container used when storing _in memory_.
      container_t mContainer;

      //! Appended properties, if any; preserves the order in which they were appended.
      std::vector<container_t::value_type> mToBeAppendedToFile;

      //@{
      //! Format settings used for appending new property lines.
      std::ios::fmtflags mAppendFlags = std::ios::scientific | std::ios::uppercase;
      Uin mAppendPrecision = I_22;
      //@

      //! `enum` specifying how the file is currently stored.
      Storage mStorage;

      /*************************************************************************
       * PRIVATE FUNCTIONS
       ************************************************************************/
      //@{
      //! Validates i/o fstream; MIDASERROR if invalid.
      void ValidateIFStream(const std::ifstream& arFS, const std::string& arFileName) const;
      void ValidateOFStream(const std::ofstream& arFS, const std::string& arFileName) const;
      //@}

      //! Converts Storage `enum` to string.
      std::string StorageMode(Storage) const;

      //! Search for calc. code on disc.
      std::pair<property_t, bool> SearchOnDisc(const calc_code_t&) const;

      //! Search for calc. code in memory.
      std::pair<property_t, bool> SearchInMemory(const calc_code_t&) const;

      //! Directly appends property line to file.
      bool AppendOnDisc(container_t::value_type&&) const;

      //! Inserts property to container in memory and remembers to write it to file later.
      bool AppendInMemory(container_t::value_type&&);

      //! Handles the actual writing to disc.
      bool AppendToFile(std::vector<container_t::value_type>&&) const;

      //! Whether any properties in memory need be written to file.
      bool HasPropertiesToAppend() const;
};

/*******************************************************************************
 * NON-MEMBER FUNCTIONS
 ******************************************************************************/
//! Searches for calc. code and directly extracts results into argument references.
bool ExtractPropertyLine
   (  const PropertyFileStorage& arPfs
   ,  const PropertyFileStorage::calc_code_t& arCalcCode
   ,  PropertyFileStorage::value_t& arVal
   ,  PropertyFileStorage::calc_num_t& arCalcNum
   );
//! Same as ExtractPropertyLine, but calc. num. is discarded.
bool ExtractPropertyLine
   (  const PropertyFileStorage& arPfs
   ,  const PropertyFileStorage::calc_code_t& arCalcCode
   ,  PropertyFileStorage::value_t& arVal
   );

//! Wraps arguments into calc.code/property pair and appends to object.
bool AppendPropertyLine
   (  PropertyFileStorage& arPfs
   ,  PropertyFileStorage::calc_code_t aCalcCode
   ,  PropertyFileStorage::value_t aVal
   ,  PropertyFileStorage::calc_num_t aCalcNum
   );



#endif/*PROPERTYFILESTORAGE_H_INCLUDED*/
