#include "pes/PesUtility.h"

#include <fstream>

#include "inc_gen/Const.h"
#include "util/Error.h"

namespace midas
{
namespace pes
{


/**
 * Find the number of a given property in a target  file 
 *
 * Search in "prop_files.mpesinfo" for arLabel with arIstate. Returns -1 if not found.
 * Default: The last occurance of the match is returned, The entire file is searched through , Iopt = 0.
 * Iopt != 0, the search is stoped after the first match
 *
 * @param aPesInfo
 * @param arLabel
 * @param arTarget
 * @param arIstate
 * @param Iopt
 *
 * @return          Return property number.
 * */
In FindPropertyNumberInFile
   (  const PesInfo& aPesInfo
   ,  const std::string& arLabel
   ,  const std::string& arTarget
   ,  In arIstate
   ,  In Iopt
   ) 
{
   // Try to open file with list of calculated properties.
   std::ifstream Calculated_Properties(arTarget, std::ios_base::in);
   if (!Calculated_Properties)
   {
      MIDASERROR(" Error when trying to open file: " + arTarget);
   }
     
   // Loop through the PropertyInfo file in order to determine the property number of the property we are currently investigating
   In property_file_number = -I_1;
   std::string ss;
   while (std::getline(Calculated_Properties, ss))
   {
      // Check for comment line
      std::size_t found_comment = ss.find("#");
      if (found_comment != std::string::npos)
      {
         continue;
      }
      
      In electronic_state;
      std::string file_name;
      std::string property_label;
      
      std::istringstream line_s(ss);
      line_s >> electronic_state >> file_name >> property_label;
      
      if (  (electronic_state == arIstate) 
         && (property_label == arLabel) 
         )
      {
         auto file_number = file_name.erase(0, 8);
         file_number.erase(file_number.find(".mop"));
         property_file_number = midas::util::FromString<In>(file_number);
         
         // Found the property we were looking for, no need to carry on
         break;
      }
   }
   Calculated_Properties.close();
   return property_file_number;
}

/**
 * Find the number of a given property in "prop_files.mpesinfo"
 * 
 * Search in "prop_files.mpesinfo" for arLabel with arIstate. Returns -1 if not found.
 * Default: The last occurance of the match is returned, The entire file is searched through , Iopt = 0.
 * Iopt != 0, the search is stoped after the first match
 *
 * @param aPesInfo
 * @param arLabel
 * @param arIstate
 * @param Iopt
 *
 * @return          Return property number.
 **/
In FindPropertyNumberInFile
   (  const PesInfo& aPesInfo
   ,  const std::string& arLabel
   ,  In arIstate
   ,  In Iopt
   )
{
   std::string Namelabel = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_files.mpesinfo";
   return FindPropertyNumberInFile(aPesInfo, arLabel, Namelabel, arIstate, Iopt);
}

/**
* Dump freqs on file
* */
void DumpFreqsOnFile
   (  std::vector< std::pair<In,Nb> >& arNumberVector
   ,  const std::string& arName
   )
{
   std::ofstream file(arName.c_str(),std::ios_base::out);
   file.setf(std::ios::scientific);
   file.setf(std::ios::uppercase);
   file.precision(I_22);
   for( auto i = arNumberVector.begin(); i!=arNumberVector.end(); i++) 
   {
      file << (*i).first << "    " << (*i).second << std::endl;
   }
   file.close();
}

/**
 * Read frequencies from file harmonic_frequencies.mpesinfo
**/
void GetFreqsOnFile
   (  std::vector< std::pair<In,Nb> >& arNumberVector
   ,  const std::string& arName
   )
{
   std::ifstream file(arName.c_str(), std::ios_base::out);
   if (!file)
   {
      MIDASERROR(" Error when trying to open file: " + arName);
   }
   file.setf(std::ios::scientific);
   file.setf(std::ios::uppercase);
   file.precision(I_22);

   std::string ss;
   while(getline(file,ss))
   {
      In Mode;
      Nb Freq;
      std::istringstream line_s(ss);
      line_s >> Mode >> Freq;
      arNumberVector[Mode-1] = std::make_pair(Mode-1,Freq);
   }
   file.close();
}

} /* namespace pes */
} /* namespace midas */
