/**
************************************************************************
* 
* @file                GridFileHandlerMethods.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for handling common algorithm for Grid/Adga Bar files
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef GRIDFILEHANDLERMETHODS_H
#define GRIDFILEHANDLERMETHODS_H

// std headers
#include<vector>
#include<map>
#include<set>
#include<string>
#include<fstream>

// midas headers
#include "pes/BaseBarFileHandler.h"
#include "pes/PesInfo.h"
#include "inc_gen/TypeDefs.h"
#include "util/FileHandler.h"
#include "mmv/MidasVector.h"
#include "pes/Derivatives.h"

// using declarations
using std::vector;
using std::map;
using std::set;
using std::string;
using std::ofstream;

// forward declarations
class Derivatives;

/**
 *
 **/
class GridFileHandlerMethods 
   : public BaseBarFileHandler
{
   private:
      FileHandles::FileHandler mFile;
      ofstream mPlotfile;
      ofstream mPlotfilebar;
      bool mOnlyPotPlot;
   
   protected:
      bool mLocalPlot;
      
      GridFileHandlerMethods(PesInfo& arPesInfo) 
         : BaseBarFileHandler(arPesInfo)
         , mOnlyPotPlot(true) 
         , mLocalPlot(false)
      {
      }
      
      void OpenFiles() 
      {
         mFile.OpenFiles(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_", mPesInfo.GetMultiLevelInfo().GetNoOfProps(), ".mbar");
      }

      bool AvailableDer(const Derivatives& arDerivatives, In aProp)
      {
         string name_pr=mPesInfo.GetMultiLevelInfo().GetPropertyName(aProp);
         return arDerivatives.AvailableDer(name_pr);
      }
      void FinishPlot() {mPlotfile.close(); mPlotfilebar.close();}
      void GenerateLinearCombinationMap(vector<In>&, map<InVector, Nb>&);
      void SetupPlot(const vector<In>&);
      
      void WriteDataToFiles
         (  const std::vector<Nb>&
         ,  const std::vector<MidasVector>&
         ,  const MidasVector&
         ,  const std::vector<In>&
         ,  const Derivatives&
         ,  Nb
         );
      
      //!
      void SetupFilesForPlot(const vector<In>&);

   public:
      
      //!
      virtual ~GridFileHandlerMethods() {}

      //!
      void Extrapolation(const Derivatives&, const CalculationList&, const std::vector<std::string>& arPropWithDerInfo);
};
#endif //GRIDFILEHANDLERMETHODS_H
