/**
************************************************************************
*
* @file                CalculationList.h
*
* Created:             01-06-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) M. Sparta (msparta@chem.au.dk)
*                      Ian Heide Godtliebsen (ian@chem.au.dk) (almost complete rewrite)
*
* Short Description:   stores informations about the List of calculations that have to be 
*                      be computed
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef CALCULATION_LIST_H
#define CALCULATION_LIST_H

// std headers
#include <vector>
#include <string>
#include <map>
#include <mutex>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/BiMap.h"
#include "input/Input.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "pes/CalcCode.h"
#include "pes/GeneralProp.h"
#include "pes/DisplacementGenerator.h"


// forward declarations
class PesInfo;
class CalcCode;

namespace midas
{
namespace pes
{
class PesSinglePoint;
class GeneralProp;
} /* namespace pes */
} /* namespace midas */

// using declarations
using namespace midas;
using namespace midas::pes;

/**
 * Declarations:
 **/
class CalculationList
{
   private:
      //! Forward declaration
      struct CalculationProperty;

      //! Helper class to handle calculation symmetry
      class CalculationSymmetryRelation
      {
         private:
            //! The parent property
            const CalculationProperty* mParent = nullptr;
            //! The rotation
            MidasMatrix mRotMat;
            //! The nuclei relation
            std::vector<In> mRelation;

         public:
            //! Constructructor
            CalculationSymmetryRelation() 
               : mRotMat(I_3, C_0) 
            { 
               mRotMat[0][0] = C_1;
               mRotMat[1][1] = C_1;
               mRotMat[2][2] = C_1;
            }

            //! Define symmetry relation
            void DefineRelation(const CalculationProperty* aParent, const MidasMatrix& aRotMat, const std::vector<In>& aRelation)
            {
               mParent = aParent;
               mRotMat = aRotMat;
               mRelation = aRelation;
            }

            //! Apply the symmetry relation
            GeneralProp ApplyRelation() const
            {
               GeneralProp prop = mParent->mProperties;
               prop.Rotate(mRotMat, mRelation);
               return prop;
            }
            
            //! Does the point have a symmetry relation to a previoulsy calculated point?
            bool IsSymmetryRelated() const
            {
               return bool(mParent);
            }
      };

      //! Helper class that combines a calculation code with calculated properties for storing in a map/table
      struct CalculationProperty
      {
         //! Calculation code.
         CalcCode mCalcCode;
         //! Calculation number.
         In mCalculationNumber;
         //! Properties.
         GeneralProp mProperties;
         //! Symmetry relation of properties.
         CalculationSymmetryRelation mSymmetry;
         
         //! Constructor
         CalculationProperty
            (  const CalcCode& aCalcCode
            ,  In aCalculationNumber
            ,  const PropertyInfo& aPropInfo
            )
            :  mCalcCode(aCalcCode)
            ,  mCalculationNumber(aCalculationNumber)
            ,  mProperties(aPropInfo)
         {
         }
      };
      
      //! Helper class to store CalculationProperties with easy look-up both from CalcCode and calculation number.
      class CalculationMap
         : private util::BiMap<CalculationProperty, CalcCode, In>
      {
         private:
            using calccode_map_type = util::BiMap<CalculationProperty, CalcCode, In>;
            std::unique_ptr<std::mutex> mMutex = std::unique_ptr<std::mutex>(new std::mutex);
            
         public:
            CalculationMap() = default;
            CalculationMap(CalculationMap&&) = default;

            void Insert(const std::shared_ptr<CalculationProperty>& aEntry)
            {
               std::lock_guard<std::mutex> mutex_guard(*mMutex);
               calccode_map_type::insert
                  (  aEntry
                  ,  aEntry->mCalcCode
                  ,  aEntry->mCalculationNumber
                  );
            }

            const CalculationProperty& GetCalculationProperty(const CalcCode& aCalcCode) const
            {
               auto iter = this->find1(aCalcCode);
               if(iter == this->map1().end())
               {
                  MIDASERROR("Didn't find properties for calccode.");
               }
               return *iter->second;
            }
            
            CalculationProperty& GetCalculationProperty(const CalcCode& aCalcCode) 
            {
               auto iter = this->find1(aCalcCode);
               if(iter == this->map1().end())
               {
                  MIDASERROR("Didn't find properties for calccode.");
               }
               return *iter->second;
            }
            
            const CalculationProperty& GetCalculationProperty(const In& aCalcNumber) const
            {
               auto iter = this->find2(aCalcNumber);
               if(iter == this->map2().end())
               {
                  MIDASERROR("Didn't find properties for calc number: " + std::to_string(aCalcNumber));
               }
               return *iter->second;
            }
            
            CalculationProperty& GetCalculationProperty(const In& aCalcNumber) 
            {
               auto iter = this->find2(aCalcNumber);
               if(iter == this->map2().end())
               {
                  MIDASERROR("Didn't find properties for calc number: " + std::to_string(aCalcNumber));
               }
               return *iter->second;
            }

            void Clear()
            {
               calccode_map_type::clear();
            }

            const auto& CalcNumberMap() const
            {
               return calccode_map_type::map2();
            }
            
            auto find(const CalcCode& aCalcCode) const
            {
               return calccode_map_type::map1().find(aCalcCode);
            }

            auto begin() const
            {
               return calccode_map_type::map1().begin();
            }
            
            auto end() const
            {
               return calccode_map_type::map1().end();
            }

            auto size() const
            {
               return calccode_map_type::size();
            }
      };
      
      //! Reference to Molecule.
      const molecule::MoleculeInfo& mMolecule;
      //! information on current 'pes' surface
      const PesInfo& mPesInfo; 
      //!
      const DisplacementGenerator mDisplacementGenerator;
      //! A map of all CalcCodes with available properties.
      CalculationMap mCalculated;
      //! Temporary list of newly added points for current batch.
      CalculationMap mNewCodes;
      //!
      In mCalculationNumber = 1;
     
      //!
      std::map<In,In>         mDerivOffsets; ///< ??
      
      //! WHAT IS THIS USED FOR? IT SHOULD GO AWAY! AND IT WILL
      std::vector<std::string> mPropertyNames;

      //! Handle symmetry related points.
      void ApplySymmetryRelations();
      
      //! Dump the prop_no_x.mpoints.
      void DumpPropFiles(bool = true);
      
      // 
      CalculationMap IdentifyUniqueCalculations();

      //!
      static void RunSinglePoints
         (  const std::string& aSinglePointName
         ,  const PesInfo& aPesInfo
         ,  const DisplacementGenerator& aDisplacementGenerator
         ,  CalculationMap& aNewCalculationMap
         );

#ifdef VAR_MPI
   private:
      //!
      static void RunSinglePointsMaster
         (  const mpi::Communicator& aCommunicator
         ,  const std::string& aSinglePointName
         ,  const PesInfo& aPesInfo
         ,  const DisplacementGenerator& aDisplacementGenerator
         ,  CalculationMap& aNewCalculationMap
         );
      
   public:
      //!
      static void RunSinglePointsSlave
         (  const mpi::Communicator& aCommunicator
         ,  const molecule::MoleculeInfo& aMolecule
         ,  const PesInfo& aPesInfo
         );
#endif /* VAR_MPI */


   public:

      //! Delete default ctor
      CalculationList() = delete;
      
      //! Enable move constructor 
      CalculationList(CalculationList&&) = default;
      
      //! Constructor
      CalculationList(const molecule::MoleculeInfo& aMolecule, const PesInfo& arPesInfo);
      
      //! Destructor
      ~CalculationList();
      
      //! Used in extrapolation code, which is a mess :C (DONT CALL IT FROM ANYWHERE ELSE, YOU WILL REGRET IT) 
      std::string GetCalcCodeFromCalcNumber(In aCalcNumber) const;
      //! Used in extrapolation code, which is a mess :C (DONT CALL IT FROM ANYWHERE ELSE, YOU WILL REGRET IT) 
      In GetCalcNrFromCode(const std::string&) const;
      //! Used in extrapolation code, don't use anywhere else....
      In GetNrOfNewCalcs() const { return mNewCodes.size(); }

      //! NEW CODES ARE CLEANED BECAUSE OF EXTRAPCODE....
      void Clean() { mCalculated.Clear(); mPropertyNames.clear(); mCalculationNumber = 1; mNewCodes.Clear(); }
      
      //! Get the total number of calculations on the list. 
      In GetNrOfCalcs() const { return mCalculated.size(); }
      
      //! Restart-ability
      void CheckForCalcsDone();
      
      //! Add new entry to be evaluated.
      bool AddEntry(const CalcCode&);
      
      //! Evaluate the list.
      void EvaluateList(bool = true);
      
      //! GetReference structure calculation properties, for setting the ReferenceValue keyword in the .mop file
      std::vector<Nb> GetReferenceProperties() const {return mCalculated.GetCalculationProperty(CalcCode("*#0*")).mProperties.GetValues();}
      
      //! Overload for output operator
      friend ostream& operator<<(ostream&,const CalculationList&);
};

#endif /* CALCULATION_LIST_H */
