/**
* **********************************************************************
* 
* @file                PolyFit.cc
* 
* Created:             20-10-2006
* 
* Author:              Daniele Toffoli & Ove Christiansen
* 
* Short Description:   Define class PolyFit for linear least squares fitting
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
* 
* **********************************************************************
* */

#include "pes/fitting/PolyFit.h"

// std headers
#include <vector>
#include <cmath>
#include <numeric>
#include <string>
#include <iostream>
#include <utility>
#include <algorithm>
#include <fstream>
#include <cctype>
#include <map>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "mmv/Diag.h"

#include "lapack_interface/GELSS.h"

/**
 * Local Declarations
**/
typedef string::size_type Sst;

/**
 * Constructor
**/
PolyFit::PolyFit
   (  const In& arNumOfFitFuncs
   ) 
   :  mLeastSqrParms(arNumOfFitFuncs)
   ,  mCovarMatrx
         (  arNumOfFitFuncs
         ,  arNumOfFitFuncs
         )
   ,  mChiSq(C_0)
{
}

/**
 * Perform the actual fitting by appropriate calls to nr routinees
**/ 
void PolyFit::MakeFit
   (  const std::string& aFitMethod
   ,  const MidasVector& aValue
   ,  const std::vector<MidasVector>& aCoords
   ,  const MidasVector& aVariance
   ,  const std::vector<InVector>& aBasisFuncOrders
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   ,  bool aCutSingular
   ,  const Nb aSvdCutThr
   ,  bool aTikhonov
   ,  const Nb aTikhonovParam
   )
{
   // Perform the actual fitting by passing needed arrays to nr routines
   In n_dim = aCoords.size();
   In grid_points = I_1;
   for (In i_dim = I_0; i_dim < n_dim; i_dim++)
   {
      grid_points *= aCoords[i_dim].Size();
   }
   In num_of_fit_funcs = aBasisFuncOrders.size();
   In num_of_fit_parms = num_of_fit_funcs;
   if (grid_points != aValue.Size())
   {
      Mout << " Mismatch in the number of grid points and functional values  " << std::endl;
      MIDASERROR(" Stop in PolyFit::MakeFit due to mismatch in aCoords's and aValue shapes");
   }
   
   MidasMatrix u(grid_points, num_of_fit_funcs);
   MidasMatrix v(num_of_fit_funcs, num_of_fit_funcs);
   MidasVector w(num_of_fit_funcs);
   std::vector<bool> ia(num_of_fit_parms);
   for (In i = I_0; i < num_of_fit_parms; i++)
   {
      ia[i] = true;
   }
   
   //********************************************************************************************
   // Do fitting
   //********************************************************************************************
   Timer time_all;
   // Do a SVD calling SvdFit routine 
   if (aFitMethod == "SVD")
   {
      SvdFit(arFuncMat, aBasisFuncOrders, aCoords, aValue, aVariance, mLeastSqrParms, u, v, w, mChiSq,
             aCutSingular, aSvdCutThr, aTikhonov, aTikhonovParam); 
      if (gTime)
      {
         time_all.CpuOut(Mout, "\n CPU time used in MakeFit::SvdFit: ");
         time_all.WallOut(Mout, " Wall time used in MakeFit::SvdFit: ");
      }
      
      SvdVar(v, w, mCovarMatrx, aTikhonov, aTikhonovParam);
      if (gTime)
      {
         time_all.CpuOut(Mout, "\n CPU time used in MakeFit::SvdVar: ");
         time_all.WallOut(Mout, " Wall time used in MakeFit::SvdVar: ");
      }
   }
   // Solve the normal equations (in this preliminary version set every element of ia to true)
   else if (aFitMethod == "NORMALEQ")
   {
      lfit(arFuncMat, aBasisFuncOrders, aCoords, aValue, aVariance, mLeastSqrParms, ia, mCovarMatrx, mChiSq);
      if (gTime)
      {
         time_all.CpuOut(Mout, "\n CPU time used in MakeFit::lfit: ");
         time_all.WallOut(Mout, " Wall time used in MakeFit::lfit: ");
      }
   }
   // Fit using lapack gelss routine (fit through svd)
   else if (aFitMethod == "SVDGELSS")
   {
      if (aTikhonov)
      {
         MIDASERROR("SVDGELSS fitting routine cannot be called with a Tikhonov parameter");
      }
      GELSSfit(arFuncMat, aBasisFuncOrders, aCoords, aValue, aVariance, mLeastSqrParms, mCovarMatrx, mChiSq, aSvdCutThr);
      if (gTime)
      {
         time_all.CpuOut(Mout,"\n CPU  time used in MakeFit::GELSSfit: ");
         time_all.WallOut(Mout," Wall time used in MakeFit::GELSSfit: ");
      }
   }
   else
   {
      MIDASERROR("Unknown fit method passed to MakeFit!");
   }
   
   //********************************************************************************************
   // determines Rms (root-mean-square) deviation with input functional values
   //********************************************************************************************
   MidasVector fitted_value(grid_points);
   PlotFit(fitted_value, aCoords, aBasisFuncOrders, arFuncMat);
   RmsDevPerElement(mRms, aValue, fitted_value, grid_points);
}

/**
 * Plot the result of fitting ?!?
**/
void PolyFit::PlotFit
   (  MidasVector& aValue
   ,  const vector<MidasVector>& aCoords
   ,  const vector<InVector>& arNvecVec
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   )
{
   // *** Plot the result of the fitting on a given output grid
   In n_dim=aCoords.size();
   In n_data=I_1;
   for (In i_dim=I_0; i_dim<n_dim; i_dim++)
   {
      n_data *=aCoords[i_dim].Size();
   }
   In num_of_fit_funcs = arNvecVec.size();
   In num_of_fit_parms = num_of_fit_funcs;
   if (n_data != aValue.Size())
   {
      Mout << " Mismatch in the number of grid points and functional values  " << std::endl;
      MIDASERROR(" Stop in PolyFit::PlotFit due to mismatch in aCoords's and aValue shapes");
   }
   
   //
   MidasVector vec_tmp(n_dim);
   MidasVector afunc(num_of_fit_funcs);
   for(In i=I_0; i< n_data; i++)
   {
      In curr_dim = I_1;
      In curr_index;
      for(In i_dim=n_dim-I_1; i_dim>=I_0; i_dim--)
      {
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim]=aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size();
      }
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);
      aValue[i]=C_0;
      for(In j=I_0; j<num_of_fit_parms; j++) 
      {
         aValue[i] += mLeastSqrParms[j]*afunc[j];
      }
   }
}

/**
 * overload the operator << for printing out PolyFit objects
**/
ostream& operator<<
   (  ostream& arOut
   ,  const PolyFit& arFit
   )
{ 
   if (gDebug)
   {
      arOut << " Set of fitting parameters                              " << std::endl;
      for (In i = I_0; i < arFit.mLeastSqrParms.Size(); i++)
      {
         arOut << " a[" << i << "] = " << "  " << arFit.mLeastSqrParms[i] << std::endl; 
      }
      arOut << " Covariance matrix:                                     " << std::endl;
      arOut << arFit.mCovarMatrx;
   }
   arOut << "  Minimized chi-squared:                                   " << arFit.mChiSq << std::endl;
   arOut << "  Rms deviation with respect to input function:            " << arFit.mRms << std::endl;
   return arOut;
}

/**
 * Evaluates the set of functions on a given point
**/
void PolyFit::EvalFitFunc
   (  const std::vector<InVector>& arNvecVec 
   ,  const MidasVector& aCoords
   ,  MidasVector& arFuncVals
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   )
{
   In n_dim = arNvecVec.size();

   // evaluate the basis functions on the given aCoords point
   MidasVector vec_tmp(aCoords.Size());
   for (In i = I_0; i < n_dim; ++i)
   {
      vec_tmp = aCoords;
      if (vec_tmp.Size() != arNvecVec[i].size())
      {
         MIDASERROR("Mismatch between dimensions used in functions and the dimension of the mode-coupling in polyfit");
      }
      for (In j = I_0; j < vec_tmp.Size(); ++j)
      {
         // If additional fit-function parameters beside the coordinate Q exists, then we need to add these to the list of variables before we can begin to evaluate the fit functions and obtain the potential coefficients
         auto variables = arFuncMat[j][arNvecVec[i][j]]->GetmVariables();
         variables[I_0] = vec_tmp[j];

         vec_tmp[j] = arFuncMat[j][arNvecVec[i][j]]->EvaluateFuncForVarVec(variables);
      }
      arFuncVals[i] = vec_tmp.ProductEle();
   }
}

/**
* svdfit routine, a kind of Numerical Recipes in c++, pag. 683: 
* Taken a generic functional values (aValue) evaluated on a grid aCoords (unitary standard 
* deviation is assumed) use chi^2 minimization to determine the coefficients a of the fitting functions
* (assumed simple direct product polynomials) using the SVD of the design matrix A (= u * w * vT) 
* aCutSingular: Close to zero w(i) elements are put to zero corresponding to neglect/general inverse
**/
void PolyFit::SvdFit
   (  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   ,  const std::vector<InVector>& arNvecVec
   ,  const std::vector<MidasVector>& aCoords
   ,  const MidasVector& aValue
   ,  const MidasVector& aVariance
   ,  MidasVector& a
   ,  MidasMatrix& u
   ,  MidasMatrix& v
   ,  MidasVector& w
   ,  Nb& chisq
   ,  bool aCutSingular
   ,  const Nb aSvdCutThr
   ,  bool aTikhonov
   ,  const Nb aTikhonovParam
   )
{
   In i, j;
   Nb wmax, wmin, tmp, thresh, sum;

   In n_dim = aCoords.size();
   In ndata = aValue.Size();
   In ma = a.Size();
   MidasVector afunc(ma); 
   MidasVector b(ndata);
   MidasVector vec_tmp(n_dim);

   // *** now run over the grid points
   for (i = I_0; i < ndata; i++)
   {
      // *** extract the actual point
      In curr_dim = I_1;
      In curr_index;
      for (In i_dim = n_dim - I_1; i_dim >= I_0; i_dim--)
      {
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim] = aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size(); 
      } 
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);
      tmp = C_1/aVariance[i];
      for (j = I_0; j < ma; j++)
      {
         u[i][j] = afunc[j]*tmp;
      }
      b[i] = aValue[i]*tmp;
   }
   svdcmp(u, w, v);
   
   wmax = C_0; 
   wmin = C_NB_MAX; 
   for (j = I_0; j < ma; j++)
   {
      if (w[j] > wmax)
      {
         wmax = w[j];
      }
      if (w[j] < wmin)
      {
         wmin = w[j];
      }
   }
   thresh = aSvdCutThr*wmax;
   In no_of_del_val = I_0;
   // Here cut zero elements, meaning close to zero elements are put to zero exactly!
   // SVBKSB will later discard actual zeros, so this leads to generalized inverse 
   if (aCutSingular)
   {
      for (j = I_0; j < ma; j++)
      {
         if (w[j] < thresh) 
         {
            w[j] = C_0;
            no_of_del_val += I_1;
         }
      }
   }

   if (gPesIoLevel > I_11)
   {
      Mout << "\n PolyFit::SvdFit - Original maximum singular value: " << wmax << std::endl; 
      Mout << " PolyFit::SvdFit - Original minimum singular value: " << wmin << std::endl; 
      Mout << " PolyFit::SvdFit - Condition number original        ";
      if (wmin > C_0) Mout << wmax/wmin; 
      else Mout << " INFINITE! ";
      Mout << std::endl;  
      if (aCutSingular) 
      {
         Nb wmincut = C_NB_MAX; 
         Nb wmincut2 = C_NB_MAX; 
         for (j = I_0; j < ma; j++)
         {
            if (w[j] < wmincut) wmincut = w[j];
            if ((w[j] != 0) && (w[j] < wmincut2)) wmincut2 = w[j];
         }
         Mout << " PolyFit::SvdFit - Condition number - after cut - with zeros: "; 
         if (wmincut > C_0) Mout << wmax/wmincut;
         else Mout << " INFINITE! "; 
         Mout << " non-zero part: "; 
         if (wmincut2 > C_0) Mout << wmax/wmincut2; 
         else Mout << " INFINITE! "; // This should not happen if t-regul > 0. 
         Mout << std::endl; 
         Mout << " PolyFit::SvdFit - Gen Least Squares fitting: SVD cut abs. cut " << thresh << " rel cut " << aSvdCutThr << std::endl; 
         Mout << " PolyFit::SvdFit - Number of deleted diagonal elements w[i] after SVD in svdfit: " << no_of_del_val << std::endl;
      }
      if (aTikhonov)
      {
         if (aCutSingular) Mout << " PolyFit::SvdFit - Gen Least Squares fitting: Do Tikhonov type of regularization after SVD cut with " << aTikhonovParam << std::endl; 
         else              Mout << " PolyFit::SvdFit - Gen Least Squares fitting: Do Tikhonov type of regularization with " << aTikhonovParam << std::endl; 
         Mout << " PolyFit::SvdFit - Condition number of S^2, [for A=USV^T,(A^TA) = V(S^2)V^T] "; 
         if ((wmin*wmin) > C_0) Mout << (wmax*wmax)/(wmin*wmin);
         else Mout << " INFINITE! "; 
         Mout << std::endl; 
         Mout << " PolyFit::SvdFit - Condition number of S^2 + t, [for A=USV^T,(A^TA+t) = V(S^2+t)V^T] "; 
         if ((wmin*wmin+aTikhonovParam) > C_0) Mout << (wmax*wmax+aTikhonovParam)/(wmin*wmin+aTikhonovParam);
         else Mout << " INFINITE! "; 
         Mout << std::endl; 
      }
   }

   SvBksb(u, w, v, b, a, aTikhonov, aTikhonovParam);
   chisq = C_0;
   for (i = I_0; i < ndata; i++) 
   {
      In curr_dim = I_1;
      In curr_index;
      for(In i_dim = n_dim - I_1; i_dim >= I_0; i_dim--)
      {
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim] = aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size();  
      }
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);
      sum = C_0;
      for (j = I_0; j < ma; j++)
      {
         sum += a[j]*afunc[j];
      }
      chisq += (tmp = (aValue[i] - sum)/aVariance[i], tmp*tmp);
   }
}

/**
* subroutine SvdVar: Spirit from numerical recipes in cpp pag. 684.
* - evaluate the covariance matrix of the fit for the ma parameters
*   obtained by SvdFit
* Standard  case: V^t(w^-2) T
* aTikhonov case: V^t(w^2+tI_n)^-1 V 
**/
void PolyFit::SvdVar
   ( MidasMatrix& v
   , MidasVector& w
   , MidasMatrix& cvm
   , bool aTikhonov 
   , const Nb aTikhonovParam
   )
{
   In i,j,k;
   Nb sum;

   In ma=w.Size();
   MidasVector  wti(ma);
   for (i=I_0; i<ma; i++) 
   {
      wti[i]=C_0;
      if (aTikhonov)        wti[i] = C_1/(w[i]*w[i]+aTikhonovParam);
      else if (w[i] != C_0) wti[i] = C_1/(w[i]*w[i]);
   }
   for (i=I_0; i<ma; i++) 
   {
      for (j=I_0; j<i+I_1; j++) 
      {
         sum=C_0;
         for (k=I_0; k<ma; k++)
           sum += v[i][k]*v[j][k]*wti[k];
         cvm[j][i]=cvm[i][j]=sum;
      }
   }
}

/**
* routine lfit, taken from Numerical Recipes in c++, pag. 679.
* Perform linear least squares fit by solving the normal equations.
**/
void PolyFit::lfit
   ( vector<vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   , const vector<InVector>& arNvecVec
   , const vector<MidasVector>& aCoords
   , const MidasVector& aValue
   , const MidasVector& aVariance
   , MidasVector& a
   , vector<bool>& ia
   , MidasMatrix& covar
   , Nb& chisq
   )
{
   In i,j,k,l,m,mfit=I_0;
   Nb ym,wt,sum,variance2i;

   In n_dim=aCoords.size();
   In ndat=aValue.Size();
   In ma = a.Size();
   MidasVector afunc(ma);
   MidasMatrix beta(ma,I_1);

   for (j=I_0; j<ma; j++)
   {
      if (ia[j]) mfit++;
   }
   if (mfit == I_0) 
   {
      MIDASERROR("lfit: no parameters to be fitted");
   }

   for (j=I_0;j<mfit;j++) 
   {
      for (k=I_0; k<mfit; k++) covar[j][k]=C_0;
      beta[j][I_0]=C_0;
   }
   MidasVector vec_tmp(n_dim);
   for (i=I_0; i<ndat; i++) 
   {
      In curr_dim = I_1;
      In curr_index;
      for (In i_dim=n_dim-I_1; i_dim>=I_0; i_dim--)
      {    
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim]=aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size();
      } 
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);
      ym=aValue[i];
      if (mfit < ma) 
      {
         for (j=I_0; j<ma; j++)
         if (!ia[j]) ym -= a[j]*afunc[j];
      }
      variance2i=C_1/(aVariance[i])*(aVariance[i]);
      for (j=I_0,l=I_0;l<ma;l++) 
      {
         if (ia[l]) 
         {
            wt=afunc[l]*variance2i;
            for (k=I_0,m=I_0; m<=l; m++)
            if (ia[m]) covar[j][k++] += wt*afunc[m];
            beta[j++][I_0] += ym*wt;
         }
      }
   }
   for (j=I_1;j<mfit;j++)
      for (k=I_0;k<j;k++)
         covar[k][j]=covar[j][k];
   MidasMatrix mat_temp(mfit,mfit);
   for (j=I_0; j<mfit; j++)
      for (k=I_0; k<mfit; k++)
         mat_temp[j][k]=covar[j][k];
   gaussj(mat_temp,beta);
   for (j=I_0; j<mfit; j++)
      for (k=I_0; k<mfit; k++)
         covar[j][k]=mat_temp[j][k];
   for (j=I_0,l=I_0;l<ma;l++)
      if (ia[l]) a[l]=beta[j++][I_0];
   chisq = C_0;
   for (i = I_0; i < ndat; i++) 
   {
      In curr_dim = I_1;
      In curr_index;
      for (In i_dim = n_dim - I_1; i_dim >= I_0; i_dim--)
      {
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim] = aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size();
      }
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);

      sum = C_0;
      for (j = I_0; j < ma; j++)
      {
         sum += a[j]*afunc[j];
      }
      chisq += ((aValue[i] - sum)/aVariance[i]) * ((aValue[i] - sum)/aVariance[i]);
   }
   covsrt(covar,ia,mfit);
}

/**
* subroutine gaussj, taken from numerical recipes in c++, pag. xxx
* perform a gauss-jordan elimination for solving the linear system A x = b
**/
void PolyFit::gaussj
   ( MidasMatrix& a
   , MidasMatrix& b
   )
{
   In i=0, icol=0, irow=0, j=0, k=0, l=0, ll=0;
   Nb big=C_0, dum=C_0, pivinv=C_0;

   In n=a.Nrows();
   In m=b.Ncols();
   vector<In> indxc(n),indxr(n),ipiv(n);
   for (j=I_0; j<n; ++j) 
   {
      ipiv[j]=I_0;
   }
   for (i=I_0; i<n; i++) 
   {
      big=C_0;
      for (j=I_0; j<n; j++)
      {
         if (ipiv[j] != I_1)
         {
            for (k=I_0; k<n; k++) 
            {
               if (ipiv[k] == I_0) 
               {
                  if (fabs(a[j][k]) >= big) 
                  {
                     big=fabs(a[j][k]);
                     irow=j;
                     icol=k;
                  }
               }
            }
         }
      }
      ++(ipiv[icol]);
      if (irow != icol) 
      {
         for (l=I_0;l<n;l++) 
         {
            std::swap(a[irow][l],a[icol][l]);
         }
         for (l=I_0;l<m;l++) 
         {
            std::swap(b[irow][l],b[icol][l]);
         }
      }
      indxr[i]=irow;
      indxc[i]=icol;
      if (a[icol][icol] == C_0) 
      {
         MIDASERROR("gaussj: Singular Matrix");
      }
      pivinv=C_1/a[icol][icol];
      a[icol][icol]=C_1;
      for (l=I_0; l<n; l++) 
      {
         a[icol][l] *= pivinv;
      }
      for (l=I_0; l<m; l++) 
      {
         b[icol][l] *= pivinv;
      }
      for (ll=I_0;ll<n;ll++)
      {
         if (ll != icol) 
         {
            dum=a[ll][icol];
            a[ll][icol]=C_0;
            for (l=I_0; l<n; l++) 
            {
               a[ll][l] -= a[icol][l]*dum;
            }
            for (l=I_0; l<m; l++) 
            {
               b[ll][l] -= b[icol][l]*dum;
            }
         }
      }
   }
   for (l=n-I_1;l>=I_0;l--) 
   {
      if (indxr[l] != indxc[l])
      {
         for (k=I_0; k<n; k++)
         {
            std::swap(a[k][indxr[l]],a[k][indxc[l]]);
         }
      }
   }
}

/**
* routine covsrt: taken from numerical recipes in c++, pag. 680.
* expand in storage the covariance matrix covar so as to take into 
* account parameters that are being fixed. (for which it returns zero covariances)
**/
void PolyFit::covsrt
   ( MidasMatrix& covar
   , vector<bool>& ia
   , const In mfit
   )
{
   In i,j,k;

   In ma=ia.size();
   for (i=mfit;i<ma;i++)
      for (j=I_0;j<i+I_1;j++) covar[i][j]=covar[j][i]=C_0;
   k=mfit-I_1;
   for (j=ma-I_1;j>=I_0;j--) 
   {
      if (ia[j]) 
      {
         for (i=I_0;i<ma;i++) std::swap(covar[i][k],covar[i][j]);
         for (i=I_0;i<ma;i++) std::swap(covar[k][i],covar[j][i]);
         k--;
      }
   }
}

/**
 *
 **/
void PolyFit::GELSSfit
   ( vector<vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   , const vector<InVector>& arNvecVec
   , const vector<MidasVector>& aCoords
   , const MidasVector& aValue
   , const MidasVector& aVariance
   , MidasVector& aLeastSqrParms
   , MidasMatrix& aCovarMatrx
   , Nb& aChiSq
   , const Nb aSvdCutThr
   )
{
   // setup needed vectors and matrices
   MidasMatrix u(aValue.Size(), In(arNvecVec.size()));
   MidasVector b(aValue.Size());
   
   MidasVector afunc(aLeastSqrParms.Size()); 
   MidasVector vec_tmp(aCoords.size());

   for (int i=I_0; i<aValue.Size(); i++)
   {
      // *** extract the actual point
      In curr_dim = I_1;
      In curr_index;
      for(In i_dim=aCoords.size()-I_1; i_dim>=I_0; i_dim--)
      {
         curr_index = (i%(curr_dim*aCoords[i_dim].Size()))/curr_dim;
         vec_tmp[i_dim]=aCoords[i_dim][curr_index];
         curr_dim = curr_dim*aCoords[i_dim].Size();
      }
      EvalFitFunc(arNvecVec, vec_tmp, afunc, arFuncMat);
      Nb tmp=C_1/aVariance[i];
      for (In j=I_0; j<afunc.Size(); j++) u[i][j]=afunc[j]*tmp;
      b[i]=aValue[i]*tmp;
   }
   
   // do fitting
   auto fit = GELSS(u, b, aSvdCutThr);
   
   // load fitting parameters
   aLeastSqrParms.SetNewSize(fit.n_);
   for(In i = 0; i < fit.n_; ++i) 
      aLeastSqrParms.at(i) = fit.B_[i];
   
   // calculate covariance matrix
   MidasMatrix v(static_cast<In>(fit.rank_), static_cast<In>(fit.n_));
   In counter = 0;
   for(In i = 0; i< fit.rank_; ++i)
      for(In j = 0; j < fit.n_; ++j)
      {
         v[i][j] = fit.A_[counter];
         ++counter;
      }
   MidasVector s(static_cast<In>(fit.rank_)); 
   for(In i = 0; i < fit.rank_; ++i) s[i] = fit.S_[i];
   // The aTikhonov argument is set to false as the GELSS fit is not implemented with a Tikhonov parameter
   SvdVar(v, s, aCovarMatrx, false);

   // calculate chi^2
   aChiSq = 0.0; // init to zero
   for(In i = 0; i < u.Nrows(); ++i)
   {
      Nb sum = 0.0;
      for(In j = 0; j < u.Ncols(); ++j)
      { 
         sum += u[i][j]*aLeastSqrParms[j];
      }
      aChiSq += (b[i] - sum)*(b[i] - sum);
   }
   
   // do some output
   if (gPesIoLevel > I_8)
   {
      Mout << " ======================= GELSS =========================" << std::endl;
      Mout << " LeastSqrParms: " << aLeastSqrParms << std::endl;
      Mout << " aChiSqr: " << aChiSq << std::endl;
      Mout << " ======================= GELSS END ======================" << std::endl;
   }
}
