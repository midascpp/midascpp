/**
************************************************************************
* 
* @file                PolyFitBasisSet.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef POLYFITBASISSET_H_INCLUDED
#define POLYFITBASISSET_H_INCLUDED

// std headers
#include <vector>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/fitting/BaseFitBasisSet.h"

/**
 *
 **/
class PolyFitBasisSet 
   : public BaseFitBasisSet
{
   public:
      //! ctor
      PolyFitBasisSet(In aDegree);
};

#endif /* POLYFITBASISSET_H_INCLUDED */
