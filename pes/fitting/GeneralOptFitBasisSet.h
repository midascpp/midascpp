/**
************************************************************************
* 
* @file                GeneralOptFitBasisSet.h
*
* Created:             03-11-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting 
*                      with general functions in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERALOPTFITBASISSET_H_INCLUDED
#define GENERALOPTFITBASISSET_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/fitting/BaseFitBasisSet.h"
#include "libmda/functions.h"

/**
 *
 **/
class GeneralOptFitBasisSet 
   : public BaseFitBasisSet
{
   private:

      FunctionContainer<taylor<Nb, 1, 1> >         mFunctionCont;
      std::vector<Nb>                              mVarVals;
      const std::vector<std::string>               mVariables;
      const In                                     mModeNr;
      const std::string                            mOptFunc;
      const Nb                                     mOptThr;

   public:

      //! Constructor
      GeneralOptFitBasisSet
         (  const std::vector<std::string>&
         ,  const std::vector<std::string>&
         ,  const std::vector<Nb>&
         ,  const std::string&
         ,  const FunctionContainer<Nb>&
         ,  const FunctionContainer<taylor<Nb, 1, 1> >&
         ,  const In&
         ,  const Nb&
         ); 

      //! Optimize parameters
      void OptimizeParam(const MidasVector&, const MidasVector&);
      
      //! 
      std::string GiveConst() const;
      
      //!
      void GiveConst(MidasOperatorWriter&) const;
};

Nb norm(std::vector<Nb>);

#endif /* GENERALOPTFITBASISSET_H_INCLUDED */
