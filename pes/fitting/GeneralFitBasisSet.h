/**
************************************************************************
* 
* @file                GeneralFitBasisSet.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting 
*                      with general functions in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERALFITBASISSET_H_INCLUDED
#define GENERALFITBASISSET_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/fitting/BaseFitBasisSet.h"

/**
 *
 **/
class GeneralFitBasisSet 
   : public BaseFitBasisSet
{
   public:
      GeneralFitBasisSet(const std::vector<std::string>&, const FunctionContainer<Nb>&);
};

#endif /* GENERALFITBASISSET_H_INCLUDED */
