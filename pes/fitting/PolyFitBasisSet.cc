/**
************************************************************************
* 
* @file                PolyFitBasisSet.cc
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/generalfunctions/ShuntingYard.h"
#include "pes/fitting/PolyFitBasisSet.h"
#include "pes/fitting/BaseFitBasisSet.h"

/**
 * Contructor for polynomial fit basis set.
 *
 * @param aDegree   Give degree of polynomial.
 **/
PolyFitBasisSet::PolyFitBasisSet
   ( In aDegree
   )
{
   for (In i = I_1; i <= aDegree; ++i)
   {
      mFunctionStrings.emplace_back("Q^" + std::to_string(i));
      mFunctions.emplace_back(mFunctionStrings.back(), FunctionContainer<Nb>(), false);
   }
}
