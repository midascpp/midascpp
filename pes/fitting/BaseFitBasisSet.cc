/**
************************************************************************
* 
* @file                FitFunctions.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>

// midas headers
#include "pes/fitting/BaseFitBasisSet.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "input/MidasOperatorWriter.h"

/**
 *
 **/
In BaseFitBasisSet::GetNrFunctions
   (
   ) const 
{
   return mFunctions.size();
}

/**
 *
 **/
void BaseFitBasisSet::GetFunctions
   (  const In& aSetNr
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& aFunctionCont
   ,  const In& aNrFunc
   )
{
   for (In i = I_0; i < aNrFunc; ++i)
   {
      aFunctionCont[aSetNr].emplace_back(&mFunctions[i]);
   }
}

/**
 *
 **/
std::string BaseFitBasisSet::GetFunctionString
   (  In aNr
   )  const
{
   return mFunctionStrings[aNr];
}

/**
 *
 **/
BaseFitBasisSet::~BaseFitBasisSet
   (
   )
{
}

/**
 *
 **/
std::string BaseFitBasisSet::GiveConst
   (
   )  const
{
   return std::string();
}

/**
 *
 **/
void BaseFitBasisSet::GiveConst
   (  MidasOperatorWriter& 
   )  const
{
}

/**
 *
 **/
void BaseFitBasisSet::OptimizeParam
   (  const MidasVector& arXvals
   ,  const MidasVector& arYvals
   )
{
}
