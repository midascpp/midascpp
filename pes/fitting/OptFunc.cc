/**
************************************************************************
*
* @file                OptFunc.cc
*
* Created:             03-11-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Interface class for non linear fitting
*
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include <string>
#include <vector>

#include "pes/fitting/OptFunc.h"
#include "mmv/MidasVector.h"
#include "mathlib/Taylor/taylor.hpp"

namespace midas
{
namespace fitting
{

/**
 * Constructor
 *
 * @param aFuncStr
 * @param aFuncCont
 * @param aVarNames
 * @param arXvals
 * @param arYvals
 **/
OptFunc::OptFunc
   (  const std::string& aFuncStr
   ,  FunctionContainer<taylor<Nb,1,1> >& aFuncCont
   ,  const std::vector<std::string>& aVarNames
   ,  const MidasVector& arXvals
   ,  const MidasVector& arYvals
   )
   :  mFunction
      (  aFuncStr
      ,  aFuncCont
      ,  false
      ,  aVarNames
      )
   ,  mXvals(arXvals)
   ,  mYvals(arYvals)
{
}

/**
 * Note that res corresponds to ||F(x) - g(x)||^2 for this and the following function.
 *
 * @param aVars   The point in which to evaluate the function.
 **/
Nb OptFunc::operator()
   (  const std::vector<Nb>& aVars
   )  const
{
   taylor<Nb, 1, 1> res(C_0);
   for (Uin i = I_0; i < aVars.size(); ++i)
   {
      mFunction.SetVariableI(taylor<Nb, 1, 1>(aVars[i]), i + I_1);
   }
   mFunction.ReZero();

   for (In i = I_0; i < mXvals.Size(); ++i)
   {
      if (std::isinf(mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[i]))[I_0]) || std::isnan(mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[i]))[I_1]) || std::isinf(res[I_0]))
      {
         Mout  << "WARNING: The OptFunc operator is for Xval = " << mXvals[i]
               << " calculating Yval = " << mFunction.EvaluateFuncForVar(taylor<Nb, I_1, I_1>(mXvals[i]))[I_0]
               << std::endl
               << "This does ultimately lead to result = " << res[I_0] << ", which is not too fortunate!"
               << std::endl;
      }
      res += (mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[i])) - mYvals[i]) * (mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[i])) - mYvals[i]);
   }
   return res[I_0];
}

/**
 * Note that res[I_0], can be outputted from solver/ncg.h as m_f, while res[I_1] can be found as m_df_new.
 *
 * @param arVars   The non-linear optimized fit function parameters
 * @param arDers   (OUTPUT) On output, the first order derivatives at point arVars.
 **/
void OptFunc::first_deriv
   (  const std::vector<Nb>& arVars
   ,  std::vector<Nb>& arDers
   )  const
{
   taylor<Nb, 1, 1> res;

   for (Uin i = 0; i < arVars.size(); ++i)
   {
      mFunction.SetVariableI(taylor<Nb, 1, 1>(arVars[i]), i + I_1);
   }

   for (Uin i = 0; i < arVars.size(); ++i)
   {
      res = C_0;
      mFunction.SetVariableI(arVars[i] + taylor<Nb,1,1>(0,0), i + I_1);
      mFunction.ReZero();
      for (In j = I_0; j < mXvals.Size(); ++j)
      {
         res += (mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[j])) - mYvals[j])*(mFunction.EvaluateFuncForVar(taylor<Nb, 1, 1>(mXvals[j])) - mYvals[j]);
      }
      arDers[i] = res[I_1];
      mFunction.SetVariableI(taylor<Nb, 1, 1>(arVars[i]), i + I_1);
   }
}

} /* namespace fitting */
} /* namespace midas */
