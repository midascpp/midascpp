/**
************************************************************************
* 
* @file                OptFunc.h
*
* Created:             03-11-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Interface class for non linear fitting
*
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef OPTFUNC_H_INCLUDED
#define OPTFUNC_H_INCLUDED

#include <string>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "mathlib/Taylor/taylor.hpp"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "mmv/MidasVector.h"

namespace midas
{
namespace fitting
{

class OptFunc
{
   private:
      MidasFunctionWrapper<taylor<Nb,1,1> >           mFunction;
      const MidasVector&                              mXvals;
      const MidasVector&                              mYvals;
   public:
      OptFunc(const std::string&, FunctionContainer<taylor<Nb,1,1> >&, const std::vector<std::string>&, const MidasVector&, const MidasVector&);
      Nb operator()(const std::vector<Nb>&) const;
      void first_deriv(const std::vector<Nb>&, std::vector<Nb>&) const;
};

} /* namespace fitting */
} /* namespace midas */

#endif /* OPTFUNC_H_INCLUDED */
