/**
************************************************************************
* 
* @file                FitFunctions.cc
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <algorithm>
#include <memory>

// midas headers
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "input/FindKeyword.h"
#include "input/MidasOperatorWriter.h"
#include "pes/PesInfo.h"
#include "pes/fitting/FitFunctions.h"
#include "pes/fitting/BaseFitBasisSet.h"
#include "pes/fitting/PolyFitBasisSet.h"

// using declarations
using std::string;
using std::map;
using std::vector;
using std::ifstream;
using std::make_pair;

/**
 * Default constructor for fit-basis functions. Will initialize the container for the complete fit-basis for all modes
**/
FitFunctions::FitFunctions
   (  const FitBasisCalcDef& aFitBasisCalcDef
   ) 
   :  mFitBasisMap()
   ,  mFitBasisSets()
{
   mFitBasisSets.emplace_back(new PolyFitBasisSet(aFitBasisCalcDef.GetmFitFuncsMaxOrder()[I_0]));
   mFitBasisMap.insert(std::make_pair(std::make_pair(-I_1, "ALL"), mFitBasisSets[I_0]));
}

/**
 * Function for checking that the modes chosen under the keyword "#4 Modes" does exist in the molecule
**/
bool FitFunctions::CheckModeNumbers
   ( const In aMaxModeNr
   ) const 
{
   return std::all_of(mFitBasisMap.cbegin(), mFitBasisMap.cend(), [aMaxModeNr](std::pair<std::pair<In, std::string>, std::shared_ptr<BaseFitBasisSet> > i){return i.first.first < aMaxModeNr && i.first.first > -I_2;});
}

/**
 *
**/
void FitFunctions::OptimizeFunctionsForMode
   (  const In& aModeNr
   ,  const std::string& aPropType
   ,  const MidasVector& arXvals
   ,  const MidasVector& arYvals 
   )  const
{
   auto ptr = FindBasis(aModeNr, aPropType);
   auto& mutex_basis_set = *(mMutexBasisSets[aModeNr]);

   // Optimize non-linear fit-function parameters but make sure that not more than one thread can do this at a time
   std::lock_guard<std::mutex> lock(mutex_basis_set);
   ptr->OptimizeParam(arXvals, arYvals);
}

/**
 *
**/
In FitFunctions::GetNrOfBasFuncsInMode
   (  const In aModeNr
   ,  const std::string& aPropType
   )  const 
{
   return FindBasis(aModeNr, aPropType)->GetNrFunctions();
}      

/**
 * Insert the fit-basis function into the fit-basis, (FitFunctions holds everything related to fit-basis functions but the individual fit-basis functions are set to be of a specific fit-basis type)
**/
void FitFunctions::InsertBasis
   (  const std::vector<string>& arPropNames
   ,  const std::vector<In>& arModes
   ,  std::shared_ptr<BaseFitBasisSet> aPtr
   )
{
   // Create a mutex for each new basis set
   mMutexBasisSets.emplace_back(new std::mutex{});

   //
   mFitBasisSets.emplace_back(aPtr);
   for (auto it_1 = arPropNames.begin(); it_1 != arPropNames.end(); ++it_1)
   {
      for (auto it_2 = arModes.begin(); it_2 != arModes.end(); ++it_2)
      {
         auto it_ins = mFitBasisMap.insert(make_pair(make_pair(*it_2, *it_1), mFitBasisSets.back()));
         if (!it_ins.second)
         {
            it_ins.first->second = mFitBasisSets.back();
         }
      }
   }
}

/**
 *
**/
std::shared_ptr<BaseFitBasisSet> FitFunctions::FindBasis
   (  const In aModeNr
   ,  const std::string& aPropType
   )  const
{
   auto it = mFitBasisMap.find(make_pair(aModeNr, aPropType));
   if (mFitBasisMap.end() == it)
   {
      it = mFitBasisMap.find(make_pair(aModeNr, "ALL"));
      if (mFitBasisMap.end() == it)
      {
         it = mFitBasisMap.find(make_pair(-I_1, aPropType));
         if (mFitBasisMap.end() == it)
         {
            it = mFitBasisMap.find(make_pair(-I_1, "ALL"));
            if (mFitBasisMap.end() == it)
            {
               MIDASERROR("Something is wrong, the requested basis set for mode " + std::to_string(aModeNr) + " and prop " + aPropType + " does not exist and neither does the generic counterparts");
            }
         }
      }
   }
   return it->second;
}

/**
 *
**/
void FitFunctions::AddToConstants
   (  const std::map<std::string, Nb>& aConstantMap 
   )
{
   for (auto it = aConstantMap.begin(); it != aConstantMap.end(); ++it)
   {
      auto it_ins = mConstants.insert(std::make_pair(it->first, it->second));
      if (!it_ins.second)
      {
         MIDASERROR("Constant \"" + it->first + "\" does already exist, check your input");
      }
   }
}

/**
 *
**/
void FitFunctions::AddToFunctions
   (  const std::map<std::string, std::string>& aFuncMap
   )
{
   for (auto it = aFuncMap.begin(); it != aFuncMap.end(); ++it)
   {
      auto it_ins = mFunctions.insert(std::make_pair(it->first, it->second));
      
      if (!it_ins.second)
      {
         MIDASERROR("Functions \"" + it->first + "\" does already exist, check your input");
      }
   }
}

/**
 *
**/
void FitFunctions::GetFunctions
   (  const In& aSetNr
   ,  const In& aModeNr
   ,  const In& aNrOfFunc
   ,  const std::string& aPropType
   ,  std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
   )  const
{
   auto ptr = FindBasis(aModeNr, aPropType);
   ptr->GetFunctions(aSetNr, arFuncMat, std::min(aNrOfFunc, ptr->GetNrFunctions()));
}

/**
 *
**/
std::string FitFunctions::GetFunctionName
   (  const In aFuncNr
   ,  const std::string& aPropType
   ,  const In aModeNr
   )  const
{
   std::shared_ptr<BaseFitBasisSet> ptr = FindBasis(aModeNr, aPropType);
   return ptr->GetFunctionString(aFuncNr);
}

/**
 *
**/
void FitFunctions::SetConstAndFunct
   (  std::vector<MidasOperatorWriter>& aVecOfOperFiles
   )  const
{
   for (auto it = aVecOfOperFiles.begin(); it != aVecOfOperFiles.end(); ++it)
   {
      it->AddFunctions(mFunctions);
      it->AddConstants(mConstants);
      for (auto it_bas = mFitBasisSets.begin(); it_bas != mFitBasisSets.end(); ++it_bas)
      {
         (*it_bas)->GiveConst(*it);
      }
   }
}

/**
 *
**/
std::string FitFunctions::GetFuncPart() const
{
   std::stringstream ret;

   if (!mConstants.empty())
   {
      ret << " Fit-basis function parameters, (input) " << std::endl;
      for (auto it = mConstants.begin(); it != mConstants.end(); ++it)
      {
         ret << (*it) << std::endl;
      }
   }

   if (!mFunctions.empty())
   {
      ret << " Fit-basis functions, (input) " << std::endl;
      for (auto it = mFunctions.begin(); it != mFunctions.end(); ++it)
      {
         ret << *it << std::endl;
      }
   }

   ret << " Fit-basis function parameters " << std::endl;
   for (auto it = mFitBasisSets.begin(); it != mFitBasisSets.end(); ++it)
   {
      ret << " " << (*it)->GiveConst() << std::endl;
   }

   ret << " Fit-basis functions " << std::endl; 
   for (auto it = mFitBasisSets.begin(); it != mFitBasisSets.end(); ++it)
   {
      if (it == mFitBasisSets.begin())
      {
         continue;
      }

      for (In i = I_0; i < (*it)->GetNrFunctions(); ++i)
      {
         ret << " " << (*it)->GetFunctionString(i);
      }
      ret << std::endl;
   }

   return ret.str();
}

/**
 *
**/
std::ostream& operator<<
   (  std::ostream& aStream
   ,  const FitFunctions& aFitFunc
   )
{
   aStream << std::endl << "  Fit-basis overview  " << std::endl;
   aStream << aFitFunc.GetFuncPart() << std::endl;

   if (gDebug)
   {
      aStream << "Mode Prop Ptr" << std::endl;
      for (auto it = aFitFunc.mFitBasisMap.begin(); it != aFitFunc.mFitBasisMap.end(); ++it)
      {
         aStream << it->first.first << " " << it->first.second << " " << it->second << std::endl;
      }
      for (auto it = aFitFunc.mFitBasisSets.begin(); it != aFitFunc.mFitBasisSets.end(); ++it)
      {
         aStream << *it << std::endl;
      }
   }

   return aStream;
}

