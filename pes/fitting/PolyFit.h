/**
************************************************************************
*
* @file                PolyFit.h
*
* Created:             24-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Declares class PolyFit for linear least squares fitting 
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef POLYFIT_H
#define POLYFIT_H

// std headers
#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

/**
 * Declarations:
**/

class PolyFit
{
   private:

      //! Stores second derivatives on the input grid (ig)
      MidasVector                            mLeastSqrParms; 

      //! Stores the covariance matrix
      MidasMatrix                            mCovarMatrx;

      //! Final chi squared value
      Nb                                     mChiSq;    

      //! Rms deviation with input function
      Nb                                     mRms;     
      
      //! Evaluates the fitting functions on a given point
      void EvalFitFunc(const std::vector<InVector>&, const MidasVector&, MidasVector&, std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&); 

      //! Perform svd fitting
      void SvdFit
            ( std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&
            , const std::vector<InVector>&, const std::vector<MidasVector>&
            , const MidasVector&
            , const MidasVector&
            , MidasVector&
            , MidasMatrix&
            , MidasMatrix&
            , MidasVector&
            , Nb&
            , bool aCutSingular = true
            , const Nb aSvdCutThr = C_I_10_13
            , bool aTikhonov = false
            , const Nb aTikhonovParam = C_0
            );    

      //! Calculate variance matrix
      void SvdVar
            ( MidasMatrix&
            , MidasVector&
            , MidasMatrix&
            , bool aTikhonov = false
            , const Nb aTikhonovParam = C_0
            ); 

      //! Perform fitting via normal equations
      void lfit
            ( std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&
            , const std::vector<InVector>&
            , const std::vector<MidasVector>&
            , const MidasVector&
            , const MidasVector&
            , MidasVector&
            , std::vector<bool>&
            , MidasMatrix&
            , Nb&
            ); 

      //! Solve a linear system by Gauss-Jordan elimination
      void gaussj(MidasMatrix&, MidasMatrix&); 

      //! Sort back the variance matrix
      void covsrt(MidasMatrix& covar, vector<bool>& ia, const In mfit); 

      //!
      void GELSSfit
            ( std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& arFuncMat
            , const std::vector<InVector>& arNvecVec
            , const std::vector<MidasVector>& arXog
            , const MidasVector& arYog
            , const MidasVector& sig
            , MidasVector& aLeasSqrParms
            , MidasMatrix& aCovarMatrx
            , Nb& aChiSq
            , const Nb aSvdCutThr
            );

   public:
      
      //! Get the value of chi squared
      const Nb& GetChiSq() const                      {return mChiSq;}

      //! Get the rmsd value
      const Nb& GetRmsd() const                       {return mRms;}

      //! Return some statistics
      void GetCovarMatrx(MidasMatrix& arMtrx)         {arMtrx = mCovarMatrx;}                             
      //! Return the set of parameters
      void GetLeastSqrParms(MidasVector& arVtr)       {arVtr = mLeastSqrParms;}  

      //! Constructor
      PolyFit(const In& aNumOfFitFuncs);

      //! Perform the actual fitting
      void MakeFit
            ( const std::string&
            , const MidasVector&
            , const std::vector<MidasVector>&
            , const MidasVector&
            , const std::vector<InVector>&
            , std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&
            , bool aCutSingular = true
            , const Nb aSvdCutThr = C_I_10_12
            , bool aTikhonov = false
            , const Nb aTikhonovParam = C_0
            );    

      //! Plot the results
      void PlotFit
            ( MidasVector&
            , const std::vector<MidasVector>&
            , const std::vector<InVector>&
            , std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&
            ); 

      //! Overloadinng of ouput stream
      friend ostream& operator<<(ostream& arOut, const PolyFit& arFit);
};
#endif
