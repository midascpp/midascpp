/**
************************************************************************
* 
* @file                FitFunctions.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASEFITBASISSET_H_INCLUDED
#define BASEFITBASISSET_H_INCLUDED

// std headers
#include <vector>
#include <string>

// midas headers
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/Io.h"

class MidasOperatorWriter;

/**
 *
 **/
class BaseFitBasisSet
{
   protected:

      //!
      std::vector<MidasFunctionWrapper<Nb> > mFunctions;

      //!
      std::vector<std::string>               mFunctionStrings;
      
      //! Default constructor
      BaseFitBasisSet() = default;

   public:
      //! Get number of functions in basis-set
      In GetNrFunctions() const;

      //!
      void GetFunctions(const In& aSetNr, std::vector<std::vector<MidasFunctionWrapper<Nb>* > >& aFunctionCont, const In& aNrFunc);
      
      //! Get function string
      std::string GetFunctionString(In aNr) const;
      
      //!
      virtual ~BaseFitBasisSet();
   
      //!
      virtual std::string GiveConst() const;

      //!
      virtual void GiveConst(MidasOperatorWriter&) const;
      
      //!
      virtual void OptimizeParam(const MidasVector& arXvals, const MidasVector& arYvals);
};

#endif /* BASEFITBASISSET_H_INCLUDED */
