/**
************************************************************************
* 
* @file                GeneralOptFitBasisSet.cc
*
* Created:             03-11-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting 
*                      with general functions in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <vector>
#include <string>
#include <sstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "pes/fitting/GeneralOptFitBasisSet.h"
#include "pes/fitting/BaseFitBasisSet.h"
#include "pes/fitting/OptFunc.h"
#include "util/Io.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "solver/polak_ribiere.h"
#include "solver/fr_pr.h"
#include "input/MidasOperatorWriter.h"
#include "pes/PesInfo.h"

/**
 *
**/
GeneralOptFitBasisSet::GeneralOptFitBasisSet
   (  const std::vector<std::string>& arFunctions 
   ,  const std::vector<std::string>& arVarNames
   ,  const std::vector<Nb>& aStartGuess
   ,  const std::string& arOptFunc
   ,  const FunctionContainer<Nb>& arFuncCont 
   ,  const FunctionContainer<taylor<Nb, I_1, I_1> >& arDerFuncCont
   ,  const In& aModeNr
   ,  const Nb& aOptFuncThr
   ) 
   :  mFunctionCont(arDerFuncCont)
   ,  mVarVals
         (  aStartGuess.begin() + I_1
         ,  aStartGuess.end()
         )
   ,  mVariables(arVarNames)
   ,  mModeNr(aModeNr)
   ,  mOptFunc(arOptFunc)
   ,  mOptThr(aOptFuncThr)
{
   mFunctionStrings = arFunctions;
   for (auto it = mFunctionStrings.begin(); it != mFunctionStrings.end(); ++it)
   {
      mFunctions.emplace_back(*it, arFuncCont, false, mVariables);
      for (auto it_vars = mVariables.begin() + I_1; it_vars != mVariables.end(); ++it_vars)
      {
         size_t pos = 0;
         pos = it->find(*it_vars, pos);
         while (pos != std::string::npos)
         {
            it->insert(pos + it_vars->size(), "_" + std::to_string(aModeNr));
            pos = it->find(*it_vars, pos + I_1);
         }
      }
   }

   if (gDebug)
   {
      Mout << " mModeNr:          " << mModeNr << std::endl; 
      Mout << " mFunctionStrings: " << mFunctionStrings << std::endl;
      Mout << " mNonOptThr:       " << mOptThr << std::endl; 
   }
}

/**
 * Performs non-linear fitting of the potential parameters provided on user input. This is performed by the Fletcher-Reeves-Polak-Ribiere method
**/
void GeneralOptFitBasisSet::OptimizeParam
   (  const MidasVector& arXvals
   ,  const MidasVector& arYvals
   )
{
   if (gDebug)
   {
      Mout << " Will attempt to determine non-linear fit function parameters " << mVariables << " with optimization function " << mOptFunc << " for mode Q" << mModeNr << std::endl;
      Mout << "  Coordinate values are: " << arXvals << std::endl;
      Mout << "  Property values are:   " << arYvals << std::endl;
   }

   mVarVals = fr_pr(midas::fitting::OptFunc(mOptFunc, mFunctionCont, mVariables, arXvals, arYvals), mVarVals, mOptThr);
   
   if (gIoLevel > I_1)
   {
      Mout << std::endl << " Non-linear optimization for mode Q" << mModeNr << " resulted in " << std::endl;
      for (In i = I_0; i < mVarVals.size(); i++)
      {
         Mout << "  Fit function parameter " << mVariables[i + I_1] << ": " <<  mVarVals[i] << std::endl;
      }
   }

   for (auto it = mFunctions.begin(); it != mFunctions.end(); ++it)
   {
      for (Uin i = I_0; i < mVarVals.size(); ++i)
      {
         (*it).SetVariableI(mVarVals[i], i + I_1);
      }
      (*it).ReZero();
   }
}

/**
 *
**/
std::string GeneralOptFitBasisSet::GiveConst() const
{
   std::stringstream ret;
   auto it_1 = mVariables.begin() + I_1;
   for (auto it_2 = mVarVals.begin(); it_1 != mVariables.end(); ++it_1, ++it_2)
   {
      ret << *it_1 << "_" << mModeNr << " " << *it_2 << std::endl;
   }
   return ret.str();
}

/**
 *
**/
void GeneralOptFitBasisSet::GiveConst
   (  MidasOperatorWriter& aOper
   )  const
{
   auto it_1 = mVariables.begin() + I_1;
   for (auto it_2 = mVarVals.begin(); it_1 != mVariables.end(); ++it_1, ++it_2)
   {
      aOper.AddConstant(*it_1 + "_" + std::to_string(mModeNr), *it_2);
   }
}

/**
 *
**/
Nb norm(std::vector<Nb> vec)
{
   Nb sum = C_0;
   for(std::vector<Nb>::iterator it=vec.begin(); it < vec.end(); ++it)
   {
      sum += *(it) * *(it);
   }
   return std::sqrt(sum);
}
