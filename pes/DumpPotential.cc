/**
************************************************************************
*
* @file                DumpPotential.cc
*
* Created:             28-03-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Handler for dumping potential files 
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// midas headers
#include "pes/DumpPotential.h"

/**
 *
 * @param aPesCalcDef
 * @param aPesInfo
**/
DumpPotential::DumpPotential
   (  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   )
   :  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo) 
   ,  mOperatorTermsMutex(new std::mutex{})
{

}

/**
 *
**/
void DumpPotential::Initialize
   (  const ModeCombiOpRange& aModeCom
   ,  const std::vector<std::vector<Nb>>& aOneModeGridBounds
   ,  const In& aNmodes
   ,  const std::vector<Nb>& arReferenceProp
   )
{
   for (Uin i = 1; i <= mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i)
   {
      auto property_file = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(i) + ".mop";

      mOperFiles.emplace_back(property_file);
      // Add the reference value to the operator writer
      mOperFiles.back().AddReferenceValue(arReferenceProp[i-1]); // Minus one because these are counted from zero and not one
      mOperFiles.back().AddModeNames(mPesInfo.GetMolecule().GetModeNames());

      // If we can figure out which property is connected to which .mop file, then we can add that info directly to the .mop file from here! -> Will require some small tweaks to MidasOperatorWriter and MidasOperatorReader to take advantage of this...
      const auto info_label = mPesInfo.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(i - 1).GetInfoLine();
      if(!info_label.empty())
      {
         mOperFiles.back().AddPropertyLabel(info_label);
      }

      // Copy the .mop file and add an extension to document the progression of the operators
      if (  mPesCalcDef.GetmSaveItOperFile() 
         && mPesInfo.GetIterNo() > I_0
         )
      {
         In iter = mPesInfo.GetIterNo() - I_1;
         In mcl = mPesInfo.GetMCLevel(); 

         // Copy for later analysis
         midas::filesystem::Copy(property_file, property_file + "_" + std::to_string(mcl) + "m_it" + std::to_string(iter));
      }
   }

   // If scaling is used, then this information need to be passed to MidasOpertorWriter
   MidasVector scal_or_freq(mPesInfo.GetMolecule().GetNoOfVibs());
   // Use frequency scaling in fitting
   if (mPesCalcDef.GetmPesFreqScalCoordInFit() && !mPesCalcDef.GetmPesScalCoordInFit())
   { 
      for (In k = I_0; k < mPesInfo.GetMolecule().GetNoOfVibs(); ++k)
      {
         scal_or_freq[k] = mPesInfo.GetMolecule().GetFreqI(k)/C_AUTKAYS;
      }

      //
      for (auto it = mOperFiles.begin(); it != mOperFiles.end(); ++it)
      {
         it->AddFrequencies(scal_or_freq);
      }
   }
   // Use a scaling such that the point furthest from the reference point is scaled to one  and all other points have values in the interval [0,1]
   else if (mPesCalcDef.GetmPesScalCoordInFit())
   {
      mScalFacts.SetNewSize(mPesInfo.GetMolecule().GetNoOfVibs());
      In nr = I_0;
      for (const auto& mt: aModeCom)
      {
         // Check for the possible presence of () mode and check if MC has the correct dimension
         if (mt.Size() == I_1)
         {
            InVector curr_mode = mt.MCVec();
            if (curr_mode.size() == I_0)
            {
               continue;
            }
            if (curr_mode.size() > aNmodes)
            {
               break;
            }
            In add = mt.Address();
            const std::vector<Nb>& grid_bounds = aOneModeGridBounds[add];

            // The scaling factor is the potential boundary which is furthest from the reference point
            if (std::fabs(grid_bounds[I_0]) < std::fabs(grid_bounds[I_1]))
            {
               scal_or_freq[nr++] = (C_1/std::fabs(grid_bounds[I_1]));
               mScalFacts[nr] = std::fabs(grid_bounds[I_1])/std::sqrt(C_FAMU);
            }
            else 
            {
               scal_or_freq[nr++] = (C_1/std::fabs(grid_bounds[I_0]));
               mScalFacts[nr] = std::fabs(grid_bounds[I_0])/std::sqrt(C_FAMU);
            }
         }
      }

      //
      for (auto it = mOperFiles.begin(); it != mOperFiles.end(); ++it)
      {
         it->AddScalings(scal_or_freq);
      }
   }
   // Do not use scaling
   else
   { 
      for (In k = I_0; k < mPesInfo.GetMolecule().GetNoOfVibs(); ++k)
      {
         scal_or_freq[k] = C_1;
      }

      //
      for (auto it = mOperFiles.begin(); it != mOperFiles.end(); ++it)
      {
         it->AddFrequencies(scal_or_freq);
      }
   }
}

/**
 * Store information on potential energy or property surface terms. Note that the actual operator file will first be dumped after all fitting have been carried out
 *
 * @param aPropType           Type of fit-basis functions
 * @param aBasisFuncOrders    Total number of available fit-basis functions
 * @param aFitFunctions       The fit-basis functions themselves
 * @param aModeCombi          Mode combination          
 * @param aBar                Reference number of property file
 * @param aLeastSqrParms      Linear least-squares parameters obtained from the fitted potential
 * @param aOperatorTerms      Container for the complete operator terms
**/
void DumpPotential::SetSurfaceTerm
   (  const std::string& aPropType
   ,  const std::vector<std::vector<In> >& aBasisFuncOrders
   ,  const FitFunctions& aFitFunctions
   ,  const ModeCombi& aModeCombi
   ,  const In& aBar
   ,  const MidasVector& aLeastSqrParms
   ,  std::map<std::tuple<In, In, std::vector<In> >, std::tuple<Nb, std::vector<std::string>, std::vector<std::string> > >& aOperatorTerms
   )  const
{
   for (In i = I_0; i < aBasisFuncOrders.size(); ++i)
   {
      Nb conv_factor = C_1;
      std::string operator_label;
      std::vector<std::string> functions_vector;
      std::vector<std::string> modes_vector;
      std::vector<In> basis_func_order = aBasisFuncOrders[i];

      for (In j = I_0; j < basis_func_order.size(); ++j)
      {
         In mode_number = aModeCombi.Mode(j);

         operator_label += aFitFunctions.GetFunctionName(basis_func_order[j], aPropType, mode_number) + "(" + mPesInfo.GetMolecule().GetModeLabel(aModeCombi.Mode(j)) + ") "; 

         functions_vector.emplace_back(aFitFunctions.GetFunctionName(basis_func_order[j], aPropType, mode_number));

         modes_vector.emplace_back(mPesInfo.GetMolecule().GetModeLabel(aModeCombi.Mode(j)));
         
         // If 'historic' scaling we modify the conversion factor
         if (mPesCalcDef.HistoricAdgaScaling())
         {
            Nb inv_sqrt_cfamu = C_1/std::sqrt(C_FAMU);
            conv_factor *= std::pow(inv_sqrt_cfamu, Nb(basis_func_order[j] + I_1));
         }
      }
      
      // These are the actual values that show up in the prop_no_1.mop file
      Nb oper_coef_output = aLeastSqrParms[i]*conv_factor;

      // Create a unique key for this operator term
      auto key_for_operator_term = std::make_tuple(aBar, aModeCombi.Address(), aBasisFuncOrders[i]);
     
      // Make sure that the individual threads do not write to the aOperatorTerms object at the same time so as to create memory errors by locking it 
      auto& operator_terms_mutex = *mOperatorTermsMutex;

      // Check that the operator coefficient is non-vanishing before commiting it to the map of operator terms 
      if ( mPesCalcDef.ScreenOperCoef() 
        && mPesCalcDef.GetmPesFreqScalCoordInFit() 
        && (std::fabs(oper_coef_output) < mPesCalcDef.ScreenOperCoefThresh())
         )
      {
         oper_coef_output = C_0;

         // If the operator term already exists in the map of operators terms, then erase it
         if (aOperatorTerms.count(key_for_operator_term) == I_1)
         {
            std::lock_guard<std::mutex> lock(operator_terms_mutex);
            aOperatorTerms.erase(key_for_operator_term);
         }
      }
      else
      {
         // Set the operator term in the map of operator terms  
         std::lock_guard<std::mutex> lock(operator_terms_mutex);
         aOperatorTerms[key_for_operator_term] = std::make_tuple(oper_coef_output, functions_vector, modes_vector);
      }
   }
}

/**
 * Store a property surface term for later dump to file
 *
 * @param aBar             Reference number of property file
 * @param aPropertyTerm    A property surface term
**/
void DumpPotential::DumpSurfaceTerm
   (  const In& aBar
   ,  const std::tuple<Nb, std::vector<std::string>, std::vector<std::string> >& aPropertyTerm
   )
{
   MidasOperatorWriter& oper_file = mOperFiles[aBar];

   oper_file.AddOperatorTerm
      (  std::get<I_0>(aPropertyTerm)
      ,  std::get<I_1>(aPropertyTerm)
      ,  std::get<I_2>(aPropertyTerm)
      );
}

/**
 * Output and dump harmonic frequencies from fitted potential
 *
 * @param aHarmLeastSqrParms  Harmonic frequencies obtained from the fitted potential
**/
void DumpPotential::OadHarmonicFreqs
   (  const std::vector<Nb>& aHarmLeastSqrParms
   )  const
{
   // Collect the harmonic frequencies for all modes
   std::vector<std::pair<In, Nb>> vib_freqs; 

   for (In j = I_0; j < aHarmLeastSqrParms.size(); j++)
   {
      if (mPesCalcDef.GetmPesFreqScalCoordInFit())
      {
         Nb vib_freq = std::sqrt(std::fabs(aHarmLeastSqrParms[j] * (mPesInfo.GetMolecule().GetFreqI(j)/C_AUTKAYS)) * C_2) * C_AUTKAYS;
         vib_freqs.push_back(std::make_pair(j + I_1, vib_freq));
      }
      else
      {
         Nb vib_freq = std::sqrt(std::fabs(aHarmLeastSqrParms[j]) * C_2) * C_AUTKAYS;
         vib_freqs.push_back(std::make_pair(j + I_1, vib_freq));
      }
   }
  
   if (vib_freqs.size() != I_0 && !gPesCalcDef.GetmPscInPes())
   {
      Mout << std::endl;
      Mout << " Electronic state No. " << std::to_string(mPesCalcDef.GetmPesExcState()) << std::endl;
      Mout << " Calculated Harmonic vibrational frequencies in cm^-1: " << std::endl;
      Mout << " ***************************************************** " << std::endl;

      for (In k = I_0; k < vib_freqs.size(); k++)
      {
         Mout << left << setw(5) << vib_freqs[k].first << "     " << setw(25) << vib_freqs[k].second << std::endl;
      }

      Mout << std::endl;
      Mout << " Input Harmonic vibrational frequencies in cm^-1: " << std::endl;
      Mout << " ************************************************ " << std::endl;
      for (In k = I_0; k < mPesInfo.GetMolecule().GetNoOfVibs(); k++)
      {
         Mout << left << setw(5) << k + I_1 << "     " << setw(25) << mPesInfo.GetMolecule().GetFreqI(k) << std::endl;
      }
      Mout << std::endl;

      // Dump the harmonic frequencies to file
      std::string freq_file = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "harmonic_frequencies.mpesinfo";
      DumpFreqsOnFile(vib_freqs, freq_file);
   }
}

/**
 *
**/
void DumpPotential::PlotNonFittedPointsForMc
   (  const ModeCombi& aModeCom
   ,  const std::vector<Nb>& aOneModeGridBounds
   ,  const In& aPropNumber
   ,  const std::vector<MidasVector>& aCgCoords
   ,  const MidasVector aCgProperty
   )  const
{
   In n_modes = aModeCom.Size();
   In curr_iter = mPesInfo.GetIterNo();
   std::string esp_file_base_name = mPesInfo.GetMultiLevelInfo().AnalysisDir() + "/" + "EsPoints_Prop" + std::to_string(aPropNumber) + "_";
   for (In i_mode = I_0; i_mode < n_modes; i_mode++)
   {
      esp_file_base_name += "Q" + std::to_string(aModeCom.Mode(i_mode));
   }
   auto curr_esp_file_name = esp_file_base_name + ".mplot";
   ofstream es_points;
   es_points.open(curr_esp_file_name, ios_base::out);
   es_points.setf(ios::scientific);
   es_points.setf(ios::uppercase);
   es_points.precision(22);
   for (In k = I_0; k < aCgProperty.size(); k++)
   {
      Nb property_value = aCgProperty[k];
      
      if (  mPesCalcDef.ScreenOperCoef() 
         && (std::fabs(property_value) < mPesCalcDef.ScreenOperCoefThresh())
         )
      {
         property_value = C_0;
      }

      es_points << (aCgCoords[I_0][k]*aOneModeGridBounds[I_0])/aCgCoords[I_0][I_0] << "\t" << property_value << std::endl;
   } 
   es_points.close();
   CopyFile(curr_esp_file_name, esp_file_base_name + "_" + std::to_string(aModeCom.Size()) + "m_it" + std::to_string(curr_iter) + ".mplot");
}
