/**
************************************************************************
* 
* @file                DisplacementGenerator.cc
*
* Created:             06-06-2013
*
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: 
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/DisplacementGenerator.h"

// std headers
#include <fstream>
#include <mutex>

// midas headers
#include "util/conversions/VectorFromString.h"
#include "input/PesCalcDef.h"
#include "input/Trim.h"
#include "mmv/MidasVector.h"
#include "pes/PesInfo.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/ScalingInfo.h"
#include "pes/kinetic/Inertia.h"
#include "pes/kinetic/CoriolisCoeffs.h"

namespace midas
{
namespace pes
{

//! Define kin oper type.
using kin_oper_t = std::map<std::string, Nb>;

//! Define displacement types
enum DisplacementType : int { UNKNOWN, RECTILIN, CARTESIAN, POLYSPHERICAL };

/* ==========================================================================
 *
 * DisplacementGenerator helpers
 *
 * ========================================================================== */
namespace detail
{

/**
 * Get displacement type as defined by PesCalcDef.
 *
 * @param aPesCalcDef The pes calculation definition.
 *
 * @return Returns the displacement type.
 **/
DisplacementType GetDisplacementType
   (  const PesCalcDef& aPesCalcDef
   )
{
   DisplacementType displacement_type = DisplacementType::UNKNOWN;

   if (aPesCalcDef.GetmNormalcoordInPes())
   {
      displacement_type = DisplacementType::RECTILIN;
   }
   else if (aPesCalcDef.GetmCartesiancoordInPes() || aPesCalcDef.GetmPesDiatomic())
   {
      displacement_type = DisplacementType::CARTESIAN;
   }
   else if (aPesCalcDef.GetmFalconInPes())
   {
      displacement_type = DisplacementType::RECTILIN;
   }
   else if (aPesCalcDef.GetmPscInPes())
   {
      displacement_type = DisplacementType::POLYSPHERICAL;
   }
   else
   {
      MIDASERROR("No molecule type selected.");
   }

   return displacement_type;
}

/**
 *
 **/
MidasVector GenerateQValues
   (  const molecule::MoleculeInfo& aMolecule
   ,  const CalcCode& aCalcCode
   ,  const pes::ScalingInfo& aScalingInfo
   )
{
   auto number_of_vibrations = aMolecule.GetNoOfVibs();
   MidasVector q_values(number_of_vibrations, C_0);

   for (In i = I_0; i < aCalcCode.Size(); ++i)
   {
      In i_mode = aCalcCode.GetModeI(i);
  
      if (aCalcCode.GetDispI(i) > C_0)
      {
         q_values[i_mode] = aScalingInfo.GetRScalFact(i_mode) * aCalcCode.GetDispI(i);
      }
      else
      {
         q_values[i_mode] = aScalingInfo.GetLScalFact(i_mode) * aCalcCode.GetDispI(i);
      }
   }

   return q_values;
}

/**
 * Create displacement using normal coordinates.
 *
 * @param aPesCalcDef The pes calculation definition.
 * @param aMolecule     The molecule to displace.
 * @param aCalcCode     The calculation code defining the displacement.
 * @param aScalingInfo  Coordinate scaling information.
 *
 * @return   Returns displaced structure as std::vector of Nuclei.
 **/
std::vector<Nuclei> CreateDisplacedStructureNormCoord
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const CalcCode& aCalcCode
   ,  const pes::ScalingInfo& aScalingInfo
   )
{
   // Start from reference structure
   std::vector<Nuclei> new_struct = aMolecule.GetCoord();

   auto number_of_nuclei = aMolecule.GetNumberOfNuclei();
   const auto& normal_coord = aMolecule.NormalCoord();
   
   // Generate q-displacements
   auto q_values = GenerateQValues(aMolecule, aCalcCode, aScalingInfo);

   // Determine the new structures by displacement from the reference structure
   for (In i = I_0; i < aCalcCode.Size(); ++i)
   {
      In i_mode = aCalcCode.GetModeI(i);
    
      In k = I_0;
      for (In j = I_0; j < number_of_nuclei; ++j)
      {
         new_struct[j].SetX(new_struct[j].X() + q_values[i_mode] * normal_coord.at(i_mode, k    ));
         new_struct[j].SetY(new_struct[j].Y() + q_values[i_mode] * normal_coord.at(i_mode, k + 1));
         new_struct[j].SetZ(new_struct[j].Z() + q_values[i_mode] * normal_coord.at(i_mode, k + 2));
         k += 3;
      }
   }
   
   // Return the generated structure
   return new_struct;
}

/**
 * Create displacement using Cartesian coordinates.
 *
 * @param aPesCalcDef The pes calculation definition.
 * @param aMolecule   The molecule to displace.
 * @param aCalcCode   The calculation code defining the displacement.
 * @param aScalingInfo  Coordinate scaling information.
 *
 * @return   Returns displaced structure as std::vector of Nuclei.
 **/
std::vector<Nuclei> CreateDisplacedStructureCartesian
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const CalcCode& aCalcCode
   ,  const pes::ScalingInfo& aScalingInfo
   )
{
   // Start from reference structure
   std::vector<Nuclei> new_struct = aMolecule.GetCoord();
   
   // Generate displacement
   if (aPesCalcDef.GetmPesDiatomic())
   {
      if (aCalcCode.Size() > I_0)
      {
         new_struct[I_1].SetZ(new_struct[I_1].Z() + aCalcCode.GetDispI(I_0) * aScalingInfo.GetRScalFact(I_0));
      }
   }
   else
   {
      for (In i = I_0; i < aCalcCode.Size(); ++i)
      {
         In i_mode = aCalcCode.GetModeI(i);
         In rst = (i_mode + I_3) % I_3;
         if (rst == I_0)
         {
            new_struct[i_mode/I_3].SetX(new_struct[i_mode/I_3].X() + aCalcCode.GetDispI(i) * aScalingInfo.GetRScalFact(i_mode));
         }
         if (rst == I_1)
         {
            new_struct[i_mode/I_3].SetY(new_struct[i_mode/I_3].Y() + aCalcCode.GetDispI(i) * aScalingInfo.GetRScalFact(i_mode));
         }
         if (rst == I_2)
         {
            new_struct[i_mode/I_3].SetZ(new_struct[i_mode/I_3].Z() + aCalcCode.GetDispI(i) * aScalingInfo.GetRScalFact(i_mode));
         }
      }
   }
   
   // Return the generated structure
   return new_struct;
}

/**
 * Create displacement using Cartesian coordinates.
 *
 * @param aPesCalcDef   The pes calculation definition.
 * @param aMolecule     The molecule to displace.
 * @param aCalcNumber   The (unique) calculation number
 * @param aCalcCode     The calculation code defining the displacement.
 * @param aPesInfo      Iterative surface information.
 *
 * @return   Returns displaced structure as std::vector of Nuclei.
 **/
std::vector<Nuclei> CreateDisplacedStructurePolyspherical
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const In& aCalcNumber
   ,  const CalcCode& aCalcCode
   ,  const PesInfo& aPesInfo
   )
{
   // Get the types of modes
   auto mode_types = aMolecule.GetmTypeLabels();

   // Get the reference structure in Cartesian coordinates
   std::vector<Nuclei> ref_geom_cartesian = aMolecule.GetCoord();

   // The new geometry in Cartesian coordinates that will eventually be returned
   auto displaced_geom_cartesian = ref_geom_cartesian;
   
   // Get the reference structure in internal (curvilinear) coordinates
   MidasVector ref_geom_internal;
   ref_geom_internal.SetNewSize(aMolecule.GetNoOfVibs());
   if (aMolecule.HasInactiveModes())
   {
      ref_geom_internal = aMolecule.GetmInternalActiveCoords();
   }
   else
   {
      ref_geom_internal = aMolecule.GetmInternalCoord();
   }
  
   // The new gerometry in internal (curvilinear) coordinates
   auto displaced_geom_internal = ref_geom_internal;

   // Generate q-displacements
   auto q_values = GenerateQValues(aMolecule, aCalcCode, aPesInfo.GetScalingInfo());
  
   // For the reference geometry there is nothing to do, so we just return the structure
   if (aCalcCode.Size() == I_0 && !gPesCalcDef.GetmSavePscGeomScrDir())
   {
      return displaced_geom_cartesian;
   }

   // Determine the new structures by displacement from the reference structure
   for (In i = I_0; i < aCalcCode.Size(); ++i)
   {
      In i_mode = aCalcCode.GetModeI(i);
  
      displaced_geom_internal[i_mode] = ref_geom_internal[i_mode] + q_values[i_mode];

      if (gPesIoLevel > I_14)
      {
         Mout << " Single point " << aCalcNumber << ": Displacement along mode Q" << i_mode << " corresponding to internal coordinate type " << mode_types[i_mode] << std::endl;
      }

      // Check that the angles are not displaced so much as to transpose the molecule
      if (mode_types[i_mode].at(I_0) == 'R' && displaced_geom_internal[i_mode] < I_0)
      {
         MIDASERROR("The mode Q" + std::to_string(i_mode) + " corresponding to the internal coordinate " + mode_types[i_mode] + " has been displaced through the fusion point");
      }
      else if (mode_types[i_mode].at(I_0) == 'V' && (displaced_geom_internal[i_mode] < C_0 || displaced_geom_internal[i_mode] > C_PI))
      {
         MIDASERROR("The mode Q" + std::to_string(i_mode) + " corresponding to the internal coordinate " + mode_types[i_mode] + " has been displaced through the point of transposition");
      }
      else if (mode_types[i_mode].at(I_0) == 'D')
      {
         // Identify if the grid is symmetric around the reference structure
         if (ref_geom_internal[i_mode] == C_0)
         {
            if (displaced_geom_internal[i_mode] < C_M_1 * C_PI || displaced_geom_internal[i_mode] > C_PI)
            {
               MIDASERROR("The mode Q" + std::to_string(i_mode) + " corresponding to the internal coordinate " + mode_types[i_mode] + " has been displaced through the point of transposition, (symmetric grid)");
            }
         }
         else
         {
            if (displaced_geom_internal[i_mode] < C_0 || displaced_geom_internal[i_mode] > C_2 * C_PI)
            {
               MIDASERROR("The mode Q" + std::to_string(i_mode) + " corresponding to the internal coordinate " + mode_types[i_mode] + " has been displaced through the point of transposition, (unsymmetric grid)");

            }
         }
      }
   }
   
   // Insert the inactive/frozen internal modes with their reference value before submitting everything to the Tana program 
   if (aMolecule.HasInactiveModes())
   {
      auto inactive_mode_nrs = aMolecule.GetmInactiveModeNrs();
      auto all_internal_modes = aMolecule.GetmInternalCoord();
      MidasVector tmp_displaced_geom_internal;
      tmp_displaced_geom_internal.SetNewSize(all_internal_modes.Size());

      // Figure out which elements are "missing" and insert these at the right positions
      auto index_counter = I_0;
      for (In imode = I_0; imode < all_internal_modes.Size(); ++imode)
      {
         if (std::find(inactive_mode_nrs.begin(), inactive_mode_nrs.end(), imode) != inactive_mode_nrs.end())
         {
             tmp_displaced_geom_internal[imode] = all_internal_modes[imode];
             ++index_counter;
         }
         else
         {
            tmp_displaced_geom_internal[imode] = displaced_geom_internal[imode - index_counter];
         }
      }
      displaced_geom_internal.SetNewSize(tmp_displaced_geom_internal.Size());
      displaced_geom_internal = tmp_displaced_geom_internal;
   }

   // Setup unique scratch directory for the new Cartesian coordinates to be generated in
   std::string psc_geom_scratch = midas::os::Getcwd();
   
   // Get name of current subsystem and multilevel and create subdirectories
   const auto sub_system_name  = aPesInfo.GetmSubSystemName();
   const auto multi_level_name = aPesInfo.GetMultiLevelInfo().MultiLevelName();
   if (sub_system_name == "")
   {
      MIDASERROR("Empty subsystem name! This should not happen!");
   }
   else if (multi_level_name == "")
   {
      MIDASERROR("Empty multilevel name! This should not happen!");
   }
   else
   {
      psc_geom_scratch += "/" + sub_system_name;   // cwd/subsystem_n
      midas::filesystem::Mkdir(psc_geom_scratch);
      psc_geom_scratch += "/Multilevels";          // cwd/subsystem_n/Multilevels
      midas::filesystem::Mkdir(psc_geom_scratch);
      psc_geom_scratch += "/" + multi_level_name;  // cwd/subsystem_n/Multilevels/level_m
      midas::filesystem::Mkdir(psc_geom_scratch);
   }
   
   // Add individual geom scratch
   psc_geom_scratch += "/geomscr" + std::to_string(aCalcNumber);

   // Delete any pre-existing geometry scratchdir
   if (midas::filesystem::Exists(psc_geom_scratch))
   {
      MidasWarning("The directory " + psc_geom_scratch + " already exists. I will remove it before proceeding!");
      midas::filesystem::Remove(psc_geom_scratch);
   }

   // Construct the geometry scratchdir
   midas::filesystem::Mkdir(psc_geom_scratch);

   // All the displaced structures in terms of internal coordinates are now written to file and converted into structures in terms of Cartesian coordinates by the Tana program
   std::string disp_struct_file = psc_geom_scratch + "/" + "DisplacedStruct";
   ofstream ofs(disp_struct_file);
   ofs.setf(ios::scientific);
   ofs.precision(22);
   for (In j = I_0; j < displaced_geom_internal.size(); ++j)
   {
      ofs << displaced_geom_internal[j] << std::endl;
   }
   ofs.close();
  
   // Obtain the path the Tana runscript and check that this is present
   std::string tana_run_file_name = aPesInfo.GetMultiLevelInfo().SetupDir() + "/" + gPesCalcDef.GetmTanaRunFile(); 

   //
   if (!midas::filesystem::Exists(tana_run_file_name))
   {
      MIDASERROR("Could not find the Tana run file: " + gPesCalcDef.GetmTanaRunFile() + " remember to place it in the setup directory. Searched the path: " + tana_run_file_name);
   }

   // Run the Tana program
   system_command_t run_tana = {tana_run_file_name, psc_geom_scratch};
   
   auto run_status = run_process(run_tana, Mlog);
   if (run_status.status != 0) 
   {
      MIDASERROR("Tana computation failed for calculation: " + psc_geom_scratch);
   }

   // Open the file with Cartesian coordinates, as obtained from Tana
   std::string data_file = psc_geom_scratch + "/" + "XYZfromPsC";
   std::ifstream sstream(data_file);
   if (!sstream.is_open())
   {
      MIDASERROR("Could not open data file: " + data_file + ", please investigate");
   }

   // Extract the Cartesian coordinates from file
   std::string s;
   std::vector<std::vector<Nb>> nuclei_coords;
   while (std::getline(sstream, s))
   {
      // If a line contain the # character, then it is a comment line and can be skipped
      std::size_t found_comment_line = s.find("#");
      if (found_comment_line != std::string::npos)
      {
         continue;
      }

      auto nucleus_coords = midas::util::VectorFromString<Nb>(s);
      if (nucleus_coords.size() != I_3)
      {
         MIDASERROR("Too few Cartesian coordinates are returned from the Tana program for a nucleus, please investigate the XYZfromPsC file");
      }
      nuclei_coords.push_back(nucleus_coords);
   }

   auto number_of_nuclei = aMolecule.GetNumberOfNuclei();
   if (nuclei_coords.size() != number_of_nuclei)
   {
      MIDASERROR("Too few Cartesian coordinates are returned from the Tana program for the total number of nuclei, please investigate");
   }
   
   // Construct the displaced structure in terms of Cartesian coordinates and return it
   for (In j = I_0; j < number_of_nuclei; ++j)
   {
      displaced_geom_cartesian[j].SetX(nuclei_coords[j][I_0]);
      displaced_geom_cartesian[j].SetY(nuclei_coords[j][I_1]);
      displaced_geom_cartesian[j].SetZ(nuclei_coords[j][I_2]);
   }

   // Remove the geometry scratch directories after displaced Cartesian structures have been obtained
   if (!gPesCalcDef.GetmSavePscGeomScrDir())
   {
      midas::filesystem::Rmdir(psc_geom_scratch, true);
   }

   return displaced_geom_cartesian;
}

/**
 *
 **/
void CreateKinOperNormCoord
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const CalcCode& aCalcCode
   ,  const pes::ScalingInfo& aScalingInfo
   ,  const std::vector<Nuclei>& aStructure
   ,  kin_oper_t& aKinOper
   )
{
   // Take care of coriolis couplings for Watson operator
   if (aPesCalcDef.GetmPesCalcMuTens())
   {
      auto q_values = GenerateQValues(aMolecule, aCalcCode, aScalingInfo);
      Inertia tens_in(aMolecule.IsLinear());
      CoriolisCoeffs coriolis(aMolecule.GetNoOfVibs());
      coriolis.FillCoriolisMatrices(aMolecule.GetCoord(), aMolecule.NormalCoord());
      tens_in.GenImatInv(aStructure, aMolecule.GetCoord(), q_values, aMolecule.NormalCoord(), &coriolis);
      if (aMolecule.IsLinear())
      {
         aKinOper.insert(make_pair(string("MU_LINEAR_INERTIA"), tens_in.GetMuVal()));
      }
      else
      {
         //mbh: typecast strings to avoid typecast warning for pgCC
         aKinOper.insert(std::make_pair(std::string("TRACE_EFFINERTIAINV"), tens_in.GetMuTrace()));
         aKinOper.insert(std::make_pair(std::string("XX-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(0,0)));
         aKinOper.insert(std::make_pair(std::string("XY-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(0,1)));
         aKinOper.insert(std::make_pair(std::string("XZ-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(0,2)));
         aKinOper.insert(std::make_pair(std::string("YY-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(1,1)));
         aKinOper.insert(std::make_pair(std::string("YZ-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(1,2)));
         aKinOper.insert(std::make_pair(std::string("ZZ-1_EFFINERTIAINV"), tens_in.GetImatCorrInv(2,2)));
      }
   }
}

/**
 *
 **/
void CreateKinOperGeneric
   (
   )
{
   //auto metric = std::unique_ptr<BaseMetric>(new InvTypeMetric(*this));
   //metric->CalculateKEOTerms(aKinOper, aNewStruct);
}

//! Mutex for making writing of structures to disc concurrent
std::mutex dump_mutex;

/**
 * Dump structure to disc.
 *
 * @param aStructure    The structure to dump.
 * @param aCalcCode     The calculation code for the structure.
 * @param aAnalysisDir  The directory in which files for later analysis should be placed
 * @param aPesCalcDef   Reference to calcdef.
 **/
void DumpStructureToDisc
   (  const std::vector<Nuclei>& aStructure
   ,  const CalcCode& aCalcCode
   ,  const std::string& aAnalysisDir
   ,  const PesCalcDef&  aPesCalcDef
   )
{
   enum format_type {scientific, fixed};

   auto format_line  = midas::input::ToUpperCase(aPesCalcDef.GetSaveDispGeomFormat());
   auto format_split = midas::util::StringVectorFromString(format_line);

   int         precision = 20;
   format_type format    = format_type::scientific;

   for(int i = 0; i < format_split.size(); ++i)
   {
      if(auto optional_precision = midas::util::FromStringOptional<In>(format_split[i]))
      {
         precision = *optional_precision;
      }
      else if(format_split[i] == "SCIENTIFIC")
      {
         format = format_type::scientific;
      }
      else if(format_split[i] == "FIXED")
      {
         format = format_type::fixed;
      }
      else
      { 
         MidasWarning("DumpStructureToDisc(...): Format flag '" + format_split[i] + "' ignored.");
      }
   }

   std::lock_guard<std::mutex> dump_lock(dump_mutex);

   std::string geom_file_name = aAnalysisDir + "/" + "displaced_structures";
   std::ofstream geom_file; ///< Output file for displaced structures
   geom_file.open(geom_file_name, std::ofstream::out | std::ofstream::app);
   geom_file.precision(precision);
      
   if(format == format_type::fixed)
   {
      geom_file.setf(std::ofstream::fixed);
      geom_file.setf(std::ofstream::showpoint);
   }
   else
   {
      geom_file.setf(std::ofstream::scientific);
   }

   std::ios::fmtflags f( geom_file.flags() );
   geom_file << aStructure.size() << std::endl;
   geom_file << aCalcCode.GetString() << std::endl;
   for (In l = I_0; l < aStructure.size(); ++l)
   {
      Nb x = aStructure[l].X();
      Nb y = aStructure[l].Y();
      Nb z = aStructure[l].Z();

      geom_file << " " << aStructure[l].AtomLabel() 
         << "  " << (x >= 0 ? " ":"") << x
         << "  " << (y >= 0 ? " ":"") << y 
         << "  " << (z >= 0 ? " ":"") << z << std::endl;
   }
   geom_file.flags(f);
   geom_file.close();
}


/**
 * Create SP structure for displacement of a molecule.
 *
 * @param aPesCalcDef        The pes calculation definition.
 * @param aMolecule          The molecule to displace.
 * @param aCalcNumber        The (unique) calculation number
 * @param aCalcCode          The calculation code defining the displacement.
 * @param aPesInfo           Iterative surface information.
 * @param aDisplacementType  How to generate displacement.
 *
 * @return   Returns displaced structure as std::vector of Nuclei.
 **/
std::vector<Nuclei> CreateDisplacedStructure
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const In& aCalcNumber
   ,  const CalcCode& aCalcCode
   ,  const PesInfo& aPesInfo
   ,  DisplacementType aDisplacementType = DisplacementType::UNKNOWN
   )
{
   // Need information on the different kinds of scaling
   auto scaling_info = aPesInfo.GetScalingInfo();

   // Set displacement type, if it is not set in input
   if (aDisplacementType == DisplacementType::UNKNOWN)
   {
      aDisplacementType = GetDisplacementType(aPesCalcDef);
   }
   
   // Create displaced structure
   std::vector<Nuclei> displaced_structure;

   switch (aDisplacementType)
   {
      case DisplacementType::RECTILIN:
      {
         displaced_structure = CreateDisplacedStructureNormCoord(aPesCalcDef, aMolecule, aCalcCode, scaling_info);
         break;
      }
      case DisplacementType::CARTESIAN:
      {
         displaced_structure = CreateDisplacedStructureCartesian(aPesCalcDef, aMolecule, aCalcCode, scaling_info);
         break;
      }
      case DisplacementType::POLYSPHERICAL:
      {
         displaced_structure = CreateDisplacedStructurePolyspherical(aPesCalcDef, aMolecule, aCalcNumber, aCalcCode, aPesInfo);
         break;
      }
      default:
      {
         MIDASERROR("Unknown displacement type.");
      }
   }

   // If requested dump structure to disc
   if (aPesCalcDef.GetmSaveDispGeom())
   {
      DumpStructureToDisc(displaced_structure, aCalcCode, aPesInfo.GetMultiLevelInfo().AnalysisDir(), aPesCalcDef);
   }
   
   // Return displaced structure
   return displaced_structure;
}

/**
 * Create kinetic operator for displaced structure.
 *
 * @param aPesCalcDef The pes calculation definition.
 * @param aMolecule   The molecule.
 * @param aCalcCode   The calculation code.
 * @param aScalingInfo  The scaling information.
 * @param aStructure  The displaced structure.
 * @param aKinOper    On output will have appended the kinetic oper terms.
 * @param aDisplacementType The type of displacement.
 **/
void CreateKinOper
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  const CalcCode& aCalcCode
   ,  const pes::ScalingInfo& aScalingInfo
   ,  const std::vector<Nuclei>& aStructure
   ,  kin_oper_t& aKinOper
   ,  DisplacementType aDisplacementType = DisplacementType::UNKNOWN
   )
{
   // Set displacement type, if it is not set in input
   if (aDisplacementType == DisplacementType::UNKNOWN)
   {
      aDisplacementType = GetDisplacementType(aPesCalcDef);
   }

   switch (aDisplacementType)
   {
      case DisplacementType::RECTILIN:
      {
         if (aPesCalcDef.GetmPesCalcMuTens())
         {
            CreateKinOperNormCoord(aPesCalcDef, aMolecule, aCalcCode, aScalingInfo, aStructure, aKinOper);
         }
         break;
      }
      case DisplacementType::CARTESIAN:
      {
         // Nothing to do.
         break;
      }
      // If using polyspherical coordinates, then Coriolis terms are already supplied and should not be constructed
      case DisplacementType::POLYSPHERICAL:
      {
         break;
      }
      default:
      {
         MIDASERROR("Unknown displacement type.");
      }
   }
}

} /* namespace detail */

/* ==========================================================================
 *
 * DisplacementGenerator implementation
 *
 * ========================================================================== */

/**
 * Constructor.
 *
 * @param aPesCalcDef   The pes calculation definition.
 * @param aPesInfo      The pes info.
 * @param aMolecule     The molecule.
 **/
DisplacementGenerator::DisplacementGenerator
   (  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   ,  const molecule::MoleculeInfo& aMolecule
   )
   :  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo)
   ,  mMolecule(aMolecule)
{
}

/**
 * Generate displaced structure.
 *
 * @param aCalcNumber   The (unique) calculation number
 * @param aCalcCode     The calculation code to generate displaced structure for.
 * @param aStructure    On output will contain displaced structure.
 * @param aKinOper      ON output will contain map with added kinetic oper terms.
 **/
void DisplacementGenerator::GenerateDisplacedStructure
   (  const In& aCalcNumber
   ,  const CalcCode& aCalcCode
   ,  std::vector<Nuclei>& aStructure
   ,  std::map<std::string, Nb>& aKinOper
   )  const
{
   // Generate displaced structure
   aStructure = ::midas::pes::detail::CreateDisplacedStructure(mPesCalcDef, mMolecule, aCalcNumber, aCalcCode, mPesInfo);

   // Generate kin oper
   ::midas::pes::detail::CreateKinOper(mPesCalcDef, mMolecule, aCalcCode, mPesInfo.GetScalingInfo(), aStructure, aKinOper);
}

} /* namespace pes */
} /* namespace midas */

