/**
************************************************************************
* 
* @file                CutAna.h
*
* Created:             
*
* Author:             
*
* Short Description:   Class for holding operator definition and utilities
* 
* Last modified: Tue Jul 31, 2007  01:50PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef CUTANA_H
#define CUTANA_H

// std headers
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include<utility>
  
// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

// using declarations
using std::ifstream;
using std::string;
using std::vector;
using std::map;

class CutAna
{
//   private:
  private:
      In mNfree;                       ///< Number of free coordinates in cut 
      vector<In> mQfreeNames;          ///< Names of free coordinates 
      vector<In> mQfreeN;              ///< Number of intervals for free coordinates 
      In mNfixed;                      ///< Number of fixed coordinates in cut 
      vector<In> mQfixedNames;         ///< Names of free coordinates 
      vector<Nb> mQfixedValues;        ///< Values for fixed coordinates 
  public:
      CutAna();
      ~CutAna();

      friend void InitiateCutAnalysis(CutAna* mCuts);
      void PrepareCutFile( const std::vector<Nb>& mFreq, const PesInfo& aPesInfo);
         
         
      friend class OpDef;
      friend string OperInput(ifstream& Minp,bool aReadActOp);
      void GetFreqsOnFile(vector< pair<In,Nb> >& arNumberVector, string arName);
};

#endif

