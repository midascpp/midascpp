/**
************************************************************************
* 
* @file                PropertyInfo.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PropertyInfo datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_PES_PROPERTY_INFO_H_INCLUDED
#define MIDAS_PES_PROPERTY_INFO_H_INCLUDED

// std headers
#include <string>
#include <vector>

#include "libmda/util/to_string.h"

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"

#ifdef VAR_MPI
#include "mpi/Blob.h"
#endif /* VAR_MPI */

namespace midas
{
namespace pes 
{

/**
 * Holds the information on a single property for a singlepoint, 
 * this could e.g be GROUND_STATE_ENERGY, X_DIPOLE, etc.
 **/
class PropertyInfoEntry
{
   private:
      //! Order of the property
      In mOrder;
      //! Rotation group of the property
      In mRotGroup; 
      //! Element index in property tensor.
      std::vector<In> mElement; 
      //! Property descriptor, e.g. GROUND_STATE_ENERGY, X_DIPOLE, etc.
      std::string mDescriptor;
      //! Derivative file
      std::string mDerFile; 
      //!
      std::string mType;
      //!
      std::vector<Nb> mFrq;
      
      //! Construct a map from a string (called from the static Setup function).
      bool ConstructMapFromString(std::string aS, std::map<std::string,std::string>& aKeyValMap) const;
      
      //!@{
      //! Internal setters.
      bool SetDescriptor (std::map<std::string, std::string>& aKeyValMap);
      bool SetOrder      (std::map<std::string, std::string>& aKeyValMap);
      void SetRotGroup   (std::map<std::string, std::string>& aKeyValMap);
      void SetElement    (std::map<std::string, std::string>& aKeyValMap);
      void SetDerivatives(std::map<std::string, std::string>& aKeyValMap);
      void SetType       (std::map<std::string, std::string>& aKeyValMap);
      void SetFrq        (std::map<std::string, std::string>& aKeyValMap);
      //!@}
      
      //! Print input map from parsing the property info file.
      static void PrintMap(const std::map<std::string,std::string>& aKeyValMap);

   public:
      //! Default constructor
      PropertyInfoEntry();
      
      //! Default constructor
      PropertyInfoEntry(const std::string& aDescriptor);

      //! Deleted copy constructor.
      PropertyInfoEntry(const PropertyInfoEntry&) = default;
      
      //! Deleted copy assignment.
      PropertyInfoEntry& operator=(const PropertyInfoEntry& aOtherInfo) = delete;

      //! Default move constructor.
      PropertyInfoEntry(PropertyInfoEntry&&) = default;
      
      //! Get rotation group.
      In GetRotGroup()                    const { return mRotGroup; }
      
      //! Get the index of the property for the rotation group.
      const std::vector<In>& GetElement() const { return mElement; }

      //! Get order of property, e.g. energy is a scalar so it is order 0.
      In GetOrder()                       const { return mOrder; }
      
      //! Get description, e.g. "GROUND_STATE_ENERGY".
      const std::string& GetDescriptor()  const { return mDescriptor; }

      //!
      const std::string& GetDerFile()     const { return mDerFile; }

      //!
      std::string GetInfoLine() const
      {
         std::string info = "";
         
         if(!mType.empty())
         {
            info += "type=" + mType;
            for(int i = 0; i < mFrq.size(); ++i)
            {
               info += " frq_" + std::to_string(i) + "=" + libmda::util::to_string_with_precision(mFrq[i]);
            }
         }

         return info;
      }
      
      //! Construct a map of property infos from the property info file
      static std::vector<PropertyInfoEntry> Setup(const std::string& aMidasIfcPropInfo);

      //! Output operator overload
      friend ostream& operator<<(ostream& aOS, const PropertyInfoEntry& aPropInfo);
};

/**
 * PropertyInfo
 **/
class PropertyInfo
{
   private:
      //! Vector of all property infos.
      std::vector<PropertyInfoEntry> mPropertyInfos;

   public:
      //! Default constructor.
      PropertyInfo() = default;
         
      //! Update property information (used for when starting a new level in the multilevel scheme).
      void Update(bool aCalcMuTens, const std::string& aMidasIfcPropInfo, bool aLinear);
      
      //! Update property information (used for when starting a new level in the multilevel scheme).
      void Update(const PesCalcDef& aPesCalcDef, const std::string& aMidasIfcPropInfo, bool aLinear);
 
      //! Check that the property information does not contain any duplicate entries.
      void CheckPropertyInfos(const std::vector<PropertyInfoEntry>& aPropertyInfos);

      //! Get number of properties.
      In Size() const;

      //! Get PropertyInfoEntry indexed by integer.
      const PropertyInfoEntry& GetPropertyInfoEntry(In aI) const;
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_PROPERTY_INFO_H_INCLUDED */
