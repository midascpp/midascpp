#ifndef MIDAS_PES_PES_UTILITY_H_INCLUDED
#define MIDAS_PES_PES_UTILITY_H_INCLUDED

/* 
 * These functions were moved here from PesInfo.
 * At some point the code that uses these functions should propbably be refactored 
 * and the functions should be removed!
 *
 * NB: Do not use these functions in any new code!
 */

#include <string>
#include <vector>
#include <utility>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "pes/PesInfo.h"

namespace midas
{
namespace pes
{

//! Find property number in file.
In FindPropertyNumberInFile
   (  const PesInfo& aPesInfo
   ,  const std::string&
   ,  const std::string&
   ,  In
   ,  In Iopt = I_0
   ); 

//! Find property number in file.
In FindPropertyNumberInFile
   (  const PesInfo& aPesInfo
   ,  const std::string&
   ,  In
   ,  In Iopt = I_0
   );

//!
void DumpFreqsOnFile
   (  std::vector< std::pair<In,Nb> >& arNumberVector
   ,  const std::string& arName
   );

//!
void GetFreqsOnFile
   (  std::vector< std::pair<In,Nb> >& arNumberVector
   ,  const std::string& arName
   );

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_PES_UTILITY_H_INCLUDED */
