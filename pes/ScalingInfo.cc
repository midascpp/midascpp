#include "pes/ScalingInfo.h"

#include "mpi/Impi.h"


namespace midas
{
namespace pes
{
namespace detail
{

/**
 * Initialize scaling factors for Pes construction using normal coordinates.
 *
 * @param aPesCalcDef        The pes calculation definition.
 * @param aMolecule          The molecule.
 * @param aRScalFactVector   On output holds right scaling factors.
 * @param aLScalFactVector   On output holds left scaling factors.
 **/
void RectilinearCoordScalingFactors
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  MidasVector& aRScalFactVector
   ,  MidasVector& aLScalFactVector
   )
{
   // Initialize correct size for scaling factor vectors.
   aRScalFactVector.SetNewSize(aMolecule.GetNoOfVibs());
   aLScalFactVector.SetNewSize(aMolecule.GetNoOfVibs());

   // Get some constants
   const auto  number_of_nuclei = aMolecule.GetNumberOfNuclei();
   const auto  number_of_vibs   = aMolecule.GetNoOfVibs();
   //const auto& freqs            = aMolecule.GetFreqs();
   
   // Construct scaling factor vectors based on displacement type.
   if (aPesCalcDef.GetmPesDispType() == "SIMPLEDISPLACEMENT")
   {
      if (aPesCalcDef.IoLevel() > I_5)
      {
         Mout << " Will use simple (non-frequency scaled) displacements " << std::endl;
      }

      MidasVector VectorOfNormalCoord(3 * number_of_nuclei);
      Nb dot_sqrt = C_0;
      for (In i=I_0;i<number_of_vibs;i++)
      {
         VectorOfNormalCoord.Zero();
         aMolecule.NormalCoord().GetRow(VectorOfNormalCoord,i);
         dot_sqrt = VectorOfNormalCoord.Norm(); 
         aRScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu()/dot_sqrt;
         aLScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu()/dot_sqrt;
      }
   }
   else if (   (aPesCalcDef.GetmPesDispType() == "FREQDISPLACEMENT") 
           &&  (!aPesCalcDef.GetmPesGrid()) 
           )
   {
      if (aPesCalcDef.IoLevel() > I_5)
      {
         Mout << " Will use frequency scaled displacements " << std::endl;
      }

      Nb MinFreq = aMolecule.GetFreqI(0);
      for(In i = I_1; i < number_of_vibs; ++i)
      {
         if(aMolecule.GetFreqI(i) < MinFreq)
         {
            MinFreq = aMolecule.GetFreqI(i);
         }
      }
      MidasVector VectorOfNormalCoord(3 * number_of_nuclei);
      for (In i=I_0;i<number_of_vibs;i++)
      {
         Nb dot_sqrt = C_0;
         VectorOfNormalCoord.Zero();
         aMolecule.NormalCoord().GetRow(VectorOfNormalCoord, i);
         dot_sqrt = VectorOfNormalCoord.Norm(); 
         aRScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu()*sqrt(MinFreq/aMolecule.GetFreqI(i))/dot_sqrt;
         aLScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu()*sqrt(MinFreq/aMolecule.GetFreqI(i))/dot_sqrt;
      }
   }
   else if ( (aPesCalcDef.GetmPesDispType() == "FREQDISPLACEMENT") && (aPesCalcDef.GetmPesGrid()) )
   {
      //    Do a little testing on the scaling factors for the grid settings
      //    The default is to use grid settings from the HO basis set. However, the dimesnsion can also
      //    be typed in explicitly. This dimensio is in the vector aPesCalcDef.GetmPesUserDefinedBound() ordered after the modes
      if (aPesCalcDef.IoLevel() > I_5)
      {
         Mout << " Will use frequency scaled displacements " << std::endl;
      }

      bool Userdef = false;
      if (aPesCalcDef.GetmPesUserDefinedBound().size() != I_0)
      {
         Userdef = true;
      }
      
      // Calculate grid scaling
      std::vector<Nb> grid_scal(number_of_vibs);
      if (aPesCalcDef.GetmPes_Grid_Scal().size() == I_1)
      {
         Nb local_fac = aPesCalcDef.GetmPes_Grid_Scal()[I_0];
         for (In i = I_0; i < number_of_vibs; i++)  
         {
            grid_scal[i] = local_fac; //the first already there.
         }
      }
      else if (aPesCalcDef.GetmPes_Grid_Scal().size() != number_of_vibs)
      {
         MidasWarning("Use the one given PesGridScal value for all modes");
         Nb value = aPesCalcDef.GetmPes_Grid_Scal().at(I_0);
         for (In i=I_0; i < number_of_vibs; ++i) 
         {
            grid_scal[i] = value;
         }
      }
      else
      {
         for (In i=I_0;i<number_of_vibs;i++)
         {
            grid_scal[i] = aPesCalcDef.GetmPes_Grid_Scal().at(i);
         }
      }

      if (aPesCalcDef.IoLevel() > I_10)
      {
         Mout << " grid_scal = " << grid_scal << std::endl;
      }
      
      // Then calculate actual scaling.
      if (!Userdef)
      {
         if (aPesCalcDef.GetmPesAdga())
         {
            for (In i = I_0; i < number_of_vibs; i++)
            {
               aRScalFactVector[i] = std::sqrt(C_2 * (Nb(aPesCalcDef.GetmPesGridInitialDim()) + C_I_2)/(aMolecule.GetFreqI(i)/C_AUTKAYS)) * (C_1/sqrt(C_FAMU)) * grid_scal[i];
               aLScalFactVector[i] = std::sqrt(C_2 * (Nb(aPesCalcDef.GetmPesGridInitialDim()) + C_I_2)/(aMolecule.GetFreqI(i)/C_AUTKAYS)) * (C_1/sqrt(C_FAMU)) * grid_scal[i];
            }
         }
         else
         {
            for (In i = I_0; i < number_of_vibs; i++)
            {
               aRScalFactVector[i] = std::sqrt(C_2 * (Nb(aPesCalcDef.GetmPesGridMaxDim()) + C_I_2)/(aMolecule.GetFreqI(i)/C_AUTKAYS)) * (C_1/sqrt(C_FAMU)) * grid_scal[i];
               aLScalFactVector[i] = std::sqrt(C_2 * (Nb(aPesCalcDef.GetmPesGridMaxDim()) + C_I_2)/(aMolecule.GetFreqI(i)/C_AUTKAYS)) * (C_1/sqrt(C_FAMU)) * grid_scal[i];
            }
         }
      }
      else
      {
         if (aPesCalcDef.GetmPesUserDefinedBound().size() != number_of_vibs)
         {
            MIDASERROR("Wrong dimension in user defined grid dimension. I quit.");
         }

         for (In i = I_0; i < number_of_vibs; i++)
         {
            aRScalFactVector[i] = aPesCalcDef.GetmPesUserDefinedBound()[i] * grid_scal[i];
            aLScalFactVector[i] = aPesCalcDef.GetmPesUserDefinedBound()[i] * grid_scal[i];
         }
      }

      // If Adga pes the box is automatically quadrupled
      if (aPesCalcDef.GetmPesAdga())
      {
         aRScalFactVector.Scale(std::pow(C_2, aPesCalcDef.GetmPesItGridExpScalFact()));
         aLScalFactVector.Scale(std::pow(C_2, aPesCalcDef.GetmPesItGridExpScalFact()));
      }
   }
   else
   {
      MIDASERROR(" Displacements for " + aPesCalcDef.GetmPesDispType() + " is not known. I stop ... ");
   }

}

/**
 * Initialize scaling factor for Cartesian calculation.
 *
 * @param aPesCalcDef        The pes calculation definition.
 * @param aMolecule          The molecule.
 * @param aRScalFactVector   On output holds right scaling factors.
 * @param aLScalFactVector   On output holds left scaling factors.
 **/
void CartesianScalingFactors
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  MidasVector& aRScalFactVector
   ,  MidasVector& aLScalFactVector
   )
{
   // Initialize correct size for scaling factor vectors.
   aRScalFactVector.SetNewSize(aMolecule.GetNoOfVibs());
   aLScalFactVector.SetNewSize(aMolecule.GetNoOfVibs());

   // Init scalings to step size in au (given on input)
   for (In i = I_0; i < aRScalFactVector.Size(); ++i)
   {
      aRScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu();
      aLScalFactVector[i] = aPesCalcDef.GetmPesStepSizeInAu();
   }
}


/**
 * Initialize scaling factor for calculations using internal (curvilinear) coordinates as representation.
 *
 * @param aPesCalcDef        The pes calculation definition.
 * @param aMolecule          The molecule.
 * @param aRScalFactVector   On output holds right scaling factors.
 * @param aLScalFactVector   On output holds left scaling factors.
 **/
void InternalCoordinateScalingFactors 
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  MidasVector& aRScalFactVector
   ,  MidasVector& aLScalFactVector
   )
{
   // Get the number of vibrational modes
   auto no_vib_modes = aMolecule.GetNoOfVibs();

   // Set correct size for scaling factor containers
   aRScalFactVector.SetNewSize(no_vib_modes);
   aLScalFactVector.SetNewSize(no_vib_modes);

   // Get the type and reference value of the internal coordinate
   auto mode_types = aMolecule.GetmTypeLabels();
  
   MidasVector ref_mode_values;
   ref_mode_values.SetNewSize(no_vib_modes);
   if (aMolecule.HasInactiveModes())
   {
      ref_mode_values = aMolecule.GetmInternalActiveCoords();
   }
   else
   {
      ref_mode_values = aMolecule.GetmInternalCoord();
   }
   //auto ref_mode_values = aMolecule.GetmInternalCoord();

   // Get scale factor modifiers
   auto grid_bounds_modifiers = aPesCalcDef.GetmPscMaxGridBounds();

   // Define the scaling factors in relation to the type of internal, these will be used as the absolute box sizes
   for (In i = I_0; i < no_vib_modes; ++i)
   {
      // A distance
      if (mode_types[i].at(I_0) == 'R')
      {
         aRScalFactVector[i] = ref_mode_values[i] + grid_bounds_modifiers[I_0];
         aLScalFactVector[i] = aRScalFactVector[i];
      }
      // A valence angle
      else if (mode_types[i].at(I_0) == 'V')
      {
         aRScalFactVector[i] = (C_PI - ref_mode_values[i]) + grid_bounds_modifiers[I_1];
         aLScalFactVector[i] = ref_mode_values[i] + grid_bounds_modifiers[I_1];
      }
      // A dihedral angle
      else if (mode_types[i].at(I_0) == 'D')
      {
         if (ref_mode_values[i] == C_0)
         {
            if (aPesCalcDef.IoLevel() > I_7)
            {
               Mout << " Dihedral angle " << mode_types[i] << " is equal to zero, will set symmetric extension space " << std::endl; 
            }
            
            aRScalFactVector[i] = C_PI + grid_bounds_modifiers[I_2];
            aLScalFactVector[i] = aRScalFactVector[i];
         }
         else
         {
            aRScalFactVector[i] = (C_2 * C_PI - ref_mode_values[i]) + grid_bounds_modifiers[I_2];
            aLScalFactVector[i] = ref_mode_values[i] + grid_bounds_modifiers[I_2];
         }
      }
      else
      {
         MIDASERROR("Unrecognized type of internal mode, please make sure that the Molecule.mmol file is set up correctly");
      }
   }
}

/**
 * Initialize scaling factor vectors.
 * On output aRScalFactVector and aLScalFactorVector 
 * will hold right and left scaling factors respectively.
 *
 * @param aPesCalcDef        The pes calculation definition.
 * @param aMolecule          The molecule.
 * @param aRScalFactVector   On output holds right scaling factors.
 * @param aLScalFactVector   On output holds left scaling factors.
 **/
void InitializeScalingFactors
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   ,  MidasVector& aRScalFactVector
   ,  MidasVector& aLScalFactVector
   )
{
   // Initialize scaling factors based on how we want to construct the surface(s)
   if (aPesCalcDef.GetmNormalcoordInPes())
   {
      if (aPesCalcDef.IoLevel() > I_9)
      {
         Mout << " Normal (rectilinear) coordinate scaling " << std::endl;
      }

      RectilinearCoordScalingFactors(aPesCalcDef, aMolecule, aRScalFactVector, aLScalFactVector);
   }
   else if (aPesCalcDef.GetmCartesiancoordInPes() || aPesCalcDef.GetmPesDiatomic())
   {
      if (aPesCalcDef.IoLevel() > I_9)
      {
         Mout << " Cartesian coordinate scaling " << std::endl;
      }

      CartesianScalingFactors(aPesCalcDef, aMolecule, aRScalFactVector, aLScalFactVector);
   }
   else if (aPesCalcDef.GetmFalconInPes())
   {
      if (aPesCalcDef.IoLevel() > I_9)
      {
         Mout << " FALCON (rectilinear) coordinate scaling " << std::endl;
      }
      
      RectilinearCoordScalingFactors(aPesCalcDef, aMolecule, aRScalFactVector, aLScalFactVector);
   }
   else if (aPesCalcDef.GetmPscInPes())
   {
      if (aPesCalcDef.IoLevel() > I_9)
      {
         Mout << " Internal coordinate scaling " << std::endl;
      }
      
      InternalCoordinateScalingFactors(aPesCalcDef, aMolecule, aRScalFactVector, aLScalFactVector);
   }
   else
   {
      MIDASERROR("Error when calculating scaling factors : No/unknown molecule type selected.");
   }
   
   // Do a little optional printout
   if (aPesCalcDef.IoLevel() > I_10)
   {
      aRScalFactVector.PrintVertical(" Scaling factors in RScalFactVector in NumDer ");
      aLScalFactVector.PrintVertical(" Scaling factors in LScalFactVector in NumDer ");
   }
}

} /* namespace detail */

/**
 * Constructor.
 *
 * @param aPesCalcDef  CalcDef for current pes calculation.
 * @param aMolecule    The molecule we are constructing a pes for.
 **/
ScalingInfo::ScalingInfo
   (  const PesCalcDef& aPesCalcDef
   ,  const molecule::MoleculeInfo& aMolecule
   )
{
   detail::InitializeScalingFactors(aPesCalcDef, aMolecule, mRScalFactVector, mLScalFactVector);
}

} /* namespace pes */
} /* namespace midas */
