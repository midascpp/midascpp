/**
************************************************************************
*
* @file                Derivatives.cc
*
* Created:             17-07-2009
*
* Author:              Manuel Sparta (msparta@chem.au.dk)
*
* Short Description:   Class members definitions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include <vector>
#include <list>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "pes/Derivatives.h"
#include "util/Io.h"

/**
* Default constructor
**/
Derivatives::Derivatives
   (
   )
{
}

/**
 * For a given property return the derivatives container and handler.
 *
 * @param aProp          The property to find the derivatives for.
 * @param aDerCont       The derivatives of the property.
 * @param aDerContHand   The handler?
 **/
void Derivatives::GetDerivatives
   (  const std::string& aProp
   ,  std::vector<Nb>& aDerCont
   ,  std::map<In,In>& aDerContHand
   )  const
{
  for (In i=I_0; i<mProp.size(); i++)
  {
     if (mProp[i] == aProp)
     {
        aDerCont = mVecDerCont[i];
        aDerContHand = mVecDerContHandler[i];
        return;
     }
  }

  Mout << " I could find derivative info stored for prop: " << aProp << std::endl;
  return;
}

/**
 *  Store a set of derivatives containers and handler.
 *
 *  @param aProp           The property to set the derivatives for.
 *  @param aOrder          The order of the property.
 *  @param aDerCont        The derivatives of the property.
 *  @param aDerContHand    The handler?
 **/
void Derivatives::SetDerivatives
   (  const std::string& aProp
   ,  const In& aOrder
   ,  const std::vector<Nb>& aDerCont
   ,  const std::map<In, In>& aDerContHand
   )
{
   for (In i = I_0; i < mProp.size(); i++)
   {
      if (mProp[i] == aProp)
      {
         Mout << " Updating derivative information for property: " << aProp << std::endl;
         mVecDerCont[i] = aDerCont;
         mVecDerContHandler[i] = aDerContHand;
         return;
      }
   }
 
   if (mProp.size() != mVecDerCont.size() || mProp.size() != mVecDerContHandler.size())
   {
      MIDASERROR("Something strange, it seems that sizes do not match");
   }
 
   mProp.push_back(aProp);
   mOrder.push_back(aOrder);
   mVecDerCont.push_back(aDerCont);
   mVecDerContHandler.push_back(aDerContHand);
}

/**
 * Check if a prop has available derivatives.
 *
 * @param aProp  The property to check.
 *
 * @return    Return true if property is available.
 **/
bool Derivatives::AvailableDer
   (  const std::string& aProp
   )  const
{
  for (In i=I_0; i<mProp.size(); i++)
  {
     if (mProp[i] == aProp)
     {
        return true;
     }
  }
  return false;
}

/**
 * Return the order of the available derivatives for a property.
 *
 * @param aProp   The property to get the order for.
 *
 * @return     Return rthe order of the property.
 **/
In Derivatives::OrderDer
   (  const std::string& aProp
   )  const
{
  for (In i=I_0; i<mProp.size(); i++)
  {
     if (mProp[i] == aProp)
     {
        return mOrder[i];
     }
  }
  return I_0;
}

/** 
 * Overload for output operator.
 *
 * @param aOs            The ostream to output to.
 * @param arDerivatives  The derivatives to output.
 *
 * @return Return ostream for chaining of operator<<.
 **/
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const Derivatives& arDerivatives
   )
{
   aOs << " DERIVATIVE CONTAINER \n"
       << " PROP " <<  arDerivatives.mProp << "\n"
       << " ORDER " << arDerivatives.mOrder << "\n"
       << " VECDERCONT " << arDerivatives.mVecDerCont << "\n"
       << " VECDERCONTHANDLER " << arDerivatives.mVecDerContHandler << "\n"
       << std::flush;

   return aOs;
}
