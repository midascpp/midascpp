/**
************************************************************************
* 
* @file                SinglePoint.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PesSinglePoint datatype. Interface to SinglePoint for the PES module.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/PesSinglePoint.h"

// std headers
#include <string>
#include <vector>
#include <unistd.h>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "util/Io.h"
#include "mmv/MidasMatrix.h"
//#include "pes/singlepoint/SinglePoint.h"
#include "pes/PesFuncs.h"
#include "pes/CalcCode.h"
#include "pes/PesInfo.h"
#include "pes/molecule/MoleculeInfo.h"
//#include "pes/molecule/MoleculeDisplacement.h"

// using declarations

namespace midas
{
namespace pes
{

using namespace PesFuncs;

/**
 * Constructor from calculation code and calculation number.
 * 
 * @param aDisplacementGenerator   The generator for molecular displacements.
 * @param arCalcCode    The calculation code for the point, i.e. the displacement
 * @param aCalcNumber   The calculation number
 **/
PesSinglePoint::PesSinglePoint
   (  const DisplacementGenerator& aDisplacementGenerator
   ,  const CalcCode& arCalcCode
   ,  In aCalcNumber
   ) 
   :  mCalcNumber(aCalcNumber) 
   ,  mCalcCode(arCalcCode)
   ,  mStructure()
   ,  mKinInfo()
   ,  mDisplacementGenerator(aDisplacementGenerator)
{
}

/**
 * @brief Generate structure and store in mStructure
 **/
void PesSinglePoint::ConstructStructure
   (
   )
{
   // The structure is initialized with the reference structure.
   mDisplacementGenerator.GenerateDisplacedStructure(mCalcNumber, mCalcCode, mStructure, mKinInfo);
}


/**
 * Operator less than, used to order calculation lists
 * 
 * @param aS1 Lhs
 * @param aS2 Rhs
 **/
bool operator<
   (  const PesSinglePoint& aS1
   ,  const PesSinglePoint& aS2
   )
{
   return aS1.GetCalculationNumber() < aS2.GetCalculationNumber();
}

} /* namespace pes */
} /* namespace midas */
