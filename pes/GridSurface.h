/**
************************************************************************
* 
* @file                GridSurface.h
*
* Created:             8-3-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   For creating a grid surface 
* 
* Last modified: Thu Jul 15, 2010  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GRIDSURFACE_H_INCLUDED
#define GRIDSURFACE_H_INCLUDED

#include <vector>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "pes/Surface.h"

class Molecule;
class PesInfo;

namespace midas
{
namespace pes
{

/**
 *
 **/
class GridSurface 
   : public Surface
{
   public:

      //! Constructor
      GridSurface(std::vector<PesInfo>& aPesInfos, const std::string& aPesMainDir);

      //! Destructor
      ~GridSurface() = default;

      //! Run Grid surface
      void RunSurface();
 
      //! Ehm...
      void GradHess(const PesInfo& aPesInfo) const;

      //! Ehm...
      In DoLinearComb(std::vector<std::string>&, std::string&);
};

} /* namespace pes */
} /* namespace midas */

#endif /* GRIDSURFACE_H_INCLUDED */
