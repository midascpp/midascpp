/**
************************************************************************
* 
* @file                AdgaBarFileHandler.cc
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<string>
#include<vector>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Isums.h"
#include "util/Io.h"
#include "util/FileHandler.h"
#include "util/FileSystem.h"
#include "util/Timer.h"
#include "input/ModeCombiOpRange.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "pes/AdgaBarFileHandler.h"
#include "pes/adga/ItGridHandler.h"
#include "pes/GridType.h"
#include "pes/Derivatives.h"
#include "pes/PesFuncs.h"
#include "pes/PesInfo.h"
#include "pes/CalcCode.h"
#include "pes/PropertyFileStorage.h"
#include "potentials/MidasPotential.h"
#include "util/CallStatisticsHandler.h"

// using declarations
using std::string;
using std::vector;
using std::map;
using std::pair;

typedef string::size_type Sst;
typedef pair<In,In> ipair;

/**
 * Constructor.
 *
 * @param arPesInfo     Reference to pes info.
 **/
AdgaBarFileHandler::AdgaBarFileHandler
   (  PesInfo& arPesInfo
   ) 
   :  GridFileHandlerMethods(arPesInfo)
{
}

/**
 * Intrinsict driver for actual displacements along coordinates 
 * for calculating either derivatives or coarse grid points.
 **/
void AdgaBarFileHandler::DoDispDer_zero
   (  const ModeCombiOpRange& aModeCom
   ,  In aActualDimMc
   ,  const GridType& GpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_dodispder;
   mPesInfo.CleanUpDerCalcList();
   
   if (gPesIoLevel > I_11) 
   {
      Mout << " Mode Combination Range: " << std::endl << aModeCom << std::endl;
   }

   In IbarCount = I_0;
   std::vector<std::vector<In> > vec_of_all_k_vecs;
   Timer msi_test_timer;

   // Run over the set of modes
   for (const auto& mt: aModeCom)
   {
      // For MC screening neglect the calculation of the full MC
      if (mt.IsToBeScreened())
      {
         continue;
      }
      
      //
      if (!mIterGrid->Is1dConv() && mt.Size() > I_1)
      {
         continue;
      }
      
      // Generate the K vecs for each of the mode combi
      In add = mt.Address();
      std::vector<In> curr_mode = mt.MCVec();
      vec_of_all_k_vecs.clear();

      if (mt.Size() != I_0) 
      {  
         mIterGrid->GetKvecsVecForModeCombi(add, curr_mode, vec_of_all_k_vecs);
      }

      // Loop now over the set of k vectors and do the actual displacements (ModeDisplacements).
      for (In k = I_0; k < vec_of_all_k_vecs.size(); ++k)
      {
         std::vector<In> kvec = vec_of_all_k_vecs[k];
         ModeDisplacements_zero(mt, kvec, GpFr, aCalculationList);
      }
   } 

   if (gTime) 
   {
      time_dodispder.CpuOut(Mout,"\n CPU  time used in the DoDispDer_zero: ");
      time_dodispder.WallOut(Mout," Wall time used in the DoDispDer_zero: ");
   }
}

/**
 * Create displament for mode combination using a k-vector.
 * This creates a calculation code which is added to 
 * the calculation list of singlepoints to be run later.
 * If the mode combination is to be extrapolated, it is added to the list of extrapolated points instead.
 *
 * @param arModeCombi       The mode combination to displace.
 * @param arKvec            Kvector for displacement.
 * @param arGpFr            A grid type...
 * @param aCalculationList  The list of calculations we are adding to.
 **/
void AdgaBarFileHandler::ModeDisplacements_zero
   (  const ModeCombi& arModeCombi
   ,  const std::vector<In>& arKvec
   ,  const GridType& arGpFr
   ,  CalculationList& aCalculationList
   )
{
   Timer time_ada;
   //arMatch = true;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR(" Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

   std::string list_of_calcs = "";
   std::string full_dim_list_of_calcs = "";

   // Generate calculation name and return if we should skip
   if(GenerateCalcName(list_of_calcs, full_dim_list_of_calcs, arKvec, arModeCombi))
   {
      return;
   }
   list_of_calcs = "*" + list_of_calcs + "*";

   // Remove any "#0#"
   while(list_of_calcs.find("#0#")!=list_of_calcs.npos)
   {
      list_of_calcs.erase(list_of_calcs.find("#0#"), I_2);
   }

   //now do the actual displacements ...
   // ----------------------------------------------------------------------
   bool calc_todo=true;
   
   // Check if the dispalcement is needed
   calc_todo = CheckDisplacement(arKvec, arModeCombi, true);
   //if ( gPesCalcDef.GetmPesScreenModeCombi() )
   //{
   //   calc_todo=false;
   //}
   if (!arModeCombi.IsToBeExtrap() || !gPesCalcDef.GetmPesDoExtrap())
   {
      //if(!mPesInfo.IsOnList(list_of_calcs) && calc_todo)
      if(calc_todo)
      {
         aCalculationList.AddEntry(list_of_calcs); //add to the list 
      }
   }
   else if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap())
   {
      if(calc_todo)
      {
         mPesInfo.AddDerCalc(list_of_calcs); //add to the list to be extrapolated
      }
   }
   
   if (gTime)
   {
      time_ada.CpuOut(Mout, "\n CPU  time used in the ModeDisplacements_zero: ", true);
      time_ada.WallOut(Mout, " Wall time used in the ModeDisplacements_zero: ", true);
   }
}

/**
 * Intrinsic driver for actual displacements along coordinates for calculating either derivatives or coarse grid points.
 *
 * @param ModeCom
 * @param ActualMaxDimMc
 * @param GpFr
 * @param arDerivatives
 **/
void AdgaBarFileHandler::DoDispDer_one
   (  const ModeCombiOpRange& ModeCom
   ,  In ActualMaxDimMc
   ,  const GridType& GpFr
   ,  const Derivatives& arDerivatives
   )
{
   Timer time_dodispder;
   
   // Do now the K vectors on basis of the N vectors. All k are constructed but some may not be used actually later. 
   std::vector<InVector> vec_of_all_k_vecs;

   // Open the files for output of the bar and _extrap files
   OpenFiles();

   std::vector< pair<In,Nb> > Vib_Freqs;
   std::set<std::string> Test_Calc;

   // Open file for writing information about the order in the .mbar files
   In IbarCount = I_0;
   In IbarCountSave = I_0;
   std::string LL = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_bar_files_navigation.mpesinfo";
   ofstream BarInterface(LL.c_str(), ios_base::out);

   BarInterface << "# Mode(s) " << " Offset " << std::endl;

   // Set up with vector of PropertyFileStorage objects, each handling a property file.
   std::vector<PropertyFileStorage> vec_prop_file_stor;
   vec_prop_file_stor.reserve(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
   for (Uin i = I_0; i < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i)
   {
      std::string prop_file_name = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(i + I_1) + "_bar.mpoints";
      vec_prop_file_stor.emplace_back(prop_file_name);
   }

   // Load all property files into memory if so requested.
   // NB! Ideally, ought to just have _one_ prop. file in memory at a time, but
   // this would require changing the existing loop structure, but it's not
   // worth the effort at the moment. But feel free to do so, if memory becomes
   // an issue. -MBH, Oct 2017.
   if (gPesCalcDef.GetmPesPropInMem())
   {
      for (auto&& pfs: vec_prop_file_stor)
      {
         pfs.ReadIntoMemory();
      }
   }

   // Set up with vector of PropertyFileStorage objects, each handling a property file.
   std::vector<PropertyFileStorage> vec_prop_dump_file;
   if (gPesCalcDef.GetmPesCalcPotFromOpFile())
   {
      vec_prop_dump_file.reserve(mPesInfo.GetMultiLevelInfo().GetNoOfProps());

      for (Uin i = I_0; i < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i)
      {
         std::string dump_file_name = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(i + I_1) + "_mop_bar.mpoints";
      
         vec_prop_dump_file.emplace_back(dump_file_name);
      
         // Dump file may not yet exist; touch it if so to create; required by PropertyFileStorage.
         if (!midas::filesystem::IsFile(dump_file_name))
         {
            if (midas::filesystem::Touch(dump_file_name) != 0)
            {
               MIDASERROR("Tried to touch " + dump_file_name + " but failed.");
            }
         }
      }
      
      // Load all property files into memory if so requested.
      // NB! Ideally, ought to just have _one_ prop. file in memory at a time, but
      // this would require changing the existing loop structure, but it's not
      // worth the effort at the moment. But feel free to do so, if memory becomes
      // an issue. -MBH, Oct 2017.
      if (gPesCalcDef.GetmPesPropInMem())
      {
         for (auto&& pfs: vec_prop_dump_file)
         {
            pfs.ReadIntoMemory();
         }
      }
   }

   // Then run over the set of modes
   for (const auto& mt: ModeCom)
   {
      if (gDebug || gPesIoLevel > I_14)
      {
         Mout << " current mode: " << mt << " to be screened: " << mt.IsToBeScreened() << ", to be extrap: " << mt.IsToBeExtrap() << std::endl;
      }
      // The mode combination is empty we can skip it
      if (mt.MCVec().size() == I_0 || mt.Size() > mPesInfo.GetMCLevel())
      {
         continue;
      }
      // For MC screening neglect the calculation of the full MC
      if (mt.IsToBeScreened())
      {
         continue;
      }
      // Do not do anything about higher mode couplings yet
      if (!mIterGrid->Is1dConv() && mt.Size() > I_1) 
      {
         continue; 
      }
      // Do nothing if is to be extrapolated
      if (gPesCalcDef.GetmPesDoExtrap() && mt.IsToBeExtrap()) 
      {
         continue; 
      }

      // If the pes is Adgaly constructed, then generate here the K vecs for each of the mode combi
      In add = mt.Address();
      std::vector<In> curr_mode = mt.MCVec();
      //Mout << " Constructing k vecs for mode combi: " << curr_mode << " address: " << add << endl;
      vec_of_all_k_vecs.clear();
      if (mt.Size() != I_0)
      {
         mIterGrid->GetKvecsVecForModeCombi(add, curr_mode,vec_of_all_k_vecs);
      }

      IbarCountSave = IbarCount + I_1;
      BarInterface << " (";
      for (In i = I_0; i < mt.Size(); ++i)
      {
         BarInterface << mPesInfo.GetMolecule().GetModeLabel(mt.Mode(i));
         if (mt.Size() > I_1 && i != mt.Size() - I_1)
         {
            BarInterface << ",";
         }
      }
      BarInterface << ")";
      if (mt.Size() != I_0)
      {
         BarInterface << "    ";
      }
      
      // Sets up the plotting and opens the relavant plot files
      SetupPlot(curr_mode);
      Timer time_all;
      std::map<InVector, Nb> lc_map;
      
      // Generates the linear combinations of property-points which are needed to calculate the bar corrected properties
      GenerateLinearCombinationMap(curr_mode,lc_map);
      //time_all.CpuOut(Mout,"\n CPU  time used in the costruction of the LinCom map: ");
      //time_all.WallOut(Mout," Wall time used in the costruction of the LinCom map: ");
      //Mout << " Linear combination: ";
      //for(map<InVector,Nb>::iterator ci=lc_map.begin(); ci!=lc_map.end();ci++)
      //Mout << ci->second << " " << ci->first << " ";
      //Mout << endl;
      //run over n
      MidasVector Properties(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
      Properties.Zero();
      bool Match;

      //loop now over the set of k vectors and do the actual displacements (ModeDisplacements).
      for (In k = I_0; k < vec_of_all_k_vecs.size(); ++k)
      {
         vector<Nb> Ncor;
         vector<In> kvec = vec_of_all_k_vecs[k];
         string calc_code="";
         string calc_no="";
         vector<MidasVector> grad_hess;

         for (In prop = I_0; prop < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++prop)
         {
            In n_dim=I_0;
            MidasVector grad_hess_v;
            if (mPesInfo.GetmUseDerivatives() && gPesCalcDef.GetmPesShepardInt() )
            {
               In order = gPesCalcDef.GetmPesShepardOrd();                     // At mc=1, It=1 we assume order gPesCalcDef.GetmPesShepardOrd() for all the prop
               if (InquireFile(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_files.mpesinfo"))
               {
                  std::string name_pr = mPesInfo.GetMultiLevelInfo().GetPropertyName(prop);
                  order = arDerivatives.OrderDer(name_pr);
               }
               //Mout << " Shepard interpolation for mode combi: " << mc << endl;
               if (order>I_0) n_dim+=mt.Size();
               if (order==I_2) n_dim+=mt.Size()*(mt.Size()+I_1)/I_2;
               else if (order>I_2) MIDASERROR(" Shepard order greater than 2 ");
            }
            grad_hess_v.SetNewSize(n_dim);
            grad_hess_v.Zero();
            grad_hess.push_back(grad_hess_v);
         }
         
         // Stores value of full potential on the selected point
         Nb vgrid = C_0;
         ModeDisplacements_one(mt, kvec, Properties, Match, Ncor, GpFr, vgrid, lc_map, arDerivatives, grad_hess, vec_prop_file_stor, vec_prop_dump_file);

         // If the point is correct print it to file and increment the IbarCount by one to get the right offsets
         if (Match)
         {
            WriteDataToFiles(Ncor, grad_hess, Properties, kvec, arDerivatives, vgrid);
            ++IbarCount;
         }
      }
      
      if (mt.Size() != I_0)
      {
         BarInterface << IbarCountSave << " " << IbarCount << std::endl;
      }
   }
   BarInterface.close();

   if (gPesIoLevel > I_10)
   {
      Mout << " DoDispDer_one: Done! " << std::endl;
   }

   if (gTime) 
   {
      time_dodispder.CpuOut(Mout, "\n CPU  time used in the DoDispDer_one: ");
      time_dodispder.WallOut(Mout, " Wall time used in the DoDispDer_one: ");
   }
}

/**
 * Calculate derivative (no grid) or V-bar potentials (grid) 
 **/
void AdgaBarFileHandler::ModeDisplacements_one
   (  const ModeCombi& arModeCombi
   ,  const std::vector<In>& arKvec
   ,  MidasVector& arProperties
   ,  bool& arMatch
   ,  std::vector<Nb>& arNCor
   ,  const GridType& arGpFr
   ,  Nb& vgrid
   ,  std::map<InVector,Nb>& arLinCom
   ,  const Derivatives& arDerivatives
   ,  std::vector<MidasVector>& arGradHess
   ,  const std::vector<PropertyFileStorage>& arVecPropFileStor
   ,  std::vector<PropertyFileStorage>& arVecPropDumpFile
   )
{
   Timer time_ada;

   arMatch = true;

   if (arModeCombi.Size() != arKvec.size())
   {
      MIDASERROR(" Dimension of k vector wrong in ModeDisplacements. I stop ...");
   }

   // If gPesCalcDef.GetmPesGrid() we need to know the address of the MC in order to find the grid information in class GridType.
   // In case of derivative force field Adr simply keep its value of -1.
   In add=arModeCombi.Address();
   string list_of_calcs = "";
   string full_dim_list_of_calcs = "";

   //Name              = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + Name;
   //If the following function returns true we have nothing left to do here...
   if(GenerateCalcName(list_of_calcs, full_dim_list_of_calcs, arKvec, arModeCombi))
   {
      arMatch = false;
      return;
   }
   list_of_calcs = "*" + list_of_calcs + "*";
   //now do the actual displacements ...

   arMatch = true;
   bool calc_todo=true;
   //if Adga pes check now if the dispalcement is needed
   calc_todo=CheckDisplacement(arKvec,arModeCombi,false);
   //if (gPesCalcDef.GetmPesScreenModeCombi() && arModeCombi.IsToBeScreened()) calc_todo=false;
   if (arModeCombi.IsToBeScreened()) calc_todo=false;
   if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap()) calc_todo=false;
   if (!calc_todo)
   {
      arMatch = false;
      return;
   }
   
   // find out which k value belongs to which mode. If just one k element
   // is zero then the bar(V) term is zero   
   bool skip = false;
   map<In,In> mode_k;
   for (In i=I_0;i<arKvec.size();++i)
   {
      mode_k[arModeCombi.Mode(i)] = arKvec[i];
      if (arKvec[i] == I_0) skip = true;
   }

   // find linear combination in terms of lower order potentials on file/in
   // memory. if "**" is found the Search will return (double) zero.
   // what is the current MC vector ?
   vector<In> mc=arModeCombi.MCVec();

   string list_of_calcs_1;
   map<string,Nb> code_coef;

   Timer time_all;
   for(map<InVector,Nb>::iterator ci=arLinCom.begin(); ci!=arLinCom.end(); ++ci)
   {
      list_of_calcs_1 = "";
      InVector mode_vec=ci->first;
      for (In i=I_0;i<mode_vec.size();++i)
      {
         In i_mode=mode_vec[i];
         string s1=std::to_string(i_mode+1);
         string s2;
         // where is this mode (i_mode) in MC ?
         In lsave=-I_1;
         for (In l=I_0;l<mc.size();++l)
            if (mc[l]==mode_vec[i]) lsave=l;

         In den = (arGpFr.GridPoints(add)[lsave])/I_2;  // notice the use of lsave to acces the grid here
         In num = mode_k[i_mode];

         // remember to simpify the expression as much as possible
         s2=PesFuncs::SimplifyFraction(num,den);

         // Read the simplified fraction and use this result instead of 'den' and 'num' from this point onwards.
         In simplified_numerator = midas::util::FromString<In>(s2.substr(0,s2.find("/")));
         // If 'simplified_numerator' is equal to 0, SimplifyFraction does nothing.
         // But then simplified_denominator would just become 'den', which can still be gigantic causing integer overflow.
         // Therefore it is simply set to 0 if the numerator is 0. This requires some of the if-checks in the
         // following to be '<=', but this is already the case, as I guess that if the fraction is simplified to 1, certain
         // thing should be the same as if the fraction reduced to something < 1. -ABJ, Mar 2022.
         In simplified_denominator = simplified_numerator == I_0 ? I_0 : midas::util::FromString<In>(s2.substr(s2.find("/")+1));

         // if "top" <= I_0 use LeftFractions else if "top" > I_0 use RightFractions
         In idx_frac = simplified_numerator <= I_0 ? lsave : lsave + mc.size();

         string frac = arGpFr.Fractions(add)[idx_frac]; // notice the use of idx_frac to acces the Fractioning here

         // Multiply 'frac' and 'simplified_denominator' overwriting 'simplified_denominator'
         PesFuncs::StringMultiplication(frac,simplified_denominator);

         if (!gPesCalcDef.GetmPesShepardInt() || (gPesCalcDef.GetmPesShepardInt() && gPesCalcDef.GetmPesShepardOrd()==I_0))
         {
            if (skip && (abs(simplified_numerator) <= simplified_denominator) ) break;
         }
         if (skip && (abs(simplified_numerator) > simplified_denominator) )
         {
            arMatch = false;
            return;
         }

         if ( abs(simplified_numerator) <= simplified_denominator )
         {
            list_of_calcs_1=list_of_calcs_1 + "#"+s1+"_"+s2;
         }
         else
         {
            list_of_calcs_1="DO_NOT_CALCULATE";
         }
      }
      list_of_calcs_1 = "*" + list_of_calcs_1 + "*";
      string::size_type loc = list_of_calcs_1.find("DO_NOT_CALCULATE",0);
      if( loc != string::npos )
      {
         arMatch = false;
         return;
      }
      else
      {
         code_coef[list_of_calcs_1]=ci->second;
      }
   }

   // Write out linear combination, i.e. V-bar in terms of the V's:
   if (gPesIoLevel > I_11 || gDebug)
   {
      Mout << "bar(" << full_dim_list_of_calcs << ")" << " = " << "   ";
      for (map<string, Nb>::iterator Msn = code_coef.begin(); Msn != code_coef.end(); ++Msn)
      {
         Mout.setf(ios::scientific);
         Mout.setf(ios::uppercase);
         Mout.precision(I_1);
         Mout << (*Msn).second << " " << (*Msn).first << " ";
      }
      Mout << std::endl;
      Mout.precision(I_22);
   }

   MidasVector temp(mPesInfo.GetMultiLevelInfo().GetNoOfProps());
   temp.Zero();

   for (std::map<std::string, Nb>::iterator Msn = code_coef.begin(); Msn != code_coef.end(); ++Msn)
   {
      bool warn=true;
      bool warn1=true;
      bool warn2=true;
      bool test=true;
      bool test_d=true;

      for (In i = I_0; i < mPesInfo.GetMultiLevelInfo().GetNoOfProps(); ++i)
      {

         In prop_calc_num = I_0;
         Nb prop = C_0;
         // V-bar terms which have no V terms connected are set to zero, e.g. for terms connected to the reference point 
         if ((*Msn).first == "**")
         {
            if (  !gPesCalcDef.GetmPesShepardInt() 
               || (  gPesCalcDef.GetmPesShepardInt() 
                  && gPesCalcDef.GetmPesShepardOrd() == I_0
                  )
               )
            {
               prop = C_0;
            }
         }
         else
         {
            // Look in general if a k vec is in the origin and polish the calculation code accordingly
            std::string calc_code = "";
            SetCodeFromString(calc_code, (*Msn).first);

            // if the first property is not found in the appropriate prop_no_X_bar.mpoints
            // there is no need to perform the test for the subsequent properties. 
            if (test)
            {
               test = ExtractPropertyLine(arVecPropFileStor.at(i), calc_code, prop, prop_calc_num);
            }

            if (!test)
            {
               if (warn && (gPesIoLevel > 8)) {
                  Mout << " Warning : I did not find property " << i+1
                       << " for " << (*Msn).first << "  " << calc_code << endl;
               }

               warn = false;
               if (gPesCalcDef.GetmPesCalcPotFromOpFile())
               {
                  // if the first property is not found in the appropriate
                  // prop_no_X_bar.mpoints there is no need to perform the test for the
                  // subsequent properties. 
                  if (test_d)
                  {
                     test_d = ExtractPropertyLine(arVecPropDumpFile.at(i), calc_code, prop, prop_calc_num);
                  }
                  if (test_d && warn1 && (gPesIoLevel > 8))
                  {
                     Mout << " Missing value found in the _dump file " << std::endl;
                  }
                  if (test_d)
                  {
                     warn1 = false;
                  }

                  // If !test_d, i.e. if calc_code/value not found in dump
                  // file, calculate and append it.
                  if (!test_d)
                  {
                     if (warn2 && (gPesIoLevel > 8))
                     {
                        Mout << " Missing points will be obtained from info on the .mop file " << std::endl;
                     }
                     warn2 = false;
                     In prop_no = i + I_1;
                     prop = GetPotValFromOpFile(calc_code, prop_no);
                     AppendPropertyLine(arVecPropDumpFile.at(i), calc_code, prop, I_0);
                  }
               }
               else
               {
                  // Wooooow, what happens if getting to here, i.e. if !test
                  // and !gPesCalcDef.GetmPesCalcPotFromOpFile()?? Then the
                  // value of prop is zero, which could be very wrong!!
                  // -MBH, June 2018
                  std::stringstream ss;
                  ss << "Didn't find calc code " << calc_code
                     << " and gPesCalcDef.GetmPesCalcPotFromOpFile() is false,"
                     << " so prop has not been set accordingly; now prop = " << prop
                     << ". This is probably NOT what you want;"
                     << " consider using #2POTVALSFROMOPFILE (see manual)."
                     ;
                  MIDASERROR(ss.str());
               }
            }
         }
         temp[i]=temp[i]+Nb((*Msn).second)*prop;
         
         // Shepard interpolation only for the energy now..
         if (  mPesInfo.GetmUseDerivatives() 
            && gPesCalcDef.GetmPesShepardInt() 
            && InquireFile(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_files.mpesinfo")
            )
         {
            std::string name_pr = mPesInfo.GetMultiLevelInfo().GetPropertyName(i);
            if (arDerivatives.AvailableDer(name_pr))
            {
               DerivativesHelper(name_pr, prop_calc_num, Msn, mc, i, arDerivatives, arGradHess);
            }
         }

         if (gPesIoLevel > I_14)
         {
            Mout << " Gradient and Hessian data for calc code: " << full_dim_list_of_calcs << std::endl;
            Mout << arGradHess[i] << std::endl;
         }
      }
   }

   // Find out the full potential for plotting
   if (mLocalPlot)
   {
      bool test = ExtractPropertyLine(arVecPropFileStor.at(0), list_of_calcs, vgrid);
      
      if (!test && gPesIoLevel > I_10)
      {
         Mout << " Warning: did not find full potential for " << list_of_calcs << " calculation code " << std::endl;
      }

      if (gPesCalcDef.GetmPesCalcPotFromOpFile())
      {
         Mout << " Missing points will be obtained from info on the .mop file " << endl;
         In prop_no = I_1;
         vgrid = GetPotValFromOpFile(list_of_calcs, prop_no);
      }
   }

   // Construct coordinate for the specific bar(V):
   // Actual it seems like we transform from the fraction logic into the Q-units here and then return the Q-unit values... 
   arNCor.resize(arKvec.size());
   In i = I_0;
   Nb scale_factor;
   for (std::map<In,In>::iterator p = mode_k.begin(); p!= mode_k.end(); ++p)
   {
      In denominator = (arGpFr.GridPoints(add)[i])/I_2;
      Nb lg_point = Nb((*p).second)/Nb(denominator);
      if (lg_point > C_0)
      {
         scale_factor = mPesInfo.GetScalingInfo().GetRScalFact((*p).first);
         arNCor[i] = scale_factor * lg_point;
      }
      else
      {
         scale_factor = mPesInfo.GetScalingInfo().GetLScalFact((*p).first);
         arNCor[i] = scale_factor * lg_point;
      }
      
      if (gPesIoLevel > I_11)
      {
         Mout << " The coordinate: " << arNCor[i] << ", have been scaled by a factor of: " << scale_factor << ", on the original coordinate: " << lg_point << std::endl;
      }
      
      ++i;
   }
   arProperties = temp;
   
   if (gTime)
   {
      time_ada.CpuOut(Mout, "\n CPU  time used in the ModeDisplacements_one: ", true);
      time_ada.WallOut(Mout, " Wall time used in the ModeDisplacements_one: ", true);
   }
}

/**
 * Check the displacement.
 **/
bool AdgaBarFileHandler::CheckDisplacement
   (  const std::vector<In>& arKvec
   ,  const ModeCombi& arModeCombi
   ,  bool arZero
   )
{
   if (  !mIterGrid->Is1dConv() 
      && arKvec.size() > 1
      ) 
   {
      return false;
   }
   In add = arModeCombi.Address();
   return mIterGrid->CheckDisplacement(arKvec, add, arZero);
}

/**
 *
 **/
void AdgaBarFileHandler::DerivativesHelper
   (  const std::string& name_pr
   ,  In aPropCalcNum
   ,  std::map<std::string, Nb>::iterator& Msn
   ,  std::vector<In>& mc
   ,  const In& i
   ,  const Derivatives& arDerivatives
   ,  std::vector<MidasVector>& arGradHess
   )
{
   std::vector<Nb> der_cont;
   std::map<In, In> der_cont_handler;

   arDerivatives.GetDerivatives(name_pr, der_cont, der_cont_handler);

   //
   if (  mPesInfo.GetmUseDerivatives() 
      && gPesCalcDef.GetmPesShepardInt() 
      && gPesCalcDef.GetmPesShepardOrd() > I_0
      )
   {
      MidasVector q_grad_in(mPesInfo.GetMolecule().GetNoOfVibs());
      q_grad_in.Zero();
      In pos=der_cont_handler[aPropCalcNum];
      for (In i_e=I_0; i_e<mPesInfo.GetMolecule().GetNoOfVibs(); ++i_e) q_grad_in[i_e]=der_cont[i_e+pos];
      MidasMatrix q_hess_in(I_0);
      if (arDerivatives.OrderDer(name_pr)>I_1)
      {
         q_hess_in.SetNewSize(mPesInfo.GetMolecule().GetNoOfVibs(),false,true);
         q_hess_in.Zero();
         In j_pos=I_0;
         pos+=mPesInfo.GetMolecule().GetNoOfVibs();
         for (In i_r=I_0; i_r<q_hess_in.Nrows(); ++i_r)
         {
            for (In i_c=I_0; i_c<q_hess_in.Ncols(); ++i_c)
            {
               q_hess_in[i_r][i_c]=der_cont[pos+j_pos];
               ++j_pos;
            }
         }
      }
      //construct derivative informations
      if (gPesIoLevel > I_11)
      {
         Mout << " Retrieving derivative infos on calc no: " << aPropCalcNum << " calc code: " << (*Msn).first << std::endl;
         Mout << q_grad_in << std::endl;
         if (arDerivatives.OrderDer(name_pr) > I_1)
         {
            Mout << q_hess_in << std::endl;
         }
      }
      vector<string> k_vec_s;
      vector<In> mc_vec_s;
      //string calc_code="";
      //mPesInfo.SetCodeFromString(calc_code,(*Msn).first);
      //mPesInfo.GetInfoFromString((*Msn).first,mc_vec_s,k_vec_s);
      GetInfoFromString((*Msn).first,mc_vec_s,k_vec_s);
      //Mout << " For code: " << (*Msn).first << " mode_combi: " << mc_vec_s << endl;
      //Mout << " For code: " << calc_code << " calc. #: " <<  aPropCalcNum << " mc_vec_s: " << mc_vec_s << " k_vec_s: " << k_vec_s << endl;
      //Mout << " q_gradient: " << endl;
      //Mout << q_grad_in << endl;
      //Mout << " q_hessian: " << endl;
      //Mout << q_hess_in << endl;
      //now retrieve the derivatives of Vbar with respect to the pertaining modes
      In i_g_pos=I_0;
      for (In i_g_mode=I_0; i_g_mode<mc.size(); ++i_g_mode)
      {
         vector<In> curr_mc;
         curr_mc.push_back(mc[i_g_mode]);
         //Mout << " Current gradient mc: " << curr_mc << endl;
         vector<In> vec_mode_int;
         vec_mode_int.clear();
         std::set_intersection(curr_mc.begin(),curr_mc.end(),mc_vec_s.begin(),mc_vec_s.end(), std::back_inserter(vec_mode_int));
         //Mout << " Intersection for " << curr_mc << " and " << mc_vec_s << " is: " << vec_mode_int << endl;
         //Mout << " Gradient contribution: " << q_grad_in[mc[i_g_mode]] << endl;
         //Mout << " coef: " << Nb((*Msn).second) << endl;
         if (vec_mode_int.size()!=I_0) arGradHess[i][i_g_pos]+=Nb((*Msn).second)*q_grad_in[mc[i_g_mode]];
         //Mout << " final result: " << arGradHess[i][i_g_pos] << endl;
         ++i_g_pos;
      }

      if (arDerivatives.OrderDer(name_pr)==I_2)
      {
         for (In i_g_mode=I_0; i_g_mode<mc.size(); ++i_g_mode)
         {
            for (In j_g_mode=i_g_mode; j_g_mode<mc.size(); ++j_g_mode)
            {
               vector<In> curr_mc;
               curr_mc.push_back(mc[i_g_mode]);
               if (j_g_mode!=i_g_mode) curr_mc.push_back(mc[j_g_mode]);
               //Mout << " Current hessian mc: " << curr_mc << endl;
               vector<In> vec_mode_int;
               vec_mode_int.clear();
               std::set_intersection(curr_mc.begin(),curr_mc.end(),mc_vec_s.begin(),mc_vec_s.end(), std::back_inserter(vec_mode_int));
               //Mout << " Intersection for " << curr_mc << " and " << mc_vec_s << " is: " << vec_mode_int << endl;
               //Mout << " hessian contribution: " << q_hess_in[mc[i_g_mode]][mc[j_g_mode]] << endl;
               //Mout << " coef: " << Nb((*Msn).second) << endl;
               if (vec_mode_int==curr_mc) arGradHess[i][i_g_pos]+=Nb((*Msn).second)*q_hess_in[mc[i_g_mode]][mc[j_g_mode]];
               //Mout << " final result: " << arGradHess[i][i_g_pos] << endl;
               ++i_g_pos;
            }
         }
      }
   }
}

/**
* Retrieves energy values for a given string calc by using
* info on the .mop files
* */
Nb AdgaBarFileHandler::GetPotValFromOpFile(string aCalcCode,Uin aPropNo)
{
   vector<Nb> grid;
   vector<In> mode_com;
   //vector<In> k_vec;
   //Mout << " string in input to GetEnergyFromString: " << aCalcCode << endl;
   string s_inf=aCalcCode;
   //erase *
   Sst sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   //special case for reference structure
   while (s_inf.size()>0)
   {
      string piece;
      Sst sfind=s_inf.find("#");
      Sst efind=s_inf.find("#",sfind+1);
      //if (sfind==s_inf.npos) 
         //Mout << " end reached " << endl;
      piece=s_inf.substr(sfind, efind-sfind);
      s_inf.erase(sfind,efind);
      //Mout << " piece: " << piece << endl;
      if (piece.find("#")!=piece.npos) //always
      {
         Sst cfind=piece.find("_");
         string mode=piece.substr(piece.find("#"),cfind);
         mode.erase(I_0,I_1);
         string k_val=piece.substr(cfind);
         k_val.erase(I_0,I_1);
         //Mout << " mode: " << mode << " k_val: " << k_val << endl;
         mode_com.push_back(InFromString(mode)-1);
         //k_vec.push_back(InFromString(k_val));
         Nb q_scal_fac = C_1;
         PesFuncs::StringMultiplication(k_val,q_scal_fac);
         if(q_scal_fac > C_0)
            q_scal_fac *= mPesInfo.GetScalingInfo().GetRScalFact(InFromString(mode)-I_1);
         else
            q_scal_fac *= mPesInfo.GetScalingInfo().GetLScalFact(InFromString(mode)-I_1);
         q_scal_fac*=sqrt(C_FAMU);
         //Mout << " q_scal_fac: " << q_scal_fac << endl;
         grid.push_back(q_scal_fac);
      }
   }
   
   //Mout << " infos from string: " << endl;
   //Mout << " mode_vec: " << mode_com << endl;
   //Mout << " grid: " << grid << endl;
   //Mout << " k vec:    " << k_vec << endl;
   Nb pot_value = C_0;
   In n_modes = mode_com.size();
   MidasPotential potential(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + std::to_string(aPropNo) + ".mop", mPesInfo.GetMolecule().GetModeLabels());
   MidasVector Points(grid.size());
   for(In i = I_0; i < grid.size(); ++i)
   {
      Points[i] = grid[i];
   }
   return potential.EvaluatePotential(mode_com, Points);
}

bool AdgaBarFileHandler::GenerateCalcName
   (  string& arCalcName
   ,  string& arFullCalcName
   ,  const std::vector<In>& arKvec
   ,  const ModeCombi& arModeCombi
   )
{
   In add = arModeCombi.Address();
   bool skip = false;
   for (In i=I_0;i<arKvec.size();++i)
   {
      In i_mode = arModeCombi.Mode(i);
      std::string string1 = std::to_string(i_mode + 1);
      std::string string2;
      In denominator = In(pow(C_2, gPesCalcDef.GetmPesIterMax() + In(gPesCalcDef.GetmPesItGridExpScalFact())));
      In numerator = arKvec[i];

      // simplify as much as possible the fraction
      string2 = PesFuncs::SimplifyFraction(numerator,denominator);

      // It might be that we should skip the most distant points on the grid. Therefore "denominator" is updated
      // and if FRAC is differet from one it will be changed
      arFullCalcName = arFullCalcName + "#"+string1+"_"+string2;

      // Do only points that are required 
      if (!gPesCalcDef.GetmPesDoExtrap() || !arModeCombi.IsToBeExtrap() )
      {
         if ( /*(abs(arKvec[i]) <= denominator ) &&*/ (arKvec[i] != I_0) )
         {
            arCalcName = arCalcName + "#"+string1+"_"+string2;
            //arNoOfUpdates += 1;
         }
         else if ( (arKvec.size() == I_1) && (arKvec[i] == I_0) )
         {
            arCalcName = "#0";
            //arNoOfUpdates += 1;
         }
         else if ( (arKvec.size() != I_1) && (InNorm2(arKvec) == I_0) )
         {
            arCalcName = "#0";
         }
         // else do nothing.
      }
      else if (gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap())
      {
         arCalcName = arCalcName + "#"+string1+"_"+string2;
         //arNoOfUpdates += 1;
      }
   }
   
   if ( gPesCalcDef.GetmPesDoExtrap() && arModeCombi.IsToBeExtrap() && skip)
   {
       arCalcName = ""; //skip the whole
   }

   if (arCalcName == "")
   {
      return true;
   }

   return false;
}
