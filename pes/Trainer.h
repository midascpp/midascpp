/**
************************************************************************
* 
* @file                Trainer.h
*
* Created:             01.08.2018 
*
* Author:              Gunnar Schmitz
*
* Short Description:   Class for dealing with training data obtained in
*                      PES constructions
* 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TRAINER_H
#define TRAINER_H

// std headers
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include<utility>
  
// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "geoopt/GeoDatabase.h"
#include "geoopt/CoordType.h"
#include "mlearn/GauPro.h"
#include "mlearn/MLTask.h"

#include "pes/CalcCode.h"
#include "pes/CalculationList.h"

#include "pes/ScalingInfo.h"
#include "pes/PesInfo.h"
#include "pes/ModeCoupling.h"

class ModeCombiOpRange;

class Trainer 
{
//   private:
  private:

   MLTask mMLTask;
   GPMeanFunction<Nb> mMeanFunction;

   const std::string mPesMainDir;
  
   In mMaxIter = 200;
   Nb mEpsGrad = 1e-2;
   Nb mEpsLike = 1e-3;

   bool mUseDensity = true;
   Nb mTolSelect = 1e-4;
   In mMaxModeCombAdd = 6;

   std::string mMLDriver = "";

   std::string mSaveCoVar = "";
   bool mHaveCoVar = false;

   std::string mFileHparam = "";
   bool mHaveHparam = false;

   std::string mDBOut = "";
   bool mHaveDBOut = false;

   bool mDoOpt = true;

   bool mSortSigmaDescending = false;

   bool mUseVariance = true;

   bool mPrintConditionNumber = false;

   In mNumLayer = 1;

   void WriteXYZ(std::ofstream&, const std::vector<std::string>&, const std::vector<Nb>&, const Nb&, const bool&, const MidasVector&, const bool&, const MidasMatrix&);
   std::map<std::string, std::tuple<std::vector<std::string>,std::vector<Nb>>> ReadDisplacements(const std::string&);
   std::map<std::string, Nb> ReadProperty(const std::string&); 
   void  ReadPropertyDeriv(std::map<std::string, MidasVector>&, std::map<std::string, MidasMatrix>&, bool&, const std::string&, const std::string&, const In&); 
   std::map<std::string, Nb> ReadSigmaSquared(const std::string&); 

   bool CheckSigma(const std::string&, const Nb&, const Nb&, const Nb&, const Nb&, const Nb&);
   void RemoveRestart(const std::string&);
   void PrepareRestart(const std::string&, bool);

   GauPro<Nb> CreateGPR(const std::string&, const std::string&, const bool&, const bool&, const Nb&);

  public:

      Trainer(const std::string& = "", const std::string& = "");
      ~Trainer();

      void DumpTrainingData(const In&, const In&, const bool&, const bool&, const In&, const std::string&, const std::string&, bool aMovePropData = false, bool aReuseInitialSet = false);
      void ExtendTrainingData(CalculationList&, PesInfo&, midas::pes::ModeCoupling&, const In&, const bool&, const bool&, In&, const In&, const std::string&, const std::string&);
      void TrainGPR(const In&, const In&, bool, bool, const std::string&, const std::string&, In, Nb, Nb);
      void TrainGPR(const In&, const In&, bool, bool, const std::string&, const std::string&);
      void PrepareForNextRun(const In&, const In&, const std::string&, const std::string&);         
      void SaveTrainingSet(const In&);
      void CheckGPR(const In&, const std::string&, const std::string&);

      In CheckRestart(const std::string&);
      void WriteCheckFile(const std::string&, const In&);
         
};

#endif
