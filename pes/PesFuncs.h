#ifndef PESFUNCS_H_INCLUDED
#define PESFUNCS_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

#include "pes/PesInfo.h"
#include "pes/ModeCoupling.h"

class GridType;
class ModeCombiOpRange;

// Forward declarations.
template<class T> class GeneralMidasVector;
using MidasVector = GeneralMidasVector<Nb>;

namespace PesFuncs
{
   std::string SimplifyFraction(In arTop, In arBut);
   
   void StringMultiplication(const string& arFrac, In& arI);
   void StringMultiplication(const string& arFrac, Nb& arN);
   
   std::string StringMultiplication(const std::string& aStr1, const std::string& aStr2);
   
   std::string StringNominator(const std::string&);
   std::string StringDenominator(const std::string&);
   
   //! Simple polynomial interpolation
   void polint(const MidasVector& xa, const MidasVector& ya, const Nb x, Nb& y, Nb& dy);

   //@{
   //! Bracket the point to be evaluated.
   void hunt(const MidasVector&, const Nb, In& );
   void locate(const MidasVector&, const Nb, In& );
   //@}

   //! Prepare a Grid 
   void GridSetup(const In, ModeCombiOpRange&, In&, GridType&, const PesInfo&);

   void ReadDerInfo
      ( std::map<std::string, MidasVector>&
      , std::map<std::string, MidasMatrix>&
      , std::vector<Nb>&
      , std::map<In, In>&
      , In&
      , const bool
      , const std::string&
      , const std::string&
      , In
      );

} /* namespace PesFuncs */

#endif /* PESFUNCS_H_INCLUDED */
