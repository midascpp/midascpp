/**
************************************************************************
*
* @file                DoInterpolation.h
*
* Created:             05-04-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Driver for interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef DOINTERPOLATION_H_INCLUDED
#define DOINTERPOLATION_H_INCLUDED

// std headers
#include <vector>

// midas headers
#include "input/PesCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "pes/Nss.h"
#include "pes/PesInfo.h"
#include "pes/PesFuncs.h"
#include "pes/GridType.h"
#include "pes/Derivatives.h"
#include "pes/CalculationList.h"
#include "pes/splines/Shepard.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/Spline3D.h"
#include "pes/splines/SplineND.h"
#include "util/Isums.h"

// Forward declarations
class ItGridHandler;

/**
 *
**/
class DoInterpolation
{
   private:
      
      //! PesCalcDef reference 
      const PesCalcDef&       mPesCalcDef;
      
      //! PesInfo reference
      const PesInfo&          mPesInfo;
   
   public:
      
      //! Default constructor.
      DoInterpolation();

      //! Constructor
      DoInterpolation(const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo);

      //!
      void GetInterpolatedPoints(const In& aBar, const ModeCombi& aModeCombi, const std::vector<MidasVector>& aCgCoords, const std::vector<MidasVector>& aFgCoords, std::vector<MidasVector>& aShepardDerivs, const std::vector<MidasVector>& aCgCoordsGhosts, const MidasVector& aCgPropertyGhosts, const std::vector<MidasVector>& aShepardDerivsGhosts, MidasVector& aCgProperty, MidasVector& aFgProperty, MidasVector& aFgSigma, std::vector<MidasVector>& aCgCoordsUncompressed, const In& aCgPoints) const;
      
      //! Gets the information about the derivatives for the lower MCs for the shepard interpolation
      std::tuple<std::vector<MidasVector>, MidasVector, std::vector<MidasVector>> GetLowerMCDerInfo(const ModeCombi&, const Derivatives&, const std::string&, const In&, const GridType&, const ItGridHandler*, const CalculationList&) const;
      
      //!
      void FillXigGhosts(const ModeCombi&, std::vector<std::vector<In> >&, const ItGridHandler*) const;

      //! Output functions for debugging and general output during the calculation...
      void OutputCoreGrid(const std::vector<MidasVector>&, const MidasVector&, const In&, const In&, const std::vector<MidasVector>&) const;
      
      //!
      void GradHessBar(const CalculationList&, const Derivatives&, const GridType&, const std::string&, const In&, const std::string&, MidasVector&, MidasVector&, MidasMatrix&) const;
};

#endif /* DOINTERPOLATION_H_INCLUDED */
