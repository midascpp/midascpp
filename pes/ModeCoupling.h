/**
************************************************************************
*
* @file                ModeCoupling.h
*
* Created:             23-08-2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for estimation of two-mode couplings
*                      from infos on normal coordinates
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MIDAS_PES_MODECOUPLING_H_INCLUDED
#define MIDAS_PES_MODECOUPLING_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "pes/molecule/MoleculeInfo.h"
#include "util/Io.h"

namespace midas
{
namespace pes
{

/**
 * Declarations:
 **/
class ModeCoupling
{
   private:
      //! Screening method
      In mScreeningMethod;
      //! Contains all information about the molecule; normcoord, etc.
      const molecule::MoleculeInfo& mMolecule;        
      //! Nr. of modes
      In mNmodes;                       
      //! Nr. of atoms
      In mNatoms;                       
      //! Vibrational quantum numbers
      std::vector<In> mVibNum;               
      //! The screening
      std::map<InVector, bool> mModeCoupling; 
      
      //! The number of modes
      In Nmodes() {return mNmodes;}                        
      
      //! The number of atoms
      In Natoms() {return mNatoms;}                        
      
      //! Generate the displacement
      void GenQvector(MidasVector& arQvals);               
      
      //! Evaluate the coupling
      void EvalPIC();                                
      
      //! Evaluate the coupling according to Gerber
      void EvalPICGerber();                                      
      
      //! Calculate the displaced geometry
      void MakeDisplacements(In aMode, Nb aQval, MidasVector& arDispVec);   
      
      //! Set the method of screening (only two implemented)
      In ScreeningMethod() {return mScreeningMethod;}  
   public:
      //! Constructor from Molecule.
      ModeCoupling(const molecule::MoleculeInfo& aMolecule);

      //! Set the method of screening (only two implemented)
      void SetMethod(In aMethod);                       
      
      //! Set number of vibrations.
      void SetVibNum(const vector<In>& arVibNum);            
      
      //! Evaluate couplings.
      void EvalCouplings();                        

      //! Is a ModeCombi to be screened ?
      bool ScreenModeCombi(InVector& arModeCombi);          
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_MODECOUPLING_H_INCLUDED */

