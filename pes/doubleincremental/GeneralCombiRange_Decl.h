/**
************************************************************************
* 
* @file                GeneralCombiRange.h
*
* Created:             13-07-2015
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Class for containing information general combi range 
* 
* Last modified:       
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <iostream>

#include"inc_gen/TypeDefs.h"
#include"pes/doubleincremental/GeneralCombi.h"


template <class T>
class GeneralCombiRange
{
   private:
      std::vector<T>               mElementVec;
      std::vector<GeneralCombi<T>> mCombiRange;       ///< Contains the considered combinations .
      In                           mMaxCombiLevel;    ///< Higest Level of CombiCombis 
      In                           mMinCombiLevel;    ///< Higest Level of CombiCombis 
      void AddCombi(const vector<T> arCombi);
      void ReInitFromMax();
      void RecursAdd(const vector<T>& arTmp,In aLevel,In aBegin,const In& aMaxLevel);
 
   public:
      GeneralCombiRange(const std::vector<T>& arElementVec, const In& arMax, const In& arMin=I_0):
             mElementVec(arElementVec), mMaxCombiLevel(arMax) , mMinCombiLevel(arMin) 
             {ReInitFromMax();} ///< 
      GeneralCombiRange(const std::set<T>& arElementSet, const In& arMax,const In& arMin=I_0):
             mMaxCombiLevel(arMax) , mMinCombiLevel(arMin) 
             {mElementVec.assign(arElementSet.begin(),arElementSet.end()); ReInitFromMax();}

      In NoOfCombis() const {return mCombiRange.size();}
      In GetMaxCombiLevel() const {return mMaxCombiLevel;}
      std::vector<T> CombiVec (const In& arCombiNr) const {return mCombiRange.at(arCombiNr).CombiVec();}
      std::set<T> CombiSet (const In& arCombiNr) const {return mCombiRange.at(arCombiNr).CombiSet();}
      GeneralCombi<T> Combi (const In& arCombiNr) const {return mCombiRange.at(arCombiNr);}

      typename std::vector<GeneralCombi<T>>::const_iterator Begin() {return mCombiRange.begin();}
      typename std::vector<GeneralCombi<T>>::const_iterator End() {return mCombiRange.end();}

      void Print();
};
