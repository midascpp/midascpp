/**
************************************************************************
* 
* @file                GeneralCombi.h
*
* Created:             14-07-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Templated class for containing combis of anything 
* 
* Last modified:       27-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERALCOMBI_H_INCLUDED
#define GENERALCOMBI_H_INCLUDED


#include "pes/doubleincremental/GeneralCombi_Decl.h"
#include "pes/doubleincremental/GeneralCombi_Impl.h"


#endif //GENERALCOMBI_H_INCLUDED
