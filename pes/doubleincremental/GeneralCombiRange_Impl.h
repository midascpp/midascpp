/**
************************************************************************
* 
* @file                GeneralCombiRange_Impl.h
*
* Created:             13-07-2015
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Implement General combin range class members 
* 
* Last modified:                 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:

#include <algorithm>

// My headers:

/**
* Initializes from a maximal combi level in 
* */
template<class T>
void GeneralCombiRange<T>::ReInitFromMax()
{

   In level     = 0;

   mCombiRange.clear();
   // Do the work
   {
      vector<T>   tmp;
      //mModeCombis.insert(ModeCombi(tmp,-I_1));
      In begin     = 0;
      level        = I_1;
      if (mMaxCombiLevel>=level) 
      {
         RecursAdd(tmp,level,begin,mMaxCombiLevel);
      }
   }
   std::sort(mCombiRange.begin(),mCombiRange.end());
}
/**
* Add to the Combi vector recursively
* */
template<class T>
void GeneralCombiRange<T>::RecursAdd(const vector<T>& arTmp,In aLevel,In aBegin,const In& aMaxLevel) 
{
   vector<T> tmp(aLevel);
   for(In i=0;i<aLevel-1;i++) tmp[i] = arTmp[i];

   for (In mi = aBegin; mi < mElementVec.size() ; mi++)
   {
      tmp[aLevel-1] = mElementVec.at(mi);
      if (aLevel>=mMinCombiLevel)
         AddCombi(tmp);
      if (aMaxLevel > aLevel) 
         RecursAdd(tmp,aLevel+1,mi+1,aMaxLevel);
   }
}

/**
* Checks whether Combi is present and if not add it to mCombis
* */

template<class T>
void GeneralCombiRange<T>::AddCombi(const vector<T> arCombi)
{
   vector<T> vec = arCombi;
   sort(vec.begin(),vec.end());
   GeneralCombi<T> local(vec);
   if (std::find(mCombiRange.begin(),mCombiRange.end(),local)==mCombiRange.end()) 
   {
      mCombiRange.push_back(local);
   }
   else 
      Mout << "WARNING: group combination occured more than once"<< endl;
}


template<class T>
void  GeneralCombiRange<T>::Print() 
{
   for (In i=I_0; i < mCombiRange.size(); ++i)
   {
      Mout <<  mCombiRange.at(i) << std::endl;
   }   
}
