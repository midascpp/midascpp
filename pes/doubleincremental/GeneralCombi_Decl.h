/**
************************************************************************
* 
* @file                GeneralCombi.h
*
* Created:             14-07-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Templated class for containing combis of anything 
* 
* Last modified:       27-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


#include<iostream>
#include<algorithm>
#include<vector>
#include<set>


#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"

template <class T>
class GeneralCombi;
template <class T>
std::ostream& operator<<(std::ostream& os, const GeneralCombi<T>& ar1);
template <class T>
bool operator== (const GeneralCombi<T>& ar1,const GeneralCombi<T>& ar2);
template <class T>
class GeneralCombi
{
   private:
      std::vector<T> mEntries;             ///< Contains the mode numbers.

   public:
      
      GeneralCombi(const In& aN) {mEntries.reserve(aN);}
      ///< Constructor - reserve space only.
      
      GeneralCombi(const std::vector<T>& arV1):mEntries(arV1){;}  ///< Constructor from vector of integers.
      GeneralCombi(const std::set<T>& arS1) {mEntries.assign(arS1.begin(),arS1.end());}     ///< Constructor from set of int integers.

      // gets stuff
      const std::vector<T>& CombiVec() const {return mEntries;}   ///< returns the vector.
      const std::set<T> CombiSet() const {std::set<T> combi_set; combi_set.insert(mEntries.begin(),mEntries.end()); return combi_set;}   ///< returns the mode set.

      // Analysis
      bool Contains(const GeneralCombi<T>& arCombi) const;
      In Size() const {return mEntries.size();}            ///< Number of entries.
      std::string ToString() {std::string combi_string;
            for (In i=I_0; i < mEntries.size(); ++i)
            {
               combi_string.insert(combi_string.size(),std::to_string(mEntries.at(i)));
               combi_string.insert(combi_string.size()," ");
            }
            return combi_string;
        }
      friend bool operator<(const GeneralCombi<T>& ar1,const GeneralCombi<T>& ar2)
      {
         In s1 = ar1.Size();
         In s2 = ar2.Size();
         if (s1 != s2) 
         {
            return (s1 < s2);
         }
         else 
         {
            for (In i=I_0;i<s1;i++)
               if (ar1.mEntries.at(i) != ar2.mEntries.at(i)) return (ar1.mEntries.at(i) < ar2.mEntries.at(i));
            return false;
         }
      }
      friend bool operator==(const GeneralCombi<T>& ar1,const GeneralCombi<T>& ar2)
            {return (ar1.mEntries==ar2.mEntries);}
      friend bool operator!=(const GeneralCombi<T>& ar1,const GeneralCombi<T>& ar2)
            {return !(ar1==ar2);}
      friend std::ostream& operator<< (std::ostream& os, const GeneralCombi<T>& ar1)
      {
         std::for_each(ar1.mEntries.begin(),ar1.mEntries.end(),[&os](const T& e) {os << e << "," ;});
         return os;
      }
};

