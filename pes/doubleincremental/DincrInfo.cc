/**
************************************************************************
*
* @file                DincrInfo.cc
*
* Created:             08-03-2019
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk), original implementation
*                      Emil Lund Klinting (klint@chem.au.dk), reintegration into the pes module
*
* Short Description:   Driver for double incremental surface generation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// midas headers
#include "pes/doubleincremental/DincrInfo.h"

/**
 * Constructor
 *
 * @param arPesCalcDef
 * @param arPesMainDir
**/
DincrInfo::DincrInfo
   (  const PesCalcDef& arPesCalcDef
   ,  const std::string& arPesMainDir
   )
   :  mPesCalcDef(arPesCalcDef)
   ,  mPesMainDir(arPesMainDir)
   ,  mFcl(I_0)
{

}

/**
 * Setup information on fragment combinations
**/
void DincrInfo::InitializeFragmentCombinations
   (  std::vector<std::string>& arFcNames
   )
{
   // Path to the fragment combination molecule files are situated
   auto fragment_dir = mPesMainDir + "/FC_savedir";
  
   // Check that fragment combination directory exists
   if (!midas::filesystem::Exists(fragment_dir))
   {
      MIDASERROR("Cannot detect the directory containing fragment combination information: " + fragment_dir);
   }

   // Read file with information on fragment combinations
   auto fragment_file_name = fragment_dir + "/" + mPesCalcDef.GetmFcInfo();
   std::ifstream ifs(fragment_file_name, std::ios_base::in); 

   // Check that file exists
   if (!ifs)
   {
      MIDASERROR("Could not open the file indicated under the #2 DOUBLEINCREMENTAL keyword, which contain information on fragment combinations: " + fragment_file_name);
   }
  
   // Start reading the fragment combination information file
   auto subsystem_nr = I_0;
   auto fcl = I_0;
   std::string s;
   while (std::getline(ifs, s))
   {
      // Check for comment line
      std::size_t found_comment = s.find("#");
      if (found_comment != std::string::npos)
      {
         continue;
      }

      std::string fc_name;
      std::string fc_subsystems;
      In fc_inter_modes;
      In fc_aux_modes;

      std::istringstream iss(s);
      // If DIF is chosen, then we only need 3 columns of input
      if (mPesCalcDef.GetmDincrMethod() == "DIF")
      {
         iss >> fc_name >> fc_subsystems >> fc_inter_modes;
      }
      // If DIFACT is chosen, then we need 4 columns of input
      else
      {
         iss >> fc_name >> fc_subsystems >> fc_inter_modes >> fc_aux_modes;
      }
 
      auto fc_vec = midas::util::VectorFromString<In>(fc_subsystems, ',');
      if (fc_vec.size() > fcl)
      {
         fcl = fc_vec.size(); 
      }
      std::set<GlobalSubSysNr> fc;
      fc.insert(fc_vec.begin(), fc_vec.end());

      MidasAssert(fc_vec.size() == fc.size(), "Found double entries for a fragment combination in the " + fragment_file_name);
      GeneralCombi<GlobalSubSysNr> fragment_combi(fc);
      
      // If DIFACT is chosen, then do an extra check on the auxiliary modes
      auto found_auxiliary_coords = false;
      if (mPesCalcDef.GetmDincrMethod() == "DIFACT")
      {
         if (fc_aux_modes < fc_inter_modes)
         {
            found_auxiliary_coords = true;
         }
         else
         {
            MIDASERROR("Double incremental method is DIFACT but " + fragment_file_name + " contains no auxiliary coordinates");
         }
      }

      if (gPesIoLevel > I_4)
      {
         Mout << " Fragment combination " << fc_name << " contain couplings between subsystems " << fc_subsystems << std::endl;   
      }
     
      // Store fragment combination
      auto fc_map_entry = mFragmentCombiMap.emplace
         (  fragment_combi
         ,  std::make_pair
            (  fc_name
            ,  found_auxiliary_coords
            )
         );
      MidasAssert(fc_map_entry.second, "Adding a second fragment combination of the same name");
      
      // Make a pair which maps the fragment combination label/name with the subsystem directory
      mFcDirInfo.push_back(std::make_pair(fc_name, subsystem_nr));

      // Store the fragment combination name
      arFcNames.emplace_back(fc_name); 

      // Increment the subsystem number
      ++subsystem_nr;
   }

   // Set the maximum fragment combination level
   mFcl = fcl;
}

/**
 * Initializes an object which is used for identifying inter-connecting modes 
**/
void DincrInfo::InitializeMcSubsystemConv
   (  const std::vector<In>& arSubsystemMcs
   )
{
   const auto& no_subsystems = arSubsystemMcs.size(); 
   mMcSubsystemConv.reserve(no_subsystems);

   // Start out by assuming that no inter-connecting modes are present
   for (In i = I_0; i < no_subsystems; ++i)
   {  
      std::vector<bool> init_vec_bools(arSubsystemMcs[i], false);
      mMcSubsystemConv.emplace_back(init_vec_bools);
   }
}

/**
 *
**/
void DincrInfo::ConstructRotationMatrix
   (  MidasMatrix& arRotationMatrix
   ,  const molecule::MoleculeInfo& arInterMolInfo
   ,  const molecule::MoleculeInfo& arRelMolInfo
   ,  const In& arInterNoVibs
   ,  const In& arRelNoVibs
   )  const
{
   // Check that dimensions are the same
   MidasAssert(arInterMolInfo.GetNumberOfNuclei() == arRelMolInfo.GetNumberOfNuclei()," Number of nuclei not the same in rotation matrix"); 
   MidasAssert(arInterMolInfo.GetCoord() == arRelMolInfo.GetCoord()," Nuclei not the same for rotation matrix "); 

   // Check orthogonality
   for (In i = I_0; i < arRelNoVibs; ++i)
   {
      MidasVector rel_mode_i(I_3 * arRelMolInfo.GetNumberOfNuclei()); 
      arRelMolInfo.GetNormalModeMw(rel_mode_i, i);

      for (In j = i; j < arRelNoVibs; ++j)
      {
         MidasVector rel_mode_j(I_3 * arRelMolInfo.GetNumberOfNuclei()); 
         arRelMolInfo.GetNormalModeMw(rel_mode_j, j);
         
         Nb check = Dot(rel_mode_i, rel_mode_j);
         if (i == j)
         {
            check -= C_1;
         }

         MidasAssert(std::fabs(check) < C_I_10_12, "Vibrational modes are not orthonomal in rotation matrix"); 
      }
   }

   // Construct the matrix
   for (In i = I_0; i < arInterNoVibs; ++i)
   {
      MidasVector inter_mode(I_3 * arInterMolInfo.GetNumberOfNuclei()); 
      arInterMolInfo.GetNormalModeMw(inter_mode, i);
      
      for (In j = I_0; j < arRelNoVibs; ++j)
      {
         MidasVector rel_mode(I_3 * arRelMolInfo.GetNumberOfNuclei()); 
         arRelMolInfo.GetNormalModeMw(rel_mode, j);
         
         arRotationMatrix[i][j] = Dot(inter_mode, rel_mode);
      }
   }
}

/**
 *
**/
void DincrInfo::AdaptAuxBoundaries
   (  const In& arFcNumber
   ,  const molecule::MoleculeInfo& arAuxCappedMolecule
   ,  PesInfo& arAuxCappedPesInfo
   )  const
{
   // Construct transformaton matrix between inter-connecting and auxiliary modes
   auto rot_mat = mInterRelRotMatVec[arFcNumber];
 
   // Construct scale factors for inter-connecting modes 
   auto inter_r_vector = mInterScalingInfoVec[arFcNumber].GetRScalFactVector();
   auto inter_l_vector = mInterScalingInfoVec[arFcNumber].GetLScalFactVector();
   
   //
   In j_max = std::min(gPesCalcDef.GetmPesNumMCR(), mInterNoVibsVec[arFcNumber]);
   auto rel_molecule_mode_names = mRelModeNamesVec[arFcNumber];
   
   // Loop over all auxilieary coordinates and scale accordingly
   for (In i = I_0; i < mRelNoVibsVec[arFcNumber]; ++i)
   {
      std::multiset<Nb> l_set;
      std::multiset<Nb> r_set;
      for (In j = I_0; j < mInterNoVibsVec[arFcNumber]; ++j)
      {
         l_set.emplace(std::fabs(rot_mat[j][i]) * inter_l_vector[j]);
         r_set.emplace(std::fabs(rot_mat[j][i]) * inter_r_vector[j]);
      }
   
      auto l_scal_fac = C_0;
      auto r_scal_fac = C_0;
      auto it_l = l_set.rbegin();
      auto it_r = r_set.rbegin();
      for (In j = I_0; j < j_max; ++j)
      {
         l_scal_fac += *(it_l++); 
         r_scal_fac += *(it_r++); 
      }

      //
      auto mode_no = arAuxCappedMolecule.GetModeNo(rel_molecule_mode_names[i]);
      MidasAssert(mode_no >= I_0, "Could not match the modes in AdaptAuxBoundaries"); 
      arAuxCappedPesInfo.GetScalingInfo().SetLScalFact(mode_no, l_scal_fac);
      arAuxCappedPesInfo.GetScalingInfo().SetRScalFact(mode_no, r_scal_fac);
   }
}

/**
 * Transformation of the potentials 
**/
void DincrInfo::TransformPot
   (  const In& arFcNumber
   ,  const In& aTotalNoProps
   ,  const std::vector<std::string>& arAuxCappedModeNames
   )  const
{
   // Construct transformaton matrix between inter-connecting and auxiliary modes
   auto rot_mat = mInterRelRotMatVec[arFcNumber];

   for (In iprop = I_0; iprop < aTotalNoProps; ++iprop)
   {
      // Read the operator file and do some standard setup of the OpDef object
      std::string oper_file = mPesMainDir + "/System/subsystem_" + std::to_string(arFcNumber) + "/FinalSurfaces/savedir/prop_no_" + std::to_string(iprop + I_1) + ".mop";
      OpDef op_def;
      op_def.SetOpFile(oper_file);
      op_def.InitLastOper(oper_file, C_1, -I_1, -I_1, -I_1, false);
      input::InputOperFromFile(op_def, I_0, C_0);   
      op_def.InitOpRange(); 
      op_def.Reorganize(true);
      op_def.CleanOper();

      //
      auto aux_capped_mode_names = arAuxCappedModeNames;
      std::sort(aux_capped_mode_names.begin(), aux_capped_mode_names.end(), modename_less_than());
      OperTransLinCombHandler op_trans(&op_def, aux_capped_mode_names, true);
      op_trans.TransformCurrOper(rot_mat, mRelModeNamesVec[arFcNumber], mInterModeNamesVec[arFcNumber]);
 
      // Get the correct frequencies
      for (In i = I_0; i < mFragNoVibsVec[arFcNumber]; ++i) 
      {
         op_trans.SetFreq(mFragModeNamesVec[arFcNumber].at(i), mFragFreqsVec[arFcNumber].at(i));
      }
     
      // Write out the transformed surface in the standard operator file format
      op_trans.DumpPotentialNew("_transformed");
   }
}

/**
 * Merge surfaces from different multilevel calculations of different subsystems.
 *
**/
void DincrInfo::FinalizeFragmentSurface
   (  const std::vector<std::string>& aSubSystemPaths
   ,  const std::vector<In>& aSubSystemProps
   ,  const std::string& aTargetDirectory
   )  const
{
   if (gPesIoLevel > I_4)
   {
      Mout << " Double Incremental Procedure: Parsing the mop files" << std::endl;
   }
   
   // Check that the number of properties make sense before combining mop files
   for (In iprop = I_1; iprop < aSubSystemProps.size(); ++iprop)
   {
      if (aSubSystemProps[I_0] != aSubSystemProps[iprop])
      {
         MIDASERROR("The same number of properties have not been calculated for all subsystems, please investigate");
      }
   }

   // Loop over all calculated properties
   for (In jprop = I_0; jprop < aSubSystemProps[I_0]; ++jprop)
   {
      // Define an operator file name for the final property file to be written
      MidasOperatorWriter mop_writer(aTargetDirectory + "/savedir/prop_no_" + std::to_string(jprop + I_1) + ".mop");

      // Loop over all fragment combinations and read in the operator files
      std::vector<MidasOperatorReader> fragment_mop_readers;
      for (In isystem = I_0; isystem < aSubSystemPaths.size(); ++isystem)
      {
         // Path to directory where information is taken
         auto source_directory = aSubSystemPaths[isystem] + "/FinalSurfaces";
         
         // Read the operator file 
         std::string oper_path;
         if (gPesCalcDef.GetmDincrMethod() == "DIF")
         {
            oper_path = source_directory + "/savedir/prop_no_" + std::to_string(jprop + I_1) + ".mop";
         }
         else
         {
            oper_path = source_directory + "/savedir/prop_no_" + std::to_string(jprop + I_1) + ".mop_transformed";
         }
         auto oper_ss = midas::mpi::FileToStringStream(oper_path);
         MidasOperatorReader mop_reader(oper_path);
         mop_reader.ReadOperator(oper_ss);
         fragment_mop_readers.push_back(mop_reader);
      }

      // Check for the same subsystem being present in multiple fragment combinations and take measures to avoid overcounting of the same terms
      std::vector<std::pair<In, Nb> > overcounted_fcs;
      for (const auto& pfc : mFragmentCombiMap)
      {
         auto parent_fc_vec = (pfc.first).CombiVec();
         auto parent_fc_size = (pfc.first).Size();

         // Over-counting is only a problem for higher-order fragment combinations
         if (parent_fc_size > I_1)
         {
            // Loop through the fragment combination range and find all fragment combinations which share subsystem contribution to this one
            for (const auto& cfc : mFragmentCombiMap)
            {
               auto child_fc_vec = (cfc.first).CombiVec();
               auto child_fc_size = (cfc.first).Size();
               if (child_fc_size == parent_fc_size)
               {
                  break;
               }

               // Check if the child FC shares subsystems with the parent FC
               std::vector<bool> found_element(child_fc_size, false);
               for (In a = I_0; a < parent_fc_size; ++a)
               {
                  for (In b = I_0; b < child_fc_size; ++b)
                  {
                     if (parent_fc_vec[a] == child_fc_vec[b])
                     {
                        found_element[b] = true;
                     }
                  }
               }
              
               // If all elements of the child FC is found in the parent FC, then we need to subtract the child FC potential from the parent FC potential in order to make the parent FC potential into a bar-potential, (with regards to FC logic)
               if (std::none_of
                     (  found_element.cbegin()
                     ,  found_element.cend()
                     ,  [](bool aBool) 
                        {
                           return !aBool;
                        }
                     )
                  )
               {
                  auto coeff = std::pow(C_M_1, (parent_fc_size - child_fc_size));
                  auto overcounted_fc = (cfc.second).first;
                  auto overcounted_fc_number = -I_1;

                  // Get the correct subsystem (directory) number based on the fragment combination label
                  for (const auto& fc_dir_index : mFcDirInfo)
                  {
                     if (fc_dir_index.first == overcounted_fc)
                     {
                        overcounted_fc_number = fc_dir_index.second;
                     }
                  }

                  // Store identification of the overcounted fragment combinations
                  overcounted_fcs.push_back
                     (  std::make_pair
                        (  overcounted_fc_number
                        ,  coeff
                        )
                     );
               }
            }
         }
         else
         {
            continue;
         }
      }

      // First, prepare all the read in operator files for write out
      mop_writer.FromReadPotential(fragment_mop_readers[I_0], C_1);
      for (In i = I_1; i < fragment_mop_readers.size(); ++i)
      {
         mop_writer.AddPotential(fragment_mop_readers[i], C_1); 
      }

      // Second, subtract the overcounted terms 
      for (In j = I_0; j < overcounted_fcs.size(); ++j)
      {
         auto fc_number = overcounted_fcs[j].first;
         auto coefficient = overcounted_fcs[j].second;
         mop_writer.AddPotential(fragment_mop_readers[fc_number], coefficient);
      }

      // If using the ADGA, then we also need partitions of the combined potential, which only includes the contributions of the individual subsystem mode combination ranges 
      if (mPesCalcDef.GetmPesAdga())
      {
         for (In isystem = I_0; isystem < aSubSystemPaths.size(); ++isystem)
         {
            MidasOperatorWriter fragment_mop_writer(aTargetDirectory + "/savedir/prop_no_" + std::to_string(jprop + I_1) + "_combined_fc" + std::to_string(isystem) + ".mop");
            fragment_mop_writer.AddPotential(fragment_mop_readers[isystem], C_0);
         
            const auto& combined_mop_terms = mop_writer.GetmOperators();
            auto fragment_mop_terms = fragment_mop_readers[isystem].GetOperators();
       
            // Now search through the terms and see if they match...
            for (const auto& combined_mop_term : combined_mop_terms)
            {
               for (const auto& fragment_mop_term : fragment_mop_terms)
               {
                  // Check that the size of the operator term key's constituents are of equal size
                  if (  combined_mop_term.first.first.size() != fragment_mop_term.first.first.size()
                     || combined_mop_term.first.second.size() != fragment_mop_term.first.second.size()
                     )
                  {
                     continue;
                  }
       
                  // Check for the same term being found in the combined .mop file and fragment .mop file
                  if (combined_mop_term.first == fragment_mop_term.first)
                  {
                     // Swap the value of the linear coefficient
                     fragment_mop_writer.AddOperatorTerm(combined_mop_term.second, combined_mop_term.first.second, combined_mop_term.first.first);
                     
                     // Correct term is found, break and check next term
                     break;
                  }
               }
            }
         }
      }
   }

   // Combine the files containing information on the potential boundaries 
   if (gPesCalcDef.GetmDincrSurfaceType() != "GIVEN")   // TODO: Bounds should also be provided when using "GIVEN"
   {
      FinalizeFragmentBounds(aSubSystemPaths, aSubSystemProps, aTargetDirectory);
   }
}

/**
 * Combine the files containing information on the potential boundaries
 *
**/
void DincrInfo::FinalizeFragmentBounds
   (  const std::vector<std::string>& aSubSystemPaths
   ,  const std::vector<In>& aSubSystemProps
   ,  const std::string& aTargetDirectory
   )  const
{
   // First, we need to get one_mode_grids.mbounds information from all subsystems
   std::vector<std::vector<std::tuple<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb> > > pes_grid_bounds; 
   pes_grid_bounds.assign(aSubSystemPaths.size(), std::vector<std::tuple<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb> >());
   for (In isystem = I_0; isystem < aSubSystemPaths.size(); ++isystem)
   {
      // Path to file from which information is read
      auto source_file = aSubSystemPaths[isystem] + "/FinalSurfaces/analysis/one_mode_grids.mbounds";
      auto bounds_ss = midas::mpi::FileToStringStream(source_file);

      std::string s;
      while (std::getline(bounds_ss, s))
      {
         // The first line of the one_mode_grids.mbounds file is a comment line that we need to skip
         std::size_t found_comment = s.find("#");
         if (found_comment != std::string::npos) 
         {
            continue;
         }
         auto string_vec = midas::util::StringVectorFromString(s);

         if (string_vec.size() < I_3)
         {
            MIDASERROR("Too few columns in one_mode_grids.mbounds file");
         }
         else if (string_vec.size() < I_9)
         {
            for (In i = string_vec.size(); i < I_9; ++i)
            {
               string_vec.emplace_back("0.0");
            }
         }
         
         // Get a line with grid bounds information
         auto grid_bounds_line = midas::util::TupleFromStringVec<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb>(string_vec);
         
         // If the grid bound is associated with an auxiliary coordinate, the it should be disregarded 
         if (  gPesCalcDef.GetmDincrMethod() == "DIFACT"
            && (std::get<I_0>(grid_bounds_line).find("AUX") != std::string::npos)
            )
         {
            continue;
         }

         // Store the grid bound information for further processing
         pes_grid_bounds[isystem].push_back(grid_bounds_line);
      }
   }

   // Second, sort the data and remove duplicate entries
   std::list<std::tuple<Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb> > pes_grid_bounds_list;
   for (In jsystem = I_0; jsystem < pes_grid_bounds.size(); ++jsystem)
   {
      for (In iline = I_0; iline < pes_grid_bounds[jsystem].size(); ++iline)
      {
         auto data_line = pes_grid_bounds[jsystem][iline];
         auto new_data_line = std::make_tuple
            (  midas::util::FromString<Nb>(std::get<I_0>(data_line).erase(0, 1))
            ,  std::get<I_1>(data_line) 
            ,  std::get<I_2>(data_line) 
            ,  std::get<I_3>(data_line) 
            ,  std::get<I_4>(data_line) 
            ,  std::get<I_5>(data_line) 
            ,  std::get<I_6>(data_line) 
            ,  std::get<I_7>(data_line) 
            ,  std::get<I_8>(data_line) 
            );
         pes_grid_bounds_list.push_back(new_data_line);
      }
   }
   pes_grid_bounds_list.sort();
   pes_grid_bounds_list.unique();

   // Third, we do the actual merge, check for double entries and then write the file 
   std::ofstream merged_pes_bounds;
   auto merged_pes_bounds_file_name = aTargetDirectory + "/analysis/one_mode_grids.mbounds";
   merged_pes_bounds.open(merged_pes_bounds_file_name.c_str(), ios_base::out);
   merged_pes_bounds << "# Mode l_pes_bound r_pes_bound l_pes_energy r_pes_energy l_gradient r_gradient l_max_energy_point r_max_energy_point " << std::endl;
   merged_pes_bounds.setf(ios::scientific);
   merged_pes_bounds.setf(ios::uppercase);
   merged_pes_bounds.precision(22);
   
   for (auto const& ibounds : pes_grid_bounds_list)
   {
      merged_pes_bounds << "Q" << In(std::get<I_0>(ibounds))
                        << " " << std::get<I_1>(ibounds)
                        << " " << std::get<I_2>(ibounds)
                        << " " << std::get<I_3>(ibounds)
                        << " " << std::get<I_4>(ibounds)
                        << " " << std::get<I_5>(ibounds)
                        << " " << std::get<I_6>(ibounds)
                        << " " << std::get<I_7>(ibounds)
                        << " " << std::get<I_8>(ibounds)
                        << std::endl;
   }
   merged_pes_bounds.close();
}

/**
 *
**/
void DincrInfo::DoHessianCalc
   (  const In& arFcNumber
   ,  molecule::MoleculeInfo* apMolecule
   ,  input::ModSysCalcDef& arModSysCalcDef
   )  const
{
   FreqAna freq_ana(apMolecule, arModSysCalcDef.GetpFreqAnaCalcDef());
   MidasMatrix fc_hessian=freq_ana.CalcHessian();
   fc_hessian.Symmetrize();
   MidasOperatorWriter mop_writer(mPesMainDir + "/FC_savedir/FC_" + std::to_string(arFcNumber) + ".mop");

   std::vector<std::string> functions_diag;
   functions_diag.reserve(I_1);
   functions_diag.emplace_back("Q^2"); 

   std::vector<std::string> functions_off;
   functions_diag.reserve(I_2);
   functions_off.emplace_back("Q^1"); 
   functions_off.emplace_back("Q^1"); 


   mop_writer.AddModeNames(apMolecule->GetModeNames());
   std::vector<std::string> modes;
  

   modes.reserve(I_2);
   for (In i = I_0 ; i < fc_hessian.Nrows(); ++i)
   {
      modes.emplace_back(apMolecule->GetModeLabel(i));         
      mop_writer.AddOperatorTerm(0.5e0*fc_hessian[i][i],functions_diag,modes);
      for (In j = i + 1 ; j < fc_hessian.Ncols(); ++j)
      {
         modes.emplace_back(apMolecule->GetModeLabel(j));
         mop_writer.AddOperatorTerm(fc_hessian[i][j],functions_off,modes);
         modes.pop_back();
      }
      modes.clear();
   }
}

/**
 *
**/
void DincrInfo::AnalyzeHessian
   (
   )  const
{
   molecule::MoleculeInfo super_mol(molecule::MoleculeFileInfo("final_vib.mmol","MIDAS"));
   MidasMatrix super_hess(super_mol.GetNoOfVibs(),super_mol.GetNoOfVibs(),C_0);   
   MidasOperatorReader comp_op(gSaveDir+"/Combined.mop");
  
   if (super_mol.HasSelectVibs())
   {
       MidasWarning("Analyze Hessian does not work with SELECTVIBS, I will skip it.");
       return;
   }

 
   std::map<std::string,Nb,modename_less_than> scalfac_map;
   bool scaling=comp_op.GetScalingMap(scalfac_map);


   Mout << " Scaling " << scaling << endl;

   for (auto it_op=comp_op.GetFrontOfOperators(); it_op!=comp_op.GetEndOfOperators(); ++it_op)
   {
       if (it_op->first.first.size()==I_1 && it_op->first.second.size()==I_1 && 
              it_op->first.second.at(I_0)=="Q^2")
       {
          In k=super_mol.GetModeNo(it_op->first.first.at(I_0));
          MidasAssert(k>=I_0,"mode tag not found");
          super_hess[k][k]=C_2*it_op->second;
          if (scaling)
          {
             super_hess[k][k]*=scalfac_map.at(it_op->first.first.at(I_0))*scalfac_map.at(it_op->first.first.at(I_0));
          }
       }
       else if (it_op->first.first.size()==I_2 && it_op->first.second.size()==I_2 && 
              it_op->first.second.at(I_0)=="Q^1" && it_op->first.second.at(I_1)=="Q^1")
       {
          In k=super_mol.GetModeNo(it_op->first.first.at(I_0));
          In l=super_mol.GetModeNo(it_op->first.first.at(I_1));
          MidasAssert(k>=I_0 && l >=I_0,"mode tag not found");
          Nb value=super_hess[k][l]=it_op->second;
          if (scaling)
          {
             value*=scalfac_map[it_op->first.first.at(I_0)]*scalfac_map[it_op->first.first.at(I_1)];
          }
          super_hess[k][l]=value;
          super_hess[l][k]=value;
       }
   }

   ofstream output("incr_red_hess");
   output.setf(ios::scientific);
   output.precision(I_12);
   for (int i=0 ; i < super_hess.Ncols() ; ++i)
   {
      for (int j=0 ; j < super_hess.Nrows() ; ++j)
      {
         output << " " << super_hess[i][j] << " " ;
      }
      output << endl;
   }

   FreqAna freq_ana(&super_mol);
   freq_ana.DoFreqAna(super_hess,"FromFragmentHessian"); 
}

