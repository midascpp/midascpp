/**
************************************************************************
*
* @file                DincrInfo.cc
*
* Created:             08-03-2019
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk), original implementation
*                      Emil Lund Klinting (klint@chem.au.dk), reintegration into the pes module
*
* Short Description:   Driver for double incremental surface generation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef DINCRSURFACE_H_INCLUDED
#define DINCRSURFACE_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "input/FileConversionInput.h"
#include "input/MidasOperatorWriter.h"
#include "input/MidasOperatorReader.h"
#include "pes/PesInfo.h"
#include "pes/doubleincremental/GeneralCombi.h"
#include "util/conversions/TupleFromString.h"
#include "coordinates/freqana/FreqAna.h"
#include "coordinates/rotcoord/OperTransLinCombHandler.h"

/**
 *
**/
class DincrInfo
{
   private:
      
      //! PesCalcDef reference 
      const PesCalcDef&                         mPesCalcDef;

      //! Path for the surface calculation main dir
      const std::string&                        mPesMainDir;
   
      //! Maximum fragment combination level
      In                                        mFcl;
      
      //! Holds information on which fragment combination is paired to which subsystem directory
      std::vector<std::pair<std::string, In> >  mFcDirInfo;
      
      //! Hold information on individual fragment combinations
      std::map<GeneralCombi<GlobalSubSysNr>, std::pair<std::string, bool> > mFragmentCombiMap;

      //! Keeps track of convergence for mode combinations of specific subsystems
      std::vector<std::vector<bool> >           mMcSubsystemConv;
    
      //@{
      //! Attributes used in the DIFACT scheme 
      std::vector<In>                           mInterNoVibsVec;
      std::vector<In>                           mRelNoVibsVec;
      std::vector<In>                           mFragNoVibsVec;
      std::vector<std::vector<std::string> >    mInterModeNamesVec;
      std::vector<std::vector<std::string> >    mRelModeNamesVec;
      std::vector<std::vector<std::string> >    mFragModeNamesVec;
      std::vector<std::vector<Nb> >             mFragFreqsVec;
      std::vector<ScalingInfo>                  mInterScalingInfoVec;
      std::vector<MidasMatrix>                  mInterRelRotMatVec;
      //@}

   public:
      
      //! Default constructor.
      DincrInfo();

      //! Constructor
      DincrInfo(const PesCalcDef& arPesCalcDef, const std::string& arPesMainDir);
      
      //! Setup information on fragment combinations
      void InitializeFragmentCombinations(std::vector<std::string>& arFcNames);

      //!
      void InitializeMcSubsystemConv(const std::vector<In>& arSubsystemMcs);

      //!
      void ConstructRotationMatrix(MidasMatrix& arRotationMatrix, const molecule::MoleculeInfo& arInterMolInfo, const molecule::MoleculeInfo& arRelMolInfo, const In& arInterNoVibs, const In& arRelNoVibs) const;

      //!
      void AdaptAuxBoundaries(const In& arFcNumber, const molecule::MoleculeInfo& arAuxCappedMolecule, PesInfo& arAuxCappedPesInfo) const;

      //!
      void TransformPot(const In& arFcNumber, const In& aTotalNoProps, const std::vector<std::string>& arAuxCappedModeNames) const;

      //! Merge surfaces from different multilevel calculations of individual subsystems.
      void FinalizeFragmentSurface(const std::vector<std::string>& aSubSystemPaths, const std::vector<In>& aSubSystemProps, const std::string& aTargetDirectory) const;

      //! Combine the files containing information on the potential boundaries
      void FinalizeFragmentBounds(const std::vector<std::string>& aSubSystemPaths, const std::vector<In>& aSubSystemProps, const std::string& aTargetDirectory) const;
      
      //!
      void DoHessianCalc(const In& arFcNumber, molecule::MoleculeInfo* apMolecule, input::ModSysCalcDef& arModSysCalcDef) const;

      //!
      void AnalyzeHessian() const;

      //@{
      //! DincrInfo class setters
      void SetmInterNoVibsVec(const std::vector<In>& aInVec)                              {mInterNoVibsVec = aInVec;}
      void SetmRelNoVibsVec(const std::vector<In>& aInVec)                                {mRelNoVibsVec = aInVec;}
      void SetmFragNoVibsVec(const std::vector<In>& aInVec)                               {mFragNoVibsVec = aInVec;}
      void SetmInterModeNamesVec(const std::vector<std::vector<std::string> >& aStrVec)   {mInterModeNamesVec = aStrVec;}
      void SetmRelModeNamesVec(const std::vector<std::vector<std::string> >& aStrVec)     {mRelModeNamesVec = aStrVec;}
      void SetmFragModeNamesVec(const std::vector<std::vector<std::string> >& aStrVec)    {mFragModeNamesVec = aStrVec;}
      void SetmFragFreqsVec(const std::vector<std::vector<Nb> >& aNbVec)                  {mFragFreqsVec = aNbVec;}
      void SetmInterScalingInfoVec(const std::vector<ScalingInfo>& aScalInfoVec)          {mInterScalingInfoVec = aScalInfoVec;} 
      void SetmInterRelRotMatVec(const std::vector<MidasMatrix>& aMatVec)                 {mInterRelRotMatVec = aMatVec;}
      void SetmMcSubsystemConv(const In& aSubsys, const In& aModes, const bool& aBool)
      {
         mMcSubsystemConv[aSubsys][aModes] = aBool;
      }
      //@}
      
      //@{
      //! DincrInfo class getters
      const In& GetmFcl() const                                                  {return mFcl;}
      const bool GetmMcSubsystemConv(const In& aSubsys, const In& aModes) const  {return mMcSubsystemConv[aSubsys][aModes];}
      //@}
      
};

#endif /* DINCRSURFACE_H_INCLUDED */
