/**
************************************************************************
* 
* @file                GeneralCombiRange.h
*
* Created:             13-07-2015
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Class for containing information general combi range 
* 
* Last modified:       
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERALCOMBIRANGE_H
#define GENERALCOMBIRANGE_H

#include "pes/doubleincremental/GeneralCombiRange_Decl.h"
#include "pes/doubleincremental/GeneralCombiRange_Impl.h"

#endif //GENERALCOMBINRANGE
