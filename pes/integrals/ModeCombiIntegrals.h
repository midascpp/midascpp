/**
 *******************************************************************************
 * 
 * @file    ModeCombiIntegrals.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODECOMBIINTEGRALS_H_INCLUDED
#define MODECOMBIINTEGRALS_H_INCLUDED

#include <vector>

#include "pes/integrals/OneModeIntegrals.h"

namespace midas{
namespace pes{
namespace integrals{

/**
 * @brief
 *    Given a sum-over-product, and set of one-mode-intervals/grids, calculates
 *    all integrals on the grid boxes as sums of 1-mode integrals.
 *
 * All terms of the sum-over-product must have the same number of
 * 1-mode-functions/modes, i.e. it is supposed to contain terms of just one
 * ModeCombi.
 *
 * The template parameter shall be a 1-mode function/functor type, i.e. have an
 * overload of `double operator(double)`.
 *
 * Constructed from a sum-over-product function in the form of
 * SumOverProductIndexer (which again comes from a vector of coefficients and a
 * vector of the products of 1-mode-functions), as well as the subintervals of
 * interest for each mode/variable.
 *
 * Upon construction calculates all relevant 1-mode integrals.  When requested
 * calculates and returns the integral of the given total sum-over-product
 * function on every multi-dimensional sub-interval. The sum-over-product
 * structure is
 * \f[
 *    f(q_1,\dots,q_n) = \sum_t c_t f_{o(1,t)}(q_1) \cdots f_{o(n,t)}(q_n)
 * \f]
 * where \f$ f_{o(m,t)} \f$ is the function for mode \f$ m \f$ of term \f$ t \f$.
 * Utilizes this structure by first calculating relevant 1-mode-integrals
 * (functions identified and stored by SumOverProductIndexer), then calculating
 * total integrals as
 * \f[
 *    I(i_1,\dots, i_n) = \sum_t c_t I^{i_1}_{o(1,t)} \cdots I^{i_n}_{o(n,t)}
 * \f]
 * where \f$ I^{i_m}_{o(m,t)} \f$ is
 * \f[
 *    I^{i_m}_{o(m,t)} = \int_{a_m}^{b_m} f_{o(m,t)}(q_m) d q_m
 *    ,
 * \f]
 * i.e. the integral of \f$ f_{o(m,t)} \f$ on the \f$ i_m \f$'th interval
 * \f$ [a_m ; b_m] \f$.
 * 
 * This avoids evaluation of multi-dimensional integrals, which lowers the
 * scaling of the calculation. Furthermore, the same 1-mode integrals are
 * reused for all choices of subintervals/grid boxes.
 *
 * Integrals are evaluated numerically using a fixed-point Gauss-Legendre rule,
 * the order of which must be given to the constructor.
 *
 * Optionally, can also take scaling factors for each mode, \f$ \alpha_m \f$,
 * replacing all mode \f$ m \f$ functions \f$ f^m(q_m) \f$ by 
 * \f$ f^m(\alpha_m q_m) \f$.
 **/
template<typename F>
class ModeCombiIntegrals
{
   public:
      //! Type alias for the functor type.
      using function_t = F;

      //! Construct from sum-over-product, intervals for each mode, quad. points and scaling factors.
      ModeCombiIntegrals
         (  SumOverProductIndexer<function_t> aSopIndexer
         ,  std::vector<std::vector<std::pair<Nb,Nb>>> aIntervalsPerMode
         ,  Uin aNumQuadPoints
         ,  const std::vector<Nb>& arScalingFactors
         );

      //@{
      //! Copies deleted (shouldn't be necessary, but change if needed), moves are default.
      ModeCombiIntegrals() = delete;
      ModeCombiIntegrals(const ModeCombiIntegrals<F>&) = delete;
      ModeCombiIntegrals<F>& operator=(const ModeCombiIntegrals<F>&) = delete;
      ModeCombiIntegrals(ModeCombiIntegrals<F>&&) = default;
      ModeCombiIntegrals<F>& operator=(ModeCombiIntegrals<F>&&) = default;
      ~ModeCombiIntegrals() = default;
      //@}

      //! The number of modes (must be identical for all terms).
      Uin NumModes() const;

      //! The number of intervals for the given mode.
      Uin NumIntervals(Uin aMode) const;

      //! Calculates and returns the integral for the given subinterval indices.
      Nb Integral(const std::vector<Uin>& arIntervalIndices) const;

   private:
      //! Everything related to the SOP function; 1-mode functions and coefficients.
      SumOverProductIndexer<function_t> mSopIndexer;

      //! Calculates and stores 1-mode integrals on the specified intervals.
      std::vector<OneModeIntegrals<function_t>> mVecOneModeIntegrals;

      //! The coefficient of a given term.
      Nb Coefficient(Uin aTerm) const;

      //! Calculates the product of 1-mode integrals for a term.
      Nb IntegralForTerm(const std::vector<Uin>& arIntervalIndices, Uin aTerm) const;

      //! Prepares/returns calculated 1-mode integrals for all modes.
      std::vector<OneModeIntegrals<function_t>> CalculatedIntegralsForAllModes
         (  const SumOverProductIndexer<function_t>& arSopIndexer
         ,  std::vector<std::vector<std::pair<Nb,Nb>>> aIntervalsPerMode
         ,  Uin aNumQuadPoints
         ,  const std::vector<Nb>& arScalingFactors
         )  const;

      //! Asserts that the indices of the index vector is within range.
      void AssertIntervalIndices(const std::vector<Uin>&) const;
};


} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/


#include "pes/integrals/ModeCombiIntegrals_Impl.h"

#endif/*MODECOMBIINTEGRALS_H_INCLUDED*/
