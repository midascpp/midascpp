/**
 *******************************************************************************
 * 
 * @file    SumOverProductIndexer.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    SumOverProductIndexer and related stuff.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SUMOVERPRODUCTINDEXER_H_INCLUDED
#define SUMOVERPRODUCTINDEXER_H_INCLUDED

#include <functional>
#include <vector>

#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

namespace midas{
namespace pes{
namespace integrals{

/**
 * @brief
 *    Identifies individual functions of sum-over-product and indexes terms
 *    accordingly.
 *
 * Say you have a function with sum-over-product structure;
 * \f[
 *    f(q_1,\dots,q_M)
 *    =  \sum_{t} c_t \prod_{m=1}^M f_{t,m}(q_m)
 *    .
 * \f]
 * Construct this object from the coefficients and the functions for each term.
 * It will then go through all terms to identify which different one-mode
 * functions are actually contained in the sum, then store these separately for
 * each mode, and then provide a multi-index linking each term to its
 * corresponding functions.
 * All terms are assumed to contain the same number of factors in the product.
 *
 * The function (functor) type is given by the template parameter F. Actually
 * it probably doesn't need be one-mode functions (or maybe not even
 * functions), but it's only been tested for that so far.
 * What you _have_ to do, however, is to provide _some_ equality comparison to
 * the constructor.
 **/
template<typename F>
class SumOverProductIndexer
{
   public:
      using function_t = F;
      using equality_t = std::function<bool(const F&, const F&)>;

      //! Construct from coefficients, functions and function equality comparison.
      SumOverProductIndexer
         (  std::vector<Nb> aCoefficients
         ,  const std::vector<std::vector<function_t>>& arFunctionsForEachTerm
         ,  equality_t aFunctionEquality
         );

      //@{
      //! Copies deleted (shouldn't be necessary, but change if needed), moves are default.
      SumOverProductIndexer() = delete;
      SumOverProductIndexer(const SumOverProductIndexer<F>&) = delete;
      SumOverProductIndexer<F>& operator=(const SumOverProductIndexer<F>&) = delete;
      SumOverProductIndexer(SumOverProductIndexer<F>&&) = default;
      SumOverProductIndexer<F>& operator=(SumOverProductIndexer<F>&&) = default;
      ~SumOverProductIndexer() = default;
      //@}

      //! The number of modes (must be identical for all terms).
      Uin NumModes() const;

      //! The number of terms.
      Uin NumTerms() const;

      //! The number of functions for a given mode.
      Uin NumFunctions(Uin aMode) const;

      //! The coefficient for a specific term.
      Nb Coefficient(Uin aTerm) const;

      //! The indices of the functions (as provided by Functions(Uin)) for a given term.
      const std::vector<Uin>& FunctionIndices(Uin aTerm) const;

      //! The (unique) functions for a given mode.
      const std::vector<function_t>& Functions(Uin aMode) const;

   private:
      //! The number of modes (must be the same for all terms).
      Uin mNumModes;

      //! The functor used for equality comparison.
      equality_t mFunctionEquality;

      //! The coefficients of the terms.
      std::vector<Nb> mCoefficients;

      //! The function indices of the terms.
      std::vector<std::vector<Uin>> mFunctionIndices;

      //! The (unique) one-mode functions.
      std::vector<std::vector<function_t>> mFunctions;

      //! Index of given function for given mode. If not contained returns current num. funcs.
      Uin FindFunctionIndex(Uin aMode, const function_t&) const;

      //! Calculates and returns indices of given product of functions.
      std::vector<Uin> AssignIndex(const std::vector<function_t>&);

      //! The (unique) functions for a given mode.
      std::vector<function_t>& Functions(Uin aMode);

      //! Returns number of modes and checks consistency of num. terms/modes.
      Uin AssertSameNumModesAndTerms
         (  const std::vector<Nb>& arCoefs
         ,  const std::vector<std::vector<function_t>>& arFuncs
         )  const;
};

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#include "pes/integrals/SumOverProductIndexer_Impl.h"

#endif/*SUMOVERPRODUCTINDEXER_H_INCLUDED*/
