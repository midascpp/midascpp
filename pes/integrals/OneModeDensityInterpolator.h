/**
 *******************************************************************************
 * 
 * @file    OneModeDensityInterpolator.h
 * @date    20-04-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef ONEMODEDENSITYINTERPOLATOR_H_INCLUDED
#define ONEMODEDENSITYINTERPOLATOR_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

// Forward declarations.
class ItGridDensities;
template<class T> class GeneralMidasVector;
using MidasVector = GeneralMidasVector<Nb>;


namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * Holds (ref to) grid points/values of density for one mode. Functor interface
 * with operator() evaluating density at given point by interpolating from grid
 * points.
 *
 * Can incorporate a scaling factor. This is for use when integrating over
 * products of density and potential functions, since the latter need scaling
 * while the densities don't. Thus the scaling can be counteracted by putting
 * the _reciprocal_ scaling factor into this function.
 *
 * @note
 *    Not implemented with `PesFuncs::hunt`. This could be changed but requires
 *    conversion of `std::vector` to `MidasVector`. Instead uses plain old
 *    binary search - most likely not a performance issue.
 * @note
 *    If deciding to use `PesFuncs::hunt` at some point, need to store the last
 *    found index, probably as a private attribute.(?) This would make
 *    operator() non-const - means that parallization can't use const-refs to
 *    the same OneModeDensityInterpolator object.
 * @note
 *    Assumes grid values are ordered (ascending) and unique.
 ******************************************************************************/
class OneModeDensityInterpolator
{
   public:
      //! Construct from ItGridDensities, property index, mode number, num. interp. points, and scaling fact.
      OneModeDensityInterpolator(const ItGridDensities&, In, In, Uin, Nb = C_1);

      //@{
      //! Copy assignment deleted, other copy/moves default. Change if necessary.
      OneModeDensityInterpolator() = delete;
      OneModeDensityInterpolator& operator=(const OneModeDensityInterpolator&) = delete;

      OneModeDensityInterpolator(const OneModeDensityInterpolator&) = default;
      OneModeDensityInterpolator(OneModeDensityInterpolator&&) = default;
      ~OneModeDensityInterpolator() = default;
      //@}

      //! Interpolated density value at given point.
      Nb operator()(Nb) const;

      //! Two densities are assumed equal if (and only if) for the same mode.
      bool operator==(const OneModeDensityInterpolator&) const;

   private:
      //@{
      //! Grid points and corresponding density values.
      const std::vector<Nb>& mGridValues;
      const std::vector<Nb>& mDensValues;
      //@}

      //! The mode number. Two densities are assumed equal if for the same mode.
      In mMode;

      //! Num. points for interpolation (must be <= number of grid points).
      Uin mNumPointsPolynomialInterpol;

      //! The scaling factor, multiplied onto input value when evaluated.
      Nb mScalingFactor;

      //! Returns index of end grid point for the interval containing value.
      Uin SearchEndGridPointIndex(Nb aGridValue) const;

      //! Beginning index of subrange containing value. Considers num. interpol. points.
      Uin BeginIndexOfSubRange(Nb aGridValue) const;

      //! MidasVector for use with PesFuncs::polint().
      MidasVector ValuesForInterpolation(const std::vector<Nb>&, Uin) const;

      //! Check that values have been loaded, sizes of match, strictly ascending ordering.
      void CheckLoadedValues() const;
};

//! Constructs a vector with density functors for the given ModeCombi.
std::vector<OneModeDensityInterpolator> DensityInterpolatorsForModes
   (  const ItGridDensities& arItGridDensities
   ,  In aPropIdx
   ,  const std::vector<In>& arModeCombi
   ,  Uin aNumPointsPolynomialInterpol
   ,  const std::vector<Nb>& arScalingFactors
   ,  bool aInvertScalingFactors = false
   );

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#endif/*ONEMODEDENSITYINTERPOLATOR_H_INCLUDED*/
