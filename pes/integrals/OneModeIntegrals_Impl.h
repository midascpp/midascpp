/**
 *******************************************************************************
 * 
 * @file    OneModeIntegrals_Impl.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <algorithm>
#include <sstream>

#include "util/Error.h"

namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * @param[in] aNumQuadPoints
 *    Tells object to use an n-point Gauss-Legendre quadrature.
 * @param[in] aFunctions
 *    The functions to integrate.
 * @param[in] aIntervals
 *    The intervals that each function should be integrated over.
 * @param[in] aScalingFactor
 *    Scaling factor, such that we actually integrate f(ax) instead of f(x).
 *******************************************************************************/
template<typename F>
OneModeIntegrals<F>::OneModeIntegrals
   (  Uin aNumQuadPoints
   ,  std::vector<function_t> aFunctions
   ,  std::vector<std::pair<Nb,Nb>> aIntervals
   ,  Nb aScalingFactor
   )
   :  mQuadRule(aNumQuadPoints)
   ,  mFunctions(std::move(aFunctions))
   ,  mIntervals(std::move(aIntervals))
   ,  mIntegrals(NumIntervals(), std::vector<Nb>(NumFunctions(), C_0))
   ,  mScalingFactor(aScalingFactor)
{
   CalculateIntegrals();
}

/***************************************************************************//**
 * @return
 *    The number of intervals/functions held by the object.
 *******************************************************************************/
template<typename F>
Uin OneModeIntegrals<F>::NumIntervals
   (
   )  const
{
   return mIntervals.size();
}
template<typename F>
Uin OneModeIntegrals<F>::NumFunctions
   (
   )  const
{
   return mFunctions.size();
}

/***************************************************************************//**
 * @note
 *    Throws a (hard) MIDASERROR if interval index is out of range.
 *
 * @param[in] aInterval
 *    Index of the interval for which to retrieve beginning/end point.
 * @return
 *    Beginning/end point of interval.
 *******************************************************************************/
template<typename F>
Nb OneModeIntegrals<F>::IntervalBegin
   (  Uin aInterval
   )  const
{
   try
   {
      return mIntervals.at(aInterval).first;
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return C_0;
   }
}
template<typename F>
Nb OneModeIntegrals<F>::IntervalEnd
   (  Uin aInterval
   )  const
{
   try
   {
      return mIntervals.at(aInterval).second;
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return C_0;
   }
}

/***************************************************************************//**
 * Returns value of an (already calculated) integral.
 *
 * @note
 *    Throws a (hard) MIDASERROR if interval/function index is out of range.
 * 
 * @param[in] aInterval
 *    Index of interval. Bookkeeping must be done externally.
 * @param[in] aFunction
 *    Index of function. Bookkeeping must be done externally.
 * @return
 *    Value of integral.
 *******************************************************************************/
template<typename F>
Nb OneModeIntegrals<F>::Integral
   (  Uin aInterval
   ,  Uin aFunction
   )  const
{
   try
   {
      return mIntegrals.at(aInterval).at(aFunction);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return C_0;
   }
}

/***************************************************************************//**
 * Loops over all intervals and functions, calculating each integral, putting
 * results into OneModeIntegrals::mIntegrals.
 *******************************************************************************/
template<typename F>
void OneModeIntegrals<F>::CalculateIntegrals
   (
   )
{
   for(Uin i = I_0; i < NumIntervals(); ++i)
   {
      const auto i_beg = IntervalBegin(i);
      const auto i_end = IntervalEnd(i);
      for(Uin f = I_0; f < NumFunctions(); ++f)
      {
         mIntegrals[i][f] = Quadrature(mFunctions[f], i_beg, i_end, mScalingFactor);
      }
   }
}

/***************************************************************************//**
 * @note
 *    As of now makes a temporary lambda function that incorporates the scaling
 *    factor, and then passes this to the quadrature object. Change at will.
 *
 * @param[in] arFunc
 *    The (raw, i.e. without scaling factor) function to integrate.
 * @param[in] aBegin
 *    Beginning of interval.
 * @param[in] aEnd
 *    End of interval.
 * @param[in] aScalingFactor
 *    Scaling factor, thus integrating f(ax) instead of f(x).
 * @return
 *    Value of integral.
 *******************************************************************************/
template<typename F>
Nb OneModeIntegrals<F>::Quadrature
   (  const function_t& arFunc
   ,  Nb aBegin
   ,  Nb aEnd
   ,  Nb aScalingFactor
   )  const
{
   auto f = [&arFunc, aScalingFactor](Nb x){return arFunc(aScalingFactor*x);};
   return mQuadRule.Evaluate(f, aBegin, aEnd);
}

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

