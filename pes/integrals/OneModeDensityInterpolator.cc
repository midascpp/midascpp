/**
 *******************************************************************************
 * 
 * @file    OneModeDensityInterpolator.cc
 * @date    20-04-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.
#include <algorithm>
#include <sstream>

// Midas headers.
#include "pes/integrals/OneModeDensityInterpolator.h"

#include "mmv/MidasVector.h"
#include "pes/adga/ItGridDensities.h"
#include "pes/PesFuncs.h"


namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if improper values (or none) has been loaded in
 *    arItGridDensities.
 *
 * @param[in] arItGridDensities
 *    This object contains the raw data for interpolation.
 * @param[in] aGridIdx
 *    The index of the property surface of interest
 * @param[in] aMode
 *    The mode of interest; will be used to load the correct values from
 *    arItGridDensities. And for equality checks.
 * @param[in] aNumPointsPolynomialInterpol
 *    The desired number of points for polynomial interpolation; however, if
 *    larger than the size of the grid values vector, will be reduced to that
 *    number.
 * @param[in] aScalingFactor
 *    The scaling factor to be used when evaluating.
 ******************************************************************************/
OneModeDensityInterpolator::OneModeDensityInterpolator
   (  const ItGridDensities& arItGridDensities
   ,  In aGridIdx
   ,  In aMode
   ,  Uin aNumPointsPolynomialInterpol
   ,  Nb aScalingFactor
   )
   :  mGridValues(arItGridDensities.GetGridValues(aGridIdx, aMode))
   ,  mDensValues(arItGridDensities.GetPotValues(aGridIdx, aMode))
   ,  mMode(aMode)
   ,  mNumPointsPolynomialInterpol(std::min(aNumPointsPolynomialInterpol, Uin(mGridValues.size())))
   ,  mScalingFactor(aScalingFactor)
{
   CheckLoadedValues();
}

/***************************************************************************//**
 * @param[in] aInputValue
 *    Argument value for which to evaluate density.
 * @return
 *    Density at input value, interpolated from stored grid points/values.
 ******************************************************************************/
Nb OneModeDensityInterpolator::operator()
   (  Nb aInputValue
   )  const
{
   aInputValue *= mScalingFactor;
   Uin beg_index = BeginIndexOfSubRange(aInputValue);
   Nb result = C_0;
   Nb error_estimate = C_0;
   PesFuncs::polint
      (  ValuesForInterpolation(mGridValues, beg_index)
      ,  ValuesForInterpolation(mDensValues, beg_index)
      ,  aInputValue
      ,  result
      ,  error_estimate
      );
   return result;
}

/***************************************************************************//**
 * @note
 *    Doesn't take different scaling factors into account.
 *
 * @param[in] arOther
 *    The other object, to be compared with this one.
 * @return
 *    Whether the two densities are equal. Since there's only one-mode density
 *    for a mode, _always_ assumed equal if for the same mode, otherwise not.
 ******************************************************************************/
bool OneModeDensityInterpolator::operator==
   (  const OneModeDensityInterpolator& arOther
   )  const
{
   return mMode == arOther.mMode;
}

/***************************************************************************//**
 * Say the grid points in OneModeDensityInterpolator::mGridValues are
 *     index =  0     1     2
 *     value =  -1.0  0.0   1.0.
 * Given an argument, the function will return index for end point of
 * containing subinterval. E.g. given `0.4`, it will return `2`. It is then
 * implied that `0.4` is contained in [`mGridValues[1]`, `mGridValues[2]`).
 * I.e. it returns index of the first value _greater than_ the argument.
 * 
 * @note
 *    Uses `std::upper_bound` which does a log-scaling binary search when used
 *    on random-access iterators, such as
 *    OneModeDensityInterpolator::mGridValues.
 *
 * @param[in] aGridValue
 *    Value to find interval for.
 * @return
 *    Index of the grid point at the _end_ of the interval containing the
 *    value. Returns `0` if smaller than first grid point, mGridValues.size()
 *    if larger than last grid point.
 ******************************************************************************/
Uin OneModeDensityInterpolator::SearchEndGridPointIndex
   (  Nb aGridValue
   )  const
{
   auto upper = std::upper_bound(mGridValues.begin(), mGridValues.end(), aGridValue);
   return std::distance(mGridValues.begin(), upper);
}

/***************************************************************************//**
 * Example of points of subrange with even/odd number of interpolation points.
 * The returend index is the first one marked. `X` is the index returned by
 * SearchEndGridPointIndex().
 * Implemented such that
 *  - even num. interp. points: equal number on both sides of number.
 *  - odd  num. interp. points: one more point on left side.
 * Regular cases:
 *     Middle subrange:
 *                      grid:    0     1     2     3     4     5     6 
 *                     value:                        3.5
 *     with 4 interp. points:                ^     ^     X     ^
 *     with 5 interp. points:          ^     ^     ^     X     ^
 *     Beginning subrange:
 *                     value:      0.5
 *     with 4 interp. points:    ^     X     ^     ^
 *     with 5 interp. points:    ^     X     ^     ^     ^
 *     Ending subrange:
 *                     value:                                    5.5
 *     with 4 interp. points:                      ^     ^     ^     X
 *     with 5 interp. points:                ^     ^     ^     ^     X
 * Out-of-range and spot on values.
 *     Spot-on value:
 *                      grid:    0     1     2     3     4     5     6 
 *                     value:                      3.0   
 *     with 4 interp. points:                ^     ^     X     ^
 *     with 5 interp. points:          ^     ^     ^     X     ^
 *     Larger than last:
 *                      grid:    0     1     2     3     4     5     6 
 *                     value:                                          6.5
 *     with 4 interp. points:                      ^     ^     ^     ^    (X)
 *     with 5 interp. points:                ^     ^     ^     ^     ^    (X)
 *     Smaller than first:
 *                      grid:    0     1     2     3     4     5     6 
 *                     value: -1
 *     with 4 interp. points:    X     ^     ^     ^
 *     with 5 interp. points:    X     ^     ^     ^     ^
 ******************************************************************************/
Uin OneModeDensityInterpolator::BeginIndexOfSubRange
   (  Nb aGridValue
   )  const
{
   Uin end = SearchEndGridPointIndex(aGridValue) + mNumPointsPolynomialInterpol/I_2;
   end = std::min(end, Uin(mGridValues.size()));
   end = std::max(end, mNumPointsPolynomialInterpol);
   return end - mNumPointsPolynomialInterpol;
}

/***************************************************************************//**
 * @param[in] arValues
 *    Vector of all the values, from which we want a subrange in MidasVector
 *    format.
 * @param[in] aBeginIndex
 *    Index of the first element in the desired subrange.
 * @result
 *    MidasVector containing the subrange starting at aBeginIndex and
 *    containing OneModeDensityInterpolator::mNumPointsPolynomialInterpol
 *    elements.
 ******************************************************************************/
MidasVector OneModeDensityInterpolator::ValuesForInterpolation
   (  const std::vector<Nb>& arValues
   ,  Uin aBeginIndex
   )  const
{
   auto beg = arValues.begin() + aBeginIndex;
   return MidasVector(std::vector<Nb>(beg, beg + mNumPointsPolynomialInterpol));
}

/***************************************************************************//**
 * Call when values have been loaded in constructor. A check that sizes match
 * between the grid/density values, that they have non-zero size (i.e.
 * _something_ has actually been loaded), and that grid values are in strictly
 * increasing ordering.
 *
 * @note
 *    Throws MIDASERROR if any of these checks fail.
 ******************************************************************************/
void OneModeDensityInterpolator::CheckLoadedValues
   (
   )  const
{
   if (mGridValues.size() != mDensValues.size())
   {
      std::stringstream err;
      err   << "mGridValues.size() (which is " << mGridValues.size()
            << ") not equal to mDensValues.size() (which is " << mDensValues.size()
            << ")."
            ;
      MIDASERROR(err.str());
   }

   if (mGridValues.empty())
   {
      MIDASERROR("It seems that the ItGridDensities had not loaded any files yet.");
   }

   // Check for strictly ascending ordering.
   if (  !  std::is_sorted
               (  mGridValues.begin()
               ,  mGridValues.end()
               ,  [](Nb current, Nb previous)->bool {return current <= previous;}
               )
      )
   {
      MIDASERROR("Grid values are not in strictly ascending ordering.");
   }
}




/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if:
 *     - arModeCombi and arScalingFactors have different sizes.
 *     - one of arScalingFactors is zero with aInvertScalingFactors being
 *       requested.
 *
 * @param[in] arItGridDensities
 *    The object containg the grid points/values for interpolation.
 * @param[in] aPropIdx
 *    The index of the relevant property
 * @param[in] arModeCombi
 *    Vector of the modes of interest.
 * @param[in] aNumPointsPolynomialInterpol
 *    Number of points to be used for polynomial interpolation.
 * @param[in] arScalingFactors
 *    The scaling factors to be used, one for each mode.
 * @param[in] aInvertScalingFactors
 *    Whether to actually use scaling factors that are the inverses of those
 *    given.
 * @return
 *    OneModeDensityInterpolator%s, one for each of the given modes.
 ******************************************************************************/
std::vector<OneModeDensityInterpolator> DensityInterpolatorsForModes
   (  const ItGridDensities& arItGridDensities
   ,  In aPropIdx
   ,  const std::vector<In>& arModeCombi
   ,  Uin aNumPointsPolynomialInterpol
   ,  const std::vector<Nb>& arScalingFactors
   ,  bool aInvertScalingFactors
   )
{
   // Check sizes.
   if (arModeCombi.size() != arScalingFactors.size())
   {
      MIDASERROR( "Size mismatch: arModeCombi.size() = "+std::to_string(arModeCombi.size())+
                  ", arScalingFactors.size() = "+std::to_string(arScalingFactors.size())+
                  ".");
   }

   std::vector<OneModeDensityInterpolator> v;
   v.reserve(arModeCombi.size());
   for(Uin m = I_0; m < arModeCombi.size(); ++m)
   {
      Nb scale = arScalingFactors[m];
      if (aInvertScalingFactors)
      {
         if (scale == C_0)
         {
            MIDASERROR("Can't divide by zero scaling factor.");
         }
         scale = C_1/scale;
      }

      v.emplace_back 
         (  arItGridDensities
         ,  aPropIdx
         ,  arModeCombi[m]
         ,  aNumPointsPolynomialInterpol
         ,  scale
         );
   }
   return v;
}


} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/
