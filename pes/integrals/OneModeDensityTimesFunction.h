/**
 *******************************************************************************
 * 
 * @file    OneModeDensityTimesFunction.h
 * @date    26-04-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef ONEMODEDENSITYPOTENTIAL_H_INCLUDED
#define ONEMODEDENSITYPOTENTIAL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"

// Forward declarations.
#include "pes/integrals/MidasPesIntegrals_Fwd.h"
template<class T> class MidasFunctionWrapper;


namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * @brief
 *    Combines a one-mode density with a one-mode function.
 *
 * Evaluation using operator()(Nb) and comparison are then based on this
 * underlying density/function pair.
 *
 * @note
 *    As currently implemented, with the OneModeDensityInterpolator and
 *    MidasFunctionWrapper, be aware that the latter often needs a scaling
 *    factor, while the former often doesn't. An inverse scaling factor to
 *    counteract this has been incorporated in OneModeDensityInterpolator.
 * @note
 *    Uses const-refs. Be sure not to leave any references to dangle!
 ******************************************************************************/
class OneModeDensityTimesFunction
{
   public:
      using dens_t = OneModeDensityInterpolator;
      using func_t = MidasFunctionWrapper<Nb>;

      //! Construct from density and function.
      OneModeDensityTimesFunction(const dens_t&, const func_t&);

      //@{
      //! Copy assignment deleted, other copy/moves default. Change if necessary.
      OneModeDensityTimesFunction() = delete;
      OneModeDensityTimesFunction& operator=(const OneModeDensityTimesFunction&) = delete;

      OneModeDensityTimesFunction(const OneModeDensityTimesFunction&) = default;
      OneModeDensityTimesFunction(OneModeDensityTimesFunction&&) = default;
      ~OneModeDensityTimesFunction() = default;
      //@}

      //! Return density(Nb)*function(Nb).
      Nb operator()(Nb) const;

      //! Equal iff both densities and functions are equal.
      bool operator==(const OneModeDensityTimesFunction&) const;

   private:
      //! The density function factor.
      const dens_t& mDens;

      //! The (potential) function factor.
      const func_t& mFunc;
};

//! "Zip"-combines densities and functions, one for each mode.
std::vector<OneModeDensityTimesFunction> CombineDensitiesAndFunctionOneTerm
   (  const std::vector<OneModeDensityTimesFunction::dens_t>&
   ,  const std::vector<OneModeDensityTimesFunction::func_t>&
   );

//! "Zip"-combines densities (same for all terms) and terms with different functions.
std::vector<std::vector<OneModeDensityTimesFunction>> CombineDensitiesAndFunctions
   (  const std::vector<OneModeDensityTimesFunction::dens_t>&
   ,  const std::vector<std::vector<OneModeDensityTimesFunction::func_t>>&
   );

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#endif/*ONEMODEDENSITYPOTENTIAL_H_INCLUDED*/
