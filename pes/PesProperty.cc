/**
************************************************************************
* 
* @file                PesProperty.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   PesProperty datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/PesProperty.h"

// std headers
#include <string>
#include <vector>
#include <exception>
#include <iostream>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"

#include "mpi/Impi.h"

namespace midas
{
namespace pes
{

/**
 * Constructor from PropertyInfoEntry.
 *
 * @param aPropertyInfoEntry   The property info to construct from.
 **/
PesProperty::PesProperty
   (  const PropertyInfoEntry& aPropertyInfoEntry
   ) 
   :  mOrder     (aPropertyInfoEntry.GetOrder())
   ,  mRotGroup  (aPropertyInfoEntry.GetRotGroup())
   ,  mElement   (aPropertyInfoEntry.GetElement())
   ,  mDescriptor(aPropertyInfoEntry.GetDescriptor())
   ,  mDerFile   (aPropertyInfoEntry.GetDerFile())
   ,  mValue(C_0)
   ,  mFirstDer(I_0)
   ,  mSecondDer(I_0)
{
}

/**
 * Copy assignment. This is needed to correctly copy mFirstDer and mSecondDer.
 *
 * @param aOtherInfo   The info to copy.
 *
 * @return   Return reference to this, so operator= can be chained.
 **/
PesProperty& PesProperty::operator=
   (  const PesProperty& aOtherInfo
   )
{
   if(this != &aOtherInfo)
   {
      mOrder = aOtherInfo.mOrder;
      mRotGroup = aOtherInfo.mRotGroup;
      mElement = aOtherInfo.mElement;
      mDescriptor = aOtherInfo.mDescriptor;
      mDerFile = aOtherInfo.mDerFile;
      mValue = aOtherInfo.mValue;
      mFirstDer.SetNewSize(aOtherInfo.mFirstDer.Size());
      mFirstDer = aOtherInfo.mFirstDer;
      mSecondDer.SetNewSize(aOtherInfo.mSecondDer.Nrows(), aOtherInfo.mSecondDer.Ncols());
      mSecondDer = aOtherInfo.mSecondDer;
   }

   return *this;
}

/**
 * Correct the nuclear ordering of derivatives for the property.
 * This corrects for the fact that the electronic structure program might have 
 * switched the ordering of the nuclei in its output compared to what Midas uses.
 * 
 * @param aNucMap    The map relating ES and Midas ordering.
 **/
void PesProperty::CorrectNucOrder
   (  const std::vector<In>& aNucMap
   )
{
   // Correct the gradient.
   // ----------------------------------------------------------------------
   if(mFirstDer.Size() > I_0)
   {
      In n_dim = mFirstDer.Size();
      In n_atom = n_dim / I_3;
      
      MidasVector grad_reord(n_dim, C_0);
      for (In i_at = I_0; i_at < n_atom; i_at++)
      {
         In i_orig = aNucMap[i_at];
         In ind1x = I_3 * i_at; 
         In indx = I_3 * i_orig; 

         for (In i1=I_0; i1<I_3; i1++) 
         {
            grad_reord[ind1x + i1] = mFirstDer[indx + i1];
         }
      }
      
      // Save the reordered gradient
      mFirstDer = grad_reord;
   }
   
   // Correct the Hessian
   // ----------------------------------------------------------------------
   if( (mSecondDer.Ncols() > I_0) && (mSecondDer.Nrows() > I_0) )
   {
      In n_dim = mSecondDer.Ncols();
      In n_atom = n_dim / I_3;

      MidasMatrix hess_reord(n_dim, C_0);
      for (In i = I_0; i < n_atom; i++)
      {
         In i_orig = aNucMap[i];
         In indx  = I_3 * i_orig;
         In ind1x = I_3 * i; 
         for (In j = I_0; j < n_atom; j++)
         {
            In j_orig = aNucMap[j];
            In indy  = I_3 * j_orig; 
            In ind1y = I_3 * j;      
            for (In i1 = I_0; i1 < I_3; i1++)
            {
               for (In j1 = I_0; j1 < I_3; j1++)
               {
                  hess_reord[ind1x + i1][ind1y + j1] = mSecondDer[indx+i1][indy+j1];
               }
            }
         }
      }
      
      // Save the reordered hessian
      mSecondDer = hess_reord;
   }
}

/**
 * Does the property contain derivatives?
 *
 * @return    Returns true if either first or second derivatives are present else false.
 **/
bool PesProperty::HasDerivatives
   (
   )  const
{
   return   (  mFirstDer.Size() != 0
            || (  mSecondDer.Nrows() != 0
               && mSecondDer.Ncols() != 0
               )
            ); 
}

/**
 * Set the first derivatives of property.
 *
 * @param aV   The first derivatives.
 **/
void PesProperty::SetFirstDerivatives
   (  const MidasVector& aV
   ) 
{
   mFirstDer.SetNewSize(aV.Size(), false);
   mFirstDer = aV;
}

/**
 * Set the second derivatives.
 *
 * @param aM   The second derivatives.
 **/
void PesProperty::SetSecondDerivatives
   (  const MidasMatrix& aM
   ) 
{
   mSecondDer.SetNewSize(aM.Ncols(), false);
   mSecondDer = aM;
}

/**
 * Dump to file for restart-ability.
 *
 * @param aOs    The filestream to dump to.
 **/
void PesProperty::DumpToStream
   (  std::ostream& aOs
   )  const
{
   aOs << mOrder    << " "
       << mRotGroup << " ";

   aOs << mElement.size() << " ";
   for(int i = 0 ; i < mElement.size(); ++i)
   {
      aOs << mElement[i] << " ";
   }

   aOs << mDescriptor << " "
       << (mDerFile.size() ? mDerFile : "lolfile")   << " "
       << mValue << " ";
   
   // first derivatives
   aOs << mFirstDer.Size() << " ";
   for(int i = 0; i < mFirstDer.Size(); ++i)
   {
      aOs << mFirstDer[i] << " ";
   }

   // second derivatives
   aOs << mSecondDer.Nrows() << " "
       << mSecondDer.Ncols() << " ";
   for(int i = 0; i < mSecondDer.Nrows(); ++i)
   {
      for(int j = 0; j < mSecondDer.Ncols(); ++j)
      {
         aOs << mSecondDer[i][j] << " ";
      }
   }

}

/**
 * Read from file
 *
 * @param aIs     The stream to read from.
 **/
void PesProperty::ReadFromStream
   (  std::istream& aIs
   )
{
   aIs >> mOrder
       >> mRotGroup;

   int size;
   aIs >> size;
   if(size)
   {
      mElement.resize(size);
      for(int i = 0; i < size; ++i)
      {
         aIs >> mElement[i];
      }
   }

   aIs >> mDescriptor
       >> mDerFile
       >> mValue;

   // read in first derivatives
   aIs >> size;
   if(size)
   {
      mFirstDer.SetNewSize(size);
      for(int i = 0; i < size; ++i)
      {
         aIs >> mFirstDer[i];
      }
   }
   
   // read in second derivatives
   In nrows = 0;
   In ncols = 0;
   aIs >> nrows >> ncols;
   if(nrows && ncols)
   {
      mSecondDer.SetNewSize(nrows, ncols);
      for(int i = 0; i < nrows; ++i)
      {
         for(int j = 0; j < ncols; ++j)
         {
            aIs >> mSecondDer[i][j];
         }
      }
   }
}


/**
 *
 **/
std::ostream& operator<<(std::ostream& aOut, const PesProperty& aPi)
{
   aOut << "Descr: " << aPi.GetDescriptor() << endl;
   aOut << "Order: " << aPi.GetOrder() << endl;
   //aOut << "Elem : " << aPi.GetElement() << endl;
   aOut << "RG   : " << aPi.GetRotGroup() << endl;
   aOut << "value: " << aPi.GetValue() << endl;
   return aOut;
}

#ifdef VAR_MPI
/**
 * Constructor from mpi::Blob
 *
 * @param aBlob    The blob to construct from.
 **/
PesProperty::PesProperty
   (  mpi::Blob& aBlob
   )
{
   aBlob.Unload(reinterpret_cast<char*>(&mOrder), sizeof(mOrder));
   aBlob.Unload(reinterpret_cast<char*>(&mRotGroup), sizeof(mRotGroup));
   
   int size;
   aBlob.Unload(reinterpret_cast<char*>(&size), sizeof(size));
   mElement.resize(size);
   if(size)
   {
      aBlob.Unload(reinterpret_cast<char*>(&mElement[0]), size * sizeof(mElement[0]));
   }

   aBlob.Unload(mDescriptor);
   aBlob.Unload(mDerFile);

   aBlob.Unload(reinterpret_cast<char*>(&mValue), sizeof(mValue));

   // Unload first derivatives
   aBlob.Unload(reinterpret_cast<char*>(&size), sizeof(size));
   mFirstDer.SetNewSize(size);
   if(size)
   {
      aBlob.Unload(reinterpret_cast<char*>(&mFirstDer[0]), size * sizeof(mFirstDer[0]));
   }

   // Unload second derivatives
   int nrows;
   int ncols;
   aBlob.Unload(reinterpret_cast<char*>(&nrows), sizeof(nrows));
   aBlob.Unload(reinterpret_cast<char*>(&ncols), sizeof(ncols));
   mSecondDer.SetNewSize(nrows, ncols);
   if(nrows && ncols)
   {
      for(int i = 0; i < nrows; ++i)
      {
         aBlob.Unload(reinterpret_cast<char*>(mSecondDer[i]), ncols * sizeof(mSecondDer[i]));
      }
   }
   
   MidasWarning("MPI BLOB IMPLEMENTATION NOT TESTED FOR SECOND DERIVATIVES");
}

/**
 * Create an mpi::Blob
 *
 * @return     Returns object as an mpi blob.
 **/
void PesProperty::LoadIntoMpiBlob
   (  mpi::Blob& aBlob
   )  const
{
   //
   aBlob.Load(reinterpret_cast<const char*>(&mOrder), sizeof(mOrder));
   aBlob.Load(reinterpret_cast<const char*>(&mRotGroup), sizeof(mRotGroup));

   int size = mElement.size();
   aBlob.Load(reinterpret_cast<const char*>(&size), sizeof(size));
   aBlob.Load(reinterpret_cast<const char*>(&mElement[0]), size * sizeof(mElement[0]));
   
   aBlob.Load(mDescriptor);
   aBlob.Load(mDerFile);

   aBlob.Load(reinterpret_cast<const char*>(&mValue), sizeof(mValue));
   
   // Load first derivatives
   size = mFirstDer.Size();
   aBlob.Load(reinterpret_cast<const char*>(&size), sizeof(size));
   aBlob.Load(reinterpret_cast<const char*>(&mFirstDer[0]), size * sizeof(mFirstDer[0]));
    
   // Load second derivatives
   int nrows = mSecondDer.Nrows();
   int ncols = mSecondDer.Ncols();
   aBlob.Load(reinterpret_cast<const char*>(&nrows), sizeof(nrows));
   aBlob.Load(reinterpret_cast<const char*>(&ncols), sizeof(ncols));
   for(int i = 0; i < nrows; ++i)
   {
      aBlob.Load(reinterpret_cast<const char*>(mSecondDer[i]), ncols * sizeof(mSecondDer[i]));
   }
   MidasWarning("MPI BLOB IMPLEMENTATION NOT TESTED FOR SECOND DERIVATIVES");
}
#endif /* VAR_MPI */

} /* namespace pes */
} /* namespace midas */
