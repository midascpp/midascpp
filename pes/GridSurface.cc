/**
************************************************************************
* 
* @file                GridSurface.cc
*
* Created:             08-03-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Running grid based non-iterative surface calculation 
* 
* Last modified: Thu Jul 15, 2010  01:13:00
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "util/Io.h"
#include "util/FileSystem.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "pes/GridType.h"
#include "pes/Derivatives.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"
#include "pes/BarFileHandlerNonGrid.h"
#include "pes/BarFileHandler.h"
#include "pes/Surface.h"
#include "pes/GridSurface.h"
#include "pes/PesUtility.h"

// using declarations
using std::ifstream;
using std::string;
using std::vector;

namespace midas
{
namespace pes
{

/**
 * Constructor for GridSurface
 *
 * @param aPesInfos        Info on current surface(s).
 * @param aPesMainDir      The main directory for the surface(s) generation. 
 **/
GridSurface::GridSurface
   (  std::vector<PesInfo>& aPesInfos
   ,  const std::string& aPesMainDir
   ) 
   :  Surface
      (  aPesInfos
      ,  aPesMainDir
      )
{
   // Single point information are handled differently depending on the surface generation strategy being Static grid or Taylor (grid) 
   if (gPesCalcDef.GetmPesGrid())
   {
      for (auto& pes_info : mPesInfos)
      {
         mBarFiles.emplace_back(new BarFileHandler(pes_info));
      }
   }
   else
   {
      for (auto& pes_info : mPesInfos)
      {
         mBarFiles.emplace_back(new BarFileHandlerNonGrid(pes_info));
      }
   }
}

/**
 * Run grid surface
 */
void GridSurface::RunSurface
   (
   )
{
   // Maximum mode combination level for the surface
   In max_pes_num_mcr = gPesCalcDef.GetmPesNumMCR();

   // Loop over multilevels, i.e. electronic structure methods 
   for (In ilevel = I_0; ilevel < gPesCalcDef.MultiLevel(); ++ilevel)
   {
      if (ilevel == I_0)
      {
         if (gPesCalcDef.MultiLevel() == I_0)
         {
            for (auto& pes_info : mPesInfos)
            {
               pes_info.SetMCLevel(max_pes_num_mcr);
            }
         }
         else
         {
            // Multilevel calculation header
            Mout << std::endl;
            Out72Char(Mout,'$','$','$');
            Out72Char(Mout,'$',' ','$');
            OneArgOut72(Mout," Begin Multilevel Procedure for PES ",'$');
            Out72Char(Mout,'$',' ','$');
            Out72Char(Mout,'$','$','$');
            Mout << std::endl;
            
            continue;
         }
      }
      else 
      {
         // Update PesInfo's with multilevel info
         for (auto& pes_info : mPesInfos)
         {
            // Touch ListOfCalcs_Done if doing linear combinations of property terms
            if (gPesCalcDef.GetmPesDoLinearComb())
            {
               midas::filesystem::Touch(pes_info.GetMultiLevelInfo().SaveIoDir() + "/ListOfCalcs_Done");
            }

            //
            pes_info.Update(gPesCalcDef, ilevel);
         
            //
            if (gPesCalcDef.GetmPesGrid())
            {
               max_pes_num_mcr = gPesCalcDef.LevelMc()[ilevel];
               pes_info.SetMCLevel(max_pes_num_mcr);
            }
         }
      }
   
      // Provide PesInfo's with derivative information
      for (auto& pes_info : mPesInfos)
      {
         // Initialize the container for property/properties with derivative/derivatives
         pes_info.CleanmPropWithDer();
         
         // Determine if derivative information is to be used
         pes_info.SetmUseDerivatives(false);
         auto no_props = pes_info.GetMultiLevelInfo().PropInfo().Size();
         for (In prop = I_0; prop < no_props; ++prop)
         {
            auto der_file = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDerFile();
            if (!der_file.empty())
            {
               auto prop_label = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDescriptor();
               
               pes_info.SetmUseDerivatives(true);
               pes_info.SetmPropWithDer(prop_label);
         
               if (gDebug || gPesIoLevel > I_8)
               {
                  Mout << " Derivative information on " << prop_label << " is available for level_" << ilevel << std::endl;
               }
            }
            else
            {
               if (gDebug || gPesIoLevel > I_8)
               {
                  auto prop_label = pes_info.GetMultiLevelInfo().PropInfo().GetPropertyInfoEntry(prop).GetDescriptor();
                  Mout << " Derivative information on " << prop_label << " is not available for level_" << ilevel << std::endl;
               }
            }
         }
      }

      // Clean the list of singlepoint calculations, and check for already calculated points for this level.
      for (auto& calculation_list : mCalculationLists)
      {
         calculation_list.Clean();
         calculation_list.CheckForCalcsDone();
      }

      // Static grid calculation header
      Mout << std::endl;
      Out72Char(Mout,'~','-','~');
      OneArgOut72(Mout, " Starting static grid procedure for generating " + std::to_string(max_pes_num_mcr) + "-mode grids ", '~');
      Out72Char(Mout,'~','-','~');
      Mout << std::endl;

      // Define properties for different subsystems, i.e. mode combination range, grid types and derivative container for modified Shepard interpolation or extrapolation 
      auto max_mcl = max_pes_num_mcr;
      std::vector<ModeCombiOpRange> subsystems_mcr;
      std::vector<GridType> grid_fractions;
      std::vector<Derivatives> subsystems_derivatives;
      
      // Allocate memory
      subsystems_mcr.reserve(mNoSubsystems);
      grid_fractions.reserve(mNoSubsystems);
      subsystems_derivatives.reserve(mNoSubsystems);

      // Loop over subsystems and set prerequisites for surface generation
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // Construct the mode combination range for each subsystem
         ModeCombiOpRange mcr;
         if (gPesCalcDef.GetmUseExtMcr())
         {
            mcr = ConstructModeCombiOpRangeFromExtendedRangeModes
               (  max_pes_num_mcr
               ,  mPesInfos[isystem].GetMolecule().GetNoOfVibs()
               ,  gPesCalcDef.GetmExtMcrModes()
               );
         }
         else
         {
            mcr = ConstructModeCombiOpRange
               (  gPesCalcDef.GetmPesFlexCoupCalcDef()
               ,  max_pes_num_mcr
               ,  mPesInfos[isystem].GetMolecule().GetNoOfVibs()
               );
         }
       
         // Initialize the grid of single points
         GridType one_mode_grid_fractions;
         if (gPesCalcDef.GetmPesGrid())
         {
            GridSetup(mPesInfos[isystem], I_0, mcr, max_mcl, one_mode_grid_fractions);
         }
       
         //
         mBarFiles[isystem]->DoDispDer_zero(mcr, max_mcl, one_mode_grid_fractions, mCalculationLists[isystem]);
      
         // Store mode combination range and grid fractions for later use
         subsystems_mcr.emplace_back(mcr);
         grid_fractions.emplace_back(one_mode_grid_fractions);
         
         //
         Derivatives subsystem_derivatives;
         subsystems_derivatives.emplace_back(std::move(subsystem_derivatives));
      }

      // Execute the calculation and collect the results
      for (auto& calculation_list : mCalculationLists) 
      {
         calculation_list.EvaluateList();
      }

      //
      for (In isystem = I_0; isystem < mNoSubsystems; ++isystem)
      {
         // Declaring an empty vector of vector which we need to pass to the GetAnalyticalSurfaces function for it to work correctly, (should definately be restructured)
         std::vector<std::vector<Nb>> one_mode_grid_bounds(C_0, std::vector<Nb>(C_0, I_0));
       
         // For static grids we only ever perform one fit to all MCs in the MCR, so we cannot skip any of them
         std::vector<bool> skip_mcs_in_fit(subsystems_mcr[isystem].Size(), false);
      
         //
         if (  gPesCalcDef.GetmPesDoExtrap() 
            || (  gPesCalcDef.GetmPesShepardInt() 
               && gPesCalcDef.GetmPesShepardOrd() > I_0
               )
            )
         {
            // Reads derivative (bar-)information from file and store this in the derivative container
            FillDerContainer(subsystems_derivatives[isystem], mCalculationLists[isystem], mPesInfos[isystem]);
         }

         if (gPesCalcDef.GetmPesGrid() && gPesIoLevel > I_10)
         {
            Mout << std::endl << " **** Linear Combination For The Transformed Potentials **** " << std::endl;
         }

         // Calculate the bar-potentials
         mBarFiles[isystem]->DoDispDer_one(subsystems_mcr[isystem], max_mcl, grid_fractions[isystem], subsystems_derivatives[isystem]);
    
         // Calculate the extrapolated points
         if (gPesCalcDef.GetmPesDoExtrap())
         {
            mPesInfos[isystem].SetExtrapFrom(gPesCalcDef.GetmExtrapolateFrom());
            mBarFiles[isystem]->Extrapolation(subsystems_derivatives[isystem], mCalculationLists[isystem], mPesInfos[isystem].GetmPropWithDer());
         }
       
         //
         if (gPesCalcDef.GetmPesGrid())
         {
            if (gPesIoLevel > I_4)
            {
               Mout << std::endl << " Coarse grid has been generated  " << std::endl;
               Mout << " Start interpolation and fitting procedure " << std::endl;
            }
       
            // Run the fitting routine and dump the analytical surface(s) as .mop files
            mDoPotentialFits[isystem].GetAnalyticalSurfaces
               (  subsystems_mcr[isystem]
               ,  grid_fractions[isystem]
               ,  subsystems_derivatives[isystem]
               ,  mPesInfos[isystem].GetmUseDerivatives()
               ,  one_mode_grid_bounds
               ,  nullptr
               ,  mCalculationLists[isystem]
               ,  I_0
               ,  skip_mcs_in_fit
               );
         }
       
         //
         if (gPesCalcDef.GetmPesDoLinearComb())
         {
            Mout << " Infinte basis set Intrinsic Mode Combination procedure" << endl;
       
            midas::filesystem::Mkdir(mPesInfos[isystem].GetMultiLevelInfo().SaveIoDir());
            vector<string> list_of_levels;
       
            for (In i = I_1; i <= gPesCalcDef.GetmPesLinearComb().size(); i++)
            {
               list_of_levels.push_back(mPesInfos[isystem].GetmSubSystemDir() + "/Multilevels/level_" + std::to_string(i) + "/savedir/");
            }
            string collect = mPesInfos[isystem].GetMultiLevelInfo().SaveIoDir();
       
            In n_of_calc_lev1 =  DoLinearComb(list_of_levels, collect);
       
            In max_pes_num_mcr = gPesCalcDef.LevelMc()[I_0]; // IAN: IS I_0 CORRECT ??
       
            auto lc_mcr = ModeCombiOpRange(max_pes_num_mcr, mPesInfos[isystem].GetMolecule().GetNoOfVibs());
            GridSetup(mPesInfos[isystem], I_1, lc_mcr, max_mcl, grid_fractions[isystem]);
       
            if (gPesCalcDef.GetmPesGrid() && gPesIoLevel > I_10)
            {
               Mout << std::endl << " **** Linear Combination For The Transformed Potentials **** " << std::endl << endl;
            }
       
            //
            if (gPesCalcDef.GetmPesGrid())
            {
               if (gPesIoLevel > I_4)
               {
                  Mout << std::endl << " ****  Coarse grid has been generated  **** " << std::endl << std::endl;
                  Mout << " **** start spline interpolation and fitting procedure ***** " << std::endl << std::endl;
               }
       
               // Dump out on the .mop files
               mDoPotentialFits[isystem].GetAnalyticalSurfaces
                  (  lc_mcr
                  ,  grid_fractions[isystem]
                  ,  subsystems_derivatives[isystem]
                  ,  true
                  ,  one_mode_grid_bounds
                  ,  nullptr
                  ,  mCalculationLists[isystem]
                  ,  I_0
                  ,  skip_mcs_in_fit
                  );
            }
         }

         //
         if (gPesCalcDef.GetmMakeCutAnalysis().size())
         {
            MakeCutAna(mPesInfos[isystem]);
         }
         
         //
         if (  (gPesCalcDef.GetmCartesiancoordInPes() 
            && (gPesCalcDef.GetmList_Of_Hessians().size() > I_0)
               )  
            || gPesCalcDef.GetmPesVibPol() 
            )
         {
            GradHess(mPesInfos[isystem]);
         }
      }

      // Multilevel calculation footer
      if (gPesCalcDef.MultiLevel() != I_0)
      {
         Mout << std::endl;
         Out72Char(Mout,'$','$','$');
         Out72Char(Mout,'$',' ','$');
         OneArgOut72(Mout," Multilevel Procedure for PES: Completed ",'$');
         Out72Char(Mout,'$',' ','$');
         Out72Char(Mout,'$','$','$');
         Mout << std::endl;
      }
   }

   if (gPesCalcDef.MultiLevel())
   {
      FinalizeMultilevelSurface();
     
      // Unlink savedir and analysis in the main directory, as they are not needed anymore
      midas::filesystem::Unlink(mPesMainDir + "/savedir");
      midas::filesystem::Unlink(mPesMainDir + "/analysis");
   }
}

/**
 * Sets up the Gradient and Hessia in and transforms to normal coordinates.
 * Also, do the vibrational polarizability in the DH approximation Should be
 * used in connection with derivative force-fields.
 **/
void GridSurface::GradHess
   (  const PesInfo& aPesInfo
   )  const
{
   // Find the dimension of vectors and matrices. Depends on if Cartesian or normal coordinates
   In Ndim = I_0; 
   bool Hessian_has_been_calculated;

   if (gPesCalcDef.GetmCartesiancoordInPes()) 
   {
      Ndim = I_3 * aPesInfo.GetMolecule().GetNumberOfNuclei();
      Hessian_has_been_calculated = false;
   }
   else
   {
      Ndim = aPesInfo.GetMolecule().GetNoOfVibs();
      Hessian_has_been_calculated = true;   
   }

   MidasVector CartesianGrad(Ndim);
   MidasMatrix CartesianHess(Ndim);

// Vectors to be used for vibrational polarizabilities has to be declared here
   MidasVector GradX;
   MidasVector GradY;
   MidasVector GradZ;
   MidasMatrix eig_vec(I_0);
   MidasVector eig_val;
   vector<In> MatchVector;
   MidasVector Dkin;

   bool Error_In_Number_Of_Freqs = false;

// Vectors to be used for general transformation of cartesian gradients to gradients in normal coordinates

   MidasVector Gradient_In_Nor;

// Matrixes to be used for general transformation of cartesian Hessian to Hessian in normal coordinates

   MidasMatrix Hessian_In_Nor(I_0);
   // Actual number of "real" modes 

   In Nmodes = I_0;

   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin GradHess module ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;

   if (gPesIoLevel > I_4)
   {
      Mout << " The following output is for the electronic state No. " << std::to_string(gPesCalcDef.GetmPesExcState()) << std::endl << std::endl;
   }

// In the following we loop over the elements on gList_Of_Hessians and transform (if required) to normal coordinates.
// It is thus important that the label for the energy (ground or excited) comes before the properties in this set.
// First we read the set into a vector and thereafter we sort the vector so energy comes first.

   vector<string> To_Do;
   for(set< string>::iterator p = gPesCalcDef.GetmList_Of_Hessians().begin(); p!= gPesCalcDef.GetmList_Of_Hessians().end(); p++)
   {
      To_Do.push_back(*p);
   }

// If we are in Cartesin coordinates, we need to have an energy label on the list, otherwise we do not know the Hessian 
// and cannot transform to normal coordinates. If, on the other hand, we are in normal coordinates, this is irrelevant
// The energy label should be the first on the list

   bool not_swaped = true;
   if (gPesCalcDef.GetmNormalcoordInPes()) not_swaped = false;
   In i = I_0;
   while(not_swaped && i < To_Do.size())
   {
      if( (To_Do[i] == "GROUND_STATE_ENERGY") || (To_Do[i] == "EXCITED_STATE_ENERGY") )
      {
         swap(To_Do[0],To_Do[i]);
         not_swaped = false;
      }
      else
         ++i;
   }

   if (not_swaped)
   {
      Mout << " I did not find either GROUND_STATE_ENERGY or EXCITED_STATE_ENERGY on the to do list on entry to GradHess, I stop " << endl;
      MIDASERROR("Missing energy label on to do list on entry to GRADHESS ");
   }
   else
   {
      if (gPesIoLevel > I_4)
      {
         Mout << " GradHess module will target the property/properties: " << std::endl;
         for (In i = I_0; i < To_Do.size(); ++i)
         {
            Mout << "  " << To_Do[i] << std::endl;
         }
      }
   }

// Now, do it ...

   ifstream* File = new ifstream [To_Do.size()];
   for(In i=I_0;i<To_Do.size();i++)
   {
      In Number = FindPropertyNumberInFile(aPesInfo, To_Do[i], gPesCalcDef.GetmPesExcState());

      if (Number == -I_1)
      {
         Mout << endl;
         Mout << " " << To_Do[i] << " for electronic state no. " << gPesCalcDef.GetmPesExcState(); 
         Mout << " not found on PropertyInfo " <<  endl;
         Mout << " I will skip the required calculations for this property " << endl;
         if (gPesCalcDef.GetmPesVibPol())
         {
            if ( (To_Do[i] == "X_DIPOLE") || (To_Do[i] == "Y_DIPOLE") || (To_Do[i] == "Z_DIPOLE") )
            {
               gPesCalcDef.SetmPesVibPol(false);
               Mout << endl;
               Mout << " VibPol calculation cancelled due to missing dipole moment calculation " << endl;
            }
         }
      continue;
      }

      string PropFileName = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_" + std::to_string(Number) + ".mop";
      File[i].open(PropFileName.c_str());
      if (!File[i])
      {
         Mout << " Unable to open file " << PropFileName << " for Gradient/Hessian calculation " << endl;
         Mout << " I will skip making Gradients and/or Hessians for this property " << endl;
         continue;
      }
      File[i].setf(ios::scientific);
      File[i].setf(ios::uppercase);
      File[i].precision(I_22);

      string s;
      getline(File[i],s);
      CartesianGrad.Zero();
      CartesianHess.Zero();
      while (!File[i].eof())
      {
         istringstream s1(s);
         Nb Prop;
         vector<string> operators;
         string oper;
         s1 >> Prop;
         while (s1 >> oper)
         {
            operators.push_back(oper);
         }
         if (operators.size() == I_1)
         {
            size_t poss = operators[I_0].find("Q^1");
            if(poss != string::npos)
            {
               string modename = operators[I_0].substr(I_4,operators[I_0].size()-I_5);
               int mode_nr = aPesInfo.GetMolecule().GetModeNo(modename);
               if(mode_nr != -1)
               {
                  CartesianGrad[mode_nr] = Prop;
                  if (gPesCalcDef.GetmPesFreqScalCoordInFit())
                  {
                     CartesianGrad[mode_nr] *= std::sqrt(aPesInfo.GetMolecule().GetFreqI(mode_nr)/C_AUTKAYS);
                  }
               }
            }
            poss = operators[I_0].find("Q^2");
            if(poss != string::npos)
            {
               string modename = operators[I_0].substr(I_4,operators[I_0].size()-I_5);
               int mode_nr = aPesInfo.GetMolecule().GetModeNo(modename);
               if(mode_nr != -1)
               {
                  CartesianHess[mode_nr][mode_nr] = Prop*C_2;
                  if (gPesCalcDef.GetmPesFreqScalCoordInFit())
                  {
                     CartesianHess[mode_nr][mode_nr] *= std::sqrt((aPesInfo.GetMolecule().GetFreqI(mode_nr)/C_AUTKAYS)*(aPesInfo.GetMolecule().GetFreqI(mode_nr)/C_AUTKAYS));
                  }
               }
            }
         }

// Care has to taken now. The factor of 1/2 from the Taylor expansion has been included in Prop, so for the 
// diagonal elements we multiply by 2. For the off-diagonal elements V_ij has been set equal to V_ij + V_ji 
// which indirectly accounts for the factor of 2 which is herefore left out.

         if (operators.size() == I_2)
         {
            size_t poss = operators[I_0].find("Q^1");
            size_t poss2 = operators[I_1].find("Q^1");
            if(poss != string::npos && poss2 != string::npos)
            {
               string modename = operators[I_0].substr(I_4,operators[I_0].size()-I_5);
               string modename2 = operators[I_1].substr(I_4,operators[I_1].size()-I_5);
               int mode_nr = aPesInfo.GetMolecule().GetModeNo(modename);
               int mode_nr2 = aPesInfo.GetMolecule().GetModeNo(modename2);
               if(mode_nr != -I_1 && mode_nr2 != -I_1)
               {    
                   CartesianHess[mode_nr][mode_nr2] = Prop;
                   CartesianHess[mode_nr2][mode_nr] = Prop;
                   if (gPesCalcDef.GetmPesFreqScalCoordInFit())
                   {
                      CartesianHess[mode_nr][mode_nr2] *= std::sqrt((aPesInfo.GetMolecule().GetFreqI(mode_nr)/C_AUTKAYS)*(aPesInfo.GetMolecule().GetFreqI(mode_nr2)/C_AUTKAYS));

                      CartesianHess[mode_nr2][mode_nr] *= std::sqrt((aPesInfo.GetMolecule().GetFreqI(mode_nr2)/C_AUTKAYS)*(aPesInfo.GetMolecule().GetFreqI(mode_nr)/C_AUTKAYS));
                   }

               }
            }
         }
         getline(File[i],s);
      }

      if (gPesIoLevel > I_5)
      {
         Mout << std::endl;
         std::string label = " " + To_Do[i] + " gradient generated from file " + PropFileName;
         CartesianGrad.PrintVertical(label);
      }

      if (gPesCalcDef.GetmPesVibPol() && !gPesCalcDef.GetmCartesiancoordInPes())
      {
         GradX.SetNewSize(Ndim);
         GradY.SetNewSize(Ndim);
         GradZ.SetNewSize(Ndim);
         if (To_Do[i] == "X_DIPOLE") GradX = CartesianGrad;
         if (To_Do[i] == "Y_DIPOLE") GradY = CartesianGrad;
         if (To_Do[i] == "Z_DIPOLE") GradZ = CartesianGrad;
      }

      if (gPesCalcDef.GetmCartesiancoordInPes())
      {
//       Dimension in the following is 3*aPesInfo.GetMolecule().GetNumberOfNuclei(), i.e. only in here for cartesian stuff
         if (gPesIoLevel > I_5)
         {
            Mout << std::endl;
            Mout << " Cartesian " << To_Do[i] << " Hessian generated from file " << PropFileName << std::endl;
            Mout << CartesianHess << std::endl;
         }

         if ( (To_Do[i] == "GROUND_STATE_ENERGY") || (To_Do[i] == "EXCITED_STATE_ENERGY") )
         {
//          MwHess is the mass-weighted molecular Hessian

            MidasMatrix MwHess(I_3 * aPesInfo.GetMolecule().GetNumberOfNuclei());
            Dkin.SetNewSize(I_3 * aPesInfo.GetMolecule().GetNumberOfNuclei());

//          Construction of mass weightning vector Dkin

            In k = I_0;
            for (In j = I_0; j < aPesInfo.GetMolecule().GetNumberOfNuclei(); j++)
            {
               Dkin[k] = C_1 / (std::sqrt(C_FAMU * aPesInfo.GetMolecule().GetNuclMassi(j)));
               Dkin[k + I_1] = C_1 / (std::sqrt(C_FAMU * aPesInfo.GetMolecule().GetNuclMassi(j)));
               Dkin[k + I_2] = C_1 / (std::sqrt(C_FAMU * aPesInfo.GetMolecule().GetNuclMassi(j)));
               k += I_3;
            }

//          Do the mass weightning

            for (In k = I_0; k < I_3 * aPesInfo.GetMolecule().GetNumberOfNuclei(); k++)
            {
               for (In l = I_0; l < I_3 * aPesInfo.GetMolecule().GetNumberOfNuclei(); l++)
               {
                  MwHess[k][l] = Dkin[k] * CartesianHess[k][l] * Dkin[l];
               }
            }

            // mbh: project out translation and rotation
            // 0) compute total mass
            Nb total_mass = C_0;
            for (In i1 = I_0; i1 < aPesInfo.GetMolecule().GetNumberOfNuclei(); i1++) 
            {
               total_mass += C_FAMU * aPesInfo.GetMolecule().GetNuclMassi(i1);
            }
         
            if (gPesIoLevel > I_10)
            {
               Mout << " TOTAL MASS = " << total_mass << std::endl;
            }

            // 0a) set up eps array
            Nb eps_array[3][3][3];
            for(In i1=0;i1<3;i1++)
            {
               for(In i2=0;i2<3;i2++)
               {
                  for(In i3=0;i3<3;i3++)
                  {
                     eps_array[i1][i2][i3]=C_0;
                     if(i1==0 && i2==1 && i3==2) eps_array[i1][i2][i3]=C_1;
                     else if(i1==2 && i2==0 && i3==1) eps_array[i1][i2][i3]=C_1;
                     else if(i1==1 && i2==2 && i3==0) eps_array[i1][i2][i3]=C_1;
                     else if(i1==1 && i2==0 && i3==2) eps_array[i1][i2][i3]=C_M_1;
                     else if(i1==0 && i2==2 && i3==1) eps_array[i1][i2][i3]=C_M_1;
                     else if(i1==2 && i2==1 && i3==0) eps_array[i1][i2][i3]=C_M_1;
                  
                     if (gPesIoLevel > I_10)
                     {
                        Mout << "  Eps[" << i1 << "][" << i2 << "][" << i3 << "] = " << eps_array[i1][i2][i3] << std::endl;
                     }
                  }
               }
            }
            // 0b) set up inverse moment of inertia
            MidasMatrix k_mat(I_3,C_0);
            for(In i2=0;i2<3;i2++) {
               for(In i3=0;i3<3;i3++) {
                  for(In i1=0;i1<aPesInfo.GetMolecule().GetNumberOfNuclei();i1++) {
                     Nb r_i=C_0;   //initialized
                     Nb r_j=C_0;   //initialized
                     if(i2==0)
                        r_i=aPesInfo.GetMolecule().GetNuci(i1)->X();
                     else if(i2==1)
                        r_i=aPesInfo.GetMolecule().GetNuci(i1)->Y();
                     else if(i2==2)
                        r_i=aPesInfo.GetMolecule().GetNuci(i1)->Z();
                     if(i3==0)
                        r_j=aPesInfo.GetMolecule().GetNuci(i1)->X();
                     else if(i3==1)
                        r_j=aPesInfo.GetMolecule().GetNuci(i1)->Y();
                     else if(i3==2)
                        r_j=aPesInfo.GetMolecule().GetNuci(i1)->Z();
                     k_mat[i2][i3]+=C_FAMU*aPesInfo.GetMolecule().GetNuclMassi(i1)*r_i*r_j;
                  }
               }
            }
            // 1) Form Proj. matrix
            MidasMatrix PrMat(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),C_0);
            PrMat.Unit();
            MidasVector x_tr(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),C_0);
            MidasVector y_tr(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),C_0);
            MidasVector z_tr(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),C_0);
            for(In i1=0;i1<aPesInfo.GetMolecule().GetNumberOfNuclei();i1++)
            {
               x_tr[i1*3]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU/total_mass);
               y_tr[i1*3+1]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU/total_mass);
               z_tr[i1*3+2]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU/total_mass);
            }
            MidasMatrix rot(3,3*aPesInfo.GetMolecule().GetNumberOfNuclei(),C_0);
            for(In i1=0;i1<aPesInfo.GetMolecule().GetNumberOfNuclei();i1++) {
               rot[0][3*i1+1]=C_M_1*sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->Z();
               rot[0][3*i1+2]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->Y();
               rot[1][3*i1]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->Z();
               rot[1][3*i1+2]=C_M_1*sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->X();
               rot[2][3*i1]=C_M_1*sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->Y();
               rot[2][3*i1+1]=sqrt(aPesInfo.GetMolecule().GetNuclMassi(i1)*C_FAMU)*aPesInfo.GetMolecule().GetNuci(i1)->X();
            }
            MidasMatrix sqrt_inv_mom(I_3,C_0);
            for(In i1=0;i1<3;i1++) {
               for(In i2=0;i2<3;i2++) {
                  for(In i3=0;i3<3;i3++) {
                     for(In i4=0;i4<3;i4++) {
                        for(In i5=0;i5<3;i5++) {
                           sqrt_inv_mom[i1][i2]+=eps_array[i1][i3][i5]*eps_array[i2][i4][i5]
                                                   *k_mat[i3][i4];
                        }
                     }
                  }
               }
            }
            SqrtInvert(sqrt_inv_mom);
            rot=sqrt_inv_mom*rot;
            for(In i1=0;i1<3*aPesInfo.GetMolecule().GetNumberOfNuclei();i1++) {
               for(In i2=0;i2<3*aPesInfo.GetMolecule().GetNumberOfNuclei();i2++) {
                  PrMat[i1][i2]-=x_tr[i1]*x_tr[i2];
                  PrMat[i1][i2]-=y_tr[i1]*y_tr[i2];
                  PrMat[i1][i2]-=z_tr[i1]*z_tr[i2];
                  PrMat[i1][i2]-=rot[0][i1]*rot[0][i2];
                  PrMat[i1][i2]-=rot[1][i1]*rot[1][i2];
                  PrMat[i1][i2]-=rot[2][i1]*rot[2][i2];
                  }
            }
            MwHess=MwHess*PrMat;
            PrMat.Transpose();
            MwHess=PrMat*MwHess;
            // end try this

//          Diagonalize the mass-weighted Hessian

            eig_vec.SetNewSize(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),3*aPesInfo.GetMolecule().GetNumberOfNuclei());
            eig_val.SetNewSize(3*aPesInfo.GetMolecule().GetNumberOfNuclei());
            string diag_method;
            if (gNumLinAlg=="LAPACK")
               diag_method="DSYEVD";
            else
               diag_method="MIDAS_JACOBI";
            Diag(MwHess,eig_vec,eig_val,diag_method,true);

            if (gPesIoLevel > I_8)
            {
               std::string label = " Eigenvalues of mass-weighted " + To_Do[i] + " Hessian in au";
               eig_val.PrintVertical(label);
            }

            //Construct the vibrational frequencies and transorm to cm^-1
            vector<bool> sad_freq;
            for (In k=I_0;k<eig_val.Size();k++)
            {
               bool is_imm=false;
               if (eig_val[k]<C_0) is_imm=true;
               eig_val[k] = pow(fabs(eig_val[k]),C_I_2);
               sad_freq.push_back(is_imm);
            }
            eig_val.Scale(C_AUTKAYS,I_0,-I_1);

            if (gPesIoLevel > I_5)
            {
               Mout << std::endl;
               Mout << " Electronic state No. " << std::to_string(gPesCalcDef.GetmPesExcState()) << std::endl;
               Mout << " Calculated Harmonic frequencies (including translation and rotational motion) in cm^-1: " << std::endl;
               Mout << " ***************************************************** " << std::endl;
            
               In local_counter = I_1;
               for (In k = I_0; k < eig_val.Size(); k++)
               {  
                  if (k < I_3)
                  {
                     Mout << "T" << local_counter << "       " << eig_val[k] << std::endl;
                  }
                  else if (k < I_6)
                  {
                     Mout << "R" << local_counter << "       " << eig_val[k] << std::endl;
                  }
                  else
                  {
                     Mout << "V" << local_counter << "       " << eig_val[k] << std::endl;
                  }
                  
                  if (k == I_2)
                  {
                     local_counter = I_0;
                  }
                  else if (k == I_5)
                  {
                     local_counter = I_0;
                  }

                  local_counter += I_1;
               }
               Mout << std::endl;
            }

//          Analyse eigenvalues and skip the rotational/translotational parts
//          Print the frequencies and Cartesian normal modes
            Mout << std::endl;
            Mout << " Electronic state No. " << std::to_string(gPesCalcDef.GetmPesExcState()) << std::endl;
            Mout << " Calculated Harmonic vibrational frequencies in cm^-1: " << std::endl;
            Mout << " ***************************************************** " << std::endl;

            In local_counter = I_1;
            // mbh; print normal coord. to file, MIDAS.NOR
            ofstream ofs("MIDAS.NOR");
            ofs.setf(ios::scientific);
            midas::stream::ScopedPrecision(20, ofs);
            ofs << "FREQ\ncm-1" << endl;
            for (In k=I_0;k<eig_val.Size();k++)
            {
               if ((eig_val[k] > gPesCalcDef.GetmNumDerFreqThres()))
               {
                  MatchVector.push_back(k);
                  std::string s_sp = "        ";
                  if (sad_freq[k])
                  {
                     s_sp = "       i";
                  }
                  Mout << local_counter << s_sp << eig_val[k] << endl;
                  ofs << eig_val[k] << endl;
                  local_counter = local_counter + I_1;
               }
            }

            Nmodes = MatchVector.size();
            if (MatchVector.size() != aPesInfo.GetMolecule().GetNoOfVibs() && 
                  (!gPesCalcDef.GetmCartesiancoordInPes() || MatchVector.size() != aPesInfo.GetMolecule().GetNumberOfNuclei()*I_3 -I_6))
            {
               Mout << endl;
               Mout << " !! Warning !! " << endl;
               Mout << " I Found " << MatchVector.size() << " vibrational frequencies but I expected to find " << aPesInfo.GetMolecule().GetNoOfVibs() << endl;
               Mout << " Normal coordinate transformation not setup correctly ! " << endl;
               Mout << endl;
               Error_In_Number_Of_Freqs = true;
            }

            Mout << endl;
            Mout << " Normal Coordinate in Cartesian Basis (bohr*amu^1/2) " << endl;
            Mout << " *************************************************** " << endl;

            MidasVector NormalCoord(3*aPesInfo.GetMolecule().GetNumberOfNuclei());
            ofs << "COORD\nau" << endl;
            for (In k=I_0;k<MatchVector.size();k++)
            {
               eig_vec.GetCol(NormalCoord,MatchVector[k]);
               for (In l=I_0;l<3*aPesInfo.GetMolecule().GetNumberOfNuclei();l++)
               {
                  NormalCoord[l] = NormalCoord[l]*Dkin[l]*sqrt(C_FAMU);
               }

               In ll = I_0;
               for (In l=I_0;l<aPesInfo.GetMolecule().GetNumberOfNuclei();l++)
               {
                  Mout << aPesInfo.GetMolecule().GetNuci(l)->AtomLabel() << "   " << NormalCoord[ll] << "   " << NormalCoord[ll+1]; 
                  Mout << "   " << NormalCoord[ll+2] << endl;
                  ofs << "   " << NormalCoord[ll] << "   " << NormalCoord[ll+1] << "   " << NormalCoord[ll+2] << endl;
                  ll += I_3;
               }
               Mout << endl;
            }
            ofs.close();
            Hessian_has_been_calculated = true;
         }
         }
   
      if ( gPesCalcDef.GetmCartesiancoordInPes() && (To_Do[i] != "GROUND_STATE_ENERGY") && (To_Do[i] != "EXCITED_STATE_ENERGY") && Hessian_has_been_calculated )
      {
//       Transform property gradient to normal coordinates
         Gradient_In_Nor.SetNewSize(Nmodes);
         Gradient_In_Nor.Zero();

         MidasVector NormalCoord(3*aPesInfo.GetMolecule().GetNumberOfNuclei());
         NormalCoord.Zero();
         for (In k=I_0;k<MatchVector.size();k++)
         {
            eig_vec.GetCol(NormalCoord,MatchVector[k]);
            for (In l=I_0;l<3*aPesInfo.GetMolecule().GetNumberOfNuclei();l++) NormalCoord[l] = NormalCoord[l]*Dkin[l];
            Gradient_In_Nor[k] = Dot(NormalCoord,CartesianGrad);
         }
      
         if (gPesIoLevel > I_5)
         {
            std::string label = To_Do[i] + " Gradient in normal coordinate basis ";
            Gradient_In_Nor.PrintVertical(label);
         }

//       In the special case of dipole component we collect the transformed vectors to be used in the calcilation of 
//       the vibrational polarizability, see beloww.
         GradX.SetNewSize(Nmodes);
         GradY.SetNewSize(Nmodes);
         GradZ.SetNewSize(Nmodes);
         if (To_Do[i] == "X_DIPOLE") GradX = Gradient_In_Nor;
         if (To_Do[i] == "Y_DIPOLE") GradY = Gradient_In_Nor;
         if (To_Do[i] == "Z_DIPOLE") GradZ = Gradient_In_Nor;

//       Transform property hessian to normal coordinates
         Hessian_In_Nor.SetNewSize(Nmodes,Nmodes);
         Hessian_In_Nor.Zero();

//       1. Construct a matrix containing the Nmodes eigenvectors as column vectors
         MidasMatrix Lambda(3*aPesInfo.GetMolecule().GetNumberOfNuclei(),Nmodes);
         MidasVector SpecificNormalcoord(3*aPesInfo.GetMolecule().GetNumberOfNuclei());
         for (In k=I_0;k<MatchVector.size();k++)
         {
            SpecificNormalcoord.Zero();
            eig_vec.GetCol(SpecificNormalcoord,MatchVector[k]);
            for (In l=I_0;l<3*aPesInfo.GetMolecule().GetNumberOfNuclei();l++)
            {
               SpecificNormalcoord[l] = SpecificNormalcoord[l]*Dkin[l];
               Lambda[l][k] = SpecificNormalcoord[l];
            }
         }

//       2. Construct the transposed matrix in a new container
         MidasMatrix LambdaT(Nmodes,3*aPesInfo.GetMolecule().GetNumberOfNuclei());
         LambdaT = Transpose(Lambda);

//       3. Do the transformation
         Hessian_In_Nor = LambdaT*CartesianHess*Lambda;

//       4. Write out the transformed matrix
         if (gPesIoLevel > I_5)
         {
            Mout << std::endl;
            Mout << To_Do[i] << " Hessian in normal coordinate basis (Taylor expansion coeficients NOT included) " << std::endl;
            Mout << Hessian_In_Nor << std::endl;
         }

//       We are now done for this property so we close the file and continue to the next element on the vector list
      }
      File[i].close();
   }

   delete [] File;

// Construct vector with harmonic vibrational frequencies (in case of a normal coordinate run, the freqs are on file, see later in VibPol section)
   MidasVector Hfreq;
   if (gPesCalcDef.GetmPesVibPol() && gPesCalcDef.GetmCartesiancoordInPes() && Hessian_has_been_calculated)
   {
      Hfreq.SetNewSize(Nmodes);
      for (In k=I_0;k<MatchVector.size();k++) Hfreq[k] = eig_val[MatchVector[k]];
   }

   // Do the vibrational polarizability in the double harmonic approximation

   if (gPesCalcDef.GetmPesVibPol() && Hessian_has_been_calculated)
   {
      In Length;
      if (gPesCalcDef.GetmCartesiancoordInPes())
      {
         Length = Nmodes;
      }
      else
      {
         Length = aPesInfo.GetMolecule().GetNoOfVibs();
      }

      vector< pair<In,Nb> > Vib_Freqs(Length);
      string Freq_File = aPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "harmonic_frequencies.mpesinfo";
      if (!gPesCalcDef.GetmCartesiancoordInPes()) 
      {
         GetFreqsOnFile(Vib_Freqs,Freq_File);
      }

      MidasMatrix AlphaVibTensor(I_3,I_3);
      AlphaVibTensor.Zero();
      for (In l=I_0;l<Length;l++)
      {
         Nb FreqSquared_I = C_0;
         if (!gPesCalcDef.GetmCartesiancoordInPes()) FreqSquared_I = C_AUTKAYS*C_AUTKAYS/((Vib_Freqs[l].second)*(Vib_Freqs[l].second));
         if (gPesCalcDef.GetmCartesiancoordInPes())  FreqSquared_I = C_AUTKAYS*C_AUTKAYS/((Hfreq[l])*(Hfreq[l]));
         AlphaVibTensor[0][0] += GradX[l]*GradX[l]*FreqSquared_I;
         AlphaVibTensor[0][1] += GradX[l]*GradY[l]*FreqSquared_I;
         AlphaVibTensor[0][2] += GradX[l]*GradZ[l]*FreqSquared_I;
         AlphaVibTensor[1][0] += GradY[l]*GradX[l]*FreqSquared_I;
         AlphaVibTensor[1][1] += GradY[l]*GradY[l]*FreqSquared_I;
         AlphaVibTensor[1][2] += GradY[l]*GradZ[l]*FreqSquared_I;
         AlphaVibTensor[2][0] += GradZ[l]*GradX[l]*FreqSquared_I;
         AlphaVibTensor[2][1] += GradZ[l]*GradY[l]*FreqSquared_I;
         AlphaVibTensor[2][2] += GradZ[l]*GradZ[l]*FreqSquared_I;
      }

      if (Error_In_Number_Of_Freqs)
      {
         Mout << endl;
         Mout << " Warning !, Vibrational polarizability calculated using " << MatchVector.size() << " vibrational frequencies ! " << endl;
      }
      Mout << endl;
      Mout << " Vibrational polarizability tensor in the double harmonic approximation (au) " << endl;
      Mout << " *************************************************************************** " << endl;
      Mout << AlphaVibTensor << endl;
      Mout << " Isotropic vibrational polarizability is: " << C_1/C_3*AlphaVibTensor.Trace() << endl;
      Mout << endl;
   }
      
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," GradHess module ended ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
}

/**
* Perfome the linear combination 
**/
In GridSurface::DoLinearComb (vector<string>& arListOfLevels, string& arCollect)
{

   if (gPesIoLevel > I_5)
   {
      for (In i = I_1; i <= arListOfLevels.size(); i++)
      {
         Mout << " Level: " << i << " coef. " << gPesCalcDef.GetmPesLinearComb()[i - I_1] << " " << arListOfLevels[i - I_1] << endl;
      }
   }


   CopyFile(arListOfLevels[arListOfLevels.size()-I_1]+"/ListOfCalcs_Done",arCollect);
   CopyFile(arListOfLevels[arListOfLevels.size()-I_1]+"/ListOfCalcs",arCollect);

   In i_calc_no_in_file = I_0;
   string List_file_name = arCollect+"/ListOfCalcs_Done";
   ifstream List_file(List_file_name.c_str(),ios_base::in);
   string s;
   getline(List_file,s);
   while (!List_file.eof())
   {
      getline(List_file,s);
      i_calc_no_in_file++;
   }
   List_file.close();
   Mout << " " << i_calc_no_in_file << " files to be parsed " << endl;


   In n_lines = I_0;
   string Namelabel = arListOfLevels[I_0] + "/" + "prop_files.mpesinfo";
   ifstream Calculated_Properties(Namelabel.c_str(),ios_base::in);
   if (!Calculated_Properties)
   {
      Mout << " Unable to open file " << Namelabel << endl;
      MIDASERROR("Error when trying to open file");
   }
   string ss;
   getline(Calculated_Properties,ss);
   while(!Calculated_Properties.eof())
   {
      n_lines++;
      getline(Calculated_Properties,ss);
   }
   Calculated_Properties.close();


   for (In i_prop = I_1; i_prop <= i_calc_no_in_file ; i_prop++)
   {
      string out_name = arCollect +"/"+ std::to_string(i_prop) + "_prop.out";
      if (InquireFile(out_name)) continue;

      if (!InquireFile(out_name)) TouchFile(out_name.c_str());

      In iprpc_1, isymp_1, nord_1, isymex_1, ispinex_1, inrex_1;
      Nb prop_1, frq_y_1, frq_z_1, frq_u_1;
      string lab_mod_1, lab_x_1, lab_y_1, lab_z_1, lab_u_1;



      for (In i_line = I_0; i_line < n_lines + I_1; i_line++)
      {
         Nb ext_prop=I_0;

         for (In i_term = I_0; i_term < gPesCalcDef.GetmPesLinearComb().size(); i_term++)
         {
            string inp_1_name = arListOfLevels[i_term] + std::to_string(i_prop) + "_prop.out";

            ifstream inp_1(inp_1_name.c_str(),ios_base::in);

            string s_inp_1;
            for (In i = I_0; i<= i_line; i++)
               getline(inp_1,s_inp_1);              //get the specific line

            istringstream s_is_1(s_inp_1);
            s_is_1 >> iprpc_1;
            s_is_1 >> isymp_1;
            s_is_1 >> nord_1;
            s_is_1 >> lab_mod_1;
            s_is_1 >> prop_1;
            s_is_1 >> lab_x_1;
            s_is_1 >> lab_y_1;
            s_is_1 >> lab_z_1;
            s_is_1 >> lab_u_1;
            s_is_1 >> frq_y_1;
            s_is_1 >> frq_z_1;
            s_is_1 >> frq_u_1;
            s_is_1 >> isymex_1;
            s_is_1 >> ispinex_1;
            s_is_1 >> inrex_1;


            inp_1.close();

            ext_prop +=  gPesCalcDef.GetmPesLinearComb()[i_term]*prop_1;

            if (gPesIoLevel > I_5)
               Mout << " level "<< i_term + I_1 << " "  << prop_1<< " " ;
         }

         if (gPesIoLevel > I_5)
            Mout << "   extrapolated " << ext_prop << endl ;

         WriteDaltonProp(ext_prop, nord_1, iprpc_1, lab_x_1, out_name,"EXTRPL1");
      }

   }
   Mout <<" Extrapolation procedure completed!" << endl;
   return i_calc_no_in_file;
}

} /* namespace pes */
} /* namespace midas */
