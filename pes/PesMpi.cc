#include "PesMpi.h"

#include <string>
#include <vector>
#include <thread>
#include <chrono>

#include "mpi/Impi.h"

#include "pes/PesInfo.h"
#include "pes/CalcCode.h"
#include "pes/CalculationList.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/PesMpiTypeDefs.h"
#include "pes/AdjustInput.h"
#include "input/Input.h"
#include "potentials/MidasPotential.h"

namespace midas
{
namespace pes
{
#ifdef VAR_MPI
namespace mpi_impl
{

/**
 * Structure to hold different kinds of data needed on the SLAVES
 * when doing a PES calculation.
 **/
struct SlaveData
{
   //! Pes Info
   std::unique_ptr<PesInfo> mPesInfo;
   //! Molecular data
   std::unique_ptr<molecule::MoleculeInfo> mMolecule;
   //! Properties (normally V) operators used in ADGA
   std::vector<std::unique_ptr<MidasPotential>> mProperties;
   //! The current subsystem number
   In mSubSystem;
};

/**
 * Update pes level information.
 *
 * @param  aCommunicator   The communicator defining the master/slave ranks.
 **/
void UpdateLevelInfoSlave
   (  const mpi::Communicator& aCommunicator
   ,  const molecule::MoleculeInfo& aMolecule
   ,  MultiLevelInfo& aMultiLevelInfo
   )
{
   // Create an MPI Blob to receive data from master node.
   mpi::Blob blob(1024);
   blob.Bcast(aCommunicator);
   
   // Parse the data.
   In level;
   blob.Unload(reinterpret_cast<char*>(&level), sizeof(In));
   std::string subdir;
   blob.Unload(subdir);
   
   // Then update multilevel info with received data.
   aMultiLevelInfo.Update(gPesCalcDef, level, subdir, aMolecule.IsLinear());
}

/**
 * Start reading molecular data from file.
 * This needs to be synced over all MPI processes,
 * as MASTER will read the file and send it out as a string to all SLAVES.
 *
 * @param aCommunicator  The communiator defining master/slaves processes.
 * @param aMolecule      On output will hold the read-in molecule.
 **/
void MoleculeDataSlave
   (  const mpi::Communicator& aCommunicator
   ,  SlaveData& aSlaveData 
   )
{
   aSlaveData.mMolecule = std::unique_ptr<molecule::MoleculeInfo>
   {  new molecule::MoleculeInfo
         (  gSystemDefs[aSlaveData.mSubSystem].GetMoleculeFileInfo()
         ) 
   };
}

/**
 *
 **/
void CreatePesInfo
   (  SlaveData& aSlaveData
   )
{ 
   auto subsystem_name     = "subsystem_" + std::to_string(aSlaveData.mSubSystem);
   auto subsystem_main_dir = gMainDir + "/System/" + subsystem_name;
   aSlaveData.mPesInfo = std::unique_ptr<PesInfo>{new PesInfo(gPesCalcDef, *(aSlaveData.mMolecule), subsystem_main_dir, subsystem_name)};
}

/**
 * Read in potential from file.
 *
 * @param aCommunicator The communicator defining master/slave processes.
 * @param aPotential    The potential we are reading into.
 **/
void ReadInPotentialSlave
   (  const mpi::Communicator& aCommunicator
   ,  std::vector<std::unique_ptr<MidasPotential>>& aProperties
   ,  std::unique_ptr<molecule::MoleculeInfo>& aMolecule
   )
{
   mpi::Blob blob(1024);
   blob.Bcast(aCommunicator);
   
   decltype(aProperties.size()) size = -1;
   blob.Unload(reinterpret_cast<char*>(size), sizeof(size));

   std::string filename;
   for (Uin i = 0; i < size; ++i)
   {  
      blob.Unload(filename);
      if (i < aProperties.size())
      {
         aProperties[i]->CleanAndReread(filename, aMolecule->GetModeLabels());
      }
      else
      {
         aProperties.emplace_back(new MidasPotential(filename, aMolecule->GetModeLabels()));
      }
   }
}

/**
 * Slave driver function for PES MPI.
 * When in PES part of an MPI calculation all slaves will be waiting in this loop for signals from MASTER.
 *
 * @param aCommunicator  The communicator to listen on for signals.
 **/
void SlaveDriver
   (  const mpi::Communicator& aCommunicator
   )
{
   if constexpr (MPI_PES_DEBUG)
   {
      std::cout << " Rank " << aCommunicator.GetRank() << " entering pes::mpi_impl::SlaveDriver. " << std::endl;
   }
   
   // Only slave ranks are allowed to enter here.
   if(aCommunicator.IsMaster())
   {
      std::cout << " Only slave can enter pes::mpi_impl::SlaveDriver() " << std::endl;
      return;
   }

   // create slave data
   SlaveData slave_data;

   // Receive and perform tasks until signal::STOP is received
   bool running = true;
   while(running)
   {
      // receive command (blocking)
      int task = mpi::ReceiveSignal(aCommunicator);
      
      // perform received command
      switch(task)
      {
         case signal::HELLO:
         {
            break;
         }
         case signal::STOP:
         {
            if constexpr (MPI_PES_DEBUG)
            {
               std::cout << " Stopping rank " << aCommunicator.GetRank() << " for PES module. " << std::endl;
            }
            
            running = false;
            break;
         }
         case signal::UPDATE_LEVEL_INFO:
         {
            UpdateLevelInfoSlave(aCommunicator, *slave_data.mMolecule, const_cast<MultiLevelInfo&>(slave_data.mPesInfo->GetMultiLevelInfo()));
            break;
         }
         case signal::CALCULATE_SINGLEPOINTS:
         {
            CalculationList::RunSinglePointsSlave(aCommunicator, *slave_data.mMolecule, *slave_data.mPesInfo);
            break;
         }
         case signal::MOLECULEDATA:
         {
            // Get data on the subsystem number
            midas::mpi::detail::WRAP_Bcast(&slave_data.mSubSystem, 1, midas::mpi::DataTypeTrait<In>::Get(), midas::mpi::MasterRank(), aCommunicator.GetMpiComm());
            
            //
            MoleculeDataSlave(aCommunicator, slave_data);
            
            // Adjust
            AdjustMolecularInput(gPesCalcDef, *(slave_data.mMolecule), slave_data.mSubSystem);
            
            //
            CreatePesInfo(slave_data);
            
            break;
         }
         case signal::READINPOTENTIAL:
         {
            //ReadInPotentialSlave(aCommunicator, slave_data.mProperties, slave_data.mMolecule);
            break;
         }
         case signal::ADGAFINALIZE:
         {
            // Read in the complete operator for use in later possible vibrational calculations (slaves only)
            for (const auto& vscf_calc_def : gVscfCalcDef)
            {
               const auto& oper_name = vscf_calc_def.Oper();
               const auto& i_oper    = gOperatorDefs.GetOperatorNr(oper_name);

               gOperatorDefs[i_oper].ReReadIn();
            }
            break;
         }
         default:
         {
            MIDASERROR("SLAVE RECIEVED UNKNOWN SIGNAL");
         }
      }
   }

   
   if constexpr (MPI_PES_DEBUG)
   {
      std::cout << " Rank " << aCommunicator.GetRank() << " leaving pes::mpi_impl::SlaveDriver. " << std::endl;
   }
}

} /* namespace mpi_impl */

#else

namespace mpi_impl
{

/**
 * Empty SlaveDriver for when we do not have MPI (this will never be called, but needed for compilation).
 **/
void SlaveDriver
   (  const mpi::Communicator& aCommunicator
   )
{
}

} /* namespace mpi_impl */

#endif /* VAR_MPI */

/**
 * Call on MASTER rank to initialize MPI for PES.
 * This will signal the SLAVES to go the PES MPI loop
 * in the SlaveDriver() function above.
 *
 * @param  aCommunicator     The communicator to send the signal on.
 *
 * @return  Will return a guard, which when destroyed will signal slaves to STOP the PES loop and return to main waiting loop.
 **/
mpi::Guard InitMpi
   (  const mpi::Communicator& aCommunicator
   )
{
   mpi::SendSignal(aCommunicator, mpi::signal::PES);
   
   int stop_signal =  mpi_impl::signal::STOP;

   return mpi::Guard 
      (  [&aCommunicator, stop_signal]()
         {
            mpi::SendSignal(aCommunicator, stop_signal);
         }
      );
}

} /* namespace pes */
} /* namespace midas */
