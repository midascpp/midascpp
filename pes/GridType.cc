/**
************************************************************************
*
* @file                GridType.cc
*
* Created:             06-11-2006
*
* Authors:             Jacob kongsted 
*
* Short Description:
*
* Last modified: Tue Jun 08, 2010  03:21PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#include "GridType.h"

// std headers
#include <vector>
#include <string>
#include <iostream>

// midas headers
#include "util/Io.h"
#include "util/conversions/FromString.h"

/**
 * update max mc level if modecombi is larger that the current largest
 * @param aModeCombi the mode combi to check
 **/
void GridType::UpdateMaxMcLevel
   ( const std::vector<In>& aModeCombi
   )
{
   int size = aModeCombi.size();
   if(size > mMaxMcLevel)
   {
      mMaxMcLevel = size;;
   }
}

/**
 * Find max element (abs) in a vector of In
 * @param arVectorOfIn vector to search for max
 **/
In GridType::GetMax
   ( const std::vector<In>& arVectorOfIn
   ) const
{
   In max = I_0;
   for (auto& elem : arVectorOfIn)
   {
      auto abs_val = std::abs(elem);
      if (abs_val > max) max = abs_val;
   }
   return max;
}

/**
 * Return a vector of In holding the Max. no of grid points at each MC level,
 * i.e. vec = (0M,1M,2M,3M,...) where 
 *    - 1M is the Max number of GP for 1-Mode potentials, 
 *    - 2M is the Max number of GP for 2-Mode potentials,
 * and so on.
 **/
std::vector<In> GridType::MaxInEachDim
   (
   ) const
{
   std::vector<In> max_in_each_dim(mMaxMcLevel + 1, 0); // init to zero
   
   for (auto& elem : mGridType)
   {
      In idx = std::get<1>(elem).size();
      In max = this->GetMax(std::get<1>(elem));
      if (max > max_in_each_dim[idx])
      {
         max_in_each_dim[idx] = max;
      }
   }
   return max_in_each_dim;
}

/**
 * add grid type nad return index
 * @param aModeCombi the modecombi to add
 * @param aGridPoints the grid point for the mode combi
 * @param aFraction the fraction of the grid points
 **/
In GridType::AddGridType
   ( const std::vector<In>& aModeCombi
   , const std::vector<In>& aGridPoints
   , const std::vector<std::string>& aFraction
   )
{
   for(auto& elem : mGridType)
   {
      if(std::get<0>(elem) == aModeCombi)
         MIDASERROR("ModeCombi already present in gridtype");
   }
   mGridType.emplace_back(aModeCombi, aGridPoints, aFraction);
   this->UpdateMaxMcLevel(aModeCombi);
   return mGridType.size() - 1; // return address
}

/**
 * update grid for specific modecombi
 * @param aModeCombi modecombi to update
 * @param aGridPoints updated grid points
 * @param aFractions updated fractions
 **/
void GridType::UpdateGridType
   ( const std::vector<In>& aModeCombi
   , const std::vector<In>&  aGridPoints
   , const std::vector<std::string>& aFractions
   )
{
   for(auto& elem : mGridType)
   {
      if(std::get<0>(elem) == aModeCombi) // if we find modecombi we update
      {
         std::get<1>(elem) = aGridPoints; // update grid points
         std::get<2>(elem) = aFractions; // update fractions
         return; // we found modecomb so we return
      }
   }
   MIDASERROR("Modecombi not found"); // modecombi was not found (11/09/15: in debuggin phase so we throw an error)
}

/**
 * get address for modecombi
 * @param aModeCombi the modecombi to get address for
 **/
In GridType::GridAddress
   ( const std::vector<In>& aModeCombi
   ) const
{
   In i = 0;
   for(auto& elem : mGridType)
   {
      if(std::get<0>(elem) == aModeCombi)
      {
         return i; // mode combi found, we return address
      }
      ++i;
   }
   MIDASERROR("Mode combi not found");
   return -1;
}

/**
* Prints (vector of) Fractions at specific address
* @param arI
* */
void GridType::PrintSpecificFraction
   ( In arI
   )
{
   if (arI<I_0) return;
   bool not_last = true;
   Mout << "(";
   for (In j = I_0; j < std::get<2>(mGridType[arI]).size(); ++j)
   {
      Mout << std::get<2>(mGridType[arI])[j];
      if (std::get<2>(mGridType[arI]).size()-I_1 == j) not_last = false;
      if (not_last) Mout << ",";
   }
   Mout << ")";
   Mout << endl;
}

/**
* Tjeck if the Fractioning is possible for the specific grid, i.e. we wish to reuse the "iner" grid poinrs not to create new ones !
* @param arI
* @param arMode
* */
void GridType::CheckFractions
   ( In arI
   , vector<In> arMode
   )
{
   // (Number of grid points)/2 should be consistent with the Fractioning factor so that no new grid points have to be evaluated
   // The mode vector and the gridpoint vector have dimension dim.
   // The fractioning have dimension 2*dim due to Left and Right fractioning. Therefor these vectors are accessed in different ways.
   In top = I_1;
   In bottom = I_1;
   if (I_2*std::get<1>(mGridType[arI]).size() != std::get<2>(mGridType[arI]).size()) 
   {
      MIDASERROR(" Wrong dimension in CheckFractions");
   }
   In dim = std::get<1>(mGridType[arI]).size();

   // We need to tjeck both the Left and Rigth Fractionings
   In Ibeg = I_0;
   In Iend = I_0;

   for (In k=I_0;k<I_2;++k)
   {
      if (k == I_0)
      {
         Ibeg = I_0;
         Iend = dim;
      }
      else if (k == I_1)
      {
         Ibeg = dim;
         Iend = I_2*dim;
      }

      In i_grid = I_0;     // used to access the grid point 

      for (In i=Ibeg;i<Iend;++i)
      {
         if(std::get<2>(mGridType[arI])[i].find("/") != string::npos) // we have a fraction
         {
            string s = std::get<2>(mGridType[arI])[i];
            top = midas::util::FromString<In>(s.substr(0,s.find("/")));
            bottom = midas::util::FromString<In>(s.substr(s.find("/")+1));
         }
         else // we have a number (In)
         {
            string s = std::get<2>(mGridType[arI])[i];
            top = midas::util::FromString<In>(s);
         }
         if ( ((std::get<1>(mGridType[arI])[i_grid]/I_2)*top)%bottom != I_0)  // notice the use of i_grid to access the grid points
         {
            std::string error = " For mode " + std::to_string(arMode[i_grid]) ;
                              //+  " in MC " + std::string(arMode) 
                              //+ " Fraction " +  std::to_string(mGridType[arI].second[i]) 
                              //+ " is not possible with " + std::string(mGridType[arI].first[i_grid]) 
                              //+ " grid points ";
            MIDASERROR(error); // notice the use of i_grid to access the grid points and mode. 
         }
         ++i_grid;
      }
   }
}
