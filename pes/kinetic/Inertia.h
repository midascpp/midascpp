/**
************************************************************************
*
* @file                Inertia.h
*
* Created:             01/17/2007
*
* Author:              Daniele Toffoli
*
* Short Description:   Declares class Inertia for
*                      computation of Inertia moments
*
* Last modified: Wed Jun 09, 2010  02:50PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef INERTIA_H
#define INERTIA_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "util/Io.h"

/**
 * Declarations:
**/
class CoriolisCoeffs;

class Inertia
{
   private:
      MidasVector mMoments;                  ///< Principal Inertia moments
      MidasMatrix mImat;                     ///< Inertia tensor
      MidasMatrix mImatCorr;                 ///< Coriolis corrected inertia tensor
      MidasMatrix mImatCorrInv;              ///< Inverse tensor of inertia (Coriolis corrected)
      vector<Nuclei> mGeomRef;               ///< Reference molecular geometry
      Nb mMuVal;                             ///< linear case: mu
      Nb mIref;                              ///< for linear molecules
      MidasVector mAvec;                     ///< store a_k coefficients for linear molecules.
      bool mIsLinear;                        ///< Flag for linear case

   public:
      Inertia(bool aIsLinear=false);                                     ///< constructor
      void SetIsLinear(bool aIsLinear=true){mIsLinear=aIsLinear;}      ///< set linear case
      void SetReference(const vector<Nuclei>& arRef){mGeomRef=arRef;}         ///< set reference geometry
      void GetReference(vector<Nuclei>& arRef) const {arRef=mGeomRef;}        ///< get reference geometry
      bool IsLinear() {return mIsLinear;}                                ///< is linear?
      void CalcAvec(const MidasMatrix& arNormalQ);                             ///< calculate a_k coefficients
      void SetIref();                                                    ///< I_0 for linear molecules
      Nb GetIref() {return mIref;}                                       ///< return I_0
      void GenImatInv(const vector<Nuclei>& arCoord, const vector<Nuclei>& arRefCoord,const MidasVector& arQ,
                       const MidasMatrix& arNormalQ, const CoriolisCoeffs* arCoriolisObj);
                                                                         ///< Driver 
      Nb GetImatCorrInv(const In& aI, const In& aJ);     ///< return a specific element of the tensor    
      Nb GetMuTrace();
      Nb GetMuVal() {return mMuVal;}  ///< return the mu function
      In GetNmodes() {return mAvec.Size();}  ///< Get the number of modes
      void CalcImat(const vector<Nuclei>& arGeom);   ///< compute mTensorOfInertia
      void CalcImatCorr(const MidasVector& arQ, const CoriolisCoeffs* arCoriolisObj);
      ///< compute mTensorOfInertiaCorr
      void CalcImatCorrInv();                                ///< compute mTensorOfInertiaCorrInv
      void CalcMuVal(const MidasVector& arQ);                ///< compute mu for the linear case
      void WriteInfoOnFile(string& arFileName);                             ///< dump info on file 
      void DumpInertia(string& arFileName);                                      ///< Dump moment of inertia tensor to file
      friend ostream& operator<<(ostream& arOut, const Inertia& arInertiaObj); ///< overload for << operator
      friend void CheckIdentity(Inertia& arInertiaObj);
      friend Nb MultiplyColByVec(const In aIComp, const In aIcol, const MidasVector& arVec, const CoriolisCoeffs* CoriolisObj);
                  ///< Extract the aIrow and multiply by an external vector arVec
};

//! Extract the aIrow and multiply by an external vector arVec
Nb MultiplyColByVec(const In aIComp, const In aIcol, const MidasVector& arVec, const CoriolisCoeffs* CoriolisObj);

//! overload for << operator
ostream& operator<<(ostream& arOut, const Inertia& arInertiaObj); 

//!
void CheckIdentity(Inertia& arInertiaObj);

#endif /* INTERTIA_H */
