/**
************************************************************************
*
* @file                Inertia.cc
*
* Created:             01/17/2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Define class for computation of Inertia moments
*
* Last modified: Wed Jun 09, 2010  02:52PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/


// std headers
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/kinetic/Inertia.h"
#include "pes/kinetic/CoriolisCoeffs.h"

// using declarations
using std::vector;

/**
 * Local Declarations
**/
typedef string::size_type Sst;

/**
 * Constructor
**/
Inertia::Inertia
   (  bool aIsLinear)
   :  mMoments(I_0)
   ,  mImat(I_0, C_0)
   ,  mImatCorr(I_0, C_0)
   ,  mImatCorrInv(I_0, C_0)
   ,  mGeomRef()
   ,  mAvec(I_0)
{
   mMuVal = C_0;
   mIref = I_0;
   SetIsLinear(aIsLinear);
   mMoments.SetNewSize(I_3, false);
   mImat.SetNewSize(I_3, false, true);
}

/**
 * Set the equilibrium moment of inertia
 * for linear molecules (it assumes the molecules
 * is oriented along the Z axis!!)
**/
void Inertia::SetIref()
{
   vector<Nuclei> ref_geom;
   GetReference(ref_geom);
   In n_nuc=ref_geom.size();
   if (gDebug)
   {
      Mout << " n_nuc: " << n_nuc << endl;
      Mout << " geometry info in SetIref.." << endl;
      for (In i_nuc=I_0; i_nuc<n_nuc;i_nuc++)
         Mout << ref_geom[i_nuc] << endl;
      Mout << endl;
   }
   //check orientation
   bool is_oriented=true;
   Nb thresh=C_NB_EPSILON;
   //Mout << " thresh= " << thresh << endl; 
   for (In i_nuc=I_0; i_nuc<ref_geom.size(); i_nuc++)
   {
      Nb x_coord=ref_geom[i_nuc].X();
      Nb y_coord=ref_geom[i_nuc].Y();
      if ((x_coord>thresh) || (y_coord>thresh)) is_oriented=false;
      if (!is_oriented) break;
   }
   if (!is_oriented) MIDASERROR(" Reference geometry not properly oriented! ");
   //find center of mass
   MidasVector rg_coord(I_3,C_0);
   Nb total_mass=C_0;
   for (In i_nuc=I_0; i_nuc<ref_geom.size(); i_nuc++)
   {
      MidasVector nuc_coord(I_3,C_0);
      nuc_coord[0]=ref_geom[i_nuc].X();
      nuc_coord[1]=ref_geom[i_nuc].Y();
      nuc_coord[2]=ref_geom[i_nuc].Z();
      Nb mass = ref_geom[i_nuc].GetMass();
      rg_coord[0]+=mass*nuc_coord[0];
      rg_coord[1]+=mass*nuc_coord[1];
      rg_coord[2]+=mass*nuc_coord[2];
      total_mass+=mass;
   }
   rg_coord.Scale(C_1/total_mass);
   //debug
   //Mout << " CM coordinates: " << rg_coord[0] << " " << rg_coord[1] << " " << rg_coord[2] << endl;
   Nb val=I_0;
   for (In i_nuc=0; i_nuc<n_nuc; i_nuc++)
   {
      Nb z_c=C_0;
      z_c=ref_geom[i_nuc].Z()-rg_coord[2];
      Nb mass = ref_geom[i_nuc].GetMass();
      mass*=C_FAMU;
      val+=mass*pow(z_c,C_2);
   }
   //Mout << " val: " << val << endl;
   mIref=val;
   return;
}

/**
 * Driver: calculate the mu tensor (non linear case) 
 * or the mu function (linear case)
**/
void Inertia::GenImatInv
   (  const vector<Nuclei>& arCoord
   ,  const vector<Nuclei>& arRefCoord
   ,  const MidasVector& arQ
   ,  const MidasMatrix& arNormalQ 
   ,  const CoriolisCoeffs* arCoriolisObj
   )
{
   if (gPesIoLevel > I_10)
   {
      Mout << " Generate the inverse effective inertia tensor for a given structure " << std::endl;
   }
   if (!IsLinear())
   {
      CalcImat(arCoord);
      mImatCorr.SetNewSize(I_3, false, true);
      CalcImatCorr(arQ, arCoriolisObj);
      mImatCorrInv.SetNewSize(I_3, false, true);
      CalcImatCorrInv();
   }
   else
   {
      //linear case.
      CalcImat(arCoord);
      SetReference(arRefCoord);
      SetIref();
      CalcAvec(arNormalQ);
      CalcMuVal(arQ);
   }
   return;
}

/**
 * Calculate the mu function for linear molecules.
 * Follow formula (45) of Mol. Phys. 4(1970), 465-487.  (vol. 19 not 4 ??)
**/
void Inertia::CalcMuVal(const MidasVector& arQ)
{
   //debug
   MidasVector q_vec=arQ;
   //Mout << " values of q coord: " << q_vec << endl;
   //get I_0
   Nb i_zero=GetIref();
   Mout << " i_zero: " << i_zero << endl;
   In n_modes=GetNmodes();
   //Mout << " n_modes: " << n_modes << endl;
   if (n_modes==I_0) 
   {
      MIDASERROR(" mAvec vector empty! ");
   }

   Nb i_pp=C_0;
   for (In i_mode=I_0; i_mode< n_modes; i_mode++)
      i_pp+=C_I_2*mAvec[i_mode]*q_vec[i_mode];
   i_pp+=i_zero;
   mMuVal=i_zero/(pow(i_pp,C_2));
   
   return;
}

/**
 * Calculate the a_k coefficients for linear molecules.
 **/
void Inertia::CalcAvec(const MidasMatrix& arNormalQ)
{
   vector<Nuclei> ref_geom;
   GetReference(ref_geom);
   In n_nuc=ref_geom.size();
   //construct the a_k coefficients according to Eq.(31)
   if (gDebug)
   {
      Mout << " reference geometry " << endl;
      Mout << " n_nuc: " << n_nuc << endl;
      for (In i=I_0; i<ref_geom.size(); i++)
         Mout << ref_geom[i] << endl;
      Mout << endl;
      Mout << " masses in amu " << endl;
      midas::stream::ScopedPrecision(22, Mout);
      Mout.setf(ios::scientific);
      Mout.setf(ios::uppercase);
      for (In i=I_0; i<ref_geom.size(); i++)
      {
         Mout << setw(30) << ref_geom[i].GetMass() << endl;
      }
   }
   In n_modes=arNormalQ.Nrows();
   //Mout << " n_modes: " << n_modes << endl;
   MidasMatrix scaled_normal_coord(n_modes,3*n_nuc);
   MidasVector mass_vector(3*n_nuc,C_0);
   In k=0;
   for (In i=0; i<n_nuc; i++)
   {
      Nb mass = ref_geom[i].GetMass();
      mass_vector[k] = sqrt(mass);
      mass_vector[k+1] = sqrt(mass);
      mass_vector[k+2] = sqrt(mass);
      k+=3;
   }
   if (gDebug)
   {
      Mout << " mass_vector " << endl;
      midas::stream::ScopedPrecision(22, Mout);
      Mout.setf(ios::scientific);
      Mout.setf(ios::uppercase);
      for (In i=0;i<3*n_nuc; i++)
      {
         Mout << " mass_vector[" << i << "] = " << setw(30) << mass_vector[i] << endl;
      }
   }
   MidasVector k_mode(3*n_nuc);
   MidasVector l_mode(3*n_nuc);
   scaled_normal_coord.Zero();
   for (In i=0; i<n_modes; i++)
      for (In j=0; j<3*n_nuc; j++)
         scaled_normal_coord[i][j] = arNormalQ[i][j]*mass_vector[j];
   
   //check orthogonality
   CheckRowUnitarity(scaled_normal_coord);
   mAvec.SetNewSize(n_modes);
   mAvec.Zero();
   for (In k=0; k<n_modes; k++)
   {
      k_mode.Zero();
      scaled_normal_coord.GetRow(k_mode,k);
      In i_c=I_0;
      for (In i_nuc=0; i_nuc<n_nuc; i_nuc++)
      {
         Nb nuc_mass=ref_geom[i_nuc].GetMass();
         nuc_mass*=C_FAMU;
         Nb z_c=ref_geom[i_nuc].Z();
         mAvec[k]+=sqrt(nuc_mass)*z_c*k_mode[i_c+I_2];
         i_c+=I_3;
      }
   }
   mAvec.Scale(C_2);
   if (gDebug)
   {
      Mout << " a_k vector: " << endl;
      Mout << mAvec << endl;
   }
   return;
}

/**
 * Compute and store the tensor of Inertia for the structure given in input
 **/
void Inertia::CalcImat
   (  const vector<Nuclei>& arRefCoord
   )
{
   if (gPesIoLevel > I_10 || gDebug)
   {
      Mout << " Geometry info given to CalcImat: " << std::endl;
      for (In i_nuc = I_0; i_nuc < arRefCoord.size(); i_nuc++)
      {
         Mout << arRefCoord[i_nuc] << std::endl;
      }
      Mout << std::endl;
   }
   //find center of mass
   MidasVector rg_coord(I_3, C_0);
   Nb total_mass = C_0;
   for (In i_nuc = I_0; i_nuc < arRefCoord.size(); i_nuc++)
   {
      MidasVector nuc_coord(I_3, C_0);
      nuc_coord[I_0] = arRefCoord[i_nuc].X();
      nuc_coord[I_1] = arRefCoord[i_nuc].Y();
      nuc_coord[I_2] = arRefCoord[i_nuc].Z();
      Nb mass = arRefCoord[i_nuc].GetMass();
      rg_coord[I_0] += mass*nuc_coord[I_0];
      rg_coord[I_1] += mass*nuc_coord[I_1];
      rg_coord[I_2] += mass*nuc_coord[I_2];
      total_mass += mass;
   }
   rg_coord.Scale(C_1/total_mass);
   
   if (gPesIoLevel > I_10 || gDebug)
   {
      Mout << " Calculated center-of-mass coordinates are: " << std::endl << "  " << rg_coord[I_0] << " " << rg_coord[I_1] << " " << rg_coord[I_2] << std::endl;
   }

   mImat.Zero();
   for (In i_nuc = I_0; i_nuc < arRefCoord.size(); i_nuc++)
   {
      MidasVector nuc_coord(I_3, C_0);
      nuc_coord[I_0] = arRefCoord[i_nuc].X() - rg_coord[I_0];
      nuc_coord[I_1] = arRefCoord[i_nuc].Y() - rg_coord[I_1];
      nuc_coord[I_2] = arRefCoord[i_nuc].Z() - rg_coord[I_2];
      Nb dist = nuc_coord.Norm2();
      Nb mass = arRefCoord[i_nuc].GetMass();
      mass *= C_FAMU;
      for (In i = I_0; i < I_3; i++)
      {
         Nb tmp = mass*dist;
         mImat[i][i] += tmp;
         for (In j = I_0; j <= i; j++)
         {
            Nb tmp1 = mass*nuc_coord[i]*nuc_coord[j];
            mImat[i][j] -= tmp1;
         }
      }
   }

   //symmetrize
   for (In i = I_0; i < I_3; i++)
   {
      for (In j = I_0; j < i; j++)
      {
         Nb tmp = mImat[i][j];  
         mImat[j][i] = tmp;
      }
   }
   MidasMatrix eig_vec(I_3, C_0);
   MidasVector eig_val(I_3, C_0);
   string diag_method; 
   if (gNumLinAlg == "LAPACK")
   {
      diag_method = "DSYEVD";
   }
   else
   {
      diag_method = "MIDAS_JACOBI";
   }
   Diag(mImat, eig_vec, eig_val, diag_method, true);
   mMoments = eig_val;
}

/**
 * Compute and store the Coriolis corrected tensor of Inertia
 **/
void Inertia::CalcImatCorr
   (  const MidasVector& arQ
   ,  const CoriolisCoeffs* arCoriolisObj
   )
{
   In local_size=arQ.Size();
   MidasVector vec_col(local_size,C_0);
   mImatCorr=mImat;

   for (In i=0; i<3; i++)
   {
      for (In j=0; j<3; j++)
      {
         for (In m=0; m<local_size; m++)
         {
            Nb left_fact=MultiplyColByVec(i, m, arQ, arCoriolisObj);
            Nb right_fact=MultiplyColByVec(j, m, arQ, arCoriolisObj);
            mImatCorr[i][j]-=left_fact*right_fact;
         }
      }
   }
   //check if it is symmetric
   bool is_symmetric=false;
   is_symmetric=mImatCorr.IsSymmetric();
   if (!is_symmetric)
   {
      MIDASERROR(" Tensor of inertia Coriolis corrected is not symmetric "); 
   }
   return;
}

/**
 * Compute and store the inverse of the tensor of inertia (Coriolis corrected)
**/
void Inertia::CalcImatCorrInv()
{
   MidasMatrix mat_sq(I_3,C_0);
   MidasMatrix mat_unit(I_3,C_0);
   MidasMatrix mat_copy(I_3,C_0);
   //use an esplicit formula for the inverse
   Nb mat_det = mImatCorr[I_0][I_0]*(mImatCorr[I_1][I_1]*mImatCorr[I_2][I_2]-
                mImatCorr[I_1][I_2]*mImatCorr[I_2][I_1]) -mImatCorr[I_0][I_1]*
               (mImatCorr[I_1][I_0]*mImatCorr[I_2][I_2]-mImatCorr[I_1][I_2]*mImatCorr[I_2][I_0])
               +mImatCorr[I_0][I_2]*(mImatCorr[I_1][I_0]*mImatCorr[I_2][I_1]-
                mImatCorr[I_1][I_1]*mImatCorr[I_2][I_0]);

   //look if it is singular
   if (mat_det < C_NB_EPSILON*C_10_2)
   {
      MIDASERROR(" Singular matrix mImatCorr ");
   }

   Nb trace = mImatCorr.Trace();
   mat_copy=mImatCorr;
   mat_sq = mImatCorr*mat_copy;
   Nb trace_sq = mat_sq.Trace();
   mat_copy.Scale(trace);
   mat_unit.Unit();
   mat_unit.Scale(((pow(trace,C_2)-trace_sq)/C_2));
   mImatCorrInv+=mat_unit;
   mImatCorrInv-=mat_copy;
   mImatCorrInv+=mat_sq;
   mImatCorrInv.Scale((C_1/mat_det));
}

/**
 * Dump inverse of Inertia matrix (Coriolis corrected) into a file
 *
 * @param arFileName
 *    The file name to which information should be dumped.
**/
void Inertia::DumpInertia(string& arFileName)
{
   ofstream out_file(arFileName.c_str(), ios_base::out);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   out_file.precision(I_22);

   out_file << mImat; 

   out_file.close();
}

/**
 * Dump inverse of Inertia matrix (Coriolis corrected) into a file
**/
void Inertia::WriteInfoOnFile(string& arFileName)
{
   ofstream out_file(arFileName.c_str(),ios_base::out);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, out_file);
   if (!IsLinear())
   {
      out_file << " XX   " << mImatCorrInv(I_0,I_0) << endl;
      out_file << " XY   " << mImatCorrInv(I_0,I_1) << endl;
      out_file << " XZ   " << mImatCorrInv(I_0,I_2) << endl;
      out_file << " YX   " << mImatCorrInv(I_1,I_0) << endl;
      out_file << " YY   " << mImatCorrInv(I_1,I_1) << endl;
      out_file << " YZ   " << mImatCorrInv(I_1,I_2) << endl;
      out_file << " ZX   " << mImatCorrInv(I_2,I_0) << endl;
      out_file << " ZY   " << mImatCorrInv(I_2,I_1) << endl;
      out_file << " ZZ   " << mImatCorrInv(I_2,I_2) << endl;
   }
   else
   {
      out_file << " MU_FUNC " << mMuVal << endl;
   }
   out_file.close();
}

/**
 * Overloaded output stream for class Inertia
**/
ostream& operator<<
   (  ostream& arOut
   ,  const Inertia& arInertiaObj
   )
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(ios::showpoint);

   arOut << " Tensor of Inertia "  << endl;
   arOut << "              X         "  <<  "             Y        "  <<   "             Z        "  << endl;
   arOut << "  X  ";
   for (In j=0; j<3; j++)
      arOut << arInertiaObj.mImat[I_0][j] << " ";
   arOut << endl;
   arOut << "  Y  ";
   for (In j=0; j<3; j++)
      arOut << arInertiaObj.mImat[I_1][j] << " ";
   arOut << endl;
   arOut << "  Z  ";
   for (In j=0; j<3; j++)
      arOut << arInertiaObj.mImat[I_2][j] << " ";
   arOut << endl << endl;
   MidasVector eig_val(I_3);
   eig_val=arInertiaObj.mMoments;
   Nb c_fau=C_TANG*C_TANG/C_FAMU;
   eig_val*=c_fau;
   arOut << " Eigenvalues (amu*AA**2) in ascending order are: " << endl;
   arOut << " eigenvalue no.1:  " << eig_val[I_0] << endl;
   arOut << " eigenvalue no.2:  " << eig_val[I_1] << endl;
   arOut << " eigenvalue no.3:  " << eig_val[I_2] << endl;
   arOut << endl;

   if (gPesIoLevel > I_10)
   {
      if (!arInertiaObj.mIsLinear)
      {
         arOut << " Tensor of Inertia Coriolis corrected " << endl;
         arOut << "              X         "  <<  "             Y        "  <<   "             Z        "  << endl;
         arOut << "  X  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorr[0][j] << " ";
         arOut << endl;
         arOut << "  Y  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorr[1][j] << " ";
         arOut << endl;
         arOut << "  Z  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorr[2][j] << " ";
         arOut << endl;
         arOut << " Inverse of tensor of Inertia corrected " << endl << endl;
         arOut << "              X         "  <<  "             Y        "  <<   "             Z        "  << endl;
         arOut << "  X  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorrInv[0][j] << " "; 
         arOut << endl;
         arOut << "  Y  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorrInv[1][j] << " ";
         arOut << endl;
         arOut << "  Z  ";
         for (In j=0; j<3; j++)
            arOut << arInertiaObj.mImatCorrInv[2][j] << " ";
         arOut << endl;
      }
      else
      {
         arOut << " Value of mu function (linear case): " << arInertiaObj.mMuVal << endl; 
      }
      arOut << endl;
   }
   return arOut;
}

/**
 * Multiply a coulmn of the given coriolis matrix by a row vector
 **/
Nb MultiplyColByVec(const In aIcomp, const In aIcol, const MidasVector& arVec, const CoriolisCoeffs* arCoriolisObj)
{
   Nb result;
   In local_size = arCoriolisObj->GetDimensions();
// check compatibility
   if (arVec.Size()!=local_size)
   {
      Mout << " Matrices not conformable in MultiplyColByVec. The Program will STOP " << endl;
      MIDASERROR(" Matrices not conformable in MultiplyColByVec ");
   }
   MidasVector mat_col(local_size);
   arCoriolisObj->GetCol(aIcol, aIcomp, mat_col);
   result=C_0;
   for (In k=0; k<local_size; k++)
      result+=mat_col[k]*arVec[k];
   return result;
}

/**
 * Get a specific element of the inverse of inertia tensor
 **/
Nb Inertia::GetImatCorrInv(const In& aI, const In& aJ)
{
   return  mImatCorrInv[aI][aJ];
}

/**
 * Get the trace of the inverse of inertia tensor
 **/
Nb Inertia::GetMuTrace()
{
   Nb trace=C_0;
   trace = mImatCorrInv.Trace();
   return trace;
}

/**
 *
 **/
void CheckIdentity(Inertia& arInertiaObj)
{
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, Mout);
   Mout.setf(ios::showpoint);
   //check out the inverse is properly computed
   MidasMatrix identity(I_3,C_0);
   MidasMatrix mat_left(I_3,C_0);
   MidasMatrix mat_right(I_3,C_0);
   mat_left=arInertiaObj.mImatCorrInv;
   mat_right=arInertiaObj.mImatCorr;
   identity=mat_left*mat_right;
   Mout << " Identity matrix  " << endl << endl;
   Mout << "              X         "  <<  "             Y        "  <<   "             Z        "  << endl;
   Mout << "  X  ";
   for (In j=0; j<3; j++)
      Mout << identity[0][j] << " ";
   Mout << endl;
   Mout << "  Y  ";
   for (In j=0; j<3; j++)
      Mout << identity[1][j] << " ";
   Mout <<  endl;
   Mout << "  Z  ";
   for (In j=0; j<3; j++)
      Mout << identity[2][j] << " ";
   Mout << endl;
}
