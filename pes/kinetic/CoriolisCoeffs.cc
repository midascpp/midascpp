/**
************************************************************************
*
* @file                CoriolisCoeffs.cc
*
* Created:             17/01/2007
*
* Author:              Daniele Toffoli
*
* Short Description:   Define class for computation of Coriolis coupling 
*                      coefficients
*
* Last modified:       25/01/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/kinetic/CoriolisCoeffs.h"

/**
 * Constructor. Will setup matrices of correct sizes, but not fill them.
 **/
CoriolisCoeffs::CoriolisCoeffs
   (  In arNoOfQmodes
   )
   :  mNrows(arNoOfQmodes)
{
   mZetaMat_X.SetNewSize(mNrows, false, true);
   mZetaMat_Y.SetNewSize(mNrows, false, true);
   mZetaMat_Z.SetNewSize(mNrows, false, true);
}

/**
 * Compute and fill in the three (X,Y, and Z) Coriolis coupling matrices
 * Use Eq. (19) of J.K.G. Watson, Mol. Phys. 5(1968), 479-490
 **/
void CoriolisCoeffs::FillCoriolisMatrices
   (  const vector<Nuclei>& arRefCoord
   ,  const MidasMatrix& arQmat
   )
{
   // Do some printout
   if (gPesIoLevel > I_10)
   {
      Mout << " Calculation of Coriolis coefficient matrices (Z matrices)            " << std::endl;
   }
   if (gPesIoLevel > I_13)
   {
      Mout << " Reference structure used in the calculation of Coriolis coefficients " << std::endl;
   
      for (In i = I_0; i < arRefCoord.size(); i++)
      {
         Mout << arRefCoord[i] << std::endl;
      }
   } 
   if (gPesIoLevel > I_10)
   {
      Mout << "Atomic masses in amu for species used in this calculation" << std::endl;
      for (In i = I_0; i < arRefCoord.size(); i++)
      {
         Mout.setf(ios::scientific);
         Mout.setf(ios::uppercase);
         Mout.precision(22);
         Mout << setw(30) << arRefCoord[i].GetMass() << std::endl;
      }
   }
   
   // Then do some work...
   In n_of_nuclei=arRefCoord.size();
   In n_of_qmodes=mNrows;
   MidasMatrix scaled_normal_coord(n_of_qmodes,3*n_of_nuclei);
   MidasVector mass_vector(3*n_of_nuclei,C_0);
   In k=0;
   for (In i=0; i<n_of_nuclei; i++)
   {
      Nb mass = arRefCoord[i].GetMass();
      mass_vector[k] = sqrt(mass);
      mass_vector[k+1] = sqrt(mass);
      mass_vector[k+2] = sqrt(mass);
      k+=3;
   }
   
   if (gPesIoLevel > I_10)
   {
      Mout << std::endl << " mass_vector in CoriolisCoeffs::FillCoriolisMatrices " << std::endl << std::endl;
      for (In i = I_0; i < I_3*n_of_nuclei; i++)
      {
         Mout.setf(ios::scientific);
         Mout.setf(ios::uppercase);
         Mout.precision(22);
         Mout << " mass_vector[" << i << "] = " << setw(30) << mass_vector[i] << std::endl;
      }
   }

   MidasVector k_mode(3*n_of_nuclei);
   MidasVector l_mode(3*n_of_nuclei);
   scaled_normal_coord.Zero();
   for (In i=0; i<n_of_qmodes; i++)
   {
      arQmat.GetRow(k_mode,i);
      for (In j=0; j<3*n_of_nuclei; j++)
      {
         scaled_normal_coord[i][j] = k_mode[j]*mass_vector[j];
      }
   }
   
   // check they satisfy common unitary (orthogonality) relationships among the rows
   CheckRowUnitarity(scaled_normal_coord);
   
   // then compute the various Coriolis coefficients
   // Zero'es may be unnecessary
   mZetaMat_X.Zero();
   mZetaMat_Y.Zero();
   mZetaMat_Z.Zero();
   for (In k=0; k<n_of_qmodes; k++)
   {
      k_mode.Zero();
      scaled_normal_coord.GetRow(k_mode,k);
      for (In l=0; l<n_of_qmodes; l++)
      {
         l_mode.Zero();
         scaled_normal_coord.GetRow(l_mode,l);
         In i_alpha=0;
         for (In i_nucl=0; i_nucl<n_of_nuclei; i_nucl++)
         {
            //X component
            mZetaMat_X[k][l]+=k_mode[i_alpha+1]*l_mode[i_alpha+2]-k_mode[i_alpha+2]*l_mode[i_alpha+1];
            //Y component
            mZetaMat_Y[k][l]+=k_mode[i_alpha+2]*l_mode[i_alpha]-k_mode[i_alpha]*l_mode[i_alpha+2];
            //Z component
            mZetaMat_Z[k][l]+=k_mode[i_alpha]*l_mode[i_alpha+1]-k_mode[i_alpha+1]*l_mode[i_alpha];
            i_alpha+=3;
         }
      }
   }

   if (gPesIoLevel > I_10)
   {
      Mout << " Coriolis Coupling coefficient Matrices constructed " << std::endl << std::endl;
   }

   // check if it is antisymmetric to a given threshold
   Nb LocalEps=C_NB_EPSILON*C_10_2;
   bool x_comp=false;
   bool y_comp=false;
   bool z_comp=false;
   x_comp=mZetaMat_X.IsAntiSymmetric(LocalEps);
   y_comp=mZetaMat_Y.IsAntiSymmetric(LocalEps);
   z_comp=mZetaMat_Z.IsAntiSymmetric(LocalEps);
   if (x_comp == false || y_comp == false || z_comp == false)
   {
      Mout << " WARNING " << std::endl;
      Mout << " Calculated Coriolis matrices are not antisymmetric. The program will STOP " << std::endl;
      MIDASERROR(" Calculated Coriolis matrices are not antisymmetric "); 
   }
   else
   {
      if (gPesIoLevel > I_10)
      {
         Mout << " Coriolis matrices display the correct symmetry " << std::endl;
      }
   }
}

/**
 * Get a particular row for the aIcomp Coriolis coupling matrix (0=X, 1=Y, 2=Z)
 **/
void CoriolisCoeffs::GetRow
   (  const In aIrow
   ,  const In aIcomp
   ,  MidasVector& arRow 
   )  const
{
   if (aIcomp == I_0)
   {
      mZetaMat_X.GetRow(arRow, aIrow);           
   }
   else if (aIcomp == I_1)
   {
      mZetaMat_Y.GetRow(arRow, aIrow);           
   }
   else if (aIcomp == I_2)
   {
      mZetaMat_Z.GetRow(arRow, aIrow);           
   }
}

/**
 * Get a particular column for the aIcomp Coriolis coupling matrix (0=X, 1=Y, 2=Z)
 **/   
void CoriolisCoeffs::GetCol
   (  const In aIcol
   ,  const In aIcomp
   ,  MidasVector& arCol
   )  const
{
   if (aIcomp == I_0)
   {
      mZetaMat_X.GetCol(arCol, aIcol);
   }
   else if (aIcomp == I_1)
   {
      mZetaMat_Y.GetCol(arCol, aIcol);
   }
   else if (aIcomp == I_2)
   {
      mZetaMat_Z.GetCol(arCol, aIcol);
   }
}

/**
 * Dump independent Coriolis coefficients to a file.
 *
 * @param arFileName   The file to dump to.
 * @param aModeNames   The mode labels to use in dump file.
 **/
void CoriolisCoeffs::DumpCoriolisCoeffsToFile
   (  const std::string& arFileName
   ,  const std::vector<std::string>& aModeNames
   )  const
{
   ofstream OutFile(arFileName.c_str(), ios_base::out);
   OutFile.setf(ios::scientific);
   OutFile.setf(ios::uppercase);
   OutFile.precision(22);
   // dump X matrix
   In i_alpha = I_1; //compatibility with older implementations
   In is = I_0;
   In js = I_0;
   for (In i = I_0; i < mZetaMat_X.Nrows(); i++)
   {
      for (In j = I_0; j < i; j++)
      {
         OutFile << i_alpha << "  " << aModeNames[i] << "  " << aModeNames[j] << "  " << mZetaMat_X[i][j]  << endl;
      }
   }
   // dump Y matrix
   i_alpha = I_2;
   for (In i = I_0; i < mZetaMat_Y.Nrows(); i++)
   {
      for (In j = I_0; j < i; j++)
      {
         OutFile << i_alpha << "  " << aModeNames[i] << "  " << aModeNames[j] << "  "  << mZetaMat_Y[i][j]  << endl;
      }
   }
   // dump Z matrix
   i_alpha = I_3;
   for (In i = I_0; i < mZetaMat_Z.Nrows(); i++)
   {                
      for (In j = I_0; j < i; j++)
      {                                         
         OutFile << i_alpha << "  " << aModeNames[i] << "  " << aModeNames[j] << "  " << mZetaMat_Z[i][j]  << endl;
      }                                                                
   }
   OutFile.close();
}

/**
 * Overloaded output stream for class Coriolis Couplings
 **/
ostream& operator<<
   (  ostream& arOut
   ,  CoriolisCoeffs& arCoriolisObj
   )
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, arOut);
   arOut.setf(ios::showpoint);

   arOut << " Matrices of Coriolis coupling coefficients (alpha = X, Y, Z) " << std::endl;
   arOut << " X component " << std::endl;
   arOut << arCoriolisObj.mZetaMat_X;
   arOut << " Y component " << std::endl;
   arOut << arCoriolisObj.mZetaMat_Y;
   arOut << " Z component " << std::endl;
   arOut << arCoriolisObj.mZetaMat_Z;
   return arOut;
}

/**
 * Check unitary properties along the rows of the matrix of normal coordinates in input.
 **/
void CheckRowUnitarity
   (  MidasMatrix& arQmat
   )
{
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, Mout);
   Nb LocalEps=C_NB_EPSILON;
   In n_rows=arQmat.Nrows();
   In n_cols=arQmat.Ncols();
   MidasVector k_mode(n_cols);
   MidasVector l_mode(n_cols);
   Nb dot_prod;
   for (In i = I_0; i < n_rows; i++)
   {
      k_mode.Zero();
      arQmat.GetRow(k_mode, i);
      for (In j = i; j < n_rows; j++)
      {
         l_mode.Zero();
         arQmat.GetRow(l_mode, j);
         dot_prod = C_0;
         for (In k = I_0; k < n_cols; k++)
         {
            dot_prod += k_mode[k]*l_mode[k];
         }
         if (gPesIoLevel > I_13)
         {
            if (i == j && fabs(dot_prod - C_1) > LocalEps)
            {
               Mout << " Norm of row " << i << " is: " << setw(30) << dot_prod << std::endl;
            }
            else if (i != j && std::fabs(dot_prod) > LocalEps)
            {
               Mout << " Dot product between rows " << i << " and " << j << " is: " << setw(30) << dot_prod << std::endl;
            }
         }
      }
   }
}
