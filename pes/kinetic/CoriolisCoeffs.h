/**
************************************************************************
*
* @file                CoriolisCoeffs.h
*
* Created:             15-01-2007
*
* Author:              Daniele Toffoli
*
* Short Description:   Declares class CoriolisCouplings for
*                      computation of Coriolis couplings coefficients
*
* Last modified:       25-01-2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef CORIOLISCOUPLINGS_H_INCLUDED
#define CORIOLISCOUPLINGS_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"

/**
* Declarations:
**/
class CoriolisCoeffs
{
   private:
      //! Dimension of coriolis matrices
      In mNrows;
      //! Zeta-x
      MidasMatrix mZetaMat_X;
      //! Zeta-y
      MidasMatrix mZetaMat_Y;
      //! Zeta-z
      MidasMatrix mZetaMat_Z;

   public:
      //! Constructor
      CoriolisCoeffs(In aNrows);

      //! Calculate the coriolis matrices
      void FillCoriolisMatrices(const std::vector<Nuclei>& arRefCoord, const MidasMatrix& arQmat);  
      
      //! Get the dimension of the coriolis matrices
      In GetDimensions() const { return mNrows; }                                         
      
      //! Get a row of the coriolis matrices
      void GetRow(const In aIrow, const In aIcomp, MidasVector& arRow) const;
      
      //! Get a column of the coriolis matrices
      void GetCol(const In aIcol, const In aIcomp, MidasVector& arCol) const;
      
      //! Dump coriolis matrices to disk.
      void DumpCoriolisCoeffsToFile(const std::string& arFileName, const std::vector<std::string>& aModeNames) const;
      
      //! Output operater overload.
      friend ostream& operator<<(ostream& arOut, CoriolisCoeffs& arCoriolisObj);
      
      //! Check the validity of the corilis matrices
      friend void CheckRowUnitarity(MidasMatrix& arNormalModes);
};

//! Overload operator << for object class
std::ostream& operator<<(std::ostream& arOut, CoriolisCoeffs& arCoriolisObj);   

//! Check
void CheckRowUnitarity(MidasMatrix& arNormalModes);

#endif /* CORIOLISCOUPLINGS_H_INCLUDED */
