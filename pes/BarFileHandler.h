/**
************************************************************************
* 
* @file                BarFileHandler.h
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BARFILEHANDLER_H_INCLUDED
#define BARFILEHANDLER_H_INCLUDED

// std headers
#include <vector>
#include <map>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/BaseBarFileHandler.h"
#include "pes/GridFileHandlerMethods.h"

// using declarations
using std::vector;
using std::map;
using std::string;
using ipair = std::pair<In,In>;

// forward declarations
class Derivatives;
class PesInfo;
class ModeCombiOpRange;
class GridType;
class PropertyFileStorage;

/**
 *
 **/
class BarFileHandler 
   : public GridFileHandlerMethods
{
   private:
      //!
      bool GenerateCalcName(string&, string&, const vector<In>&, const GridType&, const ModeCombi&);
      
      //!
      void ModeDisplacements_zero(const ModeCombi&, const vector<In>&, const GridType&, CalculationList&);
      
      //!
      void ModeDisplacements_one
         (  const ModeCombi&
         ,  const vector<In>&
         ,  MidasVector&
         ,  bool&
         ,  vector<Nb>&
         ,  const GridType&
         ,  Nb&
         ,  map<InVector,Nb>&
         ,  const Derivatives&
         ,  vector<MidasVector>&
         ,  const std::vector<PropertyFileStorage>&
         );
      
      //!
      void DerivativesHelper(const string&,In,map<string,Nb>::iterator&,vector<In>&,
                        const In&,const Derivatives&,vector<MidasVector>&);
      
      //!
      void ConstructAllKvecs(vector<InVector>&,map<ipair,ipair>&,vector<InVector>&, map<In,ipair>&);
      
      //!
      void ConstructAllNvecs(vector<InVector>&,map<ipair,ipair>&,In,In,const GridType&);
      
      //!
      void AddKvecsToVec(vector<InVector>&, InVector, const InVector& , const InVector&, In);
      
      //!
      void AddNvecsToVec(vector<InVector>&, InVector, In&, In, In&);

   public:
      //!
      BarFileHandler(PesInfo& arPesInfo);

      //!
      void DoDispDer_zero(const ModeCombiOpRange&,  In, const GridType&, CalculationList&);
      
      //!
      void DoDispDer_one(const ModeCombiOpRange&,In,const GridType&, const Derivatives&);
};

#endif // BARFILEHANDLER_H_INCLUDED
