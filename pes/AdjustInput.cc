#include "pes/AdjustInput.h"

#include "input/PesCalcDef.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/molecule/MoleculeUtility.h"
#include "mpi/Impi.h"

namespace midas
{
namespace pes
{

/**
 * Adjust molecular input based on pes input (basicly hack the molecular input...).
 * Not the prettiest function, but for now it gets the job done (whatever that job is...).
 *
 * @param aPesCalcDef    The pes calculation definition.
 * @param aMolecule      The molecule to adjust.
 * @param aSubsystemNumber  Subsystem number, used for naming of new molecule file in case of modifications.
 **/
void AdjustMolecularInput
   (  const PesCalcDef& aPesCalcDef
   ,  molecule::MoleculeInfo& aMolecule
   ,  In aSubsystemNumber
   )
{
   // Check orthogonality of the mass-weighted rectilinear normal coordinates
   //
   if (aPesCalcDef.GetmNormalcoordInPes())
   {
      midas::molecule::MoleculeInfo& new_molecule = aMolecule;
      bool modified = false;
      if (  aPesCalcDef.GetOrthoNormalizeNormalCoordinates())
      {
         OrthoNormalizeNormalCoordinates(new_molecule);
         modified = true;
      }
      else if (  aPesCalcDef.GetNormalizeNormalCoordinates())
      {
         NormalizeNormalCoordinates(new_molecule);
         modified = true;
      }
      
      if(modified && mpi::IsMaster())
      {
         new_molecule.WriteMoleculeFile(midas::molecule::MoleculeFileInfo{"new_molecule_" + std::to_string(aSubsystemNumber) + ".mmol", "MIDAS"});
      }
      
      if (  !NormAnalysis(new_molecule, aPesCalcDef.GetNormalCoordinateThreshold()) )
      {
         MIDASERROR  (  "Normal coordinates are not orthonormal to T=" 
                     +  libmda::util::to_string_with_precision(aPesCalcDef.GetNormalCoordinateThreshold(), 2) 
                     +  " when mass weighted!"
                     );
      }
   }

   if (aPesCalcDef.GetmNormalcoordInPes())
   {
      if (aMolecule.HasSelectVibs())
      {
         MIDASERROR("IMPLEMENT ME!");
         //aMolecule.SetNoOfVibs(mSelectVibs->size());
         
         auto no_of_vibs = aMolecule.GetNoOfVibs();
         std::vector<std::string> new_mode_names(no_of_vibs);
         std::vector<Nb> new_freqs(no_of_vibs,C_0);
         std::vector<std::vector<Nb>> new_modes(no_of_vibs);
         
         //for (In i=I_0; i < no_of_vibs ; ++i)
         //{
         //   Mout << " Selectvibs " << i << " " << mSelectVibs->at(i) << " " << mModeLabels.at(mSelectVibs->at(i)) << endl;
         //   new_mode_names[i] = mModeLabels.at(mSelectVibs->at(i));
         //   new_freqs[i] = mFreqs->at(mSelectVibs->at(i));
         //   MidasVector tmp_mode(C_3*mNumberOfNuclei);
         //   mNormalCoord->GetRow(tmp_mode,mSelectVibs->at(i));
         //   new_modes[i] = tmp_mode.GetStdVector();
         //}
      
         FrequencyData frequency_data(FrequencyType::Key());
         frequency_data.Initialize(origin_type::CALCULATED, std::move(new_freqs), "CM-1");
         aMolecule.ReInitFrequencies(frequency_data);
      
         VibrationalCoordinateData vib_coord_data(VibrationalCoordinateType::Key());
         vib_coord_data.Initialize(origin_type::CALCULATED, std::move(new_modes), "AU", "NORMALCOORD");
         aMolecule.ReInitVibrationalCoordinates(vib_coord_data);
      
         //mModeLabels.clear();
         //mModeLabels=new_mode_names;

         aMolecule.UnInitSigma();
         aMolecule.UnInitSelectVibs();
      }
   }
   else if (aPesCalcDef.GetmCartesiancoordInPes() || aPesCalcDef.GetmPesDiatomic())
   {
      Mout << " Initializing the molecular Cartesian coordinates " << std::endl;

      if (aPesCalcDef.GetmCartesiancoordInPes())
      {
         auto no_of_vibs = aMolecule.GetNumberOfNuclei() * I_3;
   
         if (aPesCalcDef.GetmPesDiatomic())
         {
            Mout << " The number of harmonic frequencies to be determined: " << no_of_vibs - I_5 << std::endl;
         }
         else
         {
            Mout << " The number of harmonic frequencies to be determined: " << no_of_vibs - I_6 << std::endl;
         }
         Mout << " The number of normal coordinates to be determined:   " << no_of_vibs << std::endl;
         
         aMolecule.SetNoOfVibs(no_of_vibs);
      }
      // If diatomic molecule, we set number of vibrations to 1
      else if (aPesCalcDef.GetmPesDiatomic())
      {
         aMolecule.SetNoOfVibs(I_1);
      }

      // Set symmetry labels to all A's.
      aMolecule.PurgeSymmetryLabels();
      
      // Set mode names.
      aMolecule.SetupDefaultModeNames();
   }
   else if (aPesCalcDef.GetmFalconInPes())
   {
      Mout << " Initializing the molecular FALCON coordinates " << std::endl;
   }
   else if (aPesCalcDef.GetmPscInPes())
   {
      Mout << " Initializing the molecular internal coordinates " << std::endl;
      
      // If some vibrational modes are to be frozen, then we need to adapt the MoleculeInfo object
      if (aMolecule.HasInactiveModes())
      {
         Mout << "  Will only use active/non-frozen vibrational modes in the following investigation " << std::endl; 
      }
   }
   else if (aPesCalcDef.GetmPscInPes())
   {
      Mout << " Initializing the molecular internal coordinates " << std::endl;
   }
   else
   {
      MIDASERROR("No molecule type selected.");
   }
}

} /* namespace pes */
} /* namespace midas */
