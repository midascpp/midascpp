#ifndef OPERATORBOUNDS_H_INCLUDED
#define OPERATORBOUNDS_H_INCLUDED

#include <vector>
#include <string>

#include "inc_gen/TypeDefs.h"

class OperatorBounds
{
   public:
      //                             mode         bounds  deriv   value  
      using bounds_type = std::tuple<std::string, Nb, Nb, Nb, Nb, Nb, Nb, Nb, Nb>;

   private:
      //! Holds bounds for each mode
      std::vector<bounds_type> mBounds;

   public:
      //! default ctor
      OperatorBounds() = default;

      //! ctor from filename
      OperatorBounds(const std::string& filename);

      //! get the bounds
      const std::vector<bounds_type>& GetBounds() const;
};

#endif /* OPERATORBOUNDS_H_INCLUDED */
