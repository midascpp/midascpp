#include "CombinedRspFunc.h"

#include <algorithm> // std::find

#include "util/Io.h"
#include "util/Range.h"
#include "util/stream/HeaderStream.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/GetLine.h"
#include "input/Input.h"
#include "mmv/StorageForSort.h"
#include "analysis/ResponseAnalysisInterface.h"

/**
 *
 **/
In CombinedRspFunc::IndexFromFrq(const std::vector<Nb>& frq) const
{
   for(int i = 0 ; i < mFrq.size(); ++i)
   {
      if(frq == mFrq[i])
         return i;
   }
   MIDASERROR("Did not find frq");
   return -1;
}

/**
 *
 **/
std::string CombinedRspFunc::FilenameFromIndexAndLabel(In idx, const std::string& label) const
{
   assert(idx >= 0);
   assert(idx < mFiles.size());

   auto files = mFiles[idx];

   auto files_vec = midas::util::StringVectorFromString(files, ';');

   for(int i = 0; i < files_vec.size(); ++i)
   {
      auto file_pair = midas::util::StringVectorFromString(files_vec[i], ':');
      assert(file_pair.size() == 2);
      if(file_pair[0] == label)
         return file_pair[1];
   }

   MIDASERROR("Did not find label: " + label + " for index: " + std::to_string(idx));
   return "NO_FILE";
}

/**
 *
 **/
bool CombinedRspFunc::AlreadyIncluded(const std::vector<Nb>& aFrqVec) const
{
   return std::find(mFrq.begin(), mFrq.end(), aFrqVec) != mFrq.end();
}

/**
 *
 **/
bool CombinedRspFunc::AlreadyIncluded(const RspFunc& aRspFunc) const
{
   std::vector<Nb> frq_vec(aRspFunc.GetNfrqs());
   for(size_t i=0; i<frq_vec.size(); ++i)
   {
      frq_vec[i] = aRspFunc.GetRspFrq(i);
   }
   return AlreadyIncluded(frq_vec);
}

/**
 *
 **/
bool CombinedRspFunc::AddRspFunc(const RspFunc& aRspFunc)
{
   if(!IsCompatible(aRspFunc) || !aRspFunc.HasBeenEval())
   {
      return false; // we didn't add rsp function
   }
   
   if(!AlreadyIncluded(aRspFunc))
   {
      // load frequencies
      mFrq.emplace_back(aRspFunc.GetNfrqs());
      for(int i=0; i<mFrq.back().size(); ++i)
      {
         mFrq.back()[i] = aRspFunc.GetRspFrq(i);
      }
      
      // load gamma and rsp
      mGamma.emplace_back(aRspFunc.Gamma());
      mReVal.emplace_back(aRspFunc.Value());
      mImVal.emplace_back(aRspFunc.ImValue());

      // load files
      mFiles.emplace_back(CreateFilesString(aRspFunc.Files()));
   }

   return true; // we added rsp function
}

/**
 *
 **/
void CombinedRspFunc::SkipRsp(std::istream& is) const
{
   int rsp_pos = is.tellg();
   while(is.good())
   {
      char ch = is.get();
      if(ch == '}')
      {
         return;
      }
   }
}

/**
 *
 **/
void CombinedRspFunc::ReadRsp(std::istream& is)
{
   std::string s;
   getline(is,s);
   getline(is,s);
   while(getline(is,s))
   {
      if(s=="}") // end of rsp input
      { 
         return; // return
      }
      std::istringstream iss(s);
      Nb temp;
      std::string temp_str;
      
      std::vector<Nb> frq_vec;
      frq_vec.emplace_back(0);
      for(int i = 1; i < Operators().size(); ++i)
      {
         iss >> temp;
         frq_vec.emplace_back(temp);
         frq_vec[0] -= frq_vec.back(); // calc internal frq
      }
      
      if(!AlreadyIncluded(frq_vec))
      {
         mFrq.emplace_back(frq_vec);

         iss >> temp;
         mGamma.emplace_back(temp);
         iss >> temp;
         mReVal.emplace_back(temp);
         iss >> temp;
         mImVal.emplace_back(temp);
         iss >> temp_str;
         mFiles.emplace_back(temp_str);
      }
   }
}

/**
 *
 **/
CombinedRspFunc::files_t CombinedRspFunc::ParseFilesString(const std::string& files_str) const
{
   files_t files;
   auto file_pairs = midas::util::StringVectorFromString(files_str,';');
   for(auto& file_pair_str: file_pairs)
   {
      auto file_pair = midas::util::StringVectorFromString(file_pair_str,':');
      MidasAssert(file_pair.size() == 2, "Number of 'arguments' are wrong in string: " + file_pair_str);
      files.emplace_back(file_pair[0], file_pair[1]);
   }
   return files;
}

/**
 *
 **/
std::string CombinedRspFunc::CreateFilesString(const files_t& file_pairs) const
{
   std::string files;
   int counter=0;
   for(auto& filepair: file_pairs)
   {
      files += filepair.first + ":" + filepair.second;
      if(counter != (file_pairs.size() - 1)) files += ";"; // if we are not at the last file, we print ';' as separator
      ++counter;
   }
   return files;
}
/**
 *
 **/
bool CombinedRspFunc::AddRspFunc(std::istream& is)
{
   if(!is.good())
   {
      MIDASERROR("std::istream not good on entry to CombinedRspFunc::AddRspFunc(...).");
   }

   auto type = Type();
   bool added = false;
   while(is.good())
   {
      char ch = is.get();
      switch(ch)
      {
         case '{': // start rsp input
         {
            std::string s;
            is >> s;
            if(s==type)
            {
               ReadRsp(is);
               is.seekg (0, is.beg); // reset stream to beginning
               return true;
            }
            else
            {
               SkipRsp(is);
            }
            break;
         }
         default:
         {
         }
      }
   }
   is.seekg (0, is.beg); // reset stream to beginning
   return false;
}

/**
 *
 **/
struct RspFrqLessThan
{
   bool operator()(const std::vector<Nb>& vec1, const std::vector<Nb>& vec2)
   {  
      for(size_t i=1; i<vec1.size(); ++i)
      {
         if(vec1[i] < vec2[i])
         {
            return true;
         }
      }
      return false;
   }
};

/**
 *
 **/
void CombinedRspFunc::Sort()
{
   //MidasAssert(AreEqual(mFrq.size(), mGamma.size(), mReVal.size(), mImVal.size(), mFiles.size()), "Sizes not equal");

   // find sorted positions
   std::vector<GeneralStorageForSort<std::vector<Nb> > > sort_vec;
   sort_vec.reserve(mFrq.size());

   for(size_t i=0; i<mFrq.size(); ++i)
   {
      sort_vec.emplace_back(mFrq[i],i);
   }
   
   std::stable_sort(sort_vec.begin(),sort_vec.end(),RspFrqLessThan());
   
   // do actual sort
   decltype(mFrq) sorted_frq(mFrq.size());
   decltype(mGamma) sorted_gamma(mGamma.size());
   decltype(mReVal) sorted_reval(mReVal.size());
   decltype(mImVal) sorted_imval(mImVal.size());
   decltype(mFiles) sorted_files(mFiles.size());
   for(size_t i = 0; i < sort_vec.size(); ++i)
   {
      sorted_frq[i] = mFrq[sort_vec[i].GetPoss()];
      sorted_gamma[i] = mGamma[sort_vec[i].GetPoss()];
      sorted_reval[i] = mReVal[sort_vec[i].GetPoss()];
      sorted_imval[i] = mImVal[sort_vec[i].GetPoss()];
      sorted_files[i] = mFiles[sort_vec[i].GetPoss()];
   }
   // save sorted lists
   mFrq = std::move(sorted_frq);
   mGamma = std::move(sorted_gamma);
   mReVal = std::move(sorted_reval);
   mImVal = std::move(sorted_imval);
   mFiles = std::move(sorted_files);
}

/**
 *
 **/
void CombinedRspFunc::GatherFiles()
{
   analysis::rsp().MakeAnalysisDir();
   
   int counter = 0; // counter to make sure files associated with each rsp func gets a global unique id (number) when gathered
   for(auto& files: mFiles) // loop over number of rsp functions
   {
      // loop over files for current rsp function, copy them to new location and update filename
      auto file_pairs = ParseFilesString(files); 
      for(auto& file: file_pairs)
      {
         std::string label = TypeForFile() + "_" + file.first; // unique file label
         std::string old_filename = file.second;
         std::string new_filename = analysis::rsp().analysis_dir + "/" + label + "_" + std::to_string(counter);

         CopyFile(old_filename + "_0", new_filename + "_0"); // append "_0" because of how DataCont names files
         file.second = new_filename; // remember to save new filename
      }
      
      files = CreateFilesString(file_pairs); // save new files string, created from updated the file_pairs
      ++counter; // increment counter over rsp funcs
   } 
}

/**
 *
 **/
std::string CombinedRspFunc::FilenameFromFrqAndLabel(Nb frq, const std::string& label) const
{
   assert(mFrq.size() > 0);
   assert(mFrq[0].size() == 2); // only works for linear response at the moment

   std::vector<Nb> frqs{-frq, frq};
   return FilenameFromIndexAndLabel(IndexFromFrq(frqs),label);
}

/**
 *
 **/
DataCont CombinedRspFunc::DataContFromFrqAndLabel(Nb frq, const std::string& label, In size) const
{
   DataCont temp;
   temp.GetFromExistingOnDisc(size, FilenameFromFrqAndLabel(frq,label));
   temp.ChangeStorageTo("InMem");
   return temp;
}

/**
 *
 **/
std::ostream& operator<<(std::ostream& os, const CombinedRspFunc& cmb_rsp)
{
   //HeaderInserter os_buf(os.rdbuf(),"   ",false);
   //std::ostream hos(&os_buf);
   int prec = 17;
   int width = 26;
   midas::stream::HeaderStream hos(os,"   ",false); // false -> skip header on first print
   midas::stream::ScopedPrecision(prec, hos);
   midas::stream::ScopedIosFlags(std::ios::scientific, hos);
   hos << "{\n" << static_cast<const ResponseFunction&>(cmb_rsp) << " # Comments here! \n";
   
   // print some comments
   hos << std::left; // print comments to the left

   for(int i = 0; i < cmb_rsp.Operators().size()-1; ++i)  // size()-1 because we only need/want external frequencies
   {
      hos << std::setw(width) << ("#EXT_FRQ_" + std::to_string(i)+"(au)");
   }
   hos << std::setw(width) << "#GAMMA(au)" 
       << std::setw(width) << "#REAL" 
       << std::setw(width) << "#IMAG" 
       << std::setw(width) << "#FILES" 
       << "\n";
   
   // print out values
   for(int i = 0; i < cmb_rsp.mFrq.size(); ++i)
   {
      for(int j = 1; j < cmb_rsp.mFrq[i].size(); ++j)
      {
         hos << std::setw(width) << cmb_rsp.mFrq[i][j];
      }
      hos << std::setw(width) << cmb_rsp.mGamma[i] 
          << std::setw(width) << cmb_rsp.mReVal[i] 
          << std::setw(width) << cmb_rsp.mImVal[i] 
          << std::setw(width) << cmb_rsp.mFiles[i] 
          << "\n";
   }
   
   // end
   hos << std::right; // right's bad interface makes this line nescessary :(  (cannot do skipheader after setw)
   hos << midas::stream::skipheader() << "}";
       
   return os;
}
