/**
************************************************************************
* 
* @file                OneModeGaussDef.cc
*
* Created:             30/05/2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Contain definition of one-mode localized gaussian basis
* 
* Last modified: Mon May 10, 2010  04:00PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::transform;

#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OneModeGaussDef.h"
#include "inc_gen/Const.h"

typedef string::size_type Sst;

/**
* Default constructor
**/
OneModeGaussDef::OneModeGaussDef(): mVecOfPts(), mVecOfExps()
{
   mImode=-1;
   mBasType=""; 
   mBasAuxDefs= "";
   mNbas=I_0;
   mBasDens=C_0;
}
/**
* Constructor
**/
OneModeGaussDef::OneModeGaussDef(In aMode, string aType, Nb aAlph, Nb aLeft, Nb aRight, In aNbas): mVecOfPts(aNbas,C_0), mVecOfExps(aNbas,C_0)
{
   mImode=aMode;
   mBasType=aType; 
   mBasAuxDefs="";
   mNbas=aNbas;
   mBasDens=aNbas/fabs(aRight-aLeft);
   if (aNbas==I_0) MIDASERROR(" No basis functions generated in OneModeGaussDef constructor ");
   for (In i=I_0; i<aNbas; i++)
      mVecOfPts[i]=aLeft+((aRight-aLeft)/(aNbas-C_1))*Nb(i);

   //construct the vector of exponents following the Hamilton and Light recipe
   //aAlph is the empirical constant used to generate the exponents.
   mVecOfExps[I_0]= pow(aAlph,C_2)/(pow((mVecOfPts[I_1]-mVecOfPts[I_0]),C_2));
   mVecOfExps[aNbas-I_1]= pow(aAlph,C_2)/(pow((mVecOfPts[aNbas-I_1]-mVecOfPts[aNbas-I_2]),C_2));
   for (In i=I_1; i<aNbas-I_1; i++)
   {
      Nb step=mVecOfPts[i+I_1]-mVecOfPts[i-I_1];
      mVecOfExps[i]=C_4*pow(aAlph,C_2)/pow(step,C_2);
   }
   if(gDebug) 
   {
      Mout << " Basis set definition for mode : " << aMode << endl;
      Mout << " Basis type: " << mBasType << endl;
      Mout << " aAlph: " << aAlph << endl;
      Mout << " aLeft: " << aLeft << endl;
      Mout << " aRight:" << aRight << endl;
      Mout << " No. of  basis functions: " << mNbas << endl;
      Mout << " Density of the gaussian basis: " << mBasDens << endl; 
      Mout << " Vector of positions: "  << endl;
      Mout << mVecOfPts << endl;
      Mout << " Vector of exponents: " << endl;
      Mout << mVecOfExps << endl;
   }
}
/**
* Constructor from Aux
**/
OneModeGaussDef::OneModeGaussDef(In aMode, string aType, string aAux): mVecOfPts(), mVecOfExps()
{
   mImode = aMode;
   mBasType=aType; 
   mNbas=0;
   mBasDens=C_0;

   // Transform tolower
   transform(aAux.begin(),aAux.end(),aAux.begin(),(In(*) (In))tolower);
   mBasAuxDefs = aAux;
   //debug
   if (gDebug) Mout << " auxiliary definitions: " << aAux << endl;

   // Extract info from aAux
   vector<string> aux_st_vec;

   // Go backwards and find labels;
   do {
      string piece;
      Sst cfind = aAux.rfind(",");
      if (cfind!=aAux.npos) piece = aAux.substr(cfind);
      if (cfind==aAux.npos) piece = aAux;  // No , found take it all.
      Sst l_p = piece.size();
      while(piece.find(",")!= piece.npos) piece.erase(piece.find(","),I_1);
      aux_st_vec.push_back(piece);
      if (cfind!=aAux.npos) aAux.erase(cfind,l_p);
      if (cfind==aAux.npos) aAux.erase(I_0,l_p);
      //Mout << " in loop piece = " << piece << " and aAux " << aAux << endl;
   } while (aAux.size()>0);

   Nb l_bnd=C_0;
   Nb r_bnd=C_0;
   Nb alph=C_0;
   In n_bas=I_0;
   Nb g_dens=C_0;
   //allow specification of n_bas or g_dens, whichever is different from zero
   for (In i=0;i<aux_st_vec.size();i++)
   {
      if (aux_st_vec[i].find("leftb=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("leftb"),aux_st_vec[i].find("=")+1);
         l_bnd=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " left radial endpoint found to be: "  << l_bnd << endl;
      }
      if (aux_st_vec[i].find("rightb=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("rightb"),aux_st_vec[i].find("=")+1);
         r_bnd=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " right radial endpoint found to be: "  << r_bnd << endl;
      }
      else if (aux_st_vec[i].find("alph=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("alph"),aux_st_vec[i].find("=")+1);
         alph=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " constant alph found to be: "  << alph << endl;
      }
      else if (aux_st_vec[i].find("nbas=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("nbas"),aux_st_vec[i].find("=")+1);
         n_bas=InFromString(aux_st_vec[i]);
         if (gDebug) Mout << " n_bas found to be: "  << n_bas << endl;
      }
      else if (aux_st_vec[i].find("dens=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("dens"),aux_st_vec[i].find("=")+1);
         g_dens=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " density of the gaussian basis: "  << g_dens << endl;
      }
      else
      {
         Mout << " No useful information found in basis set auxiliary input:" << aux_st_vec[i] <<endl;
      }
   }
   if ((l_bnd==C_0 && r_bnd==C_0) || r_bnd==l_bnd) MIDASERROR(" No proper boundaries given ");
   if (l_bnd>r_bnd) //swap them
   {
      Nb tmp=r_bnd;
      r_bnd=l_bnd;
      l_bnd=tmp;
   }
   if (g_dens==C_0 && n_bas==I_0) MIDASERROR("No proper no. of basis function given ");
   //construct vector gaussian positions
   if (g_dens==C_0)
   {
      mNbas=n_bas;
      mBasDens=Nb(n_bas)/(r_bnd-l_bnd);
   }
   else if (n_bas==I_0)
   {
      n_bas=In(fabs(r_bnd-l_bnd)*g_dens+C_I_2);
      mNbas=n_bas;
      mBasDens=g_dens;
      //Mout << " for g_dens: " << g_dens << " n_bas: " << n_bas << endl;
   }
   mVecOfPts.SetNewSize(n_bas);
   for (In i=0; i<n_bas; i++)
      mVecOfPts[i]=l_bnd+((r_bnd-l_bnd)/(Nb(n_bas)-C_1))*Nb(i);
   mVecOfExps.SetNewSize(n_bas);
   mVecOfExps.Zero();
   mVecOfExps[0]= pow(alph,C_2)/pow((mVecOfPts[1]-mVecOfPts[0]),C_2);
   mVecOfExps[n_bas-1]= pow(alph,C_2)/pow((mVecOfPts[mNbas-1]-mVecOfPts[mNbas-2]),C_2);
   for (In i=1; i<mVecOfExps.Size()-1; i++)
   {
      Nb step=mVecOfPts[i+1]-mVecOfPts[i-1];
      mVecOfExps[i]=C_4*pow(alph,C_2)/pow(step,C_2);
   }
   //print infos
   if(gDebug) 
   {
      Mout << " Basis set definition for mode : " << aMode << endl;
      Mout << " Basis type: " << aType << endl;
      Mout << " No. of  basis functions: " << mNbas << endl;
      Mout << " Density of the gaussian basis: " << mBasDens << endl;
      Mout << " Vector of positions: "  << endl;
      Mout << mVecOfPts << endl;
      Mout << " Vector of exponents : " << endl;
      Mout << mVecOfExps << endl;
   }
}
/**
 *  Get the values of the basis in a given point in the q space
**/
Nb OneModeGaussDef::EvalGaussBasis
   (  const In& aIbas
   ,  const Nb& aQval
   )  const
{
   Nb fval=C_0;
   //aQval is the unscaled coordinate
   Nb x=aQval;
   //Mout << " Mass-scaled coordinate x: " << x << endl;
   Nb g_exp=GetExp(aIbas);
   Nb g_ctr=GetGaussPnt(aIbas);
   Nb cst=pow(C_2*g_exp/C_PI,C_I_4);
   //Mout << " g_exp: " << g_exp << " g_ctr: " << g_ctr << " cst: " << cst << endl;  
   fval=cst*exp(-g_exp*(x-g_ctr)*(x-g_ctr));
   //Mout <<  " fval: " << fval << endl;
   return fval;
}
/**
 * Get the values of the basis for a MidasVector
**/ 
void OneModeGaussDef::EvalGaussBasis
   (  const In& aIbas
   ,  const MidasVector& arQval
   ,  MidasVector& arFuncVal
   )  const
{  
   In n_pts=arQval.Size();
   //Mout << " Nr. of points is: " << n_pts << endl;
   //aQval is the unscaled coordinate
   arFuncVal.SetNewSize(n_pts);
   Nb g_exp=GetExp(aIbas);
   Nb g_ctr=GetGaussPnt(aIbas);
   Nb cst=pow(C_2*g_exp/C_PI,C_I_4);
   //Mout << " g_exp: " << g_exp << " g_ctr: " << g_ctr << " cst: " << cst << endl;  
   for (In i=0; i<n_pts; i++)
   {  
      Nb x=arQval[i];
      //Mout << " Mass-scaled coordinate x: " << x << endl;
      arFuncVal[i]=cst*exp(-g_exp*(x-g_ctr)*(x-g_ctr));
      //Mout << " Value of " << aIbas << " harmonic function at " << x << " is " << arFuncVal[i] << endl;
   }
   return;
}
