/**
************************************************************************
*
* @file                McTdHCalcDef.cc
*
* Created:             09-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a MCTDH calculation
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "input/McTdHCalcDef.h"
#include "input/GetLine.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"
#include "input/OdeInput.h"
#include "input/KeywordMap.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Error.h"
#include "util/conversions/VectorFromString.h"
#include "util/FindInMap.h"

namespace detail
{
using mctdh_keyword_map = std::map<input::McTdHCalcDef::mctdhID, keyword_set>;
const mctdh_keyword_map& GetMcTdHDefaultsMap
   (
   )
{
   static const mctdh_keyword_map mctdh_defaults =
   {  {  input::McTdHCalcDef::mctdhID::LCASTDH, {  {"REGULARIZATIONTYPE", input::McTdHCalcDef::regularizationType::STANDARD}
                                                ,  {"REGULARIZATIONEPSILON", static_cast<Nb>(1.e-8)}
                                                ,  {"INTEGRATIONSCHEME", std::string("VMF")}
                                                ,  {"NONORTHOPROJECTOR", true}
                                                ,  {"PROJECTAFTERINVERSEDENSITY", true}
                                                ,  {"FAILIFNONORTHO", false}
                                                ,  {"RENORMALIZEWF", false}
                                                ,  {"EXACTNORM2", false}
                                                }
      }
   ,  {  input::McTdHCalcDef::mctdhID::LRASTDH,  {  {"REGULARIZATIONTYPE", input::McTdHCalcDef::regularizationType::STANDARD}
                                                 ,  {"REGULARIZATIONEPSILON", static_cast<Nb>(1.e-8)}
                                                 ,  {"INTEGRATIONSCHEME", std::string("VMF")}
                                                 ,  {"NONORTHOPROJECTOR", true}
                                                 ,  {"PROJECTAFTERINVERSEDENSITY", true}
                                                 ,  {"FAILIFNONORTHO", false}
                                                 ,  {"GOPTNONORTHOPROJECTOR", false}
                                                 ,  {"NONORTHOCONSTRAINTOPERATOR", false}
                                                 ,  {"GOPTREGULARIZATIONTYPE", input::McTdHCalcDef::GOptRegType::STANDARD}
                                                 ,  {"GOPTREGULARIZATIONEPSILON", static_cast<Nb>(1.e-8)}
                                                 ,  {"GOPTTRACESCALEEPS", false}
                                                 ,  {"GOPTTYPE", input::McTdHCalcDef::GOptType::VARIATIONAL}
                                                 ,  {"GAUGE", input::McTdHCalcDef::GaugeType::ZERO}
                                                 ,  {"MAXEXCILEVEL", I_3}
                                                 ,  {"EXTRANGEMODES", std::set<In>()}
                                                 ,  {"V3CFILEPREFIX", std::string("")}
                                                 ,  {"EVALUATECONTRIBBYCONTRIB", false}
                                                 ,  {"V3TRANS", false}
                                                 ,  {"RENORMALIZEWF", false}
                                                 ,  {"EXACTNORM2", false}
                                                 ,  {"BVECTORV3OOSTRANSFORM", false}
                                                 ,  {"CHECKBVECTOR", false}
                                                 ,  {"ALLOWEXPERIMENTAL", false}
                                                 }
      }
   };

   return mctdh_defaults;
}

const keyword_map& GetMcTdHIntegrationDefaultsMap
   (
   )
{
   static const keyword_map defaults = 
   {  {  "VMF",   {  {"ODEINFO", midas::ode::OdeInfo()}     // Default is an empty OdeInfo. This will fail, so it must always be specified on input!
                  }
      }
   };

   return defaults;
}

} /* namespace detail */

namespace midas::input
{

/**
 * Sanity check
 **/
void McTdHCalcDef::SanityCheck
   (
   )
{
   // Check that we have integrator info.
   if (  mIntegrationInfo.Empty()
      )
   {
      MIDASERROR("No McTdHIntegrationInfo in McTdHCalcDef!");
   }

   // Check if active space is empty
   if (  mActiveSpace.empty()
      )
   {
      MIDASERROR("MCTDH: No active space set!");
   }

   // Validate method info
   this->ValidateMethodInfo();

   // Validate integration info
   this->ValidateIntegrationInfo();

   // If using VMF scheme, check that the OdeInfo is not empty
   if (  this->mIntegrationInfo.template At<std::string>("SCHEME") == "VMF"
      )
   {
      auto& odeinfo = this->mIntegrationInfo.template At<midas::ode::OdeInfo>("ODEINFO");
      if (  odeinfo.empty()
         )
      {
         MIDASERROR("No OdeInfo given for VMF integration!");
      }

      // Checks on TdPropertyDef
      // NB: This should also be done for other integration schemes, but it requires some modification.
      TdPropertyDef::SanityCheck(odeinfo, this->mImagTime);
   }
   else
   {
      MidasWarning("Input TdPropertyDef is not checked for other integrators than VMF!");
   }
}

/**
 * Validate method info and set defaults.
 **/
void McTdHCalcDef::ValidateMethodInfo
   (
   )
{
   auto param_it = this->mMethodInfo.Find("PARAMETERIZATION");
   bool found_param = param_it != this->mMethodInfo.end();
   auto param = found_param ? param_it->second.template get<mctdhID>() : mctdhID::LCASTDH;    // Set default to LCASTDH
   if (  !found_param
      )
   {
      this->mMethodInfo["PARAMETERIZATION"] = mctdhID::LCASTDH;
   }

   const auto& keywords = ::detail::GetMcTdHDefaultsMap();

   auto allowed_it = keywords.find(param);
   if (  allowed_it == keywords.end()
      )
   {
      MIDASERROR("Unknown MCTDH parameterization. No default keywords available!");
   }
   auto allowed_keywords = allowed_it->second;

   // Check input
   for(auto& elem : this->mMethodInfo)
   {
      if (  elem.first == "PARAMETERIZATION"
         )
      {
         continue;
      }

      auto iter = allowed_keywords.find(elem);

      if (  iter == allowed_keywords.end()
         )
      {
         MIDASERROR("Keyword: '" + elem.first + "' is not allowed for the current MCTDH parameterization.");
      }
   }
   // set default driver values
   for(const auto& elem : allowed_keywords)
   {
      auto iter = this->mMethodInfo.Find(elem.first);
      if (  iter == this->mMethodInfo.end()
         )
      {
         this->mMethodInfo[elem.first] = elem.second;
      }
   }
}

/**
 * Validate integration info and set defaults.
 **/
void McTdHCalcDef::ValidateIntegrationInfo
   (
   )
{
   auto scheme = this->mIntegrationInfo.template At<std::string>("SCHEME");

   const auto& keyword_map_scheme = ::detail::GetMcTdHIntegrationDefaultsMap();

   auto allowed_keywords_it  =  keyword_map_scheme.find(scheme);
   if (  allowed_keywords_it == keyword_map_scheme.end()
      )
   {
      MIDASERROR("SCHEME: '" + std::string(scheme) + "' not found in McTdHIntegrationInfo input map.");
   }
   const auto& allowed_keywords = allowed_keywords_it->second;
      
   // check that all set keywords are allowed for the currectly set SCHEME
   for(auto& elem : this->mIntegrationInfo)
   {
      if (  elem.first == "SCHEME"
         )
      {
         continue;
      }

      auto iter = allowed_keywords.find(elem);

      if (  iter == allowed_keywords.end()
         )
      {
         MIDASERROR("Keyword: '" + elem.first + "' is not allowed for scheme: '" + std::string(scheme) + "'.");
      }
   }

   // set default values
   for(auto& elem : allowed_keywords)
   {
      auto iter = this->mIntegrationInfo.Find(elem.first);
      if(iter == this->mIntegrationInfo.end())
      {
         this->mIntegrationInfo[elem.first] = elem.second;
      }
   }
}

/**
 * Read OdeInfo
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next keyword?
 **/
bool McTdHCalcDef::ReadIntegrationInfo
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   enum INPUT  {  ERROR
               ,  SCHEME
               ,  ODEINFO
               ,  IOLEVEL
               };
   const std::map<std::string,INPUT> input_word =
   {  {"#4ERROR", ERROR}
   ,  {"#4SCHEME", SCHEME}
   ,  {"#4ODEINFO", ODEINFO}
   ,  {"#4IOLEVEL", IOLEVEL}
   };

   In input_level = 4;
   bool already_read = false;
   std::string s_orig = "";
   while (  (already_read || midas::input::GetLine(arInp, arS))
         && !midas::input::IsLowerLevelKeyword(arS, input_level)
         )
   {
      already_read=false;

      s_orig = arS;
      arS = midas::input::ParseInput(arS); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, arS);
      switch   (  input
               )
      {
         case SCHEME:
         {
            midas::input::GetLine(arInp, arS);
            this->mIntegrationInfo["SCHEME"] = midas::input::ToUpperCase(arS);
            break;
         }
         case ODEINFO:
         {
            midas::ode::OdeInfo odeinfo;
            already_read = OdeInput(arInp, arS, 5, odeinfo);
            this->mIntegrationInfo["ODEINFO"] = std::move(odeinfo);

            break;
         }
         case IOLEVEL:
         {
            this->mIntegrationInfo["IOLEVEL"] = midas::input::GetIn(arInp, arS);

            break;
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a McTdHIntegrationInfo level 4 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // We have read, but not processed the next input line
   return true;
}

/**
 * Read policy for initial wave function
 *
 * @param arInp
 * @param arS
 * @param arOpers    Set of operators to apply to initial wave function
 * @return
 *    Already read next line?
 **/
bool McTdHCalcDef::ReadInitialWfPolicy
   (  std::istream& arInp
   ,  std::string& arS
   ,  std::set<std::string>& arOpers
   )
{
   // Read line specifying the method for generating the initial wave packet
   midas::input::GetLine(arInp, arS);
   auto svec = midas::util::StringVectorFromString(arS);
   assert(!svec.empty());
   auto policy = midas::input::ToUpperCase(svec[0]);

   if (  policy == "VSCF"
      )
   {
      MidasAssert(svec.size() == 2, "Wrong number of arguments for MCTDH initial wave function (VSCF).");

      this->mInitialWavePacket = initType::VSCF;
      this->mInitCalcName = svec[1];

      Mout  << " McTdHCalcDef: MCTDH calculation will start from VSCF calculation: '" << this->mInitCalcName << "'." << std::endl;
   }
   else if  (  policy == "VCI"
            )
   {
      MidasAssert(svec.size() == 2, "Wrong number of arguments for MCTDH initial wave function (VCI).");

      this->mInitialWavePacket = initType::VCI;
      this->mInitCalcName = svec[1];

      Mout  << " McTdHCalcDef: MCTDH calculation will start from VCI calculation: '" << this->mInitCalcName << "'." << std::endl;
   }
//   else if  (  policy == "TDH"
//            )
//   {
//      MidasAssert(svec.size() == 2, "Wrong number of arguments for MCTDH initial wave function (TDH).");
//
//      this->mInitialWavePacket = initType::TDH;
//      this->mInitCalcName = svec[1];
//
//      Mout  << " McTdHCalcDef: MCTDH calculation will start from TDH calculation: '" << this->mInitCalcName << "'." << std::endl;
//   }
   else if  (  policy == "MCTDH"
            )
   {
      MidasAssert(svec.size() == 2, "Wrong number of arguments for MCTDH initial wave function (MCTDH).");

      this->mInitialWavePacket = initType::MCTDH;
      this->mInitCalcName = svec[1];

      Mout  << " McTdHCalcDef: MCTDH calculation will start from MCTDH calculation: '" << this->mInitCalcName << "'." << std::endl;
   }
   else
   {
      MIDASERROR("Unrecognized initialization policy: " + policy);
   }

   // Try to read new line for specifying operators applied to the wave packet
   midas::input::GetLine(arInp, arS);

   // If not a keyword, we process the input
   if (  midas::input::NotKeyword(arS)
      )
   {
      auto oper_s_vec = midas::util::StringVectorFromString(arS);

      if (  oper_s_vec.size() < 2
         )
      {
         MIDASERROR("Second argument line for InitialWf requires more than one word! (APPLYOPERATORS <oper1> <oper2> ...)");
      }

      MidasAssert(arOpers.empty(), "arOpers should be empty before reading input!");

      MidasAssert(midas::input::ToUpperCase(oper_s_vec[0]) == "APPLYOPERATORS", "The only option for second input line is 'APPLYOPERATORS'");

      for(In i=I_1; i<oper_s_vec.size(); ++i)
      {
         arOpers.emplace(oper_s_vec[i]);
      }

      // we have not read the next keyword
      return false;
   }
   // Else, we have read the next keyword
   else
   {
      return true;
   }
}

namespace detail
{
/**
 * Get map of methods with CAS
 **/
const std::map<std::string, McTdHCalcDef::mctdhID>& GetMcTdHCasMethodMap
   (
   )
{
   static const std::map<std::string, McTdHCalcDef::mctdhID> method_map =
   {  {"LCASTDH", McTdHCalcDef::mctdhID::LCASTDH}
   ,  {"L-MCTDH", McTdHCalcDef::mctdhID::LCASTDH}  // Alias
   ,  {"LMCTDH", McTdHCalcDef::mctdhID::LCASTDH}   // Alias
   ,  {"MCTDH", McTdHCalcDef::mctdhID::LCASTDH}    // Alias
   };

   return method_map;
}
/**
 * Get map of methods with truncated active spaces
 **/
const std::map<std::string, McTdHCalcDef::mctdhID>& GetRestrictedMcTdHMethodMap
   (
   )
{
   static const std::map<std::string, McTdHCalcDef::mctdhID> method_map =
   {  {"LRASTDH", McTdHCalcDef::mctdhID::LRASTDH}
   ,  {"L-MCTDH", McTdHCalcDef::mctdhID::LRASTDH}  // Alias
   ,  {"LMCTDH", McTdHCalcDef::mctdhID::LRASTDH}   // Alias
   ,  {"MCTDH", McTdHCalcDef::mctdhID::LRASTDH}    // Alias
   };

   return method_map;
}
/**
 * Get map of constraint operators
 **/
const std::map<std::string, McTdHCalcDef::GOptType>& GetGOptMap
   (
   )
{
   static const std::map<std::string, McTdHCalcDef::GOptType> g_map =
   {  {"g0", McTdHCalcDef::GOptType::NONE}
   ,  {"G0", McTdHCalcDef::GOptType::NONE}
   ,  {"D", McTdHCalcDef::GOptType::DENSITYMATRIX}
   ,  {"V", McTdHCalcDef::GOptType::VARIATIONAL}
   };

   return g_map;
}
}

/**
 * Read method name
 *
 * @param arInp      Istream
 * @param arS        String
 **/
bool McTdHCalcDef::ReadMethod
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   midas::input::GetLine(arInp, arS);
   MidasAssert(midas::input::NotKeyword(arS), "MCTDH: #3Method needs an argument line! Read: '" + arS + "'.");

   std::string name = midas::input::ToUpperCase(arS);

   auto lbracket_pos = name.find('[');
   auto rbracket_pos = name.find(']');
   bool truncated_method = (lbracket_pos != std::string::npos);
   MidasAssert(truncated_method == (rbracket_pos != std::string::npos), "Method must contain both '[' and ']' (or none of them).");

   // If no [] is found. We do CAS.
   if (  !truncated_method
      )
   {
      const auto& method_map = detail::GetMcTdHCasMethodMap();
      this->AddMethodInfo("PARAMETERIZATION", method_map.at(name));
   }
   else
   {
      const auto& method_map = detail::GetRestrictedMcTdHMethodMap();
      this->AddMethodInfo("PARAMETERIZATION", method_map.at(name.substr(0, lbracket_pos)));

      auto info_begin = lbracket_pos + 1;
      auto comma_pos = name.find(',');
      bool contains_comma = (comma_pos != std::string::npos);
      auto order_string = contains_comma ? name.substr(info_begin, comma_pos-info_begin) : name.substr(info_begin, rbracket_pos-info_begin);
      
      if (  contains_comma
         )
      {
         auto constraint_string = name.substr(comma_pos+1, rbracket_pos-(comma_pos+1));
         const auto& g_map = detail::GetGOptMap();
         this->AddMethodInfo("GOPTTYPE", g_map.at(constraint_string));
      }

      this->AddMethodInfo("MAXEXCILEVEL", midas::util::FromString<In>(order_string));
   }

   // We have only read one line
   return false;
}

/**
 * Are we using exponential parameterization?
 *
 * @return
 *    pair<exp modals, exp coefs>
 **/
std::pair<bool, bool> McTdHCalcDef::ExponentialParameterization
   (
   )  const
{
   std::pair<bool, bool> result = {false, false};

   switch   (  this->mMethodInfo.template At<mctdhID>("PARAMETERIZATION")
            )
   {
      case mctdhID::LCASTDH:
      case mctdhID::LRASTDH:
      {
         result.first = false;
         result.second = false;
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized MCTDH parameterization!");
      }
   }

   return result;
}

/**
 * Are we using exponential modal parameterization?
 *
 * @return
 *    true / false
 **/
bool McTdHCalcDef::ExponentialModalTransform
   (
   )  const
{
   return this->ExponentialParameterization().first;
}

/**
 * Are we using exponential coefs parameterization?
 *
 * @return
 *    true / false
 **/
bool McTdHCalcDef::ExponentialCoefsTransform
   (
   )  const
{
   return this->ExponentialParameterization().second;
}

} // namespace midas::input
