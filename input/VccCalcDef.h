/**
************************************************************************
* 
* @file                VccCalcDef.h
*
* Created:             28-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a Vcc calculation
* 
* Last modified:       02-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VCCCALCDEF_H_INCLUDED
#define VCCCALCDEF_H_INCLUDED

// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "mmv/MidasVector.h"
#include "vcc/v3/V3Enums.h"
#include "input/FlexCoupCalcDef.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/LaplaceInfo.h"
#include "vcc/v3/IntermediateRestrictions.h"
#include "it_solver/nl_solver/NewtonRaphsonInfo.h"
#include "vcc/v3/IntermediateRestrictions.h"
#include "td/tdvcc/TdvccEnums.h"

/**
* Construct a definition of a Vcc calculation
* */
class VccCalcDef
   :  public VscfCalcDef
{
   public:
      //! Tags for methods that can be set up automatically
      enum class MethodT : int
      {  VCC = 0        ///< General VCC[n]
      ,  CPVCC          ///< General CP-VCC[n]
      ,  VCI            ///< General VCI[n]
      ,  VMP            ///< General VMP[n]
      ,  ERROR          ///< Error
      };

   private:
      bool mVcc;                          ///< Do a Vcc calculation
      bool mVccOutOfSpace;                ///< Allow out of space excitations in basis algo?
      bool mVccRemoveSelUnlinked;         ///< Remove selected unlinked cont. 

      bool mVciH2;                        ///< Do a Vci calculation for H2
      In   mVciH2Roots;                   ///< Do a Vci (for H2) calculation for mVciRoots lowest roots.
      bool mH2Ls;                         ///< Has the H2 level shift been set?
      Nb   mH2LevelShift;                 ///< The Level shift for H2 calcs.

      bool mVci;                          ///< Do a Vci calculation
      In   mVciRoots;                     ///< Do a Vci calculation for mVciRoots lowest roots.
      bool mRepVciAsVcc;                  ///< At the end try to represent the Vci wave functions as Vcc
      bool mRepVccAsVci;                  ///< At the end try to represent the Vcc wave functions as Vci

      bool mVmp;                          ///< Do an Mp calc.
      bool mPade;                         ///< Do a Pade calc.
      In   mVmpMaxOrder;                  ///< Do an Vmp calc. up to order 
      bool mLambdaScan;                   ///< Do a scan of H_0 + lambda U eigenvalues 
      std::vector<Nb> mLambdas;                ///< The Lambdas to be studied.

      bool mVapt;                          ///< Do an Vapt calc.
      In   mVaptMaxOrder;                  ///< Do an Vapt calc. up to order 

      bool mFvciExpH;                     ///< Fvci with explicit Hamiltonian, primitive

      bool mExpHvci;                      ///< Vci  with explicit Hamiltonian
      In   mExpHvciExciLevel;             ///< Vci explicit Hamiltonian max excitation level

      //! Whether using special models with subspacesolver
      std::tuple<bool, midas::tdvcc::CorrType, midas::tdvcc::TrfType> mSubspaceSolverCalc = {false, midas::tdvcc::CorrType::VCC, midas::tdvcc::TrfType::MATREP};
      //! Do FVCI analysis (for use with MatRep).
      bool mMatRepFvciAnalysis = false;     

      In mMaxExciLevel;                   ///< The maximum excitation level 
      std::vector<In> mMaxExPrMode;            ///< Maximum excitation level for excitations including mode
      In mMaxSumN;                        ///< The maximum sum over n for an excitation.
      bool mIso;                          ///< Use interacting space order approach
      In mIsOrder;                        ///< The interaction space order 
      bool mSumNprim;                     ///< Use restrictions on sum n - very primitive impl for test 
      In mSumNThr;                        ///< the restrictions on sum n 
      bool mEmaxprim;                     ///< Use restrictions on E     - very primitive impl for test 
      Nb mEmaxThr;                        ///< the restrictions on E
      bool mZeroprim;                     ///< Minimum size or zero restrictions - very primitive impl for test 
      Nb mZeroThr;                        ///< the number below which is considered zero in amp. 

      std::string mModalFileName;         ///< Modals are on a file by the name 

      /** @name VSCF->VCC interface **/
      //!@{
      std::string mVscfReference = "";
      bool mUseAllVscf = false;
      bool mVscfInputInVcc = false;
      bool mAutoSetupMethod = false;
      //!@}

      In mRestExciLevel;                  ///< Excitation level of the calculation to restart from.
      bool mRestDiffExci;                  ///< Excitation level of the calculation to restart from.

      bool mPrimitiveBasis;               ///< Use the primitive (not a vscf modal basis)

      bool mTestTransformer;              ///< Test the transformation routines
      bool mH2Start;                      ///< Start by finding (H-E)^2 root
      bool mVmpStart;                     ///< Start from Vmp vector

      bool mTargetSpace;               ///< Use a target space 
      bool mReference;                 ///< Calculate the ref. by target.
      bool mAllFundamental;            ///< Calculate all fundamentals by target.
      bool mAllFirstOvertones;         ///< Calculate all first overtones by target.
      bool mFundamental;               ///< Calculate selected fundamentals by target.
      bool mFirstOvertones;            ///< Calculate selected first overtones by target.
      std::vector<In> mFunds;               ///< The selected fundamentals 
      std::vector<In> mFirsts;              ///< The selected first overtones 
      bool mFcVciCalc;                 ///< Set Vci FC calc.
      std::vector<std::string> mFcVciCalcOps;    ///< Vci FC calc operators.
      Nb   mTrustScaleUp;              ///< Scale step size upwards. 
      Nb   mTrustScaleDown;            ///< Scale step size downwards. 
      Nb   mMinTrustScale;             ///< Min. Scale step size downwards. 
      bool mEigValSeqSet;              ///< Take only a limited number of new vectors
      In   mEigValSeq;                 ///< The number of new solved at a time
      bool mT1TransH;                  ///< Use T1 transformed H in VCC calcs.?
      bool mOneModeTrans;              ///< Use one-mode (1m exci, 2m Hamiltonian) transformer?
      bool mTwoModeTrans;              ///< Use two-mode (2m exci, 2m Hamiltonian) transformer?
      Nb   mTwoModeTransScreen;        ///< Screening threshold, negative disables screening.
      std::vector<Nb>mVciEigVals;           ///< The converged VCI eigenvalues 
      
      bool   mV3trans;                 ///< Use V3 transformer.
      V3     mV3type;                  ///< Type of transformer (V3::* constant).
      std::string mV3FilePrefix;       ///< Force generation of contraction products from scratch.
      bool   mV3latex;                 ///< Generate LaTeX file of V3 contributions.
      
      bool   mCmpV3OrigVcc;            ///< Compare V3 and orig. VCC transformers (for debugging).
      bool   mCmpV3OrigVci;            ///< Compare V3 and orig. VCI transformers.
      bool   mCmpV3OrigVccJac;         ///< Compare V3 and orig. VCC Jacobian transformers.
      bool   mCheckV3Left;             ///< Check V3 left transformer.
      bool   mCheckMethod = true;      ///< Check V3 method against list of tested methods
      
      In mNresvecs;                    ///< Number of restart vectors
      bool mHarmonicRR;                ///< Use Harmonic Rayleigh-Ritz procedure to solve EigVal problems
      Nb mEnerShift;                   ///< Energy shift (sigma) used for (H-sigma) in Harmonic RR
      
      bool mScreenIntermeds = false;           ///< screen intermed products ?
      Nb   mScreenIntermedsThresh = 1e-12;     ///< if screen then use this threshold
      bool mScreenIntermedsAmps = false;       ///< screen intermed products only based on "amplitudes"?
      Nb   mScreenIntermedsAmpsThresh = 1e-12; ///< if screen based only on "amplitudes" then use this threshold
      
      FlexCoupCalcDef  mVccFlexCoupCalcDef;

      bool mUseNewTensorContractions = false;
      bool mInRspPart = false; // << !! HACK !!

      IntermediateRestrictions mIntermedRestrict;

      /** @name Tensor solver definitions **/
      //!@{

      //! Use TensorNlSolver instead of the old one
      bool mUseTensorNlSolver = false;
      //! Update decomposition threshold during the iterations
      Nb mAdaptiveDecompThreshold = -C_1;
      //! Update transformer decomp threshold
      Nb mAdaptiveTrfDecompThreshold = -C_1;
      //! Starting threshold for CP-tensor fittings
      Nb mFirstDecompThreshold = C_I_10_2;
      //! Use previous tensors as starting guess for re-decomposition
      bool mAdaptiveDecompGuess = false;
      //! Use largest residual tensor norm for convergence check
      bool mMaxNormConvCheck = false;
      //! Tensor decomposition information
      midas::tensor::TensorDecompInfo::Set mDecompInfoSet;
      //! Tensor decomposition information for transformer
      midas::tensor::TensorDecompInfo::Set mTransformerDecompInfoSet;
      //! Max allowed rank in tensor sums in transformer.
      In mTransformerAllowedRank = 0;
      //! Take CP contractions into account when identifying combined intermeds
      bool mUseCpScalingForIntermeds = false;
      //! Save and print tensor rank statistics at the end of the calculation.
      bool mSaveRankInfo = false;
      //! Perform rank analysis after solving the ground-state VCC problem with CP tensors
      bool mFullRankAnalysis = false;
      //! Screening thresholds for adding TensorDirectProduct%s and CanonicalTensor%s to TensorSum
      std::pair<Nb,Nb> mTensorProductScreening = {C_0, C_0};
      //! Adapt screening to decomp thresholds
      Nb mAdaptiveTensorProductScreening = -C_1;
      //! Low-rank limit for simply expanding direct products to CP
      In mTensorProductLowRankLimit = I_0;
      //! Laplace info for TensorNlSolver and TransformerV3
      midas::tensor::LaplaceInfo::Set mLaplaceInfo;
      //! Check the accuracy of each Laplace fit by comparing with exact denominators
      bool mCheckLaplaceFit = false;
      //! Exit before nth transform for debug purposes
      In mBreakBeforeTransform = -I_1;
      //! Construct energy denominators while updating amplitudes one MC at a time
      Nb mSelectiveUpdate = -C_1;
      //! Update the inverse Jacobian using Improved Diagonal Jacobian Approximation
      Nb mIdja = -C_1;
      //! Initialize IDJA inverse-Jacobian approximation to unit matrix instead of 0th-order approximation
      bool mIdjaUnitMatrixGuess = false;
      //! Check IDJA update. Angles and norms compared to old inverse Jacobian.
      bool mCheckIdja = false;
      //! Use CROP
      bool mCrop = false;
      //! Use DIIS
      bool mDiis = false;
      //! Allow improved preconditioning of the CROP/DIIS matrix
      bool mImprovedPreconInSubspaceMatrix = false;
      //! Use Newton-Raphson algorithm (previously mSecOrd)
      bool mNewtonRaphson = false;
      //! Check Newton-Raphson. Angles and norms of update compared to 0th-order approximation.
      bool mCheckNewtonRaphson = false;
      //! Info for Newton-Raphson algorithm (only for TensorNlSolver)     
      NewtonRaphsonInfo mNrInfo;
      //! Number of CROP/DIIS vectors in subspace
      In   mNSubspaceVecs = I_3;
      //! Precondition the saved residuals for CROP, DIIS, and quasi-Newton
      bool mPreconSavedResiduals = false;
      //! Individual thresholds for each MC tensor in the saved CROP residuals and/or in the transformer
      bool mIndividualMcDecompThresh = false;
      //! Scaling for individual decomp thresholds in transformer
      std::pair<Nb,Nb> mIndividualMcDecompThreshScaling = {C_1, C_0};
      //! Scale individual thresholds with tensor order
      Nb mTensorOrderThresholdScaling = -C_1;
      //! Use preconditioner for CROP, DIIS, and quasi-Newton
      bool mUsePrecon = true;
      //! Do quadratic backtracking line search when relative error decreases less than this value
      Nb   mBacktrackingThresh = C_0;
      //! Maximum number of backtracking iterations for mBacktrackingThresh > 0
      In   mMaxBacktracks = I_1;
      //! Maximum number of backsteps before error is thrown
      In   mMaxBacksteps = I_10;
      //! Allow backstep if the equations diverge
      bool mAllowBackstep = true;
      //!@}

      //! Max. eff. exc.level; for use with mExtRangeModes. -1 means not enabled.
      In mExtRangeMaxEffExciLevel = -I_1;
      //! Special modes (not counting towards max. effective exci. level).
      std::set<In> mExtRangeModes = {};

      //! Number of active modals for MVCC calculations
      std::vector<Uin> mNActiveModals;

   public:
      VccCalcDef();
      
      bool& InRspPart() { return mInRspPart; }

      void SetVcc(const bool& arVcc) {mVcc = arVcc;}                   ///< Vcc
      bool Vcc() const {return mVcc;};                                         
      void SetVccOutOfSpace(const bool& arB) {mVccOutOfSpace = arB;}; 
      bool VccOutOfSpace() const {return mVccOutOfSpace;};
      void SetVccRemoveSelUnlinked(const bool& arB) 
           {mVccRemoveSelUnlinked = arB;}; 
      bool VccRemoveSelUnlinked() const {return mVccRemoveSelUnlinked;};                                          

      void SetVciH2(const bool& arVciH2,const In& arVciH2Roots)
      {mVciH2 = arVciH2; mVciH2Roots = arVciH2Roots;}                           ///< Vci
      bool VciH2() const {return mVciH2;};                                          
      In   VciH2Roots() const  {return mVciH2Roots;}                
      void SetH2LevelShift(const bool& arB, const Nb& arLs)
           {mH2Ls  = arB; mH2LevelShift =  arLs;}      
      bool H2LevelShiftSet() const {return mH2Ls;}
      Nb   H2LevelShift() const {return mH2LevelShift;}
      
      void SetVci(const bool& arVci,const In& arVciRoots)
      {mVci = arVci; mVciRoots = arVciRoots;}                           ///< Vci
      bool Vci() const {return mVci;};                                          
      In   VciRoots() const  {return mVciRoots;}                
      void SetRepVciAsVcc(bool aB){mRepVciAsVcc = aB;}
      void SetRepVccAsVci(bool aB){mRepVccAsVci = aB;}
      bool RepVciAsVcc() const  {return mRepVciAsVcc;}                
      bool RepVccAsVci() const  {return mRepVccAsVci;}                

      void SetVmp(const bool& arVmp,const In& arVmpMaxOrder)
      {mVmp = arVmp; mVmpMaxOrder = arVmpMaxOrder;}          ///< Vmp
      bool Vmp()const {return mVmp;}                                      
      void SetPade(const bool& arPade) {mPade = arPade;}     ///< Vmp
      bool Pade()const {return mPade;}                                       
      In   VmpMaxOrder() const {return mVmpMaxOrder;}                
      void AddLambda(Nb& arLambda){mLambdaScan = true; mLambdas.push_back(arLambda);};   ///< LambdaScan                        
      bool DoLambdaScan() const {return mLambdaScan;}
      In Nlambdas() const {return mLambdas.size();}
      Nb GetLambda(In aI) const {return mLambdas[aI];}
      const std::vector<Nb>& Lambdas() const {return mLambdas;}           /// Get MaxExPrMode 

      void SetVapt(const bool& arVapt,const In& arVaptMaxOrder)
      {mVapt = arVapt; mVaptMaxOrder = arVaptMaxOrder;}          ///< Vapt
      bool Vapt()const {return mVapt;}                                      
      In   VaptMaxOrder() const {return mVaptMaxOrder;}                

      // Fvci with exp H section
      void SetFvciExpH(const bool& arFvciExpH)                          ///< Fvci with explicit Hamiltonian
      {mFvciExpH=arFvciExpH;}                                           
      bool FvciExpH() const {return mFvciExpH;}                                

      // explicit Hamiltonian Vci section 
      /**
       * @brief Vci with explicit Hamiltonian
       * @param arExpHvci
       * @param arExciLevel
       **/
      void SetExpHvci(const bool& arExpHvci,const In& arExciLevel)
      {mExpHvci = arExpHvci; mExpHvciExciLevel = arExciLevel;}          
      bool ExpHvci() const {return mExpHvci;}                                  
      In   ExpHvciExciLevel() const {return mExpHvciExciLevel;}                ///< Exci level for Vci with explicit Hamiltonian

      /**
       * @name SubspaceSolver calculations
       **/
      //!@{
      void EnableSubspaceSolverCalc(midas::tdvcc::CorrType aType, midas::tdvcc::TrfType aTrf) {mSubspaceSolverCalc = {true, aType, aTrf};}
      void DisableSubspaceSolverCalc() {std::get<0>(mSubspaceSolverCalc) = false;}
      bool UsingSubspaceSolver() const {return std::get<0>(mSubspaceSolverCalc);}
      midas::tdvcc::CorrType SubspaceSolverCorrType() const {return std::get<1>(mSubspaceSolverCalc);}
      midas::tdvcc::TrfType SubspaceSolverTrfType() const {return std::get<2>(mSubspaceSolverCalc);}

      bool MatRepFvciAnalysis() const {return mMatRepFvciAnalysis;}
      void SetMatRepFvciAnalysis(bool a) {mMatRepFvciAnalysis = a;}
      //!@}

      // Exci/mode definitions
      void SetMaxExciLevel(const In& arMaxExciLevel){mMaxExciLevel = arMaxExciLevel;}
      void SetMaxSumN(const In& arMaxSumN){mMaxSumN = arMaxSumN;}
      In   MaxExciLevel() const {return mMaxExciLevel;}                 ///< Max Exci level for Vci/Vcc
      In   MaxSumN() const {return mMaxSumN;}                           ///< Max Exci level for Vci/Vcc
      void SetMaxExPrMode(const std::vector<In>& arMaxExPrMode)              /// Set MaxExPrMode
      {mMaxExPrMode = arMaxExPrMode;}
      void GetMaxExPrMode(vector<In>& arMaxExPrMode) const              /// Get MaxExPrMode
      {arMaxExPrMode.clear(); arMaxExPrMode = mMaxExPrMode;}
      const std::vector<In>& MaxExPrMode() const {return mMaxExPrMode;}           /// Get MaxExPrMode 

      void SetIso(const bool aIso, const In& arIsOrder){mIso = aIso;mIsOrder= arIsOrder;}
      bool Iso() const {return mIso;}  
      In IsOrder() const {return mIsOrder;} 
      void SetSumN(const bool aSumNprim, const In& arSumN){mSumNThr = arSumN;mSumNprim= aSumNprim;}
      bool SumNprim() const {return mSumNprim;}
      In   SumNThr() const {return mSumNThr;}
      void SetEmax(const bool aEmaxprim, const Nb& arEmax){mEmaxThr = arEmax;mEmaxprim= aEmaxprim;}
      bool Emaxprim() const {return mEmaxprim;}
      Nb   EmaxThr() const {return mEmaxThr;}
      void SetZero(const bool aZeroprim, const Nb& arZero){mZeroThr = arZero;mZeroprim= aZeroprim;}
      bool Zeroprim() const {return mZeroprim;}
      Nb   ZeroThr() const {return mZeroThr;}
      void SetEigValSeq(const In aN){mEigValSeqSet= true; mEigValSeq= aN;}
      In   EigValSeq() const {return mEigValSeq;}
      bool EigValSeqSet() const {return mEigValSeqSet;}

      void SetModalFileName(const std::string& arFile) {mModalFileName = arFile;}  /// Modals File
      std::string GetModalFileName() const {return mModalFileName;}  

      void SetRestExciLevel(const bool arRestDiffExci, const In& arRestExciLevel=I_0)
      {
         mRestDiffExci = arRestDiffExci;
         mRestExciLevel = arRestExciLevel;
      }
      In RestExciLevel() const {return mRestExciLevel;}
      bool RestDiffExci() const {return mRestDiffExci;}

      void SetPrimitiveBasis(const bool& arPrimitiveBasis) {mPrimitiveBasis =  arPrimitiveBasis;} ///< Primitive basis
      bool UsePrimitiveBasis() const {return mPrimitiveBasis;} ///< Primitive basis

      void SetTestTransformer(const bool& arTest) {mTestTransformer =  arTest;} ///< Test transformer 
      bool TestTransformer() const {return mTestTransformer;} ///< Test transformer 
      void SetH2Start(const bool& ar) {mH2Start =  ar;} ///< Set mH2Start
      bool H2Start() const {return mH2Start;} ///< H2Start
      void SetVmpStart(const bool& ar) {mVmpStart =  ar;} ///< Set mVmpStart
      bool VmpStart() const {return mVmpStart;} ///< Start Vcc from accumulated Vmp vector 
      void SetTargetSpace(bool aB) {mTargetSpace = aB; }///< Use a target space 
      void SetReference(bool aB=true) {mReference= aB; }
      void SetAllFundamentals(bool aB=true) {mAllFundamental = aB; } ///< Calculate all fundamentals by target.
      void SetAllFirstOvertones(bool aB=true) {mAllFirstOvertones= aB; } ///< Calculate all first overtones by target.
      void SetFundamentals(const std::vector<In>& arFv,bool aB=true) {mFunds=arFv; mFundamental = aB; } ///< Calculate all fundamentals by target.
      void SetFirstOvertones(const std::vector<In>& arFf,bool aB=true) {mFirsts=arFf; mFirstOvertones= aB; } ///< Calculate all first overtones by target.
      void SetMolMCs(const std::vector<vector<In> >& aMolMCs) {mMolMCs = aMolMCs;}
      bool TargetSpace() const {return mTargetSpace; }///< Use a target space 
      bool Reference() const {return mReference; }
      bool AllFundamentals() const {return mAllFundamental; } ///< Calculate all fundamentals by target.
      bool AllFirstOvertones() const {return mAllFirstOvertones; } ///< Calculate all first overtones by target.
      bool Fundamentals() const {return mFundamental; } ///< Calculate all fundamentals by target.
      bool FirstOvertones() const {return mFirstOvertones; } ///< Calculate all first overtones by target.
      In Nfundamentals() const {return mFunds.size(); }
      In NfirstOvertones() const {return mFirsts.size(); }
      In Fundamental(In aI) const {return mFunds[aI]; }
      In FirstOvertone(In aI) const {return mFirsts[aI];}

      void SetFcVciCalc(bool aB) {mFcVciCalc = aB;}                  ///< Set Vci FC calc.
      bool FcVciCalc() const {return mFcVciCalc;}                          ///< Is Vci FC calc set?
      void AddFcVciCalcOp(string aB) {mFcVciCalcOps.push_back(aB);}  ///< Vci FC calc. operator.
      std::string FcVciCalcOp(In aOp=I_0) const {return mFcVciCalcOps[aOp];}    ///< Provid Vci FC operator. 
      In NrFcVciCalcOps() const {return mFcVciCalcOps.size();}             ///< No. of Vci FC operators. 

      void SetTrustScaleUp(Nb aNb) {mTrustScaleUp = aNb;}
      void SetTrustScaleDown(Nb aNb) {mTrustScaleDown = aNb;}
      void SetMinTrustScale(Nb aNb) {mMinTrustScale= aNb;}
      Nb TrustScaleUp() const {return mTrustScaleUp;}
      Nb TrustScaleDown() const {return mTrustScaleDown;}
      Nb MinTrustScale() const {return mMinTrustScale;}
      void SetT1TransH(bool aB) {mT1TransH = aB;}
      bool T1TransH() const {return mT1TransH;}
      void SetOneModeTrans(bool aB) {mOneModeTrans = aB;}
      bool OneModeTrans() const {return mOneModeTrans;}
      void SetTwoModeTrans(bool aB) {mTwoModeTrans = aB;}
      bool TwoModeTrans() const {return mTwoModeTrans;}
      void SetTwoModeTransScreen(Nb aC) {mTwoModeTransScreen = aC;}
      Nb TwoModeTransScreen() const {return mTwoModeTransScreen;}
      void StoreVciEigVals(MidasVector arEigVals);
      Nb GetVciEigVal(In arRoot) const {return mVciEigVals[arRoot];};

      void SetV3trans(bool aV3) {mV3trans = aV3;}
      bool V3trans() const {return mV3trans;}
      void SetV3type(const V3 aT) {mV3type = aT;}
      V3  V3type() const {return mV3type;}
      void SetV3FilePrefix(const std::string& aS) {mV3FilePrefix = aS;}
      const std::string& V3FilePrefix() const {return mV3FilePrefix;}
      void SetV3latex(bool aB) {mV3latex=aB;}
      bool V3latex() const {return mV3latex;}
      
      void SetCmpV3OrigVcc(bool aB) { mCmpV3OrigVcc = aB;}
      bool CmpV3OrigVcc() const {return mCmpV3OrigVcc;}
      void SetCmpV3OrigVci(bool aB) {mCmpV3OrigVci = aB;}
      bool CmpV3OrigVci() const {return mCmpV3OrigVci;}
      void SetCmpV3OrigVccJac(bool aB) {mCmpV3OrigVccJac = aB;}
      bool CmpV3OrigVccJac() const {return mCmpV3OrigVccJac;}
      void SetCheckV3Left(bool aB) {mCheckV3Left = aB;}
      bool CheckV3Left() const {return mCheckV3Left;}
     
      void SetNresvecs(const In& arI){mNresvecs = arI;}
      In GetNresvecs() const {return mNresvecs;}
      void SetHarmonicRR(const bool& arB){mHarmonicRR = arB;}
      bool GetHarmonicRR() const {return mHarmonicRR;}
      void SetEnerShift(const Nb& arNb){mEnerShift = arNb;}
      Nb GetEnerShift() {return mEnerShift;}

      // Intermed Product Screening
      void SetScreenIntermeds(bool aB)      { mScreenIntermeds=aB; }
      bool ScreenIntermeds()          const { return mScreenIntermeds; }
      void SetScreenIntermedsThresh(Nb aNb) { mScreenIntermedsThresh=aNb; }
      Nb   ScreenIntermedsThresh()    const { return mScreenIntermedsThresh; }
      void SetScreenIntermedsAmps(bool aB)      { mScreenIntermedsAmps=aB; }
      bool ScreenIntermedsAmps()          const { return mScreenIntermedsAmps; }
      void SetScreenIntermedsAmpsThresh(Nb aNb) { mScreenIntermedsAmpsThresh=aNb; }
      Nb   ScreenIntermedsAmpsThresh()    const { return mScreenIntermedsAmpsThresh; }
      
      FlexCoupCalcDef GetmVccFlexCoupCalcDef() const {return mVccFlexCoupCalcDef;}
      void SetmFlexCoupCalcDef(const FlexCoupCalcDef& arFlexCoupCalcDef) {mVccFlexCoupCalcDef.Reset(arFlexCoupCalcDef);}
      
      bool CheckMethod() const { return mCheckMethod; }
      void SetCheckMethod(bool aB) { mCheckMethod = aB; }
      
      bool ReadUseNewTensorContractions(std::istream&, std::string&);
      bool UseNewTensorContractions() const { return mUseNewTensorContractions; }


      bool ReadIntermediateRestrictions(std::istream&, std::string&);
      const IntermediateRestrictions& GetIntermedRestrict() const { return this->mIntermedRestrict; }

      //! Set values for TensorDataCont calculation
      void SetupTensorDataContCalculation();

      //! Set values for DataCont calculation
      void SetupDataContCalculation();


      /* Setters and Getters for TensorNlSolver input */
      bool UseTensorNlSolver()        const    { return mUseTensorNlSolver; }

      void SetAdaptiveDecompThreshold(Nb aN)   { mAdaptiveDecompThreshold = aN; }
      Nb   AdaptiveDecompThreshold()     const { return mAdaptiveDecompThreshold; }

      void SetAdaptiveTrfDecompThreshold(Nb aN){ mAdaptiveTrfDecompThreshold = aN; }
      Nb   AdaptiveTrfDecompThreshold() const  { return mAdaptiveTrfDecompThreshold; }

      void SetFirstDecompThreshold(Nb aN)       { mFirstDecompThreshold = aN; }
      Nb FirstDecompThreshold() const           { return mFirstDecompThreshold; }

      void SetAdaptiveDecompGuess(bool aB)  { mAdaptiveDecompGuess = aB; }
      bool AdaptiveDecompGuess()      const { return mAdaptiveDecompGuess; }

      void SetMaxNormConvCheck(bool aB)     { mMaxNormConvCheck = aB; }
      bool MaxNormConvCheck() const         { return mMaxNormConvCheck; }

      bool ReadVccDecompInfoSet(std::istream&, std::string&);
      midas::tensor::TensorDecompInfo::Set& GetDecompInfoSet() { return mDecompInfoSet; }

      bool UseCpScalingForIntermeds() const { return mUseCpScalingForIntermeds; }
      void SetUseCpScalingForIntermeds(bool aB)    { mUseCpScalingForIntermeds = aB; }

      void SetSaveRankInfo(bool aB)         { mSaveRankInfo = aB; }
      bool GetSaveRankInfo()          const { return mSaveRankInfo; }

      void SetFullRankAnalysis(bool aB)     { mFullRankAnalysis = aB; }
      bool FullRankAnalysis() const         { return mFullRankAnalysis; }

      bool ReadLaplaceInfoSet(std::istream&, std::string&);
      midas::tensor::LaplaceInfo::Set& GetLaplaceInfo() { return mLaplaceInfo; }
//      void SetLaplaceQuadPoints(In aIn)     { mLaplaceQuadPoints = aIn; }
//      In   LaplaceQuadPoints()        const { return mLaplaceQuadPoints; }

      void SetCheckLaplaceFit(bool aB)      { mCheckLaplaceFit = aB; }
      bool CheckLaplaceFit()          const { return mCheckLaplaceFit; }
      
      bool ReadVccTransformerDecompInfoSet(std::istream&, std::string&);
      midas::tensor::TensorDecompInfo::Set& GetTransformerDecompInfoSet() { return mTransformerDecompInfoSet; }
      
      bool ReadVccTransformerAllowedRank(std::istream&, std::string&);
      void SetTransformerAllowedRank(Nb aNb) { mTransformerAllowedRank = aNb; }
      In GetTransformerAllowedRank() const { return mTransformerAllowedRank; }

      void SetSelectiveUpdate(Nb aNb) { mSelectiveUpdate = aNb; }
      Nb SelectiveUpdate()     const { return mSelectiveUpdate; }

      void SetIdja(Nb aNb) { mIdja = aNb; }
      Nb   GetIdja() const  { return mIdja; }

      void SetCheckIdja(bool aB) { mCheckIdja = aB; }
      bool CheckIdja() const     { return mCheckIdja; }

      void SetIdjaUnitMatrixGuess(bool aB)   { mIdjaUnitMatrixGuess = aB; }
      bool IdjaUnitMatrixGuess() const       { return mIdjaUnitMatrixGuess; }

      void SetNSubspaceVecs(In aIn) { mNSubspaceVecs = aIn; }
      In   GetNSubspaceVecs()     const { return mNSubspaceVecs; }
      void SetPreconSavedResiduals(bool aB) { mPreconSavedResiduals = aB; }
      bool PreconSavedResiduals() const { return mPreconSavedResiduals; }
      void SetIndividualMcDecompThresh( bool aB ) { mIndividualMcDecompThresh = aB; }
      bool IndividualMcDecompThresh() const { return mIndividualMcDecompThresh; }
      void SetIndividualMcDecompThreshScaling
         (  Nb aAbs
         ,  Nb aRel 
         ) 
      { 
         mIndividualMcDecompThreshScaling = std::make_pair(aAbs, aRel);
      }
      const std::pair<Nb,Nb>& IndividualMcDecompThreshScaling() const { return mIndividualMcDecompThreshScaling; }
      
      Nb TensorOrderThresholdScaling() const { return mTensorOrderThresholdScaling; }
      void SetTensorOrderThresholdScaling( Nb aNb ) { mTensorOrderThresholdScaling = aNb; }

      void SetUsePrecon(bool aB)     { mUsePrecon = aB; }
      bool UsePrecon() const        { return mUsePrecon; }
      void SetCrop(bool aB)          { mCrop = aB; }
      bool GetCrop() const           { return mCrop; }
      void SetDiis(bool aB)          { mDiis = aB; }
      bool Diis() const           { return mDiis; }
      void SetNewtonRaphson(bool aB) { mNewtonRaphson = aB; }
      bool NewtonRaphson() const     { return mNewtonRaphson; }

      void SetCheckNewtonRaphson(bool aB) { mCheckNewtonRaphson = aB; }
      bool CheckNewtonRaphson() const     { return mCheckNewtonRaphson; }
      
      bool ReadNewtonRaphsonInput(std::istream&, std::string&);
      NewtonRaphsonInfo& GetNrInfo() { return mNrInfo; }

      void SetImprovedPreconInSubspaceMatrix(bool aB) { mImprovedPreconInSubspaceMatrix = aB; }
      bool ImprovedPreconInSubspaceMatrix() const { return mImprovedPreconInSubspaceMatrix; }

      void SetBacktrackingThresh(Nb aNb) { mBacktrackingThresh = aNb; }
      Nb   BacktrackingThresh() const    { return mBacktrackingThresh; }

      void SetMaxBacktracks(In aIn)      { mMaxBacktracks = aIn; }
      In   GetMaxBacktracks() const      { return mMaxBacktracks; }
      void SetMaxBacksteps(In aIn)       { mMaxBacksteps = aIn; }
      In   GetMaxBacksteps() const       { return mMaxBacksteps; }

      void SetAllowBackstep(bool aB)     { mAllowBackstep = aB; }
      bool AllowBackstep() const         { return mAllowBackstep; }
      
      void SetTensorProductScreening(Nb aTps, Nb aTs) { mTensorProductScreening = std::make_pair(aTps, aTs); }
      const std::pair<Nb,Nb>& TensorProductScreening() const { return mTensorProductScreening; }

      void SetAdaptiveTensorProductScreening(Nb aNb) { mAdaptiveTensorProductScreening = aNb; }
      Nb AdaptiveTensorProductScreening() const { return mAdaptiveTensorProductScreening; }

      void SetTensorProductLowRankLimit(In aIn) { mTensorProductLowRankLimit = aIn; }
      In TensorProductLowRankLimit() const { return this->mTensorProductLowRankLimit; }

      void SetBreakBeforeTransform(In aBreak) { mBreakBeforeTransform = aBreak; }
      In BreakBeforeTransform() const { return mBreakBeforeTransform; }

      /*********************************************************************//**
       * @name Extended range using _effective_ excitation levels
       *
       * Methods for the use of _effective_ excitation levels to create a
       * ModeCombiOpRange that is extended with ModeCombi%s containing the
       * special _extended range_ modes.
       ************************************************************************/
      //!@{
      void SetExtRangeMaxEffExciLevel(In aLevel) {mExtRangeMaxEffExciLevel = aLevel;}
      void SetExtRangeModes(const std::set<In>& aModes) {mExtRangeModes = aModes;}

      In GetExtRangeMaxEffExciLevel() const {return mExtRangeMaxEffExciLevel;}
      const std::set<In>& GetExtRangeModes() const {return mExtRangeModes;}

      bool UsingExtRangeModes() const {return mExtRangeMaxEffExciLevel >= I_0 && !mExtRangeModes.empty();}
      //!@}

      //! Get/Set NActiveModals
      const auto& NActiveModals() const { return this->mNActiveModals; }
      void SetNActiveModals(const std::vector<Uin>& aA) { this->mNActiveModals = aA; }
      
      //! Set up method from input string
      std::pair<bool, bool> AutoSetupMethod
         (  std::istream& arInp
         ,  std::string& arS
         );

      bool GetAutoSetupMethod() const { return mAutoSetupMethod; }
      void SetAutoSetupMethod(bool aB) { mAutoSetupMethod = aB; }

      //! Get/Set VscfInputInVcc
      bool VscfInputInVcc() const { return mVscfInputInVcc; }
      void SetVscfInputInVcc() { mVscfInputInVcc = true; }

      //! Get/Set VscfReference
      const std::string& VscfReference() const { return mVscfReference; }
      void SetVscfReference(const std::string& aS) { mVscfReference = aS; }

      //! Get UseAllVscf
      bool UseAllVscf() const { return mUseAllVscf; }
      void SetUseAllVscf(bool aB) { mUseAllVscf = aB; }
};

#endif /* VCCCALCDEF_H_INCLUDED */
