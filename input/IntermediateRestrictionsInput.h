/**
************************************************************************
* 
* @file                IntermediateRestrictionsInput.h
*
* 
* @date                07-11-2016
*
* @author              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief               Input that determines the conditions for saving
*                      VCC intermediates in IntermediateMachine
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATERESTRICTIONSINPUT_H_INCLUDED
#define INTERMEDIATERESTRICTIONSINPUT_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"
#include "vcc/v3/IntermediateRestrictions.h"

bool IntermediateRestrictionsInput(std::istream&, std::string&, IntermediateRestrictions&);


#endif /* INTERMEDIATERESTRICTIONSINPUT_H_INCLUDED */
