/**
************************************************************************
* 
* @file                FreqAnaInput.cc
*
* 
* Created:             05-05-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Input reader for the frequency analysis
* 
* Last modified:       09-06-2015 (Mads Boettger Hansen)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include "input/Input.h"
#include "input/FreqAnaCalcDef.h"
#include <string>

/**
 * FreqAna input driver
 **/
std::string FreqAnaInput(std::istream& Minp, input::FreqAnaCalcDef* apCalcDef, const In& arInputLevel)
{

   std::string s;//To be returned when we are done reading this level
	In input_level = arInputLevel+I_1;
   enum INPUT { 
      ERROR, 
      DISPFACTOR, 
      NONSYMMETRICHESSIAN,
      COMPARESYMANDNONSYM,
      SINGLEPOINTNAME, 
      NUMNODES, 
      PROCPERNODE, 
      DUMPINTERVAL, 
      ROTATIONTHR,
      MOLINFOOUTNAME,
      TARGETING,
      ITEQENERTHR,
      ITEQRESTHR,
      ITEQBREAKDIM,
      ITEQMAXIT,
      OVERLAPSUMMIN,
      OVERLAPNMAX,
      ALREADYMASSWEIGHTED,
   };

   const map<std::string, INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"DISPFACTOR", DISPFACTOR},
      {"#"+std::to_string(input_level)+"NONSYMMETRICHESSIAN", NONSYMMETRICHESSIAN},
      {"#"+std::to_string(input_level)+"COMPARESYMANDNONSYM", COMPARESYMANDNONSYM},
      {"#"+std::to_string(input_level)+"SINGLEPOINTNAME", SINGLEPOINTNAME},
      {"#"+std::to_string(input_level)+"NUMNODES", NUMNODES},
      {"#"+std::to_string(input_level)+"PROCPERNODE", PROCPERNODE},
      {"#"+std::to_string(input_level)+"DUMPINTERVAL", DUMPINTERVAL},
      {"#"+std::to_string(input_level)+"ROTATIONTHR", ROTATIONTHR},
      {"#"+std::to_string(input_level)+"MOLINFOOUTNAME", MOLINFOOUTNAME},
      {"#"+std::to_string(input_level)+"TARGETING", TARGETING},
      {"#"+std::to_string(input_level)+"ITEQENERTHR", ITEQENERTHR},
      {"#"+std::to_string(input_level)+"ITEQRESTHR", ITEQRESTHR},
      {"#"+std::to_string(input_level)+"ITEQBREAKDIM", ITEQBREAKDIM},
      {"#"+std::to_string(input_level)+"ITEQMAXIT", ITEQMAXIT},
      {"#"+std::to_string(input_level)+"OVERLAPSUMMIN", OVERLAPSUMMIN},
      {"#"+std::to_string(input_level)+"OVERLAPNMAX", OVERLAPNMAX},
      {"#"+std::to_string(input_level)+"ALREADYMASSWEIGHTED", ALREADYMASSWEIGHTED},
   };

   bool already_read=false;
	while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
		std::string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
		switch(input)
      {
         case DISPFACTOR:
         {
            getline(Minp,s);
            apCalcDef->SetDispFactor(midas::util::FromString<Nb>(s));
            break;
         }
         case NONSYMMETRICHESSIAN:
         {
            apCalcDef->SetNonSymmetricHessian(true);
            break;
         }
         case COMPARESYMANDNONSYM:
         {
            apCalcDef->SetCompareSymAndNonSym(true);
            break;
         }
         case SINGLEPOINTNAME:
         {
            getline(Minp,s);
            apCalcDef->SetSinglePointName(s);
            break;
         }
         case NUMNODES:
         {
            getline(Minp,s);
            apCalcDef->SetNumNodes(midas::util::FromString<In>(s));
            break;
         }
         case PROCPERNODE:
         {
            getline(Minp,s);
            apCalcDef->SetProcPerNode(midas::util::FromString<In>(s));
            break;
         }
         case DUMPINTERVAL:
         {
            getline(Minp,s);
            apCalcDef->SetDumpInterval(midas::util::FromString<In>(s));
            break;
         }
         case ROTATIONTHR:
         {
            getline(Minp,s);
            apCalcDef->SetRotationThr(midas::util::FromString<Nb>(s));
            break;
         }
         case MOLINFOOUTNAME:
         {
            getline(Minp,s);
            apCalcDef->SetMolInfoOutName(s);
            break;
         }
         case TARGETING:
         {
            apCalcDef->SetTargeting(true);
            break;
         }
         case ITEQENERTHR:
         {
            getline(Minp,s);
            apCalcDef->SetItEqEnerThr(midas::util::FromString<Nb>(s));
            break;
         }
         case ITEQRESTHR:
         {
            getline(Minp,s);
            apCalcDef->SetItEqResThr(midas::util::FromString<Nb>(s));
            break;
         }
         case ITEQBREAKDIM:
         {
            getline(Minp,s);
            apCalcDef->SetItEqBreakDim(midas::util::FromString<In>(s));
            break;
         }
         case ITEQMAXIT:
         {
            getline(Minp,s);
            apCalcDef->SetItEqMaxIt(midas::util::FromString<In>(s));
            break;
         }
         case OVERLAPSUMMIN:
         {
            getline(Minp,s);
            apCalcDef->SetOverlapSumMin(midas::util::FromString<Nb>(s));
            break;
         }
         case OVERLAPNMAX:
         {
            getline(Minp,s);
            apCalcDef->SetOverlapNMax(midas::util::FromString<In>(s));
            break;
         }
         case ALREADYMASSWEIGHTED:
         {
            apCalcDef->SetAlreadyMassWeighted(true);
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
			default:
         {
            Mout  << "Keyword " << s_orig 
                  << " is not a FreqAna level "
                  +std::to_string(input_level)+
                  " input keyword - input flag ignored!" 
                  << std::endl;
            MIDASERROR("Check your input please.");
         }
      }
   }
	return s;
}


