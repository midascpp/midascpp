/**
************************************************************************
* 
* @file                ModSysCalcDef.h
*
* 
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for generation of coordinates
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MODSYSCALCDEF_H
#define MODSYSCALCDEF_H

// std headers
#include <string>
#include <vector>

// midas headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "input/TransformPotCalcDef.h"
#include "input/FalconCalcDef.h"
#include "input/FreqAnaCalcDef.h"
#include "input/LhaCalcDef.h"
#include "input/NormalCoordCalcDef.h"

namespace midas::input
{

enum class VibCoordScheme: int
{  ERROR = 0
,  ROTCOORD                   // Rotate rectiliear coordinates to obtain localized, optimized, HOLC, etc. coordiates
,  FALCON                     // Obtain FALCON coordinates
,  ROTATEMOLECULE             // Rotate the molecule
,  FREQANA                    // Do frequency analysis
,  TRANSFORMPOT               // Transform potential surface
,  LHA                        // Diagonalize and transform harmonic potentials (LHA = Local Harmonic Approximation)
,  NORMALCOORDINATES          // Diagonalize a given input hessian and calculate the normal coordinates
};

/* ************************************************************************
************************************************************************ */
class ModSysCalcDef
{
   CREATE_VARIABLE(bool, DoGeoOpt, false);
   CREATE_VARIABLE(bool, DoVibCoordOpt, false);
   CREATE_VARIABLE(bool, RemoveGlobalTR, false);
   CREATE_VARIABLE(bool, RotateMolecule, false);
   CREATE_VARIABLE(bool, SymmetryDefined, false);
   CREATE_VARIABLE(std::string, Axis, std::string{"x"});
   CREATE_VARIABLE(std::string, PointGroup, std::string{""});
   CREATE_VARIABLE(std::string, SymmetryAxis, std::string{""});
   CREATE_VARIABLE(Nb, Angle, C_0);

   private:
      std::vector<std::string>     mSystemNames;
      VibCoordScheme               mVibCoordScheme;
      RotCoordCalcDef              mRotCoordCalcDef;
      FalconCalcDef                mFalconCalcDef;
      FreqAnaCalcDef               mFreqAnaCalcDef;
      TransformPotCalcDef          mTransformPotCalcDef;
      LhaCalcDef                   mLhaCalcDef;
      NormalCoordCalcDef           mNormalCoordDef;
   public:

      // Set stuff
      void SetVibCoordScheme(const std::string&);
      void ReserveSystemNames(const In& arIn)                     {mSystemNames.reserve(arIn);}
      void AddSysName(const std::string& arName)                  {mSystemNames.push_back(arName);}

      void AddAxisAngle(const std::string& arAxis, Nb arAngle)    
      {
         Mout << arAxis << " " << arAngle << std::endl; 
         mAxis = arAxis; 
         mAngle = arAngle;
      } 
   

      //Get stuff
      bool DoUpdatePotential() {return (mFalconCalcDef.GetpRotCoordCalcDef()->NMeasures() > 0);}

      // Get stuff
      FreqAnaCalcDef* GetpFreqAnaCalcDef()                        {return &mFreqAnaCalcDef;}
      FalconCalcDef* GetpFalconCalcDef()                          {return &mFalconCalcDef;}
      RotCoordCalcDef* GetpRotCoordCalcDef()                      {return &mRotCoordCalcDef;}
      TransformPotCalcDef* GetpTransformPotCalcDef()              {return &mTransformPotCalcDef;}
      const LhaCalcDef* const pLhaCalcDef() const                 {return &mLhaCalcDef;}
      LhaCalcDef* pLhaCalcDef()                                   {return &mLhaCalcDef;}
      NormalCoordCalcDef& GetNormalCoordCalcDef()                 {return mNormalCoordDef;}
      const NormalCoordCalcDef& GetpNormalCoordCalcDef() const    {return mNormalCoordDef;}
      VibCoordScheme GetVibCoordScheme()                          {return mVibCoordScheme;}
      const std::vector<std::string>& GetSysNames()               {return mSystemNames;} 
};

} // namespace midas::input
#endif //MODSYSCALCDEF_H
