/**
************************************************************************
* 
* @file                TotalRspFunc.cc
*
* Created:             11-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TotalRspFunc datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Link to Standard Headers
#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/TotalRspFunc.h"


typedef string::size_type Sst;

/**
* Construct a default definition of a  calculation
* */
TotalRspFunc::TotalRspFunc(In aNorder)
{
   mNorder                              = aNorder;
   mHasBeenEval                         = false; 
   mValue                               = C_0; 
   mZPVC                                = C_0; 
   mPVLPlus                             = C_0; 
   mPVLMinus                            = C_0; 
   mPVQ                                 = C_0; 
   mRspOps.clear();
   mRspFrqs.clear();
   mRspOps.resize(abs(aNorder) % I_10);
   mRspFrqs.resize(abs(aNorder) % I_10);
}
/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const TotalRspFunc& arTotalRspFunc)
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, arOut);
   arOut.setf(ios::showpoint);

   In n_ord = arTotalRspFunc.GetNorder();
   arOut << "<<";
   In AbsOrder=abs(n_ord);
   for (In i=I_0;i<AbsOrder;i++)
   {
      arOut <<arTotalRspFunc.GetRspOp(i);
      if (i!=abs(n_ord)-I_1 && n_ord!=-I_11) 
      {
         if (i>I_0) 
         {
            arOut << ",";
         }
         else
            arOut << ";";
      }

   }
   arOut << ">>";
   if (abs(n_ord) >I_1)
   { 
      arOut << "(";
      for (In i=I_1;i<abs(n_ord);i++)
      {
         arOut << arTotalRspFunc.GetRspFrq(i);
         if (i!=abs(n_ord)-I_1) arOut << ",";
      }
      arOut << ")";
   }
   if (arTotalRspFunc.mHasBeenEval) {
      arOut << " = " << arTotalRspFunc.mValue << endl;
      arOut << "ZPVC = " << arTotalRspFunc.mZPVC << " PVQ = " << arTotalRspFunc.mPVQ 
            << " PVL+ = " << arTotalRspFunc.mPVLPlus
            << " PVL- = " << arTotalRspFunc.mPVLMinus;
   }

   return arOut;
}
