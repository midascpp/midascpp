/**
************************************************************************
* 
* @file                GenFitBasisCalcDef.cc
*
* Created:             29-05-2018
*
* Author:              Emil Lund Klinting (klintg@chem.au.dk)
*
* Short Description:   Calculation definitions for general fit-bases
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

//std headers
#include <vector>
#include <string>

// midas headers
#include "input/GenFitBasisCalcDef.h"

/**
 *
**/
GenFitBasisCalcDef::GenFitBasisCalcDef()
{
   mFitModes               = {-I_1};
   mNonLinOptStartGuess    = {C_0, C_I_10, C_I_10};
   mNonLinOptThr           = C_I_10_8;
   mFitProperties          = {"ENERGY"};
}

/**
 * Reorders the list of non-linear fit function parameters to have Q as the first variable
**/
std::vector<std::string> GenFitBasisCalcDef::ReorderParams
   (  const std::string& aFuncName
   ,  const std::string& aFileName
   )
{
   size_t left_par_poss  = aFuncName.find('(');
   size_t right_par_poss = aFuncName.find(')');
   
   if (  std::string::npos == left_par_poss 
      || std::string::npos == right_par_poss 
      || left_par_poss > right_par_poss
      )
   {
      MIDASERROR("Something wrong when reading function name " + aFuncName + " in file " + aFileName);
   }
   
   std::vector<std::string> params = midas::util::StringVectorFromString(aFuncName.substr(left_par_poss + 1, right_par_poss - left_par_poss - 1), ',');
   if (params.empty())
   {
      MIDASERROR("No variables set in optimization function : " + aFuncName);
   }
   if ("Q" == params.front())
   {
      return params;
   }

   auto it = std::find(params.begin(), params.end(), "Q");
   if (params.end() == it)
   {
      MIDASERROR("Variable Q not included in optimization function : " + aFuncName + " remember that Q is the coordinate used for the surface itself");
   }
   params.erase(it);
   params.insert(params.begin(), "Q");
   
   return params;
}

/**
 *
**/
std::vector<Nb> GenFitBasisCalcDef::GetStartGuess
   (  const std::string& aVecStr
   ,  const std::string& aFileName
   )
{
   if (  aVecStr.front() != '(' 
      || aVecStr.back() != ')'
      )
   {
      MIDASERROR("Could not read the starting guess : " + aVecStr + ", check the OPTFUNC flag in file " + aFileName);
   }
   
   return midas::util::VectorFromString<Nb>(aVecStr.substr(1, aVecStr.size() - 2), ',');
}
