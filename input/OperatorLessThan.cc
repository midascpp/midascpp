#include <string>
#include <tuple>

#include "util/conversions/FromString.h"

/**
 * Extract_integer extracts first integer of string for sorting
 * @param aFunc the function string to analyse
 * @return tuple of prefix, int, postfix
 **/
std::tuple<std::string, int, std::string> extract_integer( const std::string& aFunc
                                                         )
{
   int exponent_beg_pos = aFunc.find_first_of("0123456789");
   int exponent_end_pos = aFunc.find_first_not_of("0123456789", exponent_beg_pos);
   exponent_beg_pos = exponent_beg_pos == std::string::npos ? aFunc.size() : exponent_beg_pos;
   exponent_end_pos = exponent_end_pos == std::string::npos ? aFunc.size() : exponent_end_pos;
   
   int exponent_length = exponent_end_pos - exponent_beg_pos;

   std::string func_prefix = aFunc.substr(0, exponent_beg_pos);
   int exponent = (exponent_length ? midas::util::FromString<In>(aFunc.substr(exponent_beg_pos, exponent_length))
                                   : 0
                  );
   std::string func_postfix = aFunc.substr(exponent_end_pos);
   
   return std::tuple<std::string, int, std::string>(func_prefix, exponent, func_postfix);
}

