/**
************************************************************************
* 
* @file                TensorDecompInput.cc
*
* 
* Created:             02-10-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Input reader for the response information 
* 
* Last modified: Tue Oct 2, 2012  05:55PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/RspCalcDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/TensorDecompDef.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"

/**
* Read and check tensor decomp input.
* Read until next s = "#i..." i<aInputLevel which is returned.
* */
// MAKE INPUT FOR LEVEL AS WELL !!!111onetwo...
string TensorDecompInput(std::istream& Minp, VscfCalcDef& arCalcDef, In aInputLevel, DECOMPCALCTYPE aType)
{
   TensorDecompDef tensor_decomp_def;
   tensor_decomp_def.SetType(aType);
   In input_level = aInputLevel;

   // Enum/map with input flags
   enum INPUT { ERROR, TYPE, CPRANK, CPALSTHRESHOLD, CPALSMAXITER, FINDCPMAXRANK, FINDCPRESTHRESHOLD,
      DECOMPTOEXCILVL, PSEUDOINVERSETHRESHOLD, SINGLECPANALYSIS, SPECIFICCPANALYSIS,
      DOCGDECOMP, SAVEDECOMP, NAME };

   const map<string,INPUT> input_word =
   { {"#"+std::to_string(input_level)+"TYPE",TYPE}
   , {"#"+std::to_string(input_level)+"CPRANK",CPRANK}
   , {"#"+std::to_string(input_level)+"CPALSTHRESHOLD",CPALSTHRESHOLD}
   , {"#"+std::to_string(input_level)+"CPALSMAXITER",CPALSMAXITER}
   , {"#"+std::to_string(input_level)+"FINDCPMAXRANK",FINDCPMAXRANK}
   , {"#"+std::to_string(input_level)+"FINDCPRESTHRESHOLD",FINDCPRESTHRESHOLD}
   , {"#"+std::to_string(input_level)+"DECOMPTOEXCILVL",DECOMPTOEXCILVL}
   , {"#"+std::to_string(input_level)+"PSEUDOINVERSETHRESHOLD",PSEUDOINVERSETHRESHOLD}
   , {"#"+std::to_string(input_level)+"SINGLECPANALYSIS",SINGLECPANALYSIS}
   , {"#"+std::to_string(input_level)+"SPECIFICCPANALYSIS",SPECIFICCPANALYSIS}
   , {"#"+std::to_string(input_level)+"DOCGDECOMP",DOCGDECOMP}
   , {"#"+std::to_string(input_level)+"SAVEDECOMP",SAVEDECOMP}
   , {"#"+std::to_string(input_level)+"NAME",NAME}
   };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s;
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s,input_level)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case TYPE:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetTypeFromString(s);
            break;
         }
         case CPRANK:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetCpRank(midas::util::FromString<In>(s));
            break;
         }
         case CPALSTHRESHOLD:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetCpAlsThreshold(midas::util::FromString<Nb>(s));
            break;
         }
         case CPALSMAXITER:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetCpAlsMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case FINDCPMAXRANK:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetFindCpMaxRank(midas::util::FromString<In>(s));
            break;
         }
         case FINDCPRESTHRESHOLD:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetFindCpResThreshold(midas::util::FromString<Nb>(s));
            break;
         }
         case DECOMPTOEXCILVL:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetDecompToExciLvl(midas::util::FromString<Nb>(s));
            break;
         }
         case PSEUDOINVERSETHRESHOLD:
         {
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetPseudoInverseThreshold(midas::util::FromString<Nb>(s));
            break;
         }
         case SINGLECPANALYSIS:
         {
            tensor_decomp_def.SetSingleCpAnalysis(true);
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetSingleCpAnalysisMaxRank(midas::util::FromString<size_t>(s));
            break;
         }
         case SPECIFICCPANALYSIS:
         {
            tensor_decomp_def.SetSpecificCpAnalysis(true);
            midas::input::GetLine(Minp,s);
            istringstream input_s(s);
            while(!input_s.eof())
            {
               size_t rank;
               input_s >> rank;
               tensor_decomp_def.PushBackSpecificCpAnalysisRanks(rank);
            }
            break;
         }
         case DOCGDECOMP:
         {
            tensor_decomp_def.SetDoCgDecomp(true);
            break;
         }
         case SAVEDECOMP:
         {
            tensor_decomp_def.SetSaveDecomp(true);
            break;
         }
         case NAME:
         {  
            //PeekHashtag(input_level,"NAME",Minp,s);
            midas::input::GetLine(Minp,s);
            tensor_decomp_def.SetName(s);
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a TensorDecomp level 4 Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }
   
   arCalcDef.PushBackTensorDecompDef(tensor_decomp_def);

   //Mout << " returning s = " << s << endl;
   return s;
}
