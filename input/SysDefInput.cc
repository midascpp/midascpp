/**
************************************************************************
* 
* @file                SysDefInput.cc
*
* 
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the modification of the system
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// midas headers:
#include "input/Input.h"
#include "input/GetLine.h"
#include "input/SystemDef.h"
#include "input/FindKeyword.h"
#include "input/FileConversionInput.h"

std::string SysDefInput(std::istream& Minp, const In& arInputLevel)
{
   gSystemDefs.push_back(SystemDef());
   SystemDef& calc_def = gSystemDefs[gSystemDefs.size() - I_1];   // new_oper is a reference to the new operator
   In input_level = arInputLevel + I_1;
   std::string s;                                                 // To be returned when we are done reading this level
   enum INPUT {ERROR,NAME,MOLECULEFILE,FRAGMENTTYPE,CALCMODES,ADDLOCALTRANSROT}; 
   const std::map<std::string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"NAME",NAME},
      {"#"+std::to_string(input_level)+"MOLECULEFILE",MOLECULEFILE},
      {"#"+std::to_string(input_level)+"FRAGMENTTYPE",FRAGMENTTYPE},
      {"#"+std::to_string(input_level)+"CALCMODES",CALCMODES},
      {"#"+std::to_string(input_level)+"ADDLOCALTRANSROT",ADDLOCALTRANSROT},
   };

   bool already_read=false;
   // Readin until input levels point up
   while (  (already_read || midas::input::GetLine(Minp,s)) 
         && !(s.substr(I_0, I_1) == "#" 
         && atoi(s.substr(I_1, I_1).c_str()) < input_level)
         )  
   {
      std::string s_orig = s;
      s = midas::input::ParseInput(s);
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      switch(input)
      {
         case ADDLOCALTRANSROT:
         {
            calc_def.SetAddLocalTransRot();
            break;
         }
         case NAME:
         {
            midas::input::GetLine(Minp, s);
            calc_def.SetName(s);
            break;
         }
         case CALCMODES:
         {
            midas::input::GetLine(Minp, s);
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper); // Transform to all upper.
            while (s.find(" ") != s.npos)                                  // Delete all blanks
            {
               s.erase(s.find(" "), I_1);
            }
            calc_def.SetCalcModeType(s);
            break;
         }
         case MOLECULEFILE:
         {
            calc_def.SetMoleculeFileInfo(ReadMoleculeFileInput(Minp, s, already_read, true));
            break;
         }
         case FRAGMENTTYPE:
         {
            midas::input::GetLine(Minp, s);
            transform(s.begin(), s.end(), s.begin(),( In(*) (In))toupper); // Transform to all upper.
            while (s.find(" ") != s.npos)                                  // Delete all blanks
            {
               s.erase(s.find(" "), I_1);
            }
            calc_def.SetFragmentType(s);
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig << " is not a SystemDef level " + std::to_string(input_level) + " Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}

