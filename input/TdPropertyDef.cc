/**
************************************************************************
*
* @file                TdPropertyDef.cc
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines properties of time-dependent
*                      wave functions.
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "input/TdPropertyDef.h"
#include "inc_gen/TypeDefs.h"
#include "input/GetLine.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"
#include "util/conversions/VectorFromString.h"

namespace midas::input
{

/**
 * Sanity check
 **/
void TdPropertyDef::SanityCheck
   (  midas::ode::OdeInfo& arOdeInfo
   ,  bool aImagTime
   )
{
   // Check that we have dense output from ODE if we request a spectrum
   if (  mProperties.find("SPECTRUM") != mProperties.end()
      && arOdeInfo.template get<In>("OUTPUTPOINTS") <= I_0
      )
   {
      MIDASERROR("Spectrum requires dense ODE output ('OUTPUTPOINTS' > 0)!");
   }

   // Check that we save points if we want auto-correlation function
   if (  arOdeInfo.template get<In>("OUTPUTPOINTS") == I_0
      && (  mProperties.find("AUTOCORR") != mProperties.end()
         || mProperties.find("HALFTIMEAUTOCORR") != mProperties.end()
         )
      )
   {
      MIDASERROR("We need to save wave functions in order to calculate autocorrelation functions! OUTPUTPOINTS must be non-zero!");
   }

   // Check that we calculate energy when doing imaginary-time propagation
   if (  aImagTime
      && mProperties.find("ENERGY") == mProperties.end()
      )
   {
      MIDASERROR("There is no point in doing imaginary-time propagation if we do not calculate energy?");
   }

   // Check that we save y vectors if we want to print the wave function.
   // Otherwise, fix the input!
   if (  (  !this->mWfOutModes.empty()
         || !this->mTwoModeDensityPairs.empty()
         )
      && !arOdeInfo.template get<bool>("DATABASESAVEVECTORS")
      )
   {
      Mout  << " Setting 'DATABASESAVEVECTORS' true in OdeInfo in order to print wave function and densities." << std::endl;
      arOdeInfo["DATABASESAVEVECTORS"] = true;
   }
}

/**
 * Read properties
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdPropertyDef::ReadProperties
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Allow the property keyword to stand on multiple lines, but also multiple properties per line.
   while (  midas::input::GetLine(arInp, arS)
         && !midas::input::IsKeyword(arS)
         )
   {
      // Separate the properties of current line
      auto string_vec = midas::util::StringVectorFromString(arS);

      // Insert properties
      for(const auto& prop : string_vec)
      {
         this->mProperties.insert(midas::input::ToUpperCase(prop));
      }
   }

   // We have read, but not processed the current keyword.
   return true;
}

/**
 * Read spectra
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdPropertyDef::ReadSpectra
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Allow the property keyword to stand on multiple lines, but also multiple spectra per line.
   while (  midas::input::GetLine(arInp, arS)
         && !midas::input::IsKeyword(arS)
         )
   {
      // Separate the properties of current line
      auto string_vec = midas::util::StringVectorFromString(arS);

      // Insert spectra
      for(const auto& spec : string_vec)
      {
         this->mSpectra.insert(midas::input::ToUpperCase(spec));
      }
   }

   // We have read, but not processed the current keyword.
   return true;
}

/**
 * Add property
 * @param aProp
 **/
void TdPropertyDef::AddProperty
   (  std::string aProp
   )
{
   this->mProperties.insert(aProp);
}

/**
 * Read opers for expt val
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdPropertyDef::ReadExptValOpers
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Allow the property keyword to stand on multiple lines, but also multiple properties per line.
   while (  midas::input::GetLine(arInp, arS)
         && !midas::input::IsKeyword(arS)
         )
   {
      // Separate the properties of current line
      auto string_vec = midas::util::StringVectorFromString(arS);

      // Insert properties
      for(const auto& prop : string_vec)
      {
         this->mExptValOpers.insert(prop);
      }
   }

   // We have read, but not processed the current keyword.
   return true;
}

/**
 * Read statistics
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdPropertyDef::ReadStats
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Allow the statistics keyword to stand on multiple lines, but also multiple statistics per line.
   while (  midas::input::GetLine(arInp, arS)
         && !midas::input::IsKeyword(arS)
         )
   {
      // Separate the statistics of current line
      auto string_vec = midas::util::StringVectorFromString(arS);

      // Insert statistics
      for(const auto& prop : string_vec)
      {
         this->mStats.insert(midas::input::ToUpperCase(prop));
      }
   }

   // We have read, but not processed the current keyword.
   return true;
}

/**
 * Read two-mode-density pairs
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdPropertyDef::ReadTwoModeDensityPairs
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Allow the property keyword to stand on multiple lines, but also multiple properties per line.
   while (  midas::input::GetLine(arInp, arS)
         && !midas::input::IsKeyword(arS)
         )
   {
      // Separate the properties of current line
      auto vec = midas::util::StringVectorFromString(arS);

      // Check
      if (  vec.size() != 2
         )
      {
         MIDASERROR("TdPropertyDef::ReadTwoModeDensityPairs: Only specify two modes per line!");
      }

      // Get set for this line
      std::set<std::string> set(vec.begin(), vec.end());

      // Insert into set<set<In>>
      this->mTwoModeDensityPairs.emplace(std::move(set));
   }

   return true;
}


} /* namespace midas::input */
