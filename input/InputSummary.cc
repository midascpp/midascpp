/**
************************************************************************
*
* @file                InputSummary.cc
*
*
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the midas program.
*
* Last modified: Mon Jul 12, 2010  02:06PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "test/Test.h"
#include "util/Io.h"
#include "nuclei/Nuclei.h"
#include "input/Input.h"
#include "input/OneModeBasDef.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/EcorCalcDef.h"
#include "input/RspCalcDef.h"
#include "input/RspFunc.h"
#include "input/MolStruct.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/MultiIndex.h"
#include "util/FileSystem.h"

using std::string;
using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;

/**
* Summary of input.
* */
void InputSummary()
{
   Mout << std::endl;
   Out72Char(Mout,'+','=','+');
   Out72Char(Mout,'|',' ','|');
   string s1 = " Summary of Midas input and initialization settings";
   OneArgOut72(Mout,s1,'|');
   Out72Char(Mout,'|',' ','|');

   // Settings of general flags.
   Out72Char(Mout,'+','=','+');
   s1 = " Level 0 flags                ";
   OneArgOut72(Mout,s1,'|');
   Out72Char(Mout,'|','-','|');
   s1 = " Do Test ";
   TwoArgOut72(Mout,s1,gDoTest);
   s1 = " Do Pes  ";
   TwoArgOut72(Mout,s1,gDoPes);
   s1 = " Do Vscf ";
   TwoArgOut72(Mout,s1,gDoVscf);
   s1 = " Do Vcc  ";
   TwoArgOut72(Mout,s1,gDoVcc);
   s1 = " Do Ecor ";
   TwoArgOut72(Mout,s1,gDoEcor);
   s1 = " Do TensorDecomp ";
   TwoArgOut72(Mout,s1,gDoTdecomp);

   //  General section
   if (gIoLevel > I_8)
   {
      Out72Char(Mout,'+','=','+');
      s1 = " General flags           ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      s1 = " General Io Level        ";
      TwoArgOut72(Mout,s1,gIoLevel);
      s1 = " Max. Buffer Size        ";
      TwoArgOut72(Mout,s1,gMaxBufSize);
      s1 = " Max. File Size          ";
      TwoArgOut72(Mout,s1,gMaxFileSize);
      s1 = " Timings flag            ";
      TwoArgOut72(Mout,s1,gTime);
      s1 = " Debug flag            ";
      TwoArgOut72(Mout,s1,gDebug);
      s1 = " Analysis directory ";
      TwoArgOut72(Mout,s1,gAnalysisDir);
    }
   //midas::filesystem::Mkdir(gAnalysisDir);


   // Test section
   if (gDoTest)
   {
      Out72Char(Mout,'+','=','+');
      s1 = " Test flags                ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      s1 = " Test Io Level                 ";
      TwoArgOut72(Mout,s1,gTestIoLevel);
      for(auto&& ident_bool: gTestDrivers.Summary())
      {
         s1 = " "+ident_bool.first+" ";
         TwoArgOut72(Mout, s1, ident_bool.second);
      }
   }

   // Pes section
   if (gDoPes && gPesIoLevel > I_8)
   {
      Out72Char(Mout,'+','=','+');
      s1 = " Pes flags                ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      s1 = " Pes Io Level                 ";
      TwoArgOut72(Mout,s1,gPesIoLevel);
      s1 = " Number of dalton wf inputs";
      TwoArgOut72(Mout,s1,gPesCalcDef.GetmPesNumDalton());
      s1 = " Number of sets of nuclei on input";
      TwoArgOut72(Mout,s1,gPesCalcDef.GetmPesNumNucInp());
   }

   // Vib section
   if (gDoVib && gVibIoLevel > I_8)
   {
      Out72Char(Mout,'+','=','+');
      s1 = " Vibrational structure calculation flags ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      s1 = " Vib Io Level                 ";
      TwoArgOut72(Mout,s1,gVibIoLevel);

      // Oper section
      if (gDoOper && gOperIoLevel > I_8)
      {
         Out72Char(Mout,'+','-','+');
         s1 = " Oper flags                ";
         OneArgOut72(Mout,s1,'|');
         Out72Char(Mout,'|','-','|');
         s1 = " Oper Io Level                 ";
         TwoArgOut72(Mout,s1,gOperIoLevel);
         s1 = " Number of operators           ";
         TwoArgOut72(Mout,s1,gOperatorDefs.GetNrOfOpers());
         for (In i_oper =0;i_oper<gOperatorDefs.GetNrOfOpers();i_oper++)
         {
            s1 = " Operator Name                ";
            TwoArgOut72(Mout,s1,gOperatorDefs[i_oper].Name());
            //s1 = " Operator Type                ";
            //TwoArgOut72(Mout,s1,gOperatorDefs[i_oper].Type());
            s1 = " Number of operator terms      ";
            TwoArgOut72(Mout,s1,gOperatorDefs[i_oper].Nterms());
            s1 = " Number of modes in operator   ";
            TwoArgOut72(Mout,s1,gOperatorDefs[i_oper].NmodesInOp());
            s1 = " Ignore only Qi terms          ";
            TwoArgOut72(Mout,s1,gOperatorDefs[i_oper].IgnQiOnly());
         }
         s1 = " Number of modes               ";
         TwoArgOut72(Mout,s1,gOperatorDefs.GetNrOfModes());
      }
      // Basis section
      if (gBasisIoLevel > I_8)
      {
         Out72Char(Mout,'+','-','+');
         s1 = " Basis flags                ";
         OneArgOut72(Mout,s1,'|');
         Out72Char(Mout,'|','-','|');
         s1 = " Basis Io Level                 ";
         TwoArgOut72(Mout,s1,gBasisIoLevel);
         s1 = " Number of basis sets          ";
         TwoArgOut72(Mout,s1,gBasis.size());
         for (In i_basis = I_0; i_basis < gBasis.size(); i_basis++)
         {
            s1 = " Basis Name                ";
            TwoArgOut72(Mout,s1,gBasis[i_basis].GetmBasName());
            s1 = " Basis Type                ";
            TwoArgOut72(Mout,s1,gBasis[i_basis].GetmBasSetType());
         }
      }
      // Vscf  section
      if (gDoVscf && gVibIoLevel > I_8)
      {
         Out72Char(Mout,'+','-','+');
         s1 = " Vscf flags                ";
         OneArgOut72(Mout,s1,'|');
         Out72Char(Mout,'|','-','|');
         s1 = " Number of vscf calculations   ";
         TwoArgOut72(Mout,s1,gVscfCalcDef.size());
         for (In i_calc =0;i_calc<gVscfCalcDef.size();i_calc++)
         {
            s1 = " Name                ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetName());
            s1 = " Oper                ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].Oper());
            s1 = " Basis               ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].Basis());
            s1 = " Int Storage         ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].IntStorage());
            s1 = " Vec Storage         ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].VecStorage());
            s1 = " Max iterations      ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetMaxIter());
            s1 = " Energy Threshold    ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetEnerThr());
            s1 = " Residual Threshold  ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetResidThr());
            s1 = " Io Level            ";
            TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].IoLevel());
            if (gVscfCalcDef[i_calc].Restart())
            {
               s1 = " Restart             ";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].Restart());
            }
            if (!gVscfCalcDef[i_calc].Save())
            {
               s1 = " Save                ";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].Save());
            }
            if (gVscfCalcDef[i_calc].DoRsp())
            {
               s1 = " Do Response calcs.  ";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].DoRsp());
               s1 = " Do Response functions.";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].DoRspFuncs());
               if (gVscfCalcDef[i_calc].DoRspFuncs())
               {
                  s1 = " Nr. of rsp functions.";
                  TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].NrspFuncs());
               }
               s1 = " Do Response eigeneq.";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].DoRspEig());
               if (gVscfCalcDef[i_calc].DoRspEig())
               {
                  s1 = " Nr. of rsp eigenvals.";
                  TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetRspNeig());
               }
               s1 = " Response Resid. Thr.";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetRspItEqResidThr());
               s1 = " Response Ener.  Thr.";
               TwoArgOut72(Mout,s1,gVscfCalcDef[i_calc].GetRspItEqEnerThr());
            }
         }
      }

      // Vcc  section
      if (gDoVcc && gVibIoLevel > I_8)
      {
         Out72Char(Mout,'+','-','+');
         s1 = " Vcc flags                ";
         OneArgOut72(Mout,s1,'|');
         Out72Char(Mout,'|','-','|');
         s1 = " Number of vcc calculations   ";
         TwoArgOut72(Mout,s1,gVccCalcDef.size());
         for (In i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
         {
            s1 = " Name                  ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].GetName());
            s1 = " Oper                  ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].Oper());
            s1 = " Basis               ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].Basis());
            if ((gVccCalcDef[i_calc].Vci()))
               TwoArgOut72(Mout," Vci calc. for #of roots ",gVccCalcDef[i_calc].VciRoots());
            if ((gVccCalcDef[i_calc].Vmp()))
               TwoArgOut72(Mout," Vmp calc. up to order   ",gVccCalcDef[i_calc].VmpMaxOrder());
            if ((gVccCalcDef[i_calc].Vcc()))
               TwoArgOut72(Mout," Vcc calc.               ",gVccCalcDef[i_calc].Vcc());
            s1 = " Modals from File    ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].GetModalFileName());
            s1 = " Max Exci Level      ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].MaxExciLevel());
            s1 = " Primitive Basis     ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].UsePrimitiveBasis());
            s1 = " Int Storage         ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].IntStorage());
            s1 = " Vec Storage         ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].VecStorage());
            s1 = " Max iterations      ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].GetMaxIter());
            s1 = " Energy Threshold    ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].GetEnerThr());
            s1 = " Residual Threshold  ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].GetResidThr());
            s1 = " Io Level            ";
            TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].IoLevel());
            if (gVccCalcDef[i_calc].Restart())
            {
               s1 = " Restart             ";
               TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].Restart());
            }
            if (!gVccCalcDef[i_calc].Save())
            {
               s1 = " Save                ";
               TwoArgOut72(Mout,s1,gVccCalcDef[i_calc].Save());
            }
         }
      }

   }
   // Elec  section
   if (gDoElec)
   {
      Out72Char(Mout,'+','=','+');
      s1 = " Electronic structure calculation flags ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      s1 = " Elec Io Level                 ";
      TwoArgOut72(Mout,s1,gElecIoLevel);

      // Basis section
      Out72Char(Mout,'+','-','+');
      s1 = " Basis flags                ";
      OneArgOut72(Mout,s1,'|');
      Out72Char(Mout,'|','-','|');
      //s1 = " Global Basis Io Level      ";
      //TwoArgOut72(Mout,s1,gElecBasIoLevel);
      //s1 = " Number of basis sets          ";
      //TwoArgOut72(Mout,s1,gElecBas.size());
      //for (In i_basis =0;i_basis<gElecBas.size();i_basis++)
      //{
      //   s1 = " Basis Name                ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].Name());
      //   s1 = " Basis Type                ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].Type());
      //   s1 = " Local Io Level            ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].IoLevel());
      //   s1 = " B spline order            ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].Order());
      //   s1 = " Number of B splines (1D)  ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetNBas());
      //   s1 = " Box definition: ";
      //   OneArgOut72(Mout,s1,'|');
      //   s1 = " xmin: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(0)[0],'|');
      //   s1 = " xmax: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(0)[1]);
      //   s1 = " ymin: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(1)[0]);
      //   s1 = " ymax: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(1)[1]);
      //   s1 = " zmin: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(2)[0]);
      //   s1 = " zmax: ";
      //   TwoArgOut72(Mout,s1,gElecBas[i_basis].GetBoxInDirection(2)[1]);
      ////Integral section
      //   Out72Char(Mout,'+','-','+');
      //   s1 = " Integral flags                ";
      //   OneArgOut72(Mout,s1,'|');
      //   Out72Char(Mout,'|','-','|');
      //   if ( gElecBas[i_basis].Type() == "bspline" )
      //   {
      //     s1 = " Abs Integral threshold   ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetIntegralThresAbs());
      //     s1 = " One electron small order    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetOneSmallOrder());
      //     s1 = " One electron large order    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetOneLargeOrder());
      //     s1 = " One electron fitsegments    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetOneFitSegments());
      //     s1 = " One electron fitorder   ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetOneFitOrder());
      //     s1 = " One electron fitstartpol    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetOneFitStartPol());
      //     s1 = " Two electron small order    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetTwoSmallOrder());
      //     s1 = " Two electron large order    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetTwoLargeOrder());
      //     s1 = " Two electron fitsegments    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetTwoFitSegments());
      //     s1 = " Two electron fitorder   ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetTwoFitOrder());
      //     s1 = " Two electron fitstartpol    ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetTwoFitStartPol());
      //     s1 = " Fitpoints in each segment  ";
      //     TwoArgOut72(Mout,s1,gElecIntegral[i_basis].GetFitPoints());
      //   }
      //}
      // MolStruct section
      Out72Char(Mout,'+','-','+');
      s1 = " Molecular Structure flags                ";
      OneArgOut72(Mout,s1,'|');
      s1 = " Number of nuclei:  ";
      TwoArgOut72(Mout,s1,gMolStruct.NumberOfNuclei());
      if (gMolStruct.NumberOfNuclei() == 0 ) {
         OneArgOut72(Mout,"Particle in a box calculation",'|');
      }
      for (In inuc=I_0; inuc<gMolStruct.NumberOfNuclei(); inuc++)
      {
        MidasVector coord(I_3);
        coord=gMolStruct.GetNucleusCoordinates(inuc);
        s1 = " Xnuc "+std::to_string(inuc+1);
        TwoArgOut72(Mout,s1,coord[0]);
        s1 = " Ynuc "+std::to_string(inuc+1);
        TwoArgOut72(Mout,s1,coord[1]);
        s1 = " Znuc "+std::to_string(inuc+1);
        TwoArgOut72(Mout,s1,coord[2]);
      }

      // Ecor section
      if (gDoEcor)
      {
         Out72Char(Mout,'+','-','+');
         s1 = " Ecor flags               ";
         OneArgOut72(Mout,s1,'|');
         Out72Char(Mout,'|','-','|');
         s1 = " Number of Ecor calculations  ";
         TwoArgOut72(Mout,s1,gEcorCalcDef.size());
         for (In i_calc =0;i_calc<gEcorCalcDef.size();i_calc++)
         {
            s1 = " Name                ";
            TwoArgOut72(Mout,s1,gEcorCalcDef[i_calc].GetName());
            s1 = " Io Level            ";
            TwoArgOut72(Mout,s1,gEcorCalcDef[i_calc].GetIoLevel());
            s1 = " Maximum macro iterations ";
            TwoArgOut72(Mout,s1,gEcorCalcDef[i_calc].GetMacroMaxIter());
         }
      }
   }

   Out72Char(Mout,'+','=','+');
   Mout << '\n';
   Mout << endl;
   if (gOperIoLevel > I_1)
   {
      for (In i_oper = I_0; i_oper < gOperatorDefs.GetNrOfOpers(); i_oper++)
      {
         if (gOperIoLevel > I_6)
         {
            Mout << "\n Operator summary for operator: " << gOperatorDefs[i_oper].Name() << std::endl;

            if (gOperIoLevel > I_10)
            {
               Mout << " Local Mode nrs:  ";
               for (In i_op_mode = I_0; i_op_mode < gOperatorDefs[i_oper].NmodesInOp(); i_op_mode++)
               {
                  Mout << setw(3) << i_op_mode;
               }
               Mout << std::endl;

               Mout << " Global Mode nrs: ";
               for (In i_op_mode = I_0; i_op_mode<gOperatorDefs[i_oper].NmodesInOp(); i_op_mode++)
               {
                  Mout << setw(3) << gOperatorDefs[i_oper].GetGlobalModeNr(i_op_mode);
               }
               Mout << std::endl;
            }
            Out72Char(Mout,'+','=','+');
            Mout << " Mode m | m Label    | Loc. Oper. nr.  | Operator description " << std::endl;
            Out72Char(Mout,'+','=','+');
            for (LocalModeNr i_op_mode = I_0; i_op_mode < gOperatorDefs[i_oper].NmodesInOp(); i_op_mode++)
            {
               GlobalModeNr i_mode = gOperatorDefs[i_oper].GetGlobalModeNr(i_op_mode);
               for (In i_op = I_0; i_op < gOperatorDefs[i_oper].NrOneModeOpers(i_op_mode); i_op++)
               {
                  Mout.width(7);
                  Mout << i_mode << " | ";
                  Mout.width(10);
                  Mout << gOperatorDefs.GetModeLabel(i_mode) << " | ";
                  Mout.width(15);
                  Mout << i_op << " | ";
                  Mout << gOperatorDefs.GetOneModeOperDescription(gOperatorDefs[i_oper].GetOneModeOperGlobalNr(i_op_mode, i_op)) << std::endl;
               }
               if (i_op_mode != gOperatorDefs[i_oper].NmodesInOp() - I_1)
               {
                  Out72Char(Mout,'-','-','-');
               }
            }
            Out72Char(Mout,'+','=','+');
            Mout << " The number of terms is: " << gOperatorDefs[i_oper].Nterms() << std::endl;
         }
         if (gOperIoLevel > I_13)
         {
            Mout << " The operator in terms of coefficients and one-mode operators is " << endl;
            for (In i_term=0;i_term<gOperatorDefs[i_oper].Nterms();i_term++)
            {
               Mout.width(24);
               Mout.setf(ios::scientific);
               Mout.setf(ios::uppercase);
               midas::stream::ScopedPrecision(15, Mout);
               Mout.setf(ios::showpoint);
               Mout << right << gOperatorDefs[i_oper].Coef(i_term);
               Mout << "*" << gOperatorDefs[i_oper].GetOperProd(i_term) << endl;
            }
         }
      }
   }
   if (gBasisIoLevel > I_7)
   {
      for (In i_basis = I_0; i_basis < gBasis.size(); i_basis++)
      {
         Mout << "\n Summary for basis sets: " << gBasis[i_basis].GetmBasName() << std::endl;
         if (gBasis[i_basis].GetmUseHoBasis())
         {
            Mout << " Basis generated from HO part of potential" << std::endl;
         }
         else if (gBasis[i_basis].GetmAutoGenBasis())
         {
            std::string s_bas = "";
            if (gBasis[i_basis].GetmUseGaussianBasis())
            {
               s_bas = " Gaussian basis set";
            }
            else if (gBasis[i_basis].GetmUseBsplineBasis())
            {
               s_bas = " B-spline basis set";
            }

            if (gBasis[i_basis].GetmUsePrimBasisDens())
            {
               s_bas += " from density value(s)";
            }
            else
            {
               s_bas += " from Nbas value(s)";
            }
            Mout << s_bas << std::endl;

            if (gBasis[i_basis].GetmUsePrimBasisDens())
            {
               Mout << " Basis set density per mode: " << gBasis[i_basis].GetmPrimBasisDens() << std::endl;
            }
            else
            {
               Mout << " Number of basis functions per mode: " << gBasis[i_basis].GetmNoPrimBasisFunctions() << std::endl;
            }
         }
         else
         {
            Mout << " Table of one mode basis in basis " << std::endl;
            Out72Char(Mout,'+','=','+');
            Mout << " Mode m | mode-Label | Basis Type |  Nbas | Aux Defs. " << std::endl;
            Out72Char(Mout,'+','=','+');
            for (In i_bas_mode = I_0; i_bas_mode < gBasis[i_basis].GetmNoModesInBas(); i_bas_mode++)
            {
               OneModeBasDef bas_def(gBasis[i_basis].GetBasDefForLocalMode(i_bas_mode));
               Mout << right;
               Mout.width(7);
               Mout << bas_def.GetGlobalModeNr() << " | ";
               Mout.width(10);
               Mout << gOperatorDefs.GetModeLabel(bas_def.GetGlobalModeNr()) << " | ";
               Mout.width(10);
               Mout << bas_def.Type() << " | ";
               Mout.width(5);
               Mout << bas_def.Nbas() << " | ";
               Mout << left;
               Mout << bas_def.Aux() << std::endl;

               // This section is only for checking some BasDef utils.
               GlobalModeNr ig = bas_def.GetGlobalModeNr();
               OneModeBasDef bas_def2(gBasis[i_basis].GetBasDefForGlobalMode(ig));
               if (bas_def2.Aux() != bas_def.Aux())
               {
                  MIDASERROR("Something is wrong!");
               }
            }
            Out72Char(Mout,'+','=','+');
         }
      }
   }
   if (gDoVscf)
   {
      for (In i_calc = I_0; i_calc < gVscfCalcDef.size(); i_calc++)
      {
         if (gVscfCalcDef[i_calc].IoLevel() > I_8)
         {
            Mout << " Vscf: " << gVscfCalcDef[i_calc].GetName() << " occupation vector: " << std::endl;
            for (In i = I_0; i < gVscfCalcDef[i_calc].GetNmodesInOcc(); i++)
            {
               Mout << " " << gVscfCalcDef[i_calc].GetOccMode(i);
            }
            Mout << std::endl;

            Mout << " Vscf: " << gVscfCalcDef[i_calc].GetName() << " response functions requested: " << std::endl;
            for (In i_rspf = I_0; i_rspf < gVscfCalcDef[i_calc].NrspFuncs(); i_rspf++)
            {
               gVscfCalcDef[i_calc].RspFuncOut(i_rspf);
            }
         }
      }
   }
   if (gDoVcc)
   {
      for (In i_calc = I_0; i_calc < gVccCalcDef.size(); i_calc++)
      {
         if (gVccCalcDef[i_calc].IoLevel() > I_8)
         {
            if (gVccCalcDef[i_calc].Vcc()) Mout << " VCC: ";
            if (gVccCalcDef[i_calc].Vci()) Mout << " VCI: ";
            if (gVccCalcDef[i_calc].Vmp()) Mout << " VMP: ";
            Mout << gVccCalcDef[i_calc].GetName() << " occupation vector: " << std::endl;
            for (In i = I_0; i < gVccCalcDef[i_calc].GetNmodesInOcc(); i++)
            {
               Mout << " " << gVccCalcDef[i_calc].GetOccMode(i);
            }
            Mout << std::endl;
            if (gVccCalcDef[i_calc].Vcc()) Mout << " VCC: ";
            if (gVccCalcDef[i_calc].Vci()) Mout << " VCI: ";
            if (gVccCalcDef[i_calc].Vmp()) Mout << " VMP: ";
            Mout << gVccCalcDef[i_calc].GetName() << " Number of Modals pr mode " << std::endl;
            for (In i = I_0; i < gVccCalcDef[i_calc].NmodesInModals(); i++)
            {
               Mout << " " << gVccCalcDef[i_calc].Nmodals(i);
            }
            Mout << std::endl;

            Mout << " Vcc: " << gVccCalcDef[i_calc].GetName() << " response functions requested: " << std::endl;
            for (In i_rspf = I_0; i_rspf < gVccCalcDef[i_calc].NrspFuncs(); i_rspf++)
            {
               gVccCalcDef[i_calc].RspFuncOut(i_rspf);
            }
         }
      }
   }
}
