/*
************************************************************************
*
* @file                McTdHInput.h
*
* Created:             13-12-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input reader for the MCTDH module
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHINPUT_H_INCLUDED
#define MCTDHINPUT_H_INCLUDED

/**
 * Read and check MCTDH input.
 * Read until next s = "$i..." i<3 which is returned.
 **/
std::string McTdHInput
   (  std::istream& Minp
   );

#endif /* MCTDHINPUT_H_INCLUDED */
