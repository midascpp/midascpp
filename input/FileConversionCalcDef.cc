#include "FileConversionCalcDef.h"

#include "input/GetLine.h"
#include "input/IsKeyword.h"
#include "input/AbsPath.h"
#include "util/conversions/VectorFromString.h"
#include "util/FileSystem.h"

using namespace midas;

/**
 * Read potential input.
 * 
 * @param Minp
 * @param s
 * @param aNonZeroOrderTens
 * @return       Is next line already read.
 **/
bool FileConversionCalcDef::ReadPotentialInput
   (  std::istream& Minp
   ,  std::string& s
   ,  bool aNonZeroOrderTens
   )
{
   // get line with operator name
   midas::input::GetLine(Minp, s);
   midas::input::AssertNotKeyword(s);
   auto name = s;
   
   // get line with short description of operator
   midas::input::GetLine(Minp, s);
   midas::input::AssertNotKeyword(s);
   auto description = s;
   
   // If we are dealing with a non zeroth order tensor prop read a line with the order
   int order = 0;
   if(aNonZeroOrderTens)
   {
      midas::input::GetLine(Minp, s);
      midas::input::AssertNotKeyword(s);      
      order = util::FromString<int>(s);
   }
   
   // get line(s) with operator filename
   std::vector<std::string> potentials;
   std::vector<int> reference_indicies;
   for(int i = 0; i < pow(3,order); ++i)
   {
      midas::input::GetLine(Minp, s);
      midas::input::AssertNotKeyword(s);
      auto split     = util ::StringVectorFromString(s, ' ');
      auto potential = input::SearchForFile(split[0]);
      if(!filesystem::Exists(potential))
      {
         MIDASERROR("POTENTIAL FILE \""+potential+"\" DOES NOT EXIST");
      }
      potentials.push_back(potential);
      auto reference_index = (split.size() == 2 ? util::FromString<int>(split[1]) : -1);
      reference_indicies.push_back(reference_index);
   }

   
   // get line with optional bounds
   bool already_read = false;
   midas::input::GetLine(Minp, s);
   if(midas::input::IsKeyword(s))
   {
      this->AddOperator(name, description, order, potentials, "", reference_indicies);
      already_read = true;
   }
   else
   {
      auto bounds = input::SearchForFile(s);
      if(!filesystem::Exists(bounds))
      {
         MIDASERROR("BOUNDS FILE DOES NOT EXIST");
      }
      this->AddOperator(name, description, order, potentials, bounds, reference_indicies);
   }

   return already_read;
}

/**
 * Read reference values input.
 *
 * #2 REFERENCEVALUES
 *    string with filename
 *
 * @param Minp    Midas input stream.
 * @param s       Read in string, is used to read into. Will always hold the last read string from Minp.
 *
 * @return    Is next line already read.
 **/
bool FileConversionCalcDef::ReadReferenceValues
   (  std::istream& Minp
   ,  std::string& s
   )
{
   input::GetLine(Minp, s);
   input::AssertNotKeyword(s);
   
   mReferenceValues = input::SearchForFile(s);

   if(!filesystem::Exists(mReferenceValues))
   {
      MIDASERROR("REFERENCE_VALUES FILE DOES NOT EXIST");
   }

   return false;
}

/**
 * Read description input. The descritpion can span multiple lines.
 * Reads until next keyword.
 *
 * #2 DESCRIPTION
 *    A description spanning multiple lines.
 *
 * @param Minp    Midas input stream.
 * @param s       Read in string, is used to read into. Will always hold the last read string from Minp.
 *
 * @return    Is next line already read.
 **/
bool FileConversionCalcDef::ReadDescription
   (  std::istream& Minp
   ,  std::string& s
   )
{
   while(midas::input::GetLine(Minp, s) && midas::input::NotKeyword(s))
   {
      mDescription += s + "\\n";
   }

   return true;
}
