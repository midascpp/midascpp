/**
************************************************************************
* 
* @file                RspFunc.h
*
* Created:             30-04-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Base class for holding a response function
* 
* Last modified: Tue Sep 08, 2009  10:16AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RSPFUNC_H
#define RSPFUNC_H

// std headers
#include <string>
#include <vector>
#include <set>
#include <iostream> 
#include <utility> // for std::pair

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Contribution.h"
#include "input/ResponseFunction.h"

// using declarations
using std::string;
using std::vector;
using std::set;
using std::ostream;

/**
* Construct a definition of a response calculation calculation
* */
class RspFunc: public ResponseFunction
{
   using files_t = std::vector<std::pair<std::string,std::string> >;

   private:
      In             mNorder;   ///< Order of response property 
      //vector<string> Operators();   ///< Response operators as labels 
      vector<Nb>     mRspFrqs;  ///< Frequencies corresponding to operators
      Nb mGamma;
      Nb mValue;                ///< The value for the response function
      Nb mImValue;
      bool mHasBeenEval;        ///< Has the RF been evaluated?
      bool mSos;                ///< Sum over state evaluation
      In mRightState;           ///< The right state index
      In mLeftState;            ///< The left state index 
      bool mNoTwoNPlusTwo;      ///< Do not use 2n+2 rule for non-variational wave functions. 
      bool mUseMvec;            ///< Use M vector in calculation of VCC transition moments.
      bool mAsymmetric;
      bool mApproxRspFunc;
      files_t mFiles; ///< associated files, e.g. p1rsp files holding linear response vectors 
                      ///< in the case of linear response

   public:
      RspFunc(In aNorder);                          ///< Constructor
      void SetRspOrder(In aRsp); ///< Set Order of response func. 
      void SetOp(const string& arString,In aI=I_0) { Operators()[aI]=arString;}       ///< Set a particular operator  
      void SetFrq(const Nb& arFrq,In aI=I_0) {mRspFrqs[aI]= arFrq;} ///< Set a particular frequency 
      void AddOp(const string& arString) {Operators().push_back(arString);}       ///< Add operator 
      void AddFrq(const Nb& arFrq) {mRspFrqs.push_back(arFrq);} ///< Add frequency

      // Get Data
      In GetNorder() const {return mNorder;}
      In GetNopers() const {return Operators().size();}
      In GetNfrqs()  const {return mRspFrqs.size();}

      string GetRspOp(In aI) const {return Operators()[aI];} 
      Nb GetRspFrq(In aI)  const {return mRspFrqs[aI];}
      Nb Gamma() const { return mGamma; }
      
      void SetGamma(Nb aGamma) { mGamma = aGamma; }
      void SetHasBeenEval(bool aHasBeenEval) {mHasBeenEval=aHasBeenEval;}
      void SetValue(Nb aValue) {mValue = aValue;}
      void SetImValue(Nb aValue) {mImValue = aValue;}
      
      Nb Value() const {return mValue;}
      Nb ImValue() const {return mImValue;}
      bool HasBeenEval() const {return mHasBeenEval;}
      
      void SetSos(bool aB) {mSos=aB;}
      bool Sos() const {return mSos;}
      void SetRightState(In aS) {mRightState =aS;}
      In RightState() const {return mRightState;}
      void SetLeftState(In aS) {mLeftState =aS;}
      In LeftState() const {return mLeftState;}
      
      void SetNoTwoNPlusTwo(bool aB) {mNoTwoNPlusTwo=aB;}
      bool NoTwoNPlusTwo() const {return mNoTwoNPlusTwo;}
      void SetUseMvec(bool aB) {mUseMvec = aB;}
      bool UseMvec() const {return mUseMvec;}
      bool Gexp() const {return (mNorder==I_1 && mLeftState == -I_1
                     && mRightState == -I_1);} // Ground state exp. val. 
      bool Xexp() const {return (mNorder==I_1 && mLeftState != -I_1
                     && mRightState != -I_1 && mLeftState == mRightState);} 
                    // Excited state exp. val. 
      bool TmxGtoX() const {return (mNorder==I_1 && mLeftState == -I_1
                     && mRightState != -I_1);}
                    // Transition matrix element from ground to excited state
      bool TmxXtoG() const {return (mNorder==I_1 && mLeftState != -I_1
                     && mRightState == -I_1);}
                    // Transition matrix element from excited to ground state
      bool TmxXtoY() const {return (mNorder==I_1 && mLeftState != -I_1
                     && mRightState != -I_1 && mLeftState != mRightState);}
                    // Transition matrix element between 2 excited states 
      void SetDoAsymmetric(bool aB) {mAsymmetric=aB;}
      bool GetDoAsymmetric() const {return mAsymmetric;}
      void SetApproxRspFunc(bool aB) {mApproxRspFunc=aB;}
      bool GetApproxRspFunc() const {return mApproxRspFunc;}
      Contribution ToContribution();

      bool ComplexRspFunc() const { return mGamma!=C_0; }
      std::string TypeString() const;
      
      void AssociateFile(const std::vector<std::string>& opers, const std::string& label, const std::string& file);

      const files_t& Files() const { return mFiles; }

      friend bool operator<(const RspFunc& ar1,const RspFunc& ar2); ///< Compare
      friend bool operator==(const RspFunc& ar1,const RspFunc& ar2); ///< Compare
      friend ostream& operator<<(ostream&, const RspFunc&);  ///< Output overload
};

#endif

