/**
************************************************************************
* 
* @file                OneModeGaussDef.h
*
* Created:             30/05/2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode localized gaussian basis set
* 
* Last modified:       15/11/2007
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONEMODEGAUSSDEF_H
#define ONEMODEGAUSSDEF_H

// Standard Headers
#include <string>
using std::string;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

class OneModeGaussDef
{
   private:
      GlobalModeNr     mImode;                          ///< Global Mode number 
      string mBasType;                                  ///< Basis type = "LGB" etc. 
      string mBasAuxDefs;                               ///< Further Basis definition
      MidasVector mVecOfPts;                            ///< Vector for the position of the LG 
      MidasVector mVecOfExps;                           ///< Exponents for the Gaussian LBG(i) = mPreConst*exp(-mAlph*(x-mEqVec(i))**2)
      In mNbas;                                         ///< Total nr.of basis functions
      Nb mBasDens;                                      ///< Density of the LGB
   public:
      OneModeGaussDef();                                     ///< constructor as "zero"
      OneModeGaussDef(In aMode, string aType, string aAux);  ///< constructor
      OneModeGaussDef(In aMode, string aType, Nb aAlph, Nb aLeft, Nb aRight, In aNbas);  ///< constructor
                                                                
      void SetNewSize(In aNdim) 
         {mVecOfPts.SetNewSize(aNdim); mVecOfExps.SetNewSize(aNdim);}   ///< Resize the containers
      void GetVecOfExps(MidasVector& arVec) const 
         {arVec=mVecOfExps;}                                            ///< Get the exponents
      Nb GetExp(In aAdd) const {return mVecOfExps[aAdd];}               ///< Get a pecific point location
      In Nbas() const  {return mNbas;}                                  ///< Get the nr. of basis functions
      In Nint() const  {return mVecOfPts.Size()-1;}                     ///< Get the number of intervals
      Nb GetBasDens() const {return mBasDens;}                          ///< Get the "density" of the one mode basis

      //! Get the vector of LBG positions along the normal coordinate
      void GetVecOfPts(MidasVector& arVec) const
      {
         arVec.SetNewSize(mVecOfPts.Size());
         arVec = mVecOfPts;
      }
      
      Nb GetGaussPnt(In aAdd) const {return mVecOfPts[aAdd];}           ///< Get a pecific point location
      GlobalModeNr GetGlobalModeNr() const  {return mImode;}            ///< Get Global Mode number
      string Type() const {return mBasType;}                            ///< Get Type
      string Aux() const {return mBasAuxDefs;}                          ///< Get Aux info 
      void ChangeModeNr(In aGlobalModeNr) {mImode = aGlobalModeNr;}     ///< Change mode number
      
      //! Get value of basis function
      Nb EvalGaussBasis(const In& aIbas, const Nb& aQval) const;
      
      //! Get value of basis function
      void EvalGaussBasis(const In& aIbas, const MidasVector& arQval, MidasVector& arFuncVal) const;

};
#endif
