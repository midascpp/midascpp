/**
 *******************************************************************************
 * 
 * @file    TdvccInput.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Trim.h"
#include "input/Input.h"
#include "input/TdvccCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "input/FindKeyword.h"
#include "td/tdvcc/TdvccEnums.h"


/*
* Read and check TDVCC input.
* Read until next s = "$i..." i<3 which is returned.
*/
std::string TdvccInput
   (  std::istream& Minp
   )
{
   if (  gDoTdvcc
      && gDebug
      )
   {
      Mout << " I have read one Tdvcc input - reading a new Tdvcc input "<< endl;
   }

   gDoTdvcc = true;
   gTdvccCalcDef.push_back(input::TdvccCalcDef());   // Add to vector of Tdvcc-calcs.
   input::TdvccCalcDef& new_tdvcc = gTdvccCalcDef[gTdvccCalcDef.size()-I_1]; 

   // Enum/map with input flags
   enum INPUT
      {  ERROR
      ,  NAME
      ,  CORRMETHOD
      ,  MAXEXCI
      ,  EXTRANGEMODES
      ,  TRANSFORMER
      ,  MODALBASISFROMVSCF
      ,  LIMITMODALBASIS
      ,  TDMODALBASIS
      ,  VCCGS
      ,  INITSTATE
      ,  OPER
      ,  PULSE
      ,  BASIS
      ,  IMAGTIME
      ,  IMAGTIMEHAULTTHR
      ,  IOLEVEL
      ,  TIMEIT
      ,  PRINTOUTINTERVAL
      ,  COMPAREFVCIVECS
      ,  ANALYZEHERMPARTJACVCCGS
      ,  INTEGRATOR
      ,  PROPERTIES
      ,  EXPECTATIONVALUES
      ,  STATISTICS
      ,  V3CFILEPREFIX
      ,  PRINTPARAMSTOFILE
      ,  RESTARTFROMFILE
      ,  PSEUDOTDH
      // Spectra:
      ,  NOAUTOCORRCONVOLUTION
      ,  FFTPADLEVEL
      ,  NORMALIZESPECTRUM
      ,  SPECTRUMENERGYSHIFT
      ,  SPECTRUMOUTPUTSCREENING
      ,  SPECTRUMINTERVAL
      // TDMVCC
      ,  CONSTRAINT
      ,  INVDENSREGULARIZATION
      ,  GOPTREGULARIZATION
      ,  NONORTHOPROJECTOR
      ,  DISABLENONORTHOPROJECTOR
      ,  TDMVCC2AUTOCORR // MGH: Added 28/5/21
      ,  TDMODALPLOTS
      ,  FULLACTIVEBASIS
      ,  UPDATEONEMODEDENSITIES
      ,  WRITEONEMODEDENSITIES
      ,  SPACESMOOTHENING
      ,  TIMESMOOTHENING
      };
   const map<string,INPUT> input_word =
      {  {  {"#3ERROR"}, ERROR}
      ,  {  {"#3NAME"}, NAME}
      ,  {  {"#3CORRMETHOD"}, CORRMETHOD}
      ,  {  {"#3MAXEXCI"}, MAXEXCI}
      ,  {  {"#3EXTRANGEMODES"}, EXTRANGEMODES}
      ,  {  {"#3TRANSFORMER"}, TRANSFORMER}
      ,  {  {"#3MODALBASISFROMVSCF"}, MODALBASISFROMVSCF}
      ,  {  {"#3LIMITMODALBASIS"}, LIMITMODALBASIS}
      ,  {  {"#3TDMODALBASIS"}, TDMODALBASIS}
      ,  {  {"#3VCCGS"}, VCCGS}
      ,  {  {"#3INITSTATE"}, INITSTATE}
      ,  {  {"#3OPER"}, OPER}
      ,  {  {"#3PULSE"}, PULSE}
      ,  {  {"#3BASIS"}, BASIS}
      ,  {  {"#3IMAGTIME"}, IMAGTIME}
      ,  {  {"#3IMAGTIMEHAULTTHR"}, IMAGTIMEHAULTTHR}
      ,  {  {"#3IOLEVEL"}, IOLEVEL}
      ,  {  {"#3TIMEIT"}, TIMEIT}
      ,  {  {"#3PRINTOUTINTERVAL"}, PRINTOUTINTERVAL}
      ,  {  {"#3COMPAREFVCIVECS"}, COMPAREFVCIVECS}
      ,  {  {"#3ANALYZEHERMPARTJACVCCGS"}, ANALYZEHERMPARTJACVCCGS}
      ,  {  {"#3INTEGRATOR"}, INTEGRATOR}
      ,  {  {"#3PROPERTIES"}, PROPERTIES}
      ,  {  {"#3EXPECTATIONVALUES"}, EXPECTATIONVALUES}
      ,  {  {"#3STATISTICS"}, STATISTICS}
      ,  {  {"#3V3CFILEPREFIX"}, V3CFILEPREFIX}
      ,  {  {"#3PRINTPARAMSTOFILE"}, PRINTPARAMSTOFILE}
      ,  {  {"#3RESTARTFROMFILE"}, RESTARTFROMFILE}
      ,  {  {"#3PSEUDOTDH"}, PSEUDOTDH}
      ,  {  {"#3NOAUTOCORRCONVOLUTION"}, NOAUTOCORRCONVOLUTION}
      ,  {  {"#3FFTPADLEVEL"}, FFTPADLEVEL}
      ,  {  {"#3NORMALIZESPECTRUM"}, NORMALIZESPECTRUM}
      ,  {  {"#3SPECTRUMENERGYSHIFT"}, SPECTRUMENERGYSHIFT}
      ,  {  {"#3SPECTRUMOUTPUTSCREENING"}, SPECTRUMOUTPUTSCREENING}
      ,  {  {"#3SPECTRUMINTERVAL"}, SPECTRUMINTERVAL}
      ,  {  {"#3CONSTRAINT"}, CONSTRAINT}
      ,  {  {"#3INVDENSREGULARIZATION"}, INVDENSREGULARIZATION}
      ,  {  {"#3GOPTREGULARIZATION"}, GOPTREGULARIZATION}
      ,  {  {"#3NONORTHOPROJECTOR"}, NONORTHOPROJECTOR}
      ,  {  {"#3DISABLENONORTHOPROJECTOR"}, DISABLENONORTHOPROJECTOR}
      ,  {  {"#3TDMVCC2AUTOCORR"}, TDMVCC2AUTOCORR} // MGH: Added 28/5/21
      ,  {  {"#3TDMODALPLOTS"}, TDMODALPLOTS}
      ,  {  {"#3FULLACTIVEBASIS"}, FULLACTIVEBASIS}
      ,  {  {"#3UPDATEONEMODEDENSITIES"}, UPDATEONEMODEDENSITIES}
      ,  {  {"#3WRITEONEMODEDENSITIES"}, WRITEONEMODEDENSITIES}
      ,  {  {"#3SPACESMOOTHENING"}, SPACESMOOTHENING}
      ,  {  {"#3TIMESMOOTHENING"}, TIMESMOOTHENING}
      };

   std::string s = "";
   std::string s_orig = "";
   In input_level = 3;
   bool already_read = false;
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case NAME:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetName(s);
            break;
         }
         case CORRMETHOD:
         {
            midas::input::GetParsedLine(Minp, s);
            new_tdvcc.SetCorrMethod(midas::tdvcc::EnumFromString<midas::tdvcc::CorrType>(s));
            break;
         }
         case MAXEXCI:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetMaxExci(midas::util::FromString<Uin>(s));
            break;
         }
         case EXTRANGEMODES:
         {
            midas::input::GetLine(Minp, s);
            auto vec = midas::util::StringVectorFromString(s);
            std::set<std::string> set(vec.begin(), vec.end());
            new_tdvcc.SetExtRangeModeLabels(set);
            break;
         }
         case TRANSFORMER:
         {
            midas::input::GetParsedLine(Minp, s);
            new_tdvcc.SetTransformer(midas::tdvcc::EnumFromString<midas::tdvcc::TrfType>(s));
            break;
         }
         case MODALBASISFROMVSCF:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetModalBasisFromVscf(s);
            break;
         }
         case LIMITMODALBASIS:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetLimitModalBasis(midas::util::VectorFromString<Uin>(s));
            break;
         }
         case TDMODALBASIS:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetTdModalBasis(std::make_pair(true,midas::util::VectorFromString<Uin>(s)));
            break;
         }
         case VCCGS:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetVccGs(s);
            break;
         }
         case INITSTATE:
         {
            midas::input::GetParsedLine(Minp, s);
            new_tdvcc.SetInitStateType(midas::tdvcc::EnumFromString<midas::tdvcc::InitType>(s));
            break;
         }
         case OPER:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetOper(s);
            break;
         }
         case PULSE:
         {
            new_tdvcc.SetUsePulse(true);
            midas::input::GetLine(Minp, s);
            new_tdvcc.PushBackOperPulse(s);
            midas::input::GetLine(Minp, s);
            const auto v = midas::util::VectorFromString<Nb>(s);
            if (v.size() != 5)
            {
               std::stringstream ss;
               ss << "Expected v.size() == " << v.size() << " != 5; v = " << v;
               MIDASERROR(ss.str());
            }
            std::map<midas::td::GaussParamID,Nb> m;
            m[midas::td::GaussParamID::AMPL] = v.at(0);
            m[midas::td::GaussParamID::TPEAK] = v.at(1);
            m[midas::td::GaussParamID::SIGMA] = v.at(2);
            m[midas::td::GaussParamID::FREQ] = v.at(3);
            m[midas::td::GaussParamID::PHASE] = v.at(4);
            new_tdvcc.PushBackPulseSpecs(m);
            break;
         }
         case BASIS:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetBasis(s);
            break;
         }
         case IMAGTIME:
         {
            new_tdvcc.SetImagTime(true);
            break;
         }
         case IMAGTIMEHAULTTHR:
         {
            auto thr = midas::input::GetNb(Minp, s);
            new_tdvcc.SetImagTimeHaultThr(thr);
            break;
         }
         case IOLEVEL:
         {
            auto io = midas::input::GetIn(Minp, s);
            new_tdvcc.SetIoLevel(io);
            break;
         }
         case TIMEIT:
         {
            new_tdvcc.SetTimeIt(true);
            break;
         }
         case PRINTOUTINTERVAL:
         {
            auto val = midas::input::GetNb(Minp, s);
            new_tdvcc.SetPrintoutInterval(val);
            break;
         }
         case COMPAREFVCIVECS:
         {
            new_tdvcc.SetCompareFvciVecs(true);
            break;
         }
         case ANALYZEHERMPARTJACVCCGS:
         {
            Uin val = midas::input::GetIn(Minp, s);
            new_tdvcc.SetAnalyzeHermPartJacVccGs(std::make_pair(true,val));
            break;
         }
         case INTEGRATOR:
         {
            already_read = new_tdvcc.ReadOdeInfo(Minp, s);
            break;
         }
         case PROPERTIES:
         {
            already_read = new_tdvcc.ReadProperties(Minp, s);
            break;
         }
         case EXPECTATIONVALUES:
         {
            already_read = new_tdvcc.ReadExptValOpers(Minp, s);
            break;
         }
         case STATISTICS:
         {
            already_read = new_tdvcc.ReadStats(Minp, s);
            break;
         }
         case V3CFILEPREFIX:
         {
            midas::input::GetLine(Minp, s);

            MidasAssert(!midas::input::IsKeyword(s), "New keyword found under #3V3cFilePrefix!");

            new_tdvcc.SetV3cFilePrefix(s);
            break;
         }
         case PRINTPARAMSTOFILE:
         {
            new_tdvcc.SetPrintParamsToFile(true);
            break;
         }
         case RESTARTFROMFILE:
         {
            new_tdvcc.SetRestartFromFile(true);
            already_read = new_tdvcc.ReadRestartInfo(Minp, s);
            break;
         }
         case PSEUDOTDH:
         {
            new_tdvcc.SetPseudoTDH(true);
            break;
         }
         case NOAUTOCORRCONVOLUTION:
         {
            new_tdvcc.SetConvoluteAutoCorr(false);
            break;
         }
         case FFTPADLEVEL:
         {
            new_tdvcc.SetPaddingLevel(midas::input::GetIn(Minp, s));
            break;
         }
         case NORMALIZESPECTRUM:
         {
            new_tdvcc.SetNormalizeSpectrum(true);
            break;
         }
         case SPECTRUMENERGYSHIFT:
         {
            Nb e_shift = C_0;
            std::string type = "FIXED";

            midas::input::GetLine(Minp, s);
            auto s_vec = midas::util::StringVectorFromString(s);
            assert(s_vec.size() > 0);

            if (  s_vec[0] == "FIXED"
               )
            {
               assert(s_vec.size() == 2);
               type = s_vec[0];
               e_shift = midas::util::FromString<Nb>(s_vec[1]);
            }
            else if  (  s_vec[0] == "E0"
                     )
            {
               assert(s_vec.size() == 1);
               type = "E0";
               e_shift = C_0;
            }
            else
            {
               MIDASERROR("Unrecognized energy-shift type: " + s_vec[0]);
            }
            
            new_tdvcc.SetSpectrumEnergyShift(std::pair<std::string, Nb>(type, e_shift));
            break;
         }
         case SPECTRUMOUTPUTSCREENING:
         {
            Nb thresh = midas::input::GetNb(Minp, s);
            new_tdvcc.SetSpectrumOutputScreeningThresh(thresh);
            break;
         }
         case SPECTRUMINTERVAL:
         {
            midas::input::GetLine(Minp, s);
            auto [f0, f1] = midas::util::TupleFromString<Nb, Nb>(s);
            new_tdvcc.SetSpectrumInterval(std::pair<Nb, Nb>(f0, f1));
            break;
         }
         case CONSTRAINT:
         {
            midas::input::GetLine(Minp, s);
            auto s_up = midas::input::ToUpperCase(s);
            new_tdvcc.SetConstraint(midas::tdvcc::EnumFromString<midas::tdvcc::ConstraintType>(s_up));
            break;
         }
         case INVDENSREGULARIZATION:
         {
            midas::input::GetLine(Minp, s);
            auto [type, eps] = midas::util::TupleFromString<std::string, Nb>(s);
            new_tdvcc.SetInvDensRegularization(std::make_pair(midas::tdvcc::EnumFromString<midas::tdvcc::RegInverseType>(type), eps));
            break;
         }
         case GOPTREGULARIZATION:
         {
            midas::input::GetLine(Minp, s);
            auto [type, eps] = midas::util::TupleFromString<std::string, Nb>(s);
            new_tdvcc.SetGOptRegularization(std::make_pair(midas::tdvcc::EnumFromString<midas::tdvcc::RegInverseType>(type), eps));
            break;
         }
         case NONORTHOPROJECTOR:
         {
            new_tdvcc.SetNonOrthoProjector(true);
            already_read = new_tdvcc.ReadNonOrthoProjector(Minp, s);
            break;
         }
         case DISABLENONORTHOPROJECTOR:
         {
            new_tdvcc.SetNonOrthoProjector(false);
            break;
         }
         case TDMVCC2AUTOCORR:  // MGH: Added 28/5/21
         {
            already_read = new_tdvcc.ReadAutoCorrSettings(Minp, s);
            break;
         }
         case TDMODALPLOTS:
         {
            new_tdvcc.SetSaveTdModalPlots(true);
            break;
         }
         case FULLACTIVEBASIS:
         {
            new_tdvcc.SetFullActiveBasis(true);
            break;
         }
         case UPDATEONEMODEDENSITIES:
         {
            In step = midas::input::GetIn(Minp, s);
            new_tdvcc.SetUpdateOneModeDensities(true);
            new_tdvcc.SetUpdateOneModeDensitiesStep(step);
            break;
         }
         case WRITEONEMODEDENSITIES:
         {
            new_tdvcc.SetWriteOneModeDensities(true);
            break;
         }
         case SPACESMOOTHENING:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetSpaceSmoothening(midas::util::FromString<Uin>(s));
            break;
         }
         case TIMESMOOTHENING:
         {
            midas::input::GetLine(Minp, s);
            new_tdvcc.SetTimeSmoothening(midas::util::FromString<Uin>(s));
            break;
         }
         case ERROR: // Fall through to default
         default:
         {
            Mout << " Keyword " << s_orig << " is not a TDVCC level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check sanity
   new_tdvcc.SanityCheck();

   return s;
}

/**
 * Initialize Tdvcc variables
 * */
void InitializeTdvcc()
{
   gDoTdvcc = false; 
}
