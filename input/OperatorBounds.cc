#include "OperatorBounds.h"

#include "input/GetLine.h"
#include "input/IsKeyword.h"
#include "util/conversions/TupleFromString.h"
#include "mpi/FileToString.h"

/**
 * Construct operator bounds from file.
 * 
 * @param filename    Name of the bounds file.
 **/
OperatorBounds::OperatorBounds
   (  const std::string& filename
   )
{
   std::istringstream file = midas::mpi::FileToStringStream(filename);
   
   std::string s;
   while (midas::input::GetLine(file, s))
   {
      if (midas::input::IsKeyword(s))
      {
         continue;
      }
      mBounds.emplace_back(midas::util::TupleFromString<bounds_type>(s));
   }
}

/**
 * Get the operator bounds.
 *
 * @return   Return a const reference to the bounds.
 **/
const std::vector<typename OperatorBounds::bounds_type>& OperatorBounds::GetBounds
   (
   )  const
{
   return mBounds;
}
