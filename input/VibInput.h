/**
************************************************************************
* 
* @file                VibInput.h
* 
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   VibInput reader for the midas program.
* 
* Last modified: Mon Jul 12, 2010  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef VIBINPUT_H_INCLUDED
#define VIBINPUT_H_INCLUDED

// std headers
#include <string>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

//!
std::string VibInput(std::istream&, bool);

#endif /* VIBINPUT_H_INCLUDED */
