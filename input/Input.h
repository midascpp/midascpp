/**
************************************************************************
* 
* @file                Input.h
*
* Created:             28-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store input logical information 
* 
* Last modified:       17-12-2014 (C. Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <map>   
#include <list>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/ProgramSettings.h"
#include "input/OneModeBasDef.h"
#include "input/BasDef.h"
#include "input/VscfCalcDef.h"
#include "input/PesCalcDef.h"
#include "input/FitBasisCalcDef.h"
#include "input/EcorCalcDef.h"
#include "input/GeoOptCalcDef.h"
#include "input/MLCalcDef.h"
#include "input/TdecompCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/AnalysisCalcDef.h"
#include "input/TdHCalcDef.h"
#include "input/TdvccCalcDef.h"
#include "input/McTdHCalcDef.h"
#include "input/SystemDef.h"
#include "input/ModSysCalcDef.h"
#include "input/FileConversionCalcDef.h"
#include "input/SinglePointCalcDef.h"
#include "input/OpDef.h"
#include "input/OpInfo.h"
#include "input/MolStruct.h"
#include "input/TensorDecompInput.h"
#include "nuclei/AtomicData.h"
#include "test/TestDrivers.h"
#include "input/GlobalOperatorDefinitions.h"
#ifdef VAR_HAS_TINKER
#include "tinker_interface/TinkerMM.h"
#endif

// using declarations
using std::string;
using std::vector;
using std::map;
using std::list;

/**
* Declaration of Input methods .
* */
void InputDrv(const std::string& aFileName);
void ReadInput(std::istream&);
void ParseSetInfoLine(string&, OpInfo&);

/**
* Section logicals 
* */
extern bool      gDoTest;               ///< Do Test of various sections.
extern bool      gDoPes;                ///< Do Pes investigation
extern bool      gDoAnalysis;           ///< Do Analysis investigation
extern bool      gDoVib;                ///< Do vibrational structure calculation.
extern bool      gDoElec;               ///< Do electronic structure calculation.
extern bool      gDoSystem;             ///< Do modifications to the system 


/**
* General options. 
* */
extern bool          gGeneral;            ///< General
extern bool          gDebug;              ///< Debug - write out immense ammounts of shit
extern In            gIoLevel;            ///< Global IO level.
extern bool          gTime;               ///< Time whatever you do....
extern bool          gTechInfo;           ///< Gives information on the size of Nb, In and STL containers
extern In            gMaxBufSize;         ///< Maximum size of intermediate memory buffers used (DataCont)
extern In            gMaxFileSize;        ///< Maximum size of files used (DataCont)
extern bool          gTempSet;            ///< Temperature has been set - average 
extern Nb            gTempFirst;          ///< First or only Temperature 
extern Nb            gTempLast;           ///< Last Temperature 
extern In            gTempSteps;          ///< Nr. of Temperatures 
extern string        gNumLinAlg;          ///< Choice of lin.alg. methods 
extern std::string   gMainDir;            ///< Global savedir directory location
extern std::string   gSaveDir;            ///< Global savedir directory location
extern std::string   gAnalysisDir;        ///< Global analysis directory location  

/**
* Test input options. 
* */
extern In gTestIoLevel;          ///< Local test IO level (>= global IO level)
extern In gTestVectorDim;        ///< Vector dimensions used in the large Test(timing)
extern In gTestMatrixDimM;       ///< Vector dimensions used in the large Test(timing)
extern In gTestMatrixDimN;       ///< Vector dimensions used in the large Test(timing)
extern midas::test::TestDrivers gTestDrivers;

/**
* Pes input options. 
* */
extern PesCalcDef gPesCalcDef;          ///< Global PesCalcDef
extern In         gPesIoLevel;           ///< Local pes IO level (>= global IO level)

/**
 * FitBasis input options.
**/
extern FitBasisCalcDef gFitBasisCalcDef;
extern In gFitBasisIoLevel;

/**
* Analysis input options. 
* */
extern string    gOutLink;              ///< Output file link
extern In        gAnalysisIoLevel;      ///< Local pes IO level (>= global IO level)
extern In        gExtHistogram;         ///< 
extern Nb        gAnalysisFwhm;         ///< Full with half max.
extern Nb        gAnalysisSpeBegin;     ///< 
extern Nb        gAnalysisSpeEnd;       ///< 
extern bool      gNoPs;                 ///< 



/**
* Vib input options. 
* Give a few int to be used for vector.reserve(int)
* Reasons: 1. efficiency - avoid many reallocations.
*          2. security   - for large calculations problems has
*                          been seen in the reallocations.
*                          May use excessive memory!!!
*                          since automatic capacity may be double
*                          each time maximum is achieved.
*
* The provided constants can be modified in the input, however,
* it is important to do so before their use!
* */
extern In        gVibIoLevel;           ///< Local vib IO level (>= global IO level)
extern In        gReserveNterms;        ///< Provide reserve STL for # terms in opers.
extern In        gReserveNmodes;        ///< Provide reserve STL for # modes in opers.
/**
* Oper input options. 
* */
extern bool      gDoOper;               ///< Do Oper calculation.
extern In        gOperIoLevel;          ///< Local Oper IO level (>= global IO level,vib IO level)
extern GlobalOperatorDefinitions gOperatorDefs;

/**
* Basis options. 
* */
extern In        gBasisIoLevel;         ///< Local Basis IO level (>= global IO level,vib IO level)
extern std::vector<BasDef> gBasis;           ///< The Basis set defs.
//allow for multiple basis set definitions
extern In        gReserveNdefs;                   ///< Reserve space for STL containers
extern std::vector<OneModeHoDef> gOneModeHoDefs;       ///< Vector for Ho basis defs.
extern std::vector<OneModeGaussDef> gOneModeGaussDefs; ///< Vector for gauss basis defs.
extern std::vector<OneModeBsplDef> gOneModeBsplDefs; ///< Vector for B-spline basis defs.
extern std::vector<OneModeStateBasis> gOneModeStateBasis;   ///< Vector of state-basis defs.

/**
* Vscf input options. 
* */
extern bool      gDoVscf;               ///< Do VSCF calculation.
extern In        gVscfIoLevel;          ///< Local VSCF IO level (>= global IO level,vib IO level)
extern vector<VscfCalcDef> gVscfCalcDef;///< The calculations defs.

/**
* Vcc input options. 
* */
extern bool      gDoVcc;                ///< Do Vcc  calculation.
extern In        gVccIoLevel;           ///< Local Vcc  IO level (>= global IO level,vib IO level)
extern vector<VccCalcDef> gVccCalcDef;  ///< The calculations defs.

/**
* TdH input options. 
* */
extern bool      gDoTdH;                ///< Do TDH calculation.
extern In        gTdHIoLevel;           ///< Local TDH IO level (>= global IO level,vib IO level)
extern vector<input::TdHCalcDef> gTdHCalcDef;  ///< The calculations defs.

/**
* McTdH input options. 
* */
extern bool gDoMcTdH;                           ///< Do MCTDH
extern In gMcTdHIoLevel;                        ///< Local MCTDH IO level (>= global IO level,vib IO level)
extern std::vector<input::McTdHCalcDef> gMcTdHCalcDef; ///< The calculations defs.

/**
 * TDVCC input options.
 **/
extern bool      gDoTdvcc;                ///< Do Vcc  calculation.
extern In        gTdvccIoLevel;           ///< Local Vcc  IO level (>= global IO level,vib IO level)
extern vector<input::TdvccCalcDef> gTdvccCalcDef;  ///< The calculations defs.

/**
* System options 
* */
extern In        gSystemIoLevel;               ///< System IO Level 
extern vector<SystemDef> gSystemDefs;
extern vector<input::ModSysCalcDef> gModSysCalcDefs;

/**
 * Elec input options. 
**/
extern In        gElecIoLevel;           ///< Local vib IO level (>= global IO level)

/** 
 * B spline basis flags
 * Allow for multiple basis set definitions. Is this fun? Currently we only got B splines
**/
extern bool gDoBspline;

/**
* Molecular geometry input options
**/
extern MolStruct gMolStruct; 

/**
* Ecor input options. 
* */
extern bool      gDoEcor;               ///< Do Ec (electron correlation)  calculation.
extern In        gEcorLevel;          ///< Local SCF IO level (>= global IO level,vib IO level)
extern vector<EcorCalcDef> gEcorCalcDef; ///< The calculations defs.

/**
 * GeoOpt input options.
 **/
extern bool      gDoGeoOpt;                    ///< Do a geometry optimization
extern In        gGeoOptLevel;                 ///< Local IO level (>= global IO level,vib IO level)
extern vector<GeoOptCalcDef> gGeoOptCalcDef;   ///< The calculations defs
/**
 * Machine Learning input options.
 **/
extern bool      gDoMLTask;                    ///< Do a geometry optimization
extern In        gMLLevel;                     ///< Local IO level (>= global IO level,vib IO level)
extern vector<MLCalcDef> gMLCalcDef;           ///< The calculations defs
/**
 * TensorDecomp input options. 
 **/
extern bool      gDoTdecomp;                   ///< Do a user defined tensor decomposition
extern In        gTdecompLevel;                ///< Local IO level (>= global IO level,vib IO level)
extern vector<TdecompCalcDef> gTdecompCalcDef; ///< The calculations defs.

/**
 * Analysis Input options
 **/
extern bool gDoAnalysis;
extern vector<AnalysisCalcDef> gAnalysisCalcDef;

/**
 * File conversion input options
 **/
extern bool gDoFileConversion;
extern vector<FileConversionCalcDef> gFileConversionCalcDef;

FileConversionCalcDef& CreateFileConversionCalcDef();

/**
 * Singlepoint input options
 **/
extern vector<SinglePointCalcDef> gSinglePointCalcDef;
SinglePointCalcDef& CreateSinglePointCalcDef(In aLevel);
const SinglePointCalcDef& SinglePointCalc(const std::string& aName);

/**
* General utilities. 
* */
extern AtomicData gAtomicData;

#endif /* INPUT_H_INCLUDED */
