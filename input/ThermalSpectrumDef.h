/**
************************************************************************
* 
* @file                ThermalSpectrumDef.h
*
* Created:             18-11-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Base class for a thermal spectrum
* 
* Last modified: Fri Mar 11, 2011  08:41PM mbh
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef THERMALSPECTRUMCALCDEF_H_INCLUDED
#define THERMALSPECTRUMCALCDEF_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

// using declarations
using std::string;
using std::vector;
using std::map;

/**
* Construct a definition of a  calculation
* */
class ThermalSpectrumDef
{
   private:
      vector<string> mOps;
      Nb mTemp;
      Nb mMaxFrq;
      In mNstep;
      Nb mGamma;
      string mPrefix;

      void CheckKeys(const std::map<string,string>&);
      void InitOpers(const std::map<string,string>&);
      void InitTemp(const std::map<string,string>&);
      void InitMaxFrq(const std::map<string,string>&);
      void InitGamma(const std::map<string,string>&);
      void InitNstep(const std::map<string,string>&);
      void ConstructionError(const string&, const std::map<string,string>&);
     
   public:
      ///< Constructor
      ThermalSpectrumDef();                            
      ///< Deconstructor
      ~ThermalSpectrumDef();
      
      void Setup(const std::map<string,string>&);
      
      string GetOperator(In aI) 
      {
         if(aI>=mOps.size())
            MIDASERROR("");
         return mOps[aI];
      }
      
      // ian: why are the below not const ???
      vector<string> GetOperators() 
      {
         return mOps;
      }
      
      Nb GetTemp() 
      {
         return mTemp;
      }
      
      Nb GetGamma() 
      {
         return mGamma;
      }
      
      Nb GetMaxFrq() 
      {
         return mMaxFrq;
      }
      
      In GetNstep() 
      {
         return mNstep;
      }
      
      const string& GetPrefix() 
      {
         return mPrefix;
      }
};

#endif // THERMALSPECTRUMCALCDEF_H_INCLUDED
