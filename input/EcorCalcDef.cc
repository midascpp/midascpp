/**
************************************************************************
* 
* @file                EcorCalcDef.cc
*
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a calculation
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/EcorCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"
#include "mpi/FileToString.h"

typedef string::size_type Sst;

/**
* Get first integer in line  
* */
In FirstIntInLine(std::istream& aIfs)
{
   string s; 
   getline(aIfs,s); 
   return InFromString(s);
}

/**
* Construct a default definition of a  calculation
* */
EcorCalcDef::EcorCalcDef()
    /**
     * General
     **/
   :  mCalcName("")
   ,  mIoLevel (I_0)
   /**
    * Restart 
    **/
   ,  mRestart( false )
   /**
    * Macro Solver
    **/
   ,  mMacroMaxIter(100)
   /**
    * Tensor Decompositions
    **/
   ,  mDoCP(false)
   ,  mCpAlsDiffThr(C_I_10_6)
   ,  mCpAlsMaxIter(100)
   /**
    * RI-MP2 driver
    **/
   ,  mDoRIMP2(false) 
   /**
    * Coupled Cluster models
    **/
   ,  mCCModel("CCSD")
   /**
    * Reference State Information
    **/
   ,  mOrbitalsInfoFile        ("DalMid.OrbInfo")
   ,  mOrbitalsFile            ("DalMid.Orbitals")
   ,  mEnergiesFile            ("DalMid.OrbEnerg")
   ,  mOverlapIntegralsFile    ("DalMid.Overlaps")
   ,  mOneElectronIntegralsFile("DalMid.OneHamil")
   ,  mTwoElectronIntegralsFile("LSDALTONERI3DIMBLOCKS.data")
{
   // Read basic orbital information
   std::istringstream info_file = midas::mpi::FileToStringStream(mOrbitalsInfoFile);
   if (  info_file.good() 
      && !info_file.str().empty()
      ) 
   {
      info_file.exceptions( std::ios::badbit | std::ios::failbit );
  
      In n_sym; 
      n_sym=FirstIntInLine(info_file);
      if (n_sym != 1) MIDASERROR(" Number of symmetries is not one in input"); 

      mNoDalton = false;

      mNBas       =FirstIntInLine(info_file); 
      mNOrb       =FirstIntInLine(info_file);
      mNOcc       =FirstIntInLine(info_file); 
      mNVir       =FirstIntInLine(info_file); 
      mNFrozenOcc =FirstIntInLine(info_file);
      mNFrozenVir =FirstIntInLine(info_file);
   }
   else
   {
      Mout << "I could not read Dalton Files.." << std::endl; 
      Mout << "I will go on maybe this is a RI-MP2 run?" << std::endl; 

      mNoDalton = true;

      mNBas       = 0;
      mNOrb       = 0;
      mNOcc       = 0;
      mNVir       = 0;
      mNFrozenOcc = 0;
      mNFrozenVir = 0;


   }
}

/**
 *  Adds TensorDecompInfo to EcorCalcDef::mDecompInfoSet.
 *  @param Minp
 *  @param s
 **/
bool EcorCalcDef::ReadEcorDecompInfoSet(std::istream& Minp, std::string& s)
{
   return NewTensorDecompInput(Minp, s, 4, this->GetDecompInfoSet());
}

/**
 *  Adds LapaceInfo to EcorCalcDef::mLaplaceInfoSet.
 *  @param Minp
 *  @param s
 **/
bool EcorCalcDef::ReadLaplaceInfoSet(std::istream& Minp, std::string& s)
{
   return LaplaceInput(Minp, s, 4, this->GetLaplaceInfoSet());
}


