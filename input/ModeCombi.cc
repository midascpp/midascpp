/**
************************************************************************
* 
* @file                ModeCombi.cc
*
* Created:             11-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement ModeCombi class members 
* 
* Last modified:       27-06-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <string> 
#include <vector> 
#include <algorithm> 

#include "inc_gen/math_link.h" 
//
// My headers:
#include "inc_gen/TypeDefs.h" 
#include "input/Input.h" 
#include "util/Io.h" 
#include "input/ModeCombi.h" 

using std::string;
using std::vector;
using std::find;
using std::unique;
using std::set_union;
using std::set_difference;

/**
 * default ctor
 **/
ModeCombi::ModeCombi
   (
   )
   : mAddress(-I_1)
   , mModeCombiNrInRefVec(-I_1)
   , mToBeExtrap(false)
   , mToBeScreened(false) 
{
}

/**
 * ctor taking integer and reserving space
 **/
ModeCombi::ModeCombi
   ( const In& aN
   )
   : mAddress(-I_1)
   , mModeCombiNrInRefVec(-I_1)
   , mToBeExtrap(false)
   , mToBeScreened(false)
{ 
   mModeNrs.reserve(aN);
}

/**
* Concstructor from set of integers 
* */
ModeCombi::ModeCombi
   ( const std::set<In>& arV1
   , const In aAdd
   )
   : mAddress(aAdd)
   , mModeCombiNrInRefVec(-I_1)
   , mToBeExtrap(false)
   , mToBeScreened(false) 
{
   In n = arV1.size();
   mModeNrs.clear();
   mModeNrs.reserve(n);
   for (set<In>::const_iterator i=arV1.begin();i!=arV1.end();i++)
   {
      mModeNrs.push_back(*i);
   }
}

/**
* Concstructor from vector of integers 
* */
ModeCombi::ModeCombi
   ( const std::vector<In>& arV1
   , const In aAdd
   )
   : mModeNrs(arV1)
   , mAddress(aAdd)
   , mModeCombiNrInRefVec(-I_1)
{
   std::sort(mModeNrs.begin(),mModeNrs.end()); // Sort for safety, so order is increasing
}

/**
* Concstructor from vector of integers 
* */
ModeCombi::ModeCombi
   ( const std::vector<LocalModeNr>& arV1
   , const In aAdd
   )
   : mAddress(aAdd)
   , mModeCombiNrInRefVec(-I_1)
{
   //In n = arV1.size();
   //mModeNrs.clear();
   //mModeNrs.reserve(n);
   for(In i = I_0; i < arV1.size(); i++)
      mModeNrs.push_back(arV1[i]);
   //mModeNrs = arV1;
   std::sort(mModeNrs.begin(),mModeNrs.end()); // Sort for safety, so order is increasing
}

/**
* Check if a mode is in the ModeCombi
* */
bool ModeCombi::IncludeMode(const In arM) const
{
   if (find(mModeNrs.begin(),mModeNrs.end(),arM)!=mModeNrs.end()) 
      return true;
   return false;

   /* In i_hit = 0;
   while (mModeNrs[i_hit]<arM) i_hit++;
   if (mModeNrs[i_hit]==arM) return true;
   return false;
   */
}
/**
* arOne common with arOther / intersection 
* */
void ModeCombi::InOneOnly(const ModeCombi& arOne, const ModeCombi& arOther)
{
   mModeNrs.clear();
   std::set_symmetric_difference(arOne.mModeNrs.begin(),arOne.mModeNrs.end(),
             arOther.mModeNrs.begin(),arOther.mModeNrs.end(),
             std::back_inserter(mModeNrs));
}

/**
* arOne common with arOther / intersection 
* */
void ModeCombi::InFirstOnly(const ModeCombi& arOne, const ModeCombi& arOther)
{
   mModeNrs.clear();
   std::set_difference(arOne.mModeNrs.begin(),arOne.mModeNrs.end(),
             arOther.mModeNrs.begin(),arOther.mModeNrs.end(),
             std::back_inserter(mModeNrs));
}

/**
* arOne common with arOther / intersection 
* */
void ModeCombi::Intersection(const ModeCombi& arOne, const ModeCombi& arOther)
{
   mModeNrs.clear();
   std::set_intersection(arOne.mModeNrs.begin(),arOne.mModeNrs.end(),
             arOther.mModeNrs.begin(),arOther.mModeNrs.end(),
             std::back_inserter(mModeNrs));
}

/**
* arOne U arOther 
* */
void ModeCombi::Union(const ModeCombi& arOne, const ModeCombi& arOther)
{
   mModeNrs.clear();
   std::set_union(arOne.mModeNrs.begin(),arOne.mModeNrs.end(),
             arOther.mModeNrs.begin(),arOther.mModeNrs.end(),
             std::back_inserter(mModeNrs));
}
/**
* Insert any mode  - will keep it ordered 
* */
void ModeCombi::InsertMode(const In arMode)
{
   mModeNrs.push_back(arMode);
   std::sort(mModeNrs.begin(),mModeNrs.end());
   //std::unique(mModeNrs.begin(),mModeNrs.end()); // removes duplicates
   // Oc, 25.02.04 erase
   mModeNrs.erase(std::unique(mModeNrs.begin(),mModeNrs.end()),
                  mModeNrs.end()); // removes duplicates
} 
/**
* Remove a mode (assumed that is is there only once,
* which should be tru).
* */
void ModeCombi::RemoveMode(const In arMode)
{
   std::vector<In>::iterator pos = find(mModeNrs.begin(),mModeNrs.end(),arMode);
   if (pos!=mModeNrs.end()) mModeNrs.erase(pos);
} 

/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const ModeCombi& ar1)
{ 
   arOut << " (";
   for (In i=0;i<ar1.Size()-1;i++) arOut << ar1.mModeNrs[i] << ",";
   if (ar1.Size()>=1) arOut << ar1.mModeNrs[ar1.Size()-1];
   arOut << ")";
   //if (ar1.Address() >=0) 
      arOut << " Address: " << ar1.Address();
      arOut << " RefNr:   " << ar1.ModeCombiRefNr();
   return arOut;
}
/**
* overload for <
* */
bool operator<(const ModeCombi& ar1,const ModeCombi& ar2)
{
   In s1 = ar1.Size();
   In s2 = ar2.Size();
   if (s1 != s2) 
   {
      return (s1 < s2);
   }
   else 
   {
      for (In i=0;i<min(ar1.Size(),ar2.Size());i++)
         if (ar1.Mode(i) != ar2.Mode(i)) return (ar1.Mode(i) < ar2.Mode(i));
      return false;
   }
}
/**
* Find idx nr for a mode Nr , Die if its not there.
* */
In ModeCombi::IdxNrForMode(const In arMode) const
{
   for (In i=0;i<Size();i++) if (arMode == mModeNrs[i]) return i;
   ostringstream os;
   os << "ModeCombi::IdxNrForMode():\n"
      << "Mode " << arMode << " not found in MC: " << *this << "\n";
   Error (os.str());
   return -I_1;
} 
/**
* Does arOne intersect with arOther?
* */
bool ZeroIntersect(const ModeCombi& arOne, const ModeCombi& arOther)
{
   if (arOne.Size()==I_0||arOne==I_0) return true;
   vector<In> tst_vec; 
   std::set_intersection(arOne.mModeNrs.begin(),arOne.mModeNrs.end(),
             arOther.mModeNrs.begin(),arOther.mModeNrs.end(),
             std::back_inserter(tst_vec));
   if (tst_vec.size()!= I_0) return false;
   else return true;

   //if (arOne.Size()==I_0 || arOther.Size()==I_0) return true;
   //for (In i=0;i<arOne.Size();i++) 
      //if (arOther.IncludeMode(arOne.mModeNrs[i])) return false;
   //return true;
}
In ModeCombi::SizeOut() const
{
   In size_in = 2*sizeof(In) + sizeof(mModeNrs) + mModeNrs.size()*sizeof(In);
   return size_in;
}
bool ModeCombi::Contains(const ModeCombi& arModeCombi) const
{
   return std::includes(mModeNrs.begin(), mModeNrs.end(),
          arModeCombi.mModeNrs.begin(), arModeCombi.mModeNrs.end());
}

namespace midas::detail
{
/***************************************************************************//**
 * @brief
 *    Implementation detail for 
 *    NumParams(const ModeCombi&, const std::vector<Uin>&, const In).
 ******************************************************************************/
template<typename I>
Uin NumParamsImpl
   (  const ModeCombi& arMc
   ,  const std::vector<I>& arNmodals
   ,  const In aAddend
   )
{
   static_assert(std::is_integral_v<I>, "Integral type expected.");
   Uin n = 1;
   for(const auto& m: arMc.MCVec())
   {
      try
      {
         n *= arNmodals.at(m) + aAddend;
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
         return 0;
      }
   }
   return n;
}
} /* namespace midas::detail */

/***************************************************************************//**
 * E.g. if 
 *     arMc      = {   1,    3,    5} 
 *     arNmodals = {8, 7, 6, 5, 4, 3, 2}
 *     aAddend   = -1
 * then it returns `(7-1)*(5-1)*(3-1) = 6*4*2 = 48`. Here the client might want
 * to subtract the 1 every number of modals to account for the reference state
 * modal, thus getting the number of excitation parameters.
 *
 * @note
 *    Throws MIDASERROR if a mode number is out-of-range of the arNmodals.
 * @warning
 *    Undefined behaviour if an integer in arNmodals becomes negative when
 *    adding aAddend to it; there are no checks on this.
 *
 * @param[in] arMc
 *    The ModeCombi.
 * @param[in] arNmodals
 *    Vector holding the number of modals for each mode.
 * @param[in] aAddend
 *    Number to be _added_ to each number of modals before multiplying them.
 *    `aAddend = -1` is the typical usage for getting the number of excitation
 *    parameters for the ModeCombi.
 * @return
 *    The number of exc. coefs./ampls.
 ******************************************************************************/
Uin NumParams
   (  const ModeCombi& arMc
   ,  const std::vector<In>& arNmodals
   ,  const In aAddend
   )
{
   return midas::detail::NumParamsImpl(arMc, arNmodals, aAddend);
}
Uin NumParams
   (  const ModeCombi& arMc
   ,  const std::vector<Uin>& arNmodals
   ,  const In aAddend
   )
{
   return midas::detail::NumParamsImpl(arMc, arNmodals, aAddend);
}
