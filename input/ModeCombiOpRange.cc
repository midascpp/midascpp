/**
 *******************************************************************************
 * 
 * @file    ModeCombiOpRange.cc
 * @date    14-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Class for containing a range of ModeCombi, and related functions.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "util/OrderedCombination.h"
#include "input/GroupCouplings.h"
#include "input/FlexCouplings.h"
#include "input/FlexCoupCalcDef.h"

// Forward declarations and such.
extern In gVibIoLevel;
extern In gPesIoLevel;

// Type aliases.
using const_iterator = ModeCombiOpRange::const_iterator;
using const_reverse_iterator = ModeCombiOpRange::const_reverse_iterator;

//==============================================================================
//==============================================================================
//
//    CONSTRUCTORS, ETC.
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 *
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (
   )
   :  ModeCombiOpRange(I_0, I_0, [](const std::vector<In>&) -> bool {return false;})
{
}

/***************************************************************************//**
 * Example:
 * ModeCombiOpRange(2, 4) shall result in a range with the ModeCombi%s
 *     {},
 *     {0}, {1}, {2}, {3}
 *     {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}
 *
 * @param[in] aMaxExciLevel
 *    The maximum excitation level (i.e. number of modes) for the ModeCombi%s
 *    in constructed range.
 * @param[in] aNumModes
 *    The number of modes for the range, i.e. modes will be taken from
 *    `{0,...,aNumModes-1}`.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  Uin aMaxExciLevel
   ,  Uin aNumModes
   )
   :  ModeCombiOpRange
         (  aMaxExciLevel
         ,  aNumModes
         ,  [](const std::vector<In>&) -> bool {return true;}
         )
{
}

/***************************************************************************//**
 * @param[in] aExciLevels
 *    The excitation levels that are included in the MCR.
 * @param[in] aNumModes
 *    The number of modes for the range, i.e. modes will be taken from
 *    `{0,...,aNumModes-1}`.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  const std::set<Uin>& aExciLevels
   ,  Uin aNumModes
   )
   :  ModeCombiOpRange
         (  *std::max_element(aExciLevels.begin(), aExciLevels.end())
         ,  aNumModes
         ,  [&aExciLevels](const std::vector<In>& arMc) -> bool
            {
               return ( aExciLevels.find(static_cast<Uin>(arMc.size())) != aExciLevels.end() );
            }
         )
{
}

/***************************************************************************//**
 * Example:
 * ModeCombiOpRange(2, {1,2,2,1}) shall result in a range with the
 * ModeCombi%s
 *     {},
 *     {0}, {1}, {2}, {3}
 *     {1,2}
 * since only modes 1 and 2 are allowed to participate in 2-mode couplings.
 *
 * @param[in] aMaxExciLevel
 *    The maximum excitation level (i.e. number of modes) for the ModeCombi%s
 *    in constructed range.
 * @param[in] arMaxExPerMode
 *    Specifies for a given mode, that it will only contribute to ModeCombi%s
 *    with at most this many modes. Its size will specify the number of modes,
 *    i.e. modes will be taken from `{0,...,arMaxExPerMode.size() - 1}`.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  Uin aMaxExciLevel
   ,  const std::vector<Uin>& arMaxExPerMode
   )
   :  ModeCombiOpRange
         (  ActualMaxFromMaxExPerMode(aMaxExciLevel, arMaxExPerMode)
         ,  arMaxExPerMode.size()
         ,  [&arMaxExPerMode](const std::vector<In>& arMc) -> bool 
               {
                  // Check that all modes are allowed to participate in a
                  // ModeCombi of the given size.
                  for(const auto& m: arMc)
                  {
                     if (arMaxExPerMode.at(m) < arMc.size())
                     {
                        return false;
                     }
                  }
                  return true;
               }
         )
{
}

/***************************************************************************//**
 * @note
 *    See unit tests for example of generated range.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  const GroupCouplings& arGc
   )
{
   Insert(GenerateModeCombisFromGroupCouplings(arGc));
}

/***************************************************************************//**
 * The FlexCouplings object determines whether a given ModeCombi can be
 * accepted (based on screening estimates).
 * _If_ the resulting range contains a given `n`-mode ModeCombi, then _all_
 * subset `k`-ModeCombi%s (`k < n`) must also be part of the resulting range.
 *
 * @note
 *    See unit tests for example of generated range.
 *
 * @param[in] arFc
 *    FlexCouplings object.
 * @param[in] aMaxExciLevel
 *    Will consider ModeCombi%s up to this level.
 * @param[in] aNumModes
 *    The number of modes.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  const FlexCouplings& arFc
   ,  Uin aMaxExciLevel
   ,  Uin aNumModes
   )
{
   Insert(GenerateModeCombisFromFlexCouplings(arFc, aMaxExciLevel, aNumModes));
}

/***************************************************************************//**
 * Constructs a range where different modes count differently towards the
 * maximum excitation level normally employed, i.e. the max. exci. level is now
 * not equivalent to ModeCombi size (the _actual_ excitation level), but
 * there's an _effective_ excitation level, calculated as
 * \f[
 *    \sum_i w(m_i)
 * \f]
 * where the \f$ m_i \f$ are the modes of a given ModeCombi, and the \f$ w(m_i)
 * \f$ are the weights/counts/contributions to the effective exci when
 * including mode \f$ m_i \f$.
 *
 * Example:
 *     aMaxEffectiveExciLevel = 2
 *     arEffectiveExciCounts  = {1,0,2,1}
 * will generate the range:
 *     {}         // eff. level = 0
 *     {0}        // eff. level = 1
 *     {1}        // eff. level = 0
 *     {2}        // eff. level = 2
 *     {3}        // eff. level = 1
 *     {0,1}      // eff. level = 1
 *     {0,3}      // eff. level = 2
 *     {1,2}      // eff. level = 2
 *     {1,3}      // eff. level = 1
 *     {0,1,3}    // eff. level = 2
 * whereas these ModeCombi%s have too high an effective level:
 *     {0,2}      // eff. level = 3
 *     {2,3}      // eff. level = 3
 *     {0,1,2}    // eff. level = 3
 *     {0,2,3}    // eff. level = 4
 *     {1,2,3}    // eff. level = 3
 *     {0,1,2,3}  // eff. level = 4
 *
 * @note
 *    These notions of _effective_ levels only apply to the construction of the
 *    range. Once constructed the range will only deal with the _actual_
 *    excitation levels (i.e. ModeCombi sizes).
 *
 * @param[in] aMaxEffectiveExciLevel
 *    The _effective_ maximum excitation level.
 * @param[in] arEffectiveExciCounts
 *    How much does each mode count towards the _effective_ excitation level.
 *    Its size will specify the number of modes, i.e. modes will be taken from
 *    `{0,...,arEffectiveExciCounts.size() - 1}`.
 * @return
 *    The newly constructed range.
 ******************************************************************************/
ModeCombiOpRange ModeCombiOpRange::ConstructFromEffectiveExciCount
   (  Uin aMaxEffectiveExciLevel
   ,  const std::vector<Uin>& arEffectiveExciCounts
   )
{
   auto validation = 
      [aMaxEffectiveExciLevel, &arEffectiveExciCounts](const std::vector<In>& mc) -> bool
      {
         Uin count = I_0;
         for(const auto& mode: mc)
         {
            count += arEffectiveExciCounts.at(static_cast<Uin>(mode));
         }
         return count <= aMaxEffectiveExciLevel;
      };
   return ModeCombiOpRange
      (  ActualMaxFromEffectiveExciCount(aMaxEffectiveExciLevel, arEffectiveExciCounts)
      ,  arEffectiveExciCounts.size()
      ,  validation
      );
}

//==============================================================================
//==============================================================================
//
//    QUERIES
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * @return
 *    Ordered mode numbers contained (in combination) by ModeCombi%s in the
 *    range.
 ******************************************************************************/
Uin ModeCombiOpRange::Size
   (
   )  const
{
   return mModeCombisVec.size();
}

/***************************************************************************//**
 * @return
 *    The maximum excitation level of the range, i.e. the number of modes in
 *    the largest ModeCombi(s) in the range. (Or 0 if the range is empty.)
 ******************************************************************************/
Uin ModeCombiOpRange::GetMaxExciLevel
   (
   )  const
{
   if (mModeCombisVec.empty())
   {
      return I_0;
   }
   else
   {
      return mModeCombisVec.back().Size();
   }
}

/***************************************************************************//**
 * @param[in] aExcLevel
 *    The excitation level (i.e. ModeCombi size) of interest.
 * @return
 *    Number of ModeCombi%s with the given excitation level (i.e. num. modes).
 *    Will simply be zero if aExcLevel is larger than contained largest
 *    ModeCombi.
 ******************************************************************************/
Uin ModeCombiOpRange::NumModeCombisWithExcLevel
   (  Uin aExcLevel
   )  const
{
   try
   {
      return mNumModeCombisWithExcLevel.at(aExcLevel);
   }
   catch(const std::out_of_range&)
   {
      return I_0;
   }
}

/***************************************************************************//**
 * @return
 *    Ordered vector with all mode numbers contained by ModeCombi%s in the
 *    range.
 ******************************************************************************/
std::vector<In> ModeCombiOpRange::ModesContained
   (
   )  const
{
   return mModesContained;
}

/***************************************************************************//**
 * @return
 *    Number of modes contained (in combination) by ModeCombi%s in the range.
 ******************************************************************************/
Uin ModeCombiOpRange::NumberOfModes
   (
   )  const
{
   return mModesContained.size();
}

/***************************************************************************//**
 * @return
 *    Smallest mode number contained. Hard error if NumberOfModes() is zero.
 ******************************************************************************/
In ModeCombiOpRange::SmallestMode
   (
   )  const
{
   if (!mModesContained.empty())
   {
      return mModesContained.front();
   }
   else
   {
      MIDASERROR("Tried to call SmallestMode() for ModeCombiOpRange with no modes contained.");
      return 0; // Quenches compiler warning.
   }
}

/***************************************************************************//**
 * @return
 *    Largest mode number contained. Hard error if NumberOfModes() is zero.
 ******************************************************************************/
In ModeCombiOpRange::LargestMode
   (
   )  const
{
   if (!mModesContained.empty())
   {
      return mModesContained.back();
   }
   else
   {
      MIDASERROR("Tried to call LargestMode() for ModeCombiOpRange with no modes contained.");
      return 0; // Quenches compiler warning.
   }
}

/***************************************************************************//**
 * Equivalent to Size() if `aMax` is GetMaxExciLevel().
 *
 * @param[in] aMax
 *    The max. size of ModeCombi%s to be counted.
 * @param[in] aMin
 *    The min. size of ModeCombi%s to be counted.
 * @return
 *    The number of ModeCombi%s whose size is in `{aMin,...,aMax}`.
 *    Note that if `aMin > aMax`, this will be zero.
 ******************************************************************************/
Uin ModeCombiOpRange::ReducedSize
   (  Uin aMax
   ,  Uin aMin
   )  const
{
   Uin s = I_0;
   for(Uin i = aMin; i <= aMax; ++i)
   {
      s += NumModeCombisWithExcLevel(i);
   }
   return s;
}

/***************************************************************************//**
 * @return
 *    Number of empty ModeCombi%s contained. Since the range contains _unique_
 *    ModeCombi%s this can only be either 0 or 1.
 ******************************************************************************/
Uin ModeCombiOpRange::NumEmptyMCs
   (
   )  const
{
   return NumModeCombisWithExcLevel(I_0);
}

/***************************************************************************//**
 * @param[in,out] arOs
 *    The ostream to write to.
 * @return
 *    An estimate of the size in bytes.
 ******************************************************************************/
Uin ModeCombiOpRange::SizeOut
   (  std::ostream& arOs
   )  const
{
   arOs 
      << " Output the total size of the ModeCombiOpRange\n"
      << " vector  sizeof                 = "  << sizeof(mModeCombisVec)              << '\n'
      << " hashmap sizeof                 = "  << sizeof(mModeCombiHashMap)           << '\n'
      << " levels  sizeof                 = "  << sizeof(mNumModeCombisWithExcLevel)  << '\n'
      << " modes   sizeof                 = "  << sizeof(mModesContained)             << '\n'
      << std::flush;
   Uin tot_size = I_0
      +  sizeof(mModeCombisVec)
      +  sizeof(mModeCombiHashMap)
      +  sizeof(mNumModeCombisWithExcLevel)
      +  sizeof(mModesContained)
      ;

   // Size of ModeCombis in vector and hashmap.
   // For the vector it's the SizeOut() of the ModeCombi.
   // For the hashmap it's the size of the std::vector<In> key plus the Uin mapped type.
   // The hashmap has some overhead due to its implementation (think
   // bucket_count() and load_factor()), but we'll neglect that.
   Uin vec_sizes = I_0;
   Uin map_sizes = I_0;
   for(const auto& mc: *this)
   {
      vec_sizes += mc.SizeOut();
      map_sizes += sizeof(std::vector<In>) + mc.Size()*sizeof(In) + sizeof(Uin);
   }
   arOs
      << " mModeCombisVec    size         = " << vec_sizes << '\n'
      << " mModeCombiHashMap size        >= " << map_sizes << '\n'
      << std::flush;
   tot_size += vec_sizes + map_sizes;
   arOs << " Total ModeCombiOpRange size = " << tot_size<< " bytes " << std::endl;
   return tot_size;
}

//==============================================================================
//==============================================================================
//
//    MODECOMBI ACCESS
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * Two iterator flavours:
 *    - regular: Returns const_iterator to begin (first) or end
 *      (first-beyond-last) ModeCombi of the object. 
 *    - reverse: Returns const_iterator to reverse begin (last) or reverse end
 *      (first-before-first) ModeCombi of the object.
 *
 * Uses the underlying ModeCombiOpRange::mModeCombisVec
 * std::vector meaning that ModeCombi%s are stored contiguously in memory, for
 * efficiency.
 *
 * Only const versions are available, since any changes would invalidate the
 * internal consistency of the class. Therefore the const and regular versions
 * are exactly the same.
 *
 * @note
 *    The end iterators shall not be dereferenced. Thus if the object is empty
 *    (meaning begin() == end()) the begin iterators shall not be dereferenced
 *    either.
 *
 * @return
 *    const_iterator to (reverse) beginning/end.
 ******************************************************************************/
const_iterator ModeCombiOpRange::begin()  const {return cbegin();}
const_iterator ModeCombiOpRange::end()    const {return cend();}
const_iterator ModeCombiOpRange::cbegin() const {return mModeCombisVec.cbegin();}
const_iterator ModeCombiOpRange::cend()   const {return mModeCombisVec.cend();}
const_reverse_iterator ModeCombiOpRange::rbegin()  const {return crbegin();}
const_reverse_iterator ModeCombiOpRange::rend()    const {return crend();}
const_reverse_iterator ModeCombiOpRange::crbegin() const {return mModeCombisVec.crbegin();}
const_reverse_iterator ModeCombiOpRange::crend()   const {return mModeCombisVec.crend();}

/***************************************************************************//**
 * Example:
 * ~~~.cpp
 * for(auto it = mcr.Begin(2), end = mcr.End(3); it != end; ++it)
 * {
 *    // Iterates through all 2- and 3-ModeCombis of the range.
 * }
 * ~~~
 *
 * @note
 *    Due to random-access storage _and_ internal knowledge about the number of
 *    ModeCombi%s with specific numbers of modes, this is fast; doesn't require
 *    iteration until hitting the right ModeCombi.
 *
 * @note
 *    Begin(0) is equivalent to begin().
 *    End(n) is equivalent to Begin(n+1).
 *
 * @return
 *    Iterator to first ModeCombi in range with aN modes, or if no ModeCombi%s
 *    of range has aN modes, will return end().
 ******************************************************************************/
const_iterator ModeCombiOpRange::Begin
   (  Uin aN
   )  const
{
   if (aN <= GetMaxExciLevel())
   {
      auto it = begin();
      std::advance(it, ReducedSize(aN) - NumModeCombisWithExcLevel(aN));
      return it;
   }
   else
   {
      return end();
   }
}

/***************************************************************************//**
 * @see
 *    Begin(Uin)
 *
 * @note
 *    In general it's unsafe to dereference the iterator returned from this
 *    function; it might be a valid iterator to some ModeCombi in the range,
 *    but there's no guarantee, so should only be used as a halt condition in
 *    `for` statement and such.
 *
 * @return
 *    Iterator to first-beyond-last ModeCombi in range with given number of
 *    modes.
 ******************************************************************************/
const_iterator ModeCombiOpRange::End
   (  Uin aN
   )  const
{
   if (aN < GetMaxExciLevel())
   {
      return Begin(aN+1);
   }
   else
   {
      return end();
   }
}

/***************************************************************************//**
 * @note
 *    No out-of-range check, so use carefully and together with Size().
 *    Prefer using iterator based loops.
 *
 * @param[in] aI
 *    The index of the ModeCombi to retrieve.
 * @return
 *    The `aI`th ModeCombi.
 ******************************************************************************/
const ModeCombi& ModeCombiOpRange::GetModeCombi
   (  Uin aI
   )  const
{
   return mModeCombisVec[aI];
}

/***************************************************************************//**
 * Finds the corresponding ModeCombi in the range, based on the mode numbers
 * alone (i.e. not ref. number or address and such).
 *
 * @note
 *    Complexity: Uses a hash-map (unordered map) and should thus be amortized
 *    constant lookup time.
 *
 * @param[in] arMc
 *    The ModeCombi whose equivalent is searched for within the range.
 * @param[out] arIter
 *    On return this will be an iterator to the ModeCombi of interest if found.
 *    If not found, this will be the end() of the range.
 * @return
 *    Whether the argument ModeCombi was found in the range.
 ******************************************************************************/
bool ModeCombiOpRange::Find
   (  const std::vector<In>& arMc
   ,  const_iterator& arIter
   )  const
{
   auto it = mModeCombiHashMap.find(arMc);
   if (it == mModeCombiHashMap.end())
   {
      arIter = end();
      return false;
   }
   else
   {
      arIter = begin();
      std::advance(arIter, it->second);
      return true;
   }
}

bool ModeCombiOpRange::Find
   (  const ModeCombi& arMc
   ,  const_iterator& arIter
   )  const
{
   return Find(arMc.MCVec(), arIter);
}

/**
 * @param arMc
 * @param arIter
 * @param arMcNumber
 *
 * @return
 *    True if MC is found
 **/
bool ModeCombiOpRange::Find
   (  const ModeCombi& arMc
   ,  const_iterator& arIter
   ,  In& arMcNumber
   )  const
{
   if (  !this->Find(arMc, arIter)
      )
   {
      return false;
   }
   else
   {
      arMcNumber = arIter->ModeCombiRefNr();
      return true;
   }
}

/***************************************************************************//**
 * Finds the corresponding ModeCombi in the range, based on the mode numbers
 * alone (i.e. not ref. number or address and such).
 *
 * @note
 *    Complexity: Uses a hash-map (unordered map) and should thus be amortized
 *    constant lookup time.
 *
 * @param[in] arMc
 *    The ModeCombi whose equivalent is searched for within the range.
 * @return
 *    Whether the argument ModeCombi was found in the range.
 ******************************************************************************/
bool ModeCombiOpRange::Contains
   (  const std::vector<In>& arMc
   )  const
{
   const_iterator dummy;
   return Find(arMc, dummy);
}

bool ModeCombiOpRange::Contains
   (  const ModeCombi& arMc
   )  const
{
   return Contains(arMc.MCVec());
}

/***************************************************************************//**
 * Finds the corresponding ModeCombi in the range, based on the mode numbers
 * alone (i.e. not ref. number or address and such).
 * If found, then uses ModeCombi::Address() and ModeCombi::ModeCombiRefNr()
 * from the corresponding ModeCombi _in this object_ and assigns those values
 * to the argument ModeCombi.
 *
 * @note
 *    Complexity: Uses a hash-map (unordered map) and should thus be amortized
 *    constant lookup time.
 *
 * @param[in,out] arMc
 *    The ModeCombi whose equivalent is searched for within the range.
 * @return
 *    Whether the argument ModeCombi was found in the range.
 ******************************************************************************/
bool ModeCombiOpRange::IfContainedAssignAddressAndRefNum
   (  ModeCombi& arMc
   )  const
{
   const_iterator iter;
   if (Find(arMc, iter))
   {
      arMc.AssignAddress(iter->Address());
      arMc.AssignModeCombiRefNr(iter->ModeCombiRefNr());
      return true;
   }
   else
   {
      return false;
   }
}
   
//==============================================================================
//==============================================================================
//
//    RANGE MODIFICATIONS
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * If a ModeCombi already exists in the ModeCombiOpRange object, it is _not_
 * overwritten by the one from the argument.
 *
 * There is a substantial overhead to inserting, since all internal attributes
 * must be updated. Therefore try inserting ModeCombi%s in batches. This is why
 * only a vectorized insert function is provided.
 *
 * @note
 *    If any elements are inserted, all iterators, pointers, and references to
 *    ModeCombi%s in this object are (most likely) invalidated.
 *
 * @param[in] arVecMc
 *    Inserts the ModeCombi%s of this argument into the range (by
 *    copying/moving, depending on the version).
 ******************************************************************************/
void ModeCombiOpRange::Insert
   (  const std::vector<ModeCombi>& arVecMc
   )
{
   for(auto&& mc: arVecMc)
   {
      mModeCombisVec.push_back(mc);
   }
   UpdateAttributes();
}

void ModeCombiOpRange::Insert
   (  std::vector<ModeCombi>&& arVecMc
   )
{
   for(auto&& mc: arVecMc)
   {
      mModeCombisVec.push_back(std::move(mc));
   }
   UpdateAttributes();
}

/***************************************************************************//**
 * Sorts the argument, then erases from the range all the ModeCombi%s that
 * evaluate equal to the arguments.
 *
 * It has no effect on the result that the argument vector contains a ModeCombi
 * not in the range.
 *
 * @note
 *    If any elements are inserted, all iterators, pointers, and references to
 *    ModeCombi%s in this object are (most likely) invalidated.
 *
 * @param[in] arVecMc
 *    Erases the ModeCombi%s of this argument from the range (if present).
 ******************************************************************************/
void ModeCombiOpRange::Erase
   (  const std::vector<ModeCombi>& arVecMc
   )
{
   auto tmp_copy = arVecMc;
   Erase(std::move(tmp_copy));
}

void ModeCombiOpRange::Erase
   (  const ModeCombiOpRange& arVecMc
   )
{
   Erase(arVecMc.mModeCombisVec);
}

void ModeCombiOpRange::Erase
   (  ModeCombiOpRange&& arVecMc
   )
{
   Erase(std::move(arVecMc.mModeCombisVec));
}

void ModeCombiOpRange::Erase
   (  std::vector<ModeCombi>&& arVecMc
   )
{
   // Make argument range sorted and unique in order to comply with set_difference.
   SortAndRemoveDuplicates(arVecMc);
   // std::set_difference must put result in new container, different from other ranges.
   // std::move_iterators for mModeCombisVec elements (will be overwritten anyway).
   // std::inserter inserts consecutively at given position (thus resizes new_mcs).
   std::vector<ModeCombi> new_mcs;
   std::set_difference  
      (  std::make_move_iterator(mModeCombisVec.begin())
      ,  std::make_move_iterator(mModeCombisVec.end())
      ,  arVecMc.begin()
      ,  arVecMc.end()
      ,  std::inserter(new_mcs, new_mcs.end())
      );
   // Move result back into mModeCombisVec and update statistics.
   mModeCombisVec = std::move(new_mcs);
   UpdateAttributes();
}

/***************************************************************************//**
 * @param[in] aF
 *    Function that returns true for all MCs that should be erased
 ******************************************************************************/
void ModeCombiOpRange::Erase
   (  const std::function<bool(const std::vector<In>&)>& aF
   )
{
   std::vector<ModeCombi> new_mcs;
   for(const auto& mc : *this)
   {
      if (  !aF(mc.MCVec())
         )
      {
         new_mcs.emplace_back(mc);
      }
   }
   mModeCombisVec = std::move(new_mcs);
   UpdateAttributes();
}

/***************************************************************************//**
 * Clear all contents of the container and update internal statistics
 * accordingly.
 ******************************************************************************/
void ModeCombiOpRange::Clear
   (
   )
{
   mModeCombisVec.clear();
   UpdateAttributes();
}

/***************************************************************************//**
 * @note
 *    This modifies the range by modifying a ModeCombi address, but since
 *    addresses aren't used for ModeCombi sorting, the range does not need to
 *    be updated.
 *
 * @note
 *    The versions using index/iterator can access the ModeCombi directly;
 *    otherwise it needs to Find() it first, so use the former if possible.
 *
 * @param[in] arMc
 *    Index of/iterator to the ModeCombi. (Or the ModeCombi itself.)
 * @param[in] aAddress
 *    The address that will be given to the ModeCombi.
 ******************************************************************************/
void ModeCombiOpRange::AssignAddress
   (  Uin arMc
   ,  In aAddress
   )
{
   try
   {
      mModeCombisVec.at(arMc).AssignAddress(aAddress);
   }
   catch(const std::out_of_range&)
   {
      // Do nothing if out-of-range.
   }
}
void ModeCombiOpRange::AssignAddress
   (  const_iterator arMc
   ,  In aAddress
   )
{
   AssignAddress(std::distance(begin(), arMc), aAddress);
}
void ModeCombiOpRange::AssignAddress
   (  const std::vector<In>& arMc
   ,  In aAddress
   )
{
   const_iterator it;
   Find(arMc, it);
   AssignAddress(it, aAddress);
}

//==============================================================================
//==============================================================================
//
//    CONSTRUCTOR UTILITIES
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * @param[in] aMaxExciLevel
 *    The maximum excitation level (i.e. number of modes) for the ModeCombi%s
 *    in constructed range.
 * @param[in] aNumModes
 *    The number of modes for the range, i.e. modes will be taken from
 *    `{0,...,aNumModes-1}`.
 * @param[in] arValidation
 *    Shall be a functor taking a `const std::vector<In>&` as argument and
 *    returning `bool`. The return value shall signify whether the a ModeCombi
 *    with the given mode numbers shall be part of the range under
 *    construction.
 ******************************************************************************/
ModeCombiOpRange::ModeCombiOpRange
   (  Uin aMaxExciLevel
   ,  Uin aNumModes
   ,  const std::function<bool(const std::vector<In>&)>& arValidation
   )
{
   Uin max = std::min(aMaxExciLevel, aNumModes);
   for(OrderedCombination oc(I_0, max+I_1, aNumModes); !oc.AtEnd(); ++oc)
   {
      auto&& mc = oc.GetCombination();
      if (arValidation(mc))
      {
         mModeCombisVec.emplace_back(std::move(mc), -I_1);
      }
   }
   UpdateAttributes();
}

/***************************************************************************//**
 * @note
 *    See unit tests for example of generated range.
 *
 * @param[in] arGc
 *    GroupCouplings object.
 * @return
 *    A vector of (unsorted) ModeCombi%s, ready for insertion into an empty
 *    ModeCombiOpRange.
 ******************************************************************************/
std::vector<ModeCombi> ModeCombiOpRange::GenerateModeCombisFromGroupCouplings
   (  const GroupCouplings& arGc
   )
{
   std::unordered_set<ModeCombi> s_mcs;
      
   // The empty ModeCombi shall always be part of the range.
   s_mcs.emplace(std::vector<In>{}, -I_1);

   for (Uin i_gc = I_0; i_gc < arGc.NrGroupCombis(); ++i_gc)
   {
      // The group combi (may have duplicates).
      std::vector<In> group_combi = arGc.GroupCombi(i_gc); 
      std::sort(group_combi.begin(),group_combi.end());

      // The involved groups (the unique groups in group combi).
      std::vector<In> involved_groups(group_combi);
      const auto new_end = std::unique(involved_groups.begin(), involved_groups.end());
      involved_groups.resize(std::distance(involved_groups.begin(), new_end));
      
      // The modes for each involved group.
      std::vector<std::vector<In>> vec_group_ranges;
      for (In j = I_0; j < involved_groups.size(); ++j)
      { 
         vec_group_ranges.push_back(arGc.ModeGroup(involved_groups.at(j)));
      }
             
      // Then generate the actual ModeCombis.
      RecursAddModeCombiForGroup
         (  s_mcs
         ,  std::vector<In>(group_combi.size())
         ,  group_combi
         ,  vec_group_ranges
         ,  I_0
         ,  I_0
         ,  I_0
         );
   }
   return std::vector<ModeCombi>(s_mcs.begin(), s_mcs.end());
}

/***************************************************************************//**
 * The FlexCouplings object determines whether a given ModeCombi can be
 * accepted (based on screening estimates).
 * _If_ the resulting range contains a given `n`-mode ModeCombi, then _all_
 * subset `k`-ModeCombi%s (`k < n`) must also be part of the resulting range.
 *
 * Two opposite ways to achieve this:
 *    -  Top-down; loop through highest-level ModeCombi%s first (starting at
 *    aMaxExciLevel), then proceed down in ModeCombi level. If finding an
 *    _accepted_ ModeCombi, immediately also include all subset ModeCombi%s of
 *    that one.
 *    -  Buttom-up; loop through lowest-level ModeCombi%s first, then proceed
 *    up in ModeCombi level (ending with the aMaxExciLevel ones).
 *    If encountering a _rejected_ ModeCombi, immediately scan through all
 *    higher-level ModeCombi%s that contain it, and include anyway if any of
 *    those higher-level ones is accepted.
 *
 * Both these approaches seem to go through a lot of duplicate work. The
 * top-down approach seems a little easier to implement, so I think we'll use a
 * modified top-down approach in this case:
 *    -  Start with all the highest-level ModeCombi%s, then proceed down in
 *    level.
 *    -  At ModeCombi level `n`. Check FlexCouplings for whether to accept
 *    ModeCombi, and insert if accepted.
 *    -  Then check if range contains that ModeCombi (might either be due to
 *    just having been inserted, _or_ due to having been inserted as a subset
 *    of a higher-level ModeCombi).
 *    If contained, also include all its level `n-1` subsets.
 * 
 * In this way we'll eventually insert all level `{0,...,n-1}` subsets of a
 * given level `n` ModeCombi, but only doing duplicate work for the `n-1`
 * subsets.
 *
 * @note
 *    See unit tests for example of generated range.
 *
 * @param[in] arFc
 *    FlexCouplings object.
 * @param[in] aMaxExciLevel
 *    Will consider ModeCombi%s up to this level.
 * @param[in] aNumModes
 *    The number of modes.
 * @return
 *    A vector of (unsorted) ModeCombi%s, ready for insertion into an empty
 *    ModeCombiOpRange.
 ******************************************************************************/
std::vector<ModeCombi> ModeCombiOpRange::GenerateModeCombisFromFlexCouplings
   (  const FlexCouplings& arFc
   ,  Uin aMaxExciLevel
   ,  Uin aNumModes
   )
{
   std::unordered_set<std::vector<In>> s_mcs;
      
   // The empty ModeCombi shall always be part of the range.
   s_mcs.insert(std::vector<In>{});

   // Iterate through ModeCombi levels, highest to lowest (level 1).
   for(Uin level = aMaxExciLevel; level > I_0; --level)
   {
      // Loop through all mode combinations at this level.
      for(OrderedCombination oc(level, level+1, aNumModes); !oc.AtEnd(); ++oc)
      {
         const auto& vec_modes = oc.GetCombination();

         // Accept a mode combinations based on its FlexCouplings.MaxLevel().
         if (arFc.MaxLevel(vec_modes, aMaxExciLevel) >= level)
         {
            s_mcs.insert(vec_modes);
         }

         // Check if the current mode combination is in the set; might be due
         // to the insertion just above, or it might have been inserted at the
         // level+1 iteration. If it exists, insert all level-1 subsets.
         if (s_mcs.count(vec_modes) > I_0)
         {
            for(OrderedCombination sub_oc(level-I_1, level, vec_modes); !sub_oc.AtEnd(); ++sub_oc)
            {
               s_mcs.insert(sub_oc.GetCombination());
            }
         }
      }
   }

   // Then move over to vector, constructing ModeCombi%s out of the
   // std::vector<In> along the way.
   std::vector<ModeCombi> v_mcs;
   v_mcs.reserve(s_mcs.size());
   for(auto&& mc: s_mcs)
   {
      v_mcs.emplace_back(mc, -I_1);
   }

   return v_mcs;
}

/***************************************************************************//**
 * Recursively calls itself to generate ModeCombi%s based on the a group combi
 * and modes pertaining to each mode of the group combi.
 *
 * @note
 *    See unit tests for example of generated range.
 *
 * @param[out] arModeCombis
 *    The set of ModeCombi%s so far; newly generated ones will be inserted in
 *    this.
 * @param[in] aTmp
 *    The (partially) constructed ModeCombi vector.
 * @param[in] arGroupCombi
 *    The group numbers for which we're constructing ModeCombi%s.
 * @param[in] arModesOfGroups
 *    The modes pertaining to each group in arGroupCombi.
 * @param[in] aGroupNr
 *    The index telling how far we've iterated through arGroupCombi.
 * @param[in] aStartModeNr
 *    The index telling how far we've iterated through the modes of
 *    arModesOfGroups.at(aGroupNr).
 * @param[in] aLevel
 *    The index telling how far we've iterated through aTmp. When ultimately
 *    this function has been called (by itself) with `aLevel =
 *    arGroupCombi.size()-1`, aTmp will be a complete ModeCombi vector, and
 *    will thus be inserted into arModeCombis.
 ******************************************************************************/
void ModeCombiOpRange::RecursAddModeCombiForGroup
   (  std::unordered_set<ModeCombi>& arModeCombis
   ,  std::vector<In> aTmp
   ,  const std::vector<In>& arGroupCombi
   ,  const std::vector<std::vector<In>>& arModesOfGroups
   ,  In aGroupNr
   ,  In aStartModeNr
   ,  In aLevel
   )
{
   const auto& group_modes = arModesOfGroups.at(aGroupNr);
   for (In i = aStartModeNr; i < group_modes.size(); ++i)
   {
      In group = aGroupNr;
      aTmp.at(aLevel) = group_modes.at(i);
      if (aLevel + I_1 < arGroupCombi.size())
      {
         if (arGroupCombi.at(aLevel+I_1) == arGroupCombi.at(aLevel))
         {
            ++aStartModeNr;
         }
         else
         {
            aStartModeNr = I_0;
            ++group;
         }
         RecursAddModeCombiForGroup(arModeCombis,aTmp,arGroupCombi, arModesOfGroups, group,aStartModeNr,aLevel+I_1);
      }
      else
      {
         arModeCombis.emplace(aTmp, -I_1);
      }
   }
}

/***************************************************************************//**
 * @return
 *    The minimum of aMaxExciLevel and the maximum value in arMaxExPerMode.
 ******************************************************************************/
Uin ModeCombiOpRange::ActualMaxFromMaxExPerMode
   (  Uin aMaxExciLevel
   ,  const std::vector<Uin>& arMaxExPerMode
   )
{
   auto it_max_of_vec = std::max_element(arMaxExPerMode.begin(), arMaxExPerMode.end());
   return std::min(aMaxExciLevel, *it_max_of_vec);
}

/***************************************************************************//**
 * When using effective excitation counts, the largest ModeCombi%(s) will be
 * the one(s) containing the modes with the smallest counts.
 * Thus, sort the arEffectiveExciCounts in ascending order, start accumulating
 * the effective exci. level until hitting the given level. That'll give the
 * size of the largest ModeCombi that can be generated.
 *
 * Example:
 *     arEffectiveExciCounts = {2,1,0,1,1}
 *     aMaxEffectiveExciLevel = 2
 *     sorted = {0,1,1,1,2}
 * We can construct an actual 3-mode combination given this, so that's what's
 * returned.
 *
 * @param[in] aMaxEffectiveExciLevel
 *    The desired _effective_ excitation level.
 * @param[in] arEffectiveExciCounts
 *    Each modes contribution to the the _effective_ excitation level.
 * @return
 *    The resulting maximum _actual_ excitation level.
 ******************************************************************************/
Uin ModeCombiOpRange::ActualMaxFromEffectiveExciCount
   (  Uin aMaxEffectiveExciLevel
   ,  const std::vector<Uin>& arEffectiveExciCounts
   )
{
   std::vector<Uin> v(arEffectiveExciCounts);
   std::sort(v.begin(), v.end());
   Uin accumulated = I_0;
   Uin actual_level = I_0;
   for(const auto& c: v)
   {
      // First accumulate, then check if surpassing eff. exc. level.
      // If so, then return. Otherwise, accept the level incrementation.
      accumulated += c;
      if (accumulated > aMaxEffectiveExciLevel)
      {
         break;
      }
      else
      {
         ++actual_level;
      }
   }
   return actual_level;
}

//==============================================================================
//==============================================================================
//
//    INTERNAL ATTRIBUTES UTILITIES
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * Sorts the ModeCombi%s of the range and removes duplicates.
 * Then updates all internal statistics (by looping through all the
 * ModeCombi%s):
 *    -  ModeCombiOpRange::mNumModeCombisWithExcLevel
 *    -  ModeCombiOpRange::mModesContained
 *    -  AssignModeCombiRefNr() for the ModeCombi%s (to match their index in
 *       the range)
 *    -  ModeCombiOpRange::mModeCombiHashMap is constructed with the
 *       ModeCombi mode vectors and corresponding indices in
 *       ModeCombiOpRange::mModeCombisVec.
 *
 * @note
 *    This is of course a bit expensive to call, but is necessary for keeping
 *    object internally consistent.
 ******************************************************************************/
void ModeCombiOpRange::UpdateAttributes
   (
   )
{
   // Sort/remove duplicates.
   SortAndRemoveDuplicates(mModeCombisVec);

   // Counting vec.; must be large enough to have index for the last ModeCombi.
   mNumModeCombisWithExcLevel.clear();
   if (!mModeCombisVec.empty())
   {
      mNumModeCombisWithExcLevel.resize(mModeCombisVec.back().Size()+I_1, I_0);
   }
   mNumModeCombisWithExcLevel.shrink_to_fit();

   // Clear hash map, reserve space for all ModeCombi%s to avoid rehashing
   // along the way.
   mModeCombiHashMap.clear();
   mModeCombiHashMap.reserve(mModeCombisVec.size());

   // Loop through ModeCombi%s and update everything accordingly.
   std::unordered_set<In> tmp_modes;
   Uin ref_num = I_0;
   for(  auto it = mModeCombisVec.begin(), end = mModeCombisVec.end()
      ;  it != end
      ;  ++it, ++ref_num
      )
   {
      auto& mc = *it;
      tmp_modes.insert(mc.MCVec().begin(), mc.MCVec().end());
      ++mNumModeCombisWithExcLevel.at(mc.Size());
      mc.AssignModeCombiRefNr(ref_num);
      mModeCombiHashMap[mc.MCVec()] = ref_num;
   }

   // Assign and sort mModesContained.
   mModesContained.assign(tmp_modes.begin(), tmp_modes.end());
   std::sort(mModesContained.begin(), mModesContained.end());
}


//==============================================================================
//==============================================================================
//
//    NON-MEMBER FUNCTIONS
//
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * @param[in] arVec
 *    Sort this vector, remove any duplicates, then resize and shrink to fit.
 ******************************************************************************/
void SortAndRemoveDuplicates
   (  std::vector<ModeCombi>& arVec
   )
{
   std::stable_sort(arVec.begin(), arVec.end());
   const auto new_end = std::unique(arVec.begin(), arVec.end());
   arVec.resize(std::distance(arVec.begin(), new_end));
   arVec.shrink_to_fit();
}

/***************************************************************************//**
 * @param[in,out] arOs
 *    The stream to output to.
 * @param[in] arMcr
 *    The ModeCombiOpRange whose ModeCombi%s will be printed.
 * @return
 *    A reference to the ostream.
 ******************************************************************************/
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const ModeCombiOpRange& arMcr
   )
{
   Uin count = I_0;
   for(const auto& mc: arMcr)
   {
      arOs << "  " << std::setw(5) << count << "  " << mc << '\n';
      ++count;
   }
   arOs << std::flush;
   return arOs;
}

/***************************************************************************//**
 * Check if ModeCombiOpRange contains any ModeCombi for which there is both a
 * mode in common with the first ModeCombi, and a mode (possibly another one)
 * in common with the other ModeCombi.
 * E.g.: `true` if arMcr contains {1,2,3}, arMp = {0,1}, and arMt = {3,4}.
 *
 * @param[in] arMcr
 *    The ModeCombiOpRange that is scanned for ModeCombi%s that might connect
 *    the other arguments.
 * @param[in] arMp
 *    One ModeCombi.
 * @param[in] arMt
 *    Another ModeCombi.
 * @return
 *    Whether the ModeCombi%s are connected by a ModeCombi in the
 *    ModeCombiOpRange.
 ******************************************************************************/
bool DoesMcrConnectMcs
   (  const ModeCombiOpRange& arMcr
   ,  const ModeCombi& arMp
   ,  const ModeCombi& arMt
   )
{
   for(const auto& mc: arMcr)
   {
      // If there's an intersection between mc and both of the others, they're
      // connected.
      if (!ZeroIntersect(mc, arMp) && !ZeroIntersect(mc, arMt))
      {
         return true;
      }
   }
   // Otherwise, if we get to here, they weren't connected.
   return false;
}

/***************************************************************************//**
 * Build a _new_ ModeCombiOpRange consisting of those ModeCombi%s in the
 * ModeCombiOpRange argument that have any mode indices in common with the
 * ModeCombi argument.
 *
 * E.g.: `arMcr = { {}, {0}, {1}, {0,1} }` and `arMp  = {1,2}`.
 * Then the resulting ModeCombiOpRange is `{1}, {0,1}`, because those are the
 * ones that have a mode in common (1) with `{1,2}`.
 *
 * @note
 *    If arMcr is the empty range _or_ if arMp is an empty ModeCombi, then the
 *    result will be an empty ModeCombiOpRange.
 *
 * @param[in] arMcr
 *    The ModeCombiOpRange whose ModeCombi%s are used as foundation.
 * @param[in] arMc
 *    The ModeCombi against which the ModeCombi%s of arMcr are compared.
 * @return
 *    The newly built ModeCombiOpRange.
 ******************************************************************************/
ModeCombiOpRange BuildCommonMcr
   (  const ModeCombiOpRange& arMcr
   ,  const ModeCombi& arMc
   )
{
   std::vector<ModeCombi> tmp_vec;
   for(const auto& mc: arMcr)
   {
      if (!ZeroIntersect(mc, arMc))
      {
         tmp_vec.emplace_back(mc);
      }
   }
   ModeCombiOpRange mcr;
   mcr.Insert(std::move(tmp_vec));
   return mcr;
}


/***************************************************************************//**
 * Example:
 * `ModeCombisFixedNonFixedModes(2, 3, {0,1,2}, {4,5})` yields the ModeCombi%s
 * with 2 to 3 of the modes in `{0,1,2}` and both of `{4,5}` in all of them;
 *     {0,1,4,5}
 *     {0,2,4,5}
 *     {1,2,4,5}
 *     {0,1,2,4,5}
 * All ModeCombi%s will have ModeCombi::Address() -1.
 *
 * @param[in] aMinNumNonFixed
 *    The minimum number of modes to take from arSourceOfModes.
 * @param[in] aMaxNumNonFixed
 *    The maximum number of modes to take from arSourceOfModes.
 * @param[in] arSourceOfModes
 *    The variable/non-fixed modes in the generated ModeCombi%s.
 * @param[in] arFixedModes
 *    These modes will be contained by all the generated ModeCombi%s.
 * @return
 *    Vector of ModeCombi%s with same fixed modes and various combinations of
 *    non-fixed modes from arSourceOfModes.
 ******************************************************************************/
std::vector<ModeCombi> ModeCombisFixedNonFixedModes
   (  Uin aMinNumNonFixed
   ,  Uin aMaxNumNonFixed
   ,  const std::vector<In>& arSourceOfModes
   ,  const std::vector<In>& arFixedModes
   )
{
   std::vector<ModeCombi> vec_mc;
   for(  OrderedCombination oc(aMinNumNonFixed, aMaxNumNonFixed+I_1, arSourceOfModes, arFixedModes)
      ;  !oc.AtEnd()
      ;  ++oc
      )
   {
      vec_mc.emplace_back(oc.GetCombination(), -I_1);
   }
   return vec_mc;
}

/***************************************************************************//**
 * Example:
 * `ModeCombisFixedNonFixedModesRestricted(2, 3, {0,1,2}, {4,5}, range)` yields
 * those ModeCombi%s with 2 to 3 of the modes in `{0,1,2}` and both of `{4,5}`,
 * i.e. that are _also_ contained by the given `range`. If `range` does _not_
 * contain
 * `{{0,1,4,5}, {0,1,2,4,5}}` then only the ModeCombi%s
 *     {0,2,4,5}
 *     {1,2,4,5}
 * will be generated.
 *
 * The ModeCombi::Address() and ModeCombiRefNr() of the generated ModeCombi%s
 * will be equal to those of the corresponding ModeCombi in the restriction
 * range.
 *
 * If the restriction range is _empty_, then no restrictions apply,
 * corresponding to just having called ModeCombisFixedNonFixedModes().
 *
 * @param[in] aMinNumNonFixed
 *    The minimum number of modes to take from arSourceOfModes.
 * @param[in] aMaxNumNonFixed
 *    The maximum number of modes to take from arSourceOfModes.
 * @param[in] arSourceOfModes
 *    The variable/non-fixed modes in the generated ModeCombi%s.
 * @param[in] arFixedModes
 *    These modes will be contained by all the generated ModeCombi%s.
 * @param[in] arRestrictionRange
 *    Only generates ModeCombi%s that are contained by this range.
 * @return
 *    Vector of ModeCombi%s with same fixed modes and various combinations of
 *    non-fixed modes from arSourceOfModes; restricted to those ModeCombi%s
 *    that are also in arRestrictionRange.
 ******************************************************************************/
std::vector<ModeCombi> ModeCombisFixedNonFixedModesRestricted
   (  Uin aMinNumNonFixed
   ,  Uin aMaxNumNonFixed
   ,  const std::vector<In>& arSourceOfModes
   ,  const std::vector<In>& arFixedModes
   ,  const ModeCombiOpRange& arRestrictionRange
   )
{
   if (arRestrictionRange.Size() > I_0)
   {
      std::vector<ModeCombi> vec_mc;
      ModeCombiOpRange::const_iterator iter_restr_range;
      for(  OrderedCombination oc
               (  aMinNumNonFixed
               ,  aMaxNumNonFixed+I_1
               ,  arSourceOfModes
               ,  arFixedModes
               )
         ;  !oc.AtEnd()
         ;  ++oc
         )
      {
         ModeCombi mc(oc.GetCombination(), -I_1);
         if (arRestrictionRange.IfContainedAssignAddressAndRefNum(mc))
         {
            vec_mc.push_back(std::move(mc));
         }
      }
      return vec_mc;
   }
   else
   {
      return ModeCombisFixedNonFixedModes
         (  aMinNumNonFixed
         ,  aMaxNumNonFixed
         ,  arSourceOfModes
         ,  arFixedModes
         );
   }
}

/***************************************************************************//**
 * Sets the argument vector of ModeCombi%s to the range whose ModeCombi%s can
 * have nonzero overlap with
 * \f[
 *    \langle \mathbf{m} \vert \hat{O}
 * \f]
 * where \f$\mathbf{m}\f$ is `arLeftModeCombi` and \f$\hat{O}\f$ is an operator
 * with the ModeCombiOpRange `arOpRange`.
 *
 * It can be used in _way_ too many different ways. As of this writing
 * (February 2017, MBH) it is used in three different ways throughout the
 * current source code.
 *
 *    1. Build (possibly restricted) range (possibly adding to or clearing
 *       existing range). This is the simplest one, with `aIso = false`,
 *       `aIsOrder = 0`, `aCheckCouplingWhile = false`. `arModeCombiCheck` has
 *       no effect so set it to whatever.
 *       If `arRestrictRange` is nonempty, ModeCombi%s will only be added if
 *       also in this range, _and_ if `aRestrict = true`. If either
 *       `arRestrictRange` is empty _or_ `aRestrict = false`, there are no
 *       restrictions.
 *       `aAddTo` determines whether or not to add to the existing range in
 *       `*this` or clear `*this` beforehand.
 *       `arRemovedSome` is _not_ modified so has no meaning here.
 *    2. If `aCheckCouplingWhile = true` then `arModeCombiCheck` and
 *       `arRemovedSome` now has a meaning and `arRestrictRange` has _another_
 *       meaning - otherwise as above.
 *       It is checked for each operator ModeCombi that it is connected to
 *       `arModeCombiCheck` through the supplied `arRestrictRange` (see
 *       DoesMcrConnectMcs()). If not, that operator ModeCombi is skipped when
 *       building the new range, and `arRemovedSome` is set to `true`.
 *    3. If `aIso = true` and `aIsOrder >= 2` then build new ModeCombiOpRange
 *       as normal (such as in (1)), then call SetToLeftOperRange() recursively
 *       (aIsOrder - 1 times), each time letting the operator ModeCombi%s
 *       interact with all the ModeCombi%s in `*this`, thus expanding `*this`
 *       space each time.
 *       From input keyword #3 ISO in manual: This restricts the excitation
 *       space in a mode-combination sense, to those that interact with the
 *       reference state to a given order.
 *
 * My impression is that (2) and (3) are special cases that are not in common
 * everyday use when running calcs. with Midas. -MBH
 * Also, see ModeCombiOpRangeTest%s for examples on usage.
 *
 * @note
 *    If arRestrictionRange used, then ModeCombi%s in arRestrictionRange must
 *    have their addresses and ref. numbers set equal to the corresponding ones
 *    in arRestrictionRange.
 *
 * @see test/ModeCombiOpRangeTest.h
 *
 * @param[in,out] arMcr
 *    The vector of ModeCombi%s to be modified.
 *
 * @param[out] arRemovedSome
 *    Info about whether anything was removed during the process. Can _not_ be
 *    modified unless aCheckCouplingWhile = true, therefore often used with a
 *    dummy variable.
 *
 * @param[in] arLeftModeCombi
 *    The ModeCombi in the bra.
 *
 * @param[in] arModeCombiCheck
 *    Only _ever_ plays a role if aCheckCouplingWhile = `true` (so use any
 *    dummy if false). If true, see description above.
 *
 * @param[in] arOpRange
 *    The ModeCombiOpRange of the operator under investigation.
 *
 * @param[in] arRestrictRange
 *    Generally, ModeCombi%s are only inserted in new range if they are also in
 *    this range. An empty range here means no restriction. 
 *    If `aCheckCouplingWhile = true` see description above.
 *
 * @param[in] aRestrict
 *    If restriction to (a nonempty) arRestrictRange is desired, aRestrict must
 *    be true.
 *
 * @param[in] aAddTo
 *    If true add new ModeCombi%s to the existing ones, otherwise clear `*this`
 *    before adding.
 *
 * @param[in] aIso
 *    Whether to use interaction space order, and if so, aIsOrder is the order.
 *
 * @param[in] aIsOrder
 *    The interaction space order. See description above.
 *    Only has an effect if > 1.
 *
 * @param[in] aCheckCouplingWhile
 *    If true use arModeCombiCheck and arRestrictRange to trim some ModeCombi%s
 *    away, and set arRemovedSome to true if that happens. See description
 *    above.
 ******************************************************************************/
void SetToLeftOperRange
   (  std::vector<ModeCombi>& arMcr
   ,  bool& arRemovedSome
   ,  const ModeCombi& arLeftModeCombi
   ,  const ModeCombi& arModeCombiCheck
   ,  const ModeCombiOpRange& arOpRange
   ,  const ModeCombiOpRange& arRestrictRange
   ,  const bool aRestrict
   ,  const bool aAddTo
   ,  const bool aIso
   ,  const In aIsOrder
   ,  const bool aCheckCouplingWhile
   )
{
   // Check whether restrictions _actually_ apply.
   bool actually_restricted = aRestrict && arRestrictRange.Size() != I_0;

   // Clear contents if not adding to existing.
   if (!aAddTo)
   {
      arMcr.clear();
   }

   // If aCheckCouplingWhile = true, make a local common MCR outside of loop
   // that will make DoesMcrConnectMcs faster. If false, just leave it empty
   // (not used in that case).
   ModeCombiOpRange common_mcr;
   if (aCheckCouplingWhile)
   {
      common_mcr = BuildCommonMcr(arRestrictRange,arModeCombiCheck);
   }

   // Loop over mode combinations in the operator. 
   for(const auto& oper_mc: arOpRange)
   {
      ModeCombi in_left_but_not_oper;
      in_left_but_not_oper.InFirstOnly(arLeftModeCombi,oper_mc);

      // If aCheckCouplingWhile = true, and a suitable (nonempty)
      // arModeCombiCheck is provided...
      // Check then if
      // a) mt and mp has no overlap; mp n mt = {} 
      // b) if no overlap are they at all connected
      //    For all m in MCR[T];  not (m n mp != {} and m n mt != {})
      //    Thus if T = T(mp-and subsets) + T(mt and subsets) + T (no mp,mt modes)
      //    then there is no contribution
      bool skip = false;
      if (aCheckCouplingWhile && arModeCombiCheck.Size()!= I_0)
      {
         if (ZeroIntersect(arModeCombiCheck,oper_mc)) 
         {
            skip = !DoesMcrConnectMcs(common_mcr, arModeCombiCheck, oper_mc);
         }
      }
      
      // If skipping this operator ModeCombi, update arRemovedSome and do
      // nothing. If not skipping, call ModeCombisFixedNonFixedModesRestricted
      // to add needed ModeCombis.
      if (skip)
      {
         arRemovedSome = true;
      }
      else
      {
         auto&& tmp_vec = ModeCombisFixedNonFixedModesRestricted
            (  I_0
            ,  oper_mc.MCVec().size()
            ,  oper_mc.MCVec()
            ,  in_left_but_not_oper.MCVec()
            ,  (actually_restricted? arRestrictRange: ModeCombiOpRange())
            );
         // If not aRestrict, but arRestrictRange nonempty, the ModeCombis of
         // tmp_vec should however still get addresses and ref.nums from
         // arRestrictRange.
         if (!aRestrict && arRestrictRange.Size() != I_0)
         {
            for(auto&& mc: tmp_vec)
            {
               arRestrictRange.IfContainedAssignAddressAndRefNum(mc);
            }
         }
         // Then push_back into arMcr.
         arMcr.reserve(arMcr.size() + tmp_vec.size());
         for(auto&& mc: tmp_vec)
         {
            arMcr.push_back(std::move(mc));
         }
      }
   }
   // Then sort and make unique.
   SortAndRemoveDuplicates(arMcr);
   
   // If aIso, aIsOrder given, call function recursively with own ModeCombi as
   // arLeftModeCombi to build up interaction order space.
   if (aIso && aIsOrder >I_1)
   {
      for(In i_order=I_2;i_order<=aIsOrder;i_order++)
      {
         // Make temp. copy to avoid interference with newly added ModeCombis.
         std::vector<ModeCombi> tmp(arMcr);
         for(const auto& mc: tmp)
         {
            SetToLeftOperRange(arMcr,arRemovedSome,mc,mc,arOpRange,arRestrictRange, true, true);
         }
      }
   }
}

/***************************************************************************//**
 * Utility function for SubSetSets(const ModeCombi&,
 * std::vector<ModeCombiOpRange>&, const ModeCombiOpRange&, In).
 ******************************************************************************/
void SubSetSetsImpl
   (  const ModeCombi& arModeCombi
   ,  In aI
   ,  In aNvset
   ,  std::vector<std::set<ModeCombi>>& arVecSetModeCombis
   ,  std::set<In>* apVset
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  In aMinSize
   )
{
   if (aI < arModeCombi.Size())
   {
      // Make local copy of the Set
      std::set<In>* vset =  new std::set<In> [aNvset+I_1];
      for (In j=I_0; j<aNvset; j++)
      {
         vset[j] = apVset[j];
      }
      aI++;
      for (In j=I_0; j<aNvset; j++)
      {
         if (j > I_0)
         {
            vset[j-I_1] = apVset[j-I_1]; // reset
         }
         vset[j].insert(arModeCombi.Mode(aI-I_1));
         SubSetSetsImpl(arModeCombi, aI, aNvset, arVecSetModeCombis, vset, arModeCombiOpRange, aMinSize);
      }
      if (aNvset > I_0)
      {
         vset[aNvset-I_1] = apVset[aNvset-I_1]; // reset
      }
      vset[aNvset].insert(arModeCombi.Mode(aI-I_1));
      aNvset++;
      SubSetSetsImpl(arModeCombi, aI, aNvset, arVecSetModeCombis, vset, arModeCombiOpRange, aMinSize);
      delete[] vset;
   }
   else
   {
      arVecSetModeCombis.push_back(std::set<ModeCombi>());
      In inew = arVecSetModeCombis.size()-I_1; 
      bool del = false;
      for (In i=I_0; i<aNvset; i++)
      {
         ModeCombi tmp_mc(apVset[i], -I_1);
         // If the mode combi is allowed then add it.
         if (arModeCombiOpRange.IfContainedAssignAddressAndRefNum(tmp_mc))
         {
            arVecSetModeCombis[inew].insert(tmp_mc);
         }
         else
         {
            del = true;
            break;
         }
         if (tmp_mc.Size() < aMinSize) // Peter. This is REALLY UGLY. THINK.
         {
            del = true;
            break;
         }
      }
      if (del)
      {
         arVecSetModeCombis.pop_back();
      }
   }
}

/***************************************************************************//**
 * Find the different partitions of the argument ModeCombi, under certain
 * restrictions; each sub-ModeCombi in a partition must
 *    -  be in arModeCombiOpRange
 *    -  have size (excitation level) greater than or equal to aMinSize
 * The sub-ModeCombi%s generated _shall_ keep the same addresses/ref.nums.
 * that they have in arModeCombiOpRange.
 *
 * Example: arModeCombi = {0,1,2,3} and aMinSize = 2 gives the partitions
 * (stored in arModeCombiOpRangeVec):
 *    -  {0,1}, {2,3}
 *    -  {0,2}, {1,3}
 *    -  {0,3}, {1,2}
 *
 * Example: arModeCombi = {0,1,2} and aMinSize = 1 gives the partitions (stored
 * in arModeCombiOpRangeVec):
 *    -  {0}, {1}, {2}
 *    -  {0}, {1,2}
 *    -  {1}, {0,2}
 *    -  {2}, {0,1}
 *    -  {0,1,2}
 *
 * @note
 *    I am not sure which order they will appear in the arModeCombiOpRangeVec,
 *    however. -MBH, Mar 2017.
 *
 * @param[in] arModeCombi
 *     The ModeCombi to partition.
 *
 * @param[out] arVecVecModeCombis
 *    Vector to store generated vectors of ModeCombi%s.
 *
 * @param[in] arModeCombiOpRange
 *    For some generated MCR, each MC must be within this MCR.
 *
 * @param[in] aMinSize
 *    For some generated MCR, each MC must have at least this size/excitation
 *    level.
 ******************************************************************************/
void SubSetSets
   (  const ModeCombi& arModeCombi
   ,  std::vector<std::vector<ModeCombi>>& arVecVecModeCombis
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  In aMinSize
   )
{
   In i = I_0;
   In nv = I_0;
   std::set<In>* vset = new std::set<In> [nv];
   std::vector<std::set<ModeCombi>> vec_set_mode_combis;
   SubSetSetsImpl(arModeCombi, i, nv, vec_set_mode_combis, vset, arModeCombiOpRange, aMinSize);

   // Save results in vec. of vec. of ModeCombis.
   arVecVecModeCombis.clear();
   arVecVecModeCombis.reserve(vec_set_mode_combis.size());
   for(auto&& set_mcs: vec_set_mode_combis)
   {
      arVecVecModeCombis.emplace_back(set_mcs.begin(), set_mcs.end());
   }
   delete[] vset;
}

/***************************************************************************//**
 * @param[in,out] arMcr
 *    The ModeCombiOpRange to update.
 * @param[in] aLevel
 *    Consider ModeCombi%s of arMcr with exactly this many modes, i.e. will
 *    only erase such ModeCombi%s.
 * @param[in] arAdgaPreScreenThr
 *    The thresholds used for the presreening.
 * @param[in] arAnalysisDir
 *    The analysis directory where Adga calculation is run. 
 * @param[in] arMultilevelAnalysisIoDir
 *    The analysis sub-directory for the particular multilevel (level_i) that is to be updated.
 * @param[in] arMultilevelSaveIoDir 
 *    The savedir sub-directory for the particular multilevel (level_i) that is to be updated.
 ******************************************************************************/
void UpdateAdga
   (  ModeCombiOpRange& arMcr
   ,  Uin aLevel
   ,  const std::vector<Nb>& arAdgaPreScreenThr
   ,  const std::string& arAnalysisDir
   ,  const std::string& arMultilevelAnalysisIoDir
   ,  const std::string& arMultilevelSaveIoDir 
   )
{
   // Files used in the screening analysis
   std::string int_file = arMultilevelAnalysisIoDir + "/MaxInts_all";
   std::string vibcalc_int_file = arAnalysisDir+ "/MaxInts_all";
   std::string numder_file = arMultilevelSaveIoDir + "/prop_no_1.mop"; 

   // Copy screening estimates to where the analysis is carried out
   midas::filesystem::Copy(vibcalc_int_file, int_file);

   //
   McMeasure adga_pre_screen(int_file, numder_file);
   adga_pre_screen.SetScreeningType(1);
   adga_pre_screen.SetmThreshs(arAdgaPreScreenThr);
   FlexCouplings flex_couplings(adga_pre_screen);

   Mout << std::endl << " Mode combination screening resulted in the following: " << std::endl;
   std::vector<ModeCombi> v_erase;
   for (const auto& m: arMcr)
   {
      if (m.Size() != aLevel)
      {
         continue;
      }

      if (flex_couplings.MaxLevel(m.MCVec(), arMcr.GetMaxExciLevel()) < m.Size())
      {
         v_erase.emplace_back(m);
         Mout << "  MC " << m.MCVec() << "  is removed from the MCR"  << std::endl; 
      }
      else 
      {
         Mout << "  MC " << m.MCVec() << "  is considered in the MCR"  << std::endl; 
      }
   }
   arMcr.Erase(v_erase);
}

/***************************************************************************//**
 * The type of constructed ModeCombiOpRange is determined based on the contents
 * of the FlexCoupCalcDef.
 * -  If FlexCoupCalcDef::GetmSetGroupCouplings(), then constructs a
 *    ModeCombiOpRange(const GroupCouplings&);
 * -  If arFlexCoupCalcDef.GetmSetSystemCoup() or
 *    arFlexCoupCalcDef.GetmMcScreen(), then constructs a
 *    ModeCombiOpRange(const FlexCouplings&, Uin, Uin).
 * -  Otherwise, constructs an ordinary ModeCombiOpRange(Uin, Uin).
 *
 * @note
 *    It causes a hard error if both of the first two options are "activated"
 *    in the FlexCoupCalcDef.
 *
 * @note
 *    This is wrapper for some legacy functions; prefer to use the proper
 *    constructors.
 ******************************************************************************/
ModeCombiOpRange ConstructModeCombiOpRange
   (  const FlexCoupCalcDef& arFlexCoupCalcDef
   ,  Uin aMaxExciLevel
   ,  Uin aNumModes
   )
{
   // Check for correct type of input
   if (  arFlexCoupCalcDef.GetmSetGroupCouplings()
      && (  arFlexCoupCalcDef.GetmSetSystemCoup()
         || arFlexCoupCalcDef.GetmMcScreen()
         || arFlexCoupCalcDef.GetmSetFlexCoup()
         )
      )
   {
      MIDASERROR("Group couplings cannot (yet) be combined with other measures in FlexCoup");
   }

   // If coupling between groups of molecules are defined then we construct the MCR based on this
   if (arFlexCoupCalcDef.GetmSetGroupCouplings())
   {
      if (gVibIoLevel > I_2 || gPesIoLevel > I_2)
      {
         Mout << " Generating a mode combination range based on group couplings " << std::endl;
      }

      return ModeCombiOpRange
         (  GroupCouplings
            (  arFlexCoupCalcDef.GetmModeGroups()
            ,  arFlexCoupCalcDef.GetmGroupCombis()
            )
         );
   }
   // If other forms of flexible coupling is indicate then construct the MCR based on this
   else if  (  arFlexCoupCalcDef.GetmSetSystemCoup()
            || arFlexCoupCalcDef.GetmMcScreen()
            || arFlexCoupCalcDef.GetmSetFlexCoup()
            )
   {
      if (gVibIoLevel > I_2 || gPesIoLevel > I_2)
      {
         Mout << " Generating a mode combination range based on flexible choice of mode couplings " << std::endl;
      }

      ModeCombiOpRange return_mcr(FlexCouplings(arFlexCoupCalcDef), aMaxExciLevel, aNumModes);
      
      if (gVibIoLevel > I_4)
      {
         Mout << " The actual size of the ModeCombiOpRange is " << return_mcr.Size() << std::endl;

         if (gDebug || gVibIoLevel > I_9)
         {
            Mout << " The mode combinations are \n" << return_mcr << std::endl;
         }
      }

      return return_mcr;
   }
   // Construct a standard MCR based on the binomial coefficient 
   else
   {
      if (gVibIoLevel > I_2 || gPesIoLevel > I_2)
      {
         Mout << " Generating a mode combination range " << std::endl;
      }
      
      return ModeCombiOpRange(aMaxExciLevel, aNumModes);
   }
}

/***************************************************************************//**
 * Constructs one of two constructors based on:
 * -  If arMaxExPerMode is nonempty, will call ModeCombiOpRange(Uin, const
 *    std::vector<Uin>&). However, it will first assert the arguments, i.e.
 *    that arMaxExPerMode.size() equals aNumModes.
 *    Hard error if this is not the case.
 * -  If arMaxExPerMode is empty, will return ModeCombiOpRange(Uin, Uin).
 *
 * @note
 *    This is wrapper for some legacy functions; prefer to use the proper
 *    constructors.
 ******************************************************************************/
ModeCombiOpRange ConstructModeCombiOpRange
   (  Uin aMaxExciLevel
   ,  const std::vector<In>& arMaxExPerMode
   ,  Uin aNumModes
   )
{
   if (arMaxExPerMode.empty())
   {
      return ModeCombiOpRange(aMaxExciLevel, aNumModes);
   }
   else
   {
      if (arMaxExPerMode.size() != aNumModes)
      {
         std::stringstream err;
         err   << "arMaxExPerMode.size() (which is " << arMaxExPerMode.size()
               << ") != aNumModes (which is " << aNumModes
               << ")";
         MIDASERROR(err.str());
         return ModeCombiOpRange(); // To quench compiler warning.
      }
      else
      {
         return ModeCombiOpRange(aMaxExciLevel, std::vector<Uin>(arMaxExPerMode.begin(), arMaxExPerMode.end()));
      }
   }
}

/***************************************************************************//**
 * Construct a range that is _extended_ with certain ModeCombi%s involving the
 * given special modes, that do not contribute to the excitation level of the
 * ModeCombi.
 *
 * It's a wrapper for ConstructFromEffectiveExciCount() where the given modes
 * count 0, and the others 1, towards the effective excitation level.
 *
 * Example:
 *     aMaxEffectiveExciLevel = 1
 *     aNumModes              = 4
 *     arExtendedRangeModes   = {1,3}
 * constructs the range
 *     {}         // eff. level = 0
 *     {0}        // eff. level = 1
 *     {1}        // eff. level = 0
 *     {2}        // eff. level = 1
 *     {3}        // eff. level = 0
 *     {0,1}      // eff. level = 1
 *     {0,3}      // eff. level = 1
 *     {1,2}      // eff. level = 1
 *     {1,3}      // eff. level = 0
 *     {2,3}      // eff. level = 1
 *     {0,1,3}    // eff. level = 1
 *     {1,2,3}    // eff. level = 1
 * The following ModeCombi%s are excluded due to too high effective exc. level:
 *     {0,2}      // eff. level = 2
 *     {0,1,2}    // eff. level = 2
 *     {0,2,3}    // eff. level = 2
 *     {0,1,2,3}  // eff. level = 2
 *
 * @note
 *    For now, all arExtendedRangeModes modes `m` must be in
 *    `{0,...,aNumModes-1}`; hard error otherwise.
 *
 * @param[in] aMaxEffectiveExciLevel
 *    The maximum _effective_ excitation level.
 * @param[in] aNumModes
 *    The number of modes in the range, i.e. take mode numbers from
 *    `{0,...,aNumModes-1}`.
 * @param[in] arExtendedRangeModes
 *    The modes for which the range will have _extended_ ModeCombi%s, i.e. the
 *    modes that count 0 towards a ModeCombi's excitation level.
 * @return
 *    The _extended_ ModeCombiOpRange.
 ******************************************************************************/
ModeCombiOpRange ConstructModeCombiOpRangeFromExtendedRangeModes
   (  Uin aMaxEffectiveExciLevel
   ,  Uin aNumModes
   ,  const std::set<In>& arExtendedRangeModes
   )
{
   if (gVibIoLevel > I_2 || gPesIoLevel > I_2)
   {
      Mout << " Generating an extended mode combination range " << std::endl;
   }

   // Assert that modes are not out-of-range.
   if (!arExtendedRangeModes.empty())
   {
      if (  *arExtendedRangeModes.begin() < I_0
         || *arExtendedRangeModes.rbegin() >= aNumModes
         )
      {
         std::stringstream ss;
         ss << "The extended range modes { ";
         for(const auto& m: arExtendedRangeModes)
         {
            ss << m << ' ';
         }
         ss << "} are not contained in the mode pool {";
         if (aNumModes > I_0)
         {
            ss << "0,...," << aNumModes - 1 << "}";
         }
         else
         {
            ss << "}";
         }
         ss << " (aNumModes = " << aNumModes << ").";
         MIDASERROR(ss.str());
      }
   }

   // Construct the effective excitation count vector; all 1's, except for the
   // special modes, which are 0.
   std::vector<Uin> v_eff(aNumModes, I_1);
   for(const auto& m: arExtendedRangeModes)
   {
      v_eff.at(m) = I_0;
   }

   // Then call constructor.
   return ModeCombiOpRange::ConstructFromEffectiveExciCount(aMaxEffectiveExciLevel, v_eff);
}

/***************************************************************************//**
 * @param[in] arMcr
 *    ModeCombiOpRange to be checked.
 * @param[in] aNumModesExpected
 *    The number of modes you expect the ModeCombiOpRange to contain. Usually
 *    this should be the number of modes of the system/OpDef.
 * @param[in] arErrShort
 *    Stream to which a shorter error diagnostics message will be output.
 * @param[in] arErrLong
 *    Stream to which a longer error diagnostics message will be output.
 * @param[in] aMustContainEmptyMc
 *    Whether to assert the empty ModeCombi is contained.
 * @param[in] aMustContainCanonicalOneModeMcs
 *    Whether to assert the 1-mode ModeCombi%s are `{0},...,{M-1}`, `M` being
 *    `aNumModesExpected`.
 ******************************************************************************/
bool ValidateExciMcr
   (  const ModeCombiOpRange& arMcr
   ,  Uin aNumModesExpected
   ,  std::ostream& arErrShort
   ,  std::ostream& arErrLong
   ,  bool aMustContainEmptyMc
   ,  bool aMustContainCanonicalOneModeMcs
   )
{
   const auto err_short_flags = arErrShort.flags();
   const auto err_long_flags = arErrLong.flags();

   bool valid = true;
   std::stringstream err;

   // Check number of modes, and that modes are in range {0,...,M-1}.
   if (arMcr.NumberOfModes() != aNumModesExpected)
   {
      valid = false;
      err   << "   "
            << "Num. modes (" << arMcr.NumberOfModes()
            << ") not equal to expectation (" << aNumModesExpected
            << ")."
            << "\n"
            ;
   }
   if (arMcr.NumberOfModes() > 0)
   {
      if (arMcr.SmallestMode() < 0)
      {
         valid = false;
         err   << "   "
               << "Smallest mode (which is " << arMcr.SmallestMode()
               << ") is < 0."
               << "\n"
               ;
      }
      if (arMcr.LargestMode() >= aNumModesExpected)
      {
         valid = false;
         err   << "   "
               << "Largest mode (which is " << arMcr.LargestMode()
               << ") is >= the expected num. modes (which is " << aNumModesExpected
               << ")."
               << "\n"
               ;
      }
   }

   // Check empty MC.
   if (aMustContainEmptyMc)
   {
      if (arMcr.NumEmptyMCs() != 1)
      {
         valid = false;
         err   << "   "
               << "The empty ModeCombi is unexpectedly not contained."
               << "\n"
               ;
      }
   }

   // Check canonical 1-mode ModeCombi%s, i.e. {0},...,{M-1}, for expected
   // number of modes M.
   if (aMustContainCanonicalOneModeMcs)
   {
      // It's enough to check that there are M 1-modes, and that they go from
      // {0} to {M-1}.
      bool canonical_1modes = true;
      if (arMcr.NumModeCombisWithExcLevel(1) != aNumModesExpected)
      {
         canonical_1modes = false;
      }
      if (  arMcr.Begin(1) != arMcr.End(1)
         && arMcr.NumberOfModes() > 0
         && aNumModesExpected > 0
         )
      {
         auto mc1_first = *(arMcr.Begin(1));
         auto mc1_last = *(arMcr.End(1)-1);
         if (mc1_first.MCVec() != std::vector<In>{0})
         {
            canonical_1modes = false;
         }
         if (mc1_last.MCVec() != std::vector<In>{In(aNumModesExpected) - 1})
         {
            canonical_1modes = false;
         }
      }

      // Print some more detailed diagnostics in case of error.
      if (!canonical_1modes)
      {
         valid = false;
         err   << "   "
               << "Uncanonical 1-mode ModeCombis; expected "
               ;
         if (aNumModesExpected == 0)
         {
            err << "none.";
         }
         else if (aNumModesExpected == 1)
         {
            err << "{0}.";
         }
         else
         {
            err << "{0},...,{" << aNumModesExpected - 1 << "}.";
         }
         err << '\n';

         err   << "   "
               << "The following 1-mode ModeCombis are missing from the canonical range:\n"
               ;
         auto it = arMcr.Begin(1);
         std::vector<In> mc(1);
         bool canonical_missing = false;
         for(Uin m = 0; m < aNumModesExpected; ++m)
         {
            mc.at(0) = m;
            if (!arMcr.Contains(mc))
            {
               canonical_missing = true;
               err   << "      " << mc << '\n';
            }
            else
            {
               // Increment iterator so it'll end up pointing to the first
               // ModeCombi beyond the canonical 1-mode ModeCombi range.
               //++it;
               arMcr.Find(mc, it);
            }
         }
         if (!canonical_missing)
         {
            err   << "      " << "(none)" << '\n';
         }

         err   << "   "
               << "The following 1-mode ModeCombis are outside the canonical range:\n"
               ;
         bool canonical_outside = false;
         ++it; // Now points to End(1) or the first 1-ModeCombi not in canonical range.
         for(auto end = arMcr.End(1); it < end; ++it)
         {
            canonical_outside = true;
            err   << "      " << it->MCVec() << '\n';
         }
         if (!canonical_outside)
         {
            err   << "      " << "(none)" << '\n';
         }
      }
   }

   if (!valid)
   {
      std::stringstream err_beg;
      err_beg  << "Unusual ModeCombiOpRange that the current implementation\n"
               << "is not fit to handle; stopping here to avoid errors!\n"
               ;
      std::stringstream err_help;
      err_help << "(Full ModeCombiOpRange printed to output file.)\n"
               << "If possible, try adjusting your input to make these errors go away,\n"
               << "or contact support.\n"
               ;
      arErrShort << err_beg.str() << err.str() << err_help.str();
      arErrLong << err_beg.str() << err.str() << err_help.str() << std::endl;
      arErrLong << "Number of ModeCombis  = " << arMcr.Size() << std::endl;
      arErrLong << "Max. excitation level = " << arMcr.GetMaxExciLevel() << std::endl;
      arErrLong << "Modes contained       = " << arMcr.ModesContained() << std::endl;
      arErrLong << "Number of ModeCombis with excitation level:\n";
      for(Uin i = 0; i <= arMcr.GetMaxExciLevel(); ++i)
      {
         arErrLong 
            << std::right << "   " << "exc. level " << std::setw(2) << i
            << ": " << std::setw(6) << arMcr.NumModeCombisWithExcLevel(i)
            << '\n'
            ;
      }
      arErrLong << std::flush;
      arErrLong << "ModeCombiOpRange = \n" << arMcr << std::endl;
   }

   arErrShort.flags(err_short_flags);
   arErrLong.flags(err_long_flags);

   return valid;
}


/***************************************************************************//**
 *
 ******************************************************************************/
template<typename I>
Uin SetAddressesImpl
   (  ModeCombiOpRange& arMcr
   ,  const std::vector<I>& arNumModals
   )
{
   Uin add = 0;
   for(auto it = arMcr.begin(), end = arMcr.end(); it != end; ++it)
   {
      arMcr.AssignAddress(it, add);
      add += NumParams(*it, arNumModals);
   }
   return add;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arMcr       = {}, {0}, {1,2}, {0,1,2}
 *    arNumModals = {3, 5, 4}
 *    addresses   = {}     :   0  (always)
 *                  {0}    :   1  (=  0 + 1)
 *                  {1,2}  :   3  (=  1 + (3-1))
 *                  {0,1,2}:  15  (=  3 + (5-1)(4-1))
 *    return      = 39            (= 15 + (3-1)(5-1)(4-1))
 * ~~~
 *
 * @param[in,out] arMcr
 *    The ModeCombiOpRange to modify set addresses for.
 * @param[in] arNumModals
 *    The number N_m of modals per mode. Each ModeCombi will be assumed to hold
 *    a tensor with dimensions N_m - 1 for each mode m.
 * @return
 *    The total number of parameters to be contained with given
 *    ModeCombiOpRange and modals per mode.
 *    In other words, the address that would be assigned to a new ModeCombi
 *    appended to the range.
 ******************************************************************************/
Uin SetAddresses
   (  ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arNumModals
   )
{
   return SetAddressesImpl(arMcr, arNumModals);
}

Uin SetAddresses
   (  ModeCombiOpRange& arMcr
   ,  const std::vector<In>& arNumModals
   )
{
   return SetAddressesImpl(arMcr, arNumModals);
}
