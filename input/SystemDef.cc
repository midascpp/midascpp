/**
************************************************************************
* 
* @file                SystemDef.cc
*
* 
* Created:             06-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Setting system definition
*
* Last modified: March, 26th 2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
using std::string;
#include <map>
using std::map;
#include "util/Io.h"
#include "input/SystemDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/FindKeyword.h"


/* **********************************************************************
********************************************************************** */
void SystemDef::SetCalcModeType (const string& arStr)
{
   const map<string,CalcModeType> stringtoparam
   {
      {"NO",CalcModeType::NO}
      , {"CART",CalcModeType::CART}
      , {"LOCALTRANSROT",CalcModeType::LOCALTRANSROT}
   };

   CalcModeType input = midas::input::FindKeyword(stringtoparam, arStr);
 
   switch(input)
   {
      case CalcModeType::NO:
      {
         mCalcModeType = CalcModeType::NO;
         break;
      }
      case CalcModeType::CART:
      {
         mCalcModeType = CalcModeType::CART;
         break;
      }
      case CalcModeType::LOCALTRANSROT:
      {
         mCalcModeType = CalcModeType::LOCALTRANSROT;
         break;
      }
      default:
      {
         Mout << "Unknown CalcModeType scheme: " << arStr << endl;
         MIDASERROR("No known conversion to In for CalcModeType scheme");
      }
   }
}


/* **********************************************************************
********************************************************************** */
void SystemDef::SetFragmentType (const string& arStr)
{
   const map<string,FragmentType> stringtoparam
   {
      {"ATOM",FragmentType::ATOM},
      {"PREDEFINED",FragmentType::PREDEFINED},
   };

   FragmentType input = midas::input::FindKeyword(stringtoparam, arStr);
 
   switch(input)
   {
      case FragmentType::ATOM:
      {
         mFragmentType = FragmentType::ATOM;
         break;
      }
      case FragmentType::PREDEFINED:
      {
         mFragmentType = FragmentType::PREDEFINED;
         break;
      }
      default:
      {
         Mout << "Unknown FragmentType: " << arStr << endl;
         MIDASERROR("No known conversion to In for FragmentTypescheme");
      }
   }
}

/* **********************************************************************
********************************************************************** */
