/**
************************************************************************
* 
* @file                TotalRspFunc.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function framework
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
#include <vector>
#include <iostream> 
#include <map>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/TotalResponseContribution.h"

// using declarations
using std::string;
using std::vector;
using std::map;

TotalResponseContribution::TotalResponseContribution()
{
   mMap.clear();
}
 
Nb TotalResponseContribution::GetTotalValue() const
{
   // just add all values in the map
   Nb result=C_0;
   for(map<string,Nb>::const_iterator it=mMap.begin();it!=mMap.end();it++)
      result+=it->second;
   return result;
}

Nb TotalResponseContribution::GetTotalEqValue() const
{
   // just add all values in the map
   Nb result=C_0;
   for(map<string,Nb>::const_iterator it=mMap.begin();it!=mMap.end();it++)
      if(it->first=="EQ")
         result+=it->second;
   return result;
}

void TotalResponseContribution::PrintMap(ostream& os) const
{
   for(map<string,Nb>::const_iterator it=mMap.begin();it!=mMap.end();it++)
      os << it->first << " -> " << it->second << endl;
}

ostream& operator<<(ostream& os,const TotalResponseContribution& aC)
{
   os << "Map:" << endl;
   aC.PrintMap(os);
   return os;
}

