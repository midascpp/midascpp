/**
************************************************************************
* 
* @file                GroupCouplings.h
*
* Created:             14-01-2014
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Class for containing information of coupling of groups
* 
* Last modified:       02-06-2014
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GROUPCOUPLINGS_H
#define GROUPCOUPLINGS_H

#include <iostream>
#include<set>
using std::set;
#include<vector>
using std::vector;
#include<string>
using std::string;

#include"inc_gen/TypeDefs.h"
#include"util/InSet.h"
#include"input/ModeCombi.h"

/**
 *
 **/
class GroupCouplings
{
   private:
      //! Contains the groups of modes.
      vector<InSet>        mModeGroups;
      //! Contains the considered combinations of groups.
      vector<ModeCombi>    mGroupCombiRange;   
      //! Total number of modes involved
      In                   mNrModes;
      //! Highest Mode number 
      In                   mMaxModeNr; 
      //! Higest Level of GroupCombis 
      In                   mMaxGroupCombiLevel;    

   public:
      //! Default c-tor 
      GroupCouplings(); 

      //! Initializes GroupCouplings from input strings. 
      GroupCouplings(const vector<string>& arModeGroupsInStrings, const vector<string>& arGroupCombiInStrings); 
            
      //! Generates SubGroupCombiRange from string
      void AddSubGroupCombiRange(string aString); 
      
      //!
      In GetNrModes() const {return mNrModes;}
      
      //!
      In NrGroups() const {return mModeGroups.size();}
      
      //!
      In NrGroupCombis() const {return mGroupCombiRange.size();}
      
      //!
      In GetMaxGroupCombiLevel() const {return mMaxGroupCombiLevel;}
      
      //!
      vector<In> GroupCombi (const In& arGrpCombiNr) const {return mGroupCombiRange[arGrpCombiNr].MCVec();}
      
      //!
      vector<In> ModeGroup (const In& arModeGrpNr) const {return mModeGroups[arModeGrpNr].Vec();}
      
      //! creates a group combi range from an initial group combi
      void RestrictedRecurseAddGroupCombis 
         (  const vector<In>& arInitialGroupCombi
         ,  const vector<In>& arGroupList
         ,  const In& arMaxGroupCombiLevel
         ); 
      
      //! sorts, checks and adds group combination
      void AddGroupCombi(vector<In> arGroupCombi); 
};

#endif /* GROUPCOUPLINGS_H */
