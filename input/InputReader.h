#ifndef INPUTREADER_H_INCLUDED
#define INPUTREADER_H_INCLUDED

#include <string>
#include <iostream>
#include <functional>
#include <map>
#include <exception>

#include "input/IsKeyword.h"
#include "input/GetLine.h"
#include "util/conversions/FromString.h"

// Define some types.
using input_key_t = std::string;
using input_function_t = std::function<bool(std::istream&, std::string&)>;

/**
 * An input map, of keywords and functions to handle read-in of those keywords.
 * Currently it a std::map, with some extra functionality on top,
 * like saving a keyword level.
 **/
class input_map_t
   :  public std::map<input_key_t, input_function_t>
{
   private:
      //! Input level of map
      In mLevel = -1; 
   
   public:
      //! Construct map
      input_map_t(In aLevel)
         : mLevel(aLevel)
      {
      }
      
      //! Deleted copy ctor
      input_map_t(const input_map_t&) = delete; ///< we do not allow copy ctor
      
      //! Deleted move ctor
      input_map_t(input_map_t&&) = default; ///< we allow default move
      
      //! Delete copy operator=
      input_map_t& operator=(const input_map_t&) = delete; ///< we do not allow copy assign

      //! 
      input_map_t(In aLevel, std::map<input_key_t, input_function_t>&& aMap)
         :  std::map<input_key_t, input_function_t>(std::move(aMap))
         ,  mLevel(aLevel)
      {
      }

      //! Get input level of map
      In Level() const
      {
         return mLevel;
      }

      /**
       * @brief make keyword with level from string
       * @param str actual keyword
       **/
      std::string MakeKeyword(std::string&& str)
      {
         return "#" + std::to_string(Level()) + str;
      }
      

      //! Register function with map
      void RegisterFunction(const input_key_t& key, const input_function_t& f)
      {
         std::map<input_key_t, input_function_t>::insert(std::make_pair(key,f));
      }
};

/**
 * @class MidasInputException
 **/
class MidasInputException
   : public std::exception
{
   //! The line that caused the exception.
   std::string mLine;
   //! String to be returned when calling what().
   std::string mWhat;

   public:
      /**
       * @brief constructor
       *
       * @param aLine         Line being read.
       **/
      MidasInputException(const std::string& aLine)
         :  std::exception()
         ,  mLine(aLine)
         ,  mWhat("Input exception: " + mLine + "\n")
      {
      }

      /**
       * @brief print exception
       **/
      const char* what() const throw()
      {
         return mWhat.c_str();
      }
};

namespace midas
{
namespace input
{

/**
 * @brief read a string check it is not a keyword and return it
 * @param Minp input filestream
 * @param s always holds last read line
 * @param s_out output string
 **/
inline bool ReadString
   (  std::istream& Minp
   ,  std::string& s
   ,  std::string& s_out
   )
{
   midas::input::GetLine(Minp,s);
   if(IsKeyword(s)) throw MidasInputException(s);
//   if(IsKeyword(s)) throw MidasInputException(s,-1);
   s_out = s;
   return false;
}

/**
 * @brief bind an output string to a ReadString function
 **/
inline std::function<bool (std::istream&, std::string&)> BindReadString(std::string& s_out)
{
   return std::bind(ReadString, std::placeholders::_1, std::placeholders::_2, std::ref(s_out));
}

/**
 * @brief read until specified keyword
 **/
inline bool ReadStringUntilKeyword(std::istream& Minp
                                 , std::string& s
                                 , std::string& s_out
                                 , const input_key_t& end_key
                                 )
{
   while(midas::input::GetLine(Minp,s) && (end_key != midas::input::ParseInput(s)))
   {
      if(IsKeyword(s)) throw MidasInputException(s);
//      if(IsKeyword(s)) throw MidasInputException(s,-1);
      s_out += s + "\n";
   }
   return false;
}

/**
 * @brief
 **/
inline std::function<bool (std::istream&, std::string&)> BindReadStringUntilKeyword(std::string& s_out
                                                                                   , const input_key_t& end_key
                                                                                   )
{
   return std::bind(ReadStringUntilKeyword, std::placeholders::_1, std::placeholders::_2, std::ref(s_out), end_key);
}

/**
 * @brief read until specified keyword
 **/
inline bool ReadStringUntilAnyKeyword(std::istream& Minp
                                    , std::string& s
                                    , std::string& s_out
                                    )
{
   while(midas::input::GetLine(Minp,s) && !midas::input::IsKeyword(s))
   {
      s_out += s + "\n";
   }
   return true;
}

/**
 * @brief
 **/
inline std::function<bool (std::istream&, std::string&)> BindReadStringUntilAnyKeyword(std::string& s_out
                                                                                   )
{
   return std::bind(ReadStringUntilAnyKeyword, std::placeholders::_1, std::placeholders::_2, std::ref(s_out));
}


/**
 * @brief
 **/
template<class T>
inline bool SetType(std::istream& Minp, std::string& s, T& t_out, const T& t)
{
   t_out = t;
   return false;
}

/**
 * @brief
 **/
template<class T>
inline std::function<bool (std::istream&, std::string&)> BindSetType(T& t_out, const T& t)
{
   return std::bind(SetType<T>, std::placeholders::_1, std::placeholders::_2, std::ref(t_out), std::ref(t));
}

template<class T>
inline std::function<bool (std::istream&, std::string&)> BindSetType(T& t_out, T&& t)
{
   return std::bind(SetType<T>, std::placeholders::_1, std::placeholders::_2, std::ref(t_out), t);
}

/***************************************************************************//**
 * @brief
 *    Reads string from line, then converts to type T and stores in ref. arg.
 *
 * @param[in,out] Minp
 *    Input stream from which to read.
 * @param[out] s
 *    Stores contents of the newly read line.
 * @param[out] t_out
 *    Stores contents of line after conversion to type T.
 * @return
 *    Whether having already read the next line (but without processing it).
 ******************************************************************************/
template<class T>
inline bool SetTypeFromReadString
   (  std::istream& Minp
   ,  std::string& s
   ,  T& t_out
   )
{
   std::string s_out;
   bool return_val = ReadString(Minp, s, s_out);
   t_out = midas::util::FromString<T>(s_out);
   return return_val;
}

/***************************************************************************//**
 * @brief
 *    Binds a function that reads next line, converts to type T and stores in
 *    the specific given argument.
 *
 * @param[out] t_out
 *    Reference to the variable where the value of the line is stored when the
 *    returned function is called.
 * @return
 *    The function for use with InputReader, that when called reads a line from
 *    a std::istream&, saves the line in a std::string&, and stores a value in
 *    the t_out variable.
 ******************************************************************************/
template<class T>
inline std::function<bool (std::istream&, std::string&)> BindSetTypeFromReadString
   (  T& t_out
   )
{
   return std::bind
      (  SetTypeFromReadString<T>
      ,  std::placeholders::_1
      ,  std::placeholders::_2
      ,  std::ref(t_out)
      );
}

} /* namespace input */
} /* namespace midas */

/**
 * @class CalcDef
 **/
class CalcDef
{
   private:
      //! Input level
      int mLevel = -1; 
   
   protected:
      /**
       * @brief ctor (protected so we cannot have a bare CalcDef)
       * @param aLevel input level of calcdef
       **/
      CalcDef(int aLevel)
         : mLevel(aLevel)
      {
      }
   public:
      /**
       * @brief get input level of calcdef
       **/
      In Level() const { return mLevel; }
};

/**
 * @brief Input reader function
 **/
bool InputReader(std::istream& Minp, std::string& s, const input_map_t& imap);

#endif /* INPUTREADER_H_INCLUDED */
