#include "ResponseFunction.h"

#include "util/Error.h"

/**
 *
 **/
std::set<std::string> UniqueOperators(const std::vector<ResponseFunction>& rsp_funcs)
{
   std::set<std::string> unique_operators;

   for(In i_rsp=I_0; i_rsp<rsp_funcs.size(); ++i_rsp)
   {
      for(In i_oper=I_0; i_oper<rsp_funcs[i_rsp].Operators().size(); ++i_oper)
      {
         unique_operators.insert(rsp_funcs[i_rsp].Operators()[i_oper]);
      }
   }

   return unique_operators;
}

/**
 *
 **/
std::string ResponseFunction::Type() const
{
   std::string type;
   
   switch(mOperators.size())
   {
      case 0:
      {
         break;
      }
      case 1:
      {
         type += "<" + mOperators[0] + ">";
         break;
      }
      default:
      {
         type += "<<" + mOperators[0] + ";";
         for(size_t i=1; i<mOperators.size()-1;++i)
         {
            type += mOperators[i] + ",";
         }
         if(mOperators.size() > 1)
         {
            type += mOperators[mOperators.size()-1];
         }
         type += ">>";
         break;
      }
   }

   return type;
}

/**
 *
 **/
std::string ResponseFunction::TypeForFile() const
{
   std::string type;
   if(mOperators.size() > 0)
   {
      type += mOperators[0];
      for(int i = 1; i < mOperators.size(); ++i)
      {
         type += "-" + mOperators[i];
      }
   }
   return type;
}

/**
 *
 **/
bool ResponseFunction::HasOperator(const std::string& oper) const
{
   for(auto& op: mOperators)
   { // if we find operator we return 'true'
      if(oper == op) return true;
   }
   return false;
}

/**
 *
 **/
bool ResponseFunction::HasOperator(const std::vector<std::string>& opers) const
{
   for(auto& op: opers)
   { // if we dont find operator we return 'false'
      if(!HasOperator(op)) return false;
   }
   return true; // we found all operators in opers
}

/**
 *
 **/
std::ostream& operator<<(std::ostream& os, const ResponseFunction& rsp)
{
   os << rsp.Type();
   return os;
}

/**
 *
 **/
inline std::string RemoveBracketImpl(std::string str, const char front, const char back)
{
   if(str.front()!=front)
   {
      MIDASERROR("Front bracket is wrong");
   }
   if(str.back()!=back)
   {
      MIDASERROR("Back bracket is wrong");
   }

   return str.substr(1,str.size()-2);
}

/**
 *
 **/
inline std::string RemoveAngleBracket(std::string str, int num_brackets)
{
   for(int i=0; i<num_brackets; ++i)
   {
      str = RemoveBracketImpl(str,'<','>');
   }
   return str;
}

/**
 *
 **/
bool StringCheckFront(const std::string& str, std::string valid)
{
   auto size = valid.size();
   return str.substr(0,size) == valid;
}

/**
 *
 **/
void StringEraseFront(std::string& str, int i)
{
   str.erase(0,i);
}

/**
 *
 **/
void ValidateResult(const std::vector<std::string>& result)
{
   if(result.size() < 1)
   {
      MIDASERROR("TOAST");
   }
}

/**
 *
 **/
void StringReadError(const std::string& str, const std::string& message)
{
   MIDASERROR("Response function parser failed with:",message,"for string: "+str);
}

/**
 *
 **/
std::vector<std::string> ParseResponseFunctionString(std::string str)
{
   std::vector<std::string> result;
   bool end_of_input = false;

   while(!str.empty() && !end_of_input)
   {
      switch(str.front()) 
      {
         case '\\':
         {
            ValidateResult(result);
            result.back().push_back(str.at(1));
            StringEraseFront(str,2);
            break;
         }
         case '<': // start readin
         {
            if(StringCheckFront(str,"<<"))
            {
               StringEraseFront(str,2);
            }
            else if(StringCheckFront(str,"<"))
            {
               StringEraseFront(str,1);
            }
            else
            {
               StringReadError(str,"response function is not bracketed by '<<' or '<'");
            }
            result.emplace_back();
            break;
         }  
         case '>': // end readin
         { 
            if(StringCheckFront(str,">>"))
            {
               StringEraseFront(str,2);
            }
            else if(StringCheckFront(str,">"))
            {
               StringEraseFront(str,1);
            }
            else
            {
               StringReadError(str,"response function is not bracketed by '>>' or '>'");
            }
            end_of_input = true;
            break;
         }
         case ';':
         {
            result.emplace_back();
            StringEraseFront(str,1);
            break;
         }
         case ',':
         {
            result.emplace_back();
            StringEraseFront(str,1);
            break;
         }
         default:
         {
            ValidateResult(result);
            result.back().push_back(str.at(0));
            StringEraseFront(str,1);
         }
      }  
   }

   return result;
}

/**
 *
 **/
ResponseFunction CreateResponseFunctionFromString(const std::string& str)
{
   return {ParseResponseFunctionString(str)};
}
