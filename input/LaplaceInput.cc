/**
************************************************************************
* 
* @file                LaplaceInput.cc
*
* 
* Created:             17-03-2012
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Input reader for Laplace Quadrature
* 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/GeneralInputParser.h"
#include "input/FindKeyword.h"
#include "util/FindInMap.h"
#include "input/GetLine.h"
#include "input/TensorDecompInput.h"
#include "input/LaplaceInput.h"
#include "input/KeywordMap.h"
#include "util/conversions/FromString.h"

#include "tensor/LaplaceInfo.h"

namespace midas
{
namespace util
{

namespace detail
{
///> deduce_key_type for LaplaceInfo
template<>
struct deduce_key_type<midas::tensor::LaplaceInfo>
{
   using type = typename midas::tensor::LaplaceInfo::key_type;
};

} /* namespace detail */

} /* namespace util */
} /* namespace midas */

using namespace midas;

namespace detail
{

/**
 * Get map of keywords for each Laplace type
 *
 * @return Returns the map of needed/allowed keywords with default values for unset keywords.
 **/
const keyword_map& GetLaplaceKeywordMap
   (
   )
{
   static keyword_map laplace_keyword_map =
   {
      // BESTLAP default
      {  {"BESTLAP"},   {  {"LAPCONV", static_cast<Nb>(1.e-4)}
                        ,  {"HISTOGRAMDELTA", static_cast<Nb>(1.e-1)}
                        ,  {"MAXPOINTS", 20}
                        ,  {"MINPOINTS", 2}
                        ,  {"COMPLEXQUADRATURE", false}
                        ,  {"COMPLEXSHIFT", static_cast<Nb>(1.e-5)}
                        ,  {"DECOMPOSECOMPLEX", true} // Niels: Complex Laplace does not work yet!
                        ,  {"USELIBGUESS", true}
                        }
      }

      // FIXLAP default
   ,  {  {"FIXLAP"},    {  {"NUMPOINTS", 4}
                        ,  {"HISTOGRAMDELTA", static_cast<Nb>(1.e-1)}
                        ,  {"COMPLEXQUADRATURE", false}
                        ,  {"COMPLEXSHIFT", static_cast<Nb>(1.e-5)}
                        ,  {"DECOMPOSECOMPLEX", true} // Niels: Complex Laplace does not work yet!
                        ,  {"USELIBGUESS", true}
                        }
      }

      // NOOPT default
   ,  {  {"NOOPT"},     {  {"NUMPOINTS", 4}
                        ,  {"NOOPTALLOWMOREPOINTS", true}
                        ,  {"COMPLEXQUADRATURE", false}
                        ,  {"COMPLEXSHIFT", static_cast<Nb>(1.e-5)}
                        ,  {"DECOMPOSECOMPLEX", true} // Niels: Complex Laplace does not work yet!
                        ,  {"USELIBGUESS", true}
                        ,  {"HISTOGRAMDELTA", static_cast<Nb>(1.e-1)}
                        }
      }
   };

   return laplace_keyword_map;
}

/**
 * Validate input
 *
 * @param info    LaplaceInfo to be validated
 **/
void ValidateLaplaceInput
   (  midas::tensor::LaplaceInfo& info
   )
{
   auto& type = util::FindElseError("TYPE", info, "Did not find 'TYPE' in LaplaceInfo after input.").second.template get<std::string>();

   auto& laplace_keyword_map = GetLaplaceKeywordMap();

   auto& allowed_keywords =  util::FindElseError
                                (  type
                                ,  laplace_keyword_map
                                ,  "TYPE: '" + std::string(type) + "' not found in laplace_keyword_map."
                                ).second;

   // check that all set keywords are allowed for the currectly set TYPE
   for(auto& elem : info)
   {
      if (  elem.first == "TYPE"
         || elem.first == "IOLEVEL"
         || elem.first == "NAME"
         )
      {
         continue;
      }

      auto iter = allowed_keywords.find(elem);

      if (  iter == allowed_keywords.end()
         )
      {
         MIDASERROR("Keyword: '" + elem.first + "' is not allowed for type: '" + std::string(type) + "'.");
      }
   }

   // set default values
   for(auto& elem : allowed_keywords)
   {
      auto iter = info.find(elem.first);
      if(iter == info.end())
      {
         info[elem.first] = elem.second;
      }
   }

   // set other defaults
   {
      auto iter = info.find("IOLEVEL");
      if (  iter == info.end()   )
      {
         info["IOLEVEL"] = I_1;  // IoLevel is 1 as default (lowest value)
      }
   }
}

} /* namespace detail */


/**
* Read and check Laplace input.
* Read until next s = "#i..." i<aInputLevel which is returned.
* */
bool LaplaceInput
   (  istream& Minp
   ,  std::string& s
   ,  In aInputLevel
   ,  midas::tensor::LaplaceInfo::Set& laplaceinfo_set
   )
{
   In input_level = aInputLevel;

   midas::tensor::LaplaceInfo laplaceinfo;

   // Enum/map with input flags
   enum INPUT { ERROR, TYPE, NUMPOINTS, MAXPOINTS, MINPOINTS, LAPCONV, NAME, HISTOGRAMDELTA, IOLEVEL, NOOPTFIXNUMPOINTS, COMPLEXQUADRATURE, COMPLEXSHIFT, DECOMPOSECOMPLEX, NOLIBGUESS };

   const map<string,INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"TYPE",TYPE}
   ,  {"#"+std::to_string(input_level)+"NUMPOINTS",NUMPOINTS}
   ,  {"#"+std::to_string(input_level)+"LAPCONV",LAPCONV}
   ,  {"#"+std::to_string(input_level)+"HISTOGRAMDELTA", HISTOGRAMDELTA}
   ,  {"#"+std::to_string(input_level)+"NAME",NAME}
   ,  {"#"+std::to_string(input_level)+"IOLEVEL", IOLEVEL}
   ,  {"#"+std::to_string(input_level)+"MAXPOINTS", MAXPOINTS}
   ,  {"#"+std::to_string(input_level)+"MINPOINTS", MINPOINTS}
   ,  {"#"+std::to_string(input_level)+"NOOPTFIXNUMPOINTS", NOOPTFIXNUMPOINTS}
   ,  {"#"+std::to_string(input_level)+"COMPLEXQUADRATURE", COMPLEXQUADRATURE}
   ,  {"#"+std::to_string(input_level)+"COMPLEXSHIFT", COMPLEXSHIFT}
   ,  {"#"+std::to_string(input_level)+"DECOMPOSECOMPLEX", DECOMPOSECOMPLEX}
   ,  {"#"+std::to_string(input_level)+"NOLIBGUESS", NOLIBGUESS}
   };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s,input_level)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case TYPE:
         { 
            midas::input::GetLine(Minp,s);
            laplaceinfo["TYPE"] = s;
            break;
         }
         case IOLEVEL:
         {
            laplaceinfo["IOLEVEL"] = midas::input::GetIn(Minp, s);
            break;
         }
         case NUMPOINTS:
         {
            laplaceinfo["NUMPOINTS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case LAPCONV:
         {
            laplaceinfo["LAPCONV"] = midas::input::GetNb(Minp, s);
            break;
         }
         case HISTOGRAMDELTA:
         {
            laplaceinfo["HISTOGRAMDELTA"] = midas::input::GetNb(Minp, s);
            break;
         }
         case MAXPOINTS:
         {
            laplaceinfo["MAXPOINTS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case MINPOINTS:
         {
            laplaceinfo["MINPOINTS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case NOOPTFIXNUMPOINTS:
         {
            laplaceinfo["NOOPTALLOWMOREPOINTS"] = false;
            break;
         }
         case NAME:
         {  
            midas::input::GetLine(Minp,s);
            laplaceinfo["NAME"] = s;
            break;
         }
         case COMPLEXQUADRATURE:
         {
            laplaceinfo["COMPLEXQUADRATURE"] = true;
            break;
         }
         case COMPLEXSHIFT:
         {
            laplaceinfo["COMPLEXSHIFT"] = midas::input::GetNb(Minp, s);
            break;
         }
         case DECOMPOSECOMPLEX:
         {
            laplaceinfo["DECOMPOSECOMPLEX"] = true;
            break;
         }
         case NOLIBGUESS:
         {
            laplaceinfo["USELIBGUESS"] = false;
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a Laplace level " << aInputLevel << " Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // if we read any input, we validate it and push back into set
   if(laplaceinfo.size() > 0)
   {
      // validate input and set default values.
      detail::ValidateLaplaceInput(laplaceinfo);
 
      // add decomp info to set of decomp info's
      laplaceinfo_set.emplace(laplaceinfo);
   }

   // we have read, but not processed next keyword
   return true;
}
