/**
************************************************************************
* 
* @file                InputMap.h
*
* 
* Created:             06-08-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Map of string to any_type
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INPUTMAP_H_INCLUDED
#define INPUTMAP_H_INCLUDED

#include <string>
#include <map>
#include "libmda/util/any_type.h"

namespace midas::input
{

/**
 *
 **/
class InputMap
   :  private std::map<std::string, libmda::util::any_type>
{
   public:
      using Base = std::map<std::string, libmda::util::any_type>;

      using key_type = typename Base::key_type;
      using mapped_type = typename Base::mapped_type;
      using iterator = typename Base::iterator;
      using const_iterator = typename Base::const_iterator;

   public:
      //! At function (const)
      template
         <  typename T
         >
      const T& At
         (  const key_type& aKey
         )  const
      {
         try
         {
            return this->at(aKey).template get<T>();
         }
         catch (  const std::out_of_range&
               )
         {
            MIDASERROR("InputMap: Could not find '" + aKey + "'.");
            return this->at(aKey).template get<T>();
         }
      }

      //! At function (non-const)
      template
         <  typename T
         >
      T& At
         (  const key_type& aKey
         )
      {
         try
         {
            return this->at(aKey).template get<T>();
         }
         catch (  const std::out_of_range&
               )
         {
            MIDASERROR("InputMap: Could not find '" + aKey + "'.");
            return this->at(aKey).template get<T>();
         }
      }

      //! Operator []
      mapped_type& operator[]
         (  const key_type& aKey
         )
      {
         return Base::operator[](aKey);
      }

      //! Operator [] (move)
      mapped_type& operator[]
         (  key_type&& aKey
         )
      {
         return Base::operator[](aKey);
      }

      //! Find function
      iterator Find
         (  const key_type& aKey
         )
      {
         return this->find(aKey);
      }

      //! Contains
      bool Contains
         (  const key_type& aKey
         )  const
      {
         return this->find(aKey) != this->end();
      }

      // Find function (const)
      const_iterator Find
         (  const key_type& aKey
         )  const
      {
         return this->find(aKey);
      }

      //! Empty
      bool Empty
         (
         )  const noexcept
      {
         return this->empty();
      }

      /** @name Begin and end interface **/
      //!@{
      iterator begin
         (
         )
      {
         return Base::begin();
      }

      const_iterator begin
         (
         )  const
      {
         return Base::begin();
      }

      iterator end
         (
         )
      {
         return Base::end();
      }

      const_iterator end
         (
         )  const
      {
         return Base::end();
      }
      //!@}

};

} /* namespace midas::input */


#endif /* INPUTMAP_H_INCLUDED */
