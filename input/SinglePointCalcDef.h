#ifndef SINGLEPOINTCALCDEF_H_INCLUDED
#define SINGLEPOINTCALCDEF_H_INCLUDED

#include <functional>
#include <map>
#include <exception>
#include <ostream>

#include "inc_gen/TypeDefs.h"
#include "input/IsKeyword.h"
#include "input/GetLine.h"
#include "input/InputReader.h"
#include "util/stream/HeaderStream.h"
#include "pes/singlepoint/SinglePointInfo.h"

/**
 * @class SinglePointCalcDef
 **/
class SinglePointCalcDef
   : public CalcDef
{
   private:

      //! Name of single point calcdef
      std::string mName = "DEFAULT_SP_CALCDEF";

      //! Singlepoint info with a few defaults values
      SinglePointInfo mSpInfo = SinglePointInfo{ { "PROGRAM"        , "SP_ERROR"         }
                                               , { "ESSYM"          , "TRUE"             }
                                               , { "ROTATIONTHR"    , "1e-5"             } 
                                               , { "BOHR"           , "FALSE"            } 
                                               , { "SPSCRATCHDIR"   , ""                 } // This will always be overwritten by the PES module (see CalculationList.cc) but not by the SYSTEM module
                                               , { "PROPERTYINFO"   , "midasifc.propinfo"}
                                               };

   public:
      /**
       * @brief ctor
       **/
      SinglePointCalcDef(int aLevel = 2);
      
      /**
       * @brief get name
       **/
      std::string Name() const { return mName; }
      
      /**
       * @brief get singlepoint info
       **/
      const SinglePointInfo& SpInfo() const { return mSpInfo; }
       
      /**
       * @brief create map of singlepoint keywords
       **/
      input_map_t Map();

      /**
       * @brief Output operator for SinglePointCalcDef
       **/
      friend std::ostream& operator<<(std::ostream&, const SinglePointCalcDef&);

      /**
       *
       **/
      friend void AfterProcessSinglePointInput();
};

/**
 * @brief operator<< overload for ostream
 **/
inline std::ostream& operator<<
   (  std::ostream& os
   ,  const SinglePointCalcDef& aCalcDef
   )
{
   os << std::left
      << std::setw(10) << "LEVEL"  << " : " << aCalcDef.Level() << "\n"
      << std::setw(10) << "NAME"   << " : " << aCalcDef.Name() << "\n"
      << std::setw(10) << "SPINFO" << " :\n"; 
   
   midas::stream::HeaderStream hos(os,"     ",true);
   hos << aCalcDef.SpInfo() << "\n";
      
   os << std::flush;
   return os;
}

//
void AfterProcessSinglePointInput();

#endif /* SINGLEPOINTCALCDEF_H_INCLUDED */
