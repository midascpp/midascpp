/**
************************************************************************
* 
* @file                Input.cc
*
* 
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the midas program.
* 
* Last modified: Fri Apr 27, 2007  08:59AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
using std::vector;
#include <map>
using std::map;
#include <algorithm>
using std::transform;
using std::sort;
using std::unique;
using std::find;
#include <string>
using std::string;

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "test/Test.h"
#include "util/Io.h"
#include "nuclei/Nuclei.h"
#include "input/Input.h"
#include "input/OneModeBasDef.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/RspCalcDef.h"
#include "input/ProgramSettings.h"
#include "util/MultiIndex.h"
#include "input/GlobalOperatorDefinitions.h"

namespace detail
{

/**
 * Copy important data from VscfCalcDef to VccCalcDef
 *
 * @param aVscf            The VscfCalcDef
 * @param arVcc            The VccCalcDef
 * @param aCopyOper        Copy operator?
 * @param aCopyBasis       Copy basis?
 **/
void CopyVscfInputToVccCalcDef
   (  const VscfCalcDef& aVscf
   ,  VccCalcDef& arVcc
   ,  bool aCopyOper = false
   ,  bool aCopyBasis = false
   )
{
   Mout  << " Copy input from VSCF calculation '" << aVscf.GetName() << "' to Vcc calculation '" << arVcc.GetName() << "'." << std::endl;

   // Copy operator
   if (  aCopyOper
      )
   {
      Mout  << "   - Copy operator" << std::endl;
      arVcc.SetOper(aVscf.Oper());
   }

   // Copy basis set
   if (  aCopyBasis
      )
   {
      Mout  << "   - Copy basis" << std::endl;
      arVcc.SetBasis(aVscf.Basis());
   }

   // Copy occvec
   Mout  << "   - Copy occupation vector" << std::endl;
   arVcc.SetOccVec(aVscf.OccupVec());

   // Copy convergence thresholds
   Mout  << "   - Copy convergence thresholds" << std::endl;
   arVcc.SetResidThr(aVscf.GetResidThr());
   arVcc.SetEnerThr(aVscf.GetEnerThr());

   // Copy max iter
   Mout  << "   - Copy maximum iterations" << std::endl;
   arVcc.SetMaxIter(aVscf.GetMaxIter());

   // Copy screening
   Mout  << "   - Copy screening thresholds" << std::endl;
   arVcc.SetScreenZero(aVscf.ScreenZero());
   arVcc.SetScreenZeroCoef(aVscf.ScreenZeroCoef());
}

/**
 * Set VSCF-related input in VccCalcDef.
 * Add multiple VCC calculations if necessary.
 * If only one VscfCalcDef is found, this will be used in VCC.
 * If no or multiple VscfCalcDef%s are provided, VCC will run on top of a default VSCF calculation.
 *
 * @param arCalcDef     The VccCalcDef to modify.
 **/
void SetupVscfForVcc
   (  VccCalcDef& arCalcDef
   )
{
   bool use_all = arCalcDef.UseAllVscf();
   const auto& use_vscf = arCalcDef.VscfReference();
   const auto& vccname = arCalcDef.GetName();
   const auto& vccoper = arCalcDef.Oper();
   const auto& vccbasis = arCalcDef.Basis();
   bool oper_set = (!vccoper.empty() && gOperatorDefs.GetNrOfOpers() > 1); // if only 1 oper, vccoper is automatically set in VccInput
   bool basis_set = !vccbasis.empty();
   bool oper_or_basis_set = oper_set || basis_set;

   // If use all
   if (  use_all
      )
   {
      if (  gVscfCalcDef.empty()
         )
      {
         MidasWarning("UseAllVscf set in Vcc, but no VSCF input is given. Use default setup!");
      }
      else
      {
         bool first_vscf = true;
         size_t label = 0;
         for(const auto& vscf : gVscfCalcDef)
         {
            // If operator or basis has been specified in VCC, only use VSCFs which match
            if (  (  !oper_set
                  || gOperatorDefs.GetNrOfOpers() == I_1
                  || vccoper == vscf.Oper()
                  )
               && (  !basis_set
                  || vccbasis == vscf.Basis()
                  )
               )
            {
               // For the first VSCF, modify the current VccCalcDef
               if (  first_vscf
                  )
               {
                  CopyVscfInputToVccCalcDef(vscf, arCalcDef, !oper_set, !basis_set);
                  first_vscf = false;
               }
               // For the next, add new VccCalcDef and modify that
               else
               {
                  // Find basename for VCC
                  size_t method_suffix_begin = vccname.find("_EXC_");
                  std::string vcc_method_suffix = vccname.substr(method_suffix_begin);
                  std::string vcc_basename = arCalcDef.Prefix();

                  // Name for new calcdef
                  std::string newname = vcc_basename + "_" + std::to_string(label) + vcc_method_suffix;

                  // Add new VccCalcDef
                  auto new_calcdef = arCalcDef;
                  new_calcdef.SetName(newname);
                  CopyVscfInputToVccCalcDef(vscf, new_calcdef, !oper_set, !basis_set);
                  gVccCalcDef.emplace_back(std::move(new_calcdef));
               }

               // Increment label
               ++label;
            }
         }

         if (  label == 0
            )
         {
            MIDASERROR("UseAllVscf found no VSCF inputs that could be used for VCC...");
         }
      }
   }
   // If use one specific reference
   else if  (  !use_vscf.empty()
            )
   {
      bool vscf_set = false;
      bool first_vscf = true;
      size_t label = 0;
      Uin n_dump_for_matrep = 0;

      // Find all calculations with same prefix
      for(const auto& vscf : gVscfCalcDef)
      {
         // The user specifies only the VSCF prefix (the one set on input) and get the all calculations constructed by the Vscf input block.
         if (  vscf.Prefix() == use_vscf
            )
         {
            // Sanity check
            MidasAssert(!oper_or_basis_set || (vscf.Oper() == arCalcDef.Oper() && vscf.Basis() == arCalcDef.Basis()), "Different operator or basis set in VCC and VSCF!");

            // We have a match
            vscf_set = true;

            // For the first VSCF, modify the current VccCalcDef
            if (  first_vscf
               )
            {
               CopyVscfInputToVccCalcDef(vscf, arCalcDef, !oper_set, !basis_set);
               first_vscf = false;
            }
            // For the next, add new VccCalcDef and modify that
            else
            {
               // Find basename for VCC
               size_t method_suffix_begin = vccname.find("_EXC_");
               std::string vcc_method_suffix = vccname.substr(method_suffix_begin);
               std::string vcc_basename = arCalcDef.Prefix();

               // Name for new calcdef
               std::string newname = vcc_basename + "_" + std::to_string(label) + vcc_method_suffix;

               // Add new VccCalcDef
               auto new_calcdef = arCalcDef;
               new_calcdef.SetName(newname);
               CopyVscfInputToVccCalcDef(vscf, new_calcdef, !oper_set, !basis_set);
               gVccCalcDef.emplace_back(std::move(new_calcdef));
            }

            // Increment label
            ++label;

            // Increment num. DumpModalsForMatRep().
            if (vscf.DumpModalsForMatRep())
            {
               ++n_dump_for_matrep;
            }
         }
      }

      MidasAssert(vscf_set, "VSCF calculation with name '" + use_vscf + "' which should be used as reference for Vcc '" + vccname + "' not found! Make sure to use same operator and basis!");

      MidasAssert(n_dump_for_matrep <= 1, "Detected multiple VSCF calculations with name '"+use_vscf+"' and DumpModalsForMatRep enabled.\nThis is currently not tested. Aborting.");
   }
   // If only one VSCF is defined, use that!
   else if  (  gVscfCalcDef.size() == I_1
            )
   {
      if (  !arCalcDef.VscfInputInVcc()
         )
      {
         const auto& vscf = gVscfCalcDef[0];
         if (  (  !oper_set
               || gOperatorDefs.GetNrOfOpers() == I_1
               || vccoper == vscf.Oper()
               )
            && (  !basis_set
               || vccbasis == vscf.Basis()
               )
            )
         {
            Mout  << " Only one VSCF calculation has been defined and no VSCF-related keywords are given in Vcc input block. I will use that as reference for Vcc: '" << vccname << "'." << std::endl;
            CopyVscfInputToVccCalcDef(vscf, arCalcDef, !oper_set, !basis_set);
         }
      }
   }
   // If no VSCFs have been defined, use default.
   else if  (  gVscfCalcDef.empty()
            )
   {
      Mout  << " No VSCF reference set for Vcc: '" << vccname << "' and no VSCF calculations with same operator and basis have been defined. I will use default reference state in Vcc!" << std::endl;
   }
   // If multiple VSCFs have been defined and VCC does not know which one to use, issue warning and use default setup!
   else
   {
      MidasWarning("No VSCF reference set for VCC: '" + vccname + "' and multiple VSCF calculations have been defined. I will use default reference state in Vcc!");
   }
}

} /* namespace detail */

/**
* Adjustments due to dependencies - Comment well here!!! 
* */
void InputAdjustments()
{
   /**
    * Adjust some global stuff 
    **/
   gIoLevel  = midas::input::gProgramSettings.GetGeneralSettings().GetIoLevel();
   gTechInfo = midas::input::gProgramSettings.GetGeneralSettings().GetNumericLimits();
   gDebug    = midas::input::gProgramSettings.GetGeneralSettings().GetDebug();
   
   /**
    * If the TechnicalInformation keyword is specified on input, 
    * then Midas will write information on the size of Nb, In and STL containers
   **/
   if (gTechInfo)
   {
      Out72Char(Mout,'+','-','+');
      string st = " Info on Numeric type Nb   ";
      OneArgOut72(Mout,st,'|');
      Out72Char(Mout,'+','-','+');
      st = " Size of Nb  (bytes)            ";
      TwoArgOut72(Mout,st,C_NB_SIZE_OFF);
      st = " Largest Nb                     ";
      TwoArgOut72(Mout,st,C_NB_MAX);
      st = " Smallest Nb                    ";
      TwoArgOut72(Mout,st,C_NB_MIN);
      st = " Largest  Nb exp (decimal)      ";
      In nexp = std::numeric_limits<Nb>::max_exponent10;
      TwoArgOut72(Mout,st,nexp);
      st = " Smallest Nb exp (decimal)      ";
      nexp = std::numeric_limits<Nb>::min_exponent10;
      TwoArgOut72(Mout,st,nexp);
      st = " Nb mantissa #digits (binary)";
      In ndig = std::numeric_limits<Nb>::digits;
      TwoArgOut72(Mout,st,ndig);
      st = " Nb mantissa #digits (decimal)";
      ndig = std::numeric_limits<Nb>::digits10;
      TwoArgOut72(Mout,st,ndig);
      st = " Nb infinity                    ";
      TwoArgOut72(Mout,st,std::numeric_limits<Nb>::infinity());
      st = " Nb epsilon                     ";
      TwoArgOut72(Mout,st,std::numeric_limits<Nb>::epsilon());
      st = " Nb round error                 ";
      TwoArgOut72(Mout,st,std::numeric_limits<Nb>::round_error());
      
      Out72Char(Mout,'+','-','+');
      st = " Info on Integer type In   ";
      OneArgOut72(Mout,st,'|');
      Out72Char(Mout,'+','-','+');
      st = " Size of In (bytes)             ";
      TwoArgOut72(Mout,st,C_IN_SIZE_OFF);
      st = " Largest In                     ";
      TwoArgOut72(Mout,st,C_IN_MAX);
      st = " Smallest In                    ";
      TwoArgOut72(Mout,st,C_IN_MIN);
      Out72Char(Mout,'+','-','+');
      st = " Info on max_size of STL containers ";
      OneArgOut72(Mout,st,'|');
      Out72Char(Mout,'+','-','+');
      st = " Vector In max_size       ";
      vector<In> v1;
      size_t maxin = v1.max_size();
      TwoArgOut72(Mout,st,maxin);
      st = " Vector Nb max_size       ";
      vector<Nb> v2;
      size_t maxnb = v2.max_size();
      TwoArgOut72(Mout,st,maxnb);
      st = " Set In max_size       ";
      set<In> set1;
      maxin = set1.max_size();
      TwoArgOut72(Mout,st,maxin);
      st = " Set Nb max_size       ";
      set<Nb> set2;
      maxnb = set2.max_size();
      TwoArgOut72(Mout,st,maxnb);
      st = " map In,In max_size       ";
      map<In,In> m1;
      maxin = m1.max_size();
      TwoArgOut72(Mout,st,maxin);
      st = " map Nb,Nb max_size       ";
      map<Nb,Nb> m2;
      maxnb = m2.max_size();
      TwoArgOut72(Mout,st,maxnb);
      Out72Char(Mout,'+','-','+');
      Mout << std::endl;
   }

   /**
    * The global IoLevel overules the local prinout level if is larger
   **/
   if (gIoLevel > gTestIoLevel) 
   {
      gTestIoLevel = gIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Test Io Level reset to global value: " << gIoLevel << '\n';
      }
   }
   if (gIoLevel > gPesIoLevel) 
   {
      gPesIoLevel = gIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Pes Io Level reset to global value: " << gIoLevel << '\n';
      }
   }
   if (gPesIoLevel > gFitBasisIoLevel)
   {
      gFitBasisIoLevel = gPesIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " FitBasis IoLevel reset to Pes IoLevel value: " << gPesIoLevel << '\n';
      }
   }
   if (gIoLevel > gAnalysisIoLevel) 
   {
      gAnalysisIoLevel = gIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Analysis Io Level reset to global value: " << gIoLevel << '\n';
      }
   }
   if (gIoLevel > gVibIoLevel) 
   {
      gVibIoLevel = gIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Vib Io Level reset to global value: " << gIoLevel << '\n';
      }
   }
   if (gVibIoLevel > gOperIoLevel) 
   {
      gOperIoLevel = gVibIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Oper Io Level reset to vib io value: " << gOperIoLevel<< '\n';
      }
   }
   if (gVibIoLevel > gBasisIoLevel) 
   {
      gBasisIoLevel = gVibIoLevel;
      if (gIoLevel > I_8)
      {
         Mout << " Basis Io Level reset to vib io value: " << gBasisIoLevel<< '\n';
      }
   }
   if (gDoVcc && !gDoVscf)
   {
      gDoVscf     = gDoVcc;  
      if (gIoLevel > I_8)
      {
         Mout << " Vcc requires Vscf do be done, Vscf:  " << gDoVscf << '\n';
      }
   }

   /**
    * The global IoLevel overrules the local printout level if is larger
   **/
   for (In i_calc =0;i_calc<gVscfCalcDef.size();i_calc++)
   {
      string basis= gVscfCalcDef[i_calc].Basis();
      string oper = gVscfCalcDef[i_calc].Oper();

      // Set default oper if nothing is given in input
      if (oper.size()==0) 
      {
         if (gOperatorDefs.GetNrOfOpers()==1) 
         {
            // Set operator
            gVscfCalcDef[i_calc].SetOper(gOperatorDefs[0].Name());

            // Update oper variable
            oper = gVscfCalcDef[i_calc].Oper();
         }
         else
         {
            Mout << " No operator given in Vscf Input " << endl;
            if (gOperatorDefs.GetNrOfOpers()>1) Mout 
               << " Do not know which operator to choose among " 
               << gOperatorDefs.GetNrOfOpers() << endl;
            MIDASERROR("No operators given in vscf input" );
         }
      }
      // Check operator is available
      oper = gVscfCalcDef[i_calc].Oper();
      OpDef& opdef = gOperatorDefs.GetOperator(oper);

      // Set default basis if noting is given in input
      if (basis.size()==0) 
      {
         if (gBasis.size()==1) 
         {
            gVscfCalcDef[i_calc].SetBasis(gBasis[0].GetmBasName());
         }
         else if  (  opdef.StateTransferForAll()
                  )
         {
            Mout  << " Using state-transfer operators for all modes. No basis is necessary!" << std::endl;
            gVscfCalcDef[i_calc].SetBasis("NoBasis");
         }
         else
         {
            Mout << " No basis set given in Vscf Input " << endl;
            if (gBasis.size()>1) Mout 
               << " Do not know which basis to choose among " 
               << gBasis.size() << endl;
            MIDASERROR("No basis given in vscf input" );
         }
      }
      string name = gVscfCalcDef[i_calc].GetName();
      string oper_name = gVscfCalcDef[i_calc].Oper();
      string basis_name = gVscfCalcDef[i_calc].Basis();

      if (name.size()==0||name.substr(0,2)=="__"|| 
            ((name.find(oper_name)!=name.npos) && (name.find("__")!=name.npos) && (name.find("_"+basis_name)==name.npos)))
      {
         string new_name = oper_name+"_"+ basis_name;
         if (name.substr(0,2)=="__") name.erase(0,1);
         if ((name.find(oper_name)!=name.npos) && (name.find("_"+basis_name)==name.npos))
            name.erase(I_0,oper_name.size()+I_1);
         new_name = new_name + name;
         bool exist=true;
         while (exist)
         {
            bool nonexist = true;
            for (In i=0;i<gVscfCalcDef.size();i++)
               if (gVscfCalcDef[i].GetName() == new_name)  nonexist = false;
            if (nonexist) 
            {
               exist = false;
               gVscfCalcDef[i_calc].SetName(new_name);
            }
            else
            {
               new_name = new_name+"_new";
            }
         }
      }
      // Check basis is available
      In i_basis=-1;
      basis = gVscfCalcDef[i_calc].Basis();
      for (In i = I_0; i < gBasis.size(); i++)
      {
         if (gBasis[i].GetmBasName() == basis)
         {
            i_basis = i;
         }
      }
      if (  i_basis == -I_1
         && !opdef.StateTransferForAll()
         ) 
      {
         Mout << " Basis : " << basis << " is not found among " << std::endl;
         for (In i = I_0; i < gBasis.size(); i++)
         {
            Mout << gBasis[i].GetmBasName() << std::endl;
         }
         MIDASERROR(" Basis is not found in Vscf");
      }
      if (gVscfCalcDef[i_calc].GetNmodesInOcc()==0) 
      {
         gVscfCalcDef[i_calc].ZeroOcc(opdef.NmodesInOp());
      }

      if (gVscfCalcDef[i_calc].IoLevel()<gVibIoLevel) 
        gVscfCalcDef[i_calc].SetIoLevel(gVibIoLevel); 
      // Rsp
      if (gVscfCalcDef[i_calc].RspIoLevel()< gVscfCalcDef[i_calc].IoLevel())
        gVscfCalcDef[i_calc].SetRspIoLevel(gVscfCalcDef[i_calc].IoLevel()); 

      // Set analysis directory
      if (gVscfCalcDef[i_calc].GetmVscfAnalysisDir().empty())
      {
         gVscfCalcDef[i_calc].SetmVscfAnalysisDir(midas::input::gProgramSettings.GetMainDir() + "/analysis");
      }
   }

   // Set up VCC calcs from VSCF input
   // NB!!! This may add more Vcc calculations to gVccCalcDef
   for(auto& calcdef : gVccCalcDef)
   {
      // Set VSCF keywords for VCC calculation. Add more VCC calculations if UseAllVscf is set.
      detail::SetupVscfForVcc(calcdef);
   }

   // Now perform checks on all VccCalcDef%s
   for (In i_calc =0;i_calc<gVccCalcDef.size();i_calc++)
   {
      auto oper = gVccCalcDef[i_calc].Oper();
      if (oper.size()==0) 
      {
         if (gOperatorDefs.GetNrOfOpers()==1) 
         {
            gVccCalcDef[i_calc].SetOper(gOperatorDefs[I_0].Name());
         }
         else
         {
            Mout << " No operator given in Vcc Input " << endl;
            if (gOperatorDefs.GetNrOfOpers()>1) Mout 
               << " Do not know which operator to choose among " 
               << gOperatorDefs.GetNrOfOpers() << endl;
            MIDASERROR("No operators given in vcc input" );
         }
      }

      // Check operator is available
      oper = gVccCalcDef[i_calc].Oper();
      OpDef& opdef = gOperatorDefs.GetOperator(oper);


      auto basis= gVccCalcDef[i_calc].Basis();
      if (basis.size()==0) 
      {
         if (gBasis.size()==1) 
         {
            gVccCalcDef[i_calc].SetBasis(gBasis[0].GetmBasName());
         }
         else if  (  opdef.StateTransferForAll()
                  )
         {
            Mout  << " Using state-transfer operators for all modes. No basis is necessary!" << std::endl;
            gVccCalcDef[i_calc].SetBasis("NoBasis");
         }
         else
         {
            Mout << " No basis set given in Vcc Input " << endl;
            if (gBasis.size()>1) Mout 
               << " Do not know which basis to choose among " 
               << gBasis.size() << endl;
            MIDASERROR("No basis given in vcc input" );
         }
      }
      string name = gVccCalcDef[i_calc].GetName();
      string oper_name = gVccCalcDef[i_calc].Oper();
      string basis_name = gVccCalcDef[i_calc].Basis();

      if (name.size()==0||name.substr(0,2)=="__"|| 
            ((name.find(oper_name)!=name.npos) && (name.find("__")!=name.npos) &&(name.find("_"+basis_name)==name.npos)))
      {
         string new_name = oper_name+"_"+ basis_name;
         if (name.substr(0,2)=="__") name.erase(0,1);
         if ((name.find(oper_name)!=name.npos) && (name.find("_"+basis_name)==name.npos))
            name.erase(I_0,oper_name.size()+I_1);
         new_name = new_name + name;
         bool exist=true;
         while (exist)
         {
            bool nonexist = true;
            for (In i=0;i<gVccCalcDef.size();i++)
               if (gVccCalcDef[i].GetName() == new_name)  nonexist = false;
            if (nonexist) 
            {
               exist = false;
               gVccCalcDef[i_calc].SetName(new_name);
            }
            else
            {
               new_name = new_name+"_new";
            }
         }

      }

      // Check given Basis limits/primitive basis 
      if (gVccCalcDef[i_calc].NmodesInModals() > I_0 && gVccCalcDef[i_calc].GetNmodesInOcc() > I_0)
      {
         if (gVccCalcDef[i_calc].NmodesInModals() != gVccCalcDef[i_calc].GetNmodesInOcc())
         {
            MIDASERROR("Dimension of Nmodals and Occupation vectors does not match!");
         }
      }

      // Check coupling level of operator. If higher than supported by the V3 transformer, use the old and issue warning
      const auto& op_level = opdef.McLevel();
      if (  gVccCalcDef[i_calc].GetAutoSetupMethod()
         && gVccCalcDef[i_calc].V3trans()
         )
      {
         if (  op_level > I_6
            )
         {
            MidasWarning("V3 transformer has no data for higher mode-coupling level than 5. Switch to old transformer!");
            gVccCalcDef[i_calc].SetV3trans(false);
            gVccCalcDef[i_calc].SetV3type(V3::V3UNKNOWN);

            if (  gVccCalcDef[i_calc].UseTensorNlSolver()
               )
            {
               MidasWarning("TensorNlSolver only works with V3 transformer. Use old solver instead!");
               gVccCalcDef[i_calc].SetupDataContCalculation();
            }
         }
      }

      // Check basis is available
      In i_basis = -I_1;
      basis = gVccCalcDef[i_calc].Basis();
      for (In i = I_0; i < gBasis.size(); i++)
      {
         if (gBasis[i].GetmBasName() == basis)
         {
            i_basis = i;
         }
      }
      if (  i_basis == -I_1
         && !opdef.StateTransferForAll()
         ) 
      {
         Mout << " Basis : " << basis << " is not found among " << std::endl;
         for (In i = I_0; i < gBasis.size(); i++)
         {
            Mout << gBasis[i].GetmBasName() << std::endl;
         }
         MIDASERROR(" Basis is not found in Vcc");
      }
      if (gVccCalcDef[i_calc].GetNmodesInOcc()==0)
      {
         gVccCalcDef[i_calc].ZeroOcc(opdef.NmodesInOp());
      }

      string modal_filename = gVccCalcDef[i_calc].GetModalFileName();
      if (modal_filename.size()==0)
      {
         modal_filename = gVccCalcDef[i_calc].GetName() + "_Modals";
         gVccCalcDef[i_calc].SetModalFileName(modal_filename);
      }

      if (gVccCalcDef[i_calc].NmodesInModals()==0)
      {
         std::vector<In> tmp(gVccCalcDef[i_calc].GetNmodesInOcc()); 
         // Dirty - order significant here. If Vec Set then also HO from Pot. 
         if (gBasis[i_basis].GetmUseHoBasis())
         {
            for (In i_op_mode = I_0; i_op_mode < gVccCalcDef[i_calc].GetNmodesInOcc(); i_op_mode++) 
            {
               // Need to take care here, as the basis input might not have been read yet
               auto ho_qnrs = gBasis[i_basis].GetmHoQnrs();
               if (ho_qnrs.size() == I_1)
               {
                  tmp[i_op_mode] = ho_qnrs[I_0] + I_1;
               }
               else 
               {
                  tmp[i_op_mode] = ho_qnrs[i_op_mode] + I_1;
               }
            }
            Mout << " Automatic modal limits = max = " << tmp << endl;
            gVccCalcDef[i_calc].SetModalLimits(tmp);
            gVccCalcDef[i_calc].SetModalLimitsInput(true);
         }
         else
         {
            Mout  << " Basis limits for auto-generated basis will be set later!" << std::endl;
         }
      }

      //Mout << " Checking occupancies and Modals spaces " << endl;
      if (gVccCalcDef[i_calc].NmodesInModals() > I_0 && gVccCalcDef[i_calc].GetNmodesInOcc() > I_0)
      {
         bool ok = true;
         for (In i_op_mode = I_0; i_op_mode < gVccCalcDef[i_calc].GetNmodesInOcc(); i_op_mode++)
         {
            if (gVccCalcDef[i_calc].GetOccMode(i_op_mode) >= gVccCalcDef[i_calc].Nmodals(i_op_mode))
            {
               ok = false;
            }
         
            if (!ok)
            {
               MIDASERROR(" INPUT ERROR! Some occupation is beyond the modal space! ");
            }
         }
      }

      if (gVccCalcDef[i_calc].IoLevel()<gVibIoLevel) 
        gVccCalcDef[i_calc].SetIoLevel(gVibIoLevel); 
      // Rsp
      if (gVccCalcDef[i_calc].RspIoLevel()< gVccCalcDef[i_calc].IoLevel())
        gVccCalcDef[i_calc].SetRspIoLevel(gVccCalcDef[i_calc].IoLevel()); 
      
      // Set analysis directory
      if(gVccCalcDef[i_calc].GetmVscfAnalysisDir().empty())
      {
         gVccCalcDef[i_calc].SetmVscfAnalysisDir(midas::input::gProgramSettings.GetMainDir() + "/analysis");
      }
   }
   
   // Add an elementary operator if vibrational operators are at all used.  
   if (gOperatorDefs.GetNrOfOpers()>0&& gOperatorDefs[I_0].NmodesInOp()>I_0) 
   {
      gOperatorDefs.AddOperator();
      OpDef& elementary = gOperatorDefs[gOperatorDefs.GetNrOfOpers()-1];  // new_oper is a reference to the new operator 

      string s_elem="Elementary"; 
      elementary.SetIsElementaryOperator(true);
      elementary.SetName(s_elem); 
      elementary.SetType(s_elem); 
      gOperatorDefs.SetOperNameAndNr(elementary.Name(), gOperatorDefs.GetNrOfOpers()-1); 
      bool fix_old = true;
      if (fix_old && gOperatorDefs.GetNrOfOpers()>I_1)
      {
         for (In i=I_0;i<gOperatorDefs[I_0].NmodesInOp();++i)
         {
            GlobalModeNr i_g_mode = gOperatorDefs[I_0].GetGlobalModeNr(i);
            elementary.AddMode(i_g_mode);
         }
         elementary.CopyOneModeOpers(gOperatorDefs[I_0]); 
         elementary.mActiveTermsVec.resize(gOperatorDefs[I_0].NmodesInOp());
         elementary.AddModeOperProdToTerm(elementary.Nterms(), gOperatorDefs.GetModeLabel(I_0), LocalOperNr(I_0), true); // Add arbitrarily C_1*Q^2, q=first coordinates. 
         elementary.mNterms=I_1; 
         elementary.AddCterm(C_1); 
      }
      elementary.SetElementaryOperatorTo(I_0,I_0,I_0); //< Start with 0,0,0 randomly. 
   }

   // For TDVCC, add electronic DOF to MR modes if doing non-adiabatic dynamics
   for(auto& tdvcc : gTdvccCalcDef)
   {
      auto e_label = gOperatorDefs.ElectronicDofLabel();
      if (  !e_label.empty()
         )
      {
         auto e_nr = gOperatorDefs.ElectronicDofNr();
         const auto& opname = tdvcc.GetOper();
         const auto& op = gOperatorDefs.GetOperator(opname);
         
         // If operator contains active terms for electronic DOF
         if (  op.ActiveForMode(e_nr)
            )
         {
            LocalModeNr loc_e_nr = op.GetLocalModeNr(e_nr);
            auto n_elec_states = op.StateTransferIJMax(loc_e_nr) + I_1;
            MidasAssert(n_elec_states > I_1, "It makes no sense to have an electronic degree of freedom with only one electronic state!");

            auto xcs_modes = tdvcc.GetExtRangeModeLabels();
            xcs_modes.emplace(e_label);
            tdvcc.SetExtRangeModeLabels(xcs_modes);
         }
      }
   }

   // For MCTDH[n], add electronic DOF to MR modes if doing non-adiabatic dynamics
   for(auto& mctdh : gMcTdHCalcDef)
   {
      auto e_nr = gOperatorDefs.ElectronicDofNr();
      if (  mctdh.GetMethodInfo().template At<input::McTdHCalcDef::mctdhID>("PARAMETERIZATION") == input::McTdHCalcDef::mctdhID::LRASTDH
         && e_nr != -I_1
         )
      {
         const auto& opname = mctdh.GetOper();
         const auto& op = gOperatorDefs.GetOperator(opname);
         
         // If operator contains active terms for electronic DOF
         if (  op.ActiveForMode(e_nr)
            )
         {
            LocalModeNr loc_e_nr = op.GetLocalModeNr(e_nr);
            auto n_elec_states = op.StateTransferIJMax(loc_e_nr) + I_1;
            MidasAssert(n_elec_states > I_1, "It makes no sense to have an electronic degree of freedom with only one electronic state!");

            if (  mctdh.GetMethodInfo().Contains("EXTRANGEMODES")
               )
            {
               auto set = mctdh.GetMethodInfo().template At<std::set<In>>("EXTRANGEMODES");
               set.emplace(loc_e_nr);
               mctdh.AddMethodInfo("EXTRANGEMODES", set);
            }
            else
            {
               mctdh.AddMethodInfo("EXTRANGEMODES", std::set<In>{loc_e_nr});
            }
         }
      }
   }
}

