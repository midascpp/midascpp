/**
************************************************************************
* 
* @file                ElecInput.cc
* 
* Created:             18-10-2010
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*                      Kristian Sneskov (sneskov@chem.au.dk)
*
* Short Description:   ElecInput reader for the midas program.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/Input.h"
#include "nuclei/Nuclei.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "input/FindKeyword.h"

// using declarations
using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
typedef string::size_type Sst;

/**
* Local Declarations
* */
string EcorInput(std::istream&);

/**
* Read and check vib input.
* Read until next s = "#1..." which is returned.
* */
string ElecInput(std::istream& Minp,bool passive,string aInputFlag)
{
   if (gDebug) Mout << " Function: ElecInput, passive =  " << passive << endl;
   if (gDoElec) MidasWarning(" WARNING!  I have read Elec input more than once "); 
   gDoElec     = true; 
   enum INPUT {ERROR, IOLEVEL, SCF, ECOR, BASIS, INTEGRAL, MOLSTRUCT};
   const map<string,INPUT> input_word =
   {
      {"#2IOLEVEL",IOLEVEL},
      {"#2SCF",SCF},
      {"#2ECOR",ECOR},
      {"#2BASIS",BASIS},
      {"#2INTEGRAL",INTEGRAL},
      {"#2MOLSTRUCT",MOLSTRUCT}
   };
   In input_level = 2;
   string s="";
   getline(Minp,s);
   while (!Minp.eof() && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {

      if (passive) 
      {
         getline(Minp,s);
         continue;                                  // If passive loop simply to next #
      }
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> gElecIoLevel;
            getline(Minp,s);
            break;
         }
         case ECOR:
         {
            s = EcorInput(Minp);
            break;
         }
         case MOLSTRUCT:
         {
//            Mout << "sne is in molstruct" << endl;
            getline(Minp,s);
            istringstream input_s(s);
            In num_nuclei;
            string unit;
            input_s >> num_nuclei >> unit;
            Nb conv;
            if (unit == "Aa")
            {
               conv = C_1/C_TANG;
               Mout << " Coordinates readin in Aangstrom " << endl;
            }
            else if (unit == "au")
            {
               conv = C_1;
               Mout << " Coordinates readin in atomic units " << endl;
            }
            else 
            {
               Mout << " Known units is only au and Aa in Input Nuclei " << endl;
               MIDASERROR ("Unknown units in Nuclei Input in Pes input section " );
            }
            for (In i=I_0;i<num_nuclei;i++)
            {
               Nb x,y,z;
               string lbl;
               getline(Minp,s);          // Get new line with input
               istringstream input_s(s);
               input_s >> lbl;
               input_s >> x >> y >> z;
               Nuclei nuc_inp(x,y,z,-C_10_10,lbl);
               nuc_inp.SetQzFromLabel();
               if (unit == "Aa") nuc_inp.ScaleCoord(conv);
               gMolStruct.AddNucleus(nuc_inp);
            }
         getline(Minp,s);
         break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not an Elec level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
            getline(Minp,s);
         }
      }
   }
   return s;
}


/**
* Initialize Elec variables
* */
void InitializeElec()
{
   // Mout << " Initializing elec variables: " << endl;
   gDoElec              = false; 
   gDebug              = false; 
   gElecIoLevel          = 0;
}
