#include "input/ProgramSettings.h"

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <iostream>
#include <iomanip>
#include <list>
#include <cstdlib>

#include "inc_gen/Version.h"
#include "inc_gen/GitVersion.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/Input.h"
#include "util/Os.h"
#include "util/Path.h"

namespace midas
{
namespace input
{

// General types
using key_type         = std::string;
using description_type = std::string;

// Types for contruction of argument map and description list
using function_type        = std::function<int(int, char**)>;
using argument_tuple_type  = std::tuple<std::vector<key_type>, description_type, function_type>;
using argument_vector_type = std::vector<argument_tuple_type>;

// Indices for argument_tuple_type
constexpr int key_index         = 0;
constexpr int description_index = 1;
constexpr int function_index    = 2;

// Types for argument-to-function map
using function_ptr_type = std::shared_ptr<function_type>;
using argument_map_type = std::map<key_type, function_ptr_type>;

// Types for description list.
using description_tuple_type = std::tuple<std::vector<key_type>, description_type>;
using description_list_type  = std::list<description_tuple_type>;

namespace detail
{

/**
 * Create argument function map.
 **/
auto CreateArgumentMap
   ( const argument_vector_type& aArgumentVector
   )
{
   argument_map_type     argument_map;
   description_list_type description_list;

   for(auto& arg : aArgumentVector)
   {
      // Create description list entry
      description_list.emplace_back(std::make_tuple(std::get<key_index>(arg), std::get<description_index>(arg)));
      
      // Create argument map entry
      function_ptr_type function_ptr = std::make_shared<function_type>(std::get<function_index>(arg));
      for(auto& key : std::get<key_index>(arg))
      {
         argument_map.emplace(key, function_ptr);
      }
   }

   return std::make_tuple(argument_map, description_list);
}

/**
 * Print MIDAS version.
 *
 * @param aOs                The ostream to print to (defaults to std::cout).
 **/
void PrintVersion
   (  std::ostream& aOs = std::cout
   )
{
   aOs << "MidasCpp (Molecular Interactions, Dynamics, And Simulations Chemistry Program Package)\n"
       << "Version " << MidasVersion() << "\n";
}

/**
 * Print usage for midas executable.
 *
 * @param aDescriptionList   A list of descriptions for command line arguments.
 * @param aOs                The ostream to print to (defaults to std::cout).
 **/
void PrintUsage
   (  const description_list_type& aDescriptionList
   ,  std::ostream& aOs = std::cout
   )
{
   // Calculate max width
   int max_width = [&aDescriptionList]()
      {
         int max_width = 0;
         for(auto& descr : aDescriptionList)
         {
            int  width = 0;
            bool first = true;
            for(auto& key : std::get<key_index>(descr))
            {
               width += key.size();
               if(!first)
               {
                  width += 2;
               }
               first = false;
            }

            max_width = std::max(width, max_width);
         }
         return max_width;
      }();
   
   // Set options indentation
   std::string indent{"   "};
   
   // Print some general initial stuff
   aOs << "Usage  : midascpp.x [options] <input_file>\n"
       << "Options:\n";

   // Loop over descriptions and print
   for(auto& descr : aDescriptionList)
   {
      bool first = true;

      std::string key_string;
      for(auto& key : std::get<key_index>(descr))
      {
         if(!first)
         {
            key_string += ", ";
         }

         key_string += key;
         first = false;
      }
      aOs << indent << std::left << std::setw(max_width) << key_string << " : " << std::get<description_index>(descr) << "\n";
   }
}

/**
 *
 **/
void PrintGitInfo
   (  std::ostream& aOs = std::cout
   )
{
   aOs << "Git info: " << GIT_INFO_MSG << "\n";

   // Only output git info if this is a git repository.
   if constexpr(int(GIT_INFO) == 0)
   {
      // git commit and repository status (clean/dirty)
      aOs << "Commit  : " << GIT_COMMIT;
      if constexpr(int(GIT_REPO_DIRTY) == 0)
      {
         aOs << " (clean)" << "\n";
      }
      else if constexpr(int(GIT_REPO_DIRTY) == 1)
      {
         aOs << " (dirty)" << "\n";
      }
      
      // git branch
      aOs << "Branch  : " << GIT_BRANCH << "\n";
      
      // output date
      aOs << "Date    : " << GIT_DATE << "\n";
   }
}

} /* namespace detail */

/**
 * Set io level.
 **/
void GeneralSettings::SetIoLevel
   (  int  aIoLevel
   ,  bool aSetMask
   )
{
   if(!CheckCommandLineMask(mask::iolevel))
   {
      mIoLevel = aIoLevel;
      
      if(aSetMask)
      {
         UpdateCommandLineMask(mask::iolevel);
      }
   }
}

/**
 * Set debug value.
 **/
void GeneralSettings::SetDebug
   (  bool aDebug
   ,  bool aSetMask
   )
{
   if(!CheckCommandLineMask(mask::debug))
   {
      mDebug = aDebug;
      
      if(aSetMask)
      {
         UpdateCommandLineMask(mask::debug);
      }
   }
}

/**
 * Set numeric limits value.
 **/
void GeneralSettings::SetNumericLimits
   (  bool aNumericLimits
   ,  bool aSetMask
   )
{
   if(!CheckCommandLineMask(mask::numericlimits))
   {
      mNumericLimits = aNumericLimits;
      
      if(aSetMask)
      {
         UpdateCommandLineMask(mask::numericlimits);
      }
   }
}

/**
 * Set reserve oper.
 **/
void GeneralSettings::SetReserveOper
   (  In aReserveOper
   )
{
   mReserveOper = aReserveOper;
}

/**
 * Set reserve modes.
 **/
void GeneralSettings::SetReserveModes
   (  In aReserveModes
   )
{
   mReserveModes = aReserveModes;
}

/**
 * Set random seed.
 */ 
void GeneralSettings::SetSeed
   (  const std::string& aSeed
   ,  bool aSetMask
   )
{
   if(!CheckCommandLineMask(mask::seed))
   {
      mSeed = util::VectorFromString<unsigned int>(aSeed);
      
      if(aSetMask)
      {
         UpdateCommandLineMask(mask::seed);
      }
   }
}

/**
 * Constructor.
 *
 * Loads settings from environment into map of settings.
 * Settings to load are defined by a list.
 **/
EnvironmentSettings::EnvironmentSettings
   (
   )
{
   // List of required settings to load
   auto settings_required = std::vector<std::string>
      {  "MIDAS_INSTALL_PREFIX"
      ,  "MIDAS_INSTALL_DATADIR"
      };
   
   // List of settings with default
   auto settings_with_default = std::vector<std::pair<std::string, std::string> >
      {  {"MIDAS_WARNING_AS_ERROR", "0"}
      };

   // Load required settings into map
   for(  const auto& key : settings_required)
   {
      const char* const setting_ptr = std::getenv(key.c_str());
      if(setting_ptr)
      {
         std::string setting(setting_ptr);
         mSettings.insert(std::make_pair(key, setting));
      }
      else
      {
         MIDASERROR("Environment variable '" + key + "' not set.");
      }
   }
   
   // Load settings with default into map
   for(  const auto& key_value_pair : settings_with_default)
   {
      const auto& key   = std::get<0>(key_value_pair);
      const auto& value = std::get<1>(key_value_pair);

      const char* const setting_ptr = std::getenv(key.c_str());
      
      if(setting_ptr)
      {
         std::string setting(setting_ptr);
         mSettings.insert(std::make_pair(key, setting));
      }
      else
      {
         mSettings.insert(std::make_pair(key, value));
      }
   }
}

/**
 * Get environmental setting.
 *
 * @param aSetting   The setting to get, e.g. 'MIDAS_INSTALL_DATADIR'.
 *
 * @return     Returns value for setting if it exists, else throws an error.
 **/
const std::string& EnvironmentSettings::GetSetting
   (  const std::string& aSetting
   )  const
{
   auto iter = mSettings.find(aSetting);
   if(iter != mSettings.end())
   {
      return iter->second;
   }
   else
   {
      MIDASERROR("No setting '" + aSetting + "' present in environment.");
      return iter->second; // return to silence warning.
   }
}

/**
 * Take care of command Line input, and load into ProgramSettings structure.
 *
 * @param argc              The number of arguments.
 * @param argv              The vector of arguments.
 * @param aProgramSettings  On output will hold the read-in settings.
 *
 * @return  Returns true if command line was successfully parsed AND execution should continue, false otherwise.
 **/
bool ProgramSettings::CommandLineInput
   (  int argc
   ,  char* argv[]
   ,  ProgramSettings& aProgramSettings
   )
{  
   class commandline_exception
   {
      private:
         std::string m_msg;

      public:
         commandline_exception(const std::string& msg)
            :  m_msg(msg)
         {
         }

         std::string what() const
         {
            return m_msg;
         }
   };
   
   // Lambda to check for keyword
   auto is_keyword = [](char* cptr){ return cptr[0] == '-'; };

   // Some constants
   bool continue_execution = true;

   bool print_version  = false;
   bool print_usage    = false;
   bool print_git_info = false;
   bool quit           = false;

   auto check_command_line = [&]() { return !print_usage && !print_version && !print_git_info && !quit; };
   
   // Option function map
   auto [arguments_map, description_list] = detail::CreateArgumentMap
      ({
         // Print version information and then quit.
         { {"-v", "--version"}, {"Print version information, and quit."}, [&print_version](int iarg, char* argv[])
            { 
               print_version = true;
               return iarg; 
            } 
         },
         // Print help information and then quit.
         { {"-h", "--help"}, {"Print help message and quit."}, [&print_usage](int iarg, char* argv[])
            {
               print_usage = true;
               return iarg; 
            }
         },
         // Print help information and then quit.
         { {"--git-info"}, {"Print information on git repository and quit."}, [&print_git_info](int iarg, char* argv[])
            {
               print_git_info = true;
               return iarg; 
            }
         },
         // Print help information and then quit.
         { {"-q", "--quit"}, {"Read commandline, then quit."}, [&quit](int iarg, char* argv[])
            {
               quit = true;
               return iarg; 
            }
         },
         // Only try to read input, and then quit.
         { {"-i"}, {"Only do input parsing and then quit (not implemented!)."}, [](int iarg, char* argv[])
            {
               return iarg; 
            }
         },
         // Set maindir (this can also be set in input file, but setting on commandline will overwrite input file setting!).
         { {"-d", "--maindir"}, {"Set main directory for calculation (not scratch!)."}, [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--maindir' requires 1 argument.");
               aProgramSettings.mMainDir = std::string{argv[iarg]};

               return iarg; 
            }  
         },
         // Set maindir (this can also be set in input file, but setting on commandline will overwrite input file setting!).
         { {"-s", "--scratch"}, {"Set scratch directory for calculation."}, [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--scratch' requires 1 argument.");
               aProgramSettings.mScratch = std::string{argv[iarg]};
               return iarg; 
            }  
         },
         // Set number of threads to be used.
         { {"-n", "--numthreads", "--num-threads"}, {"Set number of threads to use."}, [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--num-threads' requires 1 argument.");
               aProgramSettings.mNumThreads = std::max(1, midas::util::FromString<int>(argv[iarg]));
               return iarg; 
            }  
         },
         // Set iolevel
         { {"--iolevel"}, {"Set IoLevel to use generally."}, [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--iolevel' requires 1 argument.");
               aProgramSettings.mGeneralSettings.SetIoLevel(std::max(1, midas::util::FromString<int>(argv[iarg])), true);
               return iarg; 
            }  
         },
         // Set iolevel
         { {"--seed"}, {"Provide random seed. Takes 1 integer."}, [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--seed' requires 1 argument.");
               aProgramSettings.mGeneralSettings.SetSeed(std::string(argv[iarg]), true);
               return iarg; 
            }  
         },
         // Set iolevel
         { {"--debug"}, {"Set to run in DEBUG mode (produces more output... much more)."}, [&aProgramSettings](int iarg, char* argv[])
            {
               aProgramSettings.mGeneralSettings.SetDebug(true, true);
               return iarg; 
            }  
         },
         // Set iolevel
         { {"--numeric-limits"}, {"Print extra information on numeric limits of executable."}, [&aProgramSettings](int iarg, char* argv[])
            {
               aProgramSettings.mGeneralSettings.SetNumericLimits(true, true);
               return iarg; 
            }  
         },
         // Set spawn paradigm
         {  {"--spawn-paradigm"}
         ,  {"Set the paradigm for spawning external programs. Options are: fork_exec, fork_server, or mpi_comm_spawn (default: fork_exec)."}
         ,  [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--spawn-paradigm' requires 1 argument.");
               aProgramSettings.mGeneralSettings.SetSpawnParadigm(StringToSpawnParadigm(argv[iarg]));
               return iarg; 
            }  
         },
         // Set spawn paradigm
         {  {"--socket"}
         ,  {"Path of local Unix socket used with '--spawn-paradigm=fork_server'."}
         ,  [argc, is_keyword, &aProgramSettings](int iarg, char* argv[])
            {
               ++iarg;
               if((iarg >= argc) || is_keyword(argv[iarg])) throw commandline_exception("Option '--socket' requires 1 argument.");
               aProgramSettings.mGeneralSettings.SetUnixSocketPath(argv[iarg]);
               return iarg; 
            }  
         }
      });
   
   // Get ready to parse commandline
   std::string input_file_path = "";
   
   // Loop over commandline input and parse
   try
   {
      for(int iarg = 1; iarg < argc; ++iarg)
      {
         auto arg_iter = arguments_map.find(argv[iarg]);
         if(arg_iter != arguments_map.end())
         {
            iarg = (*(arg_iter->second))(iarg, argv);
         }
         else
         {
            if(is_keyword(argv[iarg]))
            {
               throw commandline_exception("Unknown argument : " + std::string(argv[iarg]) + ".");
            }
            else if(input_file_path.empty())
            {
               input_file_path = argv[iarg];
            }
            else
            {
               throw commandline_exception("Unknown parameter : " + std::string(argv[iarg]) + ".");
            }
         }
      }

      if(check_command_line() && input_file_path.empty())
      {
         throw commandline_exception("No input file given.");
      }
   }
   catch(commandline_exception& e)
   {
      std::cout << e.what() << "\n\n";
      print_usage = true;
   }

   
   // Do some printout if requested
   if(print_usage)
   {
      detail::PrintUsage(description_list);
      exit(0);
   }

   if(print_version)
   {
      detail::PrintVersion();
      exit(0);
   }

   if(print_git_info)
   {
      detail::PrintGitInfo();
      exit(0);
   }

   if(quit) 
   {
      exit(0);
   }
   
   // Setup default paths if none we're given on command line
   if (aProgramSettings.mMainDir.empty())
   {
      aProgramSettings.mMainDir = os::Getcwd();
   }
   else
   {
      // If relative path, prepend current working directory (cwd)
      if (midas::path::IsRelPath(aProgramSettings.mMainDir))
      {
         aProgramSettings.mMainDir = os::Getcwd() +  "/" + aProgramSettings.mMainDir;
      }
   }
   
   // Setup default paths if none we're given on command line
   if (aProgramSettings.mScratch.empty())
   {
      aProgramSettings.mScratch = os::Getcwd();
   }
   else
   {
      // If relative path, prepend current working directory (cwd)
      if (midas::path::IsRelPath(aProgramSettings.mScratch))
      {
         aProgramSettings.mScratch = os::Getcwd() +  "/" + aProgramSettings.mScratch;
      }
   }

   // Check for input/output file names
   aProgramSettings.mInputFilePath = input_file_path;
   aProgramSettings.mInputFileName = path::FileName(aProgramSettings.mInputFilePath);
   if (aProgramSettings.mOutputFileName.empty())
   {
      aProgramSettings.mOutputFileName = path::BaseName(aProgramSettings.mInputFileName) + ".mout";
   }

   // Set global directory locations
   gMainDir  = aProgramSettings.mMainDir;
   gSaveDir   = gMainDir + "/savedir";
   gAnalysisDir = gMainDir + "/analysis";

   // Return whether to continue after reading command-line
   return continue_execution;
}

/**
 * Create global object
 **/
ProgramSettings gProgramSettings;

} /* namespace input */
} /* namespace midas */
