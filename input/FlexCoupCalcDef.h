/**
************************************************************************
* 
* @file                FlexCoupCalcDef.h
*
* Created:             10-02-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Handling input for flexible Coupling schemes in both
*                      PES and VCC calculations 
* 
* Last modified:       08-12-2014
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FLEXCOUPCALCDEF_H_INCLUDED
#define FLEXCOUPCALCDEF_H_INCLUDED
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <set>

#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "pes/molecule/MoleculeFile.h"

using namespace midas;

class FlexCoupCalcDef
{
   private:
      bool              mSetFlexCoup;
      bool              mSetGroupCouplings;
      vector<string>    mModeGroups;
      vector<string>    mGroupCombis;
      bool              mSetSystemCoup;
      molecule::MoleculeFileInfo  mMoleculeFileInfo = molecule::MoleculeFileInfo("","");
      bool              mSetEdiff;
      vector<Nb>        mEdiffThreshs;
      bool              mSetLocal;
      vector<Nb>        mLocalThreshs;
      bool              mSetMcScreen;
      string            mMcScreenFile;
      Nb                mMcScreenThr;
      bool              mSetAdgaPreScreen;
      string            mAdgaPreScreenFile;
      vector<Nb>        mAdgaPreScreenThr;
      In                mVccPertOrder;
      vector<string>    mVccPertFiles;
      Nb                mVccPertThr;
      In                mMaxEx;
   public:
      FlexCoupCalcDef() 
         : mSetFlexCoup(false) 
         , mSetGroupCouplings(false)
         , mSetSystemCoup(false)
         , mSetEdiff(false)
         , mSetLocal(false)
         , mSetMcScreen(false)
         , mMcScreenFile("ScreenEstimatesMcs")
         , mMcScreenThr(C_0)
         , mSetAdgaPreScreen(false)
         , mAdgaPreScreenFile("AdgaPreScreen")
         , mVccPertOrder(-I_1)
         , mVccPertThr(C_0)
         , mMaxEx(-I_1)
      {
      }

      ~FlexCoupCalcDef() {};
      FlexCoupCalcDef(const FlexCoupCalcDef& arFlexCoupCalcDef);
      void Reset(const FlexCoupCalcDef& arFlexCoupCalcDef);
      void PushBackmModeGroups(const string& arString){mModeGroups.push_back(arString);}
      void SetmVccPertThr(const Nb& arVccPertThr) {mVccPertThr=arVccPertThr;}
      void SetmVccPertOrder(const In& arVccPertOrder) {mVccPertOrder=arVccPertOrder;}
      void SetmMaxEx(const In& arMaxEx) {mMaxEx=arMaxEx;}
      void PushBackmGroupCombis(const string& arString){mGroupCombis.push_back(arString);}
      void SetmEdiffThreshs(const vector<Nb>& arEdiffThreshs) {mSetEdiff = true; mEdiffThreshs=arEdiffThreshs;}
      void SetmLocalThreshs(const vector<Nb>& arLocalThreshs) {mSetLocal = true; mLocalThreshs=arLocalThreshs;}
      void SetmSetFlexCoup(const bool& aSetFlexCoup) {mSetFlexCoup=aSetFlexCoup;}
      void SetmSetGroupCouplings(const bool& aSetGroupCouplings) {mSetGroupCouplings=aSetGroupCouplings;}
      void SetmSetSystemCoup(const bool& aSetSystemCoup) {mSetSystemCoup=aSetSystemCoup;}
      void SetmMcScreenFile(const string& arMcScreenFile) {mMcScreenFile=arMcScreenFile;}
      void SetmMcScreenThr(const Nb& arMcScreenThr) {mSetMcScreen=true; mMcScreenThr=arMcScreenThr;}
      void SetmAdgaPreScreenThr(const vector<Nb>& arAdgaPreScreenThr) {mSetAdgaPreScreen=true; mAdgaPreScreenThr=arAdgaPreScreenThr;}
      void SetMoleculeFileInfo(const molecule::MoleculeFileInfo& aMoleculeFileInfo) { mMoleculeFileInfo = aMoleculeFileInfo; }
      void AddmVccPertFile(const string& arFile) {mVccPertFiles.push_back(arFile);}
      const std::vector<std::string>& GetmModeGroups() const {return mModeGroups;}
      const std::vector<std::string>& GetmGroupCombis() const {return mGroupCombis;}
      bool GetmSetFlexCoup() const {return mSetFlexCoup;}
      bool GetmSetGroupCouplings() const {return mSetGroupCouplings;}
      bool GetmSetSystemCoup() const {return mSetSystemCoup;}
      bool GetmSetEdiff() const {return mSetEdiff;}
      bool GetmSetLocal() const {return mSetLocal;}
      bool GetmMcScreen() const {return mSetMcScreen;}
      bool GetmAdgaPreScreen() const {return mSetAdgaPreScreen;}
      In GetmVccPertOrder() const {return mVccPertOrder;}
      In GetmMaxEx() const {return mMaxEx;}
      Nb GetmVccPertThr() const {return mVccPertThr;}
      vector<string> GetmVccPertFiles() const {return mVccPertFiles;}
      const std::vector<Nb>& GetmEdiffThreshs() const {return mEdiffThreshs;}
      const std::vector<Nb>& GetmLocalThreshs() const {return mLocalThreshs;}
      Nb GetmMcScreenThr() const {return mMcScreenThr;}
      const std::vector<Nb>& GetmAdgaPreScreenThr() const {return mAdgaPreScreenThr;}
      const std::string& GetmMcScreenFile() const {return mMcScreenFile;}
      const std::string& GetmAdgaPreScreenFile() const {return mAdgaPreScreenFile;}
      const molecule::MoleculeFileInfo& GetMoleculeFileInfo() const { return mMoleculeFileInfo; }
      
      bool ReadMoleculeFile(std::istream& Minp, std::string& s);
};
#endif // FLEXCOUPCALCDEF_H_INCLUDED
