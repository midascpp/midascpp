#pragma once
#ifndef MIDAS_INPUT_MACROS_H_INCLUDED
#define MIDAS_INPUT_MACROS_H_INCLUDED

#include <type_traits>

namespace midas::input
{

namespace detail
{

/**
 *
 **/
#define CREATE_HAS_MEMBER(NAME) \
   template<typename, typename T> \
   struct has_##NAME { \
       static_assert( \
           std::integral_constant<T, false>::value, \
           "Second template parameter needs to be of function type."); \
   }; \
    \
   /* specialization that does the checking */ \
    \
   template<typename C, typename Ret, typename... Args> \
   struct has_##NAME<C, Ret(Args...)> { \
   private: \
       template<typename T> \
       static constexpr auto check(T*) \
       -> typename \
           std::is_same< \
               decltype( std::declval<T>().NAME( std::declval<Args>()... ) ), \
               Ret    /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */ \
           >::type;   /* attempt to call it and see if the return type is correct */ \
    \
       template<typename> \
       static constexpr std::false_type check(...); \
    \
       typedef decltype(check<C>(0)) type; \
    \
   public: \
       static constexpr bool value = type::value; \
   }; \
    \
   template<typename C, typename F> \
   static constexpr bool has_##NAME##_v = has_##NAME<C, F>::value;

/**
 *
 **/
CREATE_HAS_MEMBER(push_back);

/**
 *
 **/
template<class U, class T>
void push_back(T& t, const U& u)
{
   t.push_back(u);
}

} /* namespace detail */

/**
 * Define UNPACK macro.
 **/
#define UNPACK(...) __VA_ARGS__

/**
 * Define CREATE_VARIABLE macro
 **/
#define CREATE_VARIABLE(TYPE, NAME, VALUE) \
   public:\
      /*
       * If 'push_back'-able, create a push back function.
       */ \
      template \
         <  class U \
         ,  class T = TYPE \
         ,  std::enable_if_t<input::detail::has_push_back_v<T, void(U)>, int> = 0 \
         > \
      auto& PushBack##NAME(const U& u) \
      {\
         input::detail::push_back(m##NAME, u); \
         return *this; \
      } \
      /*
       * Set value.
       */ \
      auto& Set##NAME(const TYPE& a##NAME) \
      { \
         m##NAME = a##NAME; \
         return *this; \
      } \
      \
      /*
       * Get 'const' value.
       */\
      const TYPE& Get##NAME() const \
      { \
         return m##NAME; \
      } \
      \
   private: \
      /*
       * Get a modifiable value.
       */\
      TYPE& GetModifiable##NAME() \
      { \
         return m##NAME; \
      } \
      /*
       * Create member variable of type.
       */\
      TYPE m##NAME = VALUE;

} /* namespace midas::input */

#endif /* MIDAS_INPUT_MACROS_H_INCLUDED */
