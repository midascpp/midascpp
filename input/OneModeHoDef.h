/**
************************************************************************
* 
* @file                OneModeBasDef.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode HO Basis
* 
* Last modified: man mar 21, 2005  11:44
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONEMODEHODEF_H
#define ONEMODEHODEF_H

// std headers
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

// using declarations
using std::string;

class OneModeHoDef
{
   private:
      GlobalModeNr     mImode;                          ///< Global Mode number 
      string mBasType;                                  ///< Basis type = "HO" etc. 
      string mBasAuxDefs;                               ///< Further Basis definition
      Nb mOmeg;                                         ///< Omeg for HO basis definition
      Nb mMass;                                         ///< Omeg for HO basis definition
      In mLower;                                        ///< Lower bounds on quantum numb.
      In mHigher;                                       ///< Higher bounds on quantum numb.
      In mNbas;                                         ///< Higher bounds on quantum numb.
   public:
      OneModeHoDef();                                  ///< constructor as "zero"
      OneModeHoDef(In aMode, string aType, string aAux);  ///< constructor
      OneModeHoDef(In aMode, string aType, In aHigh, Nb aOmeg);  ///< constructor
                                                                
      In Lower() const  {return mLower; }                    ///< Get Global Mode number
      In Higher() const  {return mHigher; }                  ///< Get Global Mode number
      Nb Omeg() const  {return mOmeg; }                      ///< Get Omega
      Nb Mass() const  {return mMass; }                      ///< Get Mass 
      In Nbas() const  {return mNbas; }                      ///< Get Global Mode number
      GlobalModeNr GetGlobalModeNr() const  {return mImode; }   ///< Get Global Mode number
      string Type() const {return mBasType;}                 ///< Get Type
      string Aux() const {return mBasAuxDefs;}               ///< Get Aux info 
      void ChangeModeNr(In aGlobalModeNr) {mImode = aGlobalModeNr;}     ///< Change mode number
      
      //! Get value of normalized basis function
      Nb EvalHoBasis(const In& aIbas, const Nb& aQval) const;
      
      //! Get value of normalized basis function
      void EvalHoBasis(const In& aIbas, const MidasVector& arQval, MidasVector& arFuncVal) const;

      friend Nb HermitePoly(In aIbas, Nb aQval);                                    ///< Evaluate the Hermite polynomials up to aIbas
      friend void HermitePoly(In aIbas, Nb aQval, MidasVector& arFuncVal);          ///< Evaluate the Hermite polynomials up to aIbas
};

#endif /* ONEMODEHODEF_H */
