#include "input/InputOperFromFile.h"

#include "util/Isums.h"
#include "input/Trim.h"
#include "input/Input.h"
#include "input/OpInfo.h"
#include "mpi/FileToString.h"

namespace midas
{
namespace input
{

using Sst = std::string::size_type;

/**
 * Struct for holding settings for reading operators,
 * such that it is easy to add extra "keywords" or settings without
 * having to change read-in function definitions.
 * Also has the added benefit that read-in function will not have tonnes and tonnes of parameters!
 **/
struct OperInputSettings
{
   Nb mScreenZero    = 0.0;
   In mRestrictModes = 0;
};

/**
 * Define some operator input types.
 **/
enum OperFileType : int { ERROR, GENVSCF95, MIDASOLD, MIDASPOT, UNKNOWN };

/**
 * Identify operator file type, such that we later can call the appropriate operator reader.
 * The file is read line by line until a known keyword is found,
 * and based on this the type is set and returned.
 * When the function exits, the input stream has been rewinded.
 *
 * @param aIss   The input stream to classify type for. On output will read from the begining.
 *
 * @return       Returns appropriate operator file type based on what is hit first in the file.
 **/
OperFileType IdentifyOperFileType
   (  std::istream& aIss
   )
{
   OperFileType type = OperFileType::UNKNOWN;

   // Read lines until type is identified
   std::string str;
   while(GetParsedLine(aIss, str))
      {
      if (str.find("#0MIDASMOPINPUT") != std::string::npos)
      {
         MidasWarning("Use of #0MIDASMOPINPUT is depreciated. Use #0MIDASOPERATOR instead!");
         type = OperFileType::MIDASPOT;
         break;
      }
      else if (str.find("#0MIDASOPERATOR") != std::string::npos)
      {
         type = OperFileType::MIDASPOT;
         break;
      }
      else if (str.find("#3PRODUCTTERM") != std::string::npos)
      {
         MIDASERROR("Use of #3PRODUCTTERM is deprecated. Use #3OPERINPUT instead!");
      }
      else if  (  str.find("DALTON_FOR_MIDAS")   != std::string::npos
               || str.find("SCALINGFREQUENCIES") != std::string::npos
               || str.find("SCALINGFACTORS")     != std::string::npos
               )
      {
         type = OperFileType::MIDASOLD;
         break;
      }
      else if (str.find("GEN_VSCF_95") != std::string::npos)
      {
         type = OperFileType::GENVSCF95;
         break;
      }
   }

   // If badbit or failbit is set we return ERROR type
   if(aIss.fail())
   {
      type = OperFileType::ERROR;
   }

   // Reset input stream to read from beginning
   aIss.clear();
   aIss.seekg(0);

   // Return identified type
   return type;
}

/**
 * Read in operator in Midas Operator (.mop) format from an open input stream.
 *
 * @param aFile
 * @param arOpDef
 * @param aFileName
 * @param aFnr
 * @param aInpLevel
 * @param aScaling
 * @param aSkipHeaderAndFooter
 *
 * @return      Returns last line read (might be used by other input readers to continue reading the file if it contains more than the operator).
 **/
std::string ReadGenericMidasInput
   (  std::istream& aFile
   ,  OpDef& arOpDef
   ,  const std::string& aFileName
   ,  const In& aFnr
   ,  const In& aInpLevel
   ,  const Nb& aScaling
   ,  const bool& aSkipHeaderAndFooter
   )
{
   // Read operator
   MidasOperatorReader oper_reader(aFileName);
   std::string last_read = oper_reader.ReadOperator(aFile, aInpLevel, aSkipHeaderAndFooter);

   // Pass operator to MidasOperatorReader
   oper_reader.ScaleOperator(aScaling);
   oper_reader.GiveScalingAndNamestoOpDef(arOpDef);
   oper_reader.AddTermsToOpDef(arOpDef, aFnr);

   // Will try to deduce the type of operator from information given in the .mop file
   auto        operator_set_info = oper_reader.GetInfoLabel();
   const auto& operator_name     = arOpDef.Name();
   auto&       op_info           = OpDef::msOpInfo[operator_name];

   // Will only set the deduced operator type if no indication of type is previously carried out
   if (  !operator_set_info.empty()
      )
   {
      if (  op_info.GetType() == oper::unknown)
      {
         ParseSetInfoLine(operator_set_info, op_info);
      }
      else
      {
         Mout << " Property Type already set. Will not overwrite from '.mop'." << std::endl;
      }
   }

   // Return string to the other input readers
   return last_read;
}

/**
 * Read operator in the old historic Midas Mop format.
 * This format is obsolete, and the function is only here for our own sake,
 * as some potentials are currently only available in the old format.
 * At some point these might get converted, which makes this function obsolete to,
 * at which point it would be removed.
 * That means: do not use this function for anything (unless you know what you are doing, and in that case you probably shouldn't use it anyways...)! ;)
 *
 * @param aIs
 * @param aOperInputSettings
 * @param aFnr
 * @param arOpDef
 *
 * @return    Returns last read line.
 **/
std::string ReadOldMidasMopInput
   (  std::istream& aIs
   ,  const OperInputSettings& aOperInputSettings
   ,  In aFnr
   ,  OpDef& arOpDef
   )
{
   std::string str;
   In n_terms_nom = I_0;
   In n_terms_act = I_0;

   // Read the file line by line
   while (GetParsedLine(aIs, str))
   {
      if ( str.find("SCALINGFREQUENCIES") != std::string::npos )
      {
         // If the first file has scaling frequencies we go with them.
         // For the other files we ignore the frequencies alltogether, they need,
         // however to be the same as in the first file (maybe we should check them explicitly for safety?).
         if (aFnr == I_0)
         {
            Sst i_prod = str.find("N_FRQS");
            str.erase(I_0,i_prod);
            i_prod = str.find("=");
            str.erase(I_0,i_prod+1);
            istringstream input_s(str);
            In n_frqs;
            input_s >> n_frqs;
            int i = 0;
            std::vector<Nb> frequencies(n_frqs);
            while (!aIs.eof() && i < n_frqs)
            {
               aIs >> frequencies[i++];
            }
            arOpDef.SetFreqs(frequencies);
         }
      }
      else if ( str.find("SCALINGFACTORS") != std::string::npos )
      {
         MIDASERROR("SCALINGFACTORS is obsolete. If you encounter this error, tough luck! :C");
      }
      else if ( str.find("DALTON_FOR_MIDAS") != std::string::npos )
      {
         while (midas::input::GetLine(aIs, str)) // get new lines with mode and operators end.
         {
            istringstream input_s(str);
            Nb coef;
            while(input_s >> coef) // Read only terms with a number first - ignore the rest "
            {
               coef*=arOpDef.GetCoefForOpFile(aFnr);
               n_terms_nom++;
               vector<In> i_mode_vec;
               In j_mode=I_0;
               while (input_s >> j_mode)
               {
                  i_mode_vec.push_back(j_mode);
               }

               // Screen terms away if they are too close to zero.
               if (aOperInputSettings.mScreenZero && (fabs(coef) < aOperInputSettings.mScreenZero))
               {
                  continue;
               }

               In n_prod = i_mode_vec.size();
               //for (In i=0;i<n_prod;i++) input_s >> s_pow_vec[i];
               //for (In i=0;i<n_prod;i++) coef*=norms[i_mode_vec[i]-I_1];
               //coef /= Faculty(n_prod);
               set<In> idxs;
               for (In i=0;i<n_prod;i++) idxs.insert(abs(i_mode_vec[i]));
               vector<In> modes;
               for (set<In>::const_iterator ci=idxs.begin();ci!=idxs.end();ci++) modes.push_back(abs(*ci));
               vector<vector<In> > powers;
               for(In i = I_0; i < modes.size();i++)
               {
                  vector<In> temp;
                  temp.push_back(I_0);
                  temp.push_back(I_0);
                  powers.push_back(temp);
               }
               for (In i=0;i<n_prod;i++)
               {
                  for (In j=0;j<powers.size();j++)
                  {
                     if (modes[j]==i_mode_vec[i]) // we have one of the originals
                        powers[j][I_0]++;
                     else if (modes[j] == -i_mode_vec[i] && i_mode_vec[i] != i_mode_vec[I_0])
                        powers[j][I_1]--;
                     else if (modes[j]==-i_mode_vec[i])
                        powers[j][I_1]++;
                  }
               }
               /*Mout << " Read from input : " << i_mode_vec << endl;
               Mout << " Read in vector of modes  " << modes << endl;
               Mout << " Read in vector of powers [I_0] ";
               for(In i = I_0; i < powers.size(); i++)
                  Mout << powers[i][I_0] << " ";
               Mout << endl;
               Mout << " Read in vector of powers [I_1] ";
               for(In i = I_0; i < powers.size(); i++)
                  Mout << powers[i][I_1] << " ";
               Mout << endl;*/

               // Check actual nr. of modes in term
               In n_modes = modes.size();
               if (aOperInputSettings.mRestrictModes)
               {
                  if (n_modes> aOperInputSettings.mRestrictModes)
                  {
                     continue; // Skip if more than a certain number is coupled.
                  }
               }

               In n_actual=I_0;
               for (In i=0;i<n_modes;i++)
               {
                  if (modes[i]== I_0) continue;
                  string s_mode = std::to_string(modes[i]);
                  if(powers[i][I_1] == I_0)
                  {
                     //string s_mode = std::to_string(modes[i]);
                     //string s_op   = "q^"+std::to_string(powers[i][I_0]);
                     //Mout << " Add " << s_mode << " " << s_op << endl;
                     arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, powers[i][I_0], I_0, false, (i == 0));
                  }
                  else if(powers[i][I_1] != I_0 && powers[i][I_0] == I_0)
                  {
                     arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, I_0, powers[i][I_1], false, (i == 0));
                  }
                  else if(powers[i][I_1] == I_1)
                  {
                     arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, powers[i][I_0], I_0, true, (i == 0));
                  }
                  else if(powers[i][I_1] == -I_1)
                  {
                     arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, powers[i][I_0], I_1, false, (i == 0));
                  }
                  else if(powers[i][I_1] == I_2)
                  {
                     arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, powers[i][I_0], I_1, true, (i == 0));
                  }
                  n_actual++;
               }
               if (n_actual==I_0)
               {
                  Mout << " A term with no active operator - a constant " << endl;
                  string s_mode = gOperatorDefs.GetModeLabel(arOpDef.GetGlobalModeNr(I_0));
                  arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, I_0, I_0, false, true);
               }
               n_terms_act++;
               arOpDef.SetNterms(arOpDef.Nterms()+I_1);
               if (  n_prod > I_1
                  )
               {
                  arOpDef.IncrNCouplingTerms();
               }
               arOpDef.AddCterm(coef);
            }
         }
      }
   }

   // Do some print out
   if (gIoLevel > I_5)
   {
      Mout << " The number of operators terms on file:   " << setw(I_6) << n_terms_nom << std::endl;
      Mout << " The number of operators after screening: " << setw(I_6) << n_terms_act << std::endl;
   }

   // Return last read line
   return str;
}

/**
 * Read in operator in vscf_95 format.
 * The vscf_95 format is the format used in some of the formaldehyde examples in the test_suite.
 * It is more or less a dead format, but the functionality is kept here as
 * we have tests in the test_suite that uses this format.
 *
 * A short (and somewhat bad) description of the format:
 *
 *    n = # max number of modes that are coupled in a term.
 *    Assumed format i_1, i_2 i_3 ... i_n , ip_1, i..... ip_n C
 *    for the term C * (q_i_1)^ip_1 ... (q_i_n)^ip_n
 *    Its assumed that a zero among the i_1 ... i_n means mode is inactive in term.
 *    If there is ip_1 that is di, then it is assumed it is the i-th derivative.
 *
 * @param aIs                    The input stream to read from.
 * @param aOperInputSettings     Input settings.
 * @param aFnr                   The function number. (this WILL be removed !!!)
 * @param arOpDef                The OpDef to read the operator into.
 *
 * @return           Returns last read line.
 **/
std::string ReadGenVscf95Input
   (  std::istream& aIs
   ,  const OperInputSettings& aOperInputSettings
   ,  In aFnr
   ,  OpDef& arOpDef
   )
{
   std::string str;
   int n_prod = 0;

   // Read until "PROD" is found. This indicates start of input.
   while(GetParsedLine(aIs, str))
   {
      Sst i_prod;
      if ( ( i_prod = str.find("PROD")) != std::string::npos)
      {
         str.erase(I_0, i_prod);
         i_prod = str.find("=");
         str.erase(I_0,i_prod+1);
         std::istringstream input_s(str);
         input_s >> n_prod;
         break;
      }
   }

   // Preallocate some working memory
   std::vector<std::string> s_mode_vec(n_prod);
   std::vector<std::string> s_pow_vec (n_prod);

   // Read the operator input
   while (midas::input::GetLine(aIs, str) && str.substr(I_0, I_1) != "#") // get new lines with mode and operators defs.
   {
      std::istringstream input_s(str);
      for (In i=0;i<n_prod;i++) input_s >> s_mode_vec[i];
      for (In i=0;i<n_prod;i++) input_s >> s_pow_vec[i];

      // check actual nr. of modes
      if (aOperInputSettings.mRestrictModes)
      {
         In n_actual = I_0;
         for (In i=0;i<n_prod;i++)
         {
            if (atoi(s_mode_vec[i].c_str()) != I_0)
            {
               n_actual++;
            }
         }
         if (n_actual > aOperInputSettings.mRestrictModes)
         {
            continue;
         }
      }

      Nb coef;
      input_s >> coef;
      coef *= arOpDef.GetCoefForOpFile(aFnr);

      // If coefficient is non-zero we try yo add to the OpDef
      if (input_s)
      {
         // Screen terms away if they are too close to zero.
         if (aOperInputSettings.mScreenZero && (fabs(coef) < aOperInputSettings.mScreenZero))
         {
            continue;
         }

         // Read in operator term
         In n_actual = I_0;
         for (In i = 0; i < n_prod; i++)
         {
            if (atoi(s_mode_vec[i].c_str()) == I_0) continue;
            if(s_pow_vec[i].find("d") != s_pow_vec[i].npos)
            {
               In der = atoi(s_pow_vec[i].substr(I_1, s_pow_vec[i].size()).c_str());
               string s_mode = s_mode_vec[i];
               arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, I_0, der, false, (i == 0));
            }
            else
            {
               In pow = atoi(s_pow_vec[i].c_str());
               string s_mode = s_mode_vec[i];
               arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, pow, I_0, false, (i == 0));
            }
            n_actual++;
         }
         if (n_actual==I_0)
         {
            Mout << " A term with no active operator - a constant " << endl;
            string s_mode = gOperatorDefs.GetModeLabel(arOpDef.GetGlobalModeNr(I_0));
            arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), s_mode, I_0, I_0, false, true);
         }
         arOpDef.SetNterms(arOpDef.Nterms() + I_1);
         if (  n_prod > 1
            )
         {
            arOpDef.IncrNCouplingTerms();
         }
         arOpDef.AddCterm(coef);
      }
   }

   return str;
}

/**
 * Read an operator from a file.
 *
 * @param arOpDef
 * @param aNrestrictModes
 * @param aZeroThr
 **/
void InputOperFromFile
   (  OpDef& arOpDef
   ,  In aNrestrictModes
   ,  Nb aZeroThr
   )
{
   // Create settings for read-in
   OperInputSettings oper_input_settings;
   oper_input_settings.mScreenZero    = aZeroThr;
   oper_input_settings.mRestrictModes = aNrestrictModes;

   // Loop over requested operators
   for (In fnr = I_0; fnr < arOpDef.NrOfFiles(); ++fnr)
   {
      if (gIoLevel > I_5)
      {
         Mout << " Reading operator from file: " << std::endl;
         Mout << "  " << arOpDef.GetOpFile(fnr) << std::endl;
      }

      // Open operator file as input string stream
      std::istringstream oper_input_file = midas::mpi::FileToStringStream(arOpDef.GetOpFile(fnr));

      // Identify operator type
      auto type = IdentifyOperFileType(oper_input_file);

      // Do operator read-in based on type found
      switch(type)
      {
         case MIDASPOT:
         {
            auto last_read = ReadGenericMidasInput(oper_input_file, arOpDef, arOpDef.GetOpFile(fnr), fnr, 0);
            break;
         }
         case MIDASOLD:
         {
            auto last_read = ReadOldMidasMopInput(oper_input_file, oper_input_settings, fnr, arOpDef);
            break;
         }
         case GENVSCF95:
         {
            auto last_read = ReadGenVscf95Input(oper_input_file, oper_input_settings, fnr, arOpDef);
            break;
         }
         case UNKNOWN:
         {
            Mout << " Unknown operator format in file : " << arOpDef.GetOpFile(fnr) << std::endl;
         }
         case ERROR: // fallthrough
         default:
         {
            break;
         }
      }
   }
}

} /* namespace input */
} /* namespace midas */
