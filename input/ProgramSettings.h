#ifndef MIDAS_INPUT_PROGRAM_SETTINGS_H_INCLUDED
#define MIDAS_INPUT_PROGRAM_SETTINGS_H_INCLUDED

#include <string>
#include <map>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "input/Macros.h"

namespace midas
{
namespace input
{

/**
 * Enum defining the different available spawn paradigms.
 **/
enum class SpawnParadigm : int { FORK_EXEC, FORK_SERVER, MPI_COMM_SPAWN, ERROR };

/**
 * Convert std::string to SpawnParadigm enum.
 *
 * @param paradigm
 *    The std::string to convert.
 *
 * @return
 *    Return enum corresponding to string.
 **/
inline SpawnParadigm StringToSpawnParadigm
   (  const std::string& paradigm
   )
{
   if(paradigm == "fork_exec")
   {
      return SpawnParadigm::FORK_EXEC;
   }
   else if(paradigm == "fork_server")
   {
      return SpawnParadigm::FORK_SERVER;
   }
   else if(paradigm == "mpi_comm_spawn")
   {
      return SpawnParadigm::MPI_COMM_SPAWN;
   }
   else
   {
      return SpawnParadigm::ERROR;
   }
}

/**
 * Class for holding General global settings.
 **/
class GeneralSettings
{
   private:
      //! Define set of mask values (powers of 2: 2^0, 2^1, 2^2, etc.).
      enum mask : int { iolevel = 1, debug = 2, numericlimits = 4, seed = 8};
      //! Mask to see which arguments were set on command line.
      int         mCommandLineMask = 0;
      //! The global IOLEVEL. This can be overwritten locally.
      int         mIoLevel{2};
      //! Print extra runtime debug information? E.g. detailed intermediate numerical results.
      bool        mDebug{false};
      //! Print extra information in numeric limits of the executable.
      bool        mNumericLimits{false};
      //! Space to be reserved before reading in operator terms.
      In          mReserveOper  = I_0;
      //! Space to be reserved before reading in modes.
      In          mReserveModes = I_0;
      //! Seed sequence
      std::vector<unsigned int> mSeed = {};
      //! Paradigm for spawning external programs
      CREATE_VARIABLE(SpawnParadigm, SpawnParadigm, SpawnParadigm::FORK_EXEC);
      //! Path of local UNIX socket if using SpawnParadigm::FORK_SERVER
      CREATE_VARIABLE(std::string, UnixSocketPath, std::string{""});
   
      //! Update the mask.
      void UpdateCommandLineMask(const mask m)
      {
         mCommandLineMask = mCommandLineMask | m;
      }

      //! Check mask.
      bool CheckCommandLineMask(const mask m)
      {
         return (mCommandLineMask & m);
      }

   public:
      //! Constructor.
      GeneralSettings() = default;

      //! Set io level.
      void SetIoLevel(int aIoLevel, bool aSetMask = false);
      
      //! Set io level.
      void SetDebug(bool aDebug, bool aSetMask = false);
      
      //! Set io level.
      void SetNumericLimits(bool aNumericLimits, bool aSetMask = false);

      //! Set reserve oper.
      void SetReserveOper(In aReserveOper);
      
      //! Set reserve modes.
      void SetReserveModes(In aReserveModes);
      
      //! Set reserve modes.
      void SetSeed(const std::string& aSeed, bool aSetMask = false);

      //! Get io level.
      int GetIoLevel() const { return mIoLevel; }
 
      //! Get debug run true/false?
      bool GetDebug()  const { return mDebug; }
      
      //! Get print numeric limits true/false?
      bool GetNumericLimits()  const { return mNumericLimits; }

      //! Get reserve oper.
      In GetReserveOper() const { return mReserveOper; }
      
      //! Get reserve modes.
      In GetReserveModes() const { return mReserveModes; }

      //! Get seed.
      const std::vector<unsigned int>& GetSeed() const { return mSeed; }
};

/**
 * Class for holding environment settings.
 **/
class EnvironmentSettings
{
   private:
      //! Map of environment settings. E.g. Key "PATH", value "/usr/bin".
      std::map<std::string, std::string> mSettings;

   public:
      //! Constructor.
      EnvironmentSettings();

      //! Get setting from key.
      const std::string& GetSetting(const std::string&) const;
};

/**
 * Class for holding global Program settings.
 **/
class ProgramSettings
{
   private:
      //!@{
      //! Variables for holding important paths.
      //! Path of the input file
      std::string     mInputFilePath{""};
      //! The filename of the input file
      std::string     mInputFileName{""};
      //! The filename of the output file
      std::string     mOutputFileName{""};
      //! The main directory path for the calculation. Here everything will be saved (savedir, anylysis, etc.).
      std::string     mMainDir{""};
      //! The scratch directory path for the calculation.
      std::string     mScratch{""};
      //! The rank scratch directory path for the calculation.
      std::string     mRankScratch{""};
      //!@}

      //! The number of threads to use at max.
      int             mNumThreads{1};
      //! General settings for all calculation types.
      GeneralSettings mGeneralSettings{};
      //! Environment settings for all calculation types.
      EnvironmentSettings mEnvironmentSettings{};

   public:
      //! Constructor
      ProgramSettings() = default;

      //! Parse command line input.
      static bool CommandLineInput(int argc, char* argv[], ProgramSettings& aProgramSettings);

      //! Get input file path.
      const std::string& GetInputFilePath() const { return mInputFilePath; }
      
      //! Get output file name.
      const std::string& GetOutputFileName() const { return mOutputFileName; }

      //! Get main directory path.
      const std::string& GetMainDir() const { return mMainDir; }
      
      //! Get main directory path.
      void SetMainDir(const std::string& aMainDir) { mMainDir = aMainDir; }
      
      //! Get scratch directory path.
      const std::string& GetScratch() const { return mScratch; }
      
      //! Set scratch directory path.
      void SetScratch(const std::string& aScratch) { mScratch = aScratch; }
      
      //! Get rank scratch directory path.
      const std::string& GetRankScratch() const { return mRankScratch; }
      
      //! Set rank scratch directory path.
      void SetRankScratch(const std::string& aRankScratch) { mRankScratch = aRankScratch; }
      
      //! Set output file name.
      void SetOutputFileName(const std::string& aOutputFileName) { mOutputFileName = aOutputFileName; }

      //! Get number of threads.
      int GetNumThreads() const { return mNumThreads; }
      
      //! Get general/global settings.
      const GeneralSettings& GetGeneralSettings() const { return mGeneralSettings; }
      
      //! Get general/global settings.
            GeneralSettings& GetGeneralSettings()       { return mGeneralSettings; }
      
      //! Get environment settings.
      const EnvironmentSettings& GetEnvironmentSettings() const { return mEnvironmentSettings; }
};

// Declare global Program settings object.
extern ProgramSettings gProgramSettings;

} /* namespace input */
} /* namespace midas */

#endif /* MIDAS_INPUT_PROGRAM_SETTINGS_H_INCLUDED */
