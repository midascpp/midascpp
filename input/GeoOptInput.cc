/**
************************************************************************
* 
* @file                GeoOptInput.cc
*
* 
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Input reader for the GeoOpt class 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/GeoOptCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"

using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

/**
* Read and check GeoOpt input.
* Read until next s = "#i..." i<3 which is returned.
* */
std::string GeoOptInput(std::istream& Minp)
{

   gDoGeoOpt  = true; 
   gGeoOptCalcDef.push_back(GeoOptCalcDef());
   GeoOptCalcDef& new_geoopt = gGeoOptCalcDef[gGeoOptCalcDef.size()-1];  // new_oper is a reference to the new operator

   enum INPUT {ERROR, IOLEVEL, NAME, DATABASE, COORDFILE, SAMPLE, ENERGY, GRADIENT, HESSIAN, OPT, 
               ONLYICOORD, ADDGRAD, SIMCHECK, COORDTYPE, DAMPING, READCOVAR, STORECOVAR, KERNEL };
   const map<string,INPUT> input_word =
   {
      {"#2IOLEVEL",IOLEVEL}         ,
      {"#2NAME",NAME}               ,
      {"#2DATABASE",DATABASE}       ,
      {"#2COORDFILE",COORDFILE}     ,
      {"#2SAMPLE",SAMPLE}           ,
      {"#2ENERGY",ENERGY}           ,
      {"#2GRADIENT",GRADIENT}       ,
      {"#2HESSIAN",HESSIAN}         ,
      {"#2OPT",OPT}                 ,
      {"#2ONLYICOORD",ONLYICOORD}   ,
      {"#2CHECKSIM",SIMCHECK}       ,
      {"#2ADDGRAD",ADDGRAD}         ,
      {"#2COORDTYPE",COORDTYPE}     ,
      {"#2DAMPING",DAMPING}         ,
      {"#2READCOVAR",READCOVAR}     ,
      {"#2STORECOVAR",STORECOVAR}   ,        
      {"#2KERNEL",KERNEL}          
   };


   //enum COORD { ERROR, XYZ, ZMAT, XYZCENTER, MINT, DIST };
   const map<string,CoordType> input_coord =
   {
      {"XYZ",XYZ}             ,
      {"ZMAT",ZMAT}           ,
      {"XYZCENTER",XYZCENTER} ,
      {"MINT",MINT}           ,
      {"DIST",DIST}        
   };


   string s="";
   In input_level=2;
   bool already_read = false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read = false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);       // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);             // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                          // Get new line with input
            new_geoopt.SetIoLevel(midas::util::FromString<In>(s));
            break;
         } 
         case NAME:
         {
            getline(Minp,s);                                          // Get new line with input
            istringstream input_s(s);
            string s_name="";
            input_s >> s_name;
            new_geoopt.SetName(s_name);
            for (In i_calc =0; i_calc < gGeoOptCalcDef.size() - 1; i_calc++)
               if (gGeoOptCalcDef[i_calc].GetName() == new_geoopt.GetName()) 
                  MIDASERROR("name for geometry optimization is already in use");
            break;
         }
         case DATABASE:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;
            new_geoopt.SetDatabase(s_file);

            break;
         }
         case COORDFILE:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;
            new_geoopt.SetCoordFile(s_file);
            break; 
         }
         case SAMPLE:
         {
            bool add = true;
            new_geoopt.SetCompModeSample(add);
            break;

         }
         case ENERGY:
         {
            bool add = true;
            new_geoopt.SetCompModeEnergy(add);
            break;
         }
         case GRADIENT:
         {
            bool add = true;
            new_geoopt.SetCompModeGradient(add);
            break;
         }
         case HESSIAN:
         {
            bool add = true;
            new_geoopt.SetCompModeHessian(add);
            break;
         }
         case ONLYICOORD:
         {
            bool add = true;
            new_geoopt.SetOnlyCoordAna(add);
            break;
         }
         case SIMCHECK:
         {
            bool add = true;
            new_geoopt.SetOnlySimCheck(add);
            break;
         }
         case OPT:
         {
            bool add = true;
            new_geoopt.SetCompModeOptimize(add);
            break;
         }
         case ADDGRAD:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;
            bool add = true;
            new_geoopt.SetAddGrad(add);
            new_geoopt.SetFileAddGrad(s_file);
            break; 
         }
         case DAMPING:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            Nb damp = 1.0;
            input_sfile >> damp;
            new_geoopt.SetDamping(damp);
            break; 
         }
         case COORDTYPE:
         {
            getline(Minp,s);
            istringstream input_stype(s);
            string stype="";
            input_stype >> stype;

            transform(stype.begin(),stype.end(),stype.begin(),(In(*) (In))toupper);       // Transform to all upper.
            while(stype.find(" ")!= stype.npos) stype.erase(stype.find(" "),I_1);         // Delete ALL blanks
            //CoordType coordtype = midas::input::FindKeyword(input_coord, stype);
            CoordType coordtype = input_coord.at(stype);  

            new_geoopt.SetCoordinateType(coordtype); 

            break;
         }
         case READCOVAR:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;

            bool readcovar = true;
            new_geoopt.SetReadCovar(readcovar);
            new_geoopt.SetFileCovarRead(s_file);
            break; 
         }
         case STORECOVAR:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;

            bool storecovar = true;
            new_geoopt.SetSaveCovar(storecovar);
            new_geoopt.SetFileCovarSave(s_file);
            break; 
         }
         case KERNEL:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_skern(s);
            string s_kern="";
            input_skern >> s_kern;

            transform(s_kern.begin(),s_kern.end(),s_kern.begin(),(In(*) (In))toupper); 

            // Sanity Check(s)
            std::vector<std::string> list = { "OUEXP", "MATERN32", "MATERN52", "SQEXP", "SC32", "SC52", "POLYNOM", "PERIODIC", "SCEXP"  };
            bool found = (std::find(list.begin(), list.end(), s_kern) != list.end());
  
            if (!found)
            {
               Mout << "Kernel " << s_kern << " unknown!" << std::endl;
               MIDASERROR(" Check your input please ");
            }

            new_geoopt.SetKernel(s_kern);
            break; 
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a GeoOpt level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}

/**
* Initialize GeoOpt variables
* */
void InitializeGeoOpt()
{
   Mout << " Initializing geometry optimization interface variables: " << endl;
   gDoGeoOpt  = false; 
}




