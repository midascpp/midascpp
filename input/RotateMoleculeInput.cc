/**
************************************************************************
* 
* @file                RotateMoleculeInput.cc
*
* 
* Created:             2016-09-23
*
* Author:              Diana Madsen (diana@chem.au.dk)
*
* Short Description:   Input reader for the RotateMolecule module
*                      
*                      
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/RotCoordCalcDef.h"
#include "input/GeneralInputParser.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "input/ModSysCalcDef.h"

using std::vector;
using std::map;
using std::make_pair;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;
using std::stringstream;

// forward declarations
string RotateMoleculeInput(std::istream& Minp, input::ModSysCalcDef* apCalcDef, const In& arInputLevel)
{

   In input_level = arInputLevel + I_1;
   string s;//To be returned when we are done reading this level
   enum INPUT
   {
      ROTAXISANGLE,
      ERROR
   };
   
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"ROTAXISANGLE",ROTAXISANGLE}
   };

   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      
      switch(input)
      {
         case ROTAXISANGLE:
         {
            Mout << "Rotating molecule!" << std::endl;
            midas::input::GetLine(Minp,s);
            midas::input::AssertNotKeyword(s);
            std::vector<string> definerotation = midas::util::StringVectorFromString(s);
            MidasAssert((definerotation.size() == I_2), "The number of entries for defining rotation of the molecule should be two!");
            MidasAssert((definerotation[I_0].length() == I_1), "Type only one character for the rotation axis: X,Y or Z");
            apCalcDef->SetRotateMolecule(true);
            apCalcDef->AddAxisAngle(definerotation[I_0], midas::util::FromString<Nb>(definerotation[I_1]));
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " RotAxisAngle " << s_orig <<
               " is not a ModVibCoord level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}
