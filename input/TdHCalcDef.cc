/**
************************************************************************
* 
* @file                TdHCalcDef.cc
*
* Created:             30-06-2011
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a Vcc calculation
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/TdHCalcDef.h"
#include "input/OdeInput.h"
#include "input/GetLine.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"
#include "util/conversions/VectorFromString.h"

namespace midas::input
{

/**
 * Sanity check
 **/
void TdHCalcDef::SanityCheck
   (
   )
{
   // Check that we have ODE-integrator info.
   if (  mOdeInfo.empty()
      )
   {
      MIDASERROR("No OdeInfo in TdHCalcDef!");
   }

   // Check sanity of modal-basis definitions
   if (  !this->mModalBasisLimits.empty()
      && !this->mSpectralBasis
      )
   {
      MIDASERROR("TdHCalcDef::mSpectralBasis must be true, if mSpectralBasisLimits is not empty!");
   }

   // Check that we transform the initial basis for exptdh
   if (  this->mParametrization == midas::tdh::tdhID::EXPONENTIAL
      && !this->mSpectralBasis
      )
   {
      MIDASERROR("We need spectral basis for exp. TDH in order to partition into occ. and vir. modals.");
   }

   // Check that the screening threshold is positive or zero
   if (  this->mScreenZeroCoef < C_0
      )
   {
      MIDASERROR("TDH screening threshold must be > 0");
   }

   // Check that the kappa threshold makes sense
   if (  this->mKappaNormThreshold > C_1
      && this->mParametrization == midas::tdh::tdhID::EXPONENTIAL
      )
   {
      MIDASERROR("Kappa-norm threshold should be between 0 and 1!");
   }

   // Checks on TdPropertyDef
   TdPropertyDef::SanityCheck(this->mOdeInfo, this->mImagTime);
}

/**
 * Read OdeInfo
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next keyword?
 **/
bool TdHCalcDef::ReadOdeInfo
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Overwrite mOdeInfo
   return OdeInput(arInp, arS, 4, this->mOdeInfo);
}

/**
 * Read constraint operator
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdHCalcDef::ReadLinearTdHConstraint
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   static const std::map<std::string, midas::tdh::constraintID> constraint_map =
   {  {"ZERO", midas::tdh::constraintID::ZERO}
   ,  {"FOCK", midas::tdh::constraintID::FOCK}
   };

   midas::input::GetLine(arInp, arS);
   
   this->mLinearTdHConstraint = constraint_map.at(midas::input::ToUpperCase(arS));

   // We have not read the next keyword
   return false;
}

/**
 * Read policy for initial wave function
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next line?
 **/
bool TdHCalcDef::ReadInitialWfPolicy
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   midas::input::GetLine(arInp, arS);
   auto svec = midas::util::StringVectorFromString(arS);
   assert(!svec.empty());
   auto policy = midas::input::ToUpperCase(svec[0]);

   if (  policy == "VSCF"
      )
   {
      assert(svec.size() == 2);

      this->mInitialWavePacket = initType::VSCF;
      this->mVscfName = svec[1];

      Mout  << " TdHCalcDef: Set VSCF name to '" << this->mVscfName << "'." << std::endl;
   }
   else
   {
      MIDASERROR("Unrecognized initialization policy: " + policy);
   }

   return false;
}

#ifdef VAR_MPI
/**
 * Read method for communicating between MPI ranks
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next line?
 **/
bool TdHCalcDef::ReadMpiComm
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   midas::input::GetLine(arInp, arS);

   static const std::map<std::string, midas::tdh::mpiComm> comm_map =
   {  {"BCAST", midas::tdh::mpiComm::BCAST}
   ,  {"IBCAST", midas::tdh::mpiComm::IBCAST}
   };

   this->mMpiComm = comm_map.at(arS);

   return false;
}
#endif /* VAR_MPI */

/**
 * Ostream overload for TdHCalcDef
 *
 * @param aOs
 * @param aTcd
 * @return
 *    os
 **/
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const TdHCalcDef& aTcd
   )
{
   aOs << "TdHCalcDef::" << aTcd.GetName() << std::endl;
   return aOs;
}

} /* namespace midas::input */
