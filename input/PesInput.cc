/**
************************************************************************
* 
* @file                PesInput.cc
*
* 
* Created:             19-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the pes midas module 
* 
* Last modified:       24-11-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "test/Test.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/FileSystem.h"
#include "util/conversions/FromString.h"
#include "input/PesCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FitBasisInput.h"
#include "input/FindKeyword.h"
#include "input/Trim.h"
#include "input/GetLine.h"
#include "input/InputReader.h"
#include "input/ProgramSettings.h"

#include "mpi/Impi.h"

#include "input/FlexCoupCalcDef.h"
string FlexCoupInput(std::istream&,FlexCoupCalcDef&, In);

// using declarations
using std::vector;
using std::map;
using std::transform;
using std::find;
using std::string;
using std::make_pair;

// forward declarations
namespace midas
{
namespace input
{
input_map_t SymmetryThresholdsMap(In, SymmetryThresholds&);
} /* namespace input */
} /* namespace midas */

using namespace midas::input;

/**
 * Read and check Pes input.
 * Read until next s = "#..." which is returned.
 **/
std::string PesInput
   (  istream& Minp
   ,  bool passive
   ,  std::string aInputFlag
   )
{
   if (gDoPes)
   {
      Mout << " Warning! I have read Pes input once " << std::endl;
   }

   gDoPes = true; 
   enum INPUT
      {  ERROR
      ,  IOLEVEL
      ,  NONORMALCOORD
      ,  CARTESIANCOORD
      ,  SAVEITOPERFILE
      ,  SAVEDISPGEOM
      ,  DUMPSPINTERVAL
      ,  ASSIGNBASIS
      ,  AVOIDRESTART
      ,  DISPLACEMENT
      ,  TAYLORINFO
      ,  STATICINFO
      ,  THRESFREQ
      ,  DIATOMIC
      ,  GENHESSIAN
      ,  VIBPOL
      ,  EXCITEDSTATE
      ,  GRIDMAXDIM
      ,  ADGAGRIDINITIALDIM
      ,  ANALYZESTATES
      ,  PESGRIDSCALFAC
      ,  EXTENDEDGRIDSETTINGS
      ,  INTERPOLATE
      ,  PESFGMESH
      ,  PESFGSCALFAC
      ,  PESUSERDEFINEDBOUND
      ,  FREQSCALCOORDINFIT
      ,  NOSCALCOORDINFIT
      ,  SCALCOORDINFIT
      ,  CALCEFFINERTIAINVTENS
      ,  DISABLECORIOLISANDINERTIA
      ,  PESPLOTALL
      ,  PESPLOTREALESP
      ,  PESPLOTSELECTED
      ,  CUTANALYSIS
      ,  CUTANALYSISMESH
      ,  INERTIALFRAME
      ,  ARNTWOMODELPOT
      ,  MORSEPOT
      ,  MORSEDEFS
      ,  DOUBLEWPOT
      ,  DOUBLEWDEFS
      ,  ADGAINFO
      ,  NITERMAX
      ,  ANALYZEDENS
      ,  ITPOTTHR
      ,  ITDENSTHR
      ,  ITVDENSTHR
      ,  ITNORMVDENSTHR
      ,  ITRESDENSTHR
      ,  DYNAMICADGAEXT
      ,  DOEXTINFIRSTITER
      ,  GLQUADRATUREPOINTS
      ,  POLYINTERPOLATIONPOINTS
      ,  ITRESENTHR
      ,  ITNORMRESENTHR
      ,  ADGARELSCALFACT
      ,  ADGAABSSCALFACT
      ,  DONOTSTOREPROPINMEM
      ,  CALCDENSATEACHMCL
      ,  DOADGAEXTATEACHMCL
      ,  UPDMEANDENSATEACHMCL
      ,  POTFROMOPFILE
      ,  ITADAPTIVSEARCH
      ,  ITEXPGRIDSCALFACT
      ,  PLOTADGA2D
      ,  ADGAPROP
      ,  ADGAPROPWEIGHT
      ,  BARFILESTORAGETYPE
      ,  BRENNERPOT
      ,  MULTILEVELPES
      ,  DOUBLEINCREMENTAL
      ,  ICMODESSCREENTHR
      ,  LEVELMC
      ,  MEANDENSONFILES
      ,  LINEARCOMB
      ,  BOUNDARIESPREOPT
      ,  NTHREADS
      ,  EXTRAPOLATE
      ,  EXTRAPONLYGRADIENT
      ,  SCREENMODECOMBI
      ,  SYMMETRY
      ,  SYMTHR
      ,  NOSCREENSYMPOTTERMS
      ,  ADGAFGSTEP
      ,  ADGAMSICUT
      ,  MSILAMBDA
      ,  TESTPOTENTIAL
      ,  NODERIVOFGDET
      ,  GMATDIAGONAL
      ,  GMATCONST
      ,  ADDPSUEDOPOTTOEN
      ,  SPECIFYFITBASIS
      ,  FITBASIS
      ,  USEFITBASIS
      ,  FLEXCOUP
      ,  HISTORICADGAPES
      ,  HISTORICADGASCALING
      ,  HISTORICADGACONV
      ,  HISTORICADGAGRID
      ,  HISTORICPESNUMSOURCE
      ,  ADGAUSEHISTORICNONSOPINTEGRALS
      ,  SCREENOPERCOEF
      ,  SPSCRATCHDIRPREFIX
      ,  POLYSPHERICALCOORD
      ,  SAVEPSCGEOMSCRATCHDIR
      ,  PSCMAXGRIDBOUNDS
      ,  EXTMCRMODES
      ,  SAVETRAINDATA
      ,  MLDRIVER
      ,  MLPES
      ,  MLREMOVEINITSET
      ,  NORMALCOORDINATETHRESHOLD
      ,  NORMALIZENORMALCOORDINATES
      ,  ORTHONORMALIZENORMALCOORDINATES
      ,  TRUNCATENEARZEROPROPVAL
      };
   const std::map<std::string, INPUT> input_word =
      {  {"#2USEFITBASIS",USEFITBASIS}
      ,  {"#2FITBASIS",FITBASIS}
      ,  {"#2IOLEVEL",IOLEVEL}
      ,  {"#2NONORMALCOORD",NONORMALCOORD}
      ,  {"#2CARTESIANCOORD",CARTESIANCOORD}
      ,  {"#2DUMPSPINTERVAL",DUMPSPINTERVAL}
      ,  {"#2SAVEITOPERFILE",SAVEITOPERFILE}
      ,  {"#2SAVEDISPGEOM",SAVEDISPGEOM}
      ,  {"#2ASSIGNBASIS",ASSIGNBASIS}
      ,  {"#2AVOIDRESTART",AVOIDRESTART}
      ,  {"#2DISPLACEMENT",DISPLACEMENT}
      ,  {"#2TAYLORINFO",TAYLORINFO}
      ,  {"#2STATICINFO",STATICINFO}
      ,  {"#2THRESFREQ",THRESFREQ}
      ,  {"#2DIATOMIC",DIATOMIC}
      ,  {"#2GENHESSIAN",GENHESSIAN}
      ,  {"#2VIBPOL",VIBPOL}
      ,  {"#2EXCITEDSTATE",EXCITEDSTATE}
      ,  {"#2GRIDMAXDIM",GRIDMAXDIM}
      ,  {"#2ADGAGRIDINITIALDIM",ADGAGRIDINITIALDIM}
      ,  {"#2ANALYZESTATES",ANALYZESTATES}
      ,  {"#2PESGRIDSCALFAC",PESGRIDSCALFAC}
      ,  {"#2EXTENDEDGRIDSETTINGS",EXTENDEDGRIDSETTINGS}
      ,  {"#2INTERPOLATE",INTERPOLATE}
      ,  {"#2PESFGMESH",PESFGMESH}
      ,  {"#2PESFGSCALFAC",PESFGSCALFAC}
      ,  {"#2PESUSERDEFINEDBOUND",PESUSERDEFINEDBOUND}
      ,  {"#2FREQSCALCOORDINFIT",FREQSCALCOORDINFIT}
      ,  {"#2NOSCALCOORDINFIT",NOSCALCOORDINFIT}
      ,  {"#2SCALCOORDINFIT",SCALCOORDINFIT}
      ,  {"#2CALCEFFINERTIAINVTENS",CALCEFFINERTIAINVTENS}
      ,  {"#2DISABLECORIOLISANDINERTIA",DISABLECORIOLISANDINERTIA}
      ,  {"#2PESPLOTALL",PESPLOTALL}
      ,  {"#2PESPLOTREALESP",PESPLOTREALESP}
      ,  {"#2PESPLOTSELECTED",PESPLOTSELECTED}
      ,  {"#2INERTIALFRAME",INERTIALFRAME}
      ,  {"#2ARNTWOMODELPOT",ARNTWOMODELPOT}
      ,  {"#2TESTPOTENTIAL",TESTPOTENTIAL}
      ,  {"#2MORSEPOTENTIAL",MORSEPOT}
      ,  {"#2BRENNERPOTENTIAL",BRENNERPOT}
      ,  {"#2MORSEDEFS",MORSEDEFS}
      ,  {"#2DOUBLEWELLPOT",DOUBLEWPOT}
      ,  {"#2DOUBLEWELLDEFS",DOUBLEWDEFS}
      ,  {"#2CUTANALYSIS",CUTANALYSIS}
      ,  {"#2CUTANALYSISMESH",CUTANALYSISMESH}
      ,  {"#2ADGAINFO",ADGAINFO}
      ,  {"#2NITERMAX",NITERMAX}
      ,  {"#2ANALYZEDENS",ANALYZEDENS}
      ,  {"#2ITPOTTHR",ITPOTTHR}
      ,  {"#2ITDENSTHR",ITDENSTHR}
      ,  {"#2ITVDENSTHR",ITVDENSTHR}
      ,  {"#2ITNORMVDENSTHR",ITNORMVDENSTHR}
      ,  {"#2ITRESDENSTHR",ITRESDENSTHR}
      ,  {"#2DYNAMICADGAEXT",DYNAMICADGAEXT}
      ,  {"#2DOEXTINFIRSTITER",DOEXTINFIRSTITER}
      ,  {"#2CALCDENSATEACHMCL",CALCDENSATEACHMCL}
      ,  {"#2DOADGAEXTATEACHMCL",DOADGAEXTATEACHMCL}
      ,  {"#2UPDMEANDENSATEACHMCL",UPDMEANDENSATEACHMCL}
      ,  {"#2GLQUADRATUREPOINTS",GLQUADRATUREPOINTS}
      ,  {"#2POLYINTERPOLATIONPOINTS",POLYINTERPOLATIONPOINTS}
      ,  {"#2ITRESENTHR",ITRESENTHR}
      ,  {"#2ITNORMRESENTHR",ITNORMRESENTHR}
      ,  {"#2ADGARELSCALFACT",ADGARELSCALFACT}
      ,  {"#2ADGAABSSCALFACT",ADGAABSSCALFACT}
      ,  {"#2PLOTADGA2D",PLOTADGA2D}
      ,  {"#2ADGAPROP",ADGAPROP}
      ,  {"#2ADGAPROPWEIGHT",ADGAPROPWEIGHT}
      ,  {"#2BARFILESTORAGETYPE",BARFILESTORAGETYPE}
      ,  {"#2DONOTSTOREPROPINMEM",DONOTSTOREPROPINMEM}
      ,  {"#2POTVALSFROMOPFILE",POTFROMOPFILE}
      ,  {"#2ITADAPTIVSEARCH",ITADAPTIVSEARCH}
      ,  {"#2ITEXPGRIDSCALFACT",ITEXPGRIDSCALFACT}
      ,  {"#2MULTILEVELPES",MULTILEVELPES}
      ,  {"#2DOUBLEINCREMENTAL",DOUBLEINCREMENTAL}
      ,  {"#2ICMODESSCREENTHR",ICMODESSCREENTHR}
      ,  {"#2LEVELMC",LEVELMC}
      ,  {"#2MEANDENSONFILES",MEANDENSONFILES}
      ,  {"#2LINEARCOMB",LINEARCOMB}
      ,  {"#2NTHREADS",NTHREADS}
      ,  {"#2BOUNDARIESPREOPT",BOUNDARIESPREOPT}
      ,  {"#2EXTRAPOLATE",EXTRAPOLATE}
      ,  {"#2EXTRAPONLYGRADIENT",EXTRAPONLYGRADIENT}
      ,  {"#2SCREENMODECOMBI",SCREENMODECOMBI}
      ,  {"#2SYMMETRY",SYMMETRY}
      ,  {"#2SYMTHR",SYMTHR}
      ,  {"#2NOSCREENSYMPOTTERMS",NOSCREENSYMPOTTERMS}
      ,  {"#2ADGAFGSTEP",ADGAFGSTEP}
      ,  {"#2ADGAMSICUT",ADGAMSICUT}
      ,  {"#2MSILAMBDA",MSILAMBDA}
      ,  {"#2NODERIVOFGDET",NODERIVOFGDET}
      ,  {"#2GMATDIAGONAL",GMATDIAGONAL}
      ,  {"#2GMATCONST",GMATCONST}
      ,  {"#2ADDPSUEDOPOTTOEN",ADDPSUEDOPOTTOEN}
      ,  {"#2SPECIFYFITBASIS",SPECIFYFITBASIS}
      ,  {"#2FLEXCOUP",FLEXCOUP}
      ,  {"#2HISTORICADGAPES",HISTORICADGAPES}
      ,  {"#2HISTORICADGASCALING",HISTORICADGASCALING}
      ,  {"#2HISTORICADGACONV",HISTORICADGACONV}
      ,  {"#2HISTORICADGAGRID",HISTORICADGAGRID}
      ,  {"#2HISTORICPESNUMSOURCE",HISTORICPESNUMSOURCE}
      ,  {"#2SCREENOPERCOEF",SCREENOPERCOEF}
      ,  {"#2ADGAUSEHISTORICNONSOPINTEGRALS",ADGAUSEHISTORICNONSOPINTEGRALS}
      ,  {"#2SPSCRATCHDIRPREFIX",SPSCRATCHDIRPREFIX}
      ,  {"#2POLYSPHERICALCOORD",POLYSPHERICALCOORD}
      ,  {"#2SAVEPSCGEOMSCRATCHDIR",SAVEPSCGEOMSCRATCHDIR}
      ,  {"#2PSCMAXGRIDBOUNDS",PSCMAXGRIDBOUNDS}
      ,  {"#2EXTMCRMODES",EXTMCRMODES}
      ,  {"#2MLDRIVER",MLDRIVER}
      ,  {"#2SAVETRAINDATA",SAVETRAINDATA}
      ,  {"#2MLPES",MLPES}
      ,  {"#2MLREMOVEINITSET",MLREMOVEINITSET}
      ,  {"#2NORMALCOORDINATETHRESHOLD" , NORMALCOORDINATETHRESHOLD}
      ,  {"#2NORMALIZENORMALCOORDINATES", NORMALIZENORMALCOORDINATES}
      ,  {"#2ORTHONORMALIZENORMALCOORDINATES", ORTHONORMALIZENORMALCOORDINATES}
      ,  {"#2TRUNCATENEARZEROPROPVAL", TRUNCATENEARZEROPROPVAL}
      };

   std::string s;
   bool dalton_local_bool = false;
   bool already_read = false;
   In input_level = 2;
   // readin until input levels point up
   while ((already_read || GetLine(Minp,s)) && !(s.substr(I_0,I_1) == "#" && atoi(s.substr(I_1,I_1).c_str()) < input_level))          
   {
      already_read = false;
      
      // Output which PES specific keywords have been read
      if (gPesIoLevel > I_1)
      {
         Mout << " Reading : " << s << std::endl;
      }

      // If passive loop simple to next #
      if (passive)
      {
         continue;
      }
      std::string s_orig = s;
      // Transform to all upper
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
      // Delete ALL blanks
      while (s.find(" ") != s.npos)
      {
         s.erase(s.find(" "), I_1);
      }
      INPUT input = midas::input::FindKeyword(input_word,s);
      switch(input)
      {
         case IOLEVEL:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> gPesIoLevel;
            break;
         }
         case FITBASIS:
         {
            already_read = true;
            s = midas::input::ReadFitFunction(Minp, midas::input::gProgramSettings.GetInputFilePath(), 3);
            break;
         }
         case USEFITBASIS:
         {
            GetLine(Minp, s);
            gPesCalcDef.SetmPesFitBasisName(s);
            break;
         }
         case SPECIFYFITBASIS:
         {
            gPesCalcDef.SetmSpecBas(true);
            break;
         }
         case ADDPSUEDOPOTTOEN:
         {
            gPesCalcDef.SetmAddPsuedoPotToEn(true);
            break;
         }
         case GMATDIAGONAL: 
         {
            gPesCalcDef.SetmGMatDiagonal(true);
            break;
         }
         case GMATCONST:
         {
            gPesCalcDef.SetmGMatConst(true);
            break;
         }
         case NODERIVOFGDET:
         {
            gPesCalcDef.SetmNoDerivOfGDet(true);
            break;
         }
         case NONORMALCOORD:
         {
            gPesCalcDef.SetmNormalcoordInPes(false);
            break;
         }
         case CARTESIANCOORD:
         {
            gPesCalcDef.SetmNormalcoordInPes(false);
            gPesCalcDef.SetmCartesiancoordInPes(true);
            
            gPesCalcDef.SetmPesCalcCoriolisAndInertia(false);
            break;
         }
         case POLYSPHERICALCOORD:
         {
            gPesCalcDef.SetmNormalcoordInPes(false);
            gPesCalcDef.SetmCartesiancoordInPes(false);
            gPesCalcDef.SetmPscInPes(true);

            GetLine(Minp, s);
            gPesCalcDef.SetmTanaRunFile(s);
            break;
         }
         case PSCMAXGRIDBOUNDS:
         {
            midas::input::GetLine(Minp, s);
            gPesCalcDef.SetmPscMaxGridBounds(midas::util::VectorFromString<Nb>(s));
            break;
         }
         case EXTENDEDGRIDSETTINGS:
         {
            already_read = gPesCalcDef.ReadExtendedGridSettings(Minp, s);
            break;
         }
         case PESUSERDEFINEDBOUND:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            string s_new;
            while (input_s >> s_new)
            {
               gPesCalcDef.PushBackmPesUserDefinedBound(midas::util::FromString<Nb>(s_new));
            }
            break;
         }
         case SAVEITOPERFILE:
         {
            gPesCalcDef.SetmSaveItOperFile(true);
            break;
         }
         case SAVEPSCGEOMSCRATCHDIR:
         {
            gPesCalcDef.SetmSavePscGeomScrDir(true);
            break;
         }
         case SAVEDISPGEOM:
         {
            gPesCalcDef.SetmSaveDispGeom(true);

            midas::input::GetLine(Minp, s);

            if(midas::input::IsKeyword(s))
            {
               already_read = true;
            }
            else
            {
               gPesCalcDef.SetSaveDispGeomFormat(s);
            }
            break;
         }
         case SAVETRAINDATA:
         {
            gPesCalcDef.SetmSaveTrainData(true);
            break;
         }
         case MLDRIVER:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            std::string mltask;
            input_s >> mltask;
            gPesCalcDef.SetmMLDriver(mltask);
            break;
         }
         case DUMPSPINTERVAL:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmDumpSpInterval(s);
            break;
         }
         case CUTANALYSIS:
         {
            GetLine(Minp,s);
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
            while (!(s.find("ENDOFCUTANALYSIS") != s.npos))
            {
                gPesCalcDef.PushBackmMakeCutAnalysis(s);
                GetLine(Minp,s);
                transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);
            }
            break;
         }
         case CUTANALYSISMESH:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmCutMesh(s);
            break;
         }
         case AVOIDRESTART:
         {
            gPesCalcDef.SetmPesAvoidRestart(true);
            break;
         }
         case DISPLACEMENT:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);
            if (s.find("SIMPLEDISPLACEMENT") != s.npos)
            {
               gPesCalcDef.SetmPesDispTypeSIMPLE();
               GetLine(Minp,s);
               istringstream input_s(s);
               gPesCalcDef.SetmPesStepSizeInAu(s);
            }
            else if (s.find("FREQDISPLACEMENT") != s.npos)
            {
               gPesCalcDef.SetmPesDispTypeFREQ();
               GetLine(Minp,s);
               istringstream input_s(s);
               gPesCalcDef.SetmPesStepSizeInAu(s);
            }
            else
            {
               Mout << " No Information of displacements given under PES input ";
               MIDASERROR(" I stop ...");
            }  
            break;
         }
         case TAYLORINFO:
         {
            gPesCalcDef.SetmPesTaylor(true);
            
            GetLine(Minp,s);                                  
            istringstream input_s(s);
            gPesCalcDef.SetTaylorInfo(s);
            break;
         }
         case STATICINFO:
         {
            gPesCalcDef.SetmPesGrid(true);
            gPesCalcDef.SetmPesStatic(true);
            
            std::vector<In> local_vector;
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesNumMCR(s);
            
            GetLine(Minp,s);
            istringstream input1_s(s);
            std::string s_new;
            while (input1_s >> s_new)
            {
               gPesCalcDef.PushBackmPesGridInMCLevel(midas::util::FromString<In>(s_new));
            }

            GetLine(Minp,s);
            istringstream input2_s(s);
            while (input2_s >> s_new)
            {
               gPesCalcDef.PushBackmPesFracInMCLevel(s_new);
            }

            break;
         }
         case THRESFREQ:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmNumDerFreqThres(s);
            break;
         }
         case DIATOMIC:
         {
            gPesCalcDef.SetmPesDiatomic(true);
            gPesCalcDef.SetmPesCalcCoriolisAndInertia(false);
            break;
         }
         case GENHESSIAN:
         {
            In NoOfFiles = I_0;
            GetLine(Minp,s);
            istringstream input_s(s);
            input_s >> NoOfFiles;
            if (NoOfFiles > I_0)
            {
               for (In i=I_0;i<NoOfFiles;i++)
               {
                  string key;
                  GetLine(Minp,s);
                  istringstream input_s(s);
                  input_s >> key;
                  transform(key.begin(),key.end(),key.begin(),(In(*) (In))toupper);
                  gPesCalcDef.InsertmList_Of_Hessians(key);
               }
            }
            break;
         }
         case VIBPOL:
         {
            gPesCalcDef.SetmPesVibPol(true);
            gPesCalcDef.InsertmList_Of_Hessians("X_DIPOLE");
            gPesCalcDef.InsertmList_Of_Hessians("Y_DIPOLE");
            gPesCalcDef.InsertmList_Of_Hessians("Z_DIPOLE");
            break;
         }
         case EXCITEDSTATE:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesExcState(s);
            break;
         }
         case GRIDMAXDIM:
         {
            GetLine(Minp, s);
            gPesCalcDef.SetmPesGridMaxDim(midas::util::FromString<In>(s));
            break;
         }
         case ADGAGRIDINITIALDIM:
         {
            GetLine(Minp, s);
            gPesCalcDef.SetmPesGridInitialDim(midas::util::FromString<In>(s));
            break;
         }
         case ANALYZESTATES:
         {
            GetLine(Minp, s);
            gPesCalcDef.SetmAnalyzeStates(midas::util::VectorFromString<In>(s));
            break;
         }
         case PESGRIDSCALFAC:
         {
            gPesCalcDef.ClearmPes_Grid_Scal();
            
            GetLine(Minp, s);
            std::istringstream input_s(s);
            std::string s_new;
            while (input_s >> s_new)
            {
               gPesCalcDef.PushBackmPes_Grid_Scal(midas::util::FromString<Nb>(s_new));
            }
            break;
         }
         case INTERPOLATE:
         {
            midas::input::GetParsedLine(Minp, s);
            auto int_type = s;
            gPesCalcDef.SetmIntType(int_type);

            midas::input::GetParsedLine(Minp, s);
            gPesCalcDef.SetmIntSurface(s);

            if (int_type == "SPLINE")
            {
               midas::input::GetParsedLine(Minp, s);
               gPesCalcDef.SetmSplineTypeFromString(s);
            }
            else if (int_type == "SHEPARD")
            {
               midas::input::GetLine(Minp, s);
               auto shepard_params = midas::util::VectorFromString<In>(s);
               
               if (shepard_params.size() != I_2)
               {
                  MIDASERROR("Need both order and exponent value for modified Shepard interpolation");
               }

               gPesCalcDef.SetmShepardParms(shepard_params);
            }
            else
            {
               MIDASERROR("Did not recognize the interpolation type");
            }
            break;
         }
         case PESFGMESH:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            string s_new;
            while (input_s >> s_new)
            {
               In x = InFromString(s_new);
               gPesCalcDef.PushBackmPesFGMesh(x);
            }
            break;
         }

         case PESFGSCALFAC:
         {
            gPesCalcDef.ClearmPesFGScalFact();

            GetLine(Minp, s);
            std::istringstream input_s(s);
            std::string s_new;
            while (input_s >> s_new)
            {
               Nb x = NbFromString(s_new);
               gPesCalcDef.PushBackmPesFGScalFact(x);
            }
            break;
         }
         case PESPLOTALL:
         {
            gPesCalcDef.SetmPesPlotAll(true);
            break;
         }
         case PESPLOTREALESP:
         {
            gPesCalcDef.SetmPesPlotRealESP(true);
            break;
         }
         case PESPLOTSELECTED:
         {
            gPesCalcDef.SetmPesPlotSelected(true);
            GetLine(Minp, s);
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
            
            while (!(s.find("ENDOFSELECTION") != s.npos) )
            {
               gPesCalcDef.PushBackmPesPlotSelectedInfo(s);
               GetLine(Minp,s);
               transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
            }
            break;
         }
         case FREQSCALCOORDINFIT:
         {
            gPesCalcDef.SetmPesFreqScalCoordInFit(true);
            break;
         }
         case NOSCALCOORDINFIT:
         {
            gPesCalcDef.SetmPesFreqScalCoordInFit(false);
            gPesCalcDef.SetmPesScalCoordInFit(false);
            break;
         }
         case SCALCOORDINFIT:
         {
            gPesCalcDef.SetmPesScalCoordInFit(true);
            break;
         }
         case CALCEFFINERTIAINVTENS:
         {
            gPesCalcDef.SetmPesCalcMuTens(true);
            gPesCalcDef.SetmPesCalcCoriolisAndInertia(false);
            break;
         }
         case DISABLECORIOLISANDINERTIA:
         {
            gPesCalcDef.SetmPesCalcCoriolisAndInertia(false);
            break;
         }
         case INERTIALFRAME:
         {
            gPesCalcDef.SetmPesInertialFrame(true);
            break;
         }
         case ADGAINFO:
         {
            gPesCalcDef.SetmPesGrid(true);
            gPesCalcDef.SetmPesAdga(true);
            
            GetLine(Minp, s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesNumMCR(s);
      
            // Need to establish if the next line is also connected to this keyword or not
            auto pos = Minp.tellg();
            midas::input::GetLine(Minp, s);

            // If the next line is not a keyword, then read it in
            if (midas::input::NotKeyword(s))
            {
               gPesCalcDef.SetmAdgaVscfNames(midas::util::VectorFromString<std::string>(s));
            }
            // If the next line is a keyword, then return the input stream to the correct position
            else
            {
               Minp.seekg(pos, std::ios::beg);
            }

            break;
         }
         case NITERMAX:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesIterMax(s);
            break;
         }
         case ANALYZEDENS:
         {
            GetLine(Minp, s);
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
            gPesCalcDef.SetmAnalyzeDensMethod(s);
            
            break;
         }
         case ITPOTTHR:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesItPotThr(s);
            gPesCalcDef.SetmPesUseItPotThr(true);
            break;
         }
         case ITDENSTHR:
         {        
            GetLine(Minp,s);
            gPesCalcDef.SetmPesItDensThr(s);
            gPesCalcDef.SetmPesUseItDensThr(true);
            break;                                
         }                                                 
         case ITVDENSTHR:
         {        
            GetLine(Minp,s);
            gPesCalcDef.SetmPesItVdensThr(s);
            gPesCalcDef.SetmPesUseItVdensThr(true);
            break;                                
         }    
         case ITNORMVDENSTHR:
         {
            gPesCalcDef.SetmPesUseItVdensThr(false);
            gPesCalcDef.SetmPesUseItNormVdensThr(true);

            GetLine(Minp,s);
            gPesCalcDef.SetmPesItNormVdensThr(s);
            break;
         }
         case ITRESENTHR:
         {                          
            GetLine(Minp,s);                            
            gPesCalcDef.SetmPesItResEnThr(s);
            gPesCalcDef.SetmPesUseItVdensThr(true);
            break;                                                                          
         }                                                                          
         case ITNORMRESENTHR:
         {                          
            GetLine(Minp,s);                            
            gPesCalcDef.SetmPesItNormResEnThr(s);
            gPesCalcDef.SetmPesUseItNormVdensThr(true);
            break;                                                                          
         }                                                                          
         case ITRESDENSTHR:
         {        
            GetLine(Minp,s);
            gPesCalcDef.SetmPesItResDensThr(s);
            break;                                
         }    
         case DYNAMICADGAEXT:
         {
            gPesCalcDef.SetmDynamicAdgaExt(true);
            break;
         }
         case DOEXTINFIRSTITER:
         {
            gPesCalcDef.SetmDoExtInFirstIter(true);
            break;
         }
         case CALCDENSATEACHMCL:
         {
            gPesCalcDef.SetmCalcDensAtEachMcl(true);
            break;
         }
         case DOADGAEXTATEACHMCL:
         {
            gPesCalcDef.SetmDoAdgaExtAtEachMcl(true);
            break;
         }
         case UPDMEANDENSATEACHMCL:
         {
            gPesCalcDef.SetmCalcDensAtEachMcl(true);
            gPesCalcDef.SetmUpdMeanDensAtEachMcl(true);
            break;
         }
         case GLQUADRATUREPOINTS:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesGLQuadPnts(s);
            break;
         }
         case POLYINTERPOLATIONPOINTS:
         {
            midas::input::GetLine(Minp, s);
            gPesCalcDef.SetmPolyInterPoints(midas::util::FromString<In>(s)); 

            break;
         }
         case ADGARELSCALFACT:
         {
            midas::input::GetLine(Minp, s);
            gPesCalcDef.SetmAdgaRelConvScalFacts(midas::util::VectorFromString<Nb>(s));

            break;
         }
         case ADGAABSSCALFACT:
         {
            midas::input::GetLine(Minp, s);
            gPesCalcDef.SetmAdgaAbsConvScalFacts(midas::util::VectorFromString<Nb>(s));

            break;
         }
         case POTFROMOPFILE:
         {
            gPesCalcDef.SetmPesCalcPotFromOpFile(true);
            break;
         }
         case ITADAPTIVSEARCH:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesAdaptivSearch(true);
            gPesCalcDef.SetmPesNmaxSplittings(s);
            break;
         }
         case ADGAPROP:
         {
            gPesCalcDef.SetmUseMultiStateAdga(true);

            GetLine(Minp, s);
            gPesCalcDef.SetmPesAdaptiveProps(s);
            break;
         }
         case ADGAPROPWEIGHT:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesAdaptivePropsWeights(s);
            break;
         }
         case BARFILESTORAGETYPE:
         {
            GetLine(Minp, s);
            istringstream input_s(s);
            std::string s_new;
            input_s >> s_new;
            transform(s_new.begin(), s_new.end(), s_new.begin(), (In(*) (In))toupper);

            gPesCalcDef.SetmPesBarFileStorageType(s_new);
            break;
         }
         case PLOTADGA2D:
         {
            gPesCalcDef.SetmPesPlotAdga2D(true);
            break;
         }
         //case AMMMODELPOT:
         //{
         //   gPesCalcDef.SetmPesModelPot(true);
         //   gPesCalcDef.SetmPesDoAmmModelPot(true);    
         //   GetLine(Minp,s);
         //   gPesCalcDef.SetmPesType(s);
         //   break;
         //}
         case TESTPOTENTIAL:
         {
            gPesCalcDef.SetmPesModelPot(true);
            gPesCalcDef.SetmPesDoTestPot(true);
            break;
         }
         case ARNTWOMODELPOT:
         {  
            gPesCalcDef.SetmPesModelPot(true);
            gPesCalcDef.SetmPesDoArNtwoModelPot(true);
            //read the pes #
            GetLine(Minp,s);
            gPesCalcDef.SetmPesType(s);
            break;
         }  
         case MORSEPOT:
         {  
            gPesCalcDef.SetmPesModelPot(true);
            gPesCalcDef.SetmPesDoMorsePot(true);
            break;
         }  
         case BRENNERPOT:
         {  
            gPesCalcDef.SetmPesModelPot(true);
            gPesCalcDef.SetmPesDoBrennerPot(true);
            break;
         }  
         case MORSEDEFS:
         {
            GetLine(Minp,s);
            istringstream input_s(s);
            gPesCalcDef.SetmPesMorseDefs(s);
            break;
         }
         case DOUBLEWPOT:
         {  
            gPesCalcDef.SetmPesModelPot(true);
            gPesCalcDef.SetmPesDoDoubleWpot(true);
            break; 
         }     
         case DOUBLEWDEFS:
         {        
            GetLine(Minp,s);
            gPesCalcDef.SetmPesDoubleWdefs(s);
            break;
         }        
         case DONOTSTOREPROPINMEM:
         {
            gPesCalcDef.SetmPesPropInMem(false);
            break;
         }
         case ITEXPGRIDSCALFACT:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesItGridExpScalFact(s);
            break;
         }
         case MULTILEVELPES:
         {
            already_read = gPesCalcDef.ReadMultiLevelPes(Minp, s); // we have already read next keyword 
           break;
         }
         case DOUBLEINCREMENTAL:
         {
            gPesCalcDef.SetmDincrSurface(true);
            
            gPesCalcDef.SetmNormalcoordInPes(false);
            gPesCalcDef.SetmFalconInPes(true);

            midas::input::GetParsedLine(Minp, s);
            auto double_incremental_method = s;
            gPesCalcDef.SetmDincrMethod(double_incremental_method);
            
            midas::input::GetLine(Minp, s);
            auto fragment_combination_info = s;
            gPesCalcDef.SetmFcInfo(fragment_combination_info);
      
            // Need to establish if the next line is also connected to this keyword or not
            auto pos = Minp.tellg();
            midas::input::GetParsedLine(Minp, s);
            // If the next line is not a keyword, then read it in
            if (midas::input::NotKeyword(s))
            {
               auto dincr_surface_type = s;
               gPesCalcDef.SetmDincrSurfaceType(s);
            }
            // If the next line is a keyword, then return the input stream to the correct position
            else
            {
               Minp.seekg(pos, std::ios::beg);
            }
            break;
         }
         case ICMODESSCREENTHR:
         {
            midas::input::GetLine(Minp, s);
            gPesCalcDef.SetmIcModesScreenThr(midas::util::VectorFromString<Nb>(s));
            
            break;
         }
         case NTHREADS:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetNThreads(midas::util::VectorFromString<Uin>(s));
            break;
         }
         case BOUNDARIESPREOPT:
         {
            already_read = gPesCalcDef.ReadBoundariesPreOpt(Minp, s);
            break;
         }
         case MEANDENSONFILES:
         {
            gPesCalcDef.SetmMeanDensOnFiles(true);
            break;
         }
         case EXTRAPOLATE:
         {
            gPesCalcDef.SetmPesDoExtrap(true);
            
            GetLine(Minp,s);
            gPesCalcDef.SetmExtrapolateFrommExtrapolateTo(s);
           
            // Need to establish if the next line is also connected to this keyword or not
            auto pos = Minp.tellg();
            midas::input::GetLine(Minp, s);

            // If the next line is not a keyword, then read it in
            if (midas::input::NotKeyword(s))
            {
               if (midas::input::ParseInput(s) == "USEEXTRAPSTARTGUESS")
               {
                  gPesCalcDef.SetmUseExtrapStartGuess(true);
               }
               else
               {
                  MIDASERROR("Did not understand the instruction given under the #2 EXTRAPOLATE keyword, was given: " + s);
               }
            }
            // If the next line is a keyword, then return the input stream to the correct position
            else
            {
               Minp.seekg(pos, std::ios::beg);
            }
            break;
         }
         case EXTRAPONLYGRADIENT:
         {
            gPesCalcDef.SetmExtrapOnlyGradient(true);
            break;
         }
         case EXTMCRMODES:
         {
            gPesCalcDef.SetmUseExtMcr(true);

            midas::input::GetLine(Minp, s);
            auto vec = midas::util::VectorFromString<In>(s);
            gPesCalcDef.SetmExtMcrModes(std::set<In>(vec.begin(), vec.end()));

            break;
         }
         case LINEARCOMB:
         {
            gPesCalcDef.SetmPesDoLinearComb(true);
            GetLine(Minp,s);
            istringstream input_s(s);
            string s_new;
            while (input_s >> s_new)
            {
               Nb x = NbFromString(s_new);
               gPesCalcDef.PushBackmPesLinearComb(x);
            }
            break;
         }
         case SCREENMODECOMBI:
         {
            gPesCalcDef.SetmPesScreenModeCombi(true);
            GetLine(Minp,s);
            istringstream input_s(s);
            string s2;
            input_s >> s2;
            transform(s2.begin(),s2.end(),s2.begin(),(In(*) (In))toupper);
            if (s2.find("DISPLACEMENTS") == s2.npos && s2.find("GERBER")==s2.npos)
            {
               string error=" Input screening type " + s2 + "  unrecognized - ";
               MIDASERROR(error);
            }
            
            if (s2.find("DISPLACEMENTS")!=s2.npos) gPesCalcDef.SetmPesScreenMethod(I_0);
            else if (s2.find("GERBER")!=s2.npos) gPesCalcDef.SetmPesScreenMethod(I_1);

            GetLine(Minp,s);
            gPesCalcDef.SetmPesScreenThresh(s);
            GetLine(Minp,s);
            gPesCalcDef.SetmPesVibNumVec(midas::util::VectorFromString<In>(s));
            break;
         }
         case SYMMETRY:
         {
            gPesCalcDef.SetmUseSym(true);
            
            GetLine(Minp,s);
            istringstream input_s(s);
            std::string s2;
            input_s >> s2;
            transform(s2.begin(),s2.end(),s2.begin(),(In(*) (In))toupper);
            gPesCalcDef.SetmPesSymLab(s2);
            gPesCalcDef.SetmPesScreenSymPotTerms(true);

            // Read numeric thresholds for symmetry determination, if any.
            already_read = InputReader
               (  Minp
               ,  s
               ,  midas::input::SymmetryThresholdsMap
                  (  input_level + I_1
                  ,  gPesCalcDef.PointGroupSymThrs()
                  )
               );

            break;
         }
         case SYMTHR:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmSymThr(s);
            break;
         }
         case NOSCREENSYMPOTTERMS:
         {
            gPesCalcDef.SetmPesScreenSymPotTerms(false);
            break;
         }
         case ADGAFGSTEP:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesAdgaFgStep(s);
            break;
         }
         case ADGAMSICUT:
         {
            gPesCalcDef.SetmPesAdgaMsiCut(true);
            
            GetLine(Minp,s);
            gPesCalcDef.SetmPesAdgaMsiIt(midas::util::VectorFromString<In>(s));
            break;
         }
         case MSILAMBDA:
         {
            GetLine(Minp,s);
            gPesCalcDef.SetmPesMsiLambda(s);
            break;
         }
         case FLEXCOUP:
         {  
            FlexCoupCalcDef tmp_flex_coup_calc_def;
            s = FlexCoupInput(Minp, tmp_flex_coup_calc_def, input_level);
            gPesCalcDef.SetmFlexCoupCalcDef(tmp_flex_coup_calc_def);
            already_read = true;
            break;
         }
         case HISTORICADGAPES:
         {
            already_read = gPesCalcDef.ReadHistoricAdgaPes(Minp, s);
            break;
         }
         case HISTORICADGASCALING:
         {
            already_read = gPesCalcDef.ReadHistoricAdgaScaling(Minp, s);
            break;
         }
         case HISTORICADGACONV:
         {
            already_read = gPesCalcDef.ReadHistoricAdgaConv(Minp, s);
            break;
         }
         case HISTORICADGAGRID:
         {
            already_read = gPesCalcDef.ReadHistoricAdgaGrid(Minp, s);
            break;
         }
         case HISTORICPESNUMSOURCE:
         {
            already_read = gPesCalcDef.ReadHistoricPesNumSource(Minp, s);
            break;
         }
         case SCREENOPERCOEF:
         {
            already_read = gPesCalcDef.ReadScreenOperCoef(Minp, s);
            break;
         }
         case ADGAUSEHISTORICNONSOPINTEGRALS:
         {
            gPesCalcDef.SetAdgaUseHistoricNonSopIntegrals(true);
            break;
         }
         case SPSCRATCHDIRPREFIX:
         {
            already_read = gPesCalcDef.ReadSpScratchDirPrefix(Minp, s);
            break;
         }
         case MLPES:
         {
            midas::input::GetLine(Minp, s);
            In num = midas::util::FromString<In>(s);

            gPesCalcDef.SetMaxMLRuns(num);
            gPesCalcDef.SetMLPes(true);

            break;
         }
         case MLREMOVEINITSET:
         {
            gPesCalcDef.SetMLRemoveInitSet(true);
            break;
         }
         case NORMALCOORDINATETHRESHOLD:
         {
            midas::input::GetLine(Minp, s);
            midas::input::AssertNotKeyword(s);
            gPesCalcDef.SetNormalCoordinateThreshold(midas::util::FromString<Nb>(s));
            break;
         }
         case NORMALIZENORMALCOORDINATES:
         {
            gPesCalcDef.SetNormalizeNormalCoordinates(true);
            break;
         }
         case ORTHONORMALIZENORMALCOORDINATES:
         {
            gPesCalcDef.SetOrthoNormalizeNormalCoordinates(true);
            break;
         }
         case TRUNCATENEARZEROPROPVAL:
         {
            gPesCalcDef.SetmTruncateNearZeroPropVal(true);
            break;
         } 
         case ERROR: //FALLTHROUGH
         default:
         {
            MIDASERROR(" Keyword " + s_orig + " is not a level 2 PES keyword! Check your input!");
         }
      }
   }

   Mout << std::endl;

   return s; 
}



/***************************************************************************//**
 * @brief
 *    Map for handling SymmetryThresholds input keyword reading.
 *
 * Map of input keywords and how to handle them. Add RegisterFunction%s to add
 * keywords.
 *    -  `#<level> THRVIBCOORDSNUMEQZEROULPS`: read number from next line and
 *       save to corresponding SymmetryThresholds member.
 *
 * @param[in] aLevel
 *    The input keyword level.
 * @param[out] arSymThrs
 *    Ref. to the object in which the read values will be stored.
 * @return
 *    Map for handling input keyword reading.
 ******************************************************************************/
input_map_t midas::input::SymmetryThresholdsMap
   (  In aLevel
   ,  SymmetryThresholds& arSymThrs
   )
{
   input_map_t imap(aLevel);
   imap.RegisterFunction
      (  imap.MakeKeyword("THRVIBCOORDSNUMEQZEROULPS")
      ,  midas::input::BindSetTypeFromReadString
         (  arSymThrs.mThrVibCoordsNumEqZeroUlps
         )
      );
   imap.RegisterFunction
      (  imap.MakeKeyword("THRNUCLEIISNUMEQUALULPS")
      ,  midas::input::BindSetTypeFromReadString
         (  arSymThrs.mThrNucleiIsNumEqualUlps
         )
      );
   imap.RegisterFunction
      (  imap.MakeKeyword("THRGETCHARACTERFLOATEQULPS")
      ,  midas::input::BindSetTypeFromReadString
         (  arSymThrs.mThrGetCharacterFloatEqUlps
         )
      );

   return imap;
}
