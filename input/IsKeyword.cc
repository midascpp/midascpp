#include "IsKeyword.h"

#include "util/Error.h"
#include "util/MidasStream.h"
extern MidasStream Mout;

namespace midas
{
namespace input
{

/**
 * Check whether input string holds a keyword
 *
 * @param str   The string to check.
 * 
 * @return      Returns true if str contains a keyword.
 **/
bool IsKeyword
   (  const std::string& str
   )
{
   return str.substr(0,1)=="#";
}

/**
 * Check that input string does not hold a keyword
 *
 * @param str   The string to check.
 * 
 * @return      Returns true if str does not contain a keyword.
 **/
bool NotKeyword
   (  const std::string& str
   )
{
   return !IsKeyword(str);
}

/**
 * Check whether string contains a lower level keyword.
 *
 * @param str           The string to check.
 * @param input_level   Current input level to check against.
 *
 * @return   Returns true if string contains a keyword of lower level than input_level.
 **/
bool IsLowerLevelKeyword
   (  const std::string& str
   ,  In input_level
   )
{
   return (atoi(str.substr(1,1).c_str()) < input_level);
}

/**
 * Create an input error. This will STOP the execution of the program.
 *
 * @param s_orig       The original input string that caused the error.
 * @param type         Input level 'type', e.g. VCC, VSCF, PES, TENSORDECOMP, etc...
 * @param input_level  The current input level.
 **/
void InputKeywordError
   (  const std::string& s_orig
   ,  std::string type
   ,  In input_level
   )
{
   Mout << " Keyword " << s_orig 
        << " is not a " << type << " level " 
        << std::to_string(input_level) 
        << " Input keyword - input flag ignored! " << std::endl;
   MIDASERROR(" Check your input please ");
}

/**
 * Assert that string contains a keyword. If it does not an error will be 'thrown',
 * and execution of the program will be halted.
 *
 * @param str   The string to check.
 **/
void AssertKeyword
   (  const std::string& str
   )
{
   if(NotKeyword(str))
   {
      MIDASERROR("On input, expected keyword, but got: "+str);
   }
}

/**
 * Assert that string does not contain a keyword. If it does an error will be 'thrown',
 * and execution of the program will be halted.
 *
 * @param str   The string to check.
 **/
void AssertNotKeyword
   (  const std::string& str
   )
{
   if(IsKeyword(str))
   {
      MIDASERROR("On input, expected non-keyword, but got: "+str);
   }
}

/**
 * Assert that string contains a specific keyword. If it does not an error will be 'thrown',
 * and execution of the program will be halted.
 *
 * @param str   The string to check.
 * @param test  The expected keyword.
 **/
void AssertSpecificKeyword
   (  const std::string& str
   ,  const std::string&& test
   )
{
   if(!IsKeyword(str) && str != test)
   {
      MIDASERROR("On input, expected keyword: '" + test +  "', but got: '" + str + "'");
   }
}

} /* namespace input */
} /* namespace midas */
