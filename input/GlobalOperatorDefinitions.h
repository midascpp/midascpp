/**
************************************************************************
*
* @file                GlobalOperatorDefinitions.h
*
* Created:             16-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for holding operator definitions and the
*                       lists of operators and modes
*
* Last modified: Thu Oct 29, 2009  11:57AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#ifndef GLOBALOPERATORDEFINITIONS_H
#define GLOBALOPERATORDEFINITIONS_H

// std headers
#include <vector>
#include <string>
#include <utility>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "operator/OneModeOper.h"
#include "input/OpDef.h"
#include "input/OpInfo.h"
#include "util/Io.h"
#include "util/generalfunctions/FunctionContainer.h"

// using declarations
using std::vector;
using std::map;
using std::pair;

/**
 * Global operator definitions.
 **/
class GlobalOperatorDefinitions
{
   private:
      //! The Mode Labels on a vector
      std::vector<std::string>                mModeLabels;
      //! A Mode Labels,GlobalModeNr map
      std::map<std::string, GlobalModeNr>     mModeMap;
      //! The number of modes
      In                                      mNmodes;
      //! The OneModeOpers in the calc
      std::vector<std::unique_ptr<OneModeOperBase<Nb>> >      mOneModeOpers;
      //! The operator defs.
      std::vector<OpDef>                      mOperators;
      //! For quickly finding the operator def. by name
      std::map<std::string, In>               mOperatorNrs;
      //! Some info on operators..
      std::map<std::string, OpInfo>           mOpInfo;
      //! Name of electronic degree of freedom (will be contained in mModeLabels if not empty)
      std::string mElectronicDegreeOfFreedom = "";

      //! Bool for forcing integral evaluation through the general code for B-splines
      bool mBsplineGenIntEval;

      //! Finds out if the function is simply Q^#, clears the string and returns the power
      In GetPolyPow(string& aFuncStr) const;

      //! Removes DDQ/DDQ^2 tags and returns the derivative structure in the variables
      void GetDerOrdersForOneModeOper(std::string&, In&, bool&) const;

   public:
      //! Default ctor
      GlobalOperatorDefinitions() : mNmodes(I_0) {}

      //! CK : need to have the possibility to reset when doing a new PES calculations for incrementals
      void ReSet();

      //!
      GlobalOperNr MultiplyOpers(GlobalOperNr aOper1, GlobalOperNr aOper2);

      //!
      void AddToOpInfo(const string& aOperName, string& aOpInfoString);

      //!
      OpInfo& GetOpInfo(const string& aOper);

      //!
      GlobalOperNr AddRspOperToken(LocalOperNr aLocalOper);

      //!
      LocalOperNr GetRspOperToken(GlobalOperNr aOperNr) {return GetOneModeOper(aOperNr)->GetOperNrForResp();}
      
      //!
      GlobalOperNr AddOneModeOper(const std::string&, const bool&, bool&, const FunctionContainer<Nb>& = FunctionContainer<Nb>());

      //!
      GlobalOperNr AddOneModeOper(const In&, const In& aRDer = I_0, const bool& aLDer = false, const bool& aIsKinOper = false);

      //! Add empty operator
      void AddOperator()
      {
         mOperators.push_back(OpDef());
      }

      //! Add new operator with name
      void AddOperator
         (  OpDef&& arOper
         ,  const std::string& aName
         )
      {
         mOperators.emplace_back(std::move(arOper));
         this->SetOperNameAndNr(aName, mOperators.size()-1);
      }

      //!
      In GetOperatorNr(const std::string& aString) const;

      //!
      void SetOperNameAndNr(const std::string& aName, In aIn)
      {
         auto ret = mOperatorNrs.insert(std::make_pair(aName, aIn));
         if(!ret.second)
         {
            MIDASERROR("Operator "+ret.first->first+" already declared in program");
         }
      }

      //!
      In GetNrOfOpers() const {return mOperators.size();}
      
      //!
      const std::string& GetModeLabel(GlobalModeNr aIn) const {return mModeLabels.at(aIn);}

      //!
      const std::vector<std::string>& GetModeLabels
         (
         )  const
      {
         return mModeLabels;
      }

      //!
      std::string GetOperLabel(GlobalOperNr aIn) const {return GetOneModeOper(aIn)->GetOrigOperString();}
      
      //!
      In GetNrOfModes() const {return mModeLabels.size();}

      //!
      void SetmBsplineGenIntEval(bool aBool) {mBsplineGenIntEval = aBool;}

      //!
      bool GetmBsplineGenIntEval() {return mBsplineGenIntEval;}

      /**
       * Note that this operator returns the individual operators in the order they were defined in the input
       * This might seem a little strange, but it makes life much easier
       **/
      OpDef& operator[](In aIn)
      {
         return mOperators[aIn];
      }
      const OpDef& operator[](In aIn) const
      {  
         return mOperators[aIn];
      }

      //!
      In GetNrOfOneModeOpers() const {return mOneModeOpers.size();}

      //! Range-checked access to aIn'th OneModeOperBase.
      const std::unique_ptr<OneModeOperBase<Nb>>& GetOneModeOper(GlobalOperNr aIn) const;
      
      std::string GetOneModeOperDescription(GlobalOperNr aIn) const {return GetOneModeOper(aIn)->OperString();}

      //!
      GlobalModeNr ModeNr
         (  const std::string& aMode
         ,  bool aAddIfNotFound=false
         );

      //!
      const std::map<std::string, GlobalModeNr>& GetModeMap
         (
         )  const
      {
         return this->mModeMap;
      }

      //!@{
      //!
      OpDef&       GetOperator(const std::string& aName);
      const OpDef& GetOperator(const std::string& aName) const;
      const std::vector<OpDef>& GetOperators() const { return mOperators; }
      //!@}
      
      //! Get/Set mElectronicDegreeOfFreedom
      GlobalModeNr ElectronicDofNr
         (
         )  const
      {
         try
         {
            return mModeMap.at(this->mElectronicDegreeOfFreedom);
         }
         catch(const std::out_of_range& oor)
         {
            return -I_1;
         }
      }
      const std::string& ElectronicDofLabel
         (
         )  const
      {
         return mElectronicDegreeOfFreedom;
      }
      void SetElectronicDof
         (  const std::string& aE
         )
      {
         this->mElectronicDegreeOfFreedom = aE;
      }
};

#endif /* GLOBALOPERATORDEFINITIONS_H */
