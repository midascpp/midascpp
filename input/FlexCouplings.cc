/**
************************************************************************
* 
* @file                FlexCouplings.cc
*
* Created:             17-02-2014
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Function determining the maximum number of
*                      couplings for a given mode combination based on 
*                      the FlexCoupCalcDef input
* 
* Last modified:       08-12-2014
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <map>
#include <vector>
#include<string>
#include <algorithm> 
#include <limits>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"

#include "input/Input.h"
#include "input/InputOperFromFile.h"
#include "input/FlexCoupCalcDef.h"
#include "input/FlexCouplings.h"
#include "pes/molecule/MoleculeInfo.h"

#include "mpi/FileToString.h"

using std::map;
using std::make_pair;
using std::max;

/**
* Calculates the spatial overlap between two modes
* */
Nb NmOverlap(const MidasVector& apNm1, const MidasVector& apNm2)
{
   Nb norm1=C_0;
   Nb norm2=C_0;
   Nb overlap=C_0;

   In lengths=apNm1.Size();
   if (lengths != apNm2.Size())
      MIDASERROR ("FLEXCOUP normal modes have different lengths...");
   
   In n_atoms=lengths/I_3;

   for (In i=I_0; i < n_atoms; i++)
   {
      Nb term_k=C_0;
      Nb term_l=C_0;
      for (In j=I_0; j < I_3; j++)
      {
         term_k += fabs(apNm1[i*I_3+j]);
         term_l += fabs(apNm2[i*I_3+j]);
      }
      overlap += term_k*term_l;       
   }
  
 
   return overlap;
}
/**
* * Initiates Measure from File 
* * */
McMeasure::McMeasure(const string& arFilename)
{
  mType=SCREENFROMFILE;
  MeasureFromFile(arFilename);
  mScreenType=UNKNOWN;
}
/**
* Initiates Measure for a type  
* */
McMeasure::McMeasure(const string& arType,const FlexCoupCalcDef arCalcDef)
{
   mType=mTypeMap[arType];  
 
   switch(mType)
   {
   case EDIFF:
   {
      midas::molecule::MoleculeInfo mol_info(arCalcDef.GetMoleculeFileInfo());
      mMeasureOrder = I_2;
      mMeasureFile="Ediffs";
      mMeasureMap = MeasureMapEdiff(mol_info); 
      PrintMeasuresToFile();
      mScreenType=PRESCREEN;
      break;
   }
   case LOCAL:
   {
      midas::molecule::MoleculeInfo mol_info(arCalcDef.GetMoleculeFileInfo());
      mMeasureOrder = I_2;
      mMeasureFile="Local";
      mMeasureMap = MeasureMapLocal(mol_info);
      PrintMeasuresToFile();
      mScreenType=PRESCREEN;
      break;
   }
   case SCREENFROMFILE:
   {
      MeasureFromFile(arCalcDef.GetmMcScreenFile());
      mScreenType = SCREEN;

      break;
   }
   case VCCPERT1:
   {
      McMeasure rec_min_eps(arCalcDef.GetmMaxEx(),arCalcDef.GetmVccPertFiles()[I_0]);
      vector<string> file_names=arCalcDef.GetmVccPertFiles();
      file_names[I_0]=rec_min_eps.GetmMeasureFile();
      VccPert1(file_names, arCalcDef.GetmMaxEx()) ;
      SetOneModesToInf();
      PrintMeasuresToFile();
      mScreenType=SCREEN;
      break;
   }
   case VCCPERT2:
   {
      McMeasure rec_min_eps(arCalcDef.GetmMaxEx(),arCalcDef.GetmVccPertFiles()[I_0]);
      vector<string> file_names=arCalcDef.GetmVccPertFiles();
      file_names[I_0]=rec_min_eps.GetmMeasureFile();
      VccPert2(file_names,  arCalcDef.GetmMaxEx());
      McMeasure vcc_pert2_f("VCCPERT2F",arCalcDef);
      vector<string> pert1_files{{file_names[I_0]},{file_names[I_1]}};
      McMeasure vcc_pert1(pert1_files, arCalcDef.GetmMaxEx());
      Nb scale_fac=VccPert2ScalingFactor(vcc_pert1,vcc_pert2_f);
      *this = vcc_pert1 + (*this)*scale_fac;
      SetOneModesToInf();
      mMeasureFile="VccPert2_scaled";
      PrintMeasuresToFile();
      mScreenType=SCREEN;
      break;
   }
   case VCCPERT2H: // not used
   {
      McMeasure rec_min_eps(arCalcDef.GetmMaxEx(),arCalcDef.GetmVccPertFiles()[I_0]);
      vector<string> file_names=arCalcDef.GetmVccPertFiles();
      file_names[I_0]=rec_min_eps.GetmMeasureFile();
      VccPert2H(file_names, arCalcDef.GetmMaxEx());
      mScreenType=UNKNOWN;
      break;
   }
   case VCCPERT2F:
   {
      McMeasure rec_min_eps(arCalcDef.GetmMaxEx(),arCalcDef.GetmVccPertFiles()[I_0]);
      vector<string> file_names=arCalcDef.GetmVccPertFiles();
      file_names[I_0]=rec_min_eps.GetmMeasureFile();
      VccPert2F(file_names, arCalcDef.GetmMaxEx());
      mScreenType=UNKNOWN;
      break;
   }
   case ADGAPRESCREEN:
   {
      mMeasureOrder = I_0;
      mScreenType=PRESCREEN;
      break;
   }
   default:
      MIDASERROR ("FlexCouplings: Dont know Measure Type");
   }
}
/**
*  Calculates Ediffs and returns them as map
* */
map<vector<In>,Nb> McMeasure::MeasureMapEdiff(const midas::molecule::MoleculeInfo& arMolInfo)
{
   map<vector<In>,Nb> e_diffs;
   In n_modes=arMolInfo.GetNoOfVibs();   

   vector<In> mc(I_2);
   for (In i = 0; i<n_modes-I_1; i++)
   {
      Nb freq_i=arMolInfo.GetFreqI(i);
      mc[I_0]=i;
      for (In j=i+1 ;j < n_modes; j++)
      {
         mc[I_1]=j;
         Nb freq_j=arMolInfo.GetFreqI(j);
         Nb tmp_diff = C_1/std::fabs(freq_j-freq_i);
         e_diffs.insert(make_pair(mc,tmp_diff));
      }
   }
 
   return e_diffs;
   
}

/**
*  Calculates Loal overlap and returns them as map
* */
map<vector<In>,Nb> McMeasure::MeasureMapLocal(const midas::molecule::MoleculeInfo& arMolInfo)
{
   map<vector<In>,Nb> local;
   In n_modes=arMolInfo.GetNoOfVibs();   

   vector<In> mc(I_2);
   for (In i = 0; i<n_modes-I_1; i++)
   {
      MidasVector nm1;
      arMolInfo.GetNormalMode(nm1,i);
      mc[I_0]=i;
      for (In j=i+1 ;j < n_modes; j++)
      {
         mc[I_1]=j;
         MidasVector nm2;
         arMolInfo.GetNormalMode(nm2,j);
         Nb overlap=NmOverlap(nm1,nm2);
         local.insert(make_pair(mc,overlap));
      }
   }
   return local;
}

/**
* reads measure from file 
* */
void McMeasure::MeasureFromFile(const string& arFileName)
{
  In order=I_0;

  string s;
  std::istringstream ifs = midas::mpi::FileToStringStream(arFileName); 
  while (getline(ifs,s))
  {  
     istringstream iss(s);
     string s_tmp; 
     iss >> s_tmp; 
     vector<In> v_mc=midas::util::VectorFromString<In>(s_tmp,',');
     iss >> s_tmp; 
     Nb value=midas::util::FromString<Nb>(s_tmp);
     mMeasureMap.insert(make_pair(v_mc,value));
     if (order < v_mc.size()) 
        order = v_mc.size();
  }
  mMeasureOrder = order ;
}
/**
 * generates Vcc perturbation measures to first order 
 **/
void McMeasure::VccPert1(const vector<string>& arFileNames, const In aOrder)
{
   mMeasureFile="VccPert1";
   if (arFileNames.size() != I_2)
      MIDASERROR("Something went wrong with the files for the perturbation measures.");

   McMeasure rec_eps(arFileNames[I_0]);
   McMeasure hamiltonian_estimates_pi(arFileNames[I_1]);

   map<vector<In>,Nb> rec_eps_map=rec_eps.GetmMeasureMap();
   map<vector<In>,Nb> h_est_map=hamiltonian_estimates_pi.GetmMeasureMap();

   mMeasureOrder= aOrder;

   for (map<vector<In>,Nb>::const_iterator p = rec_eps_map.begin(); p != rec_eps_map.end(); p++)
   {
      vector<In> mc=p->first;
      if (mc.size()>mMeasureOrder) continue;
      Nb measure=C_0;
      if(mc.size()==I_1)
      {
         mMeasureMap.insert(make_pair(mc,measure));
         continue;
      }
      ModeCombi mc_main(mc,-I_1);
      bool found=false;
      for (map<vector<In>,Nb>::const_iterator q = h_est_map.begin(); q != h_est_map.end(); q++)
      {
         if(p->first == q->first) found=true; 
         ModeCombi mc_sum(q->first,-I_1);
         if (mc_sum.Contains(mc_main)) measure+=q->second;
      }
      if (!found) MidasWarning("MCR in VCCPERT1 does not fit");
      measure*=p->second;
      mMeasureMap.insert(make_pair(mc,measure));
   }
   PrintMeasuresToFile();
}
/**
 * generates Vcc perturbation measures to second order 
 **/
void McMeasure::VccPert2(const vector<string>& arFileNames, const In aOrder)
{
   mMeasureFile="VccPert2";
   if (arFileNames.size() != I_4)
      MIDASERROR("Something went wrong with the files for the perturbation measures (2nd order).");

   // get all the information
   McMeasure rec_eps(arFileNames[I_0]);
   McMeasure hamiltonian_estimates_pi(arFileNames[I_1]);
   McMeasure hamiltonian_estimates_all(arFileNames[I_2]);
   vector<In> limit_modal_basis=midas::util::VectorFromString<In>(arFileNames[I_3]);

   vector<string> pert1_files{{arFileNames[I_0]},{arFileNames[I_1]}};
   McMeasure vcc_pert1(pert1_files, hamiltonian_estimates_all.mMeasureOrder);


   // put them into maps
   map<vector<In>,Nb> rec_eps_map=rec_eps.GetmMeasureMap();
   map<vector<In>,Nb> h_pi_map=hamiltonian_estimates_pi.GetmMeasureMap();
   map<vector<In>,Nb> h_all_map=hamiltonian_estimates_all.GetmMeasureMap();
   map<vector<In>,Nb> pert1_map=vcc_pert1.GetmMeasureMap();

   mMeasureOrder= aOrder;

   for (map<vector<In>,Nb>::const_iterator p = rec_eps_map.begin(); p != rec_eps_map.end(); p++)
   {
      vector<In> mc=p->first;
      ModeCombi mc_main(mc,-I_1);
      bool found_pert=false;
      bool found_all=false;
      Nb measure=C_0;
      for (map<vector<In>,Nb>::const_iterator t = pert1_map.begin(); t != pert1_map.end(); t++)
      {
         if(p->first == t->first) found_pert=true;
         vector<In> mc_t_vec=t->first;  
         if(mc_t_vec.size()==I_1) continue;  // exclude the terms for one-mode in T (since they are zero)
         ModeCombi mc_t(mc_t_vec,-I_1);
         Nb n_modal_combis=C_1;
         for (In i=I_0; i< mc_t_vec.size(); i++)
         {
            n_modal_combis *= limit_modal_basis[mc_t_vec[i]];
         }
         Nb term=C_0;
         for (map<vector<In>,Nb>::const_iterator h = h_all_map.begin(); h != h_all_map.end(); h++)
         {
            if(p->first == h->first) found_all=true;
            vector<In> mc_h_vec=h->first;
            if(mc_h_vec.size()==I_1) continue; // exclude all terms for one-modes in H
            ModeCombi mc_h(mc_h_vec,-I_1);
            ModeCombi mc_sym_diff;
            mc_sym_diff.InOneOnly(mc_main,mc_t); 
            if (!ZeroIntersect(mc_h,mc_t) && (mc_h.Contains(mc_sym_diff) ||  mc_sym_diff.Empty()))  
                // only account for those that intersect and where m \triangle m_H \subset m_H
            {  
               Nb term_in=n_modal_combis;  //finally start with evaluating the terms - N_{{\bf m}_T} of first term
               term_in *= h->second;       // multiply Hamiltonian measure first term
               Nb term_2= h_pi_map[mc_h_vec];
               bool add_2=false;
               if (mc_sym_diff.Empty())
               {  
                  add_2=true;
                  term_in *= (mc_h.Size()+C_1);
                  term_2 *= (mc_h.Size()-C_1);
               }
               else if (mc_sym_diff.Size() == I_1)
               {
                  term_in *= C_2;
               }
               else if (mc_main.Contains(mc_t))  // additional restriction for second term
               {  
                  add_2=true;
               }
         
               if (add_2)
               { 
                  term_in += term_2;  //add Hamilonian measure second term
               }
               term += term_in;
            }
         } 
         term *=t-> second;
         measure += term;
      }
      if (!found_pert || !found_all) MidasWarning("MCRs in VCCPERT2 do not fit - ok if level of estimates > MC level PES");
      measure*=p->second;  // multiplication with the eps^{-1}  
      mMeasureMap.insert(make_pair(mc,measure));
   }
   PrintMeasuresToFile();
}
/**
 * generates measures for \f$ \left<\mu^m | [H^{(m_H)}, T^{(1)} ] 0 \right> \f$.
 * @warning NOT USED
 **/
void McMeasure::VccPert2H(const vector<string>& arFileNames,const In aOrder)
{
   mMeasureFile="VccPert2H";
   if (arFileNames.size() != I_4)
      MIDASERROR("Something went wrong with the files for the perturbation measures (2nd order).");

   // get all the information
   McMeasure rec_eps(arFileNames[I_0]);
   McMeasure hamiltonian_estimates_pi(arFileNames[I_1]);
   McMeasure hamiltonian_estimates_all(arFileNames[I_2]);
   vector<In> limit_modal_basis=midas::util::VectorFromString<In>(arFileNames[I_3]);

   vector<string> pert1_files{{arFileNames[I_0]},{arFileNames[I_1]}};
   McMeasure vcc_pert1(pert1_files, aOrder);


   // put them into maps
   map<vector<In>,Nb> rec_eps_map=rec_eps.GetmMeasureMap();
   map<vector<In>,Nb> h_pi_map=hamiltonian_estimates_pi.GetmMeasureMap();
   map<vector<In>,Nb> h_all_map=hamiltonian_estimates_all.GetmMeasureMap();
   map<vector<In>,Nb> pert1_map=vcc_pert1.GetmMeasureMap();

   mMeasureOrder= aOrder;

   for (map<vector<In>,Nb>::const_iterator p = rec_eps_map.begin(); p != rec_eps_map.end(); p++)
   {
      vector<In> mc=p->first;
      ModeCombi mc_main(mc,-I_1);
      bool found_pert=false;
      bool found_all=false;
      Nb measure=C_0;
      for (map<vector<In>,Nb>::const_iterator t = pert1_map.begin(); t != pert1_map.end(); t++)
      {
         if(p->first == t->first) found_pert=true;
         vector<In> mc_t_vec=t->first;  
         if(mc_t_vec.size()==I_1) continue;  // exclude the terms for one-mode in T (since they are zero)
         ModeCombi mc_t(mc_t_vec,-I_1);
         Nb n_modal_combis=C_1;
         for (In i=I_0; i< mc_t_vec.size(); i++)
         {
            n_modal_combis *= limit_modal_basis[mc_t_vec[i]];
         }
         Nb term=C_0;
         for (map<vector<In>,Nb>::const_iterator h = h_all_map.begin(); h != h_all_map.end(); h++)
         {
            if(p->first == h->first) found_all=true;
            vector<In> mc_h_vec=h->first;
            ModeCombi mc_h(mc_h_vec,-I_1);
            ModeCombi mc_sym_diff;
            mc_sym_diff.InOneOnly(mc_main,mc_t); 
            if (!ZeroIntersect(mc_h,mc_t) && (mc_h.Contains(mc_sym_diff) || mc_sym_diff.Empty()))  
                // only account for those that intersect and where m \triangle m_H \subset m_H
            {  
               Nb term_in=n_modal_combis;  //finally start with evaluating the terms - N_{{\bf m}_T} of first term
               term_in *= h->second;       // multiply Hamiltonian measure first term
            
               if (mc_main.Contains(mc_t) || mc_sym_diff.Empty())  // additional restriction for second term
               {  
                 term_in += h_pi_map[mc_h_vec];  //add Hamilonian measure second term
               }
               term += term_in;
            }
         } 
        
         term *=t-> second;
         measure += term;
       
      }
      if (!found_pert || !found_all) MidasWarning("MCRs in VCCPERT2 do not fit - ok if level of estimates > MC level PES");
      measure*=p->second;  // multiplication with the eps^{-1}  
      mMeasureMap.insert(make_pair(mc,measure));
   }
   PrintMeasuresToFile();
}
/**
 * generates measures for \f$ \left<\mu^m | [F^{(m_H)}, T^{(1)} ] 0 \right> \f$.
 **/
void McMeasure::VccPert2F(const vector<string>& arFileNames,const  In aOrder)
{
   mMeasureFile="VccPert2F";
   if (arFileNames.size() != I_4)
      MIDASERROR("Something went wrong with the files for the perturbation measures (2nd order).");

   // get all the information
   McMeasure rec_eps(arFileNames[I_0]);
   McMeasure hamiltonian_estimates_pi(arFileNames[I_1]);
   McMeasure hamiltonian_estimates_all(arFileNames[I_2]);
   vector<In> limit_modal_basis=midas::util::VectorFromString<In>(arFileNames[I_3]);

   vector<string> pert1_files{{arFileNames[I_0]},{arFileNames[I_1]}};
   McMeasure vcc_pert1(pert1_files,  hamiltonian_estimates_all.mMeasureOrder);

   // put them into maps
   map<vector<In>,Nb> rec_eps_map=rec_eps.GetmMeasureMap();
   map<vector<In>,Nb> h_pi_map=hamiltonian_estimates_pi.GetmMeasureMap();
   map<vector<In>,Nb> h_all_map=hamiltonian_estimates_all.GetmMeasureMap();
   map<vector<In>,Nb> pert1_map=vcc_pert1.GetmMeasureMap();

   mMeasureOrder= aOrder;

   for (map<vector<In>,Nb>::const_iterator p = rec_eps_map.begin(); p != rec_eps_map.end(); p++)
   {
      vector<In> mc=p->first;
      ModeCombi mc_main(mc,-I_1);
      bool found_pert=false;
      bool found_all=false;
      Nb measure=C_0;
      for (map<vector<In>,Nb>::const_iterator t = pert1_map.begin(); t != pert1_map.end(); t++)
      {
         if(p->first == t->first) found_pert=true;
         vector<In> mc_t_vec=t->first;  
         if(mc_t_vec.size()==I_1) continue;  // exclude the terms for one-mode in T (since they are zero)
         ModeCombi mc_t(mc_t_vec,-I_1);
         Nb n_modal_combis=C_1;
         for (In i=I_0; i< mc_t_vec.size(); i++)
         {
            n_modal_combis *= limit_modal_basis[mc_t_vec[i]];
         }
         Nb term=C_0;
         for (map<vector<In>,Nb>::const_iterator h = h_all_map.begin(); h != h_all_map.end(); h++)
         {
            if(p->first == h->first) found_all=true;
            vector<In> mc_h_vec=h->first;
            ModeCombi mc_h(mc_h_vec,-I_1);
            ModeCombi mc_sym_diff;
            mc_sym_diff.InOneOnly(mc_main,mc_t); 
            if (!ZeroIntersect(mc_h,mc_t) && mc_h.Contains(mc_sym_diff) && mc_sym_diff.Size() <= I_1)  
            {  
               Nb term_in=n_modal_combis;  //finally start with evaluating the terms - N_{{\bf m}_T} of first term
               term_in *= h->second;       // multiply Hamiltonian measure first term
               Nb term_2=h_pi_map[mc_h_vec];
               if (mc_sym_diff.Empty() )
               {
                  term_in *= mc_h.Size();     // multiply dimension of m_H
                  term_2 *= mc_h.Size();
               }
               if (mc_main.Contains(mc_t) || mc_sym_diff.Empty())  // additional restriction for second term
               {  
                  term_in += term_2;  //add Hamilonian measure second term
               }
               term += term_in;
            }
         } 
         term *=t-> second;
         measure += term;
      }
      if (!found_pert || !found_all) MidasWarning("MCRs in VCCPERT2 do not fit - ok if level of estimates > MC level PES");
      measure*=p->second;  // multiplication with the eps^{-1}  
      mMeasureMap.insert(make_pair(mc,measure));
   }
   PrintMeasuresToFile();
}
/**
* Add measures
* */
void McMeasure::AddValues(const McMeasure& arMeasure2)
{
   map<vector<In>,Nb> map2=arMeasure2.mMeasureMap;
   for (map<vector<In>,Nb>::const_iterator p = map2.begin(); p != map2.end(); p++)
   {  
     mMeasureMap[p->first] += p->second;
   }  
}
/**
* Print measures for SystemCoup on file
* */
void McMeasure::PrintMeasuresToFile()
{
    
   string screen_file = gAnalysisDir + "/" + mMeasureFile;
   midas::mpi::OFileStream out_file(screen_file);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, out_file);

   out_file << mMeasureOrder << endl;

   for (map<vector<In>,Nb>::const_iterator p = mMeasureMap.begin(); p != mMeasureMap.end(); p++)
   {  
     out_file<< p->first << " " << p->second << endl;
   }
}
/**
 * generates the minimal reciprocal energies  
 **/
void McMeasure::RecMinEps(const In& arMaxEx,  const string& arEpsFileName)
{
   mMeasureFile=gAnalysisDir+"/RecMinEps";
   midas::mpi::OFileStream out_file_mc(mMeasureFile);
   out_file_mc.setf(ios::scientific);
   out_file_mc.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, out_file_mc);

   McMeasure eps(arEpsFileName);
   if (eps.GetmMeasureOrder() != I_1)
      MIDASERROR("Something wrong with the given eps file.");
   In n_modes=eps.Size();

   ModeCombiOpRange mcr(arMaxEx,n_modes);


   for (In i_mc=I_1; i_mc < mcr.Size(); i_mc++)
   {
      const ModeCombi& mc=mcr.GetModeCombi(i_mc);
      vector<In> mc_vec=mc.MCVec();
      In n_modes=mc_vec.size();
      //Mout << " CK mc_vec" << mc_vec << endl;
      Nb eps_min=C_0;
      for (In n=I_0 ; n < n_modes ; n++)
      {
        vector<In> mode;
        mode.push_back(mc_vec[n]);
        eps_min += eps.GetValueForMc(mode);
      }
      Nb min_rec_ex = C_1/eps_min;
      mMeasureMap.insert(make_pair(mc_vec,min_rec_ex));
      for (In i=I_0; i< mc_vec.size()-I_1; i++)
         out_file_mc  << mc_vec[i] << ",";
      out_file_mc<< mc_vec[mc_vec.size()-I_1]<< " "  <<  min_rec_ex << endl;
  }
}
/**
* Gets maximum value for a ModeCombinations 
* */
Nb McMeasure::MinValue(const vector<In> arModeCombi) const
{
   In current_level = arModeCombi.size();
   if (mMeasureOrder==current_level) 
   {
      try
      {
         return mMeasureMap.at(arModeCombi);
      }
      catch(const std::out_of_range&)
      {
         return 0.;
      }
   }
   Nb min_value= std::numeric_limits<float>::infinity();
   vector<In> tmp(mMeasureOrder);
   min_value=MinRecurse(tmp,arModeCombi,min_value,I_0,I_0);
   return min_value;
}
/**
* Gets minimum value for a ModeCombinations 
* */
Nb McMeasure::MinRecurse(const vector<In>& arCurrentMc, const vector<In>& arModeCombi, const Nb& arCurrentMax, const In& arStart, const In& arCurrentLevel) const
{
   In mc_order=arModeCombi.size();
   Nb min_value=arCurrentMax;
   vector<In> tmp=arCurrentMc;
   In start = arStart;

   for (In i=start; i< mc_order ; i++)
   {
      tmp[arCurrentLevel]=arModeCombi[i];
      if (arCurrentLevel + I_2 < mc_order)
      {  
         start++;
         min_value=MinRecurse(tmp,arModeCombi,min_value,start,arCurrentLevel+I_1);
      }
      else 
      {
         Nb tmp_min = GetValueForMc(tmp);
         if (tmp_min < min_value) min_value=tmp_min; 
      }
   }
   return min_value;
}
/**
* Gets maximum level for a ModeCombinations 
* */
In McMeasure::MaxLevelPreScreen(const vector<In>& arModeCombi,const In& arMaxLevel) const
{ 
   if (arModeCombi.size() < mMeasureOrder)
      return arMaxLevel; 

   Nb min_value=MinValue(arModeCombi);
   In max_level=mThreshs.size();
   if (max_level<arMaxLevel)
   {
      MIDASERROR ("Number of Threshholds smaller than required number of excitations");
   }
   for (In i=I_0; i< mThreshs.size(); i++)
   { 
     if (mThreshs[i] > min_value) 
        max_level -= I_1;
     else 
        break;
   }
   return max_level;
}

/**
 * Calculate (pre) screen estimates from files 
 **/
void McMeasure::CalcEstimatesFromFile(const string& arIntFileName,const string& arOpFile)
{   
   Nb** max_int=ReadInts(arIntFileName); 
   gOperatorDefs.AddOperator();
   OpDef& op_def = gOperatorDefs[gOperatorDefs.GetNrOfOpers()-1];
   op_def.SetOpFile(arOpFile);
   op_def.InitLastOper(arOpFile, C_1, -I_1, -I_1, -I_1,false);

   std::istringstream oper_inp_file = midas::mpi::FileToStringStream(arOpFile);
   auto last_read = midas::input::ReadGenericMidasInput(oper_inp_file, op_def, arOpFile, -I_1, I_0, C_1);
   op_def.Reorganize(true);
   op_def.InitOpRange();
   op_def.MCRReorder();
   GenScreenEstimates(max_int,op_def); 
   
   In n_modes=op_def.NmodesInOp();
   for (In i=I_0 ; i < n_modes; i++) delete[] max_int[i];
   delete[] max_int;
}
/**
* Read Integrals from file 
* */
Nb** McMeasure::ReadInts(const string& arIntFileName)
{
   std::istringstream ifs = midas::mpi::FileToStringStream(arIntFileName);
   string s;
   getline(ifs,s);
   istringstream iss(s);
   In n_modes;
   iss >> n_modes;
   Nb** max_ints;
   max_ints = new Nb*[n_modes];
   for (In modes=I_0; modes<n_modes; modes++)
   {
      getline(ifs,s);
      istringstream iss_m(s);
      In mode_test;
      iss_m >> mode_test;
      if (mode_test != modes)
          MIDASERROR("Something wrong when reading Integral file "+arIntFileName);
      In n_op;
      iss_m >> n_op; 
      max_ints[modes] = new Nb[n_op];
      for (In j=I_0; j<n_op ; j++)
      { 
         getline(ifs,s); 
         istringstream iss_o(s);
         In op_test;
         iss_o >> op_test;
         if (op_test != j)
            MIDASERROR("Something wrong when reading Integral file "+arIntFileName);
         Nb integral;
         iss_o >> integral;
         max_ints[modes][j] = integral;
      }
   }
   getline(ifs,s); 
   if (!ifs.eof())
      MIDASERROR("Something wrong when reading Integral file "+arIntFileName);
   return max_ints;
}
/**
* Read Integrals from file 
* */
void McMeasure::GenScreenEstimates(Nb** const appMaxInt, OpDef& apOpDef)
{
   std::string screen_file_all = gAnalysisDir+"/ScreenEstimates";
   midas::mpi::OFileStream out_file(screen_file_all);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, out_file);

   std::string screen_file_mc = gAnalysisDir+"/ScreenEstimatesMcs";
   midas::mpi::OFileStream out_file_mc(screen_file_mc);   
   out_file_mc.setf(ios::scientific);
   out_file_mc.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, out_file_mc);

   out_file << "Estimates for screening of terms and mode combinations" << endl << endl; 

   ModeCombiOpRange mcr=apOpDef.GetModeCombiOpRange();

   mMeasureOrder= I_0;

   In n_mc=mcr.Size();
   Nb** max_prod; 
   max_prod= new Nb*[n_mc];
   Nb* max_sum_mc; 
   max_sum_mc= new Nb[n_mc];
   for (In i_mc=I_0; i_mc < n_mc; i_mc++)
   {  
      const ModeCombi& mc=mcr.GetModeCombi(i_mc);
      In n_modes=mc.Size();
      if (n_modes>mMeasureOrder) mMeasureOrder=n_modes;
      vector<In> mc_vec=mc.MCVec();
      out_file << "#1 MODECOMBINATION " << endl;
      out_file << "   " <<  i_mc << " " <<mc_vec << endl;

      vector<GlobalOperNr> op_for_mc=apOpDef.OpersForMc(mc);
      In n_op = op_for_mc.size();

      max_sum_mc[i_mc]=C_0;
      max_prod[i_mc]= new Nb[n_op];
      for (In i_op=I_0; i_op < n_op; i_op++)
      { 
         Nb coef=apOpDef.Coef(op_for_mc[i_op]);
         Nb prod=fabs(coef);
         for (In i_modes=I_0; i_modes < n_modes; i_modes++)
         {
            In mode=mc_vec[i_modes];
            In op= apOpDef.OperForOperMode(op_for_mc[i_op],mode);
            Nb fac = appMaxInt[mode][op];
            prod *= fac ;
         }
         max_prod[i_mc][i_op]=prod;
         max_sum_mc[i_mc] += prod;
      }
      out_file << "   #2 SUM" << endl;
      mMeasureMap.insert(make_pair(mc_vec,max_sum_mc[i_mc]));
      out_file << "      " <<  max_sum_mc[i_mc] << endl;
      for (In i=I_0; i< mc_vec.size()-I_1; i++)
         out_file_mc  << mc_vec[i] << ",";
      out_file_mc<< mc_vec[mc_vec.size()-I_1]<< " "  <<  max_sum_mc[i_mc] << endl;
      out_file << "   #2 OPERATOR CONTRIBUTIONS" << endl;
      for (In i_op=I_0; i_op < n_op; i_op++)
         out_file << "      " << max_prod[i_mc][i_op] << " " << op_for_mc[i_op] <<  " " <<  apOpDef.GetOpers(op_for_mc[i_op]) << endl;
   }

   out_file.close();

   for (In i=I_0 ; i < n_mc; i++) delete[] max_prod[i];
   delete[] max_prod;
   delete[] max_sum_mc;
 
} 
/**
* sets all measures for one modes to infinity 
* */
void McMeasure::SetOneModesToInf()
{
   map<vector<In>,Nb> new_map;
   for (map<vector<In>,Nb>::const_iterator p = mMeasureMap.begin(); p != mMeasureMap.end(); p++) 
   { 
      if(p->first.size()==I_1)
      {
         vector<In> mc = p->first;
         new_map.insert(make_pair(mc, C_NB_MAX)); 
      }
      else
      {
         new_map.insert(make_pair(p->first, p->second));
      }
         
   }

  mMeasureMap = new_map;
}


/***************************************************************
***************************************************************
FlexCouplings 
***************************************************************
**************************************************************
*/
/**
* Calculates measures for SystemCoup and stores them in analysis 
* */
void FlexCouplings::GetMeasures(const FlexCoupCalcDef& arCalcDef)
{

   //screening
   if (arCalcDef.GetmMcScreen())
   {  
      McMeasure tmp_measure("SCREENFROMFILE",arCalcDef);
      vector<Nb> thresh;
      thresh.push_back(arCalcDef.GetmMcScreenThr());
      tmp_measure.SetmThreshs(thresh);
      mFlexCouplings.push_back(tmp_measure);
      mCheckAll=true; 
   }
   if (arCalcDef.GetmVccPertOrder()>I_0)
   {
      if (arCalcDef.GetmVccPertOrder()==I_1)
      {
         McMeasure tmp_measure("VCCPERT1",arCalcDef);
         vector<Nb> thresh;
         thresh.push_back(arCalcDef.GetmVccPertThr());
         tmp_measure.SetmThreshs(thresh);
         mFlexCouplings.push_back(tmp_measure);
      }
      else if (arCalcDef.GetmVccPertOrder()==I_2)
      {
         McMeasure tmp_measure("VCCPERT2",arCalcDef);
         vector<Nb> thresh;
         thresh.push_back(arCalcDef.GetmVccPertThr());
         tmp_measure.SetmThreshs(thresh);
         mFlexCouplings.push_back(tmp_measure);
         mCheckAll=true;
      }
      else
         MIDASERROR("Order of perturbation "+std::to_string(arCalcDef.GetmVccPertOrder()) +" in screening not known");
   }
   //pre screening - is taken care of later, so nothing here
   if (arCalcDef.GetmAdgaPreScreen())
   {  
   }
   //one-mode prescreening 

   //two-mode prescreening 
   if (arCalcDef.GetmSetEdiff())
   {  
      McMeasure tmp_measure("EDIFF",arCalcDef);
      tmp_measure.SetmThreshs(arCalcDef.GetmEdiffThreshs());
      mFlexCouplings.push_back(tmp_measure);
   }   

   if (arCalcDef.GetmSetLocal())
   {  
      McMeasure tmp_measure("LOCAL",arCalcDef);
      tmp_measure.SetmThreshs(arCalcDef.GetmLocalThreshs());
      mFlexCouplings.push_back(tmp_measure);
   }   

}

/**
* returns and integer for the largest level of coupling for the given mode combination
* based on the given FlexCoupCalcDef and Molecule.
* */
In FlexCouplings::MaxLevel(const vector<In>& arModeCombi, const In& arMaxLevel) const
{

   In max_level = arMaxLevel;
   In current_level = arModeCombi.size(); 


   for (In im=I_0; im < mFlexCouplings.size(); im++)
   { 
      if (mFlexCouplings[im].GetScreenType()=="SCREEN")
      {
         Nb value =  mFlexCouplings[im].GetValueForMc(arModeCombi);
         vector<Nb> threshs =  mFlexCouplings[im].GetThresh();
         if (threshs.size() != 1)
            MIDASERROR ("More than one threshold in screening");
         if (threshs[I_0] > value)
         {
            return I_0;
         }
      }
      else if (mFlexCouplings[im].GetScreenType()=="PRESCREEN")
      {
        if (current_level < mFlexCouplings[im].GetmMeasureOrder())
           return max_level;

        In max_level_tmp=mFlexCouplings[im].MaxLevelPreScreen(arModeCombi,arMaxLevel);
        if (max_level_tmp<max_level)
           max_level = max_level_tmp;
      }
      else
          MIDASERROR ("Something went wrong -- screening type not known");

   }

   return max_level;
 
};

/**
* FRIENDS
* */
/**
* Scaling factor 
* */
Nb VccPert2ScalingFactor(const McMeasure& arPert1, const McMeasure& arPert2F)
{
   map<vector<In>,Nb> f_map=arPert2F.mMeasureMap;
   map<vector<In>,Nb> fac_map;
   vector<Nb> fac_vec;
   for (map<vector<In>,Nb>::const_iterator p = arPert1.mMeasureMap.begin(); p != arPert1.mMeasureMap.end(); p++)
   {
     vector<In> mc= p->first;
     if (mc.size() <= I_1) continue; //they should be zero anyway
     if (p->second <= C_0)
     {
        MidasWarning("Zero entries in first-order perturbative measures are ignored for the scaling factor"); 
        continue;
     } 
     Nb f_meas=f_map[mc];
     Nb fac= p->second/f_meas;
     fac_vec.push_back(fac);
     fac_map.insert(make_pair(mc,fac));
   }

   {
     McMeasure scale_fac(fac_map,arPert1.mMeasureOrder,"Scaling_Factors");
     scale_fac.PrintMeasuresToFile();
   }
   std::sort(fac_vec.begin(),fac_vec.end());
   Nb final_fac=C_0;
   In mid=fac_vec.size() / I_2;
   if (fac_vec.size() % I_2 == I_0) // even
   {
      final_fac= fac_vec[mid-I_1]+fac_vec[mid];
      final_fac/=C_2;   
   }
   else 
   {
      final_fac= fac_vec[mid];
   }
   Mout << "Median Scaling factor used:" << final_fac << endl;
   return final_fac;
}
/**
* Operators
* */
McMeasure operator*(const McMeasure& ar1, const Nb& arFac)
{
   McMeasure m_out(ar1);
   for (map<vector<In>,Nb>::const_iterator p = m_out.mMeasureMap.begin(); p != m_out.mMeasureMap.end(); p++)
   {  
      vector<In> mc=p->first; 
      Nb measure=p->second;
      measure *= arFac;    
      m_out.mMeasureMap.erase(p);
      m_out.mMeasureMap.insert(make_pair(mc,measure));
   }
   return m_out;
}
/**
* "+" assuming that the vectors for the mode combinations are in the same orders in both 
* */
McMeasure operator+(const McMeasure& ar1, const McMeasure& ar2)
{
   map<vector<In>,Nb> map_out;
   map<vector<In>,Nb> m1=ar1.mMeasureMap;
   map<vector<In>,Nb> m2=ar2.mMeasureMap;
   for (map<vector<In>,Nb>::const_iterator p = m1.begin(); p != m1.end(); p++)
   {  
      vector<In> mc=p->first; 
      Nb measure=p->second;
      if (m2.find(mc)!=m2.end())
      {
         measure += m2[mc] ; 
      }   
      map_out.insert(make_pair(mc,measure));
   }
   for (map<vector<In>,Nb>::const_iterator p = m2.begin(); p != m2.end(); p++)
   {  
      vector<In> mc=p->first; 
      if (map_out.find(mc)!=map_out.end()) continue;
      Nb measure=p->second;
      map_out.insert(make_pair(mc,measure));
   }
   In order=max(ar1.mMeasureOrder,ar2.mMeasureOrder);
   string filename="AddedMeasure";
   McMeasure m_out(map_out,order,filename);
   return m_out;
}
