/**
************************************************************************
*
* @file                McTdHCalcDef.h
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a MCTDH calculation
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHCALCDEF_H_INCLUDED
#define MCTDHCALCDEF_H_INCLUDED

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>
#include <map>

// Midas Headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ode/OdeInfo.h"
#include "input/TdPropertyDef.h"
#include "td/oper/GaussPulse.h"
#include "input/InputMap.h"

namespace midas::input
{

/**
* Construct a definition of a McTdH calculation
* */
class McTdHCalcDef
   :  public TdPropertyDef
{
   public:
      /** @name Alias **/
      //!@{
      using integration_info_t = midas::input::InputMap;
      using method_info_t = midas::input::InputMap;
      //!@}

      //! Initial wave-packet types
      enum class initType : int
      {  VSCF = 0          // Start from VSCF wave packet
      ,  VCI               // Start from VCI wave packet with VSCF modals
      ,  TDH               // Start from TDH calculation (imaginary-time propagation/relaxation)
      ,  MCTDH             // Start from another MCTDH calculation (imaginary-time propagation/relaxation)
      };
      
      //! Parameterizations
      enum class mctdhID : int
      {  LCASTDH = 0       // Linear CASTDH / "standard" MCTDH
      ,  LRASTDH           // Linear RASTDH / restricted active space in linear parameterization
      };

      //! Regularization info
      enum class regularizationType : int
      {  NONE = 0          // Do density-matrix inverse using zgelss (SVD-based linear least squares)
      ,  STANDARD          // Standard MCTDH eigenvalue-decomposition-based matrix inversion
      ,  IMPROVED          // Regularize the coefficient tensor (not implemented at this point...)
      };

      //! Types of g-operator optimization (the one that acutally changes the wave function)
      enum class GOptType : int
      {  VARIATIONAL = 0   // Variational optimization of non-redundant blocks
      ,  DENSITYMATRIX     // Use non-redundant blocks to keep density matrix block-diagonal
      ,  NONE              // Do not optimize non-redundant blocks
      ,  ONEMODEH          // Use one-mode H also for non-redundant blocks
      };

      //! Type of gauge choice (only defining the redundant rotations)
      enum class GaugeType : int
      {  ZERO = 0          // Zero all redundant blocks
      ,  ONEMODEH          // Set all redundant blocks to one-mode part of Hamiltonian
      ,  DENSITYMATRIX     // Set all redundant blocks to propagate in natural modals
      };

      //! Types of regularization used for solving linear equations for g-operator optimization
      enum class GOptRegType : int
      {  NONE = 0          // Use a standard (Hermitian) linear solver
      ,  LS                // Use SVD-based linear-least-squares solver with rcond=eps
      ,  STANDARD          // Use the MCTDH regularization based on eigenvalue decomposition
      ,  SVD               // Use SVD-based algorithm where all singular values (sigma) are set to max(sigma, sigma_max*epsilon)
      ,  XSVD              // Use SVD-based algorithm with smooth regularization (eXponential)
      ,  TIKHONOV          // Use Tikhonov regularization (diagonal) on linear system
      };
      
      //! Method-specific info
      CREATE_VARIABLE(method_info_t, MethodInfo, method_info_t{});
      
      //! Integration scheme
      CREATE_VARIABLE(integration_info_t, IntegrationInfo, integration_info_t{});
      
      //! Name of calculation
      CREATE_VARIABLE(std::string, Name, std::string{"mctdh"});
      
      //! Hamiltonian operator
      CREATE_VARIABLE(std::string, Oper, std::string{""});
      
      //! Basis set
      CREATE_VARIABLE(std::string, Basis, std::string{""});
      
      //! Directory for analysis
      CREATE_VARIABLE(std::string, AnalysisDir, std::string{""});
      
      //! Define the size of the active space.
      CREATE_VARIABLE(std::vector<unsigned>, ActiveSpace, std::vector<unsigned>{});
      
      //! Truncate the virtual spaces when e.g. using a large bspline basis for initial VSCF.
      CREATE_VARIABLE(std::vector<In>, ModalBasisLimits, std::vector<In>{});
      
      //! Policy for initial wave packet
      CREATE_VARIABLE(initType, InitialWavePacket, initType::VSCF);
      
      //! Operator to apply to initial wave packet (e.g. a dipole operator to compute IR spectra)
      CREATE_VARIABLE(std::string, ApplyOperator, std::string{""});
      
      //! Type of spectral-basis transform. Empty string will give spectral basis of reference potential.
      CREATE_VARIABLE(std::string, SpectralBasisOper, std::string{""});
      
      //! Name of calculation that defines the initial wave packet
      CREATE_VARIABLE(std::string, InitCalcName, std::string{""});
      
      //! Do imaginary-time propagation
      CREATE_VARIABLE(bool, ImagTime, false);
      
      //! Threshold for stopping imag-time prop.
      CREATE_VARIABLE(Nb, ImagTimeThreshold, -C_1);
      
      //! Do exact propagation (i.e. ignore modal derivative)
      CREATE_VARIABLE(bool, Exact, false);
      
      //! IO level
      CREATE_VARIABLE(In, IoLevel, I_1);
      
      //! Save interpolated WFs to disc
      CREATE_VARIABLE(bool, SaveInterpolatedWfs, false);
      
      //! Save accepted WFs to disc
      CREATE_VARIABLE(bool, SaveAcceptedWfs, false);

      //! Use TDH to update one-mode density
      CREATE_VARIABLE(bool, UpdateOneModeDensities, false);
      
      //! After how many steps should the density be updated
      CREATE_VARIABLE(In, UpdateOneModeDensitiesStep, I_0);

      //! Write the one-mode densities to file
      CREATE_VARIABLE(bool, WriteOneModeDensities, false);

      //! Set value of SpaceSmoothening parameter
      CREATE_VARIABLE(In, SpaceSmoothening, I_0);

      //! Set value of TimeSmoothening parameter
      CREATE_VARIABLE(In, TimeSmoothening, I_0);
      
      //! Propagate modals for electronic DOF (debug option)
      CREATE_VARIABLE(bool, PropagateElectronicDofModals, false);
      
      /** @name Definitions for time-dependent Hamiltonian **/
      //!@{
      //! Use Gaussian pulse
      CREATE_VARIABLE(bool, GaussianPulse, false);
      
      //! Operator for pulse
      CREATE_VARIABLE(std::string, PulseOper, std::string{"pulse"});
      
      //! Specs for pulse
      CREATE_VARIABLE(UNPACK(std::map<midas::td::GaussParamID, Nb>), PulseSpecs,
         UNPACK(std::map<midas::td::GaussParamID, Nb>{
            {midas::td::GaussParamID::AMPL, 1.0}, 
            {midas::td::GaussParamID::TPEAK, 0.0}, 
            {midas::td::GaussParamID::SIGMA, 1.0}, 
            {midas::td::GaussParamID::FREQ, 0.0}, 
            {midas::td::GaussParamID::PHASE, 0.0}
         }));
      //!@}

   private:

      //! Validate method info
      void ValidateMethodInfo
         (
         );

      //! Validate integration info
      void ValidateIntegrationInfo
         (
         );

   public:
      //! Default c-tor
      McTdHCalcDef() = default;

      //! Clone
      McTdHCalcDef Clone
         (
         )  const
      {
         return *this;
      }

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read IntegrationInfo
      bool ReadIntegrationInfo
         (  std::istream&
         ,  std::string&
         );

      //! Read policy for initial wave function
      bool ReadInitialWfPolicy
         (  std::istream&
         ,  std::string&
         ,  std::set<std::string>&
         );

      //! Read method name
      bool ReadMethod
         (  std::istream&
         ,  std::string&
         );

      //! Add method info
      template
         <  typename T
         >
      void AddMethodInfo
         (  const std::string& aKey
         ,  const T& aVal
         )
      {
         mMethodInfo[aKey] = aVal;
      }

      //! Are we using exponential parameterization?
      std::pair<bool, bool> ExponentialParameterization
         (
         )  const;

      //! Are we using exponential transformation of modals?
      bool ExponentialModalTransform
         (
         )  const;

      //! Are we using exponential transformation of coefs?
      bool ExponentialCoefsTransform
         (
         )  const;

      //! Ostream
      friend std::ostream& operator<<
         (  std::ostream&
         ,  const McTdHCalcDef&
         );
};

} // namespace midas::input

#endif /* MCTDHCALCDEF_H_INCLUDED */
