/**
************************************************************************
* 
* @file                TotalRspFunc.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function framework
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NEWTOTALRSPFUNC_H
#define NEWTOTALRSPFUNC_H

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream> 
using std::ostream;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Contribution.h"
#include "input/TotalResponseContribution.h"

/**
* Construct a definition of a total response calculation calculation
* */
class TotalResponseDrv
{
   protected:
      string mName;
      set<Contribution> mBasicSet;
      set<Contribution> mIntermediateSet;
      set<string> mContribSet;
      vector<string> mTotalRspString;

      // functions
      set<string> ConvertStringToFunctions(string&);
      void CheckForDefaults(set<Contribution>&,set<Contribution>&);
      string NloOperNameForString(string,bool=false,const vector<Nb> *mpFrq=0);
      Nb ConvertAlgebraStringToNb(string&);
      Nb SymbolicMathOperation(const string&,Nb,Nb=C_0);
      void EvalParentheses(In&,In,string&);
      Nb EvaluateSimple(string);
      In FindLeftNumber(string,In);
      In FindRightNumber(string,In);
      string StringToExpression(const string&,const map<string,TotalResponseContribution>&,bool);
      Nb SearchForEqVal(const string&,vector<Nb>);
      void ReportAllTypes(const string&,map<string,TotalResponseContribution>);

   public:
      ///< Constructor. Needs a map of keys-values and an order
      ///< keys-values map contains all the remaining stuff
      TotalResponseDrv(const std::map<string,string>&);
      static TotalResponseDrv* Factory(const In, const std::map<string,string>&);
      virtual ~TotalResponseDrv() {};
      ///< Evaluate response functions
      void Evaluate();
      virtual map<string,TotalResponseContribution> ProvideMap() = 0;
      virtual vector<Nb> GetFrqVec() = 0;
      ///< Output overload
      friend ostream& operator<<(ostream&, const TotalResponseDrv&);
      virtual ostream& Print(ostream&) const = 0;
      void SetBasicSet(const set<Contribution>& aSet) {mBasicSet=aSet;}
      virtual void ConstructIntermediateSet() = 0;
};

#endif
