/*
************************************************************************
*
* @file                McTdHInput.cc
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input reader for the MCTDH module
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/McTdHCalcDef.h"
#include "input/McTdHInput.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "input/FindKeyword.h"
#include "input/Trim.h"

/*
* Read and check MCTDH input.
* Read until next s = "$i..." i<3 which is returned.
*/
std::string McTdHInput
   (  std::istream& Minp
   )
{
   if (  gDoMcTdH
      && gDebug
      )
   {
      Mout << " I have read one McTdH input - reading a new McTdH input "<< endl;
   }

   gDoMcTdH = true;
   gMcTdHCalcDef.push_back(input::McTdHCalcDef());   // Add to vector of McTdH-calcs.
   input::McTdHCalcDef& new_mctdh = gMcTdHCalcDef.back();

   // Enum/map with input flags
   enum INPUT  {  ERROR
               ,  IOLEVEL
               ,  NAME
               ,  OPER
               ,  BASIS
               ,  METHOD
               ,  INTEGRATOR
               ,  INITIALWF
               ,  IMAGTIME
               ,  EXACT
               ,  ACTIVESPACE
               ,  LIMITMODALBASIS
               ,  PROPERTIES 
               ,  SPECTRA
               ,  EXPECTATIONVALUES
               ,  WAVEFUNCTIONFORMODES
               ,  TWOMODEDENSITIES
               ,  WFGRIDDENSITY
               ,  NOAUTOCORRCONVOLUTION
               ,  SPECTRUMENERGYSHIFT
               ,  SPECTRUMOUTPUTSCREENING
               ,  SPECTRUMINTERVAL
               ,  LIFETIME
               ,  SPECTRALBASIS
               ,  REGULARIZATION
               ,  NONORTHOPROJECTOR
               ,  DISABLENONORTHOPROJECTOR
               ,  PROJECTINSIGMACALCULATION
               ,  FFTPADLEVEL
               ,  ANALYSISDIR
               ,  CONSTRAINTOPERATOR
               ,  GOPTNONORTHOPROJECTOR
               ,  NOGOPTNONORTHOPROJECTOR
               ,  NONORTHOCONSTRAINTOPERATOR
               ,  GOPTREGULARIZATION
               ,  GOPTTRACESCALEEPS
               ,  EVALUATEMCBYMC
               ,  EVALUATECONTRIBBYCONTRIB
               ,  V3FILEPREFIX
               ,  SAVEINTERPOLATEDWFS
               ,  SAVEACCEPTEDWFS
               ,  V3TRANS
               ,  EXTRANGEMODES
               ,  GAUSSIANPULSE
               ,  RENORMALIZEWF
               ,  EXACTNORM2
               ,  PROPAGATEELECTRONICDOFMODALS
               ,  BVECTORV3OOSTRANSFORM
               ,  NOBVECTORV3OOSTRANSFORM
               ,  CHECKBVECTOR
               ,  ALLOWEXPERIMENTAL
               ,  UPDATEONEMODEDENSITIES
               ,  WRITEONEMODEDENSITIES
               ,  SPACESMOOTHENING
               ,  TIMESMOOTHENING
               };
   const std::map<std::string,INPUT> input_word =
   {  {"#3NAME",NAME}
   ,  {"#3OPER",OPER}
   ,  {"#3BASIS",BASIS}
   ,  {"#3METHOD", METHOD}
   ,  {"#3INTEGRATOR",INTEGRATOR}
   ,  {"#3ACTIVESPACE", ACTIVESPACE}
   ,  {"#3LIMITMODALBASIS", LIMITMODALBASIS}
   ,  {"#3IMAGTIME",IMAGTIME}
   ,  {"#3EXACT", EXACT}
   ,  {"#3WAVEFUNCTIONFORMODES", WAVEFUNCTIONFORMODES}
   ,  {"#3PROPERTIES",PROPERTIES}
   ,  {"#3SPECTRA", SPECTRA}
   ,  {"#3NOAUTOCORRCONVOLUTION", NOAUTOCORRCONVOLUTION}
   ,  {"#3EXPECTATIONVALUES", EXPECTATIONVALUES}
   ,  {"#3TWOMODEDENSITIES", TWOMODEDENSITIES}
   ,  {"#3WFGRIDDENSITY", WFGRIDDENSITY}
   ,  {"#3IOLEVEL", IOLEVEL}
   ,  {"#3INITIALWF", INITIALWF}
   ,  {"#3SPECTRUMENERGYSHIFT", SPECTRUMENERGYSHIFT}
   ,  {"#3SPECTRUMOUTPUTSCREENING", SPECTRUMOUTPUTSCREENING}
   ,  {"#3SPECTRUMINTERVAL", SPECTRUMINTERVAL}
   ,  {"#3LIFETIME", LIFETIME}
   ,  {"#3SPECTRALBASIS", SPECTRALBASIS}
   ,  {"#3REGULARIZATION", REGULARIZATION}
   ,  {"#3NONORTHOPROJECTOR", NONORTHOPROJECTOR}
   ,  {"#3DISABLENONORTHOPROJECTOR", DISABLENONORTHOPROJECTOR}
   ,  {"#3PROJECTINSIGMACALCULATION", PROJECTINSIGMACALCULATION}
   ,  {"#3FFTPADLEVEL", FFTPADLEVEL}
   ,  {"#3ANALYSISDIR", ANALYSISDIR}
   ,  {"#3CONSTRAINTOPERATOR", CONSTRAINTOPERATOR}
   ,  {"#3GOPTNONORTHOPROJECTOR", GOPTNONORTHOPROJECTOR}
   ,  {"#3NONORTHOCONSTRAINTOPERATOR", NONORTHOCONSTRAINTOPERATOR}
   ,  {"#3NOGOPTNONORTHOPROJECTOR", NOGOPTNONORTHOPROJECTOR}
   ,  {"#3GOPTREGULARIZATION", GOPTREGULARIZATION}
   ,  {"#3GOPTTRACESCALEEPS", GOPTTRACESCALEEPS}
   ,  {"#3EVALUATEMCBYMC", EVALUATEMCBYMC}
   ,  {"#3EVALUATECONTRIBBYCONTRIB", EVALUATECONTRIBBYCONTRIB}
   ,  {"#3V3FILEPREFIX", V3FILEPREFIX}
   ,  {"#3SAVEINTERPOLATEDWFS", SAVEINTERPOLATEDWFS}
   ,  {"#3SAVEACCEPTEDWFS", SAVEACCEPTEDWFS}
   ,  {"#3V3TRANS", V3TRANS}
   ,  {"#3EXTRANGEMODES", EXTRANGEMODES}
   ,  {"#3GAUSSIANPULSE", GAUSSIANPULSE}
   ,  {"#3RENORMALIZEWF", RENORMALIZEWF}
   ,  {"#3EXACTNORM2", EXACTNORM2}
   ,  {"#3PROPAGATEELECTRONICDOFMODALS", PROPAGATEELECTRONICDOFMODALS}
   ,  {"#3BVECTORV3OOSTRANSFORM", BVECTORV3OOSTRANSFORM}
   ,  {"#3NOBVECTORV3OOSTRANSFORM", NOBVECTORV3OOSTRANSFORM}
   ,  {"#3CHECKBVECTOR", CHECKBVECTOR}
   ,  {"#3ALLOWEXPERIMENTAL", ALLOWEXPERIMENTAL}
   ,  {"#3UPDATEONEMODEDENSITIES", UPDATEONEMODEDENSITIES}
   ,  {"#3WRITEONEMODEDENSITIES", WRITEONEMODEDENSITIES}
   ,  {"#3SPACESMOOTHENING", SPACESMOOTHENING}
   ,  {"#3TIMESMOOTHENING", TIMESMOOTHENING}
   };

   static const std::map<std::string, input::McTdHCalcDef::regularizationType> regul_map =
   {  {"NONE", input::McTdHCalcDef::regularizationType::NONE}
   ,  {"STD", input::McTdHCalcDef::regularizationType::STANDARD}
   ,  {"STANDARD", input::McTdHCalcDef::regularizationType::STANDARD}
//   ,  {"IMPROVED", input::McTdHCalcDef::regularizationType::IMPROVED}  // Not implemented!
   };

   static const std::map<std::string, input::McTdHCalcDef::GOptType> g_opt_map =
   {  {"VARIATIONAL", input::McTdHCalcDef::GOptType::VARIATIONAL}
   ,  {"DENSITYMATRIX", input::McTdHCalcDef::GOptType::DENSITYMATRIX}
   ,  {"NONE", input::McTdHCalcDef::GOptType::NONE}
   ,  {"ONEMODEH", input::McTdHCalcDef::GOptType::ONEMODEH}
   };

   static const std::map<std::string, input::McTdHCalcDef::GaugeType> gauge_map =
   {  {"ZERO", input::McTdHCalcDef::GaugeType::ZERO}
   ,  {"ONEMODEH", input::McTdHCalcDef::GaugeType::ONEMODEH}
//   ,  {"DENSITYMATRIX", input::McTdHCalcDef::GaugeType::DENSITYMATRIX} // Not implemented!
   };

   static const std::map<std::string, input::McTdHCalcDef::GOptRegType> g_opt_reg_map =
   {  {"NONE", input::McTdHCalcDef::GOptRegType::NONE}
   ,  {"LS", input::McTdHCalcDef::GOptRegType::LS}
   ,  {"STANDARD", input::McTdHCalcDef::GOptRegType::STANDARD}
   ,  {"STD", input::McTdHCalcDef::GOptRegType::STANDARD}
   ,  {"SVD", input::McTdHCalcDef::GOptRegType::SVD}
   ,  {"XSVD", input::McTdHCalcDef::GOptRegType::XSVD}
   ,  {"TIKHONOV", input::McTdHCalcDef::GOptRegType::TIKHONOV}
   };

   std::string s = "";
   std::string s_orig = "";
   std::set<std::string> apply_opers;
   In input_level = 3;
   bool already_read = false;
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks

      // Debug
      if (  gDebug
         )
      {
         Mout  << " Read keyword: " << s_orig << ". Transformed to: " << s << std::endl;
      }

      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case NAME:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_mctdh.SetName(s);
            break;
         }
         case OPER:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_mctdh.SetOper(s);
            break;
         }
         case BASIS:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_mctdh.SetBasis(s);
            break;
         }
         case METHOD:
         {
            already_read = new_mctdh.ReadMethod(Minp, s);
            break;
         }
         case INTEGRATOR:
         {
            already_read = new_mctdh.ReadIntegrationInfo(Minp, s);
            break;
         }
         case ACTIVESPACE:
         {
            midas::input::GetLine(Minp, s);

            auto vec = midas::util::VectorFromString<unsigned>(s);

            new_mctdh.SetActiveSpace(vec);

            break;
         }
         case LIMITMODALBASIS:
         {
            midas::input::GetLine(Minp, s);

            auto vec = midas::util::VectorFromString<In>(s);

            new_mctdh.SetModalBasisLimits(vec);

            break;
         }
         case IMAGTIME:
         {
            new_mctdh.SetImagTime(true);

            // Calculate energy on the fly when doing imag-time propagation
            new_mctdh.AddProperty("ENERGY");

            // Try to get threshold
            midas::input::GetLine(Minp, s);
            if (  midas::input::NotKeyword(s)
               )
            {
               new_mctdh.SetImagTimeThreshold(midas::util::FromString<Nb>(s));
            }
            // If no input threshold, we have read the next keyword
            else
            {
               already_read = true;
            }

            break;
         }
         case EXACT:
         {
            new_mctdh.SetExact(true);

            break;
         }
         case WAVEFUNCTIONFORMODES:
         {
            // Get line
            midas::input::GetLine(Minp, s);

            // Get vector
            auto mode_vec = midas::util::StringVectorFromString(s);

            // Convert to set
            std::set<std::string> mode_set(mode_vec.begin(), mode_vec.end());

            new_mctdh.SetWfOutModes(mode_set);
            break;
         }
         case PROPERTIES:
         {
            already_read = new_mctdh.ReadProperties(Minp, s);
            break;
         }
         case SPECTRA:
         {
            already_read = new_mctdh.ReadSpectra(Minp, s);
            break;
         }
         case NOAUTOCORRCONVOLUTION:
         {
            new_mctdh.SetConvoluteAutoCorr(false);
            break;
         }
         case EXPECTATIONVALUES:
         {
            already_read = new_mctdh.ReadExptValOpers(Minp, s);
            break;
         }
         case TWOMODEDENSITIES:
         {
            already_read = new_mctdh.ReadTwoModeDensityPairs(Minp, s);
            break;
         }
         case WFGRIDDENSITY:
         {
            auto dens = midas::input::GetIn(Minp, s);
            new_mctdh.SetWfGridDensity(dens);
            break;
         }
         case IOLEVEL:
         {
            auto io = midas::input::GetIn(Minp, s);
            new_mctdh.SetIoLevel(io);
            gMcTdHIoLevel = io;
            break;
         }
         case INITIALWF:
         {
            already_read = new_mctdh.ReadInitialWfPolicy(Minp, s, apply_opers);

            break;
         }
         case SPECTRUMENERGYSHIFT:
         {
            Nb e_shift = C_0;
            std::string type = "FIXED";

            midas::input::GetLine(Minp, s);
            auto s_vec = midas::util::StringVectorFromString(s);
            assert(s_vec.size() > 0);

            if (  s_vec[0] == "FIXED"
               )
            {
               assert(s_vec.size() == 2);
               type = s_vec[0];
               e_shift = midas::util::FromString<Nb>(s_vec[1]);
            }
            else if  (  s_vec[0] == "E0"
                     )
            {
               assert(s_vec.size() == 1);
               type = "E0";
               e_shift = C_0;
            }
            else
            {
               MIDASERROR("Unrecognized energy-shift type: " + s_vec[0]);
            }
            
            new_mctdh.SetSpectrumEnergyShift(std::pair<std::string, Nb>(type, e_shift));
            break;
         }
         case SPECTRUMOUTPUTSCREENING:
         {
            Nb thresh = midas::input::GetNb(Minp, s);
            new_mctdh.SetSpectrumOutputScreeningThresh(thresh);
            break;
         }
         case SPECTRUMINTERVAL:
         {
            midas::input::GetLine(Minp, s);
            auto [f0, f1] = midas::util::TupleFromString<Nb, Nb>(s);
            
            new_mctdh.SetSpectrumInterval(std::pair<Nb, Nb>(f0, f1));

            break;
         }
         case LIFETIME:
         {
            auto tau = midas::input::GetNb(Minp, s);
            new_mctdh.SetLifeTime(tau);
            break;
         }
         case SPECTRALBASIS:
         {
            midas::input::GetLine(Minp, s);
            if (  midas::input::NotKeyword(s)
               )
            {
               new_mctdh.SetSpectralBasisOper(s);
            }
            else
            {
               new_mctdh.SetSpectralBasisOper(""); // empty string will give operator for VSCF reference calculation.
               already_read = true;
            }

            break;
         }
         case REGULARIZATION:
         {
            midas::input::GetLine(Minp, s);
            auto svec = midas::util::StringVectorFromString(s);

            auto nargs = svec.size();
            if (  nargs == 1
               )
            {
               new_mctdh.AddMethodInfo("REGULARIZATIONTYPE", regul_map.at(svec[0]));
            }
            else if  (  nargs == 2
                     )
            {
               new_mctdh.AddMethodInfo("REGULARIZATIONTYPE", regul_map.at(svec[0]));
               new_mctdh.AddMethodInfo("REGULARIZATIONEPSILON", midas::util::FromString<Nb>(svec[1]));
            }
            else
            {
               MIDASERROR("Wrong number of args to REGULARIZATION!");
            }

            break;
         }
         case NONORTHOPROJECTOR:
         {
            new_mctdh.AddMethodInfo("NONORTHOPROJECTOR", true);
            break;
         }
         case DISABLENONORTHOPROJECTOR:
         {
            new_mctdh.AddMethodInfo("NONORTHOPROJECTOR", false);
            break;
         }
         case PROJECTINSIGMACALCULATION:
         {
            new_mctdh.AddMethodInfo("PROJECTAFTERINVERSEDENSITY", false);
            break;
         }
         case FFTPADLEVEL:
         {
            new_mctdh.SetPaddingLevel(midas::input::GetIn(Minp, s));
            break;
         }
         case ANALYSISDIR:
         {
            midas::input::GetLine(Minp, s);
            new_mctdh.SetAnalysisDir(s);
            break;
         }
         case CONSTRAINTOPERATOR:
         {
            midas::input::GetLine(Minp, s);
            auto s_up = midas::input::ToUpperCase(s);
            auto s_vec = midas::util::StringVectorFromString(s_up);

            MidasAssert(!s_vec.empty(), "McTdHInput needs an argument to '#3ConstraintOperator'!");
            MidasAssert(s_vec.size() <= 2, "Only two strings can be given to '#ConstraintOperator'!");
            
            new_mctdh.AddMethodInfo("GOPTTYPE", g_opt_map.at(s_vec[0]));

            if (  s_vec.size() == 2
               )
            {
               new_mctdh.AddMethodInfo("GAUGE", gauge_map.at(s_vec[1]));
            }

            break;
         }
         case GOPTNONORTHOPROJECTOR:
         {
            new_mctdh.AddMethodInfo("GOPTNONORTHOPROJECTOR", true);
            break;
         }
         case NOGOPTNONORTHOPROJECTOR:
         {
            new_mctdh.AddMethodInfo("GOPTNONORTHOPROJECTOR", false);
            break;
         }
         case NONORTHOCONSTRAINTOPERATOR:
         {
            new_mctdh.AddMethodInfo("NONORTHOCONSTRAINTOPERATOR", true);
            break;
         }
         case GOPTREGULARIZATION:
         {
            midas::input::GetLine(Minp, s);

            auto s_vec = midas::util::StringVectorFromString(s);

            // We accept one or two arguments (in one line!)
            if (  s_vec.size() == 1
               )
            {
               // If the only arg is "NONE", we use no regularization, and we do not need to set an epsilon
               if (  midas::input::ToUpperCase(s_vec[0]) == "NONE"
                  )
               {
                  new_mctdh.AddMethodInfo("GOPTREGULARIZATIONTYPE", input::McTdHCalcDef::GOptRegType::NONE);
                  new_mctdh.AddMethodInfo("GOPTREGULARIZATIONEPSILON", C_0);
               }
               // Else, if the only arg is a number, we use standard regularization with the corresponding epsilon
               else
               {
                  new_mctdh.AddMethodInfo("GOPTREGULARIZATIONTYPE", input::McTdHCalcDef::GOptRegType::STANDARD);
                  auto eps = midas::util::FromString<Nb>(s_vec[0]);
                  new_mctdh.AddMethodInfo("GOPTREGULARIZATIONEPSILON", eps);
               }
            }
            else if  (  s_vec.size() == 2
                     )
            {
               // For two args, the type comes first followed by the epsilon
               auto type = g_opt_reg_map.at(midas::input::ToUpperCase(s_vec[0]));
               new_mctdh.AddMethodInfo("GOPTREGULARIZATIONTYPE", type);
               auto eps = midas::util::FromString<Nb>(s_vec[1]);
               new_mctdh.AddMethodInfo("GOPTREGULARIZATIONEPSILON", eps);
            }
            else
            {
               MIDASERROR("Expected one or two arguments to GOPTREGULARIZATON");
            }

            break;
         }
         case GOPTTRACESCALEEPS:
         {
            new_mctdh.AddMethodInfo("GOPTTRACESCALEEPS", true);
            break;
         }
         case EVALUATEMCBYMC:
         {
            new_mctdh.AddMethodInfo("EVALUATECONTRIBBYCONTRIB", false);
            break;
         }
         case EVALUATECONTRIBBYCONTRIB:
         {
            new_mctdh.AddMethodInfo("EVALUATECONTRIBBYCONTRIB", true);
            break;
         }
         case V3FILEPREFIX:
         {
            midas::input::GetLine(Minp, s);
            new_mctdh.AddMethodInfo("V3FILEPREFIX", s);
            break;
         }
         case SAVEINTERPOLATEDWFS:
         {
            new_mctdh.SetSaveInterpolatedWfs(true);
            break;
         }
         case SAVEACCEPTEDWFS:
         {
            new_mctdh.SetSaveAcceptedWfs(true);
            break;
         }
         case V3TRANS:
         {
            new_mctdh.AddMethodInfo("V3TRANS", true);
            break;
         }
         case EXTRANGEMODES:
         {
            midas::input::GetLine(Minp, s);
            auto s_vec = midas::util::VectorFromString<In>(s);
            std::set<In> mode_set(s_vec.begin(), s_vec.end());
            new_mctdh.AddMethodInfo("EXTRANGEMODES", mode_set);
            break;
         }
         case GAUSSIANPULSE:
         {
            new_mctdh.SetGaussianPulse(true);
            midas::input::GetLine(Minp, s);
            new_mctdh.SetPulseOper(s);
            midas::input::GetLine(Minp, s);
            const auto v = midas::util::VectorFromString<Nb>(s);
            if (v.size() != 5)
            {
               std::stringstream ss;
               ss << "Parameters for GaussianPulse: v.size() == " << v.size() << " (should be 5); v = " << v;
               MIDASERROR(ss.str());
            }
            std::map<midas::td::GaussParamID,Nb> m;
            m[midas::td::GaussParamID::AMPL] = v.at(0);
            m[midas::td::GaussParamID::TPEAK] = v.at(1);
            m[midas::td::GaussParamID::SIGMA] = v.at(2);
            m[midas::td::GaussParamID::FREQ] = v.at(3);
            m[midas::td::GaussParamID::PHASE] = v.at(4);
            new_mctdh.SetPulseSpecs(m);
            break;
         }
         case RENORMALIZEWF:
         {
            new_mctdh.AddMethodInfo("RENORMALIZEWF", true);
            break;
         }
         case EXACTNORM2:
         {
            new_mctdh.AddMethodInfo("EXACTNORM2", true);
            break;
         }
         case PROPAGATEELECTRONICDOFMODALS:
         {
            new_mctdh.SetPropagateElectronicDofModals(true);
            break;
         }
         case BVECTORV3OOSTRANSFORM:
         {
            new_mctdh.AddMethodInfo("BVECTORV3OOSTRANSFORM", true);
            break;
         }
         case NOBVECTORV3OOSTRANSFORM:
         {
            new_mctdh.AddMethodInfo("BVECTORV3OOSTRANSFORM", false);
            break;
         }
         case CHECKBVECTOR:
         {
            new_mctdh.AddMethodInfo("CHECKBVECTOR", true);
            break;
         }
         case ALLOWEXPERIMENTAL:
         {
            new_mctdh.AddMethodInfo("ALLOWEXPERIMENTAL", true);
            break;
         }
         case UPDATEONEMODEDENSITIES:
         {
            In step = midas::input::GetIn(Minp, s);
            new_mctdh.SetUpdateOneModeDensities(true);
            new_mctdh.SetUpdateOneModeDensitiesStep(step);
            break;
         }
         case WRITEONEMODEDENSITIES:
         {
            new_mctdh.SetWriteOneModeDensities(true);
            break;
         }
         case SPACESMOOTHENING:
         {
            In smooth = midas::input::GetIn(Minp, s);
            new_mctdh.SetSpaceSmoothening(smooth);
            break;
         }
         case TIMESMOOTHENING:
         {
            In smooth = midas::input::GetIn(Minp, s);
            new_mctdh.SetTimeSmoothening(smooth);
            break;
         }
         case ERROR: // Fall through to default
         default:
         {
            Mout << " Keyword " << s_orig << " is not a McTdH level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Add McTdHCalcDef%s for each initially applied operator
   bool first_op = true;
   auto basename = new_mctdh.GetName();
   for(const auto& op : apply_opers)
   {
      if (  first_op
         )
      {
         Mout  << " Set applied operator to '" << op << "' for first MCTDH calculation." << std::endl;
         // Set applied operator for new_mctdh
         new_mctdh.SetApplyOperator(op);

         new_mctdh.SetName(basename + "_" + op);

         new_mctdh.SanityCheck();

         first_op = false;
      }
      else
      {
         Mout  << " Push back new MCTDH calculation for operator '" << op << "'." << std::endl;
         gMcTdHCalcDef.push_back(gMcTdHCalcDef.back());   // Add to vector of McTdH-calcs.
         gMcTdHCalcDef.back().SetApplyOperator(op);
         gMcTdHCalcDef.back().SetName(basename + "_" + op);
         gMcTdHCalcDef.back().SanityCheck();
      }
   }

   // Check sanity of new_mctdh (additional calcs are checked just after creating them)
   if (  apply_opers.empty()
      )
   {
      new_mctdh.SanityCheck();
   }

   return s;
}

/**
 * Initialize McTdH variables
 * */
void InitializeMcTdH()
{
   gDoMcTdH = false; 
   gMcTdHIoLevel = I_0;
}
