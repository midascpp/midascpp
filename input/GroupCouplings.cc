/**
************************************************************************
* 
* @file                GroupCouplings.cc
*
* Created:             14-01-2013
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Implement GroupCouplings class members 
* 
* Last modified:       02-06-2014
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <algorithm> 
using std::sort;
using std::find;
using std::unique;
using std::set_union;
using std::set_difference;
using std::set_intersection;


#include "inc_gen/math_link.h" 
//
// My headers:
#include "inc_gen/TypeDefs.h" 
#include "input/Input.h" 
#include "util/Io.h" 
#include "input/GroupCouplings.h" 
#include "util/InSet.h" 
#include "input/ModeCombi.h" 


/**
* Initialized ModeGroup by input strings
* */
GroupCouplings::GroupCouplings(const vector<string>& arModeGroupsInStrings,
       const vector<string>& arGroupCombiInStrings) 
{
   {
      In n = arModeGroupsInStrings.size();
      mModeGroups.clear();
      mNrModes=I_0;
      mMaxModeNr=I_0;
      mMaxGroupCombiLevel=I_0;
 
      for (In i=I_0 ; i < n ; i++)
      {
         InSet local_mode_group(arModeGroupsInStrings[i]); 
         mModeGroups.push_back(local_mode_group);
         mNrModes += mModeGroups[i].Size();
         In max_i(mModeGroups[i].Highest());
         if (max_i > mMaxModeNr) mMaxModeNr = max_i;

         set<In>::iterator it_i_begin(mModeGroups[i].Begin());
         set<In>::iterator it_i_end(mModeGroups[i].End());
         for (In j=0 ; j < i ; j++)
         {           
            set<In>::iterator it_j_begin(mModeGroups[j].Begin());
            set<In>::iterator it_j_end(mModeGroups[j].End());
            vector<In> tst_vec; 
            std::set_intersection(it_i_begin,it_i_end,it_j_begin,it_j_end,
                      std::back_inserter(tst_vec));
            if (tst_vec.size()!= I_0) 
               Error (" At least one mode assigned to more than one group" );
         }
      }
      if (mMaxModeNr != mNrModes-I_1) 
      {
      if (mMaxModeNr < mNrModes-I_1) 
        Error ("Something went wrong in the group assignment");
      else 
        Mout << ("WARNING: less modes than maximum mode numbers") << endl;
      }
   }


   {
      In n =arGroupCombiInStrings.size();
      for (In i=I_0 ; i < n ; i++)
      {
         AddSubGroupCombiRange(arGroupCombiInStrings[i]);
      } 
   }

}
/**
* Converts input string into vector of group combinations
* The Format is:
* MaxExLevel Mode(s),
* E.g., the string "3 1 2" creates the Group combinations
* (1,2) (1,2,1) (1,2,2).
* Note that only Group combinations are created which contain
* all given modes.
* */
void GroupCouplings::AddSubGroupCombiRange(string aString)
{
   istringstream ism(aString);
   In max_grp_level;
   ism >> max_grp_level;
   if (max_grp_level > mMaxGroupCombiLevel)
      mMaxGroupCombiLevel=max_grp_level; 

   vector<In> involved_groups;
   In group_nr;
   while (ism >> group_nr)
   { 
      involved_groups.push_back(group_nr);
   }
   In n_groups = involved_groups.size();
   
   vector<In>::iterator it = max_element(involved_groups.begin(),involved_groups.end()); 
   if (*it > mModeGroups.size())
      Error (" Groups not known in GroupModeCouplings ");
   if (max_grp_level < n_groups)
      Error ("found more modes than max excitation level in group couplings - check input ");

   vector<In> tmp_gc=involved_groups;
   
   AddGroupCombi(tmp_gc);
   
   if (max_grp_level > tmp_gc.size()) 
      RestrictedRecurseAddGroupCombis(tmp_gc,involved_groups,max_grp_level);

   for (In i = 0; i < mGroupCombiRange.size() ; i++)
   {
      vector<In> tmp_vec(mGroupCombiRange[i].MCVec());
   }

}

/**
* initiates sub_group_range from one initial ModeCombi and adds the additional involved groups
* recusively
* */

void GroupCouplings::RestrictedRecurseAddGroupCombis(const vector<In>& arInitialGroupCombi, 
      const vector<In>& arGroupList, const In& arMaxGroupCombiLevel)
{
   vector<In> tmp_gc=arInitialGroupCombi;
   In length=tmp_gc.size();
   tmp_gc.resize(length+1);

   for (In gi=I_0; gi<arGroupList.size(); gi++)
   {
       tmp_gc[length]=arGroupList[gi];
       AddGroupCombi(tmp_gc);
       if (tmp_gc.size() < arMaxGroupCombiLevel)
       { 
         RestrictedRecurseAddGroupCombis(tmp_gc, arGroupList, arMaxGroupCombiLevel); 
       }
   }

}  
/**
* Checks whether GroupCombi is present and if not add it to mGroupCombis
* */

void GroupCouplings::AddGroupCombi(const vector<In> arGroupCombi)
{
   vector<In> local_gc_vec = arGroupCombi;
   sort(local_gc_vec.begin(),local_gc_vec.end());
   ModeCombi local_gc(local_gc_vec,-I_1);
   vector<ModeCombi>::const_iterator 
          i = find(mGroupCombiRange.begin(),mGroupCombiRange.end(),local_gc);
   if (i==mGroupCombiRange.end()) 
   {
      mGroupCombiRange.push_back(local_gc);
   }
   else 
      Mout << "WARNING: group combination occured more than once"<< endl;
}


