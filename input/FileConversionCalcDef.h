#ifndef FILECONVERSIONCALCDEF_H_INCLUDED
#define FILECONVERSIONCALCDEF_H_INCLUDED

#include "pes/molecule/MoleculeFile.h"
#include "input/MidasOperatorReader.h"
#include "input/OperatorBounds.h"

using namespace midas;

/**
 * @brief
 *    Calculation definition for file conversions.
 *
 * Will hold information for how to convert different molecular inputs
 * and operator files.
 **/
class FileConversionCalcDef
{
   public:
      //! Enum defining different conversion types
      enum ConversionType 
         { MOLECULEFILE    // convert from 1 molecule file format to another
         , MIDASPOT        // create a MidasPot executable
         , MIDASDYNLIB     // create a MidasDynLib dynamic library
         };

   private:
      //! Type def for potential definition. Holds: "name", "descritpion", "order", "file(s)", optional "bounds file", and reference_index(cies).
      using potential_type = std::tuple<std::string, std::string, int, std::vector<std::string>, std::string, std::vector<int>>;
      
      //! Holds which type of conversion we are doing
      ConversionType mConversionType = ConversionType::MOLECULEFILE;
      //! Set of molecular inputs
      molecule::MoleculeFileInfo::Set mInputFiles;
      //! Set of molecular outputs
      molecule::MoleculeFileInfo::Set mOutputFiles;
      //! Vector of operator/potentials
      std::vector<potential_type> mOperatorFiles;
      //! Hold filename of reference value files for midaspot stuff
      std::string mReferenceValues;
      //! Hold en optional description of MidasPot
      std::string mDescription;
      //! Generate MidasPot unit test code instead of normal MidasPot
      bool mCreateUnitTestCode = false;
      //! Use include statements for MidasPot headers (as opposed to copy the contents of the headers)
      bool mMidasPotUseInclude = false;

   public:
      //! Set the conversion type
      void SetConversionType(const ConversionType& aC) { mConversionType = aC; }
      
      //! Set create unit test code
      void SetCreateUnitTestCode(bool aCreateUnitTestCode) { mCreateUnitTestCode = aCreateUnitTestCode; } 
      
      //! Set use include
      void SetMidasPotUseInclude(bool aMidasPotUseInclude) { mMidasPotUseInclude = aMidasPotUseInclude; } 
      
      //! Get the conversion type
      ConversionType GetConversionType() const { return mConversionType; }
      
      //! Add a molecular input
      void AddInputFile(molecule::MoleculeFileInfo&& info)  { mInputFiles.emplace_back(std::move(info)); }
      
      //! Add a molecular output
      void AddOutputFile(molecule::MoleculeFileInfo&& info) { mOutputFiles.emplace_back(std::move(info)); }

      //! Add an operator
      void AddOperator
         (  const std::string&              aName
         ,  const std::string&              aDescription
         ,  int                             aOrder
         ,  const std::vector<std::string>& aPotentials
         ,  const std::string&              aBounds
         ,  const std::vector<int>&         aReferenceIndices
         )
      {
         mOperatorFiles.emplace_back(aName, aDescription, aOrder, aPotentials, aBounds, aReferenceIndices);
      }
     
      //!@{
      //! Functions for reading input from Midas input file.
      bool ReadPotentialInput(std::istream&, std::string&, bool);
      bool ReadReferenceValues(std::istream&, std::string&);
      bool ReadDescription(std::istream&, std::string&);
      //!@}
      
      //! Get the maximum tensor operator order (Tensor order should be element 2 in potential_type)
      In GetMaxTensorOrder() const {return std::get<2>(*std::max_element(mOperatorFiles.begin(), mOperatorFiles.end(), 
                                    [](const auto& a, const auto& b){return std::get<2>(a) < std::get<2>(b);}));}
      
      //! Get vector of operator files
      const std::vector<potential_type>& OperatorFiles() const { return mOperatorFiles; }
      
      //! Get set of molecular input files
      const molecule::MoleculeFileInfo::Set& InputFiles() const { return mInputFiles; }
      
      //! Get set of molecular output files
      const molecule::MoleculeFileInfo::Set& OutputFiles() const { return mOutputFiles; }

      //! Get reference values file
      const std::string& GetReferenceValues() const { return mReferenceValues; }
      
      //! Get reference values file
      const std::string& GetDescription() const { return mDescription; }

      //! Get create unit test code
      bool GetCreateUnitTestCode() const { return mCreateUnitTestCode; }
      
      //! Get use include
      bool GetMidasPotUseInclude() const { return mMidasPotUseInclude; }
};

#endif /* FILECONVERSIONCALCDEF_H_INCLUDED */
