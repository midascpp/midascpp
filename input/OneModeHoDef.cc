/**
************************************************************************
* 
* @file                OneModeHoDef.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode HO Basis
* 
* Last modified: Thu Oct 26, 2006  10:14PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::transform;

#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OneModeHoDef.h"

typedef string::size_type Sst;

///< constructor
OneModeHoDef::OneModeHoDef()
{
   mImode  = -1;
   mBasType=""; 
   mBasAuxDefs = "";
   mLower  = 0;
   mHigher = 0;
   mOmeg   = C_1;
   mMass   = C_1;
   mNbas   = mHigher-mLower+1;
}
///< constructor ws 
OneModeHoDef::OneModeHoDef(In aMode, string aType, In aHigh, Nb aOmeg)
{
   mImode = aMode;
   mBasType=aType; 
   mLower  = I_0;
   mHigher = aHigh;
   mOmeg   = aOmeg;
   mMass   = C_1;
   // Transform tolower
   mBasAuxDefs = "";
   mNbas   = mHigher-mLower+1;
}
///< constructor
OneModeHoDef::OneModeHoDef(In aMode, string aType, string aAux)
{
   mImode = aMode;
   mBasType=aType; 
   mLower  = 0;
   mHigher = 0;
   mOmeg   = C_1;
   mMass   = C_1;
   // Transform tolower
   transform(aAux.begin(),aAux.end(),aAux.begin(),(In(*) (In))tolower);
   mBasAuxDefs = aAux;


   // Extract info from aAux
   vector<string> aux_st_vec;

   // Go backwards and find labels;
   do {
      string piece;
      Sst cfind = aAux.rfind(",");
      if (cfind!=aAux.npos) piece = aAux.substr(cfind);
      if (cfind==aAux.npos) piece = aAux;  // No , found take it all.
      Sst l_p = piece.size();
      while(piece.find(",")!= piece.npos) piece.erase(piece.find(","),I_1);
      aux_st_vec.push_back(piece);
      if (cfind!=aAux.npos) aAux.erase(cfind,l_p);
      if (cfind==aAux.npos) aAux.erase(I_0,l_p);
      //Mout << " in loop piece = " << piece << " and aAux " << aAux << endl;
   } while (aAux.size()>0);

   for (In i=0;i<aux_st_vec.size();i++)
   {
      // NB CURRENTLY n_low is ignored so nbas = higher+1.
      //Mout << " Checking: " << aux_st_vec[i] << " for useful information" << endl;
      //if (aux_st_vec[i].find("n_low=")!=aux_st_vec[i].npos) 
      //{
         //aux_st_vec[i].erase(aux_st_vec[i].find("n_low"),aux_st_vec[i].find("=")+1);
         //mLower=atoi(aux_st_vec[i].c_str());
      //}
      if (aux_st_vec[i].find("n_high=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("n_high"),aux_st_vec[i].find("=")+1);
         mHigher=atoi(aux_st_vec[i].c_str());
         if (gDebug) Mout << " mHigher found to be "  << mHigher << endl;
      }
      else if (aux_st_vec[i].find("omeg=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("omeg"),aux_st_vec[i].find("=")+1);
         Nb conv = C_1;
         CheckUnit(aux_st_vec[i],conv); // Check for conversion factor
         mOmeg=NbFromString(aux_st_vec[i])*conv;
         if (gDebug) Mout << " mOmeg found to be "  << mOmeg << endl;
      }
      else if (aux_st_vec[i].find("omeg22=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("omeg22"),aux_st_vec[i].find("=")+1);
         Nb conv = C_1;
         CheckUnit(aux_st_vec[i],conv); // Check for conversion factor
         Nb omeg22=NbFromString(aux_st_vec[i]);
         mOmeg=sqrt(C_2*omeg22);
         if (gDebug) Mout << " mOmeg found to be "  << mOmeg << endl;
      }
      else if (aux_st_vec[i].find("mass=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("mass"),aux_st_vec[i].find("=")+1);
         mMass=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " mMass found to be "  << mMass << endl;
      }
      else
      {
         Mout << " No useful information found in basis set auxiliary input:" << aux_st_vec[i] <<endl;
      }
   }
   mNbas   = mHigher-mLower+1;

   //Mout << " lower,higher,nbas: "<< mLower << " " << mHigher << " " << mNbas << endl;
}
/**
 * Evaluate the Hermite polynomials by recurrence
 * The normalization is according to Numerical Recipes in c++, pag. 159
 * */
Nb HermitePoly(In aIbas, Nb aQval)
{
   In j_max=aIbas;
   Nb p1=pow(C_1/C_PI,C_I_4);
   Nb p2=C_0;
   Nb p3;
   for (In j=0; j<j_max; j++)
   {
      p3=p2;
      p2=p1;
      p1=aQval*sqrt(C_2/Nb(j+1))*p2 - sqrt(Nb(j)/(j+1))*p3;
   }
   return p1;
}
/**
* Evaluate the Hermite polynomials by recurrence
* The normalization is according to Numerical Recipes in c++, pag. 159
**/
void HermitePoly(In aIbas, Nb aQval, MidasVector& arFuncVal)
{
   In j_max=aIbas;
   arFuncVal.SetNewSize(j_max+1);
   Nb p1=pow(C_1/C_PI,C_I_4);
   Nb p2=C_0;
   Nb p3;
   arFuncVal[0]=p1;
   for (In j=0; j<j_max; j++)
   {
      p3=p2;
      p2=p1;
      p1=aQval*sqrt(C_2/Nb(j+1))*p2 - sqrt(Nb(j)/(j+1))*p3;
      arFuncVal[j+1]=p1;
   }
   return;
}
/**
 * Get the values of the basis in a given point in space
**/
Nb OneModeHoDef::EvalHoBasis
   (  const In& aIbas
   ,  const Nb& aQval
   )  const
{
   Nb fval;
   //Mout << " Harmonic frequency: " << mOmeg << endl;
   //aQval is the unscaled coordinate
   Nb x=aQval;
   //Mout << " Mass-scaled coordinate x: " << x << endl;
   Nb gam=this->mOmeg;
   //Mout << " Gamm: " << gam << endl;
   Nb cst=pow(gam,C_I_4);
   fval=cst*exp(-C_I_2*gam*x*x)*HermitePoly(aIbas,sqrt(gam)*x);
   //debug
   //Nb fval_0=cst*HermitePoly(aIbas,C_0);
   //Mout << " Value of Hermite polynomial at zero is: " << HermitePoly(aIbas,C_0) << endl;
   //Mout << " Value of " << aIbas << " harmonic function at " << C_0 << " is: " << fval_0 << endl;
   return fval;
}
/**
 * Get the values of the basis for a MidasVector
**/
void OneModeHoDef::EvalHoBasis
   (  const In& aIbas
   ,  const MidasVector& arQval
   ,  MidasVector& arFuncVal
   )  const
{
   In n_pts=arQval.Size();
   //Mout << " Value of " << aIbas << " harmonic function at a grid of points " << endl;
   //Mout << " Nr. of points is: " << n_pts << endl;
   //Mout << " Harmonic frequency: " << mOmeg << endl;
   //aQval is the unscaled coordinate
   arFuncVal.SetNewSize(n_pts);
   Nb gam=mOmeg;
   Nb cst=pow(gam,C_I_4);
   for (In i=0; i<n_pts; i++)
   {
      Nb x=arQval[i];
      //Mout << " Mass-scaled coordinate x: " << x << endl;
      arFuncVal[i]=cst*exp(-C_I_2*gam*x*x)*HermitePoly(aIbas,sqrt(gam)*x);
      //Mout << " Value of " << aIbas << " harmonic function at " << x << " is " << arFuncVal[i] << endl;
   }
   return;
}
