#include "SinglePointCalcDef.h"

#include "input/GlobalData.h"
#include "potentials/PotentialHandler.h"

using namespace midas;

/**
 * SinglePointCalcDef constructor.
 *
 * @param aLevel   Level of Singlepoint keyword.
 **/
SinglePointCalcDef::SinglePointCalcDef
   (  int aLevel
   )
   :  CalcDef(aLevel)
{
}

/**
 * @brief create map of singlepoint keywords
 *        #.NAME: name of singlepoint
 *        #.TYPE: type if singlepoint (e.g. generic, dalton)
 *        #.ESSYMMETRYON: use es program symmetry (dalton) default: on
 *        #.ESSYMMETRYOFF: do NOT use es program symmetry (dalton)
 *        #.DALTONINPUT: input for dalton program, reads until #.ENDOFDALTONINPUT
 *        #.DALTONBASIS: basis input for dalton program
 *        #.PARTRIDGEPOT: use partridge water model pot (model)
 *        #.AMMMODELPOT: use AMM model pot (model)
 *        #.ARNTWOMODELPOT: use ArN_2 model pot (model)
 *        #.DOUBLEWELLPOT: use double well model pot (model)
 *        #.DOUBLEWELLDEFS: define double well pot, see manual for example (model)
 *        #.MORSEPOTENTIAL: use morse model pot (model)
 *        #.MORSEDEFS: define morse pot, see manual for example (model)
 *        #.INPUTCREATORSCRIPT: script for creating input for generic sp
 *        #.RUNSCRIPT: script for running generic sp
 *        #.PROPERTYINFO: File with information on the properties to be calculated
 *        #.VALIDATIONSCRIPT: script for validating generic sp (optional)
 *        #.SAVESPSCRATCHDIR: save single point scratch directory (default false)
 **/
input_map_t SinglePointCalcDef::Map
   (
   )
{
   input_map_t imap(CalcDef::Level());
   //
   imap.RegisterFunction(imap.MakeKeyword("NAME") 
                       , midas::input::BindReadString(mName)
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("TYPE") 
                       , midas::input::BindReadString(mSpInfo["PROGRAM"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("BOHRUNITS")
                       , midas::input::BindSetType(mSpInfo["BOHR"], std::string("TRUE"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("ESSYMMETRYON")
                       , midas::input::BindSetType(mSpInfo["ESSYM"], std::string("TRUE"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("ESSYMMETRYOFF")
                       , midas::input::BindSetType(mSpInfo["ESSYM"], std::string("FALSE"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DALTONINPUT")
                       , midas::input::BindReadStringUntilKeyword(mSpInfo["DALTONINPUT"], imap.MakeKeyword("ENDOFDALTONINPUT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DALTONBASIS")
                       , midas::input::BindReadStringUntilAnyKeyword(mSpInfo["BASIS"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("PARTRIDGEPOTENTIAL")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("PARTRIDGEPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("AMMMODELPOT")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("AMMMODELPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("ARNTWOMODELPOT")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("ARNTWOMODELPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DOUBLEWELLPOT")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("DOUBLEWELLPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DOUBLEWELLDEFS")
                       , midas::input::BindReadStringUntilAnyKeyword(mSpInfo["MODELPOTDEFS"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MORSEPOTENTIAL")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("MORSEPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MORSEDEFS")
                       , midas::input::BindReadStringUntilAnyKeyword(mSpInfo["MODELPOTDEFS"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("INPUTCREATORSCRIPT")
                       , midas::input::BindReadString(mSpInfo["INPUTCREATORSCRIPT"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("RUNSCRIPT")
                       , midas::input::BindReadString(mSpInfo["RUNSCRIPT"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("PROPERTYINFO")
                       , midas::input::BindReadString(mSpInfo["PROPERTYINFO"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("VALIDATIONSCRIPT")
                       , midas::input::BindReadString(mSpInfo["VALIDATIONSCRIPT"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("ROTATIONTHR")
                       , midas::input::BindReadString(mSpInfo["ROTATIONTHR"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("FILEPOTENTIAL")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("FILEPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("OPERATORFILE") 
                       , midas::input::BindReadStringUntilAnyKeyword(mSpInfo["OPERATORFILE"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MOLECULEFILE") 
                       , midas::input::BindReadString(mSpInfo["MOLECULEFILE"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("SETUPDIR") 
                       , midas::input::BindReadString(mSpInfo["SETUPDIR"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DYNLIBPOT")
                       , midas::input::BindSetType(mSpInfo["MODELPOT"], std::string("DYNLIBPOT"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("DYNLIBFILE") 
                       , midas::input::BindReadString(mSpInfo["DYNLIBFILE"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MLPROP")
                       , midas::input::BindReadString(mSpInfo["MLPROP"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MLVARIANCE")
                       , midas::input::BindReadString(mSpInfo["MLVARIANCE"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("FALLBACKINPUT")
                       , midas::input::BindReadString(mSpInfo["FALLBACKINPUT"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("FALLBACKRUN")
                       , midas::input::BindReadString(mSpInfo["FALLBACKRUN"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("MLDRIVER")
                        , midas::input::BindReadString(mSpInfo["MLDRIVER"])
                        );
   //
   imap.RegisterFunction(imap.MakeKeyword("MLPES")
                       , midas::input::BindSetType(mSpInfo["MLPES"], std::string("FALSE"))
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("SAVEDIR")
                       , midas::input::BindReadString(mSpInfo["SAVEDIR"])
                       );
   //
   imap.RegisterFunction(imap.MakeKeyword("SAVESPSCRATCHDIR")
                       , midas::input::BindSetType(mSpInfo["SAVESPSCRATCHDIR"], std::string("TRUE"))
                       );
   return imap;
}

//
extern vector<SinglePointCalcDef> gSinglePointCalcDef;

/**
 *
 **/
void AfterProcessSinglePointInput
   (
   )
{
   midas::global::ServiceLocator service_locator;

   for(auto& spcalcdef : gSinglePointCalcDef)
   {
      auto& spinfo = spcalcdef.mSpInfo;
      
      auto iter = spinfo.find("PROGRAM");
      if(iter != spinfo.end())
      {
         if(iter->second == "SP_MODEL")
         {
            if(!service_locator.HasService<potentials::PotentialHandler>())
            {
               service_locator.AddService(potentials::PotentialHandler{});
            }

            auto& potential_handler = service_locator.GetService<potentials::PotentialHandler>();

            spinfo.emplace("MODELPOT_INSTANCE", std::to_string(potential_handler.LoadPotential(spinfo)));
         }
      }
   }
}
