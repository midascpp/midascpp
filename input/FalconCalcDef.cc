/**
************************************************************************
* 
* @file                FalconCalcDef.cc
*
* 
* Created:             25-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Setting itersys calculation definition
*
* Last modified:       07-07-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
#include <map>
#include "util/Io.h"
#include "input/FalconCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/FindKeyword.h"

namespace midas::input
{

/* **********************************************************************
********************************************************************** */
void FalconCalcDef::SetModeOptType (const std::string& arStr)
{
   const std::map<std::string,ModeOptType> stringtoparam
   {
      {"NO",ModeOptType::NO},
      {"HESSIAN",ModeOptType::HESSIAN},
      {"ROTCOORD",ModeOptType::ROTCOORD},
   };

   ModeOptType input = midas::input::FindKeyword(stringtoparam, arStr);
 
   switch(input)
   {
      case ModeOptType::NO:
      {
         mModeOptType = ModeOptType::NO;
         break;
      }
      case ModeOptType::HESSIAN:
      {
         mModeOptType = ModeOptType::HESSIAN;
         break;
      }
      case ModeOptType::ROTCOORD:
      {
         mModeOptType = ModeOptType::ROTCOORD;
         break;
      }
      default:
      {
         Mout << "Unknown ModeOptType scheme: " << arStr << endl;
         MIDASERROR("No known conversion to In for ModeOptType scheme");
      }
   }
}

/* **********************************************************************
********************************************************************** */
void FalconCalcDef::SetHessType (const std::string& arStr)
{
   const std::map<std::string,HessType> stringtoparam
   {
      {"NO",HessType::NO},
      {"GIVEN",HessType::GIVEN},
      {"CALC",HessType::CALC},
   };

   HessType input = midas::input::FindKeyword(stringtoparam, arStr);
 
   switch(input)
   {
      case HessType::NO:
      {
         mHessType = HessType::NO;
         break;
      }
      case HessType::GIVEN:
      {
         mHessType = HessType::GIVEN;
         break;
      }
      case HessType::CALC:
      {
         mHessType = HessType::CALC;
         break;
      }
      default:
      {
         Mout << "Unknown HessType scheme: " << arStr << endl;
         MIDASERROR("No known conversion to In for HessType scheme");
      }
   }
}

/* **********************************************************************
********************************************************************** */
void FalconCalcDef::SetCoupEstType (const std::string& arStr)
{
   const std::map<std::string,CoupEstType> stringtoparam
   {
      {"DIST",CoupEstType::DIST},
      {"DISTACT",CoupEstType::DISTACT},
      {"NUCCOUPHESSNORM",CoupEstType::NUCCOUPHESSNORM},
      {"NUCCOUPHESSNORMACT",CoupEstType::NUCCOUPHESSNORMACT}
   };

   CoupEstType input = midas::input::FindKeyword(stringtoparam, arStr);
 
   switch(input)
   {
      case CoupEstType::DIST:
      {
         mCoupEstType = CoupEstType::DIST;
         break;
      }
      case CoupEstType::DISTACT:
      {
         mCoupEstType = CoupEstType::DISTACT;
         break;
      }
      case CoupEstType::NUCCOUPHESSNORM:
      {
         mCoupEstType = CoupEstType::NUCCOUPHESSNORM;
         break;
      }
      case CoupEstType::NUCCOUPHESSNORMACT:
      {
         mCoupEstType = CoupEstType::NUCCOUPHESSNORMACT;
         break;
      }
      default:
      {
         Mout << "Unknown CoupEstType scheme: " << arStr << endl;
         MIDASERROR("No known conversion to In for CoupEstType scheme");
      }
   }
}

/* **********************************************************************
********************************************************************** */
ostream& operator<<(ostream& os,const ModeOptType& arModeOptType)
{
   switch (arModeOptType)
   {
      case ModeOptType::NO:
      {
         os<< "NO" << endl;
         break;
      }
      case ModeOptType::HESSIAN:
      {
         os<< "Hessian" << endl;
         break;
      }
      case ModeOptType::ROTCOORD:
      {
         os<< "RotCoord" << endl;
         break;
      }
      default:
      {
         MIDASERROR("Unknown ModeOptType");
         break;
      }
   }
   return os;
}

/* **********************************************************************
********************************************************************** */
ostream& operator<<(ostream& os,const HessType& arHessType)
{
   switch (arHessType)
   {
      case HessType::NO:
      {
         os<< "NO" << endl;
         break;
      }
      case HessType::GIVEN:  
      {
         os<< "GIVEN" << endl;
         break;
      }
      case HessType::CALC:
      {
         os<< "calculate Hessian using FreqAna" << endl;
         break;
      }
      default:
      {
         MIDASERROR("Unknown HessType");
         break;
      }
   }
   return os;
}

/* **********************************************************************
********************************************************************** */
ostream& operator<<(ostream& os,const CoupEstType& arCoupEstType)
{
   switch (arCoupEstType)
   {
      case CoupEstType::DIST:
      {
         os<< "Distance " << endl;
         break;
      }
      case CoupEstType::DISTACT:
      {
         os<< "Distance to any active atom" << endl;
         break;
      }
      case CoupEstType::NUCCOUPHESSNORM:
      {
         os<< "Atom-atom coupling in Hessian" << endl;
         break;
      }
      case CoupEstType::NUCCOUPHESSNORMACT:
      {
         os<< "Atom-atom coupling in Hessian to any active atom" << endl;
         break;
      }
      default:
      {
         MIDASERROR("Unknown CoupEstType");
         break;
      }
   }
   return os;
}

} //namespace midas::input

