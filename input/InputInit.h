/**
************************************************************************
* 
* @file                InputInit.h
*
* Created:             28-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store input logical information 
* 
* Last modified:       17-12-2014 (C.Koenig)
*
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INPUT_INIT_H
#define INPUT_INIT_H

// Standard Headers
#include <string>
#include <vector>
#include <map>
#include <list>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/ProductTerm.h"
#include "input/OpDef.h"
#include "input/OneModeBasDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/BasDef.h"
#include "input/VscfCalcDef.h"
#include "input/PesCalcDef.h"
#include "input/FitBasisCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/TdHCalcDef.h"
#include "input/McTdHCalcDef.h"
#include "input/ModSysCalcDef.h"
#include "input/EcorCalcDef.h"
#include "input/SystemDef.h"
#include "input/GeoOptCalcDef.h"
#include "input/MLCalcDef.h"
#include "input/TdecompCalcDef.h"
#include "input/FileConversionCalcDef.h"
#include "input/AnalysisCalcDef.h"
#include "input/SinglePointCalcDef.h"
#include "input/MolStruct.h"
#include "nuclei/AtomicData.h"
#include "test/TestDrivers.h"
#ifdef VAR_HAS_TINKER
#include "tinker_interface/TinkerMM.h"
#endif

using std::string;
using std::vector;
using std::map;
using std::list;

/**
* Section logicals 
* */
bool      gDoTest;               ///< Do Test of various sections.
bool      gDoPes;                ///< Do Pes studies 
bool      gDoAnalysis;           ///< Do Analysis calcs.  
bool      gDoVib;                ///< Do vibrational structure calculation.
bool      gDoElec;               ///< Do electronic structure calculation.


/**
* General options. 
* */
bool           gGeneral;            ///< General
bool           gDebug;              ///< Debug - write out immense ammounts of shit
In             gIoLevel;            ///< Global IO level.
bool           gTime;               ///< Time whatever you do....
bool           gTechInfo;           ///< Gives information on the size of Nb, In and STL containers
In             gMaxBufSize;         ///< Maximum size of intermediate memory buffers used (DataCont)
In             gMaxFileSize;        ///< Maximum size of files used (DataCont)
bool           gTempSet;            ///< Temperature has been set - average 
Nb             gTempFirst;          ///< First or only Temperature 
Nb             gTempLast;           ///< Last Temperature 
In             gTempSteps;          ///< Nr. of Temperatures 
std::string    gNumLinAlg;          ///< Choice of lin.alg. methods 
std::string    gMainDir;            ///< Global savedir directory location
std::string    gSaveDir;            ///< Global savedir directory location
std::string    gAnalysisDir;        ///< Global analysis directory location  

/**
* Test input options. 
* */
In gTestIoLevel;          ///< Local test IO level (>= global IO level)
In gTestVectorDim;        ///< Vector dimensions used in the large Test(timing)
In gTestMatrixDimM;       ///< Vector dimensions used in the large Test(timing)
In gTestMatrixDimN;       ///< Vector dimensions used in the large Test(timing)
midas::test::TestDrivers gTestDrivers;

/**
* Pes input options. 
* */
PesCalcDef gPesCalcDef;          ///< Global PesCalcDef
In        gPesIoLevel;           ///< Local Pes IO level (>= global IO level)

/**
 * FitBasis input options.
**/
FitBasisCalcDef gFitBasisCalcDef;   ///< Global FitBasisCalcDef
In gFitBasisIoLevel;                ///< Local FitBasis IO

/**
* Analysis input options. 
* */
string    gOutLink;              ///< Filename for linking of output
In        gAnalysisIoLevel;      ///< Local Analysis IO level (>= global IO level)
In        gExtHistogram;         ///< Number of histogram levels.
Nb        gAnalysisFwhm;         ///< Full with half max.
Nb        gAnalysisSpeBegin;     ///< Full with half max.
Nb        gAnalysisSpeEnd;       ///< Full with half max.
bool      gNoPs;                 ///< No Ps

/**
* Vib input options. 
* */
In        gVibIoLevel;           ///< Local vib IO level (>= global IO level)
In        gReserveNterms;        ///< Provide reserve STL for # terms in opers.
In        gReserveNmodes;        ///< Provide reserve STL for # modes in opers.

/**
* Oper input options. 
* */
bool      gDoOper;               ///< Do Oper calculation.
In        gOperIoLevel;          ///< Local Oper IO level (>= global IO level,vib IO level)
GlobalOperatorDefinitions gOperatorDefs; ///< Global Operator Definitions, contains all the operators defined in the calculation

/**
* Basis options. 
* */
In        gBasisIoLevel;         ///< Local Basis IO level (>= global IO level,vib IO level)
std::vector<BasDef> gBasis;            ///< The Basis set defs.
In        gReserveNdefs;         ///< Reserve space for STL containers
std::vector<OneModeHoDef> gOneModeHoDefs;       ///< Vector for Ho basis defs.
std::vector<OneModeGaussDef> gOneModeGaussDefs; ///< Vector for gauss basis defs.
std::vector<OneModeBsplDef> gOneModeBsplDefs; ///< Vector for B-spline basis defs.
std::vector<OneModeStateBasis> gOneModeStateBasis; ///< Vector of state-basis defs.

/**
 * Vscf input options. 
 **/
bool      gDoVscf;               ///< Do VSCF calculation.
In        gVscfIoLevel;          ///< Local VSCF IO level (>= global IO level,vib IO level)
vector<VscfCalcDef> gVscfCalcDef;///< The calculations defs. 

/**
* Vcc input options. 
* */
bool      gDoVcc;                ///< Do Vcc  calculation.
In        gVccIoLevel;           ///< Local Vcc  IO level (>= global IO level,vib IO level)
vector<VccCalcDef> gVccCalcDef;  ///< The calculations defs. 

/**
* TdH input options. 
* */
bool      gDoTdH;                ///< Do Vcc  calculation.
In        gTdHIoLevel;           ///< Local Vcc  IO level (>= global IO level,vib IO level)
vector<input::TdHCalcDef> gTdHCalcDef;  ///< The calculations defs. 

/**
* McTdH input options. 
* */
bool      gDoMcTdH;                ///< Do MCTDH calculation.
In        gMcTdHIoLevel;           ///< Local MCTDH IO level (>= global IO level,vib IO level)
std::vector<input::McTdHCalcDef> gMcTdHCalcDef;  ///< The calculations defs. 

/**
 * TDVCC input options.
 **/
bool      gDoTdvcc;                ///< Do Vcc  calculation.
In        gTdvccIoLevel;           ///< Local Vcc  IO level (>= global IO level,vib IO level)
std::vector<input::TdvccCalcDef> gTdvccCalcDef;  ///< The calculations defs.

/**
* ModSystem input options. 
* */
bool      gDoSystem   ;          ///< Do modification of the system 
In        gSystemIoLevel;        ///< Local ModSys  IO level
vector<SystemDef> gSystemDefs;  ///< The calculations defs.
vector<input::ModSysCalcDef> gModSysCalcDefs;  ///< The calculations defs.

/**
 * Elec input options. 
**/
In        gElecIoLevel;           ///< Local vib IO level (>= global IO level)

/** 
 * B spline basis flags
**/
bool gDoBspline;

/**
* Molecular geometry input options
**/
MolStruct gMolStruct;

/**
* Ecor input options. 
* */
bool      gDoEcor;                ///< Do Ecor (electron correlation)  calculation.
In        gEcorLevel;             ///< Local SCF IO level (>= global IO level,vib IO level)
vector<EcorCalcDef> gEcorCalcDef; ///< The calculations defs.

/**
 * GeoOpt input options.
 **/
bool      gDoGeoOpt;                   ///< Do a geometry optimization
In        gGeoOptLevel;                ///< Local IO level (>= global IO level,vib IO level)
vector<GeoOptCalcDef> gGeoOptCalcDef;  ///< The calculations defs.
/**
 * Machine Learning input options.
 **/
bool      gDoMLTask;                   ///< Do a geometry optimization
In        gMLLevel;                    ///< Local IO level (>= global IO level,vib IO level)
vector<MLCalcDef> gMLCalcDef;          ///< The calculations defs.
/**
* Tdecomp input options. 
**/
bool      gDoTdecomp;                   ///< use user itnerace for tensor decompositions
In        gTdecompLevel;                ///< Local IO level (>= global IO level,vib IO level)
vector<TdecompCalcDef> gTdecompCalcDef; ///< The calculations defs.

/**
 * Analysis options
 **/
vector<AnalysisCalcDef> gAnalysisCalcDef;

/**
 *
 **/
bool gDoFileConversion = false;
std::vector<FileConversionCalcDef> gFileConversionCalcDef;

FileConversionCalcDef& CreateFileConversionCalcDef()
{
   gDoFileConversion = true;
   gFileConversionCalcDef.emplace_back(); // create calcdef
   return gFileConversionCalcDef.back(); // and return calcdef for input
}

/**
 *
 **/
std::vector<SinglePointCalcDef> gSinglePointCalcDef;

SinglePointCalcDef& CreateSinglePointCalcDef(In aLevel)
{
   gSinglePointCalcDef.emplace_back(aLevel); // create calcdef
   return gSinglePointCalcDef.back(); // and return calcdef for input
}

const SinglePointCalcDef& SinglePointCalc(const std::string& aName)
{
   for(int i = 0; i < gSinglePointCalcDef.size(); ++i)
   {
      if(gSinglePointCalcDef[i].Name() == aName)
      {
         return gSinglePointCalcDef[i];
      }
   }
   MIDASERROR("Could not find SinglePointCalcDef with name: " + aName);
   return gSinglePointCalcDef[0]; /* will never reach here */
}



/**
* General utilities. 
* */
AtomicData gAtomicData; 

#endif

