/**
************************************************************************
* 
* @file                ModSysCalcDef.cc
*
* 
* Created:             16-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Calculation definition for modification and 
*                      generation of coordinates, 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
using std::string;
#include <map>
using std::map;
#include "util/Io.h"
#include "input/ModSysCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/FindKeyword.h"

/**
 *
**/
void input::ModSysCalcDef::SetVibCoordScheme(const std::string& aStr)
{
   const std::map<std::string,VibCoordScheme> stringtoparam
   {
      {"ROTCOORD",input::VibCoordScheme::ROTCOORD},
      {"FALCON",input::VibCoordScheme::FALCON},
      {"ROTATEMOLECULE",input::VibCoordScheme::ROTATEMOLECULE},
      {"FREQANA",input::VibCoordScheme::FREQANA},
      {"TRANSFORMPOT",input::VibCoordScheme::TRANSFORMPOT},
      {"LHA", input::VibCoordScheme::LHA},
      {"NORMALCOORDINATES", input::VibCoordScheme::NORMALCOORDINATES}
   };

   try
   {
      mVibCoordScheme = stringtoparam.at(aStr);
   }
   catch (  const std::out_of_range& err
         )
   {
      MIDASERROR("Unknown VibCoordScheme: '" + aStr + "'");
   }
}


