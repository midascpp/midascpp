/**
************************************************************************
* 
* @file                FitBasisCalcDef.cc
* 
* Created:             18-05-2018
*
* Author:              Emil Lund Klinting (klintg@chem.au.dk)
*
* Short Description:   Calculation definitions for fit-bases
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

//std headers
#include <vector>
#include <string>

// midas headers
#include "input/FitBasisCalcDef.h"

/**
 *
**/
FitBasisCalcDef::FitBasisCalcDef()
{
   mAddFitFuncsConserv           = false;       
   mAutomaticFitBasis            = false;
   mAutomaticFitFuncs            = false;
   mFitAllMcIterGrids            = false;
   mFitFuncsMaxOrder             = {I_12};
   mFitBasisMaxOrder             = {I_12, I_12, I_12, I_12, I_12, I_12};
   mFitFuncsMuMaxOrder           = {I_4};
   mFitBasisMuMaxOrder           = {I_4, I_4, I_4, I_4, I_4, I_4};
   mFitMethod                    = "SVD";
   mUseGenFitBasis               = false;
   mFitBasName                   = "DEFAULT_FITBASIS_NAME";
   mUseSpecialNthreadsFit        = false;
   mNthreadsFit                  = I_0;
   mDoSvdCut                     = true;
   mDoTikhonov                   = false; 
   mSvdCutoffThr                 = C_I_10_13;
   mTikhonovParam                = C_I_10_13;
}

/**
 *
**/
void FitBasisCalcDef::ValidateFitBasisInput
   (  const In& aNoVibs
   ,  const bool& aDoFragmentedPesCalcs
   ,  const In& aMaxModeCoupling
   ,  const std::string& aPesFitBasisName
   ,  const bool& aCalcEffInertiaInv
   )
{
   // Check that correct fit-basis names are given and that they make sense 
   if (mFitBasName != aPesFitBasisName)
   {
      MIDASERROR("The fit-basis specified under the #2 UseFitBasis keyword: " + aPesFitBasisName + ", does not match any known fit-basis specified under the #3 Name keyword in the #2 FitBas block: " + mFitBasName + "!");
   }

   // The required number of fit-basis functions might change from fragment to fragment, and we therefore reset the number here
   if (   mFitFuncsMaxOrder.size() < aNoVibs 
      &&  mFitFuncsMaxOrder.size() > 0
      )
   { 
      In max_order = mFitFuncsMaxOrder[I_0]; 
      
      // the size corresponds to the largest number of vib modes per fragment
      In diff = aNoVibs - mFitFuncsMaxOrder.size();
      for (In j = I_0; j < diff; ++j)
      {
         mFitFuncsMaxOrder.push_back(max_order);
      }
   }
   else if (mFitFuncsMaxOrder.size() == 0) 
   {
      MIDASERROR("The specified set of maximum fit-basis function orders have size " + std::to_string(mFitFuncsMaxOrder.size()) + ", which is an invalid size!");
   }

   if (mFitBasisMaxOrder.size() < aMaxModeCoupling)
   {
      if (mFitBasisMaxOrder.size() == I_1)
      {
         for (In j = I_1; j < aMaxModeCoupling; j++)
         {
            mFitBasisMaxOrder.push_back(mFitBasisMaxOrder[I_0]);
         }
      }
      else
      {
         MIDASERROR("Please ensure that the number of elements given under the #3 FitBasisMaxorder keyword matches the dimensionality of the potential energy/molecular property surface");
      }
   }

   // Check if special fitting parameters are selected for inertia components
   if (aCalcEffInertiaInv)
   {
      if (mFitFuncsMuMaxOrder.size() == I_1)
      {
         In max_inertia_order = mFitFuncsMuMaxOrder[I_0];
         for (In j = I_1; j < aNoVibs; j++)
         {
            mFitFuncsMuMaxOrder.push_back(max_inertia_order);
         }
      }
      else if (  mFitFuncsMuMaxOrder.size() != aNoVibs
              && mFitFuncsMuMaxOrder.size() != I_1 
              && mFitFuncsMuMaxOrder.size() != I_0
              )
      {
         MIDASERROR ("The specified set of maximum fit-basis function orders used for fitting the moments of inertia is of invalid size!");
      }

      if (mFitBasisMuMaxOrder.size() < aMaxModeCoupling)
      {
         if (mFitBasisMuMaxOrder.size() == I_1)
         {
            for (In j = I_1; j < aMaxModeCoupling; j++)
            {
               mFitBasisMuMaxOrder.push_back(mFitBasisMuMaxOrder[I_0]);
            }
         }
         else
         {
            MIDASERROR("Please ensure that the number of elements given under the #3 FitBasisEffInertiaInvMaxOrder keyword matches the dimensionality of the potential energy/molecular property surface");
         }
      }
   }

   // Check that the individual entries for the number and order of fit-basis functions minimally allows for full coupling
   for (In i = I_0; i < aMaxModeCoupling; i++)
   {
      if (mFitFuncsMaxOrder[i] < I_1 + i)
      {
         MIDASERROR("The number of fit-basis functions requested for mode combination level " + std::to_string(I_1 + i) + " does not allow full coupling of the modes");
      }
      
      if (mFitBasisMaxOrder[i] < I_1 + i)
      {
         MIDASERROR("The fit-basis order requested for mode combination level " + std::to_string(I_1 + i) + " does not allow full coupling of the modes");
      }

      if (aCalcEffInertiaInv)
      {
         if (mFitFuncsMuMaxOrder[i] < I_1 + i)
         {
            MIDASERROR("The number of fit-basis functions requested for mode combination level " + std::to_string(I_1 + i) + " does not allow full coupling of the modes when fitting the effective inertia inverse elements");
         }
      
         if (mFitBasisMuMaxOrder[i] < I_1 + i)
         {
            MIDASERROR("The fit-basis order requested for mode combination level " + std::to_string(I_1 + i) + " does not allow full coupling of the modes when fitting the effective inertia inverse elements");
         }
      }
   }
 
   // Check that the overlapping keywords are not set, (this is not really critical but a safety precaution)
   if (mUseGenFitBasis && mAutomaticFitBasis)
   {
      MIDASERROR("The #3 AutomaticFitBasis and #3 GenFitBasisDef keywords cannot be specified at the same time, please adjust your input");
   }

   // Set default information on modes using general fit-basis definitions, i.e. an empty vector for all modes
   mGenFitBasisModeMap.assign(aNoVibs, {});

   // Check that general fit-basis settings are correct
   if (mUseGenFitBasis)
   {
      for (In i = I_0; i < mGenFitBasisCalcDefs.size(); i++)
      {
         if (  mGenFitBasisCalcDefs[i].GetmFitProperties().empty()
            || mGenFitBasisCalcDefs[i].GetmFitModes().empty() 
            || mGenFitBasisCalcDefs[i].GetmGenFitFunctions().empty()
            ) 
         {
            MIDASERROR("Either #4 Props, #4 Modes or #4 FitFunctions have been left empty under #3 BasDef!"); 
         };

         // Store information on which modes have a general fit-basis definition and which GenFitBasisCalcDef they belong to
         std::vector<In> gen_fit_basis_modes = mGenFitBasisCalcDefs[i].GetmFitModes();
         for (In j = I_0; j < gen_fit_basis_modes.size(); j++)
         {
            In gen_fit_basis_mode = gen_fit_basis_modes[j];

            mGenFitBasisModeMap[gen_fit_basis_mode].push_back(i);
         }
      }
   }

   // If automatic determination of the number of fit-basis functions, then we need to make sure that the program can check a range of different settings
   if (mAutomaticFitFuncs)
   {
      for (In i = I_0; i < aNoVibs; i++)
      {
         if (mFitFuncsMaxOrder[i] < I_16)
         {
            mFitFuncsMaxOrder[i] = I_16;
         }
      }

      for (In j = I_0; j < aMaxModeCoupling; j++)
      {
         if (mFitBasisMaxOrder[j] < I_16)
         {
            mFitBasisMaxOrder[j] = I_16;
         }
      }
      
      if (gFitBasisIoLevel > I_3)
      {
         Mout << " The maximum order in the fit-basis functions for each mode combination have been reset to a non-default value to allow the automatic procedure to be more flexible " << std::endl;
      }
   }
   
   // Define one-mode fit-basis sets in accordance with the input-name specifier
   if (mAutomaticFitBasis)
   {
      for (In i = I_0; i < mAutoFitType.size(); ++i)
      {
         // For Morse fit-basis functions we need to define both the functions, non-linear parameters and an optimization function
         if (mAutoFitType[i] == "MORSE")
         {
            mGenFitBasisCalcDefs[i].SetmGenFitFunctions(midas::util::StringVectorFromString("EXPAND(EXP(-$i*alpha*Q),$i,[1.." + std::to_string(mFitBasisMaxOrder[I_0]) + ";1])"));

            mGenFitBasisCalcDefs[i].SetmNonLinOptFunc("De*(1-EXP(-alpha*Q))*(1-EXP(-alpha*Q))");

            std::vector<std::string> non_lin_opt_params = {"Q", "alpha", "De"};
            mGenFitBasisCalcDefs[i].SetmNonLinOptParams(non_lin_opt_params);
            
            continue;
         }
         else
         {
            MIDASERROR("The indicated fit-basis function type " + mAutoFitType[i] + ", under the #3 AutomaticFitBasis keyword is unknown!");
         }
      }
   }

   // Output for the final settings as used in the calculation
   if (gFitBasisIoLevel > I_4)
   {
      Mout << " Maximum order in the fit-basis used to fit the potential energy surfaces of increasing mode combination level is set to: " << mFitBasisMaxOrder << std::endl;
            
      if (aCalcEffInertiaInv)
      {
         Mout << " Maximum order in the fit-basis used to fit the moments of inertia tensor element surfaces of increasing mode combination level is set to: " << mFitBasisMuMaxOrder << std::endl;
      }
   }
   if (gFitBasisIoLevel > I_11)
   {
      Mout << " Maximum order in the fit-basis functions for each mode is:  " << std::endl;
      for (In i = I_0; i < mFitFuncsMaxOrder.size(); i++)
      {
         Mout << "  Mode Q"  << i << ", maximum order: " << mFitFuncsMaxOrder[i] << std::endl;
      }
      Mout << std::endl;
   }

   // Check that the fit method is set to a known method
   if (mFitMethod == "SVD")
   {
      if (gFitBasisIoLevel > I_8)
      {
         Mout << " Linear fit method is by MidasCpp SVD procedure " << std::endl;
      }
   }
   else if (mFitMethod == "NORMALEQ")
   {
      if (gFitBasisIoLevel > I_8)
      {
         Mout << " Linear fit method is by parameter extraction by normal equation solution " << std::endl;
      }
   }
   else if (mFitMethod == "SVDGELSS")
   {
      if (gFitBasisIoLevel > I_8)
      {
         Mout << " Linear fit method is by LaPack SVD procedure " << std::endl;
      }
   }
   else
   {
      MIDASERROR("The chosen fit method in unknown!");
   }

   // Check that the constants are set correct 
   if (!mFitBasisConstants.empty())
   {
      auto it_map_of_constants = mFitBasisConstants.begin();

      if (!it_map_of_constants->second)
      {
         Mout << " The fit-basis constants that have been read from input are: " << std::endl;
         
         for (auto it = mFitBasisConstants.begin(); it != mFitBasisConstants.end(); ++it)
         {
            Mout << *it << std::endl;
         }
         MIDASERROR("Duplicate entry in map of fit-basis constants");
      }
   }
   
   // Check that the functions are set correct 
   if (!mFitFuncs.empty())
   {
      auto it_map_of_functions = mFitFuncs.begin();

      if (it_map_of_functions->second.empty())
      {
         Mout << " The fit-basis functions that have been read from input are: " << std::endl;
         
         for (auto it = mFitFuncs.begin(); it != mFitFuncs.end(); ++it)
         {
            Mout << *it << std::endl;
         }
         MIDASERROR("Duplicate entry in map of fit-basis functions");
      }
   }

   Mout << std::endl;
}
