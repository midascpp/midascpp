#include "input/OperatorInput.h"

#include <vector>
#include <string>

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/Error.h"
#include "util/FileSystem.h"
#include "input/Input.h"
#include "input/ProgramSettings.h"
#include "mpi/FileToString.h"
#include "mpi/FileSystemMpi.h"

using std::string;
using std::vector;

namespace midas
{
namespace input
{

/**
 * Read an operator from a file 
 * Note: for linear molecules only pi_x and pi_y are considered. If the I_zz
 * component is zero, then it is a linear molecule. Follow the treatment of Watson
 * for a linear molecule.
 **/
void AddCoriolisTerms
   (  std::vector<Nb>& arInertia
   ,  std::string oper_file_name
   ,  OpDef& arOpDef
   )
{
   std::vector<Nb> mu(I_3);
   Nb thresh = C_10_5;
   bool is_linear = false;
   In i_c = I_0;
   for (In i = I_0; i < I_3; i++)
   {
      if (arInertia[i] <= C_NB_EPSILON*thresh)
      {
         i_c++;
      }
   }
   if (i_c > I_1)
   {
      MIDASERROR(" Two inertia principal moments are zero! ");
   }
   if (i_c == I_1)
   {
      is_linear = true;
   }
   if (!is_linear)
   {
      for (In i = I_0; i < I_3; i++)
      {
         mu[i] = C_1/arInertia[i];
      }
   }
   else if (is_linear)
   {
      for (In i = I_0; i < I_2; i++)
      {
         mu[i] = C_1/arInertia[i];
      }

      mu[I_2] = C_0;
   }
   
   if (gOperIoLevel > I_5 || gDebug)
   {
      Mout << " Inverse moment of inertia diagonal tensor elements " << mu << std::endl;
   }

   In nmodes = arOpDef.NmodesInOp();

   // If the electronic DOF is counted as a mode in the OpDef, decrement by 1
   if (  gOperatorDefs.ElectronicDofLabel() == ""
      )
   {
      --nmodes;
   }
   In nm2 = nmodes*nmodes;
   std::vector<Nb> zeta(I_3*nm2, C_0);

   std::istringstream oper_inp_file = midas::mpi::FileToStringStream(oper_file_name);
   if (oper_inp_file.fail() || oper_inp_file.str().empty())
   {
      MIDASERROR(" File with coriolis operator not found");
   }
   
   if (gOperIoLevel > I_5 || gDebug)
   {
      Mout << " Coriolis term file: " << oper_file_name.c_str() << std::endl;
   }
   
   std::string s = "";
   // Get new lines with mode and operators defs.
   while (midas::input::GetLine(oper_inp_file, s)) 
   {
      istringstream input_s(s);
      In alpha;
      std::string is = "";
      std::string js = "";

      input_s >> alpha;
      input_s >> is;
      input_s >> js;
      
      In i_g_mode = gOperatorDefs.ModeNr(is, true);
      In i = arOpDef.mOpModeMap[i_g_mode];
      
      i_g_mode = gOperatorDefs.ModeNr(js, true);
      In j = arOpDef.mOpModeMap[i_g_mode];
      
      Nb coef;
      input_s >> coef; 
      
      alpha--;
      In addij = i*nmodes + j;
      zeta[nm2*alpha + addij] = coef;
      
      In addji = j*nmodes + i;
      zeta[nm2*alpha + addji] = -coef;
   }
   
   /*
   Mout << " The zeta matrix " << endl;
   for (In alpha=I_0;alpha<I_3;alpha++) 
   {
      Mout << "alpha = " << alpha << endl;
      for (In i=I_0;i<nmodes;i++)
      {
         for (In j=I_0;j<nmodes;j++)
         {
            Mout << setw(12) << zeta[nm2*alpha+i*nmodes+j] << " ";
         }
         Mout << endl;
      }
   }
   */


   for (LocalModeNr i=I_0;i<nmodes;i++)
   {
      for (LocalModeNr j=i+I_1;j<nmodes;j++)
      {
         In addij = i*nmodes + j;
         for (LocalModeNr k=I_0;k<nmodes;k++)
         {
            for (LocalModeNr l=k+I_1;l<nmodes;l++)
            {
               In addkl = k*nmodes + l;
               //if (addkl < addij) continue; // Use z is symmetric

               Nb z = C_0;
               for (In alpha=I_0;alpha<I_3;alpha++) 
               {
                  z -= mu[alpha]*zeta[nm2*alpha+addij]*zeta[nm2*alpha+addkl];
               }
               // Minus account for that we are using ddq instead of p i^2 
               // remember we always have p squared in the coriolis term.
               //z = z/C_2;  However, coriolis = 1/2 sum_ij sum_kl but
               //for ij != kl the one half is taken by the contribution 
               //from other side.
               //if (addkl== addij) 
               Nb omeg_ik=C_1;
               Nb omeg_il=C_1;
               Nb omeg_jk=C_1;
               Nb omeg_jl=C_1;
               if(arOpDef.ScaledPot())
               {
                  //Mout << " Coriolis coefficients will be accordingly scaled: " << endl;
                  omeg_ik*=arOpDef.ScaleFactorForMode(i)*arOpDef.ScaleFactorForMode(k);
                  omeg_il*=arOpDef.ScaleFactorForMode(i)*arOpDef.ScaleFactorForMode(l);
                  omeg_jk*=arOpDef.ScaleFactorForMode(j)*arOpDef.ScaleFactorForMode(k);
                  omeg_jl*=arOpDef.ScaleFactorForMode(j)*arOpDef.ScaleFactorForMode(l);
               }
               z = z/C_2;
               if (fabs(z)>C_NB_EPSILON)
               {
                  //Mout << " i,j,k,l,z " << i << " " << j << " " << k 
                  //<< " " << l << " " << z << endl;
                  AddOneCorTerm(i,j,k,l,z/omeg_ik,arOpDef);
                  AddOneCorTerm(j,i,k,l,-z/omeg_jk,arOpDef);
                  AddOneCorTerm(i,j,l,k,-z/omeg_il,arOpDef);
                  AddOneCorTerm(j,i,l,k,z/omeg_jl,arOpDef);
               }
            }
         }
      }
   }
}

/**
 * Put a corioslis combination on the oper list.
 *
 * @param arI
 * @param arJ
 * @param arK
 * @param arL
 * @param aZ
 * @param arOpDef
 **/
void AddOneCorTerm
   (  const In& arI
   ,  const In& arJ
   ,  const In& arK
   ,  const In& arL
   ,  Nb aZ
   ,  OpDef& arOpDef
   )
{
   GlobalModeNr i1=arOpDef.GetGlobalModeNr(arI);
   GlobalModeNr i2=arOpDef.GetGlobalModeNr(arJ);
   GlobalModeNr i3=arOpDef.GetGlobalModeNr(arK);
   GlobalModeNr i4=arOpDef.GetGlobalModeNr(arL);
   if ( (i1 == i2) || (i3 == i4) ) 
   {
      MIDASERROR("Wrong use of AddOneCorTerm ");
   }

   if (  i1 == gOperatorDefs.ElectronicDofNr()
      || i2 == gOperatorDefs.ElectronicDofNr()
      || i3 == gOperatorDefs.ElectronicDofNr()
      || i4 == gOperatorDefs.ElectronicDofNr()
      )
   {
      MIDASERROR("Electronic DOF should not be included in Watson KEO!");
   }

   string s1 = gOperatorDefs.GetModeLabel(i1);
   string s2 = gOperatorDefs.GetModeLabel(i2);
   string s3 = gOperatorDefs.GetModeLabel(i3);
   string s4 = gOperatorDefs.GetModeLabel(i4);
   if ((i1!=i3)&&(i1!=i4)&&(i2!=i3)&&(i2!=i4))
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_0, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_0, I_1, false, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s3, I_1, I_0, false, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s4, I_0, I_1, false, false);
   }
   else if (i1==i3&&i2!=i4)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_0, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_0, I_1, false, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s4, I_0, I_1, false, false);
   }
   else if (i1==i4&&i2!=i3)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_1, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_0, I_1, false, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s3, I_1, I_0, false, false);
   }
   else if (i1!=i3&&i2==i4)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_0, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_0, I_2, false, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s3, I_1, I_0, false, false);
   }
   else if (i1!=i4&&i2==i3)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_0, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_1, I_1, true, false);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s4, I_0, I_1, false, false);
   }
   else if (i1==i3&&i2==i4)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_2, I_0, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_0, I_2, false, false);
   }
   else if (i1==i4&&i2==i3)
   {
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s1, I_1, I_1, false, true);
      arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, s2, I_1, I_1, true, false);
   }
   arOpDef.mNterms++;
   arOpDef.AddCterm(aZ);
}

/**
 * string from a pair of integers.
 **/
string StringFromPair
   (  std::pair<In,In>& arIpair
   )
{
//assumes pairs of integers < 3
   if(arIpair.first>2 || arIpair.second>2)
   {
      MIDASERROR(" pair not allowed in StringFromPair ");
   }
   //Mout << " In StringFromPair: first In:" << arIpair.first << " second In: " << arIpair.second << endl;
   string s_this;
   In i1=arIpair.first;
   In i2=arIpair.second;
   if (i1==0) s_this="X";
   else if (i1==1) s_this="Y";
   else if (i1==2) s_this="Z";
   if (i2==0) s_this+="X";
   else if (i2==1) s_this+="Y";
   else if (i2==2) s_this+="Z";
   //Mout << " string from pair: (" << i1 << "," << i2 << ") is: " << s_this << endl;
   return s_this;
}

/**
* Add full coriolis corrections to the kinetic energy operator
* Two options:
* 1. Non linear molecules
* 2. Linear molecules. Only pi_x and pi_y contribute
* NOTE: It is assumed that the molecule is oriented in the following way:
* 1. Non linear molecule: along the principal inertia axes
* 2. Linear molecule: along the z axis so that only pi_x and pi_y contribute.
**/
void AddFullCoriolisOp
   (  OpDef& arOpDef
   ,  std::string aCorFileName
   ,  std::vector<Nb>& arInertia
   ,  bool aInertiaFilesFromIn
   ,  std::vector<string>& arInertiaFileNames
   ,  std::map<string,string> arInertiaFileMap
   ,  bool aRestrictModes
   ,  In aNrestricModes
   ,  bool aRestricModesInWkeo
   ,  In aNrestricModesInWkeo
   ,  bool aScreenZero
   ,  Nb aZeroThr
   )
{
   if (  !gOperatorDefs.ElectronicDofLabel().empty()
      )
   {
      MIDASERROR("Full Watson operator not implemented for non-adiabatic systems.");
   }

   // Copy relevant variables for screening
   bool restric_modes = aRestrictModes;
   In n_restric_modes = aNrestricModes;

   bool restric_modes_in_wkeo = aRestricModesInWkeo;
   In n_restric_modes_in_wkeo = aNrestricModesInWkeo;

   bool screen_zero_terms = aScreenZero;
   Nb zero_thr = aZeroThr;
   
   // Check for the amount of info on input
   bool is_linear = false;
   In i_c = I_0;
   Nb thresh = C_10_5*C_NB_EPSILON;
   for (In i = I_0; i < I_3; i++)
   {
      if (arInertia[i] <= thresh)
      {
         i_c++; 
      }
   }
   if (i_c > I_1)
   {
      MIDASERROR(" Strange values of principal inertia moments! ");
   }
   if (i_c == I_1)
   {
      is_linear = true;
   }

   In n_ang_mom_op = I_3;
   if (is_linear)
   {
      n_ang_mom_op = I_2;
   }
   
   // Construct vibrational "angular momentum" operators
   std::vector<OpDef> vib_ang_mom;

   for (In i_op = I_0; i_op < n_ang_mom_op; i_op++)
   {
      vib_ang_mom.push_back(OpDef());
      OpDef& vib_oper = vib_ang_mom[vib_ang_mom.size() - I_1]; //reference to the operator
      //vib_oper.Reserve();
      for (LocalModeNr i=I_0;i<arOpDef.NmodesInOp();i++)
      {
         GlobalModeNr i_g_mode = arOpDef.GetGlobalModeNr(i);
         vib_oper.AddMode(i_g_mode);
      }
      vib_oper.mActiveTermsVec.resize(arOpDef.NmodesInOp());
      //construct the operator
      SetVibAngMomOp(i_op, aCorFileName, vib_oper, screen_zero_terms, zero_thr, arOpDef);
      bool sop_op=true;
      //Set name, check, and reorganize
      string oper_type="";
      if (sop_op) oper_type = "SOP";
      string vib_oper_name="pi"+std::to_string(i_op+1);
      vib_oper.SetName(vib_oper_name);
      //Mout << " vib_oper name: " << vib_oper.Name() << endl;
      vib_oper.SetType(oper_type);
      //Mout << " vib_oper type: " << vib_oper.Type() << endl;
      bool ign_qi_only=false;
      vib_oper.SetIgnQiOnly(ign_qi_only); // set IgnQiOnly before AddP.
      bool act_term_alg = true;
      vib_oper.SetActTermAlg(act_term_alg);
   
      // If only the general integral evaluation is used, then there is no need for inserting missing one-mode operators, as this way of evaluation is not based on a recursive strategy
      if (gOperatorDefs.GetmBsplineGenIntEval())
      {
         vib_oper.Reorganize(false);
      }
      else
      {
         vib_oper.Reorganize(true);
      }
   }

   // Debug summary for vibrational momentum operators
   if (gOperIoLevel > I_13 || gDebug)
   {
      for (In i_oper = I_0; i_oper < vib_ang_mom.size(); i_oper++)
      {        
         Mout << "\n Operator summary for operator: " << vib_ang_mom[i_oper].Name() << endl;
         Mout << " Local  Mode nrs: ";
         for (In i_op_mode=0;i_op_mode<vib_ang_mom[i_oper].NmodesInOp();i_op_mode++)
            Mout << setw(3) << i_op_mode;
         Mout << endl;
         Mout << " Global Mode nrs: ";
         for (In i_op_mode=0;i_op_mode<vib_ang_mom[i_oper].NmodesInOp();i_op_mode++)
            Mout << setw(3) << vib_ang_mom[i_oper].GetGlobalModeNr(i_op_mode);
         Mout << endl;
         Out72Char(Mout,'+','=','+');
         Mout << " Mode m | m Label    | Oper. nr.  | Operator description " << endl;
         Out72Char(Mout,'+','=','+');
         for (LocalModeNr i_op_mode=0;i_op_mode<vib_ang_mom[i_oper].NmodesInOp();i_op_mode++)
         {  
            GlobalModeNr i_mode = vib_ang_mom[i_oper].GetGlobalModeNr(i_op_mode);
            for (In i_op=0;i_op<vib_ang_mom[i_oper].NrOneModeOpers(i_op_mode);i_op++)
            {
               Mout.width(7);
               Mout << i_mode << " | ";
               Mout.width(10);
               Mout << gOperatorDefs.GetModeLabel(i_mode) << " | ";
               Mout.width(10);
               Mout << i_op << " | ";
               Mout << gOperatorDefs.GetOperLabel(vib_ang_mom[i_oper].GetOneModeOperGlobalNr(i_op_mode,i_op)) << endl;
            }
            if (i_op_mode != vib_ang_mom[i_oper].NmodesInOp() - I_1)
            {
               Out72Char(Mout,'-','-','-');
            }
         }
         Out72Char(Mout,'+','=','+');
         Mout << " The number of terms in vibrational angular momentum operator is: " << vib_ang_mom[i_oper].Nterms() << endl;
         Mout << " The operator in terms of coefficients and one-mode operators is " << endl;
         for (In i_term=0;i_term<vib_ang_mom[i_oper].Nterms();i_term++)
         {
            Mout.width(24);
            Mout.setf(ios::scientific);
            Mout.setf(ios::uppercase);
            midas::stream::ScopedPrecision(15, Mout);
            Mout.setf(ios::showpoint);
            Mout << right << vib_ang_mom[i_oper].Coef(i_term);
            Mout << "*" << vib_ang_mom[i_oper].GetOperProd(i_term) << endl;
         }
      }
   }

   // Construct inertia inverse operator components, or check for inertia vector
   std::vector<OpDef> inv_inertia_op;
   bool full_coriolis = false;

   // Bo: The following line is implemented to add the Mu trace term, remove the comment here and change two other lines around 10-30 lines from here
   // Bo: InputOperFromFile(arInertiaFileNames[0],arOpDef,restric_modes,n_restric_modes,false,screen_zero_terms,zero_thr,"", true, -0.125);
   if (aInertiaFilesFromIn && arInertiaFileNames.size() > I_0)
   {
      //read op. from file
      full_coriolis = true;
      //Mout << " reading inertia operators from files: " << endl;
      In n_op_files = I_1;
      //Mout << " is linear: " << is_linear << endl;
      
      // Bo: If you want to add the Mu trace term change the I_6 to I_7
      if (!is_linear)
      {
         n_op_files = I_6;
      }
      //Mout << " n_op_files: " << n_op_files << endl;
      //Mout << " file name size: " << arInertiaFileNames.size() << endl;
      if (arInertiaFileNames.size() != n_op_files)
      {
         MIDASERROR(" wrong no. of inertia components in input ");
      }
      if (gOperIoLevel > I_13 || gDebug)
      {
         for (In i = I_0; i < n_op_files; i++)
         {
            Mout << " file no. " << i + I_1 << " is: " << arInertiaFileNames[i] << std::endl;
         }
         Mout << std::endl;
      }


      // Run over operator components
      // Bo: And finally to add the mu trace change this loop to run from 1 to n_op_files instead of from 0.
      // Bo: for (In i_op=1; i_op<n_op_files; ++i_op)
      for (In i_op=0; i_op<n_op_files; ++i_op)
      {
         inv_inertia_op.push_back(OpDef());
         OpDef& inertia_oper=inv_inertia_op[inv_inertia_op.size()-1]; //reference to oper.
         //inertia_oper.Reserve();
         for (In i=I_0;i<arOpDef.NmodesInOp();++i)
         {
            GlobalModeNr i_g_mode = arOpDef.GetGlobalModeNr(i);
            //Mout << " i_g_mode " << i_g_mode;
            inertia_oper.AddMode(i_g_mode);
            //Mout << " added " << endl;
         }
         inertia_oper.mActiveTermsVec.resize(arOpDef.NmodesInOp());
         
         // Now read it from file
         inertia_oper.SetOpFile(arInertiaFileNames[i_op]);
         inertia_oper.InitLastOper(arInertiaFileNames[i_op], C_1, -I_1, -I_1, -I_1, false);
         
         InputOperFromFile(inertia_oper, n_restric_modes, zero_thr);
         
         bool sop_op=true;
         //Set name, check, and reorganize
         string oper_type="";
         if (sop_op) oper_type = "SOP";
         string inertia_oper_name;
         inertia_oper_name="inv_inertia_"+arInertiaFileMap[arInertiaFileNames[i_op]];
         if (is_linear && (n_op_files==I_1)) inertia_oper_name="inv_inertia_lin";
         inertia_oper.SetName(inertia_oper_name);
         //Mout << " inertia_oper name: " << inertia_oper.Name() << endl;
         inertia_oper.SetType(oper_type);
         //Mout << " inertia_oper type: " << inertia_oper.Type() << endl;
         bool ign_qi_only=false;
         inertia_oper.SetIgnQiOnly(ign_qi_only); // set IgnQiOnly before AddP.
         bool act_term_alg = true;
         inertia_oper.SetActTermAlg(act_term_alg);
   
         // If only the general integral evaluation is used, then there is no need for inserting missing one-mode operators, as this way of evaluation is not based on a recursive strategy
         if (gOperatorDefs.GetmBsplineGenIntEval())
         {
            inertia_oper.Reorganize(false);
         }
         else
         {
            inertia_oper.Reorganize(true);
         }
      }

      //debug summary for inertia operators
      if (gOperIoLevel > I_13 || gDebug)
      {
         for (In i_oper = I_0; i_oper < inv_inertia_op.size(); i_oper++)
         {        
            Mout << "\n Operator summary for operator: " << inv_inertia_op[i_oper].Name() << endl;

            Mout << " Local  Mode nrs: ";
            for (In i_op_mode=0;i_op_mode<inv_inertia_op[i_oper].NmodesInOp();i_op_mode++)
               Mout << setw(3) << i_op_mode;
            Mout << endl;
            Mout << " Global Mode nrs: ";
            for (In i_op_mode=0;i_op_mode<inv_inertia_op[i_oper].NmodesInOp();i_op_mode++)
               Mout << setw(3) << inv_inertia_op[i_oper].GetGlobalModeNr(i_op_mode);
            Mout << endl;
            Out72Char(Mout,'+','=','+');
            Mout << " Mode m | m Label    | Oper. nr.  | Operator description " << endl;
            Out72Char(Mout,'+','=','+');
            for (LocalModeNr i_op_mode=0;i_op_mode<inv_inertia_op[i_oper].NmodesInOp();i_op_mode++)
            {
               GlobalModeNr i_mode = inv_inertia_op[i_oper].GetGlobalModeNr(i_op_mode);
               for (In i_op=0;i_op<inv_inertia_op[i_oper].NrOneModeOpers(i_op_mode);i_op++)
               {
                  Mout.width(7);
                  Mout << i_mode << " | ";
                  Mout.width(10);
                  Mout << gOperatorDefs.GetModeLabel(i_mode) << " | ";
                  Mout.width(10);
                  Mout << i_op << " | ";
                  Mout << gOperatorDefs.GetOperLabel(inv_inertia_op[i_oper].GetOneModeOperGlobalNr(i_op_mode,i_op)) << endl;
               }
               if (i_op_mode != inv_inertia_op[i_oper].NmodesInOp()-1) Out72Char(Mout,'-','-','-');
            }
            Out72Char(Mout,'+','=','+');
            Mout << " The number of terms for inverse effective inertia operator: " << inv_inertia_op[i_oper].Nterms() << endl;
            Mout << " The operator in terms of coefficients and one-mode operators is " << endl;
            for (In i_term=0;i_term<inv_inertia_op[i_oper].Nterms();i_term++)
            {
               Mout.width(24);
               Mout.setf(ios::scientific);
               Mout.setf(ios::uppercase);
               midas::stream::ScopedPrecision(15, Mout);
               Mout.setf(ios::showpoint);
               Mout << right << inv_inertia_op[i_oper].Coef(i_term);
               Mout << "*" << inv_inertia_op[i_oper].GetOperProd(i_term) << endl;
            }
         }
      }
   }
   else
   {
      MidasWarning("No effective (Coriolis corrected) inertia inverse tensor components are supplied! Will resolve to adding only diagonal terms for this part of the Watson operator!");
   }
   
   // Add the inverse moment of inertia diagonal tensor elements to the Hamiltonian first
   std::vector<Nb> mu(I_3);
   for (In i_op = I_0; i_op < n_ang_mom_op; i_op++)
   {  
      if (vib_ang_mom[i_op].Nterms() == I_0)
      {
         continue;
      }
      mu[i_op] = -C_I_2/arInertia[i_op];
      
      OperMult(arOpDef, mu[i_op], vib_ang_mom[i_op], vib_ang_mom[i_op], screen_zero_terms, zero_thr, restric_modes_in_wkeo, n_restric_modes_in_wkeo);
   }  

   if (gOperIoLevel > I_5 || gDebug)
   {
      Mout << " Inverse moment of inertia diagonal tensor elements " << mu << std::endl;
   }
   
   // General expression, run over i_alpha and i_beta
   if (full_coriolis)
   {
      for (In i_a=0; i_a<n_ang_mom_op; i_a++)
      {
         OpDef dum_oper;
         //dum_oper.Reserve();
         for (In i=I_0;i<arOpDef.NmodesInOp();i++)
         {
            GlobalModeNr i_g_mode = arOpDef.GetGlobalModeNr(i);
            //Mout << " i_g_mode " << i_g_mode;
            dum_oper.AddMode(i_g_mode);
            //Mout << " added " << endl;
         }
         dum_oper.mActiveTermsVec.resize(arOpDef.NmodesInOp());
         for (In i_b=0; i_b<n_ang_mom_op; i_b++)
         {
            if (is_linear && (i_b!=i_a)) continue;
            //find out correct inertia operator and add to the hamiltonian
            In idx=-1;
            if (!is_linear)
            {
               pair<In,In> comp_ij(i_a,i_b);
               if (i_a>i_b)
               {
                  comp_ij.first=i_b;
                  comp_ij.second=i_a;
               }
               string inertia_oper_name="inv_inertia_"+StringFromPair(comp_ij);
               for (In i_oper=0; i_oper<inv_inertia_op.size(); i_oper++)
                  if (inv_inertia_op[i_oper].Name()==inertia_oper_name)
                  {
                     idx=i_oper;
                     break;
                  }
               if (idx < I_0)
               {
                  MIDASERROR(" could not find Inertia operator ");
               }
               if (gOperIoLevel > I_13 || gDebug)
               {
                  Mout << " inertia oper file name: " << inertia_oper_name << " for pair (i_a=" << i_a << ", i_b= " << i_b << ")" << std::endl;
                  Mout << " idx=" << idx;
               }
            }
            else
               idx=I_0; //only one file
            if(vib_ang_mom[i_b].Nterms()==0 || inv_inertia_op[idx].Nterms()==0) continue; //skip if it is an empty operator
            Nb coef=C_1;
            //Mout << " entering in opermult .." << endl;
            OperMult(dum_oper,coef,inv_inertia_op[idx],vib_ang_mom[i_b],screen_zero_terms,zero_thr,restric_modes_in_wkeo,n_restric_modes_in_wkeo);
            //Mout << " exiting from opermult .." << endl;
         }
         bool sop_op=true;
         //Set name, check, and reorganize
         string oper_type="";
         if (sop_op) oper_type = "SOP";
         string dum_oper_name="dum_oper";
         dum_oper.SetName(dum_oper_name);
         dum_oper.SetType(oper_type);
         bool ign_qi_only=false;
         dum_oper.SetIgnQiOnly(ign_qi_only); // set IgnQiOnly before AddP.
         bool act_term_alg = true;
         dum_oper.SetActTermAlg(act_term_alg);

         // If only the general integral evaluation is used, then there is no need for inserting missing one-mode operators, as this way of evaluation is not based on a recursive strategy
         if (gOperatorDefs.GetmBsplineGenIntEval())
         {
            dum_oper.Reorganize(false);
         }
         else
         {
            dum_oper.Reorganize(true);
         }
         
         //debug summary for ausiliary operators
         if(gOperIoLevel > I_13 || gDebug)
         {
            Mout << "\n Operator summary for operator: " << dum_oper.Name() << " for i_a: " << i_a << endl;
            Mout << " Local  Mode nrs: ";
            for (In i_op_mode=0;i_op_mode<dum_oper.NmodesInOp();i_op_mode++)
               Mout << setw(3) << i_op_mode;
            Mout << endl;
            Mout << " Global Mode nrs: ";
            for (In i_op_mode=0;i_op_mode<dum_oper.NmodesInOp();i_op_mode++)
               Mout << setw(3) << dum_oper.GetGlobalModeNr(i_op_mode);
            Mout << endl;
            Out72Char(Mout,'+','=','+');
            Mout << " Mode m | m Label    | Oper. nr.  | Operator description " << endl;
            Out72Char(Mout,'+','=','+');
            for (LocalModeNr i_op_mode=0;i_op_mode<dum_oper.NmodesInOp();i_op_mode++)
            {
               GlobalModeNr i_mode = dum_oper.GetGlobalModeNr(i_op_mode);
               for (In i_op=0;i_op<dum_oper.NrOneModeOpers(i_op_mode);i_op++)
               {
                  Mout.width(7);
                  Mout << i_mode << " | ";
                  Mout.width(10);
                  Mout << gOperatorDefs.GetModeLabel(i_mode) << " | ";
                  Mout.width(10);
                  Mout << i_op << " | ";
                  Mout << gOperatorDefs.GetOperLabel(dum_oper.GetOneModeOperGlobalNr(i_op_mode,i_op)) << endl;
               }
               if (i_op_mode != dum_oper.NmodesInOp()-1) Out72Char(Mout,'-','-','-');
            }
            Out72Char(Mout,'+','=','+');
            Mout << " The number of terms is: " << dum_oper.Nterms() << endl;
            Mout << " The ausiliary operator in terms of coefficients and one-mode operators is " << endl;
            for (In i_term = I_0; i_term < dum_oper.Nterms(); i_term++)
            {
               Mout.width(24);
               Mout.setf(ios::scientific);
               Mout.setf(ios::uppercase);
               midas::stream::ScopedPrecision(15, Mout);
               Mout.setf(ios::showpoint);
               Mout << right << dum_oper.Coef(i_term);
               Mout << "*" << dum_oper.GetOperProd(i_term) << std::endl;
            }
         }

         //find out correct inertia operator and add to the hamiltonian
         if(dum_oper.Nterms() == I_0 || vib_ang_mom[i_a].Nterms() == I_0)
         {
            continue; //skip if it is an empty operator
         }
         //then add to the hamiltonian
         Nb coef = -C_I_2;
         OperMult(arOpDef,coef,vib_ang_mom[i_a],dum_oper,screen_zero_terms,zero_thr,restric_modes_in_wkeo,n_restric_modes_in_wkeo);
      }
   }
}

/**
* Construct vibrational angular momentum operator for component icomp
**/
void SetVibAngMomOp
   (  In aIcomp
   ,  string aCorFileName
   ,  OpDef& arOp
   ,  bool aScreenZeroTerms
   ,  Nb aZeroThr
   ,  OpDef& arHamilton
   )
{
   //slighlty modified version, in view of the new implementation
   if (gOperIoLevel > I_9 || gDebug)
   {
      Mout << " Component of the vibrational momentum operator is: " << aIcomp << std::endl;
   }
   Nb local_zero = C_NB_EPSILON;
   //Mout << " local_zero in SetVibAngMomOp: " << C_NB_EPSILON << endl;
   if (aScreenZeroTerms)
   {
      local_zero = aZeroThr;
   }
   In nmodes = arHamilton.NmodesInOp();
   In nm2 = nmodes*nmodes;
   std::vector<Nb> zeta(nm2, C_0);

   std::istringstream cor_inp_file = midas::mpi::FileToStringStream(aCorFileName);
   if (gOperIoLevel > I_9)
   {
      Mout << " Coriolis z matrices are read from file: " << aCorFileName.c_str() << std::endl;
   }
   if (cor_inp_file.fail() || cor_inp_file.str().empty()) 
   {
      MIDASERROR(" File with coriolis z matrices not found");
   }

   std::string s = "";
   In alpha;
   // Get new lines with mode and operators defs.
   while (midas::input::GetLine(cor_inp_file, s)) 
   {
      istringstream input_s(s);
      std::string is = "";
      std::string js = "";

      input_s >> alpha;
      alpha--;
      if (alpha != aIcomp)
      {
         continue;
      }
      input_s >> is;
      input_s >> js;

      // as in VibInput when read from file. the mode nr. read in follow the same logic, i.e. imode+1.
      In i_g_mode = gOperatorDefs.ModeNr(is, true);
      In i = arHamilton.mOpModeMap[i_g_mode];

      i_g_mode = gOperatorDefs.ModeNr(js, true);
      In j = arHamilton.mOpModeMap[i_g_mode];

      Nb coef;
      input_s >> coef;
      
      In addij = i*nmodes + j;
      zeta[addij] = coef;
      
      In addji = j*nmodes + i;
      zeta[addji] = -coef;
   }
   if (gOperIoLevel > I_9 || gDebug)
   {
      Mout << " The zeta matrix read into for component: " << aIcomp << "  is: " << std::endl;
      Mout.precision(19);
      Mout.setf(ios::scientific);
      Mout.setf(ios::showpoint); 
      Mout.setf(ios::uppercase);
      for (In i = I_0; i < nmodes; i++)
      {
         for (In j = I_0; j < nmodes; j++)
         {
            Mout << setw(12) << zeta[i*nmodes + j] << " ";
         }
         Mout << std::endl;
      }
   }
   for (LocalModeNr i = I_0; i < nmodes; i++)
   {
      for (LocalModeNr j = i + I_1; j < nmodes; j++)
      {
         In addij = i*nmodes + j;
         //if(fabs(zeta[addij])<local_zero && aScreenZeroTerms) continue;
         if (fabs(zeta[addij]) < local_zero) continue;
         Nb coeffp = zeta[addij];
         Nb coeffm = -zeta[addij];
         //transform q to the scaled ones if required
         if(arHamilton.ScaledPot())
         {
            coeffp *= C_1/arHamilton.ScaleFactorForMode(i);
            coeffm *= C_1/arHamilton.ScaleFactorForMode(j);
            if (gOperIoLevel > I_9 || gDebug)
            {
               Mout << " Coriolis coefficients will be accordingly scaled: " << std::endl;
            
               Mout << " Scale for mode " << i << " is set to omeg_" << i << "= " << arHamilton.ScaleFactorForMode(i) << std::endl;
               Mout << " Resulting in coefficient p = " << coeffp << std::endl;
               Mout << " Scale for mode " << j << " is set to omeg_" << j << "= " << arHamilton.ScaleFactorForMode(j) << std::endl;
               Mout << " Resulting in coefficient m = " << coeffm << std::endl;
            }
         }
         //if (max(coeffp,coeffm) < local_zero) continue;
         //then follow the logic as for the old Coriolis subroutines
         GlobalModeNr i1=arHamilton.GetGlobalModeNr(i);
         string s1=gOperatorDefs.GetModeLabel(i1);
         //Mout << " string s1: " << s1 << endl;
         arOp.AddSimpleOneModeOperProd(arOp.mNterms, s1, I_1, I_0, false, true);
         GlobalModeNr j1=arHamilton.GetGlobalModeNr(j);
         string s2=gOperatorDefs.GetModeLabel(j1);
         //Mout << " string s2: " << s2 << endl;
         arOp.AddSimpleOneModeOperProd(arOp.mNterms, s2, I_0, I_1, false, false);
         arOp.mNterms++;
         arOp.mCterms.push_back(coeffp);
         arOp.AddSimpleOneModeOperProd(arOp.mNterms, s2, I_1, I_0, false, true);
         arOp.AddSimpleOneModeOperProd(arOp.mNterms, s1, I_0, I_1, false, false);
         arOp.mNterms++;         
         arOp.mCterms.push_back(coeffm);
      }
   }
}

/**
* Read in operator input from input stream.
*
* @param Minp     Input stream.
*
* @return         Returns last read string.
* */
std::string OperatorInput
   (  std::istream& Minp
   )
{
   if (gDoOper && (gOperIoLevel > I_8 || gDebug)) 
   {
      Mout << " I have already read the opeator input once - must be reading a new operator " << std::endl;
   }
   
   // Set global variables
   gDoOper = true; 
   gOperatorDefs.AddOperator();
   
   // new_oper is a reference to the new operator 
   OpDef& new_oper = gOperatorDefs[gOperatorDefs.GetNrOfOpers() - I_1]; 

   bool fix_old = true;
   if (fix_old && gOperatorDefs.GetNrOfOpers() > I_1)
   {
      for (In i = I_0; i < gOperatorDefs[I_0].NmodesInOp(); ++i)
      {
         GlobalModeNr i_g_mode = gOperatorDefs[I_0].GetGlobalModeNr(i);
         new_oper.AddMode(i_g_mode);
      }
      new_oper.mActiveTermsVec.resize(gOperatorDefs[I_0].NmodesInOp());
   }

   // Local initializations 
   std::string oper_name                     = "operator";
   OpInfo oper_info;  // To be paired with oper_name using OpDef::msOpInfo map.
   bool sop_op                               = false;
   bool ign_qi_only                          = false;
   bool act_term_alg                         = true;
   Nb ff_fact                                = C_0;
   bool on_file                              = false;
   bool watson_eq_approx                     = false;
   bool watson_const_term                    = false;
   bool fullcoriolis                         = false; 
   bool coriolis                             = false; 
   bool restrict_modes                       = false;
   bool restrict_modes_in_wkeo               = false;
   bool manual_oper_input                    = false;
   bool screen_zero_term                     = false;
   bool simple_ke                            = false;
   bool bspline_gen_int_eval                 = false;
   Nb   zero_thr                             = C_NB_EPSILON;
   In n_restric_modes                        = I_0;
   In n_restric_modes_in_wkeo                = I_0;
   bool inertia_tensor_on_file               = false;
   std::string inertia_tensor_file           ="";
   std::string oper_file_name                ="";
   std::string corfile                       ="";
   bool auto_add_kin                         = false;
   std::string keo_type                      = "NOT_SET";
   std::string psc_oper_file_name            = "";
   bool inp_inertia_file_names               = false;
   std::vector<Nb> inertia;
   std::vector<std::string> inertia_file_names;
   std::map<std::string, std::string> inertia_file_map;
   std::vector<std::string> op_name_vec;
   //bool inp_auto_op_names                    = false;
   //vector<string> auto_op_names;
  
   enum INPUT 
      {  ERROR
      ,  ADDKINTERMS
      ,  CORCOUPFILE
      ,  DUPLICATE
      ,  BSPLINEGENINTEVAL
      ,  IGNQIONLY
      ,  INERTIA
      ,  INERTIAFILES
      ,  INERTIATENSOR
      ,  IOLEVEL
      ,  KINETICENERGY
      ,  NAME
      ,  NEIGHBOURCOUP
      ,  NOACTIVETERMSALGO
      ,  OPERFILE
      ,  OPERINPUT
      ,  RESTRICTMODES
      ,  RESTRICTMODESINWKEO
      ,  SCREENZEROTERM
      ,  SETINFO
      ,  WATSONEQAPPROX
      };

   const std::map<std::string, INPUT> input_word =
   {  {"#3ADDKINTERMS",ADDKINTERMS}
   ,  {"#3CORFILE",CORCOUPFILE}
   ,  {"#3DUPLICATE",DUPLICATE}
   ,  {"#3BSPLINEGENINTEVAL",BSPLINEGENINTEVAL}
   ,  {"#3IGNQIONLY",IGNQIONLY}
   ,  {"#3INERTIA",INERTIA}
   ,  {"#3INERTIAFILES",INERTIAFILES}
   ,  {"#3INERTIATENSOR",INERTIATENSOR}
   ,  {"#3IOLEVEL",IOLEVEL}
   ,  {"#3KINETICENERGY",KINETICENERGY}
   ,  {"#3NAME",NAME}
   ,  {"#3NEIGHBOURCOUP",NEIGHBOURCOUP}
   ,  {"#3NOACTIVETERMSALGO",NOACTIVETERMSALGO}
   ,  {"#3OPERFILE",OPERFILE}
   ,  {"#3OPERINPUT",OPERINPUT}
   ,  {"#3RESTRICTMODES",RESTRICTMODES}
   ,  {"#3RESTRICTMODESINWKEO",RESTRICTMODESINWKEO}
   ,  {"#3SCREENZEROTERM",SCREENZEROTERM}
   ,  {"#3SETINFO",SETINFO}
   ,  {"#3WATSONEQAPPROX",WATSONEQAPPROX}
   //,   {"#3OPSFROMESC",OPSFROMESC},
   };
   
   string s="";
   In input_level = 3;
   In n_dup       = 1;
   In n_coup      = 0;
   Nb c_coup      = C_0; 
   bool already_read = false;
   
   gOperatorDefs.SetmBsplineGenIntEval(bspline_gen_int_eval);

   // Readin until input levels point up
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !(s.substr(I_0,I_1) == "#"
         && atoi(s.substr(I_1,I_1).c_str()) < input_level)
         )  
   {
      std::string s_orig = s;
      // Transform to all upper
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
      // Delete ALL blanks
      while (s.find(" ") != s.npos)
      {
         s.erase(s.find(" "), I_1);
      }
      INPUT input = midas::input::FindKeyword(input_word,s);
      already_read = false;
      switch(input)
      {
         case OPERINPUT:
         {
            sop_op = true;
            manual_oper_input = true;
            
            s = midas::input::ReadGenericMidasInput
               (  Minp
               ,  new_oper
               ,  midas::input::gProgramSettings.GetInputFilePath()
               ,  -1
               ,  4
               ,  C_1
               ,  true
               );

            already_read = true;
            
            break;
         }
         case ADDKINTERMS:
         {
            // read extra line that is not used... AWESOME CODE :D !
            midas::input::GetLine(Minp,s);
            auto_add_kin = true;
            break;
         }
         case IOLEVEL:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            gOperIoLevel = midas::util::FromString<In>(s);
            break;
         }
         case KINETICENERGY:
         {
            midas::input::GetLine(Minp, s);
            // Transform to only capital letters
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
            // Delete ALL blanks
            while (s.find(" ") != s.npos)
            {
               s.erase(s.find(" "), I_1);
            }
            
            // Set kinetic energy operator type
            keo_type = s;
            
            // If the keo is derived from polysherical coordinates then we need to read an additional line specifying the file in which to find the keo
            if (keo_type == "POLYSPHERICAL")
            {
               midas::input::GetLine(Minp, s);
               istringstream input_s(s);
               input_s >> psc_oper_file_name;
            }

            break;
         }
         case NAME:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> oper_name;
            break;
         }
         case IGNQIONLY:
         {
            ign_qi_only = true;
            
            break;
         }
         case OPERFILE:   
         {
            on_file = true;

            midas::input::GetLine(Minp,s);
            istringstream input_s(s);
            input_s >> oper_file_name;
            
            break;
         }
         case DUPLICATE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            n_dup = midas::util::FromString<In>(s);
            break;
         }
         case NEIGHBOURCOUP:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> n_coup;
            Nb x=C_0; 
            if (input_s >> x)
               c_coup = x; 
            break;
         }
         case WATSONEQAPPROX:
         {
            if (inertia.size() == I_3)
            {
               MIDASERROR(" Keywords Inertia or InertiaTensor are present. None of these can be specified with the WatsonEqApprox keyword! ");
            }
            else
            {
               inertia.resize(I_3);
            }

            midas::input::GetLine(Minp, s);
            istringstream input_s(s);
            std::string path_to_data_files;
            input_s >> path_to_data_files;
            inertia_tensor_file = path_to_data_files + "/inertia_tensor.mpesinfo";
            corfile = path_to_data_files + "/coriolis_matrices.mpesinfo";

            simple_ke = true;
            fullcoriolis = true;
            watson_const_term = true;
            inertia_tensor_on_file = true;
            
            break;
         }
         case INERTIA:
         {
            if (inertia.size() == I_3)
            {
               MIDASERROR(" Keywords Inertia and InertiaTensor are present, only one can be specified at a time! ");
            }
            else
            {
               inertia.resize(I_3);
            }
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            for (In ii = I_0; ii < I_3; ii++)
            {
               input_s >> inertia[ii];
            }
            
            break;
         }
         case INERTIATENSOR:
         {
            if (inertia.size() == I_3)
            {
               MIDASERROR(" Keywords Inertia and InertiaTensor are present, only one can be specified at a time! ");
            }
            else
            {
               inertia.resize(I_3);
            }
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> inertia_tensor_file;
            inertia_tensor_on_file = true;

            break;
         }
         case INERTIAFILES:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            In no_of_files = midas::util::FromString<In>(s);
            Mout << " Number of inertia files to be read is: " << no_of_files << std::endl;
            for (In ifil = I_0; ifil < no_of_files; ifil++)
            {
               std::string comp;
               std::string file_name;
               midas::input::GetLine(Minp,s);
               istringstream input_s(s);
               input_s >> file_name;
               inertia_file_names.push_back(file_name);
               input_s >> comp;
               Mout << " File name: " << file_name << " contains component: " << comp << std::endl;
               transform(comp.begin(), comp.end(), comp.begin(), (In(*) (In)) toupper);
               inertia_file_map[file_name] = comp;
            }
            inp_inertia_file_names = true;
            
            break;
         }
         case CORCOUPFILE:   
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            
            istringstream input_s(s);
            input_s >> corfile;
            
            break;
         }
         case NOACTIVETERMSALGO:
         {
            act_term_alg = false;
            
            break;
         }
         case RESTRICTMODES:
         {
            restrict_modes = true;
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            n_restric_modes = midas::util::FromString<In>(s);
            
            break;
         }
         case SCREENZEROTERM:
         {
            screen_zero_term = true;
            midas::input::GetLine(Minp,s);
            zero_thr = midas::util::FromString<Nb>(s);
            
            break;
         }
         case BSPLINEGENINTEVAL:
         {
            bspline_gen_int_eval = true;
            gOperatorDefs.SetmBsplineGenIntEval(bspline_gen_int_eval);
            
            break;
         }
         case SETINFO:
         {
            midas::input::GetLine(Minp, s);

            ParseSetInfoLine(s, oper_info);

            break;
         }
         case RESTRICTMODESINWKEO:
         {
            restrict_modes_in_wkeo = true;
            midas::input::GetLine(Minp, s);
            n_restric_modes_in_wkeo = midas::util::FromString<In>(s);
            
            break;
         }
         default:
         {
            Mout  << " Keyword " << s_orig 
                  << " is not a Oper level 3 Input keyword - input flag ignored! " 
                  << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   
   // Rename the operator after its type
   if (oper_name == "operator")
   {
      oper_name = oper::PropertyTypeToString(oper_info.GetType());
   }
   
   // Save oper_info in global OpInfos
   if(OpDef::msOpInfo.find(oper_name) != OpDef::msOpInfo.end())
   {
      MIDASERROR("There already exists an operator with name '" + oper_name + "'.");
   }
   OpDef::msOpInfo[oper_name] = oper_info;

   // Set operator name
   new_oper.SetName(oper_name);
   if (gOperIoLevel > I_1 || gDebug)
   {
      Mout << " Constructing the operator " << new_oper.Name() << std::endl;
   }

   // Set the operator file name and store this 
   if (on_file) 
   {
      new_oper.SetOpFile(oper_file_name);
      new_oper.SetProperlyReadFromFile(false);
      new_oper.InitLastOper(oper_file_name, C_1, -I_1, -I_1, -I_1, false);
      sop_op = true;
   }
   
   // Add advanced kinetic energy operator (must currently come before InputOperFromFile() !).
   if (auto_add_kin)
   {
      new_oper.AddAdvancedKe(gSaveDir);
   }
  
   // Read the (potential energy or molecular property) operator file and store information in memory for later use
   if (midas::mpi::filesystem::IsFileOnMaster(oper_file_name) && on_file)
   {
      InputOperFromFile(new_oper);
      new_oper.SetProperlyReadFromFile(true);
   }
   else
   {
      if (gIoLevel > I_5)
      {
         Mout << " Did not find any operator file for reading " << std::endl;
      }
   }
   
   if(keo_type == "NOT_SET")
   {
      if(OpDef::msOpInfo[oper_name].GetType().Is(oper::energy))
      {
         keo_type = "SIMPLE";
      }
      else
      {
         keo_type = "NONE";
      }
   }
  
   // If the operator is set manually via the input, then we assume the user knowns what is being done
   if (manual_oper_input && keo_type != "USER_DEFINED")
   {
      keo_type = "NONE";
   }
   
   // If the operator type is indicated to be something else than "energy" and kinetic energy is turned on, then this will be disabled
   if (  keo_type != "NONE"
      && !  (  OpDef::msOpInfo[oper_name].GetType().Is(oper::energy) 
            || OpDef::msOpInfo[oper_name].GetType().Is(oper::unknown) 
            )
      )
   {
      Mout  << " Operator type detected as "    
            << oper::PropertyTypeToString(OpDef::msOpInfo[oper_name].GetType()) 
            << ", will disable the addition of kinetic energy operator terms." 
            << std::endl;
      keo_type = "NONE";
   }

   // Simple (vibrational) kinetic energy operator is to be constructed
   if (keo_type == "SIMPLE")
   {
      simple_ke = true;
   }
   // General kinetic energy operator for curvilinear coordinates is to be constructed
   else if (keo_type == "GENERAL")
   {
      MIDASERROR("General kinetic energy operator is not implemented yet!");
   }
   // No kinetic energy operator type specified, hopefully terms are supplied elsewhere
   else if (keo_type == "NONE")
   {
      // Indicate that kinetic energy operator terms have not been added
      new_oper.SetOperWithKeoTerms(false);
   }
   // Kinetic energy operator is provided on input by the user, (so here we do nothing)
   else if (keo_type == "USER_DEFINED")
   {
   
   }
   // General kinetic energy operator for curvilinear polysherical coordinates is to be constructed
   else if (keo_type == "POLYSPHERICAL")
   {
      sop_op = true;
      new_oper.SetKeoOperFileName(psc_oper_file_name);
     
      // Check that the file with potential energy operator and kinetic energy operator exists before reasing in the latter, we need to have read the former
      if (  midas::mpi::filesystem::IsFileOnMaster(oper_file_name) 
         && midas::mpi::filesystem::IsFileOnMaster(psc_oper_file_name)
         )
      {
         // Open operator file as input string stream
         auto psc_oper_input_file = midas::mpi::FileToStringStream(psc_oper_file_name);

         // Read the (kinetic energy) operator file and store information in memory for later use. Note that the -2 is used to indicate that this is the file that contains operator information based on polysherical coordinates
         midas::input::ReadGenericMidasInput(psc_oper_input_file, new_oper, psc_oper_file_name, -I_2, I_0, C_1, false);
     
         //
         new_oper.SetProperlyReadFromFile(true);
      }
   }
   // Complete Watson operator is to be constructed
   else if (keo_type == "WATSON")
   {
      simple_ke = true;
      fullcoriolis = true;
      watson_const_term = true;
   }
   // Approximate Watson operator is to be constructed
   else if (keo_type == "APPROXWATSON")
   {
      simple_ke = true;
      coriolis = true;
      watson_const_term = true;
   }
   // Only the constant Watson operator term is to be constructed
   else if (keo_type == "CONSTWATSON")
   {
      simple_ke = true;
      watson_const_term = true;
   }
   // Complete Watson operator for a linear molecule is to be constructed
   else if (keo_type == "LINWATSON")
   {
      simple_ke = true;
      fullcoriolis = true;
   }
   // Input error detected
   else
   {
      MIDASERROR("The requsted kinetic energy operator type " + keo_type + " is unknown!");
   }
   
   // Indicate in the operator that kinetic terms have been added
   if (keo_type != "NONE")
   {
      new_oper.SetOperWithKeoTerms(true);

      // Change operator type to "energy" from "unknown" if kinetic energy operator terms are to be supplied
      if (OpDef::msOpInfo[oper_name].GetType().Is(oper::unknown))
      {
         OpDef::msOpInfo[oper_name].SetType(oper::energy);
      }
   }

   // Now for some output
   if (gOperIoLevel > I_1 || gDebug)
   {
      if (keo_type == "NONE")
      {
         Mout << " No kinetic energy operator will be provided " << std::endl;
      }
      else if (keo_type == "SIMPLE")
      {
         Mout << " Kinetic energy will be provided as the simple vibrational kinetic energy operator " << std::endl; 
      }
      else if (keo_type == "USER_DEFINED")
      {
         Mout << " Kinetic energy already provided, will not add any additional kinetic energy operator terms " << std::endl;
      }
      else if (keo_type == "POLYSPHERICAL")
      {
         Mout << " Kinetic energy will be provided as an operator based on polyspherical coordinates " << std::endl;
      
         Mout << " Reading kinetic energy operator from file: " << std::endl
              << "  " << psc_oper_file_name << std::endl;
      }
      else 
      {
         Mout << " Kinetic energy will be provided as the Watson operator " << std::endl;
      }
   }

   // Has to be after we have read the operator, since it needs to know the number of modes in the operator
   if (simple_ke)
   {
      new_oper.AddSimpleKe();
   }
   
   //
   if (inertia_tensor_on_file)
   {
      if (  midas::filesystem::Exists(inertia_tensor_file)
         )
      {
         if (gOperIoLevel > I_5)
         {
            Mout << " Reading diagonal moment of inertia tensor elements from: " << inertia_tensor_file << std::endl;
         }
         std::ifstream in_file(inertia_tensor_file.c_str(), ios::in);
         std::string aLineStr;
         for (In i = I_0; i < I_3; i++)
         {
            midas::input::GetLine(in_file, aLineStr);
            std::vector<Nb> inertia_tensor_row = midas::util::VectorFromString<Nb>(aLineStr);
            inertia[i] = inertia_tensor_row[i];
         }
      }
      else
      {
         MIDASERROR("Intertia tensor not found at: " + inertia_tensor_file);
      }
   }

   // Construct the mass-dependent potential correctional term of the Watson operator
   if (watson_const_term && inertia.size() != I_0)
   {
      new_oper.AddWatsonPotTerm(inertia);
   }

   // Construct the mass and Coriolis coupling dependent potential correctional term of the Watson operator
   if (coriolis)
   {
      if (gOperIoLevel > I_1 || gDebug)
      {
         Mout << std::endl << " The approximate (vibrational) Watson operator will now be constructed " << std::endl;
      }

      if (fullcoriolis)
      {
         MIDASERROR("The #3 FullCoriolis keyword is also specified. Only one of the keywords #3 FullCoriolis and #3 Coriolis can be set active at the same time!");
      }
      if (!(corfile.size() != I_0) || !(inertia.size() != I_0))
      {
         MIDASERROR("Missing information on Coriolis coupling matrices or inertia moments");
      }
      if (n_dup > I_1)
      {
         MIDASERROR("#3 Duplication and #3 Coriolis keywords cannot be specified at the same time!");
      }
      
      // Done with checking, now we can begin to do something
      if (gOperIoLevel > I_5 || gDebug)
      {
         Mout << " The approximate Coriolis coupling terms will be added to the Hamiltonian " << std::endl;
      }

      AddCoriolisTerms
         (  inertia
         ,  corfile
         ,  new_oper
         );
   }
   if (fullcoriolis)
   {
      if (gOperIoLevel > I_1 || gDebug)
      {
         Mout << std::endl << " The full (vibrational) Watson operator will now be constructed " << std::endl;
      }

      if (coriolis)
      {
         MIDASERROR("The #3 Coriolis keyword is also specified. Only one of the keywords #3 FullCoriolis and #3 Coriolis can be set active at the same time!");
      }
      if (!(corfile.size() != I_0) || !(inertia.size() != I_0))
      {
         MIDASERROR("Missing information on Coriolis coupling matrices or inertia moments");
      }
      
      // Done with checking, now we can begin to do something
      if (gOperIoLevel > I_5 || gDebug)
      {
         Mout << " The full Coriolis coupling terms will be added to the Hamiltonian " << std::endl;
      }

      AddFullCoriolisOp
         (  new_oper
         ,  corfile
         ,  inertia
         ,  inp_inertia_file_names
         ,  inertia_file_names
         ,  inertia_file_map
         ,  restrict_modes
         ,  n_restric_modes
         ,  restrict_modes_in_wkeo
         ,  n_restric_modes_in_wkeo
         ,  screen_zero_term
         ,  zero_thr
         );
   }
   
   // if DUPLICATE of input is wanted then do it.
   if (n_dup > I_1)
   {
      new_oper.Duplicate(n_dup);
   }

   //
   if (gDebug)
   {
      Mout << " Mode nr.  |" << " Mode Label " << std::endl;
      for (In i = I_0; i < gOperatorDefs.GetNrOfModes(); i++)
      {
         Mout << setw(10) << i << " |  " << gOperatorDefs.GetModeLabel(i) << std::endl;
      }
   }

   // Set type of operator, do final checks and reorganize
   std::string oper_type = "";
   if (sop_op)
   {
      oper_type = "SOP";
   }
   new_oper.SetType(oper_type);

   // set IgnQiOnly before AddP.
   new_oper.SetIgnQiOnly(ign_qi_only); 
   new_oper.SetActTermAlg(act_term_alg); 
   if (n_coup > I_0)
   {
      new_oper.AddNeighbourCouplingTerms(n_coup, c_coup);
   }

   //
   new_oper.InitOpRange();

   // If only the general integral evaluation is used, then there is no need for inserting missing one-mode operators, as this way of evaluation is not based on a recursive strategy
   if (  gOperatorDefs.GetmBsplineGenIntEval()
      || keo_type == "POLYSPHERICAL"
      )
   {
      new_oper.Reorganize(false);
   }
   else
   {
      new_oper.Reorganize(true);
   }

   // Clean operator of repeated terms, (if there are any)
   new_oper.CleanOper();

   // Order terms in the operator according to mode combination
   new_oper.MCRReorder();
  
   // 
   gOperatorDefs.SetOperNameAndNr(new_oper.Name(), gOperatorDefs.GetNrOfOpers() - I_1);

   // Operator validation. All operators should have direct local->global
   // mapping and same number of modes.
   Uin exp_num_modes = (gOperatorDefs.GetNrOfOpers() > I_0) ? gOperatorDefs[0].NmodesInOp(): new_oper.NmodesInOp();
   std::stringstream ss_err;
   if (!ValidateOpDef(new_oper, exp_num_modes, ss_err, Mout))
   {
      // If ValidateOpDef returned false, detailed info has been output to Mout
      // and ss_err contains more concise error info.
      Mout << std::endl;
      MIDASERROR(ss_err.str());
   }
      
   // To break up the output into separate operator blocks 
   if (gOperIoLevel > I_1 || gDebug)
   {
      Mout << std::endl; 
   }

   return s;
}

} /* namespace input */
} /* namespace midas */
