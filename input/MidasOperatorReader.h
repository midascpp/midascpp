

#ifndef MIDASOPERATORREADER_H_INCLUDED
#define MIDASOPERATORREADER_H_INCLUDED

#include <vector>
#include <map>
#include <string>
#include <fstream>

#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "mmv/MidasVector.h"
#include "input/OperatorLessThan.h"
#include "mathlib/Taylor/taylor.hpp"

class OpDef;

/**
 *
 **/
class MidasOperatorReader
{
   private:

      //!
      std::string                                        mName;
      
      //!
      Nb                                                 mReferenceValue;
      //!
      MidasVector                                        mScalings;
      
      //!
      MidasVector                                        mFrequencies;
      
      //!
      std::vector<std::string>                           mModeNames;
      
      //!
      std::string                                        mInfoLabel;
      
      //!
      std::string                                        mElectronicDof;
      //!
      std::vector<std::pair<std::string, Nb> >           mConstants;
      
      //!
      std::vector<std::pair<std::string, std::string> >  mFunctions;
      
      //! Key is pair of mode and operator type. Value is coefficient.
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than> mOperatorTerms;

      //! Implemenentation of operator read-in.
      std::string ReadOperatorImpl(std::istream& aOpFile, In aInpLevel);

      //!
      std::pair<std::string, std::string> SeperateFunctionAndModeName(const string& aFunc);

      //!
      void RemoveModes(const std::vector<std::string>&);

      //! Function for setting some modes inactive and evaluating associated operator terms
      void SetInactiveModes(const std::vector<std::string>&);
      
      //! delete default constructor
      MidasOperatorReader() = delete;

   public:

      //!
      MidasOperatorReader(const std::string& aName = "default_operator_name");

      //!
      std::string ReadOperator(std::istream& aOpFile, In aInputLevel = I_0, bool aSkipHeaderAndFooter = false);
      
      //!
      std::string ReadOperator(const std::string& aFileName);
      
      //! Get operator file name
      const std::string& Name() const;
      
      //! Get reference value for operator (value at *#0*)
      Nb GetRefefenceValue() const {return mReferenceValue;}
      
      //!
      void ScaleOperator(Nb aScaling);
     
      //!
      const std::string& GetInfoLabel() const {return mInfoLabel;}

      //! Get mode names
      std::vector<std::string> GetModeNames() const;
      
      //!
      template<class T>
      void GetFunctionContainer(FunctionContainer<T>& aFuncCont) const
      {
         for(auto& constant : mConstants)
         {
            aFuncCont.InsertConstant(constant.first, T(constant.second));
         }
         for(auto& func : mFunctions)
         {
            aFuncCont.InsertFunction(func.first, func.second);
         }
      }

      //!
      void AddTermsToOpDef(OpDef& arOpDef, In aFnr) const;

      //!
      void GiveScalingAndNamestoOpDef(OpDef& arOpDef) const;

      //!
      void GetScalingVector(MidasVector& arScalingVector) const;
      bool GetScalingMap(std::map<std::string,Nb,modename_less_than>& arMap) const 
      {
         MidasVector scal_vec;
         GetScalingVector(scal_vec);
         if (scal_vec.Size()==I_0) return false;
         for (In i=I_0; i < mModeNames.size() ; ++i)
         {
            arMap.emplace(mModeNames.at(i),scal_vec[i]);
         }
         return true;
      }

      //! The following is used to interface to GenericBasePot
      std::map<std::pair<std::vector<std ::string>, std::vector<std::string> >, Nb, operator_less_than>::const_iterator GetFrontOfOperators() const;
      
      //!
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>::const_iterator GetEndOfOperators() const;

      //!
      std::vector<std::pair<std::string, Nb> > GetConstants() const;

      //!
      std::vector<std::pair<std::string, std::string> > GetFunctions() const;

      //!
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than> GetOperators() const;
};

#endif /* MIDASOPERATORREADER_H_INCLUDED */
