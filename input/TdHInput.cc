/*
************************************************************************
*
* @file                TdhInput.cc
*
* Created:             30-06-2011
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input reader for the TDH module
*
* Last modified:       April 9, 2018
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/TdHCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "input/FindKeyword.h"
#include "mpi/FileToString.h"

/*
* Read and check TDH input.
* Read until next s = "$i..." i<3 which is returned.
*/
std::string TdHInput
   (  std::istream& Minp
   )
{
   if (  gDoTdH
      && gDebug
      )
   {
      Mout << " I have read one TdH input - reading a new TdH input "<< endl;
   }

   gDoTdH = true;
   gTdHCalcDef.push_back(input::TdHCalcDef());   // Add to vector of TdH-calcs.
   input::TdHCalcDef& new_tdh = gTdHCalcDef[gTdHCalcDef.size()-I_1]; 

   // Enum/map with input flags
   enum INPUT  {  ERROR, NAME, OPER, BASIS, INTEGRATOR, IMAGTIME, WAVEFUNCTIONFORMODES, KAPPANORMTHRESHOLD, PROPERTIES, LINEARTDH, EXPONENTIALTDH
               ,  LINEARTDHCONSTRAINT, EXPECTATIONVALUES, NOAUTOCORRCONVOLUTION, SPECTRALBASIS, LIMITMODALBASIS, EXPTDHMAXFAILEDDERIVATIVES
               ,  NOLINTDHNORMALIZE, SCREENZEROCOEF, TWOMODEDENSITIES, WFGRIDDENSITY, IOLEVEL, INITIALWF, EXPTDHDERIVATIVEPROJECTIONTERMS
               ,  SPECTRUMENERGYSHIFT, SPECTRUMOUTPUTSCREENING, SPECTRUMINTERVAL, LIFETIME, WRITEPRIMITIVEBASISFORMODES, SPECTRA, FFTPADLEVEL
               ,  UPDATEONEMODEDENSITIES, WRITEONEMODEDENSITIES, SPACESMOOTHENING, TIMESMOOTHENING
#ifdef VAR_MPI
               ,  MPICOMMUNICATION
#endif /* VAR_MPI */
               };
   const map<string,INPUT> input_word =
   {  {"#3NAME",NAME}
   ,  {"#3OPER",OPER}
   ,  {"#3BASIS",BASIS}
   ,  {"#3INTEGRATOR",INTEGRATOR}
   ,  {"#3IMAGTIME",IMAGTIME}
   ,  {"#3WAVEFUNCTIONFORMODES", WAVEFUNCTIONFORMODES}
   ,  {"#3KAPPANORMTHRESHOLD",KAPPANORMTHRESHOLD}
   ,  {"#3PROPERTIES",PROPERTIES}
   ,  {"#3SPECTRA",SPECTRA}
   ,  {"#3LINEARTDH",LINEARTDH}
   ,  {"#3LINEARTDHCONSTRAINT", LINEARTDHCONSTRAINT}
   ,  {"#3EXPONENTIALTDH", EXPONENTIALTDH}
   ,  {"#3NOAUTOCORRCONVOLUTION", NOAUTOCORRCONVOLUTION}
   ,  {"#3EXPECTATIONVALUES", EXPECTATIONVALUES}
   ,  {"#3SPECTRALBASIS", SPECTRALBASIS}
   ,  {"#3LIMITMODALBASIS", LIMITMODALBASIS}
   ,  {"#3EXPTDHMAXFAILEDDERIVATIVES", EXPTDHMAXFAILEDDERIVATIVES}
   ,  {"#3NOLINTDHNORMALIZE", NOLINTDHNORMALIZE}
   ,  {"#3SCREENZEROCOEF", SCREENZEROCOEF}
   ,  {"#3TWOMODEDENSITIES", TWOMODEDENSITIES}
   ,  {"#3WFGRIDDENSITY", WFGRIDDENSITY}
   ,  {"#3IOLEVEL", IOLEVEL}
   ,  {"#3INITIALWF", INITIALWF}
   ,  {"#3EXPTDHDERIVATIVEPROJECTIONTERMS", EXPTDHDERIVATIVEPROJECTIONTERMS}
   ,  {"#3SPECTRUMENERGYSHIFT", SPECTRUMENERGYSHIFT}
   ,  {"#3SPECTRUMOUTPUTSCREENING", SPECTRUMOUTPUTSCREENING}
   ,  {"#3SPECTRUMINTERVAL", SPECTRUMINTERVAL}
   ,  {"#3LIFETIME", LIFETIME}
   ,  {"#3WRITEPRIMITIVEBASISFORMODES", WRITEPRIMITIVEBASISFORMODES}
   ,  {"#3FFTPADLEVEL", FFTPADLEVEL}
   ,  {"#3UPDATEONEMODEDENSITIES", UPDATEONEMODEDENSITIES}
   ,  {"#3WRITEONEMODEDENSITIES", WRITEONEMODEDENSITIES}
   ,  {"#3SPACESMOOTHENING", SPACESMOOTHENING}
   ,  {"#3TIMESMOOTHENING", TIMESMOOTHENING}
#ifdef VAR_MPI
   ,  {"#3MPICOMMUNICATION", MPICOMMUNICATION}
#endif /* VAR_MPI */
   };

   bool method_set = false;

   std::string s = "";
   std::string s_orig = "";
   In input_level = 3;
   bool already_read = false;
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case NAME:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_tdh.SetName(s);
            break;
         }
         case OPER:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_tdh.SetOper(s);
            break;
         }
         case BASIS:
         {
            // Get new line with input
            midas::input::GetLine(Minp, s);
            new_tdh.SetBasis(s);
            break;
         }
         case INTEGRATOR:
         {
            already_read = new_tdh.ReadOdeInfo(Minp, s);
            break;
         }
         case IMAGTIME:
         {
            new_tdh.SetImagTime(true);

            // Calculate energy on the fly when doing imag-time propagation
            new_tdh.AddProperty("ENERGY");

            break;
         }
         case WAVEFUNCTIONFORMODES:
         {
            // Get line
            midas::input::GetLine(Minp, s);

            // Get vector
            auto mode_vec = midas::util::StringVectorFromString(s);

            // Convert to set
            std::set<std::string> mode_set(mode_vec.begin(), mode_vec.end());

            new_tdh.SetWfOutModes(mode_set);
            break;
         }
         case WRITEPRIMITIVEBASISFORMODES:
         {
            midas::input::GetLine(Minp, s);

            auto mode_vec = midas::util::StringVectorFromString(s);

            std::set<std::string> mode_set(mode_vec.begin(), mode_vec.end());

            new_tdh.SetWritePrimitiveBasisForModes(mode_set);

            break;
         }
         case KAPPANORMTHRESHOLD:
         {
            auto thr = midas::input::GetNb(Minp, s);
            new_tdh.SetKappaNormThreshold(thr);
            break;
         }
         case PROPERTIES:
         {
            already_read = new_tdh.ReadProperties(Minp, s);
            break;
         }
         case SPECTRA:
         {
            already_read = new_tdh.ReadSpectra(Minp, s);
            break;
         }
         case LINEARTDH:
         {
            if (  method_set
               )
            {
               MIDASERROR("TDH parametrization (LinearTdH or ExponentialTdH) can only be set once!");
            }

            new_tdh.SetParametrization(midas::tdh::tdhID::LINEAR);
            method_set = true;
            break;
         }
         case LINEARTDHCONSTRAINT:
         {
            already_read = new_tdh.ReadLinearTdHConstraint(Minp, s);
            break;
         }
         case EXPONENTIALTDH:
         {
            if (  method_set
               )
            {
               MIDASERROR("TDH parametrization (LinearTdH or ExponentialTdH) can only be set once!");
            }

            new_tdh.SetParametrization(midas::tdh::tdhID::EXPONENTIAL);
            new_tdh.SetSpectralBasis(true);
            method_set = true;
            break;
         }
         case NOAUTOCORRCONVOLUTION:
         {
            new_tdh.SetConvoluteAutoCorr(false);
            break;
         }
         case EXPECTATIONVALUES:
         {
            already_read = new_tdh.ReadExptValOpers(Minp, s);
            break;
         }
         case SPECTRALBASIS:
         {
            new_tdh.SetSpectralBasis(true);

            midas::input::GetLine(Minp, s);
            if (  midas::input::NotKeyword(s)
               )
            {
               new_tdh.SetSpectralBasisOper(s);
            }
            else
            {
               new_tdh.SetSpectralBasisOper(""); // empty string will give operator for VSCF reference calculation.
               already_read = true;
            }

            break;
         }
         case LIMITMODALBASIS:
         {
            // Get input line
            midas::input::GetLine(Minp, s);

            // Convert to In vector
            auto vec = midas::util::VectorFromString<In>(s);

            // Set limits
            new_tdh.SetModalBasisLimits(vec);

            // Spectral basis must be true in order to truncate
            new_tdh.SetSpectralBasis(true);

            break;
         }
         case EXPTDHMAXFAILEDDERIVATIVES:
         {
            In max = midas::input::GetIn(Minp, s);
            new_tdh.SetExpTdHMaxFailedDerivatives(max);
            break;
         }
         case NOLINTDHNORMALIZE:
         {
            new_tdh.SetLinearTdHNormalizeVectors(false);
            break;
         }
         case SCREENZEROCOEF:
         {
            auto zero = midas::input::GetNb(Minp, s);
            new_tdh.SetScreenZeroCoef(zero);
            break;
         }
         case TWOMODEDENSITIES:
         {
            already_read = new_tdh.ReadTwoModeDensityPairs(Minp, s);
            break;
         }
         case UPDATEONEMODEDENSITIES:
         {
            In step = midas::input::GetIn(Minp, s);
            new_tdh.SetUpdateOneModeDensities(true);
            new_tdh.SetUpdateOneModeDensitiesStep(step);
            break;
         }
         case WRITEONEMODEDENSITIES:
         {
            new_tdh.SetWriteOneModeDensities(true);
            break;
         }
         case SPACESMOOTHENING:
         {
            In smooth = midas::input::GetIn(Minp, s);
            new_tdh.SetSpaceSmoothening(smooth);
            break;
         }
         case TIMESMOOTHENING:
         {
            In smooth = midas::input::GetIn(Minp, s);
            new_tdh.SetTimeSmoothening(smooth);
            break;
         }
         case WFGRIDDENSITY:
         {
            auto dens = midas::input::GetIn(Minp, s);
            new_tdh.SetWfGridDensity(dens);
            break;
         }
         case IOLEVEL:
         {
            auto io = midas::input::GetIn(Minp, s);
            new_tdh.SetIoLevel(io);
            break;
         }
         case INITIALWF:
         {
            already_read = new_tdh.ReadInitialWfPolicy(Minp, s);

            break;
         }
         case EXPTDHDERIVATIVEPROJECTIONTERMS:
         {
            new_tdh.SetExpTdHDerivativeProjectionTerms(true);
            break;
         }
         case SPECTRUMENERGYSHIFT:
         {
            Nb e_shift = C_0;
            std::string type = "FIXED";

            midas::input::GetLine(Minp, s);
            auto s_vec = midas::util::StringVectorFromString(s);
            assert(s_vec.size() > 0);

            if (  s_vec[0] == "FIXED"
               )
            {
               assert(s_vec.size() == 2);
               type = s_vec[0];
               e_shift = midas::util::FromString<Nb>(s_vec[1]);
            }
            else if  (  s_vec[0] == "E0"
                     )
            {
               assert(s_vec.size() == 1);
               type = "E0";
               e_shift = C_0;
            }
            else
            {
               MIDASERROR("Unrecognized energy-shift type: " + s_vec[0]);
            }
            
            new_tdh.SetSpectrumEnergyShift(std::pair<std::string, Nb>(type, e_shift));
            break;
         }
         case SPECTRUMOUTPUTSCREENING:
         {
            Nb thresh = midas::input::GetNb(Minp, s);
            new_tdh.SetSpectrumOutputScreeningThresh(thresh);
            break;
         }
         case SPECTRUMINTERVAL:
         {
            midas::input::GetLine(Minp, s);
            auto [f0, f1] = midas::util::TupleFromString<Nb, Nb>(s);
            
            new_tdh.SetSpectrumInterval(std::pair<Nb, Nb>(f0, f1));

            break;
         }
         case LIFETIME:
         {
            auto tau = midas::input::GetNb(Minp, s);
            new_tdh.SetLifeTime(tau);
            break;
         }
         case FFTPADLEVEL:
         {
            new_tdh.SetPaddingLevel(midas::input::GetIn(Minp, s));
            break;
         }
#ifdef VAR_MPI
         case MPICOMMUNICATION:
         {
            already_read = new_tdh.ReadMpiComm(Minp, s);
            break;
         }
#endif /* VAR_MPI */
         case ERROR: // Fall through to default
         default:
         {
            Mout << " Keyword " << s_orig << " is not a TdH level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check sanity
   new_tdh.SanityCheck();

   return s;
}

/**
 * Initialize TdH variables
 * */
void InitializeTdH()
{
   gDoTdH = false; 
}
