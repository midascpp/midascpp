/**
************************************************************************
* 
* @file                TotalRspFunc.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function framework
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TOTALLINRSP_H
#define TOTALLINRSP_H

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream> 
using std::ostream;
#include <map>
#include <list>
using std::list;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/TotalResponseDrv.h"
#include "input/Contribution.h"
#include "input/TotalResponseContribution.h"

/**
* Construct a definition of a total response calculation calculation
* Derived from RspFunc
* */
class TotalLinRsp
   : public TotalResponseDrv
{
   ///< For now, no class attibutes
   private:
      bool mIso;
      bool mPvl;
      bool mZpvc;
      Nb mFrq;
      vector<vector<Contribution> > mEquivalent;
      set<Contribution> mDefault;

      // private functions
      void CheckKeys(const std::map<string,string>&);
      void InitExperimental(const map<string,string>&);
      void InitContributions(const map<string,string>&);
      void InitTypes(const map<string,string>&);
      void InitFrq(const map<string,string>&);
      void InitRules(const map<string,string>&);
      void ConstructionError(const string aS,const map<string,string>&);

   public:
      ///< Constructor. Needs a map of keys-values and an order
      ///< keys-values map contains all the remaining stuff
      TotalLinRsp(const std::map<string,string>&);
      ///< Evaluate the total linear response function
      map<string,TotalResponseContribution> ProvideMap();
      vector<Nb> GetFrqVec() 
      {
         vector<Nb> v;
         v.push_back(mFrq);
         return v;
      }
      ///< Output overload
      //friend ostream& operator<<(ostream&, const TotalLinRsp&);
      void ConstructIntermediateSet();
      ostream& Print(ostream&) const;
};

#endif

