/**
************************************************************************
* 
* @file                MidasOperatorInput.cc
* 
* Created:             04-02-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Reader for the midas .mop files
* 
* Last modified: Mon Feb 04, 2014  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <vector>
#include <utility>
#include <fstream>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/conversions/VectorFromString.h"
#include "util/Io.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/MidasOperatorReader.h"

namespace midas
{
namespace input
{
namespace detail
{

//! @cond DOXYGEN_SUPPRESS
/**
 * check if an operator terms should ignored on read in
 * @param aOper
 * @param aVal
 * @param aMcMax
 * @param arModeIn
 * @param aShouldBeIn
 * @return returns 'true' if the opreator term should be ignored, 'false' otherwise
 **/
//! @endcond DOXYGEN_SUPPRESS
//bool CheckIfTermShouldBeIgnored( const std::vector<std::string>& aOper
//                               , Nb aVal
//                               , In aMcMax
//                               , In arModeIn
//                               , bool aShouldBeIn
//                               )
//{
//   //If the term is too small simply ignore it (IAN: Warning this is an absolute check, it assumes the largest term is on the order of 1.0)
//   if(std::fabs(aVal) < C_NB_EPSILON)
//      return true;
//   
//   //If the Mc level is too high ignore the term
//   if(aMcMax < aOper.size())
//      return true;
//
//   In mc = -1;
//   std::string mode_name("");
//   if(arModeIn != -I_1) 
//      mode_name = gOperatorDefs.GetModeLabel(arModeIn);
//   bool ignore = true;
//   if(aShouldBeIn && !mode_name.empty())
//   {
//      for(In i = I_0; i < aOper.size(); ++i)
//         ignore = ignore && (std::string::npos == aOper[i].find(mode_name));
//   }
//   else
//      ignore = false;
//   return ignore;
//}

/**
 * Classify the operator based on the string that represents it and set associated mode and coefficient. The operator is here assumed to be read in directly from the input.
 **/
void OperFromInputReadIn
   (  const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>& aOperLines
   ,  OpDef& arOpDef
   ,  const FunctionContainer<Nb>& aFuncCont
   )
{
   // Loop over all lines of operator information
   for (auto it = aOperLines.begin(); it != aOperLines.end(); ++it)
   {
      // Determine the mode that the operator is associated to
      auto it_modes = it->first.first.begin();

      // Send the one-mode operator as a string to be processed, without the mode label
      for (auto it_funcs = it->first.second.begin(); it_funcs != it->first.second.end(); ++it_modes, ++it_funcs)
      {
         arOpDef.AddGenOneModeOperProd
            (  arOpDef.Nterms()
            ,  *it_modes
            ,  *it_funcs
            ,  (it_modes == it->first.first.begin())
            ,  false
            ,  aFuncCont
            );
      }
      arOpDef.SetNterms(arOpDef.Nterms()+I_1);

      // If we have a coupling term
      if (  it->first.first.size() > 1
         )
      {
         arOpDef.IncrNCouplingTerms();
      }

      arOpDef.AddCterm(it->second);
   }
}

/**
 * Classify the operator based on the string that represents it and set associated mode and coefficient. The operator is here assumed to be read in from a file.
 **/
void KeoFromFileReadIn
   (  const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>& aOperLines
   ,  OpDef& arOpDef
   ,  const FunctionContainer<Nb>& aFuncCont
   )
{
   for (auto it = aOperLines.begin(); it != aOperLines.end(); ++it)
   {
      auto it_modes = it->first.first.begin();
      for (auto it_funcs = it->first.second.begin(); it_funcs != it->first.second.end(); ++it_modes, ++it_funcs)
      {
           arOpDef.AddGenOneModeOperProd
            (  arOpDef.Nterms()
            ,  *it_modes
            ,  *it_funcs
            ,  (it_modes == it->first.first.begin())
            ,  true
            ,  aFuncCont
            );
      }
      arOpDef.SetNterms(arOpDef.Nterms()+I_1);

      // If we have a coupling term
      if (  it->first.first.size() > 1
         )
      {
         arOpDef.IncrNCouplingTerms();
      }

      arOpDef.AddCterm(it->second);
   }
}

/**
 * Classify the operator based on the string that represents it and set associated mode and coefficient. The operator is here assumed to be read in from a file.
 **/
void OperFromFileReadIn
   (  const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>& aOperLines
   ,  OpDef& arOpDef
   ,  const FunctionContainer<Nb>& aFuncCont
   ,  In aFnr
   )
{
   for (auto it = aOperLines.begin(); it != aOperLines.end(); ++it)
   {
      bool not_found_mode_1 = true;
      bool not_found_mode_2 = true;
      std::string mode_name;
      auto it_modes = it->first.first.begin();

      for (auto it_funcs = it->first.second.begin(); it_funcs != it->first.second.end(); ++it_funcs, ++it_modes)
      {
         std::string function = *it_funcs;
         std::string mode = *it_modes;
         GlobalModeNr g_mode_nr = gOperatorDefs.ModeNr(mode, true);
         
         if (arOpDef.GetModeNr1ForOpFile(aFnr) != -I_1 && arOpDef.GetModeNr1ForOpFile(aFnr) == g_mode_nr)
         {
            not_found_mode_1 = false;
            function = "DDQ"+function;
         }
         
         if (arOpDef.GetModeNr2ForOpFile(aFnr) != -I_1 && arOpDef.GetModeNr2ForOpFile(aFnr) == g_mode_nr)
         {
            not_found_mode_2 = false;
            function = function + "DDQ";
         }
        
         //
         arOpDef.AddGenOneModeOperProd
            (  arOpDef.Nterms()
            ,  *it_modes
            ,  function
            ,  (it_modes == it->first.first.begin())
            ,  false
            ,  aFuncCont
            );
      }

      if (arOpDef.GetModeNr1ForOpFile(aFnr) != -I_1 && not_found_mode_1)
      {
         GlobalOperNr der_oper;
         mode_name = gOperatorDefs.GetModeLabel(arOpDef.GetModeNr1ForOpFile(aFnr));
         if(arOpDef.GetModeNr2ForOpFile(aFnr) == arOpDef.GetModeNr1ForOpFile(aFnr))
         {
            arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), mode_name, I_0, I_2, false, false);
            not_found_mode_2 = false;
         }
         else
         {
            arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), mode_name, I_0, I_1, false, false);
         }
      }

      if (arOpDef.GetModeNr2ForOpFile(aFnr) != -I_1 && not_found_mode_2) 
      {
         mode_name = gOperatorDefs.GetModeLabel(arOpDef.GetModeNr2ForOpFile(aFnr));
         arOpDef.AddSimpleOneModeOperProd(arOpDef.Nterms(), mode_name, I_0, I_1, false, false);
      }
      
      arOpDef.SetNterms(arOpDef.Nterms()+I_1);

      // If we have a coupling term
      if (  it->first.first.size() > 1
         )
      {
         arOpDef.IncrNCouplingTerms();
      }

      arOpDef.AddCterm(it->second);
   }
}

} //namespace detail


} // namespace input
} // namespace midas
