#ifndef OPERATORLESSTHAN_H_INCLUDED
#define OPERATORLESSTHAN_H_INCLUDED


std::tuple<std::string, int, std::string> extract_integer( const std::string& aFunc);

/**
 *
 **/
class operator_less_than
{
   private:
      /**
       * extracts first integer of string for sorting
       * @param aFunc the function string to analyse
       * @return tuple of prefix, int, postfix
       **/
      std::tuple<std::string, int, std::string> extract_integer( const std::string& aFunc
                                                               ) const
      {
         int exponent_beg_pos = aFunc.find_first_of("0123456789");
         int exponent_end_pos = aFunc.find_first_not_of("0123456789", exponent_beg_pos);
         exponent_beg_pos = exponent_beg_pos == std::string::npos ? aFunc.size() : exponent_beg_pos;
         exponent_end_pos = exponent_end_pos == std::string::npos ? aFunc.size() : exponent_end_pos;
         
         int exponent_length = exponent_end_pos - exponent_beg_pos;

         std::string func_prefix = aFunc.substr(0, exponent_beg_pos);
         int exponent = (exponent_length ? midas::util::FromString<In>(aFunc.substr(exponent_beg_pos, exponent_length))
                                         : 0
                        );
         std::string func_postfix = aFunc.substr(exponent_end_pos);
         
         return std::tuple<std::string, int, std::string>(func_prefix, exponent, func_postfix);
      }

   public:
      /**
       * less than for pair of mode and function strings
       * @param aOper1 first operator to compare 
       * @param aOper2 second operator to compare
       * @return is aOper1 less than aOper2
       **/
      bool operator()( const std::pair<std::vector<std::string>, std::vector<string> >& aOper1
                     , const std::pair<std::vector<std::string>, std::vector<string> >& aOper2        
                     ) const
      {
         // first check on mode combi
         if(aOper1.first.size() < aOper2.first.size()) 
         {
            return true;
         }
         else if(aOper1.first.size() > aOper2.first.size())
         {
            return false;
         }
         else // mode combi is the same size, check on modes, then on operators
         {
            for(int i = 0; i < aOper1.first.size(); ++i)
            {
               // extract integer
               std::tuple<std::string, int, std::string> t1 = extract_integer(aOper1.first[i]);
               std::tuple<std::string, int, std::string> t2 = extract_integer(aOper2.first[i]);
               if(t1 < t2)
               {
                  return true;
               }
               else if(t1 > t2)
               {
                  return false;
               }
            }
            
            assert(aOper1.first.size() == aOper1.second.size());
            assert(aOper1.second.size() == aOper2.second.size()); // sizes should be the same or we would have exited function before
            for(int i = 0; i < aOper1.second.size(); ++i)
            {
               // extract exponent
               std::tuple<std::string, int, std::string> t1 = extract_integer(aOper1.second[i]);
               std::tuple<std::string, int, std::string> t2 = extract_integer(aOper2.second[i]);
               if(t1 < t2)
               {
                  return true;
               }
               else if(t1 > t2)
               {
                  return false;
               }
            }
         }

         return false;
      }
};

class test_operator_less_than
{
   public:
      bool operator()( const std::pair<std::vector<std::string>, std::vector<string> >& aOper1
                     , const std::pair<std::vector<std::string>, std::vector<string> >& aOper2        
                     ) const
      {
         return aOper1 > aOper2;
      }
};

/**
 *
 **/
class modename_less_than
{
   private:

   public:
      /**
       * operator() less than for pair of mode and function strings
       * @param aName1 first Name to compare 
       * @param aName2 second Name to compare
       * @return is aName1 less than aName2
       **/
       // extract integer
      bool operator()( const std::string& aName1
                     , const std::string& aName2        
                     ) const
      {
         std::tuple<std::string, int, std::string> t1 = extract_integer(aName1);
         std::tuple<std::string, int, std::string> t2 = extract_integer(aName2);
         {
            if(t1 < t2)
            {
               return true;
            }
            else if(t1 > t2)
            {
               return false;
            }
         }
         return false;
      }
};

#endif /* OPERATORLESSTHAN_H_INCLUDED */
