/**
************************************************************************
* 
* @file                RspInput.cc
*
* 
* Created:             19-04-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the response information 
* 
* Last modified: Sun May 16, 2010  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <regex>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/RspCalcDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "input/RspFunc.h"
#include "util/MultiIndex.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosLinRspFunc.h"
#include "vcc/LanczosIRspect.h"
#include "vcc/BandLanczosLinRspFunc.h"
#include "vcc/BandLanczosIRspect.h"
#include "input/GeneralInputParser.h"
#include "input/TotalResponseDrv.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "input/TensorDecompInput.h"
#include "input/GetLine.h"

// using declarations
using std::vector;
using std::map;
using std::make_pair;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

// Constants for switching between different lanczos methods.
enum Lanczos {DO_LANCZOS_IR, DO_BANDLANCZOS_IR, DO_BANDLANCZOS_RAMAN};

// Local declarations
//vector<RspFunc> MakeExptValues();
set<RspFunc> MakePVLinearResponseFunctions(vector<string>&,vector<Nb>,In&,In&);
set<RspFunc> MakePVQuadraticResponseFunctions(vector<string>&,vector<Nb>,In&,In&);
set<RspFunc> MakePVMixedResponseFunctions(vector<string>&,vector<string>&,vector<string>&,vector<Nb>);
void CreateAllOperPairs(set<vector<string> >&,set<string>&,vector<string>&);
void AddAllCubicLinear(RspCalcDef&,vector<Nb>);
void AddAlphaSquared(set<RspFunc>&,vector<pair<string,Nb> >&);
void AddMuBeta(set<RspFunc>&,vector<pair<string,Nb> >&);
void AddMuSquaredAlpha(set<RspFunc>&,vector<pair<string,Nb> >&);
void AddAllCubicQuadratic(RspCalcDef&,vector<Nb>);
set<string> GenerateAllPermLanczosQR(vector<string>&);

/**
 *
 **/
void AlreadyRead(bool& aAlreadyRead, string& s)  
{
   transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
   while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
   aAlreadyRead =true;
}

/**
 *
 **/
void ParseLanczosSpect(const string& aInp, RspCalcDef& aRspCalcDef, const In aMethod)
{
   string name;
   string xdip;
   string ydip;
   string zdip;
   string analyze;
   string analysis_blocks;

   bool table = false;
   Nb weight_lim = C_I_5; //0.2   
   Nb peak_lim = C_2;
   if (0 != GetValueFromKey<string>(aInp, "NAME", name))
      MIDASERROR(" ParseLanczosSpect(): No name specified.");

   GetValueFromKey<string>(aInp, "ANALYZE", analyze);          
   
   GetValueFromKey<bool>(aInp, "MAKE_TABLE", table);

   GetValueFromKey<Nb>(aInp, "WEIGHT_LIM", weight_lim);

   GetValueFromKey<Nb>(aInp, "PEAK_LIM", peak_lim);

   switch(aMethod)
   {
      case DO_LANCZOS_IR:
      {
         if (0 != GetValueFromKey<string>(aInp, "XDIP", xdip))
            MIDASERROR(" ParseLanczosSpect(): No xdip specified for Lanczos IR.");
         if (0 != GetValueFromKey<string>(aInp, "YDIP", ydip))
            MIDASERROR(" ParseLanczosSpect(): No ydip specified for Lanczos IR.");
         if (0 != GetValueFromKey<string>(aInp, "ZDIP", zdip))
            MIDASERROR(" ParseLanczosSpect(): No zdip specified for Lanczos IR.");
         
         GetValueFromKey<string>(aInp, "ANALYSISBLOCKS", analysis_blocks);

         aRspCalcDef.AddLanczosIRspect(new LanczosIRspect(name,
                                       xdip, ydip, zdip,
                                       analyze, analysis_blocks, 
                                       table, weight_lim, peak_lim));

         break;
      }
      case DO_BANDLANCZOS_IR:
      {
         string blrsp;
         if (0 != GetValueFromKey<string>(aInp, "BLRSP", blrsp))
            MIDASERROR(" ParseLanczosSpect(): No blrsp specified for Band Lanczos IR.");
         
         aRspCalcDef.AddBandLanczosIRspect(new BandLanczosIRspect(name,blrsp,table, weight_lim, peak_lim,analyze));
         break;
      }
      case DO_BANDLANCZOS_RAMAN:
      {
         string blrsp;
         string freq;
         if (0 != GetValueFromKey<string>(aInp, "BLRSP", blrsp))
            MIDASERROR(" ParseLanczosSpect(): No blrsp specified for Band Lanczos RAMAN.");
         if (0 != GetValueFromKey<string>(aInp, "FREQ", freq))
            MIDASERROR(" ParseLanczosSpect(): No freq specified for Band Lanczos RAMAN.");        
            
         aRspCalcDef.AddBandLanczosRamanSpect(new BandLanczosRamanSpect(name,blrsp,analyze,NbFromString(freq)));
         break;
      }
      default:
      {
         ///> Output to the programmers for debugging the code :P
         Mout << " No method selected to do the Lanczos-type IR calculation " << endl << endl;
         MIDASERROR("Programmer output: Set Lanczos-type IR calculation method in the code");
      }
   }
}

Nb RspFuncReadGamma(std::string aStr)
{
   std::string s_hit = "FORGAMMA";
   aStr.erase(0,aStr.find(s_hit));
   aStr.erase(aStr.find(s_hit),s_hit.size());
   Nb gamma;
   istringstream input_s(aStr);
   input_s >> gamma;
   return gamma;
}

void ParseRspFunc(std::string& s, RspCalcDef& arCalcDef)
{
   //getline(Minp,s);                                  // Get new line with input
   istringstream input_s(s);
   string s_forn = s;
   transform(s_forn.begin(),s_forn.end(),s_forn.begin(),(In(*) (In))toupper);         // Transform to all upper.
   string s_hit = "SOS";
   bool sos = false;
   if (s_forn.find(s_hit) != s_forn.npos) sos = true;
   In n_ord;
   while (input_s >> n_ord)
   {
      if (n_ord < -I_11 || (n_ord < -I_1 && n_ord > -I_11) ) MIDASERROR("Order must be at least -11 in RspInput ");
      if (n_ord == I_0) MIDASERROR("Order must not be 0 in RspInput ");
      RspFunc rsp_func_in(n_ord);
      rsp_func_in.SetSos(sos);
      //Mout << "I got this string: " << s << endl;
      for (In i=I_0;i<abs(n_ord);i++) 
      {
         string s_op;
         input_s >> s_op;
         //Mout << "Now setting oper " << i << " = " << s_op << endl;
         rsp_func_in.SetOp(s_op,i);
         if (n_ord == -11) break;
      }
      //Mout << "RF = " << rsp_func_in << endl;
      //
      // Check where ForFrq option else read in one-by-one
      //
      //string s_forn = s;
      //transform(s_forn.begin(),s_forn.end(),s_forn.begin(),(In(*) (In))toupper);         // Transform to all upper.
      string s_2np = "NO2NP2";
      if (s_forn.find(s_2np) != s_forn.npos)
         rsp_func_in.SetNoTwoNPlusTwo(true);
      
      string s_nom = "NOM";
      if (s_forn.find(s_nom) != string::npos)
         rsp_func_in.SetUseMvec(false);
      
      string s_hit = "FORFRQ";
      string s_hit_2 = "FORSTATES";
      string s_hit_3 = "FORLEFTSTATES";
      string s_hit_4 = "FORRIGHTSTATES";
      if (s_forn.find(s_hit) != s_forn.npos)
      {
         if (n_ord > I_3) MIDASERROR("ForFrq only implemented up to quadratic response yet ");

         In n_frq       = I_1;
         Nb frq_begin   = C_0;
         Nb frq_end     = C_0;
         Nb gamma       = C_0;
         string s_trash,asym;
         bool inverse_cm = false;
         std::string s_hit_6 = "INVERSECM";
         if(s_forn.find(s_hit_6) != s_forn.npos)
         {
            inverse_cm = true;
         }

         std::string s_hit_5 = "FORGAMMA";
         if(s_forn.find(s_hit_5) != s_forn.npos)
         {
            gamma = RspFuncReadGamma(s_forn);
            rsp_func_in.SetGamma(inverse_cm ? gamma/C_AUTKAYS: gamma);
         }

         while (input_s >> s_trash >> frq_begin >> frq_end >> n_frq && n_frq>I_0)
         {
            if(input_s >> asym && (asym=="-" || asym=="A" || asym=="ASYM"))
            {
               rsp_func_in.SetDoAsymmetric(true);
            }
            if(inverse_cm)
            {
               frq_begin/=C_AUTKAYS;
               frq_end/=C_AUTKAYS;
            }
            Nb frq_delta   = n_frq == 1 ? 0.0 : (frq_end-frq_begin)/(n_frq-I_1);
            //Mout << " frq_delta " << frq_delta << endl;
            // Linear response 
            if (n_ord == I_2)
            {
               for (In i=I_0;i<n_frq;i++) 
               {
                  Nb frq = frq_begin + frq_delta*i;
                  rsp_func_in.SetFrq(frq,I_1);  // Frequencies reset 
                  rsp_func_in.SetFrq(-frq,I_0);
                  arCalcDef.AddRspFunc(rsp_func_in);
               }
            }
            // Quadratic response 
            // two frequencies must be specified, the third follows from the other two
            // so only for shg here
            if (n_ord == I_3)
            {
               for (In i=I_0;i<n_frq;i++) 
               {
                  Nb frq = frq_begin + frq_delta*i;
                  rsp_func_in.SetFrq(frq,I_1);
                  rsp_func_in.SetFrq(frq,I_2);
                  rsp_func_in.SetFrq(-C_2*frq,I_0);
                  arCalcDef.AddRspFunc(rsp_func_in);
               }
            }
         }
      }
      else if (s_forn.find(s_hit_2) != s_forn.npos)
      { 
         s_forn.erase(0,s_forn.find(s_hit_2));
         s_forn.erase(s_forn.find(s_hit_2),s_hit_2.size());
         string s_trash;
         vector<In> stat_vec(0);
         stat_vec = midas::util::VectorFromString<In>(s_forn);
         if (n_ord > -10) {
            for (In is=I_0;is<stat_vec.size();is++)
            { 
               rsp_func_in.SetRightState(stat_vec[is]);  // Frequencies reset 
               rsp_func_in.SetLeftState(I_0);  // Frequencies reset 
               arCalcDef.AddRspFunc(rsp_func_in);
            }
         }
         else if(n_ord > -100) {
            // make all unique pairs of states
            // for transition matrix elements between two excited states
            for(In is=I_0;is<stat_vec.size();is++) {
               // mbh for(In it=is+1;it<stat_vec.size();it++) {
               for(In it=is;it<stat_vec.size();it++) {
                  rsp_func_in.SetRightState(stat_vec[it]);  // Frequencies reset 
                  rsp_func_in.SetLeftState(stat_vec[is]);  // Frequencies reset 
                  arCalcDef.AddRspFunc(rsp_func_in);
               }
            }
            Mout << "Found " << arCalcDef.NrspFuncs() << " excited state transition "
                 << "matrix elements." << endl;
         }
         else {
            // Perhaps cubic response here, but not implemented yet...
            MIDASERROR("Not implemented for cubic response yet! You specified: " + std::to_string(n_ord));
         }
      } 
      else if (s_forn.find(s_hit_3) != s_forn.npos || s_forn.find(s_hit_4) != s_forn.npos)
      { 
         // FORLEFTSTATES implies that FORRIGHTSTATES should also be set
         vector<In> right_stat_vec(0);
         vector<In> left_stat_vec(0);
         if (s_forn.find(s_hit_4) != s_forn.npos && 
             s_forn.find(s_hit_3) != s_forn.npos &&
             s_forn.find(s_hit_4) > s_forn.find(s_hit_3)) {
            // leftstates first
            s_forn.erase(0,s_forn.find(s_hit_3));
            s_forn.erase(s_forn.find(s_hit_3),s_hit_3.size());
            left_stat_vec = midas::util::VectorFromString<In>(s_forn.substr(0,s_forn.find(s_hit_4)));
            s_forn.erase(0,s_forn.find(s_hit_4));
            s_forn.erase(s_forn.find(s_hit_4),s_hit_4.size());
            right_stat_vec = midas::util::VectorFromString<In>(s_forn);
         }
         else if (s_forn.find(s_hit_4) != s_forn.npos && 
                  s_forn.find(s_hit_3) != s_forn.npos &&
                  s_forn.find(s_hit_4) < s_forn.find(s_hit_3)) {
            // rightstates first
            s_forn.erase(0,s_forn.find(s_hit_4));
            s_forn.erase(s_forn.find(s_hit_4),s_hit_4.size());
            right_stat_vec = midas::util::VectorFromString<In>(s_forn.substr(0,s_forn.find(s_hit_3)));
            s_forn.erase(0,s_forn.find(s_hit_3));
            s_forn.erase(s_forn.find(s_hit_3),s_hit_3.size());
            left_stat_vec = midas::util::VectorFromString<In>(s_forn);
         }
         else if (s_forn.find(s_hit_4) == s_forn.npos && 
                  s_forn.find(s_hit_3) != s_forn.npos) {
            MIDASERROR("FORRIGHTSTATES FLAG NOT SPECIFIED!");
         }
         else {
            // Either FORRIGHTSTATES or FORLEFTSTATES is not specified => quit.
            MIDASERROR("FORLEFTSTATES FLAG NOT SPECIFIED!");
         }
         for (In is=I_0;is<right_stat_vec.size();is++) {
            for (In it=I_0;it<left_stat_vec.size();it++) {
               if (left_stat_vec[it] < right_stat_vec[is]) { // only excitation
                  rsp_func_in.SetRightState(right_stat_vec[is]);
                  rsp_func_in.SetLeftState(left_stat_vec[it]);
                  arCalcDef.AddRspFunc(rsp_func_in);
                  //Mout << " LEFT = " << left_stat_vec[it] << " RIGHT = " << right_stat_vec[is] << endl;
               }
            }
         }
      }
      else
      {
         In i_frq=I_0;
         Nb frq=C_0;
         Nb sum_frq = C_0;
         vector<Nb> frq_vec;
         while (++i_frq < abs(n_ord) && input_s >> frq)
         {
            rsp_func_in.SetFrq(frq,i_frq);
            sum_frq += frq;
            frq_vec.push_back(frq);
         }
         Nb end_frq=sum_frq;
         string extra;
         input_s >> extra;
         transform(extra.begin(),extra.end(),extra.begin(),(In(*) (In))toupper);         // Transform to all upper.
         if(extra=="A" || extra=="-" || extra=="ASYM") {
            Mout << "Will therefore set asym = true" << endl;
            rsp_func_in.SetDoAsymmetric(true);
         }
         if(extra=="APPROX")
         {
            // SET THAT IT IS AN APPROX RSP FUNC!!!!
            // input is like: 2 x y 0.001 APPROX 10
            //                3 x y z 0.001 0.001 APPROX 10
            arCalcDef.AddApproxRspFunc();
            rsp_func_in.SetApproxRspFunc(true);
            // remember to set the frequencies equal to zero
            sum_frq=C_0;
            for(In ifrq=I_1;ifrq<rsp_func_in.GetNfrqs();ifrq++)
            {
               rsp_func_in.SetFrq(C_0,ifrq);
            }
            // There should be an argument more, the number of states in
            // the SOS expr. for building the F function
            In states=I_0;
            input_s >> states;
            // Add rsp. funcs. accordingly!
            vector<In> stat_vec(0);
            string str_states="0-"+std::to_string(states);
            stat_vec = midas::util::VectorFromString<In>(str_states);
            for(In iex=I_0;iex<rsp_func_in.GetNopers();iex++) {
               for (In is=I_0;is<stat_vec.size();is++)
               {
                  RspFunc extra_rf(-I_1);
                  extra_rf.SetOp(rsp_func_in.GetRspOp(iex));
                  extra_rf.SetRightState(stat_vec[is]);
                  extra_rf.SetLeftState(I_0);
                  bool do_add=true;
                  for(In ircd=I_0;ircd<arCalcDef.NrspFuncs();ircd++) {
                     if(arCalcDef.GetRspFunc(ircd)==extra_rf) {
                        do_add=false;
                     }
                  }
                  if(do_add)
                     arCalcDef.AddRspFunc(extra_rf);
               }
               // make all unique pairs of states
               // for transition matrix elements between two excited states
               for(In is=I_0;is<stat_vec.size();is++) {
                  for(In it=is;it<stat_vec.size();it++) {
                     RspFunc extra_rf(-I_11);
                     extra_rf.SetOp(rsp_func_in.GetRspOp(iex));
                     extra_rf.SetRightState(stat_vec[it]);
                     extra_rf.SetLeftState(stat_vec[is]);
                     bool do_add=true;
                     for(In ircd=I_0;ircd<arCalcDef.NrspFuncs();ircd++) {
                        if(arCalcDef.GetRspFunc(ircd)==extra_rf) {
                        do_add=false;
                        }
                     }
                     if(do_add)
                        arCalcDef.AddRspFunc(extra_rf);
                  }
               }
            }
            // finally, set the SOS frq's if they are not set previously
            if(arCalcDef.SosFreqSteps()==I_0)
               arCalcDef.SetSosFreqs(C_0,end_frq,rsp_func_in.GetNfrqs()-I_1);
         }
         rsp_func_in.SetFrq(-sum_frq,I_0);
         for (In i=i_frq+I_1;i<abs(n_ord);i++) rsp_func_in.SetFrq(C_0,i);
         arCalcDef.AddRspFunc(rsp_func_in);
      }
   }

}


/**
* Read and check response input.
* Read until next s = "#i..." 4<3 which is returned.
* */
string RspInput(std::istream& Minp,VscfCalcDef& arCalcDef, bool is_vscf)
{
   //if (gDoVscf && gDebug) 
      //Mout << " I have read one Vscf input - reading a new Vscf input "<< endl;
   //gDoVscf    = true; 
   arCalcDef.SetDoRsp(true);

   // Enum/map with input flags
   enum INPUT {ERROR, IOLEVEL, RESTART, DIAGMETH, 
      DUPLICATE, ITEQMAXIT,ITEQMAXDIM,ITEQBREAKDIM,ITEQRESIDTHR,ITEQRESIDTHRREL,ITEQRESCUEMAX, ITEQENERTHR,
      NODIIS,MAXDIIS,TIMEIT,EIGENVAL,VCISASVSCF,RSPFUNC,TARGETSPACE,
      ALLFUNDAMENTALS,ALLFIRSTOVERTONES,FUNDAMENTALS,FIRSTOVERTONES,
      SCREENZERORSP,SCREENZERORSPC,SOSFREQS,QRFFREQS,CRFFREQS,NUMVCCJACOBIAN,NUMVCCF,
      DAMPINGFACTOR,DOTOTALRESPONSE, 
      TRUEHDIAG,IMPROVEDPRECOND,TARGETINGMETHOD,OVERLAPMIN,ENERGYDIFFMAX,
      OVERLAPSUMMIN, OVERLAPNMAX, RESIDTHRFOROTHERS,ALSOLINEARASYM,ENERTHRFOROTHERS,TRUEADIAG,
      EIGVALSEQ,USEAVAILABLE,MAPPREVIOUSVECTORS,LEVEL2SOLVER,NRESVECS,
      HARMONICRR, USERAYLEIGHQUOTIENTSASEIGVALS, ENERSHIFT, USEENERSHIFTINPRECON, ADAPTIVEENERSHIFT, REFINEDHARMONICRR, LEFTEIG,LANCZOSRSP,LANCZOSSPECT,LANCZOSIR,
      BANDLANCZOSIR,BANDLANCZOSRAMAN,ALLCUBICLINEAR,ALLCUBICQUADRATIC,
      SOSRSP,TENSORDECOMP,DECOMPEIG,ITEQORTHOMAXITER,ITEQTHRESHORTHODISCARD,
      ITEQTHRESHORTHORETRY,FORCECOMPLEXRSP,ITEQSAVETODISC,ITEQRESTARTVECTOR,ITEQCOMPLEXRESIDTHR,
      ITEQSUBSPACEPRECONVECTOR,
      LAMBDATHRESHOLD, ITEQSOLTHR, ITEQPRECONRESIDTHR, ITEQPRECONSOLTHR, COMPLEXRSPOLSEN, COMPLEXIMPROVEDPRECOND,
      ITEQPRECONMAXITER, COMPLEXPRECONDUNMUTE, COMPLEXIMPROVEDPRECONDEXPLICIT,
      DENSITYANALYSIS, ITNAMO, NOSYMDENSNAMO, ITEQSUBSPACEPRECONFULL, ITEQRELCONV,
      VCCTENSORTYPE, ITEQCHECKLINEARDEPENDENCEOFSPACE,
      USENEWCONTRACTIONS, RSPTENSORDECOMPINFO, RSPTRFTENSORDECOMPINFO, BALANCETENSORSAFTERPRECON, RECOMPTENSORSAFTERPRECON, ABSPRECON, NOOLSEN, OLSENDECOMPRELTHRESH
      ,  NOGENDOWNCTRRECOMP, CHECKTENSORVCCTRANSJAC, TENSORVCCJACUSEDEBUGVECTOR, ADIAGVCCREFENERGY, LINEQMINIMUMRESIDUAL
      ,  TENSORRSPSOLVERCONVERTUNITS, TENSORRSPPREDIAG, RSPTRANSFORMERALLOWEDRANK, RSPTENSORPRODUCTSCREENING, RSPTENSORPRODUCTLOWRANKLIMIT
      ,  TENSORRSPORTHOSCREENING, TENSORRSPNOLINSOLVERPRECON, RANKANALYSIS, FULLRANKANALYSIS, NOREITERATETENSORRSPSOLVER, REITERATETENSORRSPSOLVER, NORECOMPRESSCPVCCEIGVEC
      ,  TENSORRSPFUNCS
      };
   
   const map<string,INPUT> input_word =
   {
         {"#4IOLEVEL",IOLEVEL},
         {"#4RESTART",RESTART},
         {"#4USEAVAILABLE",USEAVAILABLE},
         {"#4MAPPREVIOUSVECTORS",MAPPREVIOUSVECTORS},
         {"#4DIAGMETH",DIAGMETH},
         {"#4DUPLICATE",DUPLICATE},
         {"#4ITEQMAXIT",ITEQMAXIT},
         {"#4ITEQMAXDIM",ITEQMAXDIM},
         {"#4ITEQBREAKDIM",ITEQBREAKDIM},
         {"#4ITEQRESIDTHR",ITEQRESIDTHR},
         {"#4ITEQRESIDTHRREL",ITEQRESIDTHRREL},
         {"#4ITEQRESCUEMAX",ITEQRESCUEMAX},
         {"#4ITEQENERTHR",ITEQENERTHR},
         {"#4NODIIS",NODIIS},
         {"#4MAXDIIS",MAXDIIS},
         {"#4TIMEIT",TIMEIT},
         {"#4EIGENVAL",EIGENVAL},
         {"#4VCISASVSCF",VCISASVSCF},
         {"#4RSPFUNC",RSPFUNC},
         {"#4TARGETSPACE",TARGETSPACE},
         {"#4ALLFIRSTOVERTONES",ALLFIRSTOVERTONES},
         {"#4ALLFUNDAMENTALS",ALLFUNDAMENTALS},
         {"#4FIRSTOVERTONES",FIRSTOVERTONES},
         {"#4FUNDAMENTALS",FUNDAMENTALS},
         {"#4SCREENZERORSP",SCREENZERORSP},
         {"#4SCREENZERORSPC",SCREENZERORSPC},
         {"#4SOSFREQS",SOSFREQS},
         {"#4QRFFREQS",QRFFREQS},
         {"#4CRFFREQS",CRFFREQS},
         {"#4NUMVCCJACOBIAN",NUMVCCJACOBIAN},
         {"#4NUMVCCF",NUMVCCF},
         {"#4DAMPINGFACTOR",DAMPINGFACTOR},
         {"#4DOTOTALRESPONSE",DOTOTALRESPONSE},
         //{"#4AUTOGENERATERSPFUNC",AUTOGENERATERSPFUNC},
         //{"#4ALLEXPTVALUES",ALLEXPTVALUES},
         {"#4TRUEHDIAG",TRUEHDIAG},
         {"#4IMPROVEDPRECOND",IMPROVEDPRECOND},
         {"#4TARGETINGMETHOD",TARGETINGMETHOD},
         {"#4OVERLAPMIN",OVERLAPMIN},
         {"#4OVERLAPSUMMIN",OVERLAPSUMMIN},
         {"#4OVERLAPNMAX", OVERLAPNMAX},
         {"#4ENERGYDIFFMAX",ENERGYDIFFMAX},
         {"#4RESIDTHRFOROTHERS",RESIDTHRFOROTHERS},
         {"#4ENERTHRFOROTHERS",ENERTHRFOROTHERS},
         {"#4TRUEADIAG",TRUEADIAG},
         {"#4EIGVALSEQ",EIGVALSEQ},
         {"#4LEVEL2SOLVER",LEVEL2SOLVER},
         {"#4ALSOLINEARASYM",ALSOLINEARASYM},
         {"#4NRESVECS",NRESVECS},
         {"#4HARMONICRR",HARMONICRR},
         {"#4ENERSHIFT",ENERSHIFT},
         {"#4USEENERSHIFTINPRECON",USEENERSHIFTINPRECON},
         {"#4ADAPTIVEENERSHIFT",ADAPTIVEENERSHIFT},
         {"#4REFINEDHARMONICRR", REFINEDHARMONICRR},
         {"#4LEFTEIG",LEFTEIG},
         {"#4LANCZOSRSP",LANCZOSRSP},
         {"#4LANCZOSSPECT",LANCZOSSPECT},
         {"#4LANCZOSIR",LANCZOSIR},
         {"#4BANDLANCZOSIR",BANDLANCZOSIR},
         {"#4BANDLANCZOSRAMAN",BANDLANCZOSRAMAN},
         {"#4ALLCUBICLINEAR",ALLCUBICLINEAR},
         {"#4ALLCUBICQUADRATIC",ALLCUBICQUADRATIC},
         //{"#4SCALEPOLAROPERATORS",SCALEPOLAROPERATORS},
         {"#4SOSRSP",SOSRSP},
         {"#4TENSORDECOMP",TENSORDECOMP},
         {"#4DECOMPEIG",DECOMPEIG},
         {"#4ITEQORTHOMAXITER",ITEQORTHOMAXITER},
         {"#4ITEQTHRESHORTHODISCARD",ITEQTHRESHORTHODISCARD},
         {"#4ITEQTHRESHORTHORETRY",ITEQTHRESHORTHORETRY},
         {"#4FORCECOMPLEXRSP",FORCECOMPLEXRSP},
         {"#4ITEQSAVETODISC",ITEQSAVETODISC},
         {"#4ITEQRESTARTVECTOR",ITEQRESTARTVECTOR},
         {"#4ITEQSUBSPACEPRECONVECTOR",ITEQSUBSPACEPRECONVECTOR},
         {"#4ITEQCOMPLEXRESIDTHR",ITEQCOMPLEXRESIDTHR},
         {"#4LAMBDATHRESHOLD",LAMBDATHRESHOLD}
    ,    {"#4ITEQSOLTHR",ITEQSOLTHR}
    ,    {"#4ITEQPRECONRESIDTHR",ITEQPRECONRESIDTHR}
    ,    {"#4ITEQPRECONSOLTHR",ITEQPRECONSOLTHR}
    ,    {"#4COMPLEXRSPOLSEN", COMPLEXRSPOLSEN}
    ,    {"#4COMPLEXIMPROVEDPRECOND",COMPLEXIMPROVEDPRECOND}
    ,    {"#4ITEQPRECONMAXITER",ITEQPRECONSOLTHR}
    ,    {"#4COMPLEXPRECONDUNMUTE",COMPLEXPRECONDUNMUTE}
    ,    {"#4COMPLEXIMPROVEDPRECONDEXPLICIT",COMPLEXIMPROVEDPRECONDEXPLICIT}
    ,    {"#4DENSITYANALYSIS",DENSITYANALYSIS}
    ,    {"#4ITNAMO",ITNAMO}
    ,    {"#4NOSYMDENSNAMO",NOSYMDENSNAMO}
    ,    {"#4ITEQSUBSPACEPRECONFULL",ITEQSUBSPACEPRECONFULL}
    ,    {"#4VCCTENSORTYPE",VCCTENSORTYPE}
    ,    {"#4ITEQRELCONV",ITEQRELCONV}
    ,    {"#4ITEQCHECKLINEARDEPENDENCEOFSPACE",ITEQCHECKLINEARDEPENDENCEOFSPACE}
    ,    {"#4USENEWCONTRACTIONS",USENEWCONTRACTIONS}
    ,    {"#4RSPTENSORDECOMPINFO",RSPTENSORDECOMPINFO}
    ,    {"#4RSPTRFTENSORDECOMPINFO",RSPTRFTENSORDECOMPINFO}
    ,    {"#4BALANCETENSORSAFTERPRECON", BALANCETENSORSAFTERPRECON}
    ,    {"#4RECOMPTENSORSAFTERPRECON", RECOMPTENSORSAFTERPRECON}
    ,    {"#4ABSPRECON", ABSPRECON}
    ,    {"#4NOOLSEN", NOOLSEN}
    ,    {"#4OLSENDECOMPRELTHRESH", OLSENDECOMPRELTHRESH}
    ,    {"#4LINEQMINIMUMRESIDUAL", LINEQMINIMUMRESIDUAL}
    ,    {"#4NOGENDOWNCTRRECOMP", NOGENDOWNCTRRECOMP}
    ,    {"#4CHECKTENSORVCCTRANSJAC", CHECKTENSORVCCTRANSJAC}
    ,    {"#4TENSORVCCJACUSEDEBUGVECTOR", TENSORVCCJACUSEDEBUGVECTOR}
    ,    {"#4ADIAGVCCREFENERGY", ADIAGVCCREFENERGY}
    ,    {"#4TENSORRSPSOLVERCONVERTUNITS", TENSORRSPSOLVERCONVERTUNITS}
    ,    {"#4TENSORRSPPREDIAG", TENSORRSPPREDIAG}
    ,    {"#4RSPTRANSFORMERALLOWEDRANK", RSPTRANSFORMERALLOWEDRANK}
    ,    {"#4RSPTENSORPRODUCTSCREENING", RSPTENSORPRODUCTSCREENING}
    ,    {"#4RSPTENSORPRODUCTLOWRANKLIMIT", RSPTENSORPRODUCTLOWRANKLIMIT}
    ,    {"#4TENSORRSPORTHOSCREENING", TENSORRSPORTHOSCREENING}
    ,    {"#4USERAYLEIGHQUOTIENTSASEIGVALS", USERAYLEIGHQUOTIENTSASEIGVALS}
    ,    {"#4TENSORRSPNOLINSOLVERPRECON", TENSORRSPNOLINSOLVERPRECON}
    ,    {"#4RANKANALYSIS", RANKANALYSIS}
    ,    {"#4FULLRANKANALYSIS", FULLRANKANALYSIS}
    ,    {"#4NOREITERATETENSORRSPSOLVER", NOREITERATETENSORRSPSOLVER}
    ,    {"#4REITERATETENSORRSPSOLVER", REITERATETENSORRSPSOLVER}
    ,    {"#4NORECOMPRESSCPVCCEIGVEC", NORECOMPRESSCPVCCEIGVEC}
    ,    {"#4TENSORRSPFUNCS", TENSORRSPFUNCS}
   };

   // Initialization of default values for quantities read in later &  Local params.
   vector<In> occ_vec;
   vector<In> occmax_vec;
   vector<In> occmaxexprmode_vec;
   occ_vec.reserve(gReserveNmodes);
   occmax_vec.reserve(gReserveNmodes);
   occmaxexprmode_vec.reserve(gReserveNmodes);
   //In max_sum_n = 0;
   //In max_exci  = 0;
   In n_dup     = 1;
   //bool max_sum_n_set = false;
   //bool max_exci_set  = false;

   bool target        = false;
   bool all_fund      = false;
   bool all_first     = false;
   bool fund          = false;
   bool first         = false;


   string s;
   In input_level = 4;
   bool already_read = false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read=false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In vscf_io_level;
            input_s >> vscf_io_level;
            arCalcDef.SetRspIoLevel(vscf_io_level);
            break;
         }
         case RESTART:
         {
            bool restart = true;
            arCalcDef.SetRspRestart(restart);
            break;
         }
         case USEAVAILABLE:
         {
            bool use_available = true;
            arCalcDef.SetRspUseAvailable(use_available);
            break;
         }
         case MAPPREVIOUSVECTORS:
         {
            // Check if restart info is available
            if (InquireFile(gAnalysisDir+"/VCCRESTART.INFO"))
            {
               Mout << " Rsp Input: Restart info file found." << endl;
               bool map_it = true;
               arCalcDef.SetRspMapPreviousVectors(map_it);
            }
            else MIDASERROR(" Rsp Input: Restart info file not found.");
            break;
         }
         case DIAGMETH:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string diag_meth;
            input_s >> diag_meth;
            transform(diag_meth.begin(),diag_meth.end(),diag_meth.begin(),(In(*) (In))toupper); // Transform to all upper.
            arCalcDef.SetRspDiagMeth(diag_meth);
            break;
         }
         case DUPLICATE:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> n_dup;
            break;
         }
         case ITEQMAXIT:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In max; 
            input_s >> max;
            arCalcDef.SetRspItEqMaxIter(max);
            break;
         }
         case ITEQMAXDIM:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In max; 
            input_s >> max;
            arCalcDef.SetRspRedDimMax(max);
            break;
         }
         case ITEQBREAKDIM:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In max; 
            input_s >> max;
            arCalcDef.SetRspRedBreakDim(max);
            break;
         }
         case ITEQRESIDTHR:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspItEqResidThr(thr);
            break;
         }
         case ITEQRESIDTHRREL:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspItEqResidThrRel(thr);
            break;
         }
         case ITEQRESCUEMAX:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb max; 
            input_s >> max;
            arCalcDef.SetRspItEqRescueMax(max);
            break;
         }
         case ITEQENERTHR:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspItEqEnerThr(thr);
            break;
         }
         case NODIIS: 
         {
            arCalcDef.SetRspDiis(false);
            break;
         }
         case MAXDIIS: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In maxd;
            while (input_s >> maxd)
            {
               arCalcDef.SetRspMaxDiis(maxd);
            }
            break;
         }
         case TIMEIT: 
         {
            arCalcDef.SetRspTimeIt(true);
            break;
         }
         case EIGENVAL: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In maxd;
            while (input_s >> maxd)
            {
               arCalcDef.SetDoRspEig(true);
               arCalcDef.SetRspNeig(maxd);
            }
            break;
         }
         case VCISASVSCF: 
         {
            arCalcDef.SetDoNoPaired(true);
            break;
         }
         case DOTOTALRESPONSE:
         {
            // mbh: new interface
            map<string,string> global_map;
            vector<map<string,string> > vec_of_local;
            GeneralInputParser parser(I_4,Minp,&global_map,"RSP",&vec_of_local);
            for(In i=0;i<vec_of_local.size();i++) {
               map<string,string>::iterator loc;
               loc=vec_of_local[i].find("ORDER");
               In order=0;
               if(loc!=vec_of_local[i].end())
                  order=InFromString(loc->second);
               else
                  MIDASERROR("Did not find key: ORDER in total response input.");
               TotalResponseDrv* total_rsp=TotalResponseDrv::Factory(order,vec_of_local[i]);
               arCalcDef.AddTotalResponseDrv(total_rsp);
            }
            break;
         }
         //case AUTOGENERATERSPFUNC:
         //{
         //   // Read one line with all the LR freqs if present
         //   getline(Minp,s);
         //   vector<Nb> freqs;
         //   istringstream iss(s);   
         //   Nb tmpfreq;
         //   while(iss >> tmpfreq)
         //      freqs.push_back(tmpfreq);
         //   // Loop through the Operators and find those which have been automatically set
         //   // and make a vector of dipoole moment operators
         //   //vector<OpDef> dipole_opers,polar_opers,hpolar_opers,asym_opers;
         //   vector<string> dipole_opers,polar_opers,hpolar_opers, asym_opers;
         //   for(In i=I_0;i<gOperatorDefs.GetNrOfOpers();i++) {
         //      if(gOperatorDefs[i].GetIsAutoGenerated() && 
         //         gOperatorDefs[i].Name().find("ANTI")==string::npos) {
         //         OpInfo oi=OpDef::msOpInfo[gOperatorDefs[i].Name()];
         //         if(oi.GetType()<I_200 && oi.GetType()>I_100) { // dipole!
         //            //dipole_opers.push_back(*gOperatorDefs.GetOperator(i));
         //            dipole_opers.push_back(gOperatorDefs[i].Name());
         //         }
         //         else if(oi.GetType()<I_300 && oi.GetType()>I_200) { // pol!
         //            Mout << "WE HAVE A POLARIZABILITY!!!" << endl;
         //            Mout << "ALSO ASYM? " << arCalcDef.GetAlsoLinearAsym() << endl;
         //            if(arCalcDef.GetAlsoLinearAsym()) {
         //               // also add a modified operator to that list
         //               //polar_opers.push_back(gOperators[i]);
         //               polar_opers.push_back(gOperatorDefs[i].Name());
         //               Nb scale_fac=C_0;
         //               if(gOperatorDefs[i].Name().find("X")!=string::npos) {
         //                  // XX, XY, or XZ: if it is Z, we just take that
         //                  // otherwise we go with X
         //                  if(gOperatorDefs[i].Name().find("Z")!=string::npos)
         //                     scale_fac=arCalcDef.GetAsymScale(2);
         //                  else
         //                     scale_fac=arCalcDef.GetAsymScale(0);
         //               }
         //               else if(gOperatorDefs[i].Name().find("Y")!=string::npos) {
         //                  // YY, or YZ: if it is Z, we just take that
         //                  // otherwise we go with X
         //                  if(gOperatorDefs[i].Name().find("Z")!=string::npos)
         //                     scale_fac=arCalcDef.GetAsymScale(2);
         //                  else
         //                     scale_fac=arCalcDef.GetAsymScale(1);
         //               }
         //               else
         //                  scale_fac=arCalcDef.GetAsymScale(2);
         //               OpDef op_copy=gOperatorDefs[i];
         //               op_copy.Scale(-oi.GetFrq1()/scale_fac);
         //               string new_name=gOperatorDefs[i].Name();
         //               new_name.insert(new_name.find("POL")+3,"_ANTI");
         //               op_copy.SetName(new_name);
         //               asym_opers.push_back(new_name);
         //               gOperatorDefs.AddOperator(op_copy);
         //               OpDef::msOpInfo[new_name]=oi;
         //            }
         //            else
         //               polar_opers.push_back(gOperatorDefs[i].Name());
         //         }
         //         else if(oi.GetType()<I_400 && oi.GetType()>I_300) { // h-pol!
         //            hpolar_opers.push_back(gOperatorDefs[i].Name());
         //         }
         //      }
         //      // Also the "real" asym opers
         //      else if(gOperatorDefs[i].Name().find("ANTI")!=string::npos) {
         //         asym_opers.push_back(gOperatorDefs[i].Name());
         //      }
         //   }
         //   // Add the extra operators
         //   //for(In i=I_0;i<asym_opers.size();i++)
         //   //   gOperators.push_back(asym_opers[i]);
         //   // sort these vectors
         //   Mout << "DIPOLE OPERS BEFORE:" << endl;
         //   for(In i=I_0;i<dipole_opers.size();i++)
         //      Mout << "OPER: " << dipole_opers[i] << endl;
         //   sort(dipole_opers.begin(), dipole_opers.end());
         //   Mout << "DIPOLE OPERS AFTER:" << endl;
         //   for(In i=I_0;i<dipole_opers.size();i++)
         //      Mout << "OPER: " << dipole_opers[i] << endl;
         //   sort(polar_opers.begin(), polar_opers.end());
         //   sort(hpolar_opers.begin(), hpolar_opers.end());
         //   // need to make a vector!
         //   vector<RspFunc> e_val_vec = MakeExptValues();
         //   In index1=I_0,index2=I_0;
         //   set<RspFunc> lrf_set;; 
         //   lrf_set = MakePVLinearResponseFunctions(dipole_opers,freqs,index1,index2);
         //   index1=I_2; // We are only interested in zii,izi,iiz components for QR
         //   index2=I_0; // We are only interested in zii,izi,iiz components for QR
         //   set<RspFunc> qrf_set; 
         //   qrf_set = MakePVQuadraticResponseFunctions(dipole_opers,freqs,index1,index2);
         //   // And now for the tricky part: All the mixed response functions
         //   set<RspFunc> mixed_set = MakePVMixedResponseFunctions(polar_opers,asym_opers,dipole_opers,freqs);
         //   /*
         //      Just print some output/status
         //   */
         //   Mout << "MakeExptValues returned the following RF:" << endl << endl;
         //   for(In i=I_0;i<e_val_vec.size();i++) {
         //      Mout << "RF = " << e_val_vec[i] << endl;
         //      arCalcDef.AddRspFunc(e_val_vec[i]);
         //   }
         //   Mout << "MakePVLinearResponseFunctions returned the following RF:" << endl << endl;
         //   for(set<RspFunc>::const_iterator i=lrf_set.begin();i!=lrf_set.end();i++) {
         //      Mout << "RF = " << *i << endl;
         //      RspFunc tmp_rf=*i;
         //      arCalcDef.AddRspFunc(tmp_rf);
         //   }
         //   Mout << "MakePVQuadraticResponseFunctions returned the following RF:" << endl << endl;
         //   for(set<RspFunc>::const_iterator i=qrf_set.begin();i!=qrf_set.end();i++) {
         //      Mout << "RF = " << *i << endl;
         //      RspFunc tmp_rf=*i;
         //      arCalcDef.AddRspFunc(tmp_rf);
         //   }
         //   Mout << "MakePVMixed returned the following RF:" << endl << endl;
         //   for(set<RspFunc>::const_iterator i=mixed_set.begin();i!=mixed_set.end();i++) {
         //      Mout << "RF = " << *i << endl;
         //      RspFunc tmp_rf=*i;
         //      arCalcDef.AddRspFunc(tmp_rf);
         //   }
         //   // MIDASERROR("CODE STILL IN ITS BEGINNING STATE!");
         //   break;
         //}
         //case ALLEXPTVALUES:
         //{
         //   vector<RspFunc> e_val_vec = MakeExptValues();
         //   Mout << "MakeExptValues returned the following RF:" << endl << endl;
         //   for(In i=I_0;i<e_val_vec.size();i++) {
         //      Mout << "RF = " << e_val_vec[i] << endl;
         //      arCalcDef.AddRspFunc(e_val_vec[i]);
         //   }
         //   break;
         //}
         case RSPFUNC: 
         {
            if(!is_vscf && (!arCalcDef.DoRspEig() || (arCalcDef.GetRspNeig() == 0)))
            {
               arCalcDef.SetDoRspEig(true);
               arCalcDef.SetRspNeig(1);
            }

            while (getline(Minp,s)&& s.find("#")==s.npos) // s.substr(I_0,I_1)!="#")
            {
               ParseRspFunc(s, arCalcDef); 
            }
            AlreadyRead(already_read,s); 
            break;
         }
         case TARGETSPACE:
         {
            target = true;
            arCalcDef.SetRspTargetSpace(target);
            break;
         }
         case ALLFUNDAMENTALS:
         {
            all_fund = true;
            arCalcDef.SetRspAllFundamentals(all_fund);
            break;
         }
         case ALLFIRSTOVERTONES:
         {
            all_first = true;
            arCalcDef.SetRspAllFirstOvertones(all_first);
            break;
         }
         case FUNDAMENTALS:
         {
            fund = true;
            getline(Minp,s);                                  // Get new line with input
            vector<In> fund_vec = midas::util::VectorFromString<In>(s);
            arCalcDef.SetRspFundamentals(fund_vec,fund);
            break;
         }
         case FIRSTOVERTONES:
         {
            first = true;
            getline(Minp,s);                                  // Get new line with input
            vector<In> first_vec = midas::util::VectorFromString<In>(s);
            arCalcDef.SetRspFirstOvertones(first_vec,first);
            break;
         }
         case SCREENZERORSP: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetScreenZeroRsp(thr);
            break;
         }
         case SCREENZERORSPC: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetScreenZeroRspC(thr);
            break;
         }
         case SOSFREQS: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb begin,end; 
            In n_freq=0;
            if ((input_s >> begin >> end >> n_freq)&& n_freq>0) arCalcDef.SetSosFreqs(begin,end,n_freq-I_1);
            break;
         }
         case QRFFREQS:
         {
            getline(Minp,s);
            istringstream input_s(s);
            Nb frq1,frq2;
            if(input_s >> frq1 >> frq2) arCalcDef.SetQrfSosFreqs(frq1,frq2);
            break;
         }
         case CRFFREQS:
         {
            getline(Minp,s);
            istringstream input_s(s);
            Nb frq1,frq2,frq3;
            if(input_s >> frq1 >> frq2 >> frq3) arCalcDef.SetCrfSosFreqs(frq1,frq2,frq3);
            break;
         }
         case NUMVCCJACOBIAN:
         {
            getline(Minp, s);
            istringstream input_s(s);
            Nb step = C_0;
            input_s >> step;
            arCalcDef.SetNumVccJacobian(step);
            break;
         }
         case NUMVCCF:
         {
            getline(Minp, s);
            istringstream input_s(s);
            Nb step = C_0;
            input_s >> step;
            arCalcDef.SetNumVccF(step);
            break;
         }
         case DAMPINGFACTOR:
         {
            getline(Minp,s);
            istringstream input_s(s);
            Nb damp=C_0;
            if(input_s >> damp) arCalcDef.SetDampingFactor(damp);
            break;
         }
         case TRUEHDIAG:
         {
            arCalcDef.SetRspTrueHDiag(true);                           
            break;
         }
         case TRUEADIAG:
         {
            arCalcDef.SetRspTrueADiag(true);                           
            break;
         }
         case IMPROVEDPRECOND: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In nexcipre=I_0;
            input_s >> nexcipre;
            arCalcDef.SetRspImprovedPrecond(nexcipre);
            break;
         }
         case LEVEL2SOLVER:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            arCalcDef.SetRspLevel2Solver(meth);
            break;
         }
         case TARGETINGMETHOD:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            arCalcDef.SetRspTargetingMethod(meth);
            break;
         }
         case OVERLAPMIN:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspOverlapMin(thr);
            break;
         }
         case OVERLAPSUMMIN:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspOverlapSumMin(thr);
            break;
         }
         case OVERLAPNMAX:
         {
            auto max = midas::input::GetIn(Minp, s);
            arCalcDef.SetRspOverlapNMax(max);
            break;
         }
         case ENERGYDIFFMAX:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            thr/=C_AUTKAYS;                                   // energy conversion cm^{-1} to au
            arCalcDef.SetRspEnergyDiffMax(thr);
            break;
         }
         case RESIDTHRFOROTHERS:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
             arCalcDef.SetRspResidThrForOthers(thr);
            break;
         }
         case ENERTHRFOROTHERS:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb thr; 
            input_s >> thr;
            arCalcDef.SetRspEnerThrForOthers(thr);
            break;
         }
         case EIGVALSEQ: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In scale=-I_1;
            if (input_s >> scale) arCalcDef.SetRspEigValSeq(scale);
            break;
         }
        case ALSOLINEARASYM:
        {
            getline(Minp,s);
            istringstream input_s(s);
            // a line with the lowest dipole allowed electronic excitation
            // energies for each component, if e.g. x and y are equivalent,
            // the user must input this as 0.5 0.5 0.9
            Mout << "WE MUST ALSO MAKE ASYMS..." << endl;
            vector<Nb> freqs(3);
            input_s >> freqs[0] >> freqs[1] >> freqs[2];
            arCalcDef.SetAsymScale(freqs);
            arCalcDef.SetAlsoLinearAsym(true);
            break;
         }
         case NRESVECS: 
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In n_vecs=I_0;
            input_s >> n_vecs;
            arCalcDef.SetRspNresvecs(n_vecs);
            break;
         }
         case HARMONICRR:
         {
            arCalcDef.SetRspHarmonicRR(true);
            break;
         }
         case USERAYLEIGHQUOTIENTSASEIGVALS:
         {
            arCalcDef.SetRspUseRayleighQuotientsAsEigvals(true);
            break;
         }
         case ENERSHIFT:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb shift=C_0;
            input_s >> shift;
            arCalcDef.SetRspEnerShift(shift);
            break;
         }
         case USEENERSHIFTINPRECON:
         {
            arCalcDef.SetRspUseEnerShiftInPrecon(true);
            break;
         }
         case ADAPTIVEENERSHIFT:
         {
            arCalcDef.SetRspAdaptiveHarmonicRrShift(true);
            break;
         }
         case REFINEDHARMONICRR:
         {
            arCalcDef.SetRspRefinedHarmonicRr(true);
            break;
         }
         case TENSORRSPNOLINSOLVERPRECON:
         {
            arCalcDef.SetTensorRspNoLinSolverPrecon(true);
            break;
         }
         case LEFTEIG:
         {
            arCalcDef.SetRspLeftEig(true);
            break;
         }
         case TENSORRSPFUNCS:
         {
            arCalcDef.SetTensorRspFuncs(true);
            break;
         }
         case LANCZOSRSP:
         {
            /* NEW LANCZOS RESPONSE INPUT
               FORMAT:
               gammas=(a,b,c,...)
               chainlength=a
               frq_range=(A:B), A,B > 0
               frq_step=a
               operators=(x,y,z)
               solver=a
               rsp=(N,x_1,x_2,...,x_N,frq_1,frq_2,...,frq_{N-1})
            */
            if(!arCalcDef.DoRspEig() || (arCalcDef.GetRspNeig() == 0))
            {
               arCalcDef.SetDoRspEig(true);
               arCalcDef.SetRspNeig(1);
            }
            arCalcDef.SetDoRspFuncs(true);

            LanczosRspFunc::InitConstStringMaps();
            
            // 1) Identify keywords in each string, loop until next #
            string list;
            map<string,string> global_key_map;
            vector<map<string,string>  > vec_of_local_keys;
            GeneralInputParser parser(I_4,Minp,&global_key_map,"RSP",&vec_of_local_keys);
            
            // check that the order makes sense
            if(vec_of_local_keys.size()==0)
            {
               MIDASERROR("At least one \"RSP\" entry must be present in Lanczos response input.");
            }

            for(In i=0;i<vec_of_local_keys.size();i++) 
            {
               map<string,string> local_key_map=vec_of_local_keys[i];
               map<string,string>::iterator loc;
               In order=0;
               string band="true";
               bool Band = true;
               string nherm="false";
               bool NHerm = false;
               string name;
               
               loc=local_key_map.find("NHERM");
               if(loc!=local_key_map.end())
                  nherm = (loc->second);
               if(nherm == "true") 
                  NHerm = true;
               else if (nherm == "false")
                  NHerm = false;
               else
                  MIDASERROR("NHerm = " + nherm + " is unknown.");
               loc=local_key_map.find("BAND");
               if(loc!=local_key_map.end())
                  band = (loc->second);
               if(band == "true") 
                  Band = true;
               else if(band == "false")
                  Band = false;
               else
                  MIDASERROR("Band = " + band + " is unknown.");
               loc=local_key_map.find("NAME");
               if(loc!=local_key_map.end())
                  name = (loc->second);
               loc=local_key_map.find("ORDER");
               if(loc!=local_key_map.end())
                  order=InFromString(loc->second);
               if(order<2 || order>3)
                  MIDASERROR("Order = "+std::to_string(order)+" is not implemented.");

               // Add operators as regular response functions as, we need the multipliers to do Lanczos rsp
               // Tihs is sort of a hack, but a pretty one :)
               loc=local_key_map.find("OPERS");
               if(loc != local_key_map.end())
               {
                  std::regex regex("[^(), ]+");
                  for
                     (  std::sregex_iterator i = std::sregex_iterator
                        (  (loc->second).begin()
                        ,  (loc->second).end()
                        ,  regex
                        )
                     ;  i != std::sregex_iterator()
                     ;  ++i 
                     )
                  {
                      std::smatch m = *i;
                      std::string rsp_str("1 " + m.str());
                      ParseRspFunc(rsp_str, arCalcDef);
                  }
               }
               else
               {
                  MIDASERROR("No operators given.");
               }
               
               if(Band)
               {
                  switch(order)
                  {
                     case I_2:
                     {
                        LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                        p->SetName(name);
                        arCalcDef.AddLanczosRspFunc(p);
                        break;
                     }
                     default:
                     {
                        MIDASERROR("Order = " + std::to_string(order) + " is not implementet");
                     }                      
                  }
               }
               else if(!Band)
               {
                  // At this point we just need to treat the opers keyword
                  if(local_key_map.find("OPERS")!=local_key_map.end()) 
                  {
                     string opers=local_key_map.find("OPERS")->second;
                     if(order==2 && SplitString(opers,",").size()<1) 
                     {
                        MIDASERROR("You must specify at least one operator for standard (non-banded) Lanczos Response.");
                     }
                     else if(order==2 && SplitString(opers,",").size()==1) 
                     {
                        string op_str=opers;
                        if(op_str.at(op_str.size()-1)==')' && op_str.at(I_0)=='(') {
                           string oper=op_str.substr(I_1,op_str.size()-2);
                           op_str="("+oper+","+oper+")";
                           local_key_map["OPERS"]=op_str;
                           LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                           arCalcDef.AddLanczosRspFunc(p);
                        }
                        else {
                           local_key_map["OPERS"]=op_str+op_str;
                           LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                           arCalcDef.AddLanczosRspFunc(p);
                        }
                     }
                     else if(order==2 && SplitString(opers,",").size()>2) {
                        // make all pairs of operators
                        string op_str=opers;
                        if(op_str.at(op_str.size()-1)==')' && op_str.at(I_0)=='(')
                           op_str=op_str.substr(I_1,op_str.size()-2);
                        vector<string> oper_vec=SplitString(op_str,",");
                        // make set of unique opers
                        set<string> op_set;
                        for(In i=I_0;i<oper_vec.size();i++)
                           op_set.insert(oper_vec[i]);
                        // loop through set to define LanRsp
                        vector<string> dummy;
                        set<vector<string> > my_set;
                        CreateAllOperPairs(my_set,op_set,dummy);
                        for(set<vector<string> >::iterator i=my_set.begin();i!=my_set.end();i++) {
                           vector<string> my_str=*i;
                           local_key_map["OPERS"]="("+my_str[0]+","+my_str[1]+")";
                           LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                           arCalcDef.AddLanczosRspFunc(p);
                        }
                     }
                     else if(order==2 && SplitString(opers,",").size()==2) {
                        // just add
                        LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                        arCalcDef.AddLanczosRspFunc(p);
                     }
                     else if(order==3) {
                        // No special treatments yet, just add the beast
                        if(SplitString(opers,",").size()!=3)
                           MIDASERROR(" ONLY quadratic response functions for three operators presently.");
                        string op_str=opers;
                        map<string,string>::iterator it=local_key_map.find("ALLPERMUT");
                        if(it!=local_key_map.end() && StringNoCaseCompare(it->second,"TRUE"))
                        {
                           if(op_str.at(op_str.size()-1)==')' && op_str.at(I_0)=='(')
                              op_str=op_str.substr(I_1,op_str.size()-2);
                           vector<string> oper_vec=SplitString(op_str,",");
                           set<string> perm_set=GenerateAllPermLanczosQR(oper_vec);
                           for(set<string>::iterator perm=perm_set.begin();perm!=perm_set.end();perm++) 
                           {
                              local_key_map["OPERS"]=*perm;
                              LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                              arCalcDef.AddLanczosRspFunc(p);
                           } 
                        }
                        else
                        {
                           LanczosRspFunc* p = LanczosRspFunc::Factory(order,local_key_map,Band,NHerm);
                           arCalcDef.AddLanczosRspFunc(p);
                        }
                     }
                  }
               }
               else
                  MIDASERROR("Unknown BAND keyword");
            }
            break;
         }
         case LANCZOSIR:
         {
            while (getline(Minp, s) && s.find("#") == string::npos)
               ParseLanczosSpect(s, arCalcDef, DO_LANCZOS_IR);
            AlreadyRead(already_read,s); 
            break;
         }
         case BANDLANCZOSIR:
         {
            while (getline(Minp, s) && s.find("#") == string::npos)
               ParseLanczosSpect(s, arCalcDef, DO_BANDLANCZOS_IR);
            AlreadyRead(already_read,s); 
            break;
         }
         case BANDLANCZOSRAMAN:
         {
            while (getline(Minp, s) && s.find("#") == string::npos)
               ParseLanczosSpect(s, arCalcDef, DO_BANDLANCZOS_RAMAN);
            AlreadyRead(already_read,s); 
            break;
         }
         case ALLCUBICLINEAR:
         {
            vector<Nb> frqs(3,C_0);
            string line;
            getline(Minp,line);
            if(line.find("#") == string::npos) {
               istringstream iss(line);
               iss >> frqs[0] >> frqs[1] >> frqs[2];
            }
            AddAllCubicLinear(arCalcDef,frqs);
            break;
         }
         case ALLCUBICQUADRATIC:
         {
            vector<Nb> frqs(3,C_0);
            string line;
            getline(Minp,line);
            if(line.find("#") == string::npos) {
               istringstream iss(line);
               iss >> frqs[0] >> frqs[1] >> frqs[2];
            }
            AddAllCubicQuadratic(arCalcDef,frqs);
            break;
         }
         //case SCALEPOLAROPERATORS:
         //{
         //   vector<Nb> scal_vec(3,C_0);
         //   string line;
         //   getline(Minp,line);
         //   if(line.find("#") == string::npos) {
         //      istringstream iss(line);
         //      iss >> scal_vec[0] >> scal_vec[1] >> scal_vec[2];
         //   }
         //   // scale polarizability operators by frequency factor
         //   for(In i=0;i<gOperatorDefs.GetNrOfOpers();i++) {
         //      OpInfo oi=OpDef::msOpInfo[gOperatorDefs[i].Name()];
         //      if(oi.GetType()<I_300 && oi.GetType()>I_200) {
         //         string name=oi.GetDescr();
         //         Nb ext_frq=oi.GetFrq1();
         //         // Z takes precedence over Y, which in turn is
         //         // treated before X
         //         if(name.find("Z")!=name.npos) {
         //            gOperatorDefs[i].Scale(C_1+ext_frq/scal_vec[2]);
         //         }
         //         else if(name.find("Y")!=name.npos) {
         //            gOperatorDefs[i].Scale(C_1+ext_frq/scal_vec[1]);
         //         }
         //         else {
         //            gOperatorDefs[i].Scale(C_1+ext_frq/scal_vec[0]);
         //         }
         //      } 
         //   }
         //   break;
         //}
         case SOSRSP:
         {
            arCalcDef.SetSosRsp(true);
            break;
         }
         case TENSORDECOMP:
         {
            arCalcDef.SetDoTensorRspBenchmarkDecomp(true);
            s = TensorDecompInput(Minp,arCalcDef,input_level+1);
            already_read = true;
            break;
         }
         case DECOMPEIG:
         {
            arCalcDef.SetRspDecompEig();
            s = TensorDecompInput(Minp,arCalcDef,input_level+1,DECOMPCALCTYPE::RSP_EIG);
            already_read = true;
            break;
         }
         case ITEQORTHOMAXITER:
         {
            arCalcDef.SetRspOrthoMax(midas::input::GetIn(Minp, s));
            break;
         }
         case ITEQTHRESHORTHODISCARD:
         {
            arCalcDef.SetRspThreshOrthoDiscard(midas::input::GetNb(Minp,s ));
            break;
         }
         case ITEQTHRESHORTHORETRY:
         {
            arCalcDef.SetRspThreshOrthoRetry(midas::input::GetNb(Minp, s));
            break;
         }
         case FORCECOMPLEXRSP:
         {
            arCalcDef.SetForceComplexRsp(true);
            break;
         }
         case ITEQSAVETODISC:
         {
            arCalcDef.SetItEqSaveToDisc(true);
            break;
         }
         case ITEQRESTARTVECTOR:
         {
            already_read = arCalcDef.ReadItEqRestartVector(Minp,s);
            break;
         }
         case ITEQSUBSPACEPRECONVECTOR:
         {
            already_read = arCalcDef.ReadItEqSubspacePreconVector(Minp,s);
            break;
         }
         case LAMBDATHRESHOLD:
         {
            already_read = arCalcDef.ReadLambdaThreshold(Minp,s);
            break;
         }
         case ITEQSOLTHR:
         {
            already_read = arCalcDef.ReadRspItEqSolThr(Minp,s);
            break;
         }
         case ITEQPRECONRESIDTHR:
         {
            already_read = arCalcDef.ReadRspPreconItEqResidThr(Minp,s);
            break;
         }
         case ITEQPRECONSOLTHR:
         {
            already_read = arCalcDef.ReadRspPreconItEqSolThr(Minp,s);
            break;
         }
         case ITEQPRECONMAXITER:
         {
            already_read = arCalcDef.ReadRspPreconItEqMaxIter(Minp,s);
            break;
         }
         case COMPLEXRSPOLSEN:
         {
            arCalcDef.SetComplexRspOlsen(true);
            break;
         }
         case COMPLEXIMPROVEDPRECOND:
         {
            already_read = arCalcDef.ReadRspComplexImprovedPrecond(Minp,s);
            break;
         }
         case COMPLEXPRECONDUNMUTE:
         {
            arCalcDef.SetRspComplexImprovedPrecondMute(false);
            break;
         }
         case COMPLEXIMPROVEDPRECONDEXPLICIT:
         {
            arCalcDef.SetRspComplexImprovedPrecondExplicit(true);
            arCalcDef.SetRspComplexSolverType(COMPLEXSOLVER::IMPROVED);
            break;
         }
         case DENSITYANALYSIS:
         {
            arCalcDef.SetDoDensityMatrixAnalysis(true);
            break;
         }
         case ITNAMO:
         {
            arCalcDef.SetItNaMo(true);
            getline(Minp,s);                                         // Get new line with input
            arCalcDef.SetItNaMoMaxIter(midas::util::FromString<In>(s));
            getline(Minp,s);                                         // Get new line with input
            arCalcDef.SetItNaMoThr(midas::util::FromString<Nb>(s));
            break;
         }
         case NOSYMDENSNAMO:
         {
            arCalcDef.SetSymDensForNaMo(false);
            break;

         }
         case ITEQSUBSPACEPRECONFULL:
         {
            arCalcDef.SetItEqSubspacePreconFull(true);
            arCalcDef.SetRspComplexSolverType(COMPLEXSOLVER::SUBSPACE);
            break;
         }
         case VCCTENSORTYPE:
         {
            already_read = arCalcDef.ReadVccTensorType(Minp, s);
            break;
         }
         case ITEQRELCONV:
         {
            already_read = arCalcDef.ReadRspRelConv(Minp,s);
            break;
         }
         case ITEQCHECKLINEARDEPENDENCEOFSPACE:
         {
            already_read = arCalcDef.ReadRspItEqCheckLinearDependenceOfSpace(Minp,s);
            break;
         }
         case USENEWCONTRACTIONS:
         {
            already_read = arCalcDef.ReadRspUseNewContractions(Minp,s);
            break;
         }
         case RSPTENSORDECOMPINFO:
         {
            already_read = arCalcDef.ReadVccRspDecompInfoSet(Minp, s);
            break;
         }
         case RSPTRFTENSORDECOMPINFO:
         {
            already_read = arCalcDef.ReadVccRspTrfDecompInfoSet(Minp, s);
            break;
         }
         case BALANCETENSORSAFTERPRECON:
         {
            arCalcDef.SetTensorVccRspEigBalanceAfterPrecon(true);
            break;
         }
         case RECOMPTENSORSAFTERPRECON:
         {
            arCalcDef.SetTensorVccRspEigRecompAfterPrecon(true);
            break;
         }
         case ABSPRECON:
         {
            arCalcDef.SetTensorVccRspEigAbsPrecon(true);
            break;
         }
         case NOOLSEN:
         {
            arCalcDef.SetOlsen(false);
            break;
         }
         case OLSENDECOMPRELTHRESH:
         {
            auto thresh = midas::input::GetNb(Minp, s);
            arCalcDef.SetOlsenDecompRelThresh(thresh);
            break;
         }
         case LINEQMINIMUMRESIDUAL:
         {
            arCalcDef.SetRspLinEqMinimumResidual(true);
            break;
         }
         case NOGENDOWNCTRRECOMP:
         {
            arCalcDef.SetGenDownCtrRecompressResult(false);
            break;
         }
         case CHECKTENSORVCCTRANSJAC:
         {
            arCalcDef.SetCheckTensorVccTransJac(true);
            break;
         }
         case TENSORVCCJACUSEDEBUGVECTOR:
         {
            arCalcDef.SetTensorVccJacUseDebugVector(true);
            break;
         }
         case ADIAGVCCREFENERGY:
         {
            arCalcDef.SetRspUseVccRefEnergy(true);
            break;
         }
         case TENSORRSPSOLVERCONVERTUNITS:
         {
            arCalcDef.SetTensorRspSolverConvertUnits(true);
            break;
         }
         case TENSORRSPPREDIAG:
         {
            arCalcDef.SetRspPreDiag(true);
            break;
         }
         case RSPTRANSFORMERALLOWEDRANK:
         {
            arCalcDef.SetRspTransformerAllowedRank(midas::input::GetIn(Minp, s));
            break;
         }
         case RSPTENSORPRODUCTSCREENING:
         {
            std::getline(Minp, s);
            auto screens = midas::util::VectorFromString<Nb>(s);
            auto num_args = screens.size();
            if (  num_args == 1  )
            {
               auto tps = screens.at(0);
               arCalcDef.SetRspTensorProductScreening(tps, tps);
            }
            else if  (  num_args == 2  )
            {
               auto tps = screens.at(0);
               auto ts = screens.at(1);
               arCalcDef.SetRspTensorProductScreening(tps, ts);
            }
            else
            {
               MIDASERROR("RspInput: Wrong number of arguments for RspTensorProductScreening!");
            }

            break;
         }
         case RSPTENSORPRODUCTLOWRANKLIMIT:
         {
            arCalcDef.SetRspTensorProductLowRankLimit(midas::input::GetIn(Minp, s));
            break;
         }
         case TENSORRSPORTHOSCREENING:
         {
            arCalcDef.SetTensorRspOrthoScreening(midas::input::GetNb(Minp, s));
            break;
         }
         case RANKANALYSIS:
         {
            arCalcDef.SetRspRankAnalysis(true);
            break;
         }
         case FULLRANKANALYSIS:
         {
            arCalcDef.SetFullRspRankAnalysis(true);
            break;
         }
         case NOREITERATETENSORRSPSOLVER:
         {
            arCalcDef.SetReiterateTensorRspSolver(false);
            break;
         }
         case REITERATETENSORRSPSOLVER:
         {
            arCalcDef.SetReiterateTensorRspSolver(true);
            break;
         }
         case NORECOMPRESSCPVCCEIGVEC:
         {
            arCalcDef.SetRecompressCpvccEigVecs(false);
            break;
         }
         case ERROR: //FALLTHROUGH
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a Response level 4 Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check for tensor decomp info
   if (  arCalcDef.GetVccRspDecompInfo().empty() != arCalcDef.GetVccRspTrfDecompInfo().empty()
      )
   {
      MIDASERROR("If using the TensorDataCont eig solver, you must specify TensorDecompInfo for both the solver and the transformer!");
   }

   // Check debug input
   if (  arCalcDef.TensorVccJacUseDebugVector()
      && !arCalcDef.CheckTensorVccTransJac()
      )
   {
      MIDASERROR("To use TensorVccJacUseDebugVector, CheckTensorVccTransJac must be set!");
   }
   
   if (target && (all_first || all_fund||fund||first))
   {
      Mout << " Target states set by TargetVectors "<< endl;
      Mout << " At the same time ";
      if (all_fund) Mout << " All Rsp Fundamentals ";
      if (all_first) Mout << " All Rsp first overtones ";
      if (fund) Mout << " Some Rsp Fundamentals ";
      if (first) Mout << " Some Rsp first overtones ";
      Mout << " was set " << endl;
      MIDASERROR("Target set together with conflicting keyword ");
   }

   //Mout << " returning s = " << s << endl;
   return s;
}

//vector<RspFunc> MakeExptValues() {
//   // return a vector <Op>
//   vector<RspFunc> result;
//   for(In i=I_0;i<gOperatorDefs.GetNrOfOpers();i++) {
//      RspFunc rf(I_1);
//      rf.SetOp(gOperatorDefs[i].Name());
//      result.push_back(rf);
//   }
//   return result;
//}

/**
 *
 * */
set<RspFunc> MakePVLinearResponseFunctions(vector<string>& arOpers,vector<Nb> freqs,In& arOpIndex,In& arStartPos) {
   RspFunc rf(I_2);
   set<RspFunc> result;
   if(arStartPos>(arOpers.size()-I_1) || arOpIndex>(arOpers.size()-I_1) || arStartPos > arOpIndex) {
      return result;
   }
   // 1) Make diagonal response function (i.e. xx,yy...)
   for(In i=I_0;i<freqs.size();i++) {
      rf.SetOp(arOpers[arOpIndex],I_0);
      rf.SetOp(arOpers[arOpIndex],I_1);
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(C_M_1*freqs[i],I_0);
      result.insert(rf);
   }
   // 2) Make rf with this oper, and the argument one
   for(In i=I_0;i<freqs.size();i++) {
      rf.SetOp(arOpers[arOpIndex],I_0);
      rf.SetOp(arOpers[arStartPos],I_1);
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(C_M_1*freqs[i],I_0);
      result.insert(rf);
   }
   // 3) Call This function again with this operator
   In startPos=arStartPos+I_1;
   set<RspFunc> tmp_set=MakePVLinearResponseFunctions(arOpers,freqs,arOpIndex,startPos);
   for(set<RspFunc>::const_iterator i=tmp_set.begin();i!=tmp_set.end();i++)
      result.insert(*i);
   // 4) Call this function again with the argument oper
   tmp_set.clear();
   In opIndex=arOpIndex+I_1;
   tmp_set=MakePVLinearResponseFunctions(arOpers,freqs,opIndex,arStartPos);
   for(set<RspFunc>::const_iterator i=tmp_set.begin();i!=tmp_set.end();i++)
      result.insert(*i);
   return result;
}

/**
 *
 **/
set<RspFunc> MakePVQuadraticResponseFunctions(vector<string>&arOpers,vector<Nb> freqs,In& arOpIndex,In& arStartPos) {
   RspFunc rf(I_3);
   set<RspFunc> result;
   if(arStartPos>(arOpers.size()-I_1) || arOpIndex>(arOpers.size()-I_1)) {
      return result;
   }
   // 1) Make zii RF.
   for(In i=I_0;i<freqs.size();i++) {
      rf.SetOp(arOpers[arOpIndex],I_0);
      rf.SetOp(arOpers[arStartPos],I_1);
      rf.SetOp(arOpers[arStartPos],I_2);
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(freqs[i],I_2);
      rf.SetFrq(C_M_1*C_2*freqs[i],I_0);
      result.insert(rf);
      // also Kerr effect
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(-freqs[i],I_2);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
      // also
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(C_0,I_0);
      rf.SetFrq(-freqs[i],I_0);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
   }
   // 2) Make izi RF.
   for(In i=I_0;i<freqs.size();i++) {
      rf.SetOp(arOpers[arStartPos],I_0);
      rf.SetOp(arOpers[arOpIndex],I_1);
      rf.SetOp(arOpers[arStartPos],I_0);
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(freqs[i],I_2);
      rf.SetFrq(C_M_1*C_2*freqs[i],I_0);
      result.insert(rf);
      // also Kerr effect
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(-freqs[i],I_2);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
      // also
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(C_0,I_0);
      rf.SetFrq(-freqs[i],I_0);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
   }
   // 2) Make iiz RF.
   for(In i=I_0;i<freqs.size();i++) {
      rf.SetOp(arOpers[arStartPos],I_0);
      rf.SetOp(arOpers[arStartPos],I_1);
      rf.SetOp(arOpers[arOpIndex],I_2);
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(freqs[i],I_2);
      rf.SetFrq(C_M_1*C_2*freqs[i],I_0);
      result.insert(rf);
      // also Kerr effect
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(-freqs[i],I_2);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
      // also
      rf.SetFrq(freqs[i],I_1);
      rf.SetFrq(C_0,I_0);
      rf.SetFrq(-freqs[i],I_0);
      rf.SetFrq(C_0,I_0);
      result.insert(rf);
   }
   // 3) Call This function again with this operator
   In startPos=arStartPos+I_1;
   set<RspFunc> tmp_set=MakePVQuadraticResponseFunctions(arOpers,freqs,arOpIndex,startPos);
   for(set<RspFunc>::const_iterator i=tmp_set.begin();i!=tmp_set.end();i++)
      result.insert(*i);
   return result;
}

/**
 *
 **/
set<RspFunc> MakePVMixedResponseFunctions(vector<string>& polar_opers,
                 vector<string>& asym_opers,
                 vector<string>& dipole_opers,
                 vector<Nb> freqs) {
   // Empty for now
   set<RspFunc> result;
   RspFunc rf(I_2);
   // Loop over symmetric linear response functions
   for(In i=0;i<polar_opers.size();i++) {
      OpInfo oi=OpDef::msOpInfo[polar_opers[i]];
      // Loop over dipole opers
      for(In j=0;j<dipole_opers.size();j++) {
         rf.SetOp(dipole_opers[j],I_0);
         rf.SetOp(polar_opers[i],I_1);
         for(In k=0;k<freqs.size();k++) {
            if(freqs[k]!=oi.GetFrq(0) && freqs[k]!=oi.GetFrq(0)/C_2)
               continue;
            rf.SetFrq(freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(-freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(C_2*freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(-C_2*freqs[k],I_1);
            result.insert(rf);
         }
      }
   }
   rf.SetDoAsymmetric(true);
   for(In i=0;i<asym_opers.size();i++) {
      OpInfo oi=OpDef::msOpInfo[asym_opers[i]];
      // Loop over dipole opers
      for(In j=0;j<dipole_opers.size();j++) {
         rf.SetOp(dipole_opers[j],I_0);
         rf.SetOp(asym_opers[i],I_1);
         for(In k=0;k<freqs.size();k++) {
            if(freqs[k]!=oi.GetFrq(0) && freqs[k]!=oi.GetFrq(0)/C_2)
               continue;
            rf.SetFrq(freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(-freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(C_2*freqs[k],I_1);
            result.insert(rf);
            rf.SetFrq(-C_2*freqs[k],I_1);
            result.insert(rf);
         }
      }
   }
   return result;
}

void CreateAllOperPairs(set<vector<string> >& aRes,set<string>& aS, vector<string>& aV) {
   if(aS.size()==0) return;
   for(set<string>::iterator i=aS.begin();i!=aS.end();i++) {
      string op=*i;
      set<string> sub_set=aS;
      if(i!=aS.begin())
         sub_set.erase(sub_set.begin(),sub_set.find(*i));
      if(aV.size()==0) {
         Mout << "Size = 0" << endl;
         // 1) send the remaining set off with an empty vector
         set<string> set_copy=sub_set;
         set_copy.erase(set_copy.begin());
         Mout << "AT 1" << endl;
         CreateAllOperPairs(aRes,set_copy,aV);
         vector<string> vec_copy;
         vec_copy.push_back(op);
         Mout << "AT 2" << endl;
         CreateAllOperPairs(aRes,sub_set,vec_copy);
      }
      else if(aV.size()==1) {
         Mout << "Size = 1" << endl;
         set<string> set_copy=sub_set;
         set_copy.erase(set_copy.begin());
         Mout << "AT 3" << endl;
         CreateAllOperPairs(aRes,set_copy,aV);
         vector<string> vec_copy=aV;
         vec_copy.push_back(op);
         aRes.insert(vec_copy);
      }
   }
   return;
}

set<string> GenerateAllPermLanczosQR(vector<string>& aV)
{
   set<string> result;
   do
   {
      string s="("+aV[0]+","+aV[0]+","+aV[0]+")";
      Mout << "S = " << s << endl;
      result.insert(s);
      vector<string> my_vec;
      my_vec.push_back(aV[0]);
      my_vec.push_back(aV[0]);
      my_vec.push_back(aV[1]);
      sort(my_vec.begin(),my_vec.end());
      do
      {
         s="("+my_vec[0]+","+my_vec[1]+","+my_vec[2]+")";
         Mout << "S = " << s << endl;
         result.insert(s);
      } while(next_permutation(my_vec.begin(), my_vec.end()));
      s="("+aV[0]+","+aV[1]+","+aV[2]+")";
      Mout << "S = " << s << endl;
      result.insert(s);
   } while(next_permutation(aV.begin(), aV.end()));

   return result;
}

void AddAllCubicLinear(RspCalcDef& arCalcDef,vector<Nb> aV)
{
   Nb sum_frq=aV[0]+aV[1]+aV[2];
   vector<string> basic;
   basic.push_back("X");
   basic.push_back("Y");
   basic.push_back("Z");
   set<RspFunc> extra;
   for(In i=0;i<3;i++) {
      for(In j=0;j<3;j++) {
         for(In k=0;k<3;k++) {
            for(In l=0;l<3;l++) {
               // check for 3 different ops
               set<string> s_set;
               s_set.insert(basic[i]);
               s_set.insert(basic[j]);
               s_set.insert(basic[k]);
               s_set.insert(basic[l]);
               if(s_set.size()>I_2)
                  continue;
               vector<pair<string,Nb> > op_frq;
               op_frq.push_back(make_pair(basic[i],-sum_frq));
               op_frq.push_back(make_pair(basic[j],aV[0]));
               op_frq.push_back(make_pair(basic[k],aV[1]));
               op_frq.push_back(make_pair(basic[l],aV[2]));
               sort(op_frq.begin(),op_frq.end());
               // create all ["\alpha^2"]
               AddAlphaSquared(extra,op_frq);
               // create all ["\mu\beta"]
               sort(op_frq.begin(),op_frq.end());
               AddMuBeta(extra,op_frq);
            }
         }
      }
   }
   // now add the sets to arCalcDef
   for(set<RspFunc>::iterator it=extra.begin();it!=extra.end();it++) {
      RspFunc rsp=*it;
      arCalcDef.AddRspFunc(rsp);
   }
}

void AddAlphaSquared(set<RspFunc>& arRspSet,vector<pair<string,Nb> >& aOpFrq)
{
   do {
      RspFunc rf(I_2);
      Nb frq=aOpFrq[0].second;
      string name;
      if(frq<C_0) {
         name=aOpFrq[1].first+aOpFrq[0].first+"-POL";
         frq=-frq;
      }
      else {
         name=aOpFrq[1].first+aOpFrq[0].first+"-POL";
      }
      string info_string="type="+name+" frq="+StringForNb(frq);
      OpInfo oi;
      ParseSetInfoLine(info_string,oi);
      // find this in gOperators
      for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
         OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
         if(oi==oi_try)
            rf.SetOp(name+"_"+StringForNb(frq),I_0);
      }
      frq=aOpFrq[3].second;
      if(frq<C_0) {
         name=aOpFrq[3].first+aOpFrq[2].first+"-POL";
         frq=-frq;
      }
      else {
         name=aOpFrq[2].first+aOpFrq[3].first+"-POL";
      }
      info_string="type="+name+" frq="+StringForNb(frq);
      ParseSetInfoLine(info_string,oi);
      // find this in gOperators
      for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
         OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
         if(oi==oi_try)
            rf.SetOp(name+"_"+StringForNb(frq),I_1);
      }
      rf.SetFrq(aOpFrq[2].second+aOpFrq[3].second,I_1);
      arRspSet.insert(rf);
      Mout << "AlphaSquared added: " << rf << endl;
      rf.SetDoAsymmetric(true);
      arRspSet.insert(rf);
      Mout << "AlphaSquared added: " << rf << endl;
   } while(next_permutation(aOpFrq.begin(),aOpFrq.end()));
}

void AddMuBeta(set<RspFunc>& arRspSet,vector<pair<string,Nb> >& aOpFrq)
{
   do {
      // test all permutations of beta operator
      vector<pair<string,Nb> > beta_help;
      beta_help.push_back(aOpFrq[1]);
      beta_help.push_back(aOpFrq[2]);
      beta_help.push_back(aOpFrq[3]);
      sort(beta_help.begin(),beta_help.end());
      do {
         RspFunc rf(I_2);
         string name;
         name=aOpFrq[0].first;
         string info_string="type="+name+"_DIPOLE";
         OpInfo oi;
         ParseSetInfoLine(info_string,oi);
         // find this in gOperators
         for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
            OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
            if(oi==oi_try)
               rf.SetOp(name+"_DIPOLE",I_0);
         }
         name=beta_help[0].first+beta_help[1].first+beta_help[2].first+"_POL";
         // take care of neg. frq.
         Nb frq1=beta_help[1].second;
         Nb frq2=beta_help[2].second;
         Nb frq_sum=-(frq1+frq2);
         if(frq1<C_0) {
            string tmp_name=name.substr(I_1,I_1)+name.substr(I_0,I_1)+name.substr(I_2,I_1);
            name=tmp_name+"_POL";
            frq1=frq_sum;
            frq_sum=frq1;
         }
         else if(frq2<C_0) {
            string tmp_name=name.substr(I_2,I_1)+name.substr(I_1,I_1)+name.substr(I_0,I_1);
            name=tmp_name+"_POL";
            frq2=frq_sum;
            frq_sum=frq2;
         }
         info_string="type="+name+" frq1="+StringForNb(frq1);
         info_string+=" frq2="+StringForNb(frq2);
         ParseSetInfoLine(info_string,oi);
         Mout << "Came up with name: " << name << endl;
         Mout << oi << endl;
         // find this in gOperators
         bool success=false;
         for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
            OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
            if(oi==oi_try) {
               rf.SetOp(name+"_"+StringForNb(frq1)+"_"+StringForNb(frq2),I_1);
               success=true;
               break;
            }
         }
         if(!success)
            continue;
         //
         Nb frq=aOpFrq[0].second;
         if(frq<C_0) {
            string tmp_op=rf.GetRspOp(I_1);
            rf.SetOp(rf.GetRspOp(I_0),I_1);
            rf.SetOp(tmp_op,I_0);
            rf.SetFrq(-frq,I_1);
         }
         else {
            rf.SetFrq(frq,I_1);
         }
         arRspSet.insert(rf);
         RspFunc rf2(I_2);
         rf2.SetOp(rf.GetRspOp(I_0),I_1);
         rf2.SetOp(rf.GetRspOp(I_1),I_0);
         rf2.SetFrq(rf.GetRspFrq(I_1),I_1);
         arRspSet.insert(rf2);
         Mout << "AllMuBeta added: " << rf << endl;
         Mout << "AND AllMuBeta added: " << rf2 << endl;
      } while(next_permutation(beta_help.begin(),beta_help.end()));
   } while(next_permutation(aOpFrq.begin(),aOpFrq.end()));
}

void AddAllCubicQuadratic(RspCalcDef& arCalcDef,vector<Nb> aV)
{
   Nb sum_frq=aV[0]+aV[1]+aV[2];
   vector<string> basic;
   basic.push_back("X");
   basic.push_back("Y");
   basic.push_back("Z");
   set<RspFunc> extra;
   for(In i=0;i<3;i++) {
      for(In j=0;j<3;j++) {
         for(In k=0;k<3;k++) {
            for(In l=0;l<3;l++) {
               // check for 3 different ops
               set<string> s_set;
               s_set.insert(basic[i]);
               s_set.insert(basic[j]);
               s_set.insert(basic[k]);
               s_set.insert(basic[l]);
               if(s_set.size()>I_2)
                  continue;
               vector<pair<string,Nb> > op_frq;
               op_frq.push_back(make_pair(basic[i],-sum_frq));
               op_frq.push_back(make_pair(basic[j],aV[0]));
               op_frq.push_back(make_pair(basic[k],aV[1]));
               op_frq.push_back(make_pair(basic[l],aV[2]));
               sort(op_frq.begin(),op_frq.end());
               // create all ["\mu^2\alpha"]
               AddMuSquaredAlpha(extra,op_frq);
            }
         }
      }
   }
   // now add the sets to arCalcDef
   for(set<RspFunc>::iterator it=extra.begin();it!=extra.end();it++) {
      RspFunc rsp=*it;
      arCalcDef.AddRspFunc(rsp);
   }
}

void AddMuSquaredAlpha(set<RspFunc>& arRspSet,vector<pair<string,Nb> >& aOpFrq)
{

   vector<pair<In,pair<string,Nb> > > for_sort;
   for(In i=0;i<aOpFrq.size();i++)
   {
      for_sort.push_back(make_pair(i,aOpFrq[i]));
   }

   do {
      RspFunc rf(I_3);
      Nb frq=for_sort[2].second.second;
      string name;
      if(frq<C_0) {
         name=for_sort[3].second.first+for_sort[2].second.first+"-POL";
         frq=-frq;
      }
      else {
         name=for_sort[2].second.first+for_sort[3].second.first+"-POL";
      }
      string info_string="type="+name+" frq="+StringForNb(frq);
      OpInfo oi;
      ParseSetInfoLine(info_string,oi);
      // find this in gOperators
      for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
         OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
         if(oi==oi_try)
            rf.SetOp(name+"_"+StringForNb(frq),I_2);
      }
      // and now the two dipole operators
      info_string="type="+for_sort[0].second.first+"_DIPOLE";
      ParseSetInfoLine(info_string,oi);
      // find this in gOperators
      for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
         OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
         if(oi==oi_try)
            rf.SetOp(for_sort[0].second.first+"_DIPOLE",I_0);
      }
      info_string="type="+for_sort[1].second.first+"_DIPOLE";
      ParseSetInfoLine(info_string,oi);
      // find this in gOperators
      for(In m=0;m<gOperatorDefs.GetNrOfOpers();m++) {
         OpInfo oi_try=OpDef::msOpInfo[gOperatorDefs[m].Name()];
         if(oi==oi_try)
            rf.SetOp(for_sort[1].second.first+"_DIPOLE",I_1);
      }
      rf.SetFrq(for_sort[1].second.second,I_1);
      rf.SetFrq(for_sort[2].second.second+for_sort[3].second.second,I_2);
      arRspSet.insert(rf);
      Mout << "MuSquaredAlpha added: " << rf << endl;
   } while(next_permutation(for_sort.begin(),for_sort.end()));
}
