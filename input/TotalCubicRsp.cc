/**
************************************************************************
* 
* @file                TotalCubicRsp.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TotalCubicRsp datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <exception>
#include <list>

#include "inc_gen/math_link.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/TotalCubicRsp.h"
#include "input/TotalResponseDrv.h"
#include "input/TotalResponseContribution.h"
#include "util/conversions/FromString.h"
#include "input/OpInfo.h"

using std::string;
using std::vector;
using std::map;
using std::make_pair;
using std::exception;
using std::list;

//#define DBG(ARG) ARG
#define DBG(ARG) void(0);

static In PRECISION=8;


/**
* Constructor, no action yet...
* */
TotalCubicRsp::TotalCubicRsp(const std::map<string,string>& aM) : TotalResponseDrv(aM)
{
   Mout << "Entering total Cubic rsp..." << endl;
   CheckKeys(aM);
   InitFrq(aM);
   InitOrientation(aM);
   InitExperimental(aM);
   InitContributions(aM);
   InitTypes(aM);
   InitRules(aM);
}

void TotalCubicRsp::CheckKeys(const std::map<string,string>& aM)
{
   set<string> all_keys;
   all_keys.insert("ORDER");
   all_keys.insert("PVC");
   all_keys.insert("PVQ");
   all_keys.insert("PVL");
   all_keys.insert("ZPVC");
   all_keys.insert("FRQ");
   all_keys.insert("CONTRIBUTIONS");
   all_keys.insert("SET_VALUE");
   all_keys.insert("EXPERIMENTAL");
   all_keys.insert("STD_ORIENTATION");
   // now perform check
   map<string,string>::const_iterator map_it;
   set<string>::iterator keys_it;
   bool found_error=false;
   for(map_it=aM.begin();map_it!=aM.end();map_it++) {
      keys_it=all_keys.find(map_it->first);
      if(keys_it==all_keys.end()) {
         found_error=true;
         break;
      }
   }
   if(!found_error)
      return;
   Mout << "Unknown keyword detected: " << map_it->first << endl;
   Mout << "Possible values are:" << endl;
   for(keys_it=all_keys.begin();keys_it!=all_keys.end();keys_it++)
      Mout << *keys_it << endl;
   ConstructionError("Unknown keyword detected, see above.",aM);
}

void TotalCubicRsp::InitOrientation(const map<string,string>& aM)
{
   mStd=false;
   map<string,string>::const_iterator it = aM.find("STD_ORIENTATION");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE")) {
      mStd=true;
   }
}

void TotalCubicRsp::InitExperimental(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("EXPERIMENTAL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE"))
   {
      // experimental quantities
      string iso;
      if(!mStd) {
         iso="(1/15)*(3*XXXX+3*YYYY+3*ZZZZ+YYXX+ZZXX+XXYY";
         iso+="+XXZZ+YYZZ+YXXY+ZXXZ+XYYX+ZYYZ+XZZX+YZZY";
         iso+="+YXYX+ZXZX+XYXY+ZYZY+XZXZ+YZYZ)";
      }
      else {
         iso="(1/5)*(XXXX+YYYY+ZZZZ+2*XXYY+2*XXZZ+2*YYZZ)";
      }
      mTotalRspString.push_back(iso);
   }
}

void TotalCubicRsp::InitContributions(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("CONTRIBUTIONS");
   if(it==aM.end())
      return;
   // look for contributions.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Contributions must be enclosed in (...).", aM);
   string contrib_string = s.substr(1,s.size()-2);
   vector<string> contribs = SplitString(contrib_string, ",");
   for(In i=0;i<contribs.size();i++) {
      if(contribs[i].find_first_of("XYZ")==contribs[i].npos)
         continue;
      mContribSet.insert(contribs[i]);
   }
}

void TotalCubicRsp::InitTypes(const map<string,string>& aM)
{
   mPvl=false;
   mPvq=false;
   mPvc=true;
   mZpvc=true;
   map<string,string>::const_iterator it = aM.find("PVL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE")) {
      mPvl=true;
   }
   it = aM.find("PVC");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mPvc=false;
   }
   it = aM.find("PVQ");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE")) {
      mPvq=true;
   }
   it = aM.find("ZPVA");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mZpvc=false;
   }
   if (!mPvl && !mZpvc) {
      string s="One or more of PVC, PVQ, PVL, or ZPVA must be set.\n"; 
      s+="Default: PVC and ZPVA: true\n";
      s+="         PVL and PVQ : false\n";
      ConstructionError(s,aM);
   }
}

void TotalCubicRsp::InitFrq(const map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("FRQ");
   if (it == aM.end())
      ConstructionError(" No frequency specified.", aM);
   // frequencies must be enclosed in par, and separated by ",",
   // as: frq=(0.0,0.0656)
   string s=it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Frequencies must be enclosed in (...).", aM);
   s=s.substr(1,s.size()-2);
   vector<string> s_vec=SplitString(s,",");
   if(s_vec.size()!=3)
      ConstructionError(" Quadratic response requires two frequencies.", aM);
   
   mFrq1 = midas::util::FromString<Nb>(s_vec[0]);
   mFrq2 = midas::util::FromString<Nb>(s_vec[1]);
   mFrq3 = midas::util::FromString<Nb>(s_vec[2]);
}

void TotalCubicRsp::InitRules(const map<string,string>& aM)
{
   if(!mIso)
      return;
   map<string,string>::const_iterator it = aM.find("SET_VALUE");
   if (it == aM.end())
      return;
   // look for rules.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Values must be enclosed in (...).", aM);
   string rule_string = s.substr(1,s.size()-2);
   vector<string> rule_vec = SplitString(rule_string, ",");
   for(In i=0;i<rule_vec.size();i++) {
      vector<string> equal_contribs=SplitString(rule_vec[i],"=");
      // for each rule, check for initializing values,
      // i.e. xx=yy=0.0
      bool initial_val=false;
      Nb val = 0.0;
      for(In j=0;j<equal_contribs.size();j++) {
         if(auto optional_val = midas::util::FromStringOptional<Nb>(equal_contribs[j]))
         {
            val = *optional_val;
            Mout << "Found initializing value..." << endl;
            initial_val=true;
            break;
         }
      }
      string s_base="type=";
      if(initial_val) {
         for(In j=0;j<equal_contribs.size()-1;j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq1);
            Contribution pv_contrib(infos,frq_vec);
            pv_contrib.SetHasBeenEval(true);
            pv_contrib.SetValue(val);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq1);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            zpv_contrib.SetHasBeenEval(true);
            zpv_contrib.SetValue(val);
            mDefault.insert(pv_contrib);
            mDefault.insert(zpv_contrib);
         }
      }
      else {
         vector<Contribution> equivalent;
         for(In j=0;j<equal_contribs.size();j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq1);
            Contribution pv_contrib(infos,frq_vec);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq1);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            equivalent.push_back(pv_contrib);
            equivalent.push_back(zpv_contrib);
         }
         mEquivalent.push_back(equivalent);
      }
   }
}

void TotalCubicRsp::ConstructionError(const string aS,
                                    const map<string,string>& aM)
{
   Mout << " TotalCubicRsp construction error:" << endl
        << aS << endl << endl
        << " Constructor parameters:" << endl;

   for (map<string,string>::const_iterator it=aM.begin(); it!=aM.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;

   MIDASERROR("");
}

void TotalCubicRsp::ConstructIntermediateSet()
{
   // given mPvc, mPvq, mPvl, mZpvc, mTotalRspString, and mContribSet
   // construct the intermediate set of contributions
   set<string> c_set;
   vector<Nb> frq_vec;
   for(In i=0;i<mTotalRspString.size();i++) {
      c_set=ConvertStringToFunctions(mTotalRspString[i]);
      for(set<string>::iterator it=c_set.begin();it!=c_set.end();it++) {
         mContribSet.insert(*it);
      }
   }
   set<Contribution> help_set;
   // now the mContribSet
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++) {
      string c_s=*it;
      OpInfo oi;
      string info_line;
      // first zpv
      info_line="type="+c_s+"_POL frq1="+StringForNb(mFrq1);
      info_line+=" frq2="+StringForNb(mFrq2);
      info_line+=" frq3="+StringForNb(mFrq3);
      ParseSetInfoLine(info_line,oi);
      vector<OpInfo> oi_vec;
      oi_vec.push_back(oi);
      frq_vec.clear();
      Contribution c(oi_vec,frq_vec);
      help_set.insert(c);
      // and then pure vib
      oi_vec.clear();
      info_line="type="+c_s.substr(I_0,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_1,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_2,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_3,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(mFrq1);
      frq_vec.push_back(mFrq2);
      frq_vec.push_back(mFrq3);
      Contribution c_a(oi_vec,frq_vec);
      help_set.insert(c_a);
      // also add Pure vib linear - more troublesome...
      AddPureVibLinear(help_set,c_s); 
      AddPureVibQuadratic(help_set,c_s); 
   }
   // values are not set, we do this by searching the basic set
   for(set<Contribution>::iterator it=help_set.begin();it!=help_set.end();it++) {
      set<Contribution>::iterator basic_it=mBasicSet.find(*it);
      Contribution c(*it);
      if(basic_it!=mBasicSet.end())
         c.SetValue(basic_it->GetValue());
      mIntermediateSet.insert(c);
   }
   // now modify this according to mPvl and mZpva
   if(mPvc && mPvq && mPvl && mZpvc) {
      if(gIoLevel>I_5 || gDebug) {
         Mout << "At end of day, intermediate set is:" << endl;
         for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
            Mout << *it << endl;
         Mout << "Done in construct interm. set." << endl;
         return;
      }
   }
   if(!mZpvc) {
      // remove all vib rsp order=1
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_1)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvl) {
      // remove all vib rsp order=2
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_2)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvq) {
      // remove all vib rsp order=3
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_3)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvc) {
      // remove all vib rsp order=4
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_4)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(gIoLevel>I_5 || gDebug) {
      Mout << "At end of day, intermediate set is:" << endl;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
         Mout << *it << endl;
      Mout << "Done in construct interm. set." << endl;
   }
}

void TotalCubicRsp::AddPureVibQuadratic(set<Contribution>& aSet, const string& aS)
{
   // here we add terms for PVQ
   // this means permutation!
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),-(mFrq1+mFrq2+mFrq3)));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   perm.push_back(make_pair(aS.substr(I_3,I_1),mFrq3));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      // first mu
      string op=perm[0].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      op=perm[1].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // then alpha
      op=perm[2].first+perm[3].first+"_POL";
      info_line="type="+op+" frq="+StringForNb(perm[3].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // now frq vec
      frq_vec.push_back(perm[1].second);
      frq_vec.push_back(perm[2].second+perm[3].second);
      Contribution c(oi_vec,frq_vec);
      aSet.insert(c);
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   Mout << "Add pure vib. quad. added: " << count << " response terms." << endl;
}

void TotalCubicRsp::AddPureVibLinear(set<Contribution>& aSet, const string& aS)
{
   // alpha^2:
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),-(mFrq1+mFrq2+mFrq3)));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   perm.push_back(make_pair(aS.substr(I_3,I_1),mFrq3));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      string op=perm[0].first+perm[1].first+"_POL";
      info_line="type="+op+" frq="+StringForNb(perm[0].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      op=perm[2].first+perm[3].first+"_POL";
      info_line="type="+op+" frq="+StringForNb(perm[3].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(perm[2].second+perm[3].second);
      Contribution c(oi_vec,frq_vec);
      aSet.insert(c);
      // that was << a^e+ ; a^e+ >>^v, now find
      //          << a^e+ ; a^e- >>^v
      if(mFrq1==mFrq2 && mFrq1==mFrq3 && mFrq1==C_0) {
         count++;
         continue;
      }
      oi_vec[1].SetAsymmetric(true);
      Contribution c1(oi_vec,frq_vec);
      aSet.insert(c1);
      // now find << a^e- ; a^e- >>^v
      oi_vec[0].SetAsymmetric(true);
      Contribution c2(oi_vec,frq_vec);
      aSet.insert(c2);
      // now find << a^e- ; a^e+ >>^v
      oi_vec[1].SetAsymmetric(false);
      Contribution c3(oi_vec,frq_vec);
      aSet.insert(c3);
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   // now the mu beta term:
   sort(perm.begin(),perm.end());
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      // first mu
      string op=perm[3].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // then beta
      op=perm[0].first+perm[1].first+perm[2].first+"_POL";
      info_line="type="+op+" frq1="+StringForNb(perm[1].second+perm[2].second);
      info_line+=" frq2="+StringForNb(perm[2].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // now frq vec
      frq_vec.push_back(C_M_1*perm[0].second);
      Contribution c(oi_vec,frq_vec);
      aSet.insert(c);
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   Mout << "Pure vib linear added " << count << " response functions." << endl;
}

map<string,TotalResponseContribution> TotalCubicRsp::ProvideMap()
{
   map<string,TotalResponseContribution> result;
   // Need to loop through mContribSet and create
   // TotalResponseContrib's from that
   vector<Nb> frq_vec;
   set<Contribution>::iterator c_it;
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++)
   {
      TotalResponseContribution t_rsp;
      // we have a contribution
      string func=*it;
      if(mZpvc) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func+"_POL frq1="+StringForNb(mFrq1);
         info_line+=" frq2="+StringForNb(mFrq2);
         info_line+=" frq3="+StringForNb(mFrq3);
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.clear();
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb zpv=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("ZPVC",zpv);
      }
      if(mPvc) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func.substr(I_0,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_1,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_2,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_3,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.push_back(mFrq1);
         frq_vec.push_back(mFrq2);
         frq_vec.push_back(mFrq3);
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb pvq=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("PVC",pvq);
      }
      if(mPvl) {
         Nb alpha_2=AlphaSquaredTerm(func);
         t_rsp.AddTerm("ALPHA^2",alpha_2);
         Nb mu_beta=MuBetaTerm(func);
         t_rsp.AddTerm("MUBETA",mu_beta);
      }
      if(mPvq) {
         Nb mu2_alpha=MuSquaredAlphaTerm(func);
         t_rsp.AddTerm("MU^2ALPHA",mu2_alpha);
      }
      // also eq. value
      frq_vec.clear();
      frq_vec.push_back(mFrq1);
      frq_vec.push_back(mFrq2);
      frq_vec.push_back(mFrq3);
      Nb eq_val=SearchForEqVal(func,frq_vec);
      t_rsp.AddTerm("EQ",eq_val);
      result.insert(make_pair(func,t_rsp));
   }
   return result;
}

Nb TotalCubicRsp::AlphaSquaredTerm(const string& aS)
{
   // here we construct the pure vib. linear response function
   // this means permutation!
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),-(mFrq1+mFrq2+mFrq3)));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   perm.push_back(make_pair(aS.substr(I_3,I_1),mFrq3));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      // loop over sym and asym linear vib. rsp.
      for(In i_sym=0;i_sym<2;i_sym++) {
         vector<OpInfo> oi_vec;
         vector<Nb> frq_vec;
         string op=perm[0].first+perm[1].first+"_POL";
         Nb frq=perm[0].second;
         if(frq<C_0) {
            frq=-frq;
            op=perm[1].first+perm[0].first+"_POL";
         }
         info_line="type="+op+" frq="+StringForNb(frq);
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         // an the second one
         op=perm[2].first+perm[3].first+"_POL";
         frq=perm[3].second;
         if(frq<C_0) {
            frq=-frq;
            op=perm[3].first+perm[2].first+"_POL";
         }
         info_line="type="+op+" frq="+StringForNb(frq);
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.push_back(perm[2].second+perm[3].second);
         Contribution c(oi_vec,frq_vec);
         if(i_sym==1)
            c.SetSpecial("Asymmetric");
         // search for this contribution
         set<Contribution>::iterator c_it=mBasicSet.find(c);
         if(c_it==mBasicSet.end()) {
            ostringstream os;
            os << "Not found: " << endl << c << endl;
            MIDASERROR(os.str());
         }
         result+=c_it->GetValue();
         Mout << "Adding (in alpha^2): " << c_it->GetValue() << endl;
         // that was << a^e+ ; a^e+ >>^v, now find
         //          << a^e+ ; a^e- >>^v
         if(mFrq1==mFrq2 && mFrq1==mFrq3 && mFrq1==C_0) {
            count++;
            continue;
         }
         count++;
         continue;
         // code below must be commented in (i.e. the two lines aboe deleted)
         // if we have actual asymmetric response operators.
         oi_vec[1].SetAsymmetric(true);
         Contribution c1(oi_vec,frq_vec);
         c_it=mBasicSet.find(c1);
         if(c_it==mBasicSet.end()) {
            ostringstream os;
            os << "Not found: " << endl << c1 << endl;
            MIDASERROR(os.str());
         }
         result+=c_it->GetValue();
         // now find << a^e- ; a^e- >>^v
         oi_vec[0].SetAsymmetric(true);
         Contribution c2(oi_vec,frq_vec);
         c_it=mBasicSet.find(c2);
         if(c_it==mBasicSet.end()) {
            ostringstream os;
            os << "Not found: " << endl << c2 << endl;
            MIDASERROR(os.str());
         }
         result+=c_it->GetValue();
         // now find << a^e- ; a^e+ >>^v
         oi_vec[1].SetAsymmetric(false);
         Contribution c3(oi_vec,frq_vec);
         c_it=mBasicSet.find(c3);
         if(c_it==mBasicSet.end()) {
            ostringstream os;
            os << "Not found: " << endl << c3 << endl;
            MIDASERROR(os.str());
         }
         result+=c_it->GetValue();
         count++;
      }
   } while(next_permutation(perm.begin(),perm.end()));
   count/=I_2;
   Mout << "Count: " << count << endl;
   // take into account that there are always 24 permutations of (ops,frq) pairs
   result*=(24.0e0/Nb(count));
   // the prefactor for the ["\alpha^2"]-term
   result*=C_I_8;
   return result;
}

Nb TotalCubicRsp::MuBetaTerm(const string& aS)
{
   // here we construct the pure vib. linear response function
   // this means permutation!
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),-(mFrq1+mFrq2+mFrq3)));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   perm.push_back(make_pair(aS.substr(I_3,I_1),mFrq3));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      // first mu
      string op=perm[0].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // then beta
      op=perm[1].first+perm[2].first+perm[3].first+"_POL";
      // take care of neg. frq.
      Nb frq1=perm[2].second;
      Nb frq2=perm[3].second;
      Nb frq_sum=-(frq1+frq2);
      vector<pair<Nb,string> > frq_op;
      frq_op.push_back(make_pair(frq_sum,perm[1].first));
      frq_op.push_back(make_pair(frq1,perm[2].first));
      frq_op.push_back(make_pair(frq2,perm[3].first));
      sort(frq_op.begin(),frq_op.end());
      op=frq_op[0].second+frq_op[2].second+frq_op[1].second+"_POL";
      /*
      if(frq1<C_0) {
         string tmp_name=op.substr(I_1,I_1)+op.substr(I_0,I_1)+op.substr(I_2,I_1);
         op=tmp_name+"_POL";
         frq1=frq_sum;
         frq_sum=frq1;
      }
      else if(frq2<C_0) {
         string tmp_name=op.substr(I_2,I_1)+op.substr(I_1,I_1)+op.substr(I_0,I_1);
         op=tmp_name+"_POL";
         frq2=frq_sum;
         frq_sum=frq2;
      }
      */
      info_line="type="+op+" frq1="+StringForNb(frq_op[2].first);
      info_line+=" frq2="+StringForNb(frq_op[1].first);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // now frq vec
      if(perm[0].second<C_0) {
         vector<OpInfo> oi_vec_help;
         oi_vec_help.push_back(oi_vec[1]);
         oi_vec_help.push_back(oi_vec[0]);
         oi_vec=oi_vec_help;
         frq_vec.push_back(-perm[0].second);
      }
      else
         frq_vec.push_back(perm[0].second);
      Contribution c(oi_vec,frq_vec);
      set<Contribution>::iterator c_it=mBasicSet.find(c);
      if(c_it==mBasicSet.end()) {
         ostringstream os;
         os << "Not found: " << endl << c << endl;
         MIDASERROR(os.str());
      }
      result+=c_it->GetValue();
      if(gDebug)
         Mout << "Required (mu beta): " << endl << c << endl;
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   // take into account that there are always 24 permutations of (ops,frq) pairs
   result*=(24.0e0/Nb(count));
   // the prefactor for the ["\mu\beta"]-term
   result*=C_I_6;
   return result;
}

Nb TotalCubicRsp::MuSquaredAlphaTerm(const string& aS)
{
   // here we construct the pure vib. quad. response function
   // this means permutation!
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),-(mFrq1+mFrq2+mFrq3)));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   perm.push_back(make_pair(aS.substr(I_3,I_1),mFrq3));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      // first mu
      string op=perm[0].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      op=perm[1].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // then alpha
      Nb frq=perm[3].second;
      op=perm[2].first+perm[3].first+"_POL";
      if(perm[3].second<C_0) {
         frq=-perm[3].second;
         op=perm[3].first+perm[2].first+"_POL";
      }
      info_line="type="+op+" frq="+StringForNb(frq);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      // now frq vec
      frq_vec.push_back(perm[1].second);
      frq_vec.push_back(perm[2].second+perm[3].second);
      Contribution c(oi_vec,frq_vec);
      //Mout << "Before search for: " << endl << c << endl;
      set<Contribution>::iterator c_it=mBasicSet.find(c);
      if(c_it==mBasicSet.end()) {
         ostringstream os;
         os << "Unable to find: " << endl << c << endl;
         MIDASERROR(os.str());
      }
      result+=c_it->GetValue();
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   // take into account that there are always 24 permutations of (ops,frq) pairs
   Mout << "Count: " << count << " result = " << result << endl;
   result*=(24.0e0/Nb(count));
   // the prefactor for the ["\alpha^2"]-term
   result*=C_I_4;
   return result;
}

ostream& TotalCubicRsp::Print(ostream& aOut) const
{
   aOut << "Order = 4" << endl;
   aOut << "Frq1  = " << mFrq1 << endl;
   aOut << "Frq2  = " << mFrq2 << endl;
   aOut << "Frq3  = " << mFrq3 << endl;
   return aOut;
}
