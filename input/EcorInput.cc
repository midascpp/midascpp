/**
************************************************************************
* 
* @file                EcorInput.cc
*
* 
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the Ecor class /
*                      Electron correlation calcs. 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/EcorCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"

using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

/**
* Read and check Ecor input.
* Read until next s = "#i..." i<3 which is returned.
* */
string EcorInput(std::istream& Minp)
{
   if (gDoEcor && gDebug) 
      Mout << " I have read one Ecor input - reading a new Ecor input "<< endl;
   gDoEcor    = true; 
   gEcorCalcDef.push_back(EcorCalcDef());
   EcorCalcDef& new_ecor = gEcorCalcDef[gEcorCalcDef.size()-1];  // new_oper is a reference to the new operator

   enum INPUT {ERROR, IOLEVEL, NAME, RESTART, MACROMAXITER, CPTENSORS, CPALSDIFFTHR, CPALSMAXITER, CCMODEL, 
               TENSORDECOMPINFO, RIMP2, THRSVDRI, THRERR, USEOVLPMETRIC, LAPCONV, LAPLACE};
   const map<string,INPUT> input_word =
   {
      {"#3IOLEVEL",IOLEVEL},
      {"#3NAME",NAME},
      {"#3RESTART",RESTART},
      {"#3MACROMAXITER",MACROMAXITER},
      {"#3CPTENSORS", CPTENSORS},
      {"#3CPALSDIFFTHR",CPALSDIFFTHR},
      {"#3CPALSMAXITER",CPALSMAXITER},
      {"#3CCMODEL", CCMODEL},
      {"#3RIMP2",RIMP2},
      {"#3THRSVDRI", THRSVDRI},
      {"#3THRERR", THRERR},
      {"#3USEOVLPMETRIC", USEOVLPMETRIC},
      {"#3LAPCONV", LAPCONV},
      {"#3LAPLACE", LAPLACE},
      {"#3TENSORDECOMPINFO",TENSORDECOMPINFO}
   };
   string s="";
   In input_level=3;
   bool already_read = false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read = false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetIoLevel(midas::util::FromString<In>(s));
            break;
         } 
         case NAME:
         {
            getline(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string s_name="";
            input_s >> s_name;
            new_ecor.SetName(s_name);
            for (In i_calc =0;i_calc<gEcorCalcDef.size()-1;i_calc++)
               if (gEcorCalcDef[i_calc].GetName() == new_ecor.GetName()) 
                  MIDASERROR("Ecor calculation name is already occupied");
            break;
         }
         case RESTART:
         {
            bool restart = true;
            new_ecor.SetRestart(restart);
            break;
         }
         case MACROMAXITER:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetMacroMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case CPTENSORS:
         {
            bool do_cp = true;
            new_ecor.SetDoCP(do_cp);
            break;
         }
         case RIMP2:
         {
            bool do_rimp2 = true;
            new_ecor.SetDoRIMP2(do_rimp2);  // activate RI-MP2
           
            getline(Minp,s);                // get new line with input
            new_ecor.SetPTAlgo(s);          // which algorithm for RI-MP2 to use
            break; 
         }
         case LAPCONV:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetLapConv(midas::util::FromString<Nb>(s));
            break;
         }
         case CPALSDIFFTHR:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetCpAlsDiffThr(midas::util::FromString<Nb>(s));
            break;
         }
         case CPALSMAXITER:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetCpAlsMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case THRSVDRI:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetThrSVD(midas::util::FromString<Nb>(s));
            break;
         }
         case THRERR:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetThrErr(midas::util::FromString<Nb>(s));
            break;
         }
         case CCMODEL:
         {
            getline(Minp,s);                                  // Get new line with input
            new_ecor.SetCCModel(s);
            break;
         }
         case USEOVLPMETRIC:
         {
            bool useovlp = true;
            new_ecor.SetUseOvlpMetric(useovlp);
            break;
         }
         case TENSORDECOMPINFO:
         {
            already_read = new_ecor.ReadEcorDecompInfoSet(Minp, s);  // Input flags for decomposer
            break; 
         }
         case LAPLACE:
         {
            already_read = new_ecor.ReadLaplaceInfoSet(Minp, s);  // Input flags for Laplace Quadrature
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not an Ecor level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}
/**
* Initialize Ecor variables
* */
void InitializeEcor()
{
   gDoEcor              = false; 
}
