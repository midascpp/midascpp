/**
************************************************************************
* 
* @file                Contribution.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Contribution datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <map>

#include "inc_gen/math_link.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/Contribution.h"
#include "input/OpInfo.h"

#include "libmda/numeric/float_eq.h"


/**
* Constructor, no action yet...
* */
Contribution::Contribution
   (  const std::vector<OpInfo>& aV
   ,  const std::vector<Nb>&     aFrq
   )
   :  mMainOrder(I_0)
   ,  mVibRspOrder(aV.size())
   ,  mOpInfos(aV)
   ,  mFrq(aFrq)
   ,  mValue(C_0)
   ,  mEval(false)
   ,  mSpecial("")
{
   for(In i = 0; i < aV.size(); i++)
   {
      mMainOrder += aV[i].GetType().GetOrder();
   }
}

Contribution::Contribution(const Contribution& aC)
{
   mMainOrder=aC.GetMainOrder();
   mVibRspOrder=aC.GetVibRspOrder();
   mOpInfos=aC.GetVecOfOpInfo();
   mFrq=aC.GetFrq();
   mEval=aC.HasBeenEval();
   mValue=aC.GetValue();
   mSpecial=aC.GetSpecial();
}

bool operator<(const Contribution& aC1, const Contribution& aC2)
{
   // compare on main order
   //Mout << "compare:" << endl << aC1 << "TO:" << endl << aC2 << endl;
   //Mout << "Orders: " << aC1.GetMainOrder() << " -> " << aC2.GetMainOrder() << endl;
   if(aC1.GetMainOrder()!=aC2.GetMainOrder())
      return aC1.GetMainOrder()<aC2.GetMainOrder();
   // compare on vib rsp order
   //Mout << "Vib Orders: " << aC1.GetVibRspOrder() << " -> " << aC2.GetVibRspOrder() << endl;
   if(aC1.GetVibRspOrder()!=aC2.GetVibRspOrder())
      return aC1.GetVibRspOrder()<aC2.GetVibRspOrder();
   // compare the individual operators
   for(In i=0;i<aC1.GetVibRspOrder();i++) 
   {
      //Mout << "Vib Orders (indiv): " << aC1.GetOpInfo(i).GetType() << " -> " << aC2.GetOpInfo(i).GetType() << endl;
      if(aC1.GetOpInfo(i).GetType() != aC2.GetOpInfo(i).GetType()) 
      {
         return aC1.GetOpInfo(i).GetType() < aC2.GetOpInfo(i).GetType();
      }
      // and the frequencies of the operators
      In n_frq=aC1.GetOpInfo(i).GetType().GetOrder() - 1;
      for(int j = 0; j < n_frq; ++j)
      {
         if(libmda::numeric::float_neq(aC1.GetOpInfo(i).GetFrq(j), aC2.GetOpInfo(i).GetFrq(j))) 
         {
            return aC1.GetOpInfo(i).GetFrq(j) < aC2.GetOpInfo(i).GetFrq(j);
         }
      }
   }
   if (  aC1.GetFrq().size() == 0 
      && aC2.GetFrq().size() == 0
      )
   {
      return false;
   }
   // compare the vibrational frequencies
   //Mout << "# Vib. frq: " << aC1.GetFrq().size() << " -> " << aC2.GetFrq().size() << endl;
   if(aC1.GetFrq().size() != aC2.GetFrq().size()) 
   {
      return aC1.GetFrq().size() < aC2.GetFrq().size();
   }
   for(In i = 0; i < aC1.GetFrq().size(); i++) 
   {
      //Mout << "Vib. frq (indiv): " << aC1.GetFrq()[i] << " -> " << aC2.GetFrq()[i] << endl;
      if(libmda::numeric::float_neq(aC1.GetFrq()[i], aC2.GetFrq()[i]))
      {
         return aC1.GetFrq()[i] < aC2.GetFrq()[i];
      }
   }
   // everything the same, return false;
   //Mout << "Maybe special..." << endl;
   if(aC1.GetSpecial() != aC2.GetSpecial())
   {
      return aC1.GetSpecial() < aC2.GetSpecial();
   }
   //Mout << "I return..." << endl;
   return false;
   //return aC1.GetOpInfo(0)<aC2.GetOpInfo(0);
}

bool operator==(const Contribution& aC1,const Contribution& aC2)
{
   // compare on main order
   if(aC1.GetMainOrder()!=aC2.GetMainOrder())
      return false;
   // compare on vib rsp order
   if(aC1.GetVibRspOrder()!=aC2.GetVibRspOrder())
      return false;
   // compare the individual operators
   for(In i=0;i<aC1.GetVibRspOrder();i++) 
   {
      if(aC1.GetOpInfo(i).GetType()!=aC2.GetOpInfo(i).GetType()) 
      {
         return false;
      }
      // and the frequencies of the operators
      In n_frq=aC1.GetOpInfo(i).GetType().GetOrder() - 1;
      for(int j = 0; j < n_frq; ++j)
      {
         if(libmda::numeric::float_neq(aC1.GetOpInfo(i).GetFrq(i), aC2.GetOpInfo(i).GetFrq(j))) 
         {
            return false;
         }
      }
   }
   if (  aC1.GetSpecial() != aC2.GetSpecial())
   {
      return false;
   }
   
   // compare the vibrational frequencies
   for(int i = 0; i < aC1.GetFrq().size(); ++i) 
   {
      if (  libmda::numeric::float_neq(aC1.GetFrq()[i], aC2.GetFrq()[i]))
      {
         return false;
      }
   }
   return true;
}

ostream& operator<<(ostream& aOut,const Contribution& aC)
{
   aOut << "Main order:                 " << aC.GetMainOrder() << endl;
   aOut << "Vibrational response order: " << aC.GetVibRspOrder() << endl;
   aOut << "Special response          : " << aC.GetSpecial() << endl;
   aOut << "Operator descriptions:" << endl;
   for(In i = 0; i < aC.GetVibRspOrder(); i++) 
   {
      aOut << "   " << aC.GetOpInfo(i).GetType();
      In n_frq = aC.GetOpInfo(i).GetType().GetOrder() - 1;
      for(In j = 0; j < n_frq; j++)
      {
         aOut << "   " << aC.GetOpInfo(i).GetFrq(j);
      }
      aOut << endl;
   }
   aOut << "Vibrational response frequencies:" << endl;
   for(In i = 0; i < aC.GetVibRspOrder()-1; i++)
   {
      aOut << "   " << aC.GetFrq(i) << endl;
   }
   aOut << "Value = " << aC.GetValue() << endl;
   return aOut;
}

Contribution Contribution::operator=(const Contribution& aC)
{
   Contribution c(aC.GetVecOfOpInfo(),aC.GetFrq());
   c.SetValue(aC.GetValue());
   return c;
}
