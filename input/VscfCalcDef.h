/**
************************************************************************
* 
* @file                VscfCalcDef.h
*
* Created:             18-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Base class for a Vib wf input-info handler 
* 
* Last modified:       29-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef CALCDEF_H
#define CALCDEF_H

// Standard Headers
#include <string>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/RspCalcDef.h"
//#include "input/ComplexRspCalcDef.h"
#include "input/ThermalSpectrumDef.h"
#include "mmv/MidasMatrix.h"

using std::string;
using std::vector;

/**
 * Construct a definition of a VSCF calculation
**/
class VscfCalcDef 
   :  public RspCalcDef
{
   private:
      std::string mCalcName;     ///< Calc name
      std::string mCalcPrefix;   ///< Prefix which is identical for all calculations defined in the same input block.
      std::string mCalcOper;     ///< Calc Oper
      std::string mCalcBasis;    ///< Calc Basis 
      std::string mDiagMeth;     ///< Method of diagonalization
      std::vector<In> mOccVec;   ///< Occupation vector
      In mNiterMax;       ///< Maximum Number of global iterations 
      In mIoLevel;        ///< Io level for scf calculation.
      Nb mResidThr;       ///< Threshold on vector (resididual)
      Nb mEnerThr;        ///< Threshold on energy 
      Nb mEfinal;         ///< After the calculation store the final energy.
      Nb mEfinalStateAve; ///< After the calculation store the final state average energy.
      Nb mScreenZero;     ///< Zero threshold for screening of 
                          // oper. cont. to fock mat etc.
      Nb mScreenZeroCoef; ///< Zero threshold for screening of coef in operator 
      bool mDone;         ///< Has the calculation been done
      bool mRestart;      ///< Restart from data on file? 
      bool mUseAvailable; ///< Restart available data even though not complete...(VCC2->VCC3,f.ex).
      bool mMapPrevVecs;  ///< Restart from previous calc.: maps ``any'' size of bas^{prev}, and any level of VCC^{prev}.
      bool mSave;         ///< Save the modals at the end of the calculation.
      In mIntStorageLevel;///< Integrals in memory (0) or on disc
      In mVecStorageLevel;///< Vector Storage Level 0-3, 0 all inmem, 3 on disc

      In mItEqNiterMax;   ///< Maximum Number of iterations for it eq. solver (general or MACRO if relevant) 
      In mItEqMicroNmax;  ///< Maximum Number of iterations for it eq. solver in MICRO iterations
      In mRedDimMax;      ///< maximum dim of reduced space 
      In mNbreakDim;      ///< Dim. for breaking and starting anew in eq.solv.
      Nb mItEqResidThr;   ///< Threshold (residual norm) for It eq solver.
      Nb mItEqEnerThr;    ///< Threshold (energy change) for It eq solver.
      Nb mItEqMicroThrFac;   ///< Residual thr for Micro iterations is a vector norm (err or rhs) times this factor. 

      bool mPreDiag;      ///< Solve eq. using prediagonalization.
      In mNpreDiag;       ///< Max. number of vectors included in prediag. 
      bool mImprovedPrecond;   ///< Solve eq. to obtain a better preconditioner in Olsen.
      In mPrecondExciLevel;///< Max. number of mode couplings included in the precond. Hamiltonian
      string mLevel2Solver;          ///< Method for solving the linear equations in improved precond 
      bool mTrueHDiag;    ///< Use true diagonal transformer?
      bool mTimeIt;       ///< Time per iteration
      bool mSizeIt;       ///< Size various quantities 
      In mSequenceNr;     ///< Nr of a sequence 

      bool mStateAveVirtVscf;  ///< Make a state average virtural VSCF energy 
      bool mFundOnlyStateAveVirtVscf;  ///< Make a state average virtual VSCF energy using ONLY the ground state and the fundamentals. 
      vector<In> mStateAveNrs; ///< State average, nr of modals pr mode. 
      bool mFastTempAve;  ///< Use new implementation of temp. ave. 1
      bool mOneStateVscfAve; ///< Use new impl. of temp. ave. 2
      bool mAllVirtExptValues; ///< Use new impl. of temp. ave. 2
      MidasMatrix mOneStateMatrix; ///< Matrix containing all temperature averages and PartFuncs
      MidasMatrix mOccupancy; ///< Matrix containing all thermal occupations 
      vector<string> mPropertyList; ///< Matrix containing all temperature averages and PartFuncs
      //parameters for the calculation of the 1-particle densities on a grid of points
      bool mCalcDensities;                      ///< Compute the 1-partice density
      bool mPlotDensities;                      ///< Plot the 1-particle densities
      std::string mVscfAnalysisDir;             ///< Vscf specific analysis directory
      In mPlotNmodals;                          ///< Number of modals that should be plotted
      bool mAdgaDensAnalysis;   ///< Is the analysis on the density to be performed for the iterative pes constr.

      //! Flag used in Vscf module to indicate that we are doing ADGA
      bool mAdga = false;
      //! Flag used in Vscf module to indicate that densities should be calculated in Vscf::Finalize
      bool mAdgaMultiLevelDone = false;

      vector<In> mAnalStatesVec;      ///< compute the densities up to the # state for the pes procedure
      bool mAnalizeMaxDensity;    ///< Analize the max density
      bool mAnalizeMeanDensity;    ///< Analize the mean density
      bool mAnharmGuess;          ///< Start with the full 1-mode potential for the starting guess
      string mTargetingMethod;          ///< Converge solutions having large overlap with the targets
      Nb mOverlapMin;                   ///< Selection threshold to retain important solutions
      Nb mOverlapSumMin;                ///< Selection threshold to retain important solutions
      Nb mEnergyDiffMax;            ///< Energy range (in cm^{-1}) to retain important solutions +-E_{target}
      Nb mResidThrForOthers;           ///< Factor to change the threshold for residual norm for extra states in It eq solver.
      Nb mEnerThrForOthers;           ///< Factor to change the threshold for energy change for extra states in It eq solver.
      bool mUseThermalDensity;
      Nb mThermalDensityTemp;
      vector<In> mThermalDensityOffset;
      bool mVariational;        ///< Is it a variational model 
      // following for thermal spectra
      vector<ThermalSpectrumDef> mThermalSpecs;
      string mRestName;                   ///< Name of the calculation to start from
      bool mTruncateThermalOccup;         ///< Limit initial states to those with significant occup. 
      Nb mThermalOccupThr;                ///< Threshold for limiting initial states to those with significant occup. 

      vector<In> mNmodals;                ///< The limits (n_0,n_1,-...n_M) for post VSCF calc. 
      bool mNmodalsSetInput;              ///< ModalLimitsSetin Input. 
      bool mSosRsp;
      bool mIntAnalysis;                  ///< whether analyis of the intergrals for later screening should be carried out
      In mIntAnalysisLimit; ///< Limit the vibrational space in IntAnalysis 
      
      //std::vector<ComplexRspCalcDef> mComplexRspCalcDefs;

      //! Dump modals to file under a special name for later use in MatRep VCC calc.
      bool mDumpModalsForMatRep = false;

   protected: 
      // Parameters for Franck-Condon calculations.
      bool mFranckCondon;           ///< Calculate Franck-Condon factors.
      string mFCOtherCalc;         ///< Name of calculation giving the other set of vib. states.
      string mFCOperator;          ///< Operator in generalized overlap, <i| P |f>.
      
      //For VccCalcDef
      vector<vector<In> >      mMolMCs; ///< List of modes in different molecule
   public:
      VscfCalcDef();
      virtual ~VscfCalcDef() = default;      
      ///< Constructor
      void SetOper(const string& arOper)
      {mCalcOper=arOper;}
      ///< Set the operator name
      void SetBasis(const string& arBasis)
      {mCalcBasis=arBasis;}
      ///< Set the basis name
      void SetName(const string& arName)
      {
         mCalcName=arName;
         mRestName=arName;
      }
      ///< Set the calc name

      //! Set prefix
      void SetPrefix(const std::string& aPrefix) { mCalcPrefix = aPrefix; }

      void SetDiagMeth(const string& arDiagMeth)
      {mDiagMeth=arDiagMeth;}
      ///< Set the diagmeth
      void SetMaxIter(const In& arMaxIter)
      {mNiterMax=arMaxIter;}                
      ///< Set the maximem nr. of iterations
      void SetIoLevel(const In& arIoLevel)
      {mIoLevel =arIoLevel;}
      ///< Set the IoLevel
      void SetResidThr(const Nb& arThr)
      {mResidThr=arThr;}
      ///< Set the Threshold 
      void SetEnerThr(const Nb& arThr)
      {mEnerThr=arThr;}
      ///< Set the Threshold 
      void SetScreenZero(const Nb& arZ)
      {mScreenZero=arZ;}
      ///< Set the numerical zero for screening
      void SetScreenZeroCoef(const Nb& arZ)
      {mScreenZeroCoef=arZ;}
      ///< Set the numerical zero for screening
      void SetSave(const bool& arSave)
      {mSave = arSave;}
      ///< Set the bool save 
      void SetRestart(const bool& arRest)
      {mRestart= arRest;}
      ///< Set the bool save 
      void SetUseAvailable(const bool& arUseA)
      {mUseAvailable = arUseA;}
      ///< Set the bool for use available 
      void SetMapPreviousVectors(const bool& arMapV)
      {mMapPrevVecs = arMapV;}
      ///< Set the bool for mapping previous vectors
      void SetOccVec(const std::vector<In>& aOccVec)
      {mOccVec=aOccVec;}
      ///< Set the occupation vector 
      void SetStateAveNrs(const vector<In>& arV)
      {mStateAveVirtVscf=true; mStateAveNrs=arV;}
      In GetStateAveNr(In aImode) const {return mStateAveNrs[aImode];}  ///< Nr for a given mode 
      bool DoStateAve() {return mStateAveVirtVscf;} ///< State averaged or not, virt VSCF 
      void SetFundOnlyStateAve(){mFundOnlyStateAveVirtVscf=true; mStateAveVirtVscf=true;}
      bool DoFundOnlyStateAve(){return mFundOnlyStateAveVirtVscf;}
      ///< Set the state average occupation vector 
      void SetDone(Nb aE) { mEfinal = aE; mDone = true;}
      void SetDoneStateAve(Nb aE) { mEfinalStateAve = aE;}
      ///< Set calculation done 

      void SetItEqMaxIter(const In& arMaxIter) {mItEqNiterMax =arMaxIter;} 
      ///< Set the maximem nr. of iterations
      void SetItEqMaxIterMicro(const In& arMaxIter) {mItEqMicroNmax =arMaxIter;} 
      ///< Set the maximem nr. of MICRO iterations
      void SetRedDimMax(const In& arMax) {mRedDimMax=arMax;} 
      ///< Set maximum dim of reduced space 
      void SetRedBreakDim(const In& arMax) {mNbreakDim=arMax;}
      ///< Set Dimension for breaking and starting over in eq.solv.
      void SetItEqResidThr(const Nb& arThr) {mItEqResidThr  =arThr;}
      ///< Set threshold for eq. solver 
      void SetItEqMicroThrFac(const Nb& arThr) {mItEqMicroThrFac  =arThr;}
      ///< Set threshold for eq. solver - Fector for micro iterations 
      void SetItEqEnerThr(const Nb& arThr) {mItEqEnerThr  =arThr;}
      ///< Set Energy threshold for eq. solver 
//      void SetDiis(const bool& arB)
//      {mDiis     =arB;}
      ///< Set mDiis
      void SetTimeIt(const bool& arB) {mTimeIt =arB;}
      ///< Set mTimeIt 
      void SetSizeIt(const bool& arB) {mSizeIt =arB;}
      ///< Set mTimeIt 
//      void SetMaxDiis(const In& arI)
//      {mMaxDiis     =arI;}
      void SetPreDiag(const In& arI)
      {mPreDiag=true; mNpreDiag = arI;}
      void SetImprovedPrecond(const In& arI)
      {mImprovedPrecond=true; mPrecondExciLevel = arI;}
      void SetLevel2Solver(const string& arS)
      {mLevel2Solver=arS;}
      void SetTargetingMethod(string aS) {mTargetingMethod = aS; }///< Use improved targeting 
      void SetOverlapMin(const Nb& arThr) {mOverlapMin  =arThr;}
      ///< Set selection threshold for overlap between targets and solutions
      void SetOverlapSumMin(const Nb& arThr) {mOverlapSumMin  =arThr;}
      ///< Set selection threshold for overlap sum between targets and solutions
      void SetEnergyDiffMax(const Nb& arThr) {mEnergyDiffMax  =arThr;}
      ///< Set selection threshold for overlap between targets and solutions
      void SetResidThrForOthers(const Nb& arThr) {mResidThrForOthers  =arThr;}
      ///< Set reduce residual norm threshold for other states
      void SetEnerThrForOthers(const Nb& arThr) {mEnerThrForOthers  =arThr;}
      ///< Set reduce energy threshold for other states
      void SetTrueHDiag(bool aB) {mTrueHDiag = aB;}
      void SetSosRsp(bool aB) { mSosRsp = aB; }
      void SetIntStorage(const In aI) {mIntStorageLevel = aI;}
      ///< Return the storage mode for integrals 0,1
      void SetVecStorage(const In aI) {mVecStorageLevel = aI;}
      ///< Return the storage mode for vectors   0,1,2,3
      void SetSequence(const In& arSec) {mSequenceNr = arSec;} // Set 
      void SetFastTempAve(bool aB) {mFastTempAve=aB;}
      

      vector<vector<In> >& GetMolMCs() {return mMolMCs;} //Overwriten in VccCalcDef
      string GetTargetingMethod() const {return mTargetingMethod; }///< Use improved tageting
      Nb GetOverlapMin() const {return mOverlapMin;}
      ///< Get selection threshold for overlap between targets and solutions
      Nb GetOverlapSumMin() const {return mOverlapSumMin;}
      ///< Get selection threshold for overlap sum between targets and solutions
      Nb GetEnergyDiffMax() const {return mEnergyDiffMax;}
      ///< Get energy range to retain solutions +- E_{target}
      Nb GetResidThrForOthers() const {return mResidThrForOthers;}
      Nb GetEnerThrForOthers() const {return mEnerThrForOthers;}
      In GetItEqMaxIter() const {return mItEqNiterMax;}
      ///< Get the maximem nr. of iterations
      In GetItEqMaxIterMicro() const {return mItEqMicroNmax;}
      ///< Get the maximem nr. of MICRO iterations
      In GetRedDimMax() const {return mRedDimMax;}
      ///< Get maximum dim of reduced space 
      In GetRedBreakDim() const {return mNbreakDim;}
      ///< Get Dimension for breaking and starting over in eq.solv.
      Nb GetItEqResidThr() const {return mItEqResidThr;}
      ///< Get thresholds factor for mirco iterations 
      Nb GetItEqMicroThrFac() const {return mItEqMicroThrFac;}
      ///< Get thresholds for eq.solv.
      Nb GetItEqEnerThr() const {return mItEqEnerThr;}
      ///< Get thresholds for eq.solv.
//      In GetMaxDiis() const {return mMaxDiis;}
      ///< Get maximum vectors in diis
//      bool Diis() {return mDiis;}
      ///< is it a Diis?
      bool PreDiag() const {return mPreDiag;}
      bool GetImprovedPrecond() const {return mImprovedPrecond;}
      In GetPrecondExciLevel() const {return mPrecondExciLevel;}
      string GetLevel2Solver() const {return mLevel2Solver;}
      In NpreDiag() const {return mNpreDiag;}
      bool TimeIt() const {return mTimeIt;}
      bool SizeIt() const {return mSizeIt;}
      bool GetTrueHDiag() const {return mTrueHDiag;}
      bool GetSosRsp() const {return mSosRsp; }
      const std::string& GetName(bool restart=false) const
      {
         if (restart)
            return mRestName;
         else
            return mCalcName;
      }
      ///< Get Name 
 
      const std::string& Oper() const {return mCalcOper;}
      ///< Get Oper 

      //! Get prefix
      const std::string& Prefix
         (
         )  const
      {
         return mCalcPrefix;
      }

      const std::string& Basis() const {return mCalcBasis;}
      ///< Get Basis 
      const std::string& DiagMeth() const {return mDiagMeth;}
      ///< Return the Diag method
      In GetNmodesInOcc() const {return mOccVec.size();}
      ///< Get number of modes defined in scf
      In IoLevel() const {return mIoLevel;}
      ///< Get the iolevel 
      bool Save() const {return mSave;}
      ///< Save mSave - modals at end of calc.
      bool Restart() const {return mRestart;} 

      // Set the name of the calculation to restart from
      ///< Restart from previous data 
      bool UseAvailable() const {return mUseAvailable;} 
      ///< Restart from previous data though not complete
      bool GetMapPreviousVectors() const {return mMapPrevVecs;} 
      ///< Restart from previous smaller/larger vectors
      bool Done() const {return mDone;}
      ///<  Done?
      Nb GetResidThr() const {return mResidThr;}
      ///< Get the threshold for wf  iterations. 
      Nb GetEnerThr() const {return mEnerThr;}
      ///< Get the threshold for wf  iterations. 
      Nb GetEfinal() const {return mEfinal;}
      ///< Get the final vscf energy.
      Nb GetEfinalStateAverage() const {return mEfinalStateAve;}
      ///< Get the final vscf state average energy.
      Nb ScreenZero() const {return mScreenZero;}
      ///< Get the zero threshold.
      Nb ScreenZeroCoef() const {return mScreenZeroCoef;}
      ///< Get the zero threshold.
      In GetMaxIter() const {return mNiterMax;}
      ///< Get maximum number of iter.
      In GetOccMode(In aImode) const {return mOccVec[aImode];}
      ///< Return the occupied modal nr for aImode
      const vector<In>& OccupVec() const {return mOccVec;} 
      ///< Return the whole occupation vector

      In IntStorage() const {return mIntStorageLevel;}
      ///< Return the storage mode for integrals 0,1
      In VecStorage() const {return mVecStorageLevel;}
      ///< Return the storage mode for vectors   0,1,2,3
      friend bool VscfCalcDefLess(const VscfCalcDef& arDef1,
            const  VscfCalcDef& arDef2);                            
      ///< Define comparison
      friend void  ExtractPropsAndSaveOnFiles(vector<VscfCalcDef*> apVscfCalcDefVec,string aLabel);
      ///< Save properties for later analysis 
      void ZeroOcc(const In& arNmodes);                         
      ///< Create a (000..) occupation of dim arNmodes. 
      void Reserve();                                           
      ///< Reserve STL heap
      In Sequence() const {return mSequenceNr;} // Sequence 

      void SetOneStateVscfAve(bool aB) {mOneStateVscfAve=aB;}
      void SetOneStateMatrix(In aI, In aJ, Nb aV) {mOneStateMatrix[aI][aJ]=aV;}
      void OneStateMatrixResize(In aI, In aJ, bool aB=true) {mOneStateMatrix.SetNewSize(aI,aJ,aB);}
      void OneStateMatrixExtend(In aI) 
      {
         In rows=mOneStateMatrix.Nrows();
         In cols=mOneStateMatrix.Ncols()+aI;
         mOneStateMatrix.SetNewSize(rows,cols,true,true);
      }
      void OccupancyResize(In aI, In aJ, bool aB=true) {mOccupancy.SetNewSize(aI,aJ,aB);}
      void OccupancyZero() {mOccupancy.Zero();}
      void OccupancyExtend(In aI) 
      {
         In rows=mOccupancy.Nrows();
         In cols=mOccupancy.Ncols()+aI;
         mOccupancy.SetNewSize(rows,cols,true,true);
      }
      void SetOccupancy(In aI, In aJ, Nb aV) {mOccupancy[aI][aJ]=aV;}
      void SetProperty(string aS) {mPropertyList.push_back(aS);}
      void SetAllVirtExptValues(bool aB=false) {mAllVirtExptValues=aB;}
      void SetCalcDensities(bool aB=false) {mCalcDensities=aB;}  ///< bool flag for 1-particle density calcs
      void SetPlotDensities(bool aB=false) {mPlotDensities=aB;}  ///< bool flag for 1-particle density plots
      void SetmVscfAnalysisDir(const std::string& aStr) {mVscfAnalysisDir = aStr;}
      void SetPlotNmodals(const In arPlotNmodals) {mPlotNmodals=arPlotNmodals;} ///< Set number of modals that should be plotted                
      void SetmAdgaDensAnalysis(const bool& aB) {mAdgaDensAnalysis = aB;}  ///< bool flag for pes density analysis
      void SetNrStatesForAnalysis(const vector<In>& arInVec); ///< plot the states and densities up to this level
      void SetAnalizeMaxDensity(bool aB) {mAnalizeMaxDensity=aB;}  ///< Set the mean dens. analysis
      void SetAnalizeMeanDensity(bool aB) {mAnalizeMeanDensity=aB;}  ///< Set the mean dens. analysis
      void SetAnharmGuess(bool aB) {mAnharmGuess=aB;} ///< Start from 1-mode anharmonic guess
      void SetFranckCondon(bool aB) {mFranckCondon = aB;}
      void SetFCOtherCalc(const string& aStr) {mFCOtherCalc = aStr;}
      void SetFCOperator(const string& aStr) {mFCOperator = aStr;}
      void SetUseThermalDensity(bool b) {mUseThermalDensity=b;}
      void SetThermalDensityTemp(Nb a) {mThermalDensityTemp=a;}
      void SetThermalDensityOffset(const vector<In>& aV) {mThermalDensityOffset=aV;}

      bool FastTempAve() const {return mFastTempAve;}
      bool OneStateVscfAve() const {return mOneStateVscfAve;}
      Nb GetOneStateMatrix(In aI, In aJ) const {return mOneStateMatrix[aI][aJ];}
      MidasMatrix GetOneStateMatrix() const {return mOneStateMatrix;}
      Nb GetOccupancy(In aI, In aJ) const {return mOccupancy[aI][aJ];}
      MidasMatrix GetOccupancy() const {return mOccupancy;}
      string GetProperty(In aI) const {return mPropertyList[aI];}
      bool AllVirtExptValues() const {return mAllVirtExptValues;}
      void GetNrStatesForAnalysis(vector<In>& arInVec) const {arInVec=mAnalStatesVec;}///<plot the states and densities up to this level
      In  GetNrStatesForAnalysis(In aImode) const {return mAnalStatesVec[aImode]; }       ///<plot the states and densities up to this level
      bool Calc1pDensities() const {return mCalcDensities;}   ///< Calculate 1-particle densities
      bool Plot1pDensities() const {return mPlotDensities;}   ///< Plot 1-particle densities
      const std::string& GetmVscfAnalysisDir() const {return mVscfAnalysisDir;}
      In GetPlotNmodals() const {return mPlotNmodals;} ///< Get number of modals that should be plotted                
      const bool& GetmAdgaDensAnalysis() const  {return mAdgaDensAnalysis;} ///< it the pes constructed iteratively
      bool AnalizeMaxDensity() const {return mAnalizeMaxDensity;} ///< Analize the max. density
      bool AnalizeMeanDensity() const {return mAnalizeMeanDensity;} ///< Analize the mean density
      bool AnharmGuess() const {return mAnharmGuess;}  ///< Is the guess from the full 1-mode pot.
      bool FranckCondon() const {return mFranckCondon;}
      const string& FCOtherCalc() const {return mFCOtherCalc;}
      const string& FCOperator() {return mFCOperator;}
      bool GetUseThermalDensity() const {return mUseThermalDensity;}
      Nb GetThermalDensityTemp() const {return mThermalDensityTemp;}
      const vector<In>& GetThermalDensityOffsetVec() const {return mThermalDensityOffset;}
      In GetThermalDensityOffset(const In& aI) const {return mThermalDensityOffset[aI];}

      // following for thermal spectra
      void AddThermalSpectrum(ThermalSpectrumDef aD) {mThermalSpecs.push_back(aD);}
      ThermalSpectrumDef& GetThermalSpectrum(In aI) {return mThermalSpecs[aI];}
      In GetNThermalSpectra() const {return mThermalSpecs.size();}
      void SetThermalOccupThr(Nb aThr) {mTruncateThermalOccup=true; mThermalOccupThr=aThr;}
      bool IsThermalOccupThr() const {return mTruncateThermalOccup;} 
      Nb ThermalOccupThr() const {return mThermalOccupThr;} 

      // following for thermal spectra and other post VSCF calcs. 
      void SetModalLimits(const vector<In>& arModalBasisLimit)          ///< Set size of modal basis 
                        {mNmodals = arModalBasisLimit;}

      void SetModalLimitsInput(bool aS=true) {mNmodalsSetInput=aS;}          /// On input size of modal basis set 
      bool ModalLimitsInput() const {return mNmodalsSetInput;}                /// On input size of modal basis set 
      void SetModalLimitSize(In aN) {mNmodals.resize(aN);}          ///< Set size of modal basis 
      void SetModalLimit(In aMode,In aN) {mNmodals[aMode]=aN;}          ///< Set size of modal basis 
      In   NmodesInModals() const {return mNmodals.size();}             ///< Number of modes in basis limit, POST VSCF 
      void Nmodals(vector<In>& arModals) const                          /// Get Number of modals , POST VSCF
                        {arModals = mNmodals;}
      In   Nmodals(const In& arI) const                                 /// Get Number of modals for a mode, POST VSCF
                        {return mNmodals[arI];}
      const vector<In>& GetNModals() const {return mNmodals;}


      bool Variational() const {return mVariational;}
      void SetVariational(bool aB) {mVariational=aB;}

      // Set the name of the calculation to restart from
      void SetRestName(const string& arRestName) {mRestName = arRestName;}
      void SetIntAnalysis(bool arIntAnalysis=true) {mIntAnalysis=arIntAnalysis;}
      void SetIntAnalysisLimit(const In& arIn) {mIntAnalysisLimit=arIn;}
      bool GetIntAnalysis() const {return mIntAnalysis;}
      In GetIntAnalysisLimit() const {return mIntAnalysisLimit;}

      //! Read general mode-combi vector
      bool ReadGenCombi
         (  std::istream&
         ,  std::string&
         ,  std::set<std::map<In, In> >&
         )  const;

      //! Get/set ADGA-related flags
      bool GetAdga() const { return mAdga; }
      void SetAdga(bool aB) { mAdga = aB; }
      bool GetAdgaMultiLevelDone() const { return mAdgaMultiLevelDone; }
      void SetAdgaMultiLevelDone(bool aB) { mAdgaMultiLevelDone = aB; }

      //! Toggle whether VSCF dumps modals under a special file name for later use by MatRep calc.
      void SetDumpModalsForMatRep(bool a) {mDumpModalsForMatRep = a;}

      //! Should VSCF dump modals for later use by MatRep calc.?
      bool DumpModalsForMatRep() const {return mDumpModalsForMatRep;}

      //! Generate file label used for dumping modals to disk for later use by MatRep calc.
      static std::string DumpModalsForMatRepFileLabel(const std::string& arVscfPrefix);

      //! The file label used for dumping modals to disk for later use by MatRep calc.
      std::string DumpModalsForMatRepFileLabel() const {return DumpModalsForMatRepFileLabel(this->Prefix());}

};

bool VscfCalcDefLess(const VscfCalcDef& arDef1, const  VscfCalcDef& arDef2);
void  ExtractPropsAndSaveOnFiles(vector<VscfCalcDef*> apVscfCalcDefVec,string aLabel);
            // Declare once more since a friend decl. is only a friend.
//
#endif
