/**
************************************************************************
* 
* @file                IntermediateRestrictionsInput.cc
*
* 
* @date                07-11-2016
*
* @author              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief               Input for IntermediateRestrictions
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <tuple> // for pair
#include <set>
#include <type_traits>

// midas headers
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "libmda/util/any_type.h"

#include "vcc/v3/IntermediateRestrictions.h"
#include "input/IntermediateRestrictionsInput.h"


/**
 * Input for IntermediateRestrictions for IntermediateMachine in VCC transformer
 *
 * @param Minp           Input file stream.
 * @param s              Currently read string.
 * @param restrictions   IntermediateRestrictions struct for holding the input information
 * @return               Return whether s should be processed by calling function, 
 *                       i.e. has already beed read, but not processed.
 **/
bool IntermediateRestrictionsInput
   (  std::istream& Minp
   ,  std::string& s
   ,  IntermediateRestrictions& restrictions
   )
{
   // Enum/map with input flags
   enum INPUT {   ERROR, MINDOWN, MINFORWARD, MAXFORWARD
              };

   const map<string,INPUT> input_word =
   {  {"#4MAXFORWARD", MAXFORWARD}
   ,  {"#4MINFORWARD", MINFORWARD}
   ,  {"#4MINDOWN", MINDOWN}
   };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s, 4)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word, s);

      switch(input)
      {
         case MINDOWN:
         {
            midas::input::GetLine(Minp, s);
            restrictions.mMinDown = midas::util::FromString<In>(s);
            break;
         }
         case MINFORWARD:
         {
            midas::input::GetLine(Minp, s);
            restrictions.mForward.first = midas::util::FromString<In>(s);
            break;
         }
         case MAXFORWARD:
         {
            midas::input::GetLine(Minp, s);
            restrictions.mForward.second = midas::util::FromString<In>(s);
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a IntermediateRestrictions input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // we have read, but not processed next keyword
   return true;
}
