/**
************************************************************************
* 
* @file                FalconInput.cc
*
* 
* Created:             05-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the iterative mergin of subsystems 
* 
* Last modified:       07-07-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include "input/Input.h"
#include "input/ModSysCalcDef.h"
#include "input/FalconCalcDef.h"
//#include "mpi/Interface.h"

string FreqAnaInput (std::istream& Minp, input::FreqAnaCalcDef*  apCalcDef, const In& arInputLevel);
string RotCoordInput(std::istream& Minp, input::RotCoordCalcDef* apCalcDef, const In& arInputLevel);


string FalconInput(std::istream& Minp,input::FalconCalcDef* apCalcDef ,const In& arInputLevel)
{

   string s;//To be returned when we are done reading this level
   In input_level = arInputLevel+I_1;
   enum INPUT {ERROR,CALCALLSIGMAS,COUPLING,DEGENERACY,FUSIONINACTIVE,MAXDISTFORCONN,
          MODEOPTTYPE, HESSTYPE,NOCAPPING,
          MAXSUBSYSFORFULLADD,MAXSUBSYSFORRELAX,MAXSUBSYSFORFUSION,
          MINCOUPLINGFORFULLADD,MINCOUPLINGFORRELAX, MINCOUPLINGFORFUSION,
          PREPRUN, RELAXALL, RESTART, ROTATEALL, WRITEINCRINPUT,
          PROJECTINCR}; 
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"CALCALLSIGMAS",CALCALLSIGMAS},
      {"#"+std::to_string(input_level)+"COUPLING",COUPLING},
      {"#"+std::to_string(input_level)+"DEGENERACY",DEGENERACY},
      {"#"+std::to_string(input_level)+"FUSIONINACTIVE",FUSIONINACTIVE},
      {"#"+std::to_string(input_level)+"NOCAPPING",NOCAPPING},
      {"#"+std::to_string(input_level)+"MAXDISTFORCONN",MAXDISTFORCONN},
      {"#"+std::to_string(input_level)+"MAXSUBSYSFORFULLADD",MAXSUBSYSFORFULLADD},
      {"#"+std::to_string(input_level)+"MAXSUBSYSFORRELAX",MAXSUBSYSFORRELAX},
      {"#"+std::to_string(input_level)+"MAXSUBSYSFORFUSION",MAXSUBSYSFORFUSION},
      {"#"+std::to_string(input_level)+"MINCOUPLINGFORRELAX",MINCOUPLINGFORRELAX},
      {"#"+std::to_string(input_level)+"MINCOUPLINGFORFULLADD",MINCOUPLINGFORFULLADD},
      {"#"+std::to_string(input_level)+"MINCOUPLINGFORFUSION",MINCOUPLINGFORFUSION},
      {"#"+std::to_string(input_level)+"MODEOPTTYPE",MODEOPTTYPE},
      {"#"+std::to_string(input_level)+"HESSTYPE",HESSTYPE},
      {"#"+std::to_string(input_level)+"PREPRUN",PREPRUN},
      {"#"+std::to_string(input_level)+"RELAXALL",RELAXALL},
      {"#"+std::to_string(input_level)+"RESTART",RESTART},
      {"#"+std::to_string(input_level)+"ROTATEALL",ROTATEALL},
      {"#"+std::to_string(input_level)+"WRITEINCRINPUT",WRITEINCRINPUT},
      {"#"+std::to_string(input_level)+"PROJECTINCR",PROJECTINCR},
   };


   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      switch(input)
      {
         case CALCALLSIGMAS:
         {
            apCalcDef->SetCalcAllSigmas(true);
            break;
         }
         case COUPLING:
         {
            getline(Minp,s);
            apCalcDef->SetCoupEstType(midas::input::ParseInput(s));
            break;
         }
         case DEGENERACY:
         {
            getline(Minp,s);
            apCalcDef->SetDegThresh(midas::util::FromString<Nb>(s));
            break;
         }
         case FUSIONINACTIVE:
         {
            apCalcDef->SetFusionInactive(true);
            break;
         }
         case NOCAPPING:
         {
            apCalcDef->SetCapping(false);
            break;
         }
         case MAXDISTFORCONN:
         {
            getline(Minp,s);
            apCalcDef->SetMaxDistForConn(midas::util::FromString<Nb>(s));
            break;
         }
         case MODEOPTTYPE:
         {
            getline(Minp,s);
            transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
            apCalcDef->SetModeOptType(s);

            if (apCalcDef->GetModeOptType()==input::ModeOptType::ROTCOORD)
            {
               s=RotCoordInput(Minp, apCalcDef->GetpRotCoordCalcDef(), input_level);
               bool allowed = true;
               if (apCalcDef->GetpRotCoordCalcDef()->ContainsVscf())
               {
                  apCalcDef->GetpRotCoordCalcDef()->GetVscfCalcDef().SetOperFile("tmp.mop");
                  if  (apCalcDef->GetpRotCoordCalcDef()->GetVscfCalcDef().GetCalcType()!=input::RotCoordVscfCalcType::HARMONICGROUNDSTATE)
                     allowed=false;
               }
               MidasAssert(allowed,"RotCoord Input on ModeOptType not allowed -- maybe VSCF??");
               already_read=true;
            }
            break;
         }
         case HESSTYPE:
         {
            getline(Minp,s);
            transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
            apCalcDef->SetHessType(s);
            if (apCalcDef->GetHessType()==input::HessType::CALC)
            {
               s=FreqAnaInput(Minp, apCalcDef->GetpFreqAnaCalcDef(), input_level);
               already_read=true;
            }
            break;
         }
         case MAXSUBSYSFORFULLADD:
         {
            getline(Minp,s);
            apCalcDef->SetMaxSubSysForFullAdd(midas::util::FromString<In>(s));
            break;
         }
         case MAXSUBSYSFORRELAX:
         {
            getline(Minp,s);
            apCalcDef->SetMaxSubSysForRelax(midas::util::FromString<In>(s));
            break;
         }
         case MAXSUBSYSFORFUSION:
         {
            getline(Minp,s);
            apCalcDef->SetMaxSubSysForFusion(midas::util::FromString<In>(s));
            break;
         }
         case MINCOUPLINGFORRELAX:
         {
            getline(Minp,s);
            apCalcDef->SetMinCouplingForRelax(midas::util::FromString<Nb>(s));
            break;
         }
         case MINCOUPLINGFORFULLADD:
         {
            getline(Minp,s);
            apCalcDef->SetMinCouplingForFullAdd(midas::util::FromString<Nb>(s));
            break;
         }
         case MINCOUPLINGFORFUSION:
         {
            getline(Minp,s);
            apCalcDef->SetMinCouplingForFusion(midas::util::FromString<Nb>(s));
            break;
         }
         case PREPRUN:
         {
            apCalcDef->SetPrepRun(true);
            break;
         }
         case RELAXALL:
         {
            apCalcDef->SetRelaxAll(true);
            break;
         }
         case RESTART:
         {
            apCalcDef->SetRestart(true);
            break;
         }
         case ROTATEALL:
         {
            apCalcDef->SetRotateAll(true);
            break;
         }
         case WRITEINCRINPUT:
         {
            getline(Minp,s);
            vector<string> inp_line = midas::util::StringVectorFromString(s);
            MidasAssert((inp_line.size()>I_0 && inp_line.size() <=I_3)," Number of Incremental inputs wrong  ");
            apCalcDef->SetMaxIncrLevel(midas::util::FromString<In>(inp_line.at(I_0)));
            if (inp_line.size()>=I_2)
               apCalcDef->SetMaxIncrDist(midas::util::FromString<Nb>(inp_line.at(I_1)));
            if (inp_line.size()==I_3)
               apCalcDef->SetIncrBondDist(midas::util::FromString<Nb>(inp_line.at(I_2)));
            apCalcDef->SetAddRotToAux(true);
            break;
         }
         case PROJECTINCR:
         {
            apCalcDef->SetProj(true);
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a Falcon level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   //if (midas::mpi::get_Interface().GetMpiNrProc() > I_1 && (apCalcDef->GetpFreqAnaCalcDef()->GetNumNodes()!=I_1 || apCalcDef->GetpFreqAnaCalcDef()->GetProcPerNode()!=I_1))
   //{
   //   Mout << " The parallelization of the single points is NOT MPI-based, please run without MPI " << endl;
   //   MIDASERROR("It is not possible to combine MPI, with the parallelization of single points");
   //}
   return s;
}


