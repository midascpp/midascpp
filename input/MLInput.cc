/**
************************************************************************
* 
* @file                MLInput.cc
*
* 
* Created:             03-07-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Input reader for Machine Learning tasks 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/MLCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"

using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

/**
* Read and check MLTask input.
* Read until next s = "#i..." i<3 which is returned.
* */
std::string MLInput(std::istream& Minp)
{

   gDoMLTask  = true; 
   gMLCalcDef.push_back(MLCalcDef());
   MLCalcDef& new_mltask = gMLCalcDef[gMLCalcDef.size()-1];  // new_oper is a reference to the new operator

   enum INPUT {ERROR, IOLEVEL, NAME, DATABASE, COORDFILE, SAMPLE, GPR, KMEANS, GAUMIXMOD, 
               ONLYICOORD, SIMCHECK, COORDTYPE, READCOVAR, STORECOVAR, KERNEL, NOISE, SOAP, 
               LVALUE, HOPT, PREDICTMODE, COVARALGO, READICOORDDEF, DUMPICOORDDEF, MEANFUNCTION,
               MAXITER, GRADEPS, FUNCEPS, HYPERPARAM, USENODENSITY, TOLSELECT, MAXMCADD, 
               GPRSHIFT, INDUCINGPOINTS, READWEIGHTS, STOREWEIGHTS, NORMALIZEKERNEL, SCALESIGMA2, 
               TRAIN, ADAPTNOISE, SORTSIGMASDESCENDING, USESIGMA, PRTCONDNUM, USEREGINV };
   const map<string,INPUT> input_word =
   {
      {"#2IOLEVEL",IOLEVEL}                 ,
      {"#2NAME",NAME}                       ,
      {"#2DATABASE",DATABASE}               ,
      {"#2COORDFILE",COORDFILE}             ,
      {"#2GPR",GPR}                         ,
      {"#2KMEANS",KMEANS}                   ,
      {"#2GAUMIXMOD",GAUMIXMOD}             ,
      {"#2ONLYICOORD",ONLYICOORD}           ,
      {"#2CHECKSIM",SIMCHECK}               ,
      {"#2COORDTYPE",COORDTYPE}             ,
      {"#2READCOVAR",READCOVAR}             ,
      {"#2STORECOVAR",STORECOVAR}           ,        
      {"#2KERNEL",KERNEL}                   ,       
      {"#2NOISE",NOISE}                     , 
      {"#2SOAP",SOAP}                       ,
      {"#2LVALUE",LVALUE}                   ,     
      {"#2HOPT",HOPT}                       , 
      {"#2PREDICTMODE",PREDICTMODE}         ,
      {"#2COVARALGO", COVARALGO}            ,     
      {"#2READICOORDDEF", READICOORDDEF}    ,   
      {"#2DUMPICOORDDEF", DUMPICOORDDEF}    ,
      {"#2HYPERPARAM", HYPERPARAM}          , 
      {"#2MEANFUNCTION", MEANFUNCTION}      ,
      {"#2MAXITER", MAXITER}                ,
      {"#2GRADEPS", GRADEPS}                ,
      {"#2FUNCEPS", FUNCEPS}                ,          
      {"#2USENODENSITY", USENODENSITY}      ,          
      {"#2TOLSELECT", TOLSELECT}            ,
      {"#2MAXMCADD", MAXMCADD}              ,
      {"#2GPRSHIFT", GPRSHIFT}              , 
      {"#2INDUCINGPOINTS", INDUCINGPOINTS}  ,  
      {"#2READWEIGHTS",READWEIGHTS}         ,
      {"#2STOREWEIGHTS",STOREWEIGHTS}       ,        
      {"#2NORMALIZEKERNEL",NORMALIZEKERNEL} ,             
      {"#2SCALESIGMA2",SCALESIGMA2}         ,          
      {"#2SAMPLE",SAMPLE}                   ,            
      {"#2TRAIN",TRAIN}                     ,                     
      {"#2ADAPTNOISE",ADAPTNOISE}           ,      
      {"#2SORTSIGMASDESCENDING",SORTSIGMASDESCENDING}   ,          
      {"#2USESIGMA",USESIGMA}               ,
      {"#2PRTCONDNUM",PRTCONDNUM}           ,
      {"#2USEREGINV",USEREGINV}                 
   };

   const map<string,CoordType> input_coord =
   {
      {"XYZ",XYZ}             ,
      {"ZMAT",ZMAT}           ,
      {"XYZCENTER",XYZCENTER} ,
      {"MINT",MINT}           ,
      {"DIST",DIST}           ,      
      {"EIGDIST",EIGDIST}     ,   
      {"INV",INV}             ,      
      {"SYMG2",SYMG2}         ,      
      {"SYMG4",SYMG4}        
   };


   string s="";
   In input_level=2;
   bool already_read = false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read = false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);       // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);             // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                          // Get new line with input
            new_mltask.SetIoLevel(midas::util::FromString<In>(s));
            break;
         } 
         case NAME:
         {
            getline(Minp,s);                                          // Get new line with input
            istringstream input_s(s);
            string s_name="";
            input_s >> s_name;
            new_mltask.SetName(s_name);
            for (In i_calc =0; i_calc < gMLCalcDef.size() - 1; i_calc++)
               if (gMLCalcDef[i_calc].GetName() == new_mltask.GetName()) 
                  MIDASERROR("name for geometry optimization is already in use");
            break;
         }
         case DATABASE:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            new_mltask.SetDatabase(midas::util::FromString<std::string>(s));

            break;
         }
         case COORDFILE:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            new_mltask.SetCoordFile(midas::util::FromString<std::string>(s));
            break; 
         }
         case USENODENSITY:
         {
            new_mltask.SetUseNoDensity(true);
            break;
         }
         case GPR:
         {
            bool add = true;
            new_mltask.SetCompModeGauPro(add);
            break;
         }
         case SOAP:
         {
            bool add = true;
            new_mltask.SetCompModeSOAP(add);
            break;
         }
         case LVALUE:
         {
            midas::input::GetLine(Minp, s);
            new_mltask.SetLmax(midas::util::FromString<In>(s));
            break;
         }
         case KMEANS:
         {
            // get number of centers 
            midas::input::GetLine(Minp, s);
            In ncenter = midas::util::FromString<In>(s);
            new_mltask.SetKmeans(ncenter);      
            break;
         }
         case GAUMIXMOD:
         {
            // get number of centers 
            midas::input::GetLine(Minp, s);
            In ncenter = midas::util::FromString<In>(s);
            new_mltask.SetGauMixMod(ncenter);      
            break;
         }
         case SAMPLE:
         {
            // get number of centers 
            midas::input::GetLine(Minp, s);
            In ncenter = midas::util::FromString<In>(s);
            new_mltask.SetNumberOfSamples(ncenter);     
            new_mltask.SetCompModeSample(true); 
            break;
         }
         case ONLYICOORD:
         {
            bool add = true;
            new_mltask.SetOnlyCoordAna(add);
            break;
         }
         case SIMCHECK:
         {
            bool add = true;
            new_mltask.SetOnlySimCheck(add);
            break;
         }
         case COORDTYPE:
         {
            midas::input::GetLine(Minp, s);
            std::string stype = midas::util::FromString<std::string>(s);

            transform(stype.begin(),stype.end(),stype.begin(),(In(*) (In))toupper);       // Transform to all upper.
            while(stype.find(" ")!= stype.npos) stype.erase(stype.find(" "),I_1);         // Delete ALL blanks
            //CoordType coordtype = midas::input::FindKeyword(input_coord, stype);
            CoordType coordtype = input_coord.at(stype);  

            new_mltask.SetCoordinateType(coordtype); 

            break;
         }
         case READCOVAR:
         {
            // get path to file
            bool readcovar = true;
            new_mltask.SetReadCovar(readcovar);
            midas::input::GetLine(Minp, s);
            std::string scovar = midas::util::FromString<std::string>(s);
            new_mltask.SetFileCovarRead(scovar);
            break; 
         }
         case STORECOVAR:
         {
            // get path to file
            bool storecovar = true;
            new_mltask.SetSaveCovar(storecovar);
            midas::input::GetLine(Minp, s);
            std::string scovar = midas::util::FromString<std::string>(s);
            new_mltask.SetFileCovarSave(scovar);
            break; 
         }
         case READWEIGHTS:
         {
            // get path to file
            bool readweights = true;
            new_mltask.SetReadWeights(readweights);
            midas::input::GetLine(Minp, s);
            std::string sweights = midas::util::FromString<std::string>(s);
            new_mltask.SetFileWeightsRead(sweights);
            break; 
         }
         case STOREWEIGHTS:
         {
            // get path to file
            bool storeweights = true;
            new_mltask.SetSaveWeights(storeweights);
            midas::input::GetLine(Minp, s);
            std::string sweights = midas::util::FromString<std::string>(s);
            new_mltask.SetFileWeightsSave(sweights);
            break; 
         }
         case KERNEL:
         {
            while(midas::input::GetLine(Minp,s) && !midas::input::IsKeyword(s))
            {
               std::string s_kern = midas::util::FromString<std::string>(s);

               transform(s_kern.begin(),s_kern.end(),s_kern.begin(),(In(*) (In))toupper); 

               // Sanity Check(s)
               std::vector<std::string> list = { "OUEXP", "MATERN32", "MATERN52", "SQEXP", "SC32", "SC52"
                                               , "POLYNOM", "PERIODIC", "SCEXP", "OVLP", "RBF", "RATQUAD"  };
               bool found = (std::find(list.begin(), list.end(), s_kern) != list.end());
  
               if (!found)
               {
                  Mout << "Kernel " << s_kern << " unknown!" << std::endl;
                  MIDASERROR(" Check your input please ");
               }

               new_mltask.AddKernel(s_kern);

               already_read = true;
            }

            // get path to file
            //midas::input::GetLine(Minp, s);
            //string s_kern = midas::util::FromString<std::string>(s);

            break; 
         }
         case NORMALIZEKERNEL:
         {
            new_mltask.SetmNormalizeKernel(true);
            break;
         }
         case ADAPTNOISE:
         {
            new_mltask.SetmAdaptNoise(true);
            break;
         }
         case NOISE:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            Nb noise = midas::util::FromString<Nb>(s);
            new_mltask.SetNoise(noise);

            break;
         }
         case HOPT:
         {
            getline(Minp,s);
            std::transform(s.begin(), s.end(), s.begin(), ::tolower);
            istringstream input(s);
            bool doopt = true;
            input >> std::boolalpha >> doopt;

            new_mltask.SetDoHOpt(doopt);         

            break;
         }
         case TRAIN:
         {
            getline(Minp,s);
            std::transform(s.begin(), s.end(), s.begin(), ::tolower);
            istringstream input(s);
            bool doopt = true;
            input >> std::boolalpha >> doopt;

            new_mltask.SetDoTrain(doopt);         

            break;
         }
         case MAXITER:
         {
            midas::input::GetLine(Minp, s);
            In maxiter = midas::util::FromString<In>(s);
            new_mltask.SetMaxIter(maxiter);
            break;
         }
         case GRADEPS:
         {
            midas::input::GetLine(Minp, s);
            Nb gradeps = midas::util::FromString<Nb>(s);
            new_mltask.SetGradEps(gradeps);
            break;
         }
         case FUNCEPS:
         {
            midas::input::GetLine(Minp, s);
            Nb funceps = midas::util::FromString<Nb>(s);
            new_mltask.SetFuncEps(funceps);
            break;
         }
         case PREDICTMODE:
         {
            midas::input::GetLine(Minp, s);
            In predictmode = midas::util::FromString<In>(s);
            new_mltask.SetPredictMode(predictmode);
            break;
         }
         case COVARALGO:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            std::string algo = midas::util::FromString<std::string>(s);
            new_mltask.SetCoVarAlgo(algo);
            break; 
         }
         case READICOORDDEF:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            std::string ficoord = midas::util::FromString<std::string>(s);
            new_mltask.SetIcoordDefFile(ficoord);
            bool yes = true;
            new_mltask.SetUseIcoordDefintion(yes);
            break; 
         }
         case DUMPICOORDDEF:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            std::string ficoord = midas::util::FromString<std::string>(s);
            new_mltask.SetDumpIcoordDefFile(ficoord);

            bool yes = true;
            new_mltask.SetDumpIcoordDef(yes);
            break; 
         }
         case HYPERPARAM:
         {
            // get path to file
            midas::input::GetLine(Minp, s);
            std::string fhparam = midas::util::FromString<std::string>(s);
            new_mltask.SetFileHyperParam(fhparam);

            break;
         }
         case MEANFUNCTION:
         {
            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            std::string sfunc;
            input_sfile >> sfunc;
            std::transform(sfunc.begin(), sfunc.end(),sfunc.begin(), ::toupper);
            if (sfunc == "ZERO")
            {
               new_mltask.SetMeanFunction(sfunc);
            }
            else if (sfunc == "HESSIAN")
            {
               getline(Minp,s);
               istringstream input_sfile(s);
               string s_file="";
               input_sfile >> s_file;

               std::vector<std::string> files;
               files.push_back(s_file);

               new_mltask.SetMeanFunction(sfunc);
               new_mltask.SetMeanFunctionFiles(files);
            }
            else
            {
               MIDASERROR("Unknown mean function for GPR!");
            }

            break;

         }
         case TOLSELECT:
         {
            midas::input::GetLine(Minp, s);
            Nb tol = midas::util::FromString<Nb>(s);
            new_mltask.SetTolSelect(tol);
            break;
         }
         case MAXMCADD:
         {
            midas::input::GetLine(Minp, s);
            In max_mc_add = midas::util::FromString<In>(s);
            new_mltask.SetMaxModeCombAdd(max_mc_add);
            break;
         }
         case GPRSHIFT:
         {
            midas::input::GetLine(Minp, s);
            Nb tol = midas::util::FromString<Nb>(s);
            new_mltask.SetGPRShift(tol);
            break;
         }
         case INDUCINGPOINTS:
         {
            midas::input::GetLine(Minp, s);
            In max_mc_add = midas::util::FromString<In>(s);
            new_mltask.SetNumInducingPoints(max_mc_add);
            break;
         }
         case SCALESIGMA2:
         {
            new_mltask.SetScaleSigma2(true);
            break;
         }
         case SORTSIGMASDESCENDING:
         {
            new_mltask.SetSortSigmaDescending(true);
            break;
         }
         case USESIGMA:
         {
            new_mltask.SetUseVariance(false);
            break;
         }
         case PRTCONDNUM:
         {
            new_mltask.SetPrintConditionNumber(true);
            break;
         }
         case USEREGINV:
         {
            midas::input::GetLine(Minp, s);
            Nb eps = midas::util::FromString<Nb>(s);
            new_mltask.SetmUseRegInv(true);
            new_mltask.SetmRegEps(eps);
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a MLTask level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}

/**
* Initialize MLTask variables
* */
void InitializeMLTask()
{
   Mout << " Initializing Machine Learning interface variables: " << endl;
   gDoMLTask  = false; 
}




