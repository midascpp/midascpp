#ifndef BASISINPUT_H_INCLUDED
#define BASISINPUT_H_INCLUDED

#include <string>

//! Read in basis input
std::string BasisInput(std::istream& Minp);

#endif /* BASISINPUT_H_INCLUDED */
