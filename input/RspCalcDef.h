/**
************************************************************************
* 
* @file                RspCalcDef.h
*
* Created:             19-04-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Base class for a Vib rsp. input-info handler 
* 
* Last modified: Sun May 16, 2010  03:05PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RSPCALCDEF_H_INCLUDED
#define RSPCALCDEF_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/RspFunc.h"
#include "input/RspVecInf.h"
#include "util/Io.h"
#include "vcc/LanczosIRspect.h"
#include "input/TotalResponseDrv.h"
#include "input/TensorDecompDef.h"
#include "vcc/BandLanczosIRspect.h"
#include "vcc/BandLanczosRamanSpect.h"
#include "tensor/TensorDecompInfo.h"

// using declarations
using std::string;
using std::vector;
using std::map;

// forward declarations
class LanczosRspFunc;
class BandLanczosRspFunc;

enum class RSPEIGTYPE: int { STD = 0, LEFT, DECOMP, TENSOR };
enum class COMPLEXSOLVER: int { STANDARD = 0, IMPROVED, SUBSPACE, ERROR };
enum class VccTensorType: int { SIMPLE = 0, CANONICAL, ERROR };

/**
* Construct a definition of a response calculation calculation
* */
class RspCalcDef
{
   private:
      bool   mDoRsp;                 ///< Do response
      bool   mDoRspEig;              ///< Do response eigen values 
      bool   mDoRspFuncs;            ///< Do response functions 
      bool   mDoNoPaired;            ///< Do Rsp Func. in no-paired fashion, f.ex. Vcis as Vscf
      In     mRspNeig;               ///< Nr of rsponse eigenvalues requested. 
      bool   mRspLeftEig = false;            ///< Get left eigenvalues instead of right eigenvalues.
      string mRspDiagMeth;           ///< Method of diagonalization
      In     mRspIoLevel;            ///< Io level for response calculation.
      //Nb     mRspEigResidThr;        ///< Threshold on residual response eigenvectors vectors. (Not used. -MBH, Feb 2017) 
      In     mRspItEqNiterMax;       ///< Maximum Number of iterations for rsp it eq. solver 
      In     mRspRedDimMax;          ///< maximum dim of reduced space 
      In     mRspNbreakDim;          ///< Dim. for breaking and starting anew in eq.solv.
      Nb     mRspItEqResidThr;       ///< Threshold (residual norm) for It eq solver.
      Nb     mRspItEqResidThrRel = 1e-4; ///< Threshold (residual relative norm) for It eq solver.
      In     mRspItEqRescueMax = 0;         ///< The number of vectors that should be added to the trial space, if all other vectors have been discarded.
      Nb     mRspItEqEnerThr;        ///< Threshold (energy change) for It eq solver.
      Nb     mRspItEqSolThr = 1;     ///< Threshold (solution vector relative change) for It eq solver.
      Nb     mRspPreconItEqResidThr = 1e-4; ///< Threshold (residual norm) for It eq solver.
      Nb     mRspPreconItEqSolThr = 1;      ///< Threshold (solution vector relative change) for It eq solver.
      Nb     mRspPreconItEqMaxIter = 400;   ///< max iterations in improved precon
      In     mRspOrthoMax = 10;             ///< maximum iterations in iterative orthogonalizer
      Nb     mRspThreshOrthoDiscard = 1e-13;
      Nb     mRspThreshOrthoRetry = 1e-4;
      bool   mRspComplexImprovedPrecond = false;
      In     mRspComplexImprovedPrecondExciLevel = 0;
      bool   mRspComplexImprovedPrecondMute = true;
      bool   mRspComplexImprovedPrecondExplicit = false;
      bool   mRspItEqCheckLinearDependenceOfSpace = false;
      bool   mRspRestart;            ///< Restart rsp
      bool   mRspUseAvailable;       ///< Restart available data even though not complete...(VCC2->VCC3,f.ex).
      bool   mRspMapPrevVecs;        ///< Restart from previous calc.: maps ``any'' size of bas^{prev}, and any level of VCC^{prev}.
      bool   mRspDiis;               ///< Solve by Diis iteration accel.
      In     mRspMaxDiis;            ///< Max. number of vectors included in extrapol.
      bool   mRspPreDiag;            ///< Solve eq. using prediagonalization.
      bool   mRspTimeIt;             ///< Time per iteration
      vector<RspFunc> mRspFuncs;     ///< The response function
      //vector<string>  mRspOpers;   ///< Vector of response operator names 
      In     mNrspOps;               ///< The nr. of response operators 
      map<string,In>  mRspOpers;     ///< Response operators and nrs
      vector<Nb> mRspOpExptVals;     ///< Vector responses operators expt. values
      vector<RspVecInf> mRspVecInf;  ///< Vector of info on responses

      Nb mScreenZeroRsp;  ///< Zero threshold for screening of response intermediate op
      Nb mScreenZeroRspC; ///< Zero threshold for screening of response intermediate op

      Nb mSosFreqBegin;  ///< Sos frequencies from [mSosFreqBegin,mSosFreqEnd] in mSosFreqSteps
      Nb mSosFreqEnd;    ///< Sos frequencies from [mSosFreqBegin,mSosFreqEnd] in mSosFreqSteps
      In mSosFreqSteps;  ///< Sos frequencies from [mSosFreqBegin,mSosFreqEnd] in mSosFreqSteps

      Nb mQrfFrq1;        ///< Sos frequency 1 for quadratic RF
      Nb mQrfFrq2;      ///< Sos frequency 2 for quadratic RF
      vector<vector<Nb> > mCrfFrq;      ///< Sos frequency 2 for quadratic RF
      
      bool mRspTargetSpace;               ///< Use a target space 
      bool mRspDoAllFundamentals;         ///< Calculate all fundamentals by target.
      bool mRspDoAllFirstOvertones;       ///< Calculate all first overtones by target.
      bool mRspDoFundamentals;            ///< Some fundamentals by target.
      bool mRspDoFirstOvertones;          ///< Some first overtones by target.
      vector<In> mRspFundamentals;        ///< Rsp fundamentals 
      vector<In> mRspFirstOvertones;      ///< Rsp first overtones 
      Nb mNumVccJacobian;                 ///< If not zero, calculate VCC Jacobian numerically
                                          ///< using value as step size.
      Nb mDamp;                               ///< Damping factor to handle finite lifetimes
      vector<TotalResponseDrv*> mTotalRsp; ///< Do total response?
      In mNApproxRspFunc;               ///< No. of approximate rsp funcs.
      Nb mNumVccF;                        ///< If not zero, calculate VCC response F matrix using 
                                          ///< finitw difference method with step size mNumVccF.
      bool mSpecialQrfSosFrqs;
      bool mSpecialCrfSosFrqs;
      
      bool mOlsen = true;        ///< Use Olsen algorithm 
      bool mMinimumResidual = false; ///< Use minimum-residual method for linear equations...
      Nb mOlsenDecompRelThresh = C_M_1;   ///< threshold for recompression of solution vector before Olsen update
      bool   mRspTrueHDiag;               ///< Use true H diagonal transformer
      bool   mRspTrueADiag;               ///< Use true diagonal of the A matrix in the transformer
      bool   mRspImprovedPrecond;         ///< Solve eq. to obtain a better preconditioner in Olsen.
      In mRspPrecondExciLevel;             ///< Max. number of mode couplings included in the precond. Hamiltonian

      //! Use E_VCC as ref instead of E_VMP0 when calculating the VCC Jacobian diagonal
      bool   mRspUseVccRefEnergy = false;

      In mRspNresvecs;                     ///< Number of start vectors in restart
      string mRspLevel2Solver;          ///< Method for solving the linear equations in improved precond 
      string mRspTargetingMethod;          ///< Converge solutions having large overlap with the targets
      Nb mRspOverlapMin;                   ///< Selection threshold to retain important solutions
      Nb mRspOverlapSumMin = static_cast<Nb>(0.8);                ///< Selection threshold to retain important solutions
      In mRspOverlapNMax = I_100;                 ///< Max important vectors for OVERLAPSUM targeting
       Nb mRspEnergyDiffMax;           ///< Energy window to retain important solutions +-E_{target}
      Nb mRspResidThrForOthers;           ///< Factor to change the threshold for residual norm for extra states in It eq solver.
      Nb mRspEnerThrForOthers;           ///< Factor to change the threshold for energy change for extra states in It eq solver.
      bool mRspEigValSeqSet;        ///< Take only a limited number of new vectors
      In   mRspEigValSeq;           ///< The number of new solved at a time
      vector<Nb> mAsymFreqs;        ///< Frequencies for use in estimating the asym. lin. RF
      bool mAlsoLinearAsym;                ///< Estimate linear asym?
      bool mRspHarmonicRR;          ///< Use Harmonic Rayleigh-Ritz procedure to solve EigVal problems
      Nb mRspEnerShift = C_0;             ///< Energy shift (sigma) used for (A-sigma) in Harmonic RR

      //! For TensorDataCont eig solver, use mEnerShift in Davidson/Olsen preconditioner (NB: this is always done in the old solver)
      bool mRspUseEnerShiftInPrecon = false;

      //! Adapt the shift to the reduced eigenvalues that are not yet converged.
      bool mRspAdaptiveHarmonicRrShift = false;

      //! Refine the harmonic-RR eigenvectors
      bool mRspRefinedHarmonicRr = false;

      //! Use the Rayleigh quotients as eigval approximations if using harmonic Rayleigh-Ritz
      bool mRspUseRayleighQuotientsAsEigvals = false;

      //! Disable diagonal preconditioner for TensorDataCont linear-equation solver
      bool mTensorRspNoLinSolverPrecon = false;

      //! Use tensor framework for computing response functions
      bool mTensorRspFuncs = false;


      vector<LanczosRspFunc*> mLanczosRspFuncs;
      vector<LanczosIRspect*> mLanczosIRspects;

      vector<LanczosRspFunc*> mBandLanczosRspFuncs;
      vector<BandLanczosIRspect*> mBandLanczosIRspects;
      vector<BandLanczosRamanSpect*> mBandLanczosRamanSpects;
      
      bool                    mDoTensorDecomp; ///< do tensor decomp or not
      bool                    mDoTensorRspBenchmarkDecomp; ///< do tensor decomp or not
      vector<TensorDecompDef> mTensorDecompDefs; ///< input data for decompositions

      /** BEGIN TensorDataCont eig solver info **/
      //! Convert units to cm^-1 in it_solver
      bool mTensorRspSolverConvertUnits = false;

      //! Solve equations again after convergence to check that they are really converged (for CP-VCC)
      bool mReiterateTensorRspSolver = false;

      //! Recompress eigenvectors before writing to disc
      bool mRecompressCpvccEigVecs = true;

      //! Screening for Axpy%s in orthogonalization with CP tensors
      Nb mTensorRspOrthoScreening = static_cast<Nb>(1.e-20);

      //! TensorDecompInfo::Set for TensorDataCont eig solver
      midas::tensor::TensorDecompInfo::Set mVccRspDecompInfo;

      //! TensorDecompInfo::Set for TensorDataCont Jacobian transformer
      midas::tensor::TensorDecompInfo::Set mVccRspTrfDecompInfo;

      //! Recompress result of general down contractions before adding to tensor sum
      bool mGenDownCtrRecompressResult = true;

      //! Perform full rank analysis after TensorDataCont rsp calc
      bool mFullRspRankAnalysis = false;

      //! Perform rank analysis after TensorDatacont rsp calc
      bool mRspRankAnalysis = false;

      //! Balance preconditioned tensors in TensorDataContEigenvalueSolver
      bool mTensorVccRspEigBalanceAfterPrecon = false;

      //! Recompress tensors after preconditioning
      bool mTensorVccRspEigRecompAfterPrecon = false;

      //! Use absolute-valued preconditioner
      bool mTensorVccRspEigAbsPrecon = false;

      //! Check result of VccTransJac with TensorDataCont compared to DataCont transformation
      bool mCheckTensorVccTransJac = false;

      //! Use the debug result of VccTransJac as output to check later results
      bool mTensorVccJacUseDebugVector = false;

      //! TransformerAllowedRank for rsp transformer
      In mRspTransformerAllowedRank = -I_2;

      //! TensorProductScreening for rsp transformer
      std::pair<Nb, Nb> mRspTensorProductScreening = std::make_pair(-C_1, -C_1);

      //! TensorProductLowRankLimit for rsp transformer
      In mRspTensorProductLowRankLimit = -I_1;

      /** END TensorDataCont eig solver info **/

      RSPEIGTYPE mRspEigType = RSPEIGTYPE::STD;
      
      bool mForceComplexRsp = false;
      bool mItEqSaveToDisc = false;
      bool mRspComplexOlsen = false;
      bool mRspRelConv = false;

      std::vector<std::string> mItEqRestartVector;
      std::vector<std::string> mItEqSubspacePreconVector;
      bool mItEqSubspacePreconFull = false;
      Nb mLambdaThreshold;

      COMPLEXSOLVER mRspComplexSolverType = COMPLEXSOLVER::STANDARD;

      bool mDoDensityMatrixAnalysis = false; 

      bool mMakeItNaMo=false;           ///< Calculate iterative natural modals 
      In   mItNaMoMaxIter=I_1;          ///< Max Iterations for Iterations natural modals 
      Nb   mItNaMoThr=C_I_10_6;         ///< Threshold for Iterations natural modals calculations 
      
      VccTensorType mVccTensorType = VccTensorType::SIMPLE;
      bool mRspUseNewContractions = false;
      
      bool mSymDensNaMo=true;           ///< Symmetrize density for iterative natural modal calculations 

   public:
      RspCalcDef();
      virtual ~RspCalcDef();
      void SetDoRsp(bool aRsp) {mDoRsp = aRsp;} ///< Set to rsp calc. 
      void SetDoRspEig(bool aRsp) {mDoRspEig = aRsp;} ///< Set to rsp calc. 
      void SetDoRspFuncs(bool aRsp) {mDoRspFuncs = aRsp;} ///< Set to rsp calc. 
      void SetDoNoPaired(bool aRsp) {mDoNoPaired= aRsp;} ///< Set to rsp calc. 
      void SetRspNeig(In aIn) {mRspNeig = aIn;} ///< Set to rsp calc. 
      void SetRspLeftEig(bool aB) 
      {
         mRspLeftEig = aB;
         //if(mRspLeftEig)
         //   mRspEigType = RSPEIGTYPE::LEFT;
         //else
         //   mRspEigType = RSPEIGTYPE::STD;
      }
      void SetRspDiagMeth(const string& arDiagMeth)
      {mRspDiagMeth=arDiagMeth;}                ///< Set the diagmeth
      void SetRspIoLevel(const In& arIoLevel)
      {mRspIoLevel =arIoLevel;}                 ///< Set the IoLevel
      void SetRspRestart(const bool& arRest)
      {mRspRestart= arRest;}
      void SetRspUseAvailable(const bool& arUseA)
      {mRspUseAvailable = arUseA;}
      ///< Set the bool for use available 
      void SetRspMapPreviousVectors(const bool& arMapV)
      {mRspMapPrevVecs = arMapV;}
      ///< Set the bool for mapping previous vectors
      void SetSosFreqs(const Nb& arSosFreqBegin, const Nb& arSosFreqEnd, const In& arSosFreqSteps)
      {mSosFreqBegin= arSosFreqBegin; mSosFreqEnd= arSosFreqEnd; mSosFreqSteps= arSosFreqSteps;}
      void SetQrfSosFreqs(const Nb& arFrq1,const Nb& arFrq2,bool aB=true)
         {mQrfFrq1=arFrq1;mQrfFrq2=arFrq2;mSpecialQrfSosFrqs=aB;}
      void SetCrfSosFreqs(const Nb& arFrq1,const Nb& arFrq2,const Nb& arFrq3, bool aB=true)
      {
         vector<Nb> tmp;
         tmp.push_back(arFrq1);
         tmp.push_back(arFrq2);
         tmp.push_back(arFrq3);
         mCrfFrq.push_back(tmp);
         mSpecialCrfSosFrqs=aB;
      }
      void SetRspItEqMaxIter(const In& arMaxIter)
      {mRspItEqNiterMax =arMaxIter;}
      ///< Set the maximem nr. of iterations
      void SetRspRedDimMax(const In& arMax)
      {mRspRedDimMax=arMax;}
      ///< Set maximum dim of reduced space 
      void SetRspRedBreakDim(const In& arMax)
      {mRspNbreakDim=arMax;}
      ///< Set Dimension for breaking and starting over in eq.solv.
      void SetRspItEqResidThr(const Nb& arThr)
      {mRspItEqResidThr  =arThr;}
      ///< SetRsp threshold for eq. solve
      void SetRspItEqResidThrRel(const Nb& arThr)
      {mRspItEqResidThrRel  =arThr;}
      ///< SetRsp threshold for eq. solve
      void SetRspItEqRescueMax(const In& arMax)
      {mRspItEqRescueMax  =arMax;}
      void SetRspItEqEnerThr(const Nb& arThr)
      {mRspItEqEnerThr  =arThr;}
      void SetRspItEqSolThr(const Nb& arThr)
      {mRspItEqSolThr  =arThr;}
      void SetRspPreconItEqResidThr(const Nb& arThr)
      {mRspPreconItEqResidThr  =arThr;}
      void SetRspPreconItEqSolThr(const Nb& arThr)
      {mRspPreconItEqSolThr  =arThr;}
      void SetRspOrthoMax(In aMax) { mRspOrthoMax = aMax; }
      void SetRspThreshOrthoDiscard(Nb aThr) { mRspThreshOrthoDiscard = aThr; }
      void SetRspThreshOrthoRetry(Nb aThr) { mRspThreshOrthoRetry = aThr; }
      ///< SetRsp threshold for eq. solve
      void SetRspDiis(const bool& arB)
      {mRspDiis     =arB;}
      ///< Set mDiis
      void SetRspMaxDiis(const In& arI)
      {mRspMaxDiis     =arI;}
      ///< Set mMaxDiis
      void SetRspTimeIt(const bool& arB)
      {mRspTimeIt =arB;}
      ///< Set mTimeIt 
      void SetRspPreDiag(bool aB) {mRspPreDiag=aB;}
      void SetRspTrueHDiag(bool aB) {mRspTrueHDiag = aB;}
      ///< Set mRspTrueHDiag
      void SetRspTrueADiag(bool aB) {mRspTrueADiag = aB;}
      ///< Set mRspTrueADiag
      void SetRspImprovedPrecond(const In& arI)
      {mRspImprovedPrecond=true; mRspPrecondExciLevel = arI;}
      void SetRspNresvecs(const In& arI){mRspNresvecs = arI;}
      void SetRspLevel2Solver(const string& arS)
      {mRspLevel2Solver=arS;}
      void AddRspFunc(RspFunc& arRspFunc) {mDoRspFuncs = true; mRspFuncs.push_back(arRspFunc);}
      void AddLanczosRspFunc(LanczosRspFunc* aFunc)    {mLanczosRspFuncs.push_back(aFunc);}
      
      In GetRspNeig() const {return mRspNeig;}        ///< Set to rsp calc. 
      bool DoRsp() const {return mDoRsp;}             ///< do rsp calc. 
      bool DoRspEig() const {return mDoRspEig;}       ///< do rsp eig. calc. 
      bool DoRspFuncs() const {return mDoRspFuncs;}   ///< Do rsp func calc. 
      bool DoNoPaired() const {return mDoNoPaired;} ///< Set to rsp calc. 
      bool RspLeftEig() const {return mRspLeftEig;}
      Nb GetSpecialCrfFrq(In aI,In aJ) const
      {
         return mCrfFrq[aI][aJ];
      }
      In GetNCrfFrq() const {return mCrfFrq.size();}
      Nb GetSpecialQrfFrq(In aI) const 
         {if(aI==0) return mQrfFrq1; else return mQrfFrq2;}
      bool SpecialQrfFrq() const {return mSpecialQrfSosFrqs;}
      Nb GetQrfSosFrq1() const {return mQrfFrq1;}
      Nb GetQrfSosFrq2() const {return mQrfFrq2;}
      In SosFreqSteps() const {return mSosFreqSteps;}
      Nb SosFreqBegin() const {return mSosFreqBegin;}
      Nb SosFreqEnd() const {return mSosFreqEnd;}
      ///< Set the bool save 
      In GetRspItEqMaxIter() const {return mRspItEqNiterMax;}
      ///< Get the maximem nr. of iterations
      In GetRspRedDimMax() const {return mRspRedDimMax;}
      ///< Get maximum dim of reduced space 
      In GetRspRedBreakDim() const {return mRspNbreakDim;}
      ///< Get Dimension for breaking and starting over in eq.solv.
      Nb GetRspItEqResidThr() const {return mRspItEqResidThr;}
      ///< Get thresholds for eq.solv.
      Nb GetRspItEqResidThrRel() const {return mRspItEqResidThrRel;}
      ///< Get thresholds for eq.solv.
      In GetRspItEqRescueMax() const {return mRspItEqRescueMax;}
      ///< Get number of max rescuevectors
      Nb GetRspItEqEnerThr() const {return mRspItEqEnerThr;}
      ///< Get thresholds for eq.solv.
      Nb GetRspItEqSolThr() const {return mRspItEqSolThr;}
      ///< Get solution change threshold for eq.solv.
      Nb GetRspPreconItEqResidThr() const {return mRspPreconItEqResidThr;}
      ///< Get solution change threshold for eq.solv.
      Nb GetRspPreconItEqSolThr() const {return mRspPreconItEqSolThr;}
      ///< Get solution change threshold for eq.solv.
      Nb GetRspPreconItEqMaxIter() const {return mRspPreconItEqMaxIter;}
      ///< Get Max iterations for improved precon
      In GetRspOrthoMax() const { return mRspOrthoMax; }
      Nb GetRspThreshOrthoDiscard() const { return mRspThreshOrthoDiscard; }
      Nb GetRspThreshOrthoRetry() const { return mRspThreshOrthoRetry; }
      In GetRspMaxDiis() const {return mRspMaxDiis;}
      ///< Get maximum vectors in diis
      bool RspDiis() const {return mRspDiis;}
      ///< is it a Diis?
      bool RspPreDiag() const {return mRspPreDiag;}
      bool RspTimeIt() const {return mRspTimeIt;}

      string RspDiagMeth() const {return mRspDiagMeth;}
      ///< Return the Diag method
      In RspIoLevel() const {return mRspIoLevel;}
      ///< GetRsp the iolevel 
      bool RspRestart() const {return mRspRestart;} 
      ///< Restart from previous data 
      bool GetRspUseAvailable() const {return mRspUseAvailable;} 
      ///< Restart from previous data though not complete
      bool GetRspMapPreviousVectors() const {return mRspMapPrevVecs;} 
      ///< Restart from previous smaller/larger vectors
      //
      In NlanczosRspFuncs() const                      {return mLanczosRspFuncs.size();}
      LanczosRspFunc* GetLanczosRspFunc(In aIdx) const {return mLanczosRspFuncs[aIdx];}
      void LanczosRspFuncSetNHermTrue(In aIdx)         {mLanczosRspFuncs[aIdx]->SetNHerm(true);}
      // Temporary function to get NHerm bool for a check
      bool LanczosRspFuncGetNHerm(In aIdx) const       {return mLanczosRspFuncs[aIdx]->GetNHerm();}

      LanczosRspFunc* GetLanczosRspFunc(const string& aName) const;
      ///< Get Lanczos response function with name aName.
     
      ///> Lanczos IR spects
      void AddLanczosIRspect(LanczosIRspect* aLIR)     {mLanczosIRspects.push_back(aLIR);}
      In NlanczosIRspects() const                      {return mLanczosIRspects.size();}
      LanczosIRspect* GetLanczosIRspect(In aIdx) const {return mLanczosIRspects[aIdx];}

      ///> Band Lanczos IR spects
      void AddBandLanczosIRspect(BandLanczosIRspect* aLIR)     {mBandLanczosIRspects.push_back(aLIR);}
      In NBandLanczosIRspects() const                          {return mBandLanczosIRspects.size();}
      BandLanczosIRspect* GetBandLanczosIRspect(In aIdx) const {return mBandLanczosIRspects[aIdx];}
      
      ///> Band Lanczos Raman spects
      void AddBandLanczosRamanSpect(BandLanczosRamanSpect* aBLRaman) {mBandLanczosRamanSpects.push_back(aBLRaman);}
      In NBandLanczosRamanSpects() const                             {return mBandLanczosRamanSpects.size();}
      BandLanczosRamanSpect* GetBandLanczosRamanSpect(In aIdx) const {return mBandLanczosRamanSpects[aIdx];}
      
      void RspFuncOut(In aI);
      In NrspFuncs() const {return mRspFuncs.size();}
      RspFunc& GetRspFunc(In& aI) {return mRspFuncs[aI];}

      void SortRspVecs() {sort(mRspVecInf.begin(), mRspVecInf.end());}; //mbh sort the RF vector according to order
      void SortRspFuncs() {sort(mRspFuncs.begin(), mRspFuncs.end());}; //mbh sort the RF vector according to order

      In NrspOps() const {return mNrspOps;}
      void SetNrspOps(In aI) {mNrspOps = aI; mRspOpExptVals.resize(mNrspOps);}
      void AddRspOp(string aRspOp,In aOpNr) {mRspOpers[aRspOp]=aOpNr;}; 
      void AddRspOpExptVal(Nb& arValue,In aOpNr) 
           {if (aOpNr <mRspOpExptVals.size()) {mRspOpExptVals[aOpNr]=arValue;} 
            else MIDASERROR("Out of range in mRspOpExptVals");}
      In GetRspOpNr(const string& arS) const 
      {
         map<string, In>::const_iterator it =mRspOpers.find(arS); 
         if(it != mRspOpers.end())
            return it->second;
         else
            MIDASERROR("No such Rsp oper "+arS+" check your input");
         return -I_1;//Should never happen
      }
      string GetRspOp(In arNr);

      Nb GetRspOpExpVal(const In& arI) const 
      {
         if (arI<mRspOpExptVals.size()) 
         {
            return mRspOpExptVals[arI];
         }
         else 
         { 
            MIDASERROR("Out of range in mRspOpExptVals");
            return C_0;
         }
      }

      Nb GetRspOpExpVal(const string& arS) const 
      {
         map<string, In>::const_iterator it =mRspOpers.find(arS);
         if(it != mRspOpers.end())
            return GetRspOpExpVal(it->second);
         else
            MIDASERROR("No such Rsp oper "+arS+" check your input");
         return C_0;//Should never happen
      }
      //void AddRspOp(const string arRspOp) {mRspOpers.push_back(arRspOp);}

      void ClearRspVecContainer() {mRspVecInf.clear();} 
      void AddRspVec(const RspVecInf& arRspVecInf) 
      {
         mRspVecInf.push_back(arRspVecInf);
      }
      In FindRspEq(const RspVecInf& arRspVecInf);
      void RspVecInfOut(In aI);
      In NrspVecs() const {return mRspVecInf.size();}
      RspVecInf& GetRspVecInf(In& aI) {return mRspVecInf[aI];}

      void SetScreenZeroRsp(const Nb& arZ)
      {mScreenZeroRsp=arZ;}
      ///< Set the numerical zero for screening
      void SetScreenZeroRspC(const Nb& arZ)
      {mScreenZeroRspC=arZ;}
      ///< Set the numerical zero for screening
      Nb ScreenZeroRsp() const {return mScreenZeroRsp;}
      ///< Get the zero threshold.
      Nb ScreenZeroRspC() const {return mScreenZeroRspC;}
      ///< Get the zero threshold.

      void SetRspTargetSpace(bool aB) {mRspTargetSpace = aB; }///< Use a target space 
      void SetRspTargetingMethod(string aS) {mRspTargetingMethod = aS; }///< Use improved targeting 
      void SetRspOverlapMin(const Nb& arThr) {mRspOverlapMin  =arThr;}
      void SetRspOverlapSumMin(const Nb& arThr) {mRspOverlapSumMin  =arThr;}
      void SetRspOverlapNMax(In aIn) { mRspOverlapNMax = aIn; }
      void SetRspResidThrForOthers(const Nb& arThr) {mRspResidThrForOthers  =arThr;}
      void SetRspEnerThrForOthers(const Nb& arThr) {mRspEnerThrForOthers  =arThr;}
      void SetRspEnergyDiffMax(const Nb& arThr) {mRspEnergyDiffMax  =arThr;}
      ///< SetRsp selection threshold for energy difference between targets and solutions
      void SetRspAllFundamentals(bool aB=true) {mRspDoAllFundamentals = aB; } ///< Calculate all fundamentals by target.
      void SetRspAllFirstOvertones(bool aB=true) {mRspDoAllFirstOvertones= aB; } 
      void SetRspFundamentals(vector<In>& arI,bool aB=true) 
       {mRspDoFundamentals = aB; mRspFundamentals=arI;} ///< Calculate all fundamentals by target.
      void SetRspFirstOvertones(vector<In>& arI,bool aB=true) 
       {mRspDoFirstOvertones= aB;mRspFirstOvertones=arI;} ///< Calculate all first overtones by target.
      bool RspTargetSpace() const {return mRspTargetSpace; }///< Use a target space 
      string GetRspTargetingMethod() const {return mRspTargetingMethod; }///< Use improved tageting
      Nb GetRspOverlapMin() const {return mRspOverlapMin;}
      Nb GetRspOverlapSumMin() const {return mRspOverlapSumMin;}
      In GetRspOverlapNMax() const { return mRspOverlapNMax; }
      Nb GetRspResidThrForOthers() const {return mRspResidThrForOthers;}
      Nb GetRspEnerThrForOthers() const {return mRspEnerThrForOthers;}
      Nb GetRspEnergyDiffMax() const {return mRspEnergyDiffMax;}
      ///< GetRsp selection threshold for energy difference between targets and solutions
      bool RspAllFundamentals() const {return mRspDoAllFundamentals; } ///< Calculate all fundamentals by target.
      bool RspAllFirstOvertones() const {return mRspDoAllFirstOvertones; } ///< Calculate all first overtones by target.
      bool RspFundamentals() const {return mRspDoFundamentals; } ///< Calculate fundamentals by target.
      bool RspFirstOvertones() const {return mRspDoFirstOvertones; } ///< Calculate first overtones by target.
      void GetRspFundamentals(vector<In>& arI) const {arI=mRspFundamentals; } ///< Calculate fundamentals by target.
      void GetRspFirstOvertones(vector<In>& arI) const {arI=mRspFirstOvertones;} ///< Calculate first overtones by target.

      void SetNumVccJacobian(Nb aVal) {mNumVccJacobian = aVal;}
      double GetNumVccJacobian() const {return mNumVccJacobian;}
      
      void SetNumVccF(Nb aVal) {mNumVccF = aVal;}
      Nb GetNumVccF() const {return mNumVccF;}

      void SetDampingFactor(Nb aDamp) {mDamp = aDamp;}
      Nb GetDampingFactor() const {return mDamp;}
      void AddTotalResponseDrv(TotalResponseDrv* aTotal) {mTotalRsp.push_back(aTotal);}
      In GetDoTotalResponse() const {return mTotalRsp.size();};
      TotalResponseDrv* GetTotalResponseDrv(In aI) {return mTotalRsp[aI];};
      map<string,In> GetRspOpers() const {return mRspOpers;}
      void AddApproxRspFunc() {mNApproxRspFunc++;}
      In NApproxRspFunc() const {return mNApproxRspFunc;}

      bool GetRspTrueHDiag() const {return mRspTrueHDiag;}
      ///< Get mRspTrueHDiag
      bool GetRspTrueADiag() const {return mRspTrueADiag;}
      ///< Get mRspTrueADiag
      bool GetRspImprovedPrecond() const {return mRspImprovedPrecond;}
      In GetRspPrecondExciLevel() const {return mRspPrecondExciLevel;}
      In GetRspComplexImprovedPrecondExciLevel() const {return mRspComplexImprovedPrecondExciLevel;}
      In GetRspComplexImprovedPrecondMute() const {return mRspComplexImprovedPrecondMute;}
      In GetRspComplexImprovedPrecondExplicit() const {return mRspComplexImprovedPrecondExplicit;}
      void SetRspComplexImprovedPrecondMute(bool aBool) { mRspComplexImprovedPrecondMute = aBool;}
      void SetRspComplexImprovedPrecondExplicit(bool aBool) { mRspComplexImprovedPrecondExplicit = aBool;}
      In GetRspNresvecs() const {return mRspNresvecs;}
      string GetRspLevel2Solver() const {return mRspLevel2Solver;}
      void SetRspEigValSeq(const In aN){mRspEigValSeqSet= true; mRspEigValSeq= aN;}
      In   GetRspEigValSeq() const {return mRspEigValSeq;}
      bool GetRspEigValSeqSet() const {return mRspEigValSeqSet;}
      Nb GetAsymScale(In aI) const {return mAsymFreqs[aI];}
      void SetAsymScale(vector<Nb> aV) {mAsymFreqs=aV;}
      void SetAlsoLinearAsym(bool aB) {mAlsoLinearAsym=aB;}
      bool GetAlsoLinearAsym() const {return mAlsoLinearAsym;}
      void SetRspHarmonicRR(const bool& arB){mRspHarmonicRR = arB;}
      bool GetRspHarmonicRR() const {return mRspHarmonicRR;}

      void SetRspUseRayleighQuotientsAsEigvals(bool aB) { mRspUseRayleighQuotientsAsEigvals = aB; }
      bool RspUseRayleighQuotientsAsEigvals() const { return this->mRspUseRayleighQuotientsAsEigvals; }

      void SetRspAdaptiveHarmonicRrShift(bool aB) { this->mRspAdaptiveHarmonicRrShift = aB; }
      bool RspAdaptiveHarmonicRrShift() const { return this->mRspAdaptiveHarmonicRrShift; }

      void SetRspRefinedHarmonicRr(bool aB) { this->mRspRefinedHarmonicRr = aB; }
      bool RspRefinedHarmonicRr() const { return this->mRspRefinedHarmonicRr; }

      void SetRspEnerShift(const Nb& arNb){mRspEnerShift = arNb;}
      Nb GetRspEnerShift() const {return mRspEnerShift;}

      void SetRspUseEnerShiftInPrecon(bool aB) { this->mRspUseEnerShiftInPrecon = aB; }
      bool RspUseEnerShiftInPrecon() const { return this->mRspUseEnerShiftInPrecon; }

      void SetDoTensorDecomp(bool aDoTensorDecomp) { mDoTensorDecomp = aDoTensorDecomp; }
      bool GetDoTensorDecomp() const               { return mDoTensorDecomp; }
      void SetDoTensorRspBenchmarkDecomp(bool aDoTensorRspBenchmarkDecomp) { mDoTensorRspBenchmarkDecomp = aDoTensorRspBenchmarkDecomp; }
      //bool GetDoTensorRspDecomp() const               { return mDoTensorRspDecomp; }
      
      // below version makes for more readable code
      bool DoTensorDecomp() const      { return mDoTensorDecomp; }
      bool DoTensorRspBenchmarkDecomp() const   { return mDoTensorRspBenchmarkDecomp; }

      // Decomp info for TensorDataCont eig solver
      const midas::tensor::TensorDecompInfo::Set& GetVccRspDecompInfo() const { return this->mVccRspDecompInfo; }
      bool ReadVccRspDecompInfoSet(std::istream&, std::string&);

      // Decomp info for TensorDataCont Jacobian transformer
      const midas::tensor::TensorDecompInfo::Set& GetVccRspTrfDecompInfo() const { return this->mVccRspTrfDecompInfo; }
      bool ReadVccRspTrfDecompInfoSet(std::istream&, std::string&);

      // Get/Set balance after precon
      bool TensorVccRspEigBalanceAfterPrecon() const { return this->mTensorVccRspEigBalanceAfterPrecon; }
      void SetTensorVccRspEigBalanceAfterPrecon(bool aB) { this->mTensorVccRspEigBalanceAfterPrecon = aB; }

      // Get/Set recomp after precon
      bool TensorVccRspEigRecompAfterPrecon() const { return this->mTensorVccRspEigRecompAfterPrecon; }
      void SetTensorVccRspEigRecompAfterPrecon(bool aB) { this->mTensorVccRspEigRecompAfterPrecon = aB; }

      //! Get/Set abs precon
      bool TensorVccRspEigAbsPrecon() const { return this->mTensorVccRspEigAbsPrecon; }
      void SetTensorVccRspEigAbsPrecon(bool aB) { this->mTensorVccRspEigAbsPrecon = aB; }

      //! Get/Set FullRankAnalysis
      bool FullRspRankAnalysis() const { return this->mFullRspRankAnalysis; }
      void SetFullRspRankAnalysis(bool aB) { this->mFullRspRankAnalysis = aB; }

      //! Get/Set RankAnalysis
      bool RspRankAnalysis() const { return this->mRspRankAnalysis; }
      void SetRspRankAnalysis(bool aB) { this->mRspRankAnalysis = aB; }

      //! Get/Set CheckTensorVccTransJac
      bool CheckTensorVccTransJac() const       { return this->mCheckTensorVccTransJac; }
      void SetCheckTensorVccTransJac(bool aB)   { this->mCheckTensorVccTransJac = aB; }

      //! Get/Set TensorVccJacUseDebugVector
      bool TensorVccJacUseDebugVector() const          { return this->mTensorVccJacUseDebugVector; }
      void SetTensorVccJacUseDebugVector(bool aB)      { this->mTensorVccJacUseDebugVector = aB; }
      
      void PushBackTensorDecompDef(const TensorDecompDef& aTensorDecompDef) 
      { 
         mTensorDecompDefs.push_back(aTensorDecompDef); 
         if(gDebug)
         {
            Mout << " Pushing back TensorDecompDefinition " << std::endl;
            aTensorDecompDef.Output(Mout); 
         }
      }
      const vector<TensorDecompDef>& TensorDecompDefs() const 
      { 
         return mTensorDecompDefs; 
      }
      
      void SetRspEigType(RSPEIGTYPE aRspEigType) { mRspEigType = aRspEigType; }
      RSPEIGTYPE RspEigType() const { return mRspEigType; }

      void SetRspDecompEig();

      void SetForceComplexRsp(bool aForce) { mForceComplexRsp = aForce; }
      bool ForceComplexRsp() const { return mForceComplexRsp; }
      
      void SetItEqSaveToDisc(bool aSave) { mItEqSaveToDisc = aSave; }
      bool ItEqSaveToDisc() const { return mItEqSaveToDisc; }

      void SetComplexRspOlsen(bool aB) { mRspComplexOlsen = aB; }
      bool RspComplexOlsen() const { return mRspComplexOlsen; }

      void SetOlsen(bool aOlsen) {mOlsen = aOlsen;}
      bool Olsen() const {return mOlsen;}

      void SetRspLinEqMinimumResidual(bool aB) { mMinimumResidual = aB; }
      bool RspLinEqMinimumResidual() const { return mMinimumResidual; }

      void SetOlsenDecompRelThresh(Nb aNb) { mOlsenDecompRelThresh = aNb; }
      Nb OlsenDecompRelThresh() const { return mOlsenDecompRelThresh; }
      
      bool ReadItEqRestartVector(std::istream&,std::string&);
      const std::vector<std::string>& ItEqRestartVector() const { return mItEqRestartVector; }

      void PrintRspToFile() const;
      std::set<std::string> FindUniqueRspTypes(const std::vector<RspFunc>& aRspFuncs) const;

      bool ReadLambdaThreshold(std::istream&, std::string&);
      Nb LambdaThreshold() const { return mLambdaThreshold; }
      
      bool ReadRspItEqSolThr(std::istream&, std::string&);
      bool ReadRspPreconItEqResidThr(std::istream&, std::string&);
      bool ReadRspPreconItEqSolThr(std::istream&, std::string&);
      bool ReadRspPreconItEqMaxIter(std::istream&, std::string&);
      bool ReadRspComplexImprovedPrecond(std::istream&, std::string&);

      bool ReadRspItEqCheckLinearDependenceOfSpace(std::istream&, std::string&);
      bool RspItEqCheckLinearDependenceOfSpace() const { return mRspItEqCheckLinearDependenceOfSpace; }
      
      bool ReadItEqSubspacePreconVector(std::istream&,std::string&);
      const std::vector<std::string>& ItEqSubspacePreconVector() const { return mItEqSubspacePreconVector; }

      void SetDoDensityMatrixAnalysis(bool aD) {mDoDensityMatrixAnalysis= aD;} 
      bool DoDensityMatrixAnalysis() {return mDoDensityMatrixAnalysis;} 
      bool DoItNaMo() const {return mMakeItNaMo;};        ///< Calculate iterative natural modals 
      void SetSymDensForNaMo(bool aD) {mSymDensNaMo= aD;} 
      bool SymDensForNaMo() {return mSymDensNaMo;} 
      In GetItNaMoMaxIter() const {return mItNaMoMaxIter;}; ///< Max Iterations for Iterations natural modals 
      Nb GetItNaMoThr()     const {return mItNaMoThr;   };  ///< Threshold Iterative  natural modals 
      void SetItNaMo(bool aB)      {mMakeItNaMo=aB;};        ///< Calculate iterative natural modals 
      void SetItNaMoMaxIter(In aI) {mItNaMoMaxIter=aI;}; ///< Max Iterations for Iterations natural modals 
      void SetItNaMoThr(Nb aNb)    {mItNaMoThr=aNb;   };  ///< Threshold Iterative  natural modals 

      void SetItEqSubspacePreconFull(bool aB) { mItEqSubspacePreconFull = aB; }
      bool ItEqSubspacePreconFull() const { return mItEqSubspacePreconFull; }

      void SetRspComplexSolverType(COMPLEXSOLVER aT) { if(bool(mRspComplexSolverType) && (aT != mRspComplexSolverType)) MIDASERROR("Complex solver type already set."); mRspComplexSolverType = aT; }
      COMPLEXSOLVER RspComplexSolverType() const { return mRspComplexSolverType; }
      
      bool ReadVccTensorType(std::istream&, std::string&);
      VccTensorType GetVccTensorType() const { return mVccTensorType; }

      bool ReadRspUseNewContractions(std::istream&, std::string&);
      bool RspUseNewContractions() const { return mRspUseNewContractions; }

      bool ReadRspRelConv(std::istream&, std::string&);
      bool RspRelConv() const { return mRspRelConv; }
      
      bool GetGenDownCtrRecompressResult() const   { return mGenDownCtrRecompressResult; }
      void SetGenDownCtrRecompressResult(bool aB)  { mGenDownCtrRecompressResult = aB; }

      bool GetRspUseVccRefEnergy() const { return mRspUseVccRefEnergy; }
      void SetRspUseVccRefEnergy(bool aB) { mRspUseVccRefEnergy = aB; }

      bool TensorRspSolverConvertUnits() const { return mTensorRspSolverConvertUnits; }
      void SetTensorRspSolverConvertUnits(bool aB) { mTensorRspSolverConvertUnits = aB; }

      bool ReiterateTensorRspSolver() const { return mReiterateTensorRspSolver; }
      void SetReiterateTensorRspSolver(bool aB) { mReiterateTensorRspSolver = aB; }

      bool RecompressCpvccEigVecs() const { return mRecompressCpvccEigVecs; }
      void SetRecompressCpvccEigVecs(bool aB) { mRecompressCpvccEigVecs = aB; }

      In RspTransformerAllowedRank() const { return mRspTransformerAllowedRank; }
      void SetRspTransformerAllowedRank(In aIn) { mRspTransformerAllowedRank = aIn; }

      const std::pair<Nb, Nb>& RspTensorProductScreening() const { return mRspTensorProductScreening; }
      void SetRspTensorProductScreening(Nb aFirst, Nb aSecond) { mRspTensorProductScreening = std::make_pair(aFirst, aSecond); }

      In RspTensorProductLowRankLimit() const { return mRspTensorProductLowRankLimit; }
      void SetRspTensorProductLowRankLimit(In aIn) { mRspTensorProductLowRankLimit = aIn; }

      Nb TensorRspOrthoScreening() const { return mTensorRspOrthoScreening; }
      void SetTensorRspOrthoScreening(Nb aScreen) { mTensorRspOrthoScreening = aScreen; }

      bool TensorRspNoLinSolverPrecon() const { return mTensorRspNoLinSolverPrecon; }
      void SetTensorRspNoLinSolverPrecon(bool aB) { this->mTensorRspNoLinSolverPrecon = aB; }

      bool TensorRspFuncs() const { return this->mTensorRspFuncs; }
      void SetTensorRspFuncs(bool aB) { this->mTensorRspFuncs = aB; }
};

#endif /* RSPCALCDEF_H_INCLUDED */
