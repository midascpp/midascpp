/**
************************************************************************
* 
* @file                VccInput.cc
*
* Created:             19-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the Mvcc module 
* 
* Last modified:       02-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/VccCalcDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "input/MaxHoEner.h"
#include "input/FindKeyword.h"
#include "input/TensorDecompInput.h"
#include "input/GetLine.h"
#include "input/IsKeyword.h"
#include "input/ProgramSettings.h"
#include "util/MultiIndex.h"
#include "vcc/TransformerV3.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/Ranges.h"
#include "mpi/Impi.h"
#include "mpi/FileToString.h"

using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

using namespace midas::input;

string RspInput(std::istream&,VscfCalcDef&,bool);
string FlexCoupInput(std::istream&,FlexCoupCalcDef&,In);

void AddNewVcc(bool aSequence,bool aAddFirst,In aSeqBeg,In aSeqEnd,
      In aSeqOrdBeg=I_0,In aSeqOrdEnd=I_0)
{
   In n_tot = gVccCalcDef.size() 
      + (aSeqEnd-aSeqBeg+I_1)*(aSeqOrdEnd-aSeqOrdBeg+I_1); 
   gVccCalcDef.reserve(n_tot);
   VccCalcDef& new_vcc = gVccCalcDef[gVccCalcDef.size()-I_1];  
   // new_vcc is a reference to the newest vcc 
   string s_name_base = new_vcc.GetName();
   In exci_level = new_vcc.MaxExciLevel();
   In iso        = new_vcc.IsOrder();
   string s_name      = s_name_base 
      + "_EXC_" + std::to_string(exci_level) + "_ISO_" + std::to_string(iso); 
   new_vcc.SetName(s_name);
   //Mout << "In AddNewVcc, name = " << s_name <<endl;
   if (new_vcc.Restart() && new_vcc.RestDiffExci())
   {
      In rest_exci_level = new_vcc.RestExciLevel();
      string s_rest_name      = s_name_base 
         + "_EXC_" + std::to_string(rest_exci_level) + "_ISO_" + std::to_string(iso); 
      //Mout << "In AddNewVcc, rest_name = " << s_rest_name <<endl;
      new_vcc.SetRestName(s_rest_name);
   }
   else new_vcc.SetRestName(s_name);
   //Mout << "Debug: In AddNewVcc name (2)= " << s_name <<endl;
   if (aSequence)
   {
      new_vcc.SetUseAvailable(true);
      new_vcc.SetRestart(true);
      new_vcc.SetSequence(true);
      for (In i_max_exci=aSeqBeg;i_max_exci<=aSeqEnd;i_max_exci++)
      {
         if (i_max_exci==aSeqBeg) 
         {
            new_vcc.SetMaxExciLevel(i_max_exci);
         }
         if (i_max_exci>aSeqBeg) 
         {
            gVccCalcDef.push_back(new_vcc);
            gVccCalcDef[gVccCalcDef.size()-I_1].SetMaxExciLevel(i_max_exci);
         }
         for (In i_ord=aSeqOrdBeg;i_ord<=aSeqOrdEnd;i_ord++)
         {
            if (i_ord==aSeqOrdBeg && (i_ord!=I_0) && i_max_exci==aSeqBeg) 
            {
               new_vcc.SetIso(true,i_ord);
               new_vcc.SetMaxExciLevel(i_max_exci);
            }
            if (i_ord>aSeqOrdBeg) 
            {
               gVccCalcDef.push_back(new_vcc);
               gVccCalcDef[gVccCalcDef.size()-I_1].SetMaxExciLevel(i_max_exci);
               gVccCalcDef[gVccCalcDef.size()-I_1].SetIso(true,i_ord);
            }
            string s_name2 = s_name_base+"_EXC_"+std::to_string(i_max_exci)
                                   +"_ISO_"+std::to_string(i_ord); 
            gVccCalcDef[gVccCalcDef.size()-I_1].SetName(s_name2);
         }
      }
   }
   else
   {
      if (aAddFirst) 
      {
         gVccCalcDef.push_back(new_vcc);
      }
   }
}

void ParseFranckCondonArgs(std::istream& aInp, VccCalcDef& aVccCalcDef)
{
   string s;
   getline(aInp, s);
   transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);

   string other_calc;
   if (GetValueFromKey<string>(s, "OTHERCALC", other_calc) != I_0)
   {
      MIDASERROR("Cannot parse FranckCondon OtherCalc parameter.");
   }
   aVccCalcDef.SetFCOtherCalc(other_calc);

   string oper;
   if (GetValueFromKey<string>(s, "OPERATOR", oper) == I_0)
   {
      aVccCalcDef.SetFCOperator(oper);
   }
}

void ParseTransformerArgs(std::istream& aInp, VccCalcDef& aVccCalcDef)
{
   string inp;
   getline(aInp, inp);

   string trf;
   if (GetValueFromKey<string>(inp, "TRF", trf) != I_0)
   {
      MIDASERROR("Cannot parse #3Transformer Trf parameter.");
   }

   transform(trf.begin(), trf.end(), trf.begin(), (In(*) (In))toupper);
   if (trf == "ORIG")
   {
      bool b;
      if (GetValueFromKey<bool>(inp, "T1TRANSH", b) == 0)
      {
         aVccCalcDef.SetT1TransH(b);
      }
      if (GetValueFromKey<bool>(inp, "OUTOFSPACE", b) == 0)
      {
         aVccCalcDef.SetVccOutOfSpace(b);
      }
      if (GetValueFromKey<bool>(inp, "RSU", b) == 0)
      {
         aVccCalcDef.SetVccRemoveSelUnlinked(b);
      }
   }
   else if (trf == "1M")
   {
      aVccCalcDef.SetT1TransH(true);
      aVccCalcDef.SetVccOutOfSpace(false);
      aVccCalcDef.SetOneModeTrans(true);
   }
   else if (trf == "2M")
   {
      aVccCalcDef.SetT1TransH(true);
      aVccCalcDef.SetVccOutOfSpace(false);
      aVccCalcDef.SetTwoModeTrans(true);
      Nb threshold = C_0;
      if (GetValueFromKey<Nb>(inp, "SCREENING", threshold) == 0)
      {
         aVccCalcDef.SetTwoModeTransScreen(threshold);
      }
   }
   else if (trf == "V3")
   {
      aVccCalcDef.SetV3trans(true);
      aVccCalcDef.SetVccOutOfSpace(false);
      string type("GEN");
      GetValueFromKey<string>(inp, "TYPE", type);

      transform(type.begin(), type.end(), type.begin(), (In(*) (In))toupper);
      if (type == "GEN")
      {
         aVccCalcDef.SetV3type(V3::GEN);
      }
      else if (type == "VCC1PT2")
      {
         aVccCalcDef.SetV3type(V3::VCC1PT2);
      }
      else if (type == "VCC2")
      {
         aVccCalcDef.SetV3type(V3::VCC2);
      }
      else if (type == "VCC2PT3")
      {
         aVccCalcDef.SetV3type(V3::VCC2PT3);
      }
      else if (type == "VCC3PT4F")
      {
         aVccCalcDef.SetV3type(V3::VCC3PT4F);
      }
      else if (type == "VCC3PT4")
      {
         aVccCalcDef.SetV3type(V3::VCC3PT4);
      }

      string s;
      if (GetValueFromKey<string>(inp, "FILEPREFIX", s) == 0)
      {
         aVccCalcDef.SetV3FilePrefix(s);
      }
      else
      {
         aVccCalcDef.SetV3FilePrefix(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/");
      }
      bool b;
      if (GetValueFromKey<bool>(inp, "T1TRANSH", b) == 0)
      {
         aVccCalcDef.SetT1TransH(b);
      }
      if (GetValueFromKey<bool>(inp, "LATEX", b) == 0)
      {
         aVccCalcDef.SetV3latex(b);
      }
   }
   else
   {
      MIDASERROR("#3Transformer keyword: Unknown TRF value.");
   }
}

/***************************************************************************//**
 * Post-processes the input for the case of having set the
 * EXTRANGEMAXEFFEXCILEVEL or EXTRANGEMODES.
 *
 * Does some sanity checks;
 *    - that both of the keywords above are set (if not none)
 *    - that MAXEXCI has not been set (will be overwritten, see below)
 *
 * Adjusts the MAXEXCI value to be the EXTRANGEMAXEFFEXCILEVEL plus the number of
 * EXTRANGEMODES which is the actual size of the generated
 * ModeCombiOpRange.
 *
 * @note
 *    It's important to make VccCalcDef::mMaxExciLevel hold the right value,
 *    because this is used e.g. when setting up the transformer, regardless of
 *    what will be the actual ModeCombiOpRange::GetMaxExciLevel().
 ******************************************************************************/
void PostProcessExtendedRangeWithEffectiveExciLevelInput
   (  VccCalcDef& arVccCalcDef
   )
{
   const auto& max_eff_level = arVccCalcDef.GetExtRangeMaxEffExciLevel();
   const auto& ext_modes = arVccCalcDef.GetExtRangeModes();
   if (arVccCalcDef.UsingExtRangeModes())
   {
      if (arVccCalcDef.MaxExciLevel() >= I_0)
      {
         MIDASERROR("Input: You must not set MAXEXCI together with EXTRANGEMAXEFFEXCILEVEL.");
      }
      else
      {
         arVccCalcDef.SetMaxExciLevel(max_eff_level + ext_modes.size());
      }
   }
   else if  (  max_eff_level >= I_0
            && ext_modes.empty()
            )
   {
      MIDASERROR("Input: You've set EXTRANGEMAXEFFEXCILEVEL but forgot EXTRANGEMODES.");
   }
   else if  (  !ext_modes.empty()
            && max_eff_level < I_0
            )
   {
      MIDASERROR("Input: You've set EXTRANGEMODES but forgot EXTRANGEMAXEFFEXCILEVEL.");
   }
}

/**
* Read and check VCC input.
* Read until next s = "#i..." i<3 which is returned.
* */
string VccInput(std::istream& Minp)
{
   gVccCalcDef.reserve(6);
   if (gDoVcc && gDebug) 
   {
      Mout << " I have read one Vcc input - reading a new Vcc input "<< endl;
   }
   gDoVcc    = true; 
   gVccCalcDef.push_back(VccCalcDef());
   VccCalcDef& new_vcc = gVccCalcDef[gVccCalcDef.size()-I_1];  // new_oper is a reference to the new operator
   new_vcc.Reserve();                                           // Reservations of STL space;
   new_vcc.SetDoNoPaired(true);   // Set default VCI diff. from VSCF for response calc. 
   bool sequence=false;
   In i_seq_b=I_0;
   In i_seq_e=I_0;
   In i_seq_ord_b=I_0;
   In i_seq_ord_e=I_0;


   // Enum/map with input flags
   enum INPUT 
      {  ERROR
      ,  IOLEVEL
      ,  NAME
      ,  OPER
      ,  BASIS
      ,  METHOD
      ,  USEVSCF
      ,  USEALLVSCF
      ,  OCCUPATION
      ,  OCCUMAX
      ,  OCCMAXSUMN
      ,  MAXSUMN
      ,  OCCMAXEXCI
      ,  MAXEXCI
      ,  SEQUENCE
      ,  OCCMAXEXPRMODE
      ,  MAXEXPRMODE
      ,  NOTSAVE
      ,  RESTART
      ,  USEAVAILABLE
      ,  VSCFMAXITER
      ,  VSCFRESIDTHR
      ,  VSCFENERTHR
      ,  DIAGMETH
      ,  DUPLICATE
      ,  VSCFSCREENZERO
      ,  ITEQMAXIT
      ,  ITEQMAXDIM
      ,  ITEQBREAKDIM
      ,  ITEQRESIDTHR
      ,  ITEQENERTHR
      ,  INTSTORAGE
      ,  VECSTORAGE
      ,  FVCIEXPH
      ,  EXPHVCI
      ,  VCC
      ,  VCI
      ,  VCIH2
      ,  H2LS
      ,  VMP
      ,  VAPT
      ,  PADE
      ,  MODALSFILE
      ,  PRIMITIVEBASIS
      ,  LIMITMODALBASIS
      ,  USEALLVSCFMODALS
      ,  H2START
      ,  VMPSTART
      ,  TESTTRANSFORMER
      ,  LAMBDAS
      ,  LAMBDASCAN
      ,  ISO
      ,  SUMNPRIM
      ,  EMAXPRIM
      ,  ZEROPRIM
      ,  REPVCIASVCC
      ,  REPVCCASVCI
      ,  DIIS
      ,  PREDIAG
      ,  NEWTONRAPHSON
      ,  TIMEIT
      ,  SIZEIT
      ,  TARGETSPACE
      ,  REFERENCE
      ,  ALLFUNDAMENTALS
      ,  ALLFIRSTOVERTONES
      ,  FUNDAMENTALS
      ,  FIRSTOVERTONES
      ,  RSP
      ,  FCCALC
      ,  SCALESTEP
      ,  OCCGROUNDSTATE
      ,  OCCALLFUND
      ,  OCCFUNDAMENTALS
      ,  OCCALLFIRSTOVER
      ,  OCCFIRSTOVER
      ,  OCCGENCOMBI
      ,  NMODES
      ,  NOOLSEN
      ,  OCCALLWITHHOMAXE
      ,  EIGVALSEQ
      ,  TRUEHDIAG
      ,  IMPROVEDPRECOND
      ,  TARGETINGMETHOD
      ,  OVERLAPMIN
      ,  OVERLAPSUMMIN
      ,  ENERGYDIFFMAX
      ,  RESIDTHRFOROTHERS
      ,  ENERTHRFOROTHERS
      ,  FRANCKCONDON
      ,  MAPPREVIOUSVECTORS
      ,  LEVEL2SOLVER
      ,  TRANSFORMER
      ,  NRESVECS
      ,  HARMONICRR
      ,  ENERSHIFT
      ,  CMPV3ORIGVCC
      ,  CMPV3ORIGVCI
      ,  CMPV3ORIGVCCJAC
      ,  CHECKV3LEFT
      ,  SCREENINTERMEDS
      ,  SCREENINTERMEDSAMPS
      ,  INDIVIDUALMOLMC
      ,  FLEXCOUP
      ,  DISABLECHECKMETHOD
      ,  USENEWTENSORCONTRACTIONS
      ,  USEOLDVCCSOLVER
      ,  ADAPTIVEDECOMPTHRESHOLD
      ,  ADAPTIVETRFDECOMPTHRESHOLD
      ,  ADAPTIVEDECOMPGUESS
      ,  MAXNORMCONVCHECK
      ,  TENSORDECOMPINFO
      ,  TRANSFORMERTENSORDECOMPINFO
      ,  INDIVIDUALMCDECOMPTHRESH
      ,  TRANSFORMERALLOWEDRANK
      ,  TENSORPRODUCTSCREENING
      ,  ADAPTIVETENSORPRODUCTSCREENING
      ,  ITEQMAXITMICRO 
      ,  ITEQMICROTHRFAC
      ,  LAPLACEINFO
      ,  CHECKLAPLACEFIT
      ,  BACKTRACKINGTHRESH
      ,  MAXBACKTRACKS
      ,  MAXBACKSTEPS
      ,  SIMPLEQUASINEWTON
      ,  IDJA
      ,  IDJAUNITMATRIXGUESS
      ,  CROP
      ,  PRECONSAVEDRESIDUALS
      ,  NOPRECON
      ,  SAVERANKINFO
      ,  FULLRANKANALYSIS
      ,  CHECKIDJA
      ,  CHECKNEWTONRAPHSON
      ,  NOBACKSTEP
      ,  ALLOWBACKSTEP
      ,  IMPROVEDPRECONINSUBSPACEMATRIX
      ,  INTERMEDIATERESTRICTIONS
      ,  TENSORORDERTHRESHOLDSCALING
      ,  TENSORPRODUCTLOWRANKLIMIT
      ,  USECPSCALINGFORINTERMEDS
      ,  BREAKBEFORETRANSFORM
      ,  CPVCCDEFAULTS
      ,  EXTRANGEMAXEFFEXCILEVEL
      ,  EXTRANGEMODES
      ,  MATREP
      ,  MATREPFVCIANALYSIS
      };
   const map<string,INPUT> input_word =
      {  {"#3IOLEVEL",IOLEVEL}
      ,  {"#3NAME",NAME}
      ,  {"#3OPER",OPER}
      ,  {"#3BASIS",BASIS}
      ,  {"#3METHOD", METHOD}
      ,  {"#3USEVSCF", USEVSCF}
      ,  {"#3USEALLVSCF", USEALLVSCF}
      ,  {"#3OCCUP",OCCUPATION}
      ,  {"#3OCCUMAX",OCCUMAX}
      ,  {"#3OCCMAXSUMN",OCCMAXSUMN}
      ,  {"#3MAXSUMN",MAXSUMN}
      ,  {"#3OCCMAXEXCI",OCCMAXEXCI}
      ,  {"#3MAXEXCI",MAXEXCI}
      ,  {"#3SEQUENCE",SEQUENCE}
      ,  {"#3OCCMAXEXPRMODE",OCCMAXEXPRMODE}
      ,  {"#3MAXEXPRMODE",MAXEXPRMODE}
      ,  {"#3DONOTSAVE",NOTSAVE}
      ,  {"#3RESTART",RESTART}
      ,  {"#3USEAVAILABLE",USEAVAILABLE}
      ,  {"#3MAPPREVIOUSVECTORS",MAPPREVIOUSVECTORS}
      ,  {"#3VSCFMAXITER",VSCFMAXITER}
      ,  {"#3VSCFTHRESHOLD",VSCFENERTHR}
      ,  {"#3VSCFTHRESHOLD-RESID",VSCFRESIDTHR}
      ,  {"#3VSCFTHRESHOLD-ENER",VSCFENERTHR}
      ,  {"#3DIAGMETH",DIAGMETH}
      ,  {"#3DUPLICATE",DUPLICATE}
      ,  {"#3VSCFSCREENZERO",VSCFSCREENZERO}
      ,  {"#3ITEQMAXIT",ITEQMAXIT}
      ,  {"#3ITEQMAXDIM",ITEQMAXDIM}
      ,  {"#3ITEQBREAKDIM",ITEQBREAKDIM}
      ,  {"#3ITEQENERTHR",ITEQENERTHR}
      ,  {"#3ITEQRESIDTHR",ITEQRESIDTHR}
      ,  {"#3INTSTORAGE",INTSTORAGE}
      ,  {"#3VECSTORAGE",VECSTORAGE}
      ,  {"#3FVCIEXPH",FVCIEXPH}
      ,  {"#3EXPHVCI",EXPHVCI}
      ,  {"#3VCC",VCC}
      ,  {"#3VCI",VCI}
      ,  {"#3VCIH2",VCIH2}
      ,  {"#3H2LS",H2LS}
      ,  {"#3VMP",VMP}
      ,  {"#3VAPT",VAPT}
      ,  {"#3PADE",PADE}
      ,  {"#3MODALSFILE",MODALSFILE}
      ,  {"#3PRIMITIVEBASIS",PRIMITIVEBASIS}
      ,  {"#3LIMITMODALBASIS",LIMITMODALBASIS}
      ,  {"#3USEALLVSCFMODALS",USEALLVSCFMODALS}
      ,  {"#3TESTTRANSFORMER",TESTTRANSFORMER}
      ,  {"#3H2START",H2START}
      ,  {"#3VMPSTART",VMPSTART}
      ,  {"#3LAMBDAS",LAMBDAS}
      ,  {"#3LAMBDASCAN",LAMBDASCAN}
      ,  {"#3ISO",ISO}
      ,  {"#3SUMNPRIM",SUMNPRIM}
      ,  {"#3EMAXPRIM",EMAXPRIM}
      ,  {"#3ZEROPRIM",ZEROPRIM}
      ,  {"#3REPVCIASVCC",REPVCIASVCC}
      ,  {"#3REPVCCASVCI",REPVCCASVCI}
      ,  {"#3DIIS",DIIS}
      ,  {"#3PREDIAG",PREDIAG}
      ,  {"#3NEWTONRAPHSON",NEWTONRAPHSON}
      ,  {"#3TIMEIT",TIMEIT}
      ,  {"#3SIZEIT",SIZEIT}
      ,  {"#3TARGETSPACE",TARGETSPACE}
      ,  {"#3REFERENCE",REFERENCE}
      ,  {"#3ALLFUNDAMENTALS",ALLFUNDAMENTALS}
      ,  {"#3ALLFIRSTOVERTONES",ALLFIRSTOVERTONES}
      ,  {"#3FUNDAMENTALS",FUNDAMENTALS}
      ,  {"#3FIRSTOVERTONES",FIRSTOVERTONES}
      ,  {"#3FCCALC",FCCALC}
      ,  {"#3SCALESTEP",SCALESTEP}
      ,  {"#3RSP",RSP}
      ,  {"#3OCCGROUNDSTATE",OCCGROUNDSTATE}
      ,  {"#3OCCALLFUND",OCCALLFUND}
      ,  {"#3OCCFUNDAMENTALS",OCCFUNDAMENTALS}
      ,  {"#3OCCALLFIRSTOVER",OCCALLFIRSTOVER}
      ,  {"#3OCCFIRSTOVER",OCCFIRSTOVER}
      ,  {"#3OCCGENCOMBI",OCCGENCOMBI}
      ,  {"#3NMODES",NMODES}
      ,  {"#3NOOLSEN",NOOLSEN}
      ,  {"#3OCCALLWITHHOMAXE",OCCALLWITHHOMAXE}
      ,  {"#3EIGVALSEQ",EIGVALSEQ}
      ,  {"#3TRUEHDIAG",TRUEHDIAG}
      ,  {"#3IMPROVEDPRECOND",IMPROVEDPRECOND}
      ,  {"#3TARGETINGMETHOD",TARGETINGMETHOD}
      ,  {"#3OVERLAPMIN",OVERLAPMIN}
      ,  {"#3OVERLAPSUMMIN",OVERLAPSUMMIN}
      ,  {"#3ENERGYDIFFMAX",ENERGYDIFFMAX}
      ,  {"#3RESIDTHRFOROTHERS",RESIDTHRFOROTHERS}
      ,  {"#3ENERTHRFOROTHERS",ENERTHRFOROTHERS}
      ,  {"#3FRANCKCONDON",FRANCKCONDON}
      ,  {"#3LEVEL2SOLVER",LEVEL2SOLVER}
      ,  {"#3TRANSFORMER",TRANSFORMER}
      ,  {"#3NRESVECS",NRESVECS}
      ,  {"#3HARMONICRR",HARMONICRR}
      ,  {"#3ENERSHIFT",ENERSHIFT}
      ,  {"#3CMPV3ORIGVCC",CMPV3ORIGVCC}
      ,  {"#3CMPV3ORIGVCI",CMPV3ORIGVCI}
      ,  {"#3CMPV3ORIGVCCJAC",CMPV3ORIGVCCJAC}
      ,  {"#3CHECKV3LEFT",CHECKV3LEFT}
      ,  {"#3SCREENINTERMEDS",SCREENINTERMEDS}
      ,  {"#3SCREENINTERMEDSAMPS",SCREENINTERMEDSAMPS}
      ,  {"#3INDIVIDUALMOLMC",INDIVIDUALMOLMC}
      ,  {"#3FLEXCOUP",FLEXCOUP}
      ,  {"#3DISABLECHECKMETHOD",DISABLECHECKMETHOD}
      ,  {"#3USENEWTENSORCONTRACTIONS",USENEWTENSORCONTRACTIONS}
      ,  {"#3USEOLDVCCSOLVER",USEOLDVCCSOLVER}
      ,  {"#3ADAPTIVEDECOMPTHRESHOLD",ADAPTIVEDECOMPTHRESHOLD}
      ,  {"#3ADAPTIVETRFDECOMPTHRESHOLD",ADAPTIVETRFDECOMPTHRESHOLD}
      ,  {"#3ADAPTIVEDECOMPGUESS",ADAPTIVEDECOMPGUESS}
      ,  {"#3MAXNORMCONVCHECK", MAXNORMCONVCHECK}
      ,  {"#3SAVERANKINFO",SAVERANKINFO}
      ,  {"#3FULLRANKANALYSIS", FULLRANKANALYSIS}
      ,  {"#3TENSORDECOMPINFO", TENSORDECOMPINFO}
      ,  {"#3TRANSFORMERTENSORDECOMPINFO", TRANSFORMERTENSORDECOMPINFO}
      ,  {"#3TRANSFORMERALLOWEDRANK", TRANSFORMERALLOWEDRANK}
      ,  {"#3TENSORPRODUCTSCREENING", TENSORPRODUCTSCREENING}
      ,  {"#3ADAPTIVETENSORPRODUCTSCREENING", ADAPTIVETENSORPRODUCTSCREENING}
      ,  {"#3ITEQMAXITMICRO",ITEQMAXITMICRO}
      ,  {"#3ITEQMICROTHRFAC",ITEQMICROTHRFAC}
      ,  {"#3DISABLECHECKMETHOD",DISABLECHECKMETHOD}
      ,  {"#3LAPLACEINFO", LAPLACEINFO}
      ,  {"#3CHECKLAPLACEFIT",CHECKLAPLACEFIT}
      ,  {"#3SIMPLEQUASINEWTON", SIMPLEQUASINEWTON}
      ,  {"#3IDJA", IDJA}
      ,  {"#3IDJAUNITMATRIXGUESS", IDJAUNITMATRIXGUESS}
      ,  {"#3CROP", CROP}
      ,  {"#3PRECONSAVEDRESIDUALS", PRECONSAVEDRESIDUALS}
      ,  {"#3NOPRECON", NOPRECON}
      ,  {"#3IMPROVEDPRECONINSUBSPACEMATRIX",IMPROVEDPRECONINSUBSPACEMATRIX}
      ,  {"#3BACKTRACKINGTHRESH",BACKTRACKINGTHRESH}
      ,  {"#3MAXBACKTRACKS",MAXBACKTRACKS}
      ,  {"#3MAXBACKSTEPS",MAXBACKSTEPS}
      ,  {"#3NOBACKSTEP", NOBACKSTEP}
      ,  {"#3ALLOWBACKSTEP", ALLOWBACKSTEP}
      ,  {"#3CHECKIDJA",CHECKIDJA}
      ,  {"#3CHECKNEWTONRAPHSON",CHECKNEWTONRAPHSON}
      ,  {"#3INTERMEDIATERESTRICTIONS",INTERMEDIATERESTRICTIONS}
      ,  {"#3INDIVIDUALMCDECOMPTHRESH",INDIVIDUALMCDECOMPTHRESH}
      ,  {"#3TENSORORDERTHRESHOLDSCALING",TENSORORDERTHRESHOLDSCALING}
      ,  {"#3TENSORPRODUCTLOWRANKLIMIT", TENSORPRODUCTLOWRANKLIMIT}
      ,  {"#3USECPSCALINGFORINTERMEDS", USECPSCALINGFORINTERMEDS}
      ,  {"#3BREAKBEFORETRANSFORM", BREAKBEFORETRANSFORM}
      ,  {"#3CPVCCDEFAULTS", CPVCCDEFAULTS}
      ,  {"#3EXTRANGEMAXEFFEXCILEVEL", EXTRANGEMAXEFFEXCILEVEL}
      ,  {"#3EXTRANGEMODES", EXTRANGEMODES}
      ,  {"#3MATREP", MATREP}
      ,  {"#3SUBSPACESOLVERCALC", MATREP}    // Allow #3SUBSPACESOLVERCALC to set MATREP calculation
      ,  {"#3MATREPFVCIANALYSIS", MATREPFVCIANALYSIS}
      };
   
   // Initialization of default values for quantities read in later &  Local params.
   vector<In> occ_vec;
   vector<In> occmax_vec;
   vector<In> occmaxexprmode_vec;
   occ_vec.reserve(gReserveNmodes);
   occmax_vec.reserve(gReserveNmodes);
   occmaxexprmode_vec.reserve(gReserveNmodes);
   vector<In> limits;
   limits.reserve(gReserveNmodes);
   bool limits_set = false;
   bool use_all_vscf_modals = false;
   In max_sum_n = 0;
   In max_exci  = 0;
   In n_dup     = 1;
   bool max_sum_n_set = false;
   bool max_exci_set  = false;
   bool b_ground_state =false;
   bool b_all_fund     =false;
   bool b_fundamentals =false;
   bool b_all_firstover=false;
   bool b_firstover    =false;
   std::vector<In> fundamentals;
   std::vector<In> first_overtones;
   std::set<std::map<In, In> > gen_combis;
   In n_modes_inp = I_0;
   bool occ_all_with_ho_maxe = false;
   Nb e_occ_all_with_ho_maxe = C_0;
   string s_name="";

   // Correlation/Rsp 
   bool all_fund = false;
   bool all_first = false;
   bool fund = false;
   bool first = false;
   bool reference= false;
   bool target = false;

   // Set defaults for CP-VCC after input is read (because they depend on solver threshold)
   bool cpvcc_defaults = false;

   // Are we using old solver for gs VCC
   bool old_vcc_solver = false;

   // Are we using simple quasi newton?
   bool simple_qn = false;

   // Are the thresholds set?
   bool resid_thr_set = false;
   bool ener_thr_set = false;

   // Set up method automatically
   bool auto_method_set = false;

   // Method input (such as #3Vcc, #3MaxExci, #3Transformer, etc.) has been set
   bool method_input_given = false;

   // Have we set a targeting keyword that only works for VCI?
   bool vci_targeting = false;

   string s="";
   In input_level = 3;
   bool already_read = false;
   while ((already_read || GetLine(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read=false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);
      
      switch(input)
      {
         case IOLEVEL:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetIoLevel(midas::util::FromString<In>(s));
            break;
         }
         case NAME:
         {
            midas::input::GetLine(Minp, s); // Get new line with input
            s_name = s;
            break;
         }
         case OPER:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string s_oper="";
            input_s >> s_oper;
            new_vcc.SetOper(s_oper);
            break;
         }
         case BASIS:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string s_basis="";
            input_s >> s_basis;
            new_vcc.SetBasis(s_basis);
            break;
         }
         case METHOD:
         {
            if (  method_input_given
               )
            {
               MIDASERROR("Method input has already been given using e.g. #3Vcc, #3MaxExci, #3Transformer, etc. Automatic setup cannot be performed!");
            }
            auto setup_info = new_vcc.AutoSetupMethod(Minp, s);
            already_read = setup_info.first;
            cpvcc_defaults = setup_info.second;
            auto_method_set = true;

            new_vcc.SetAutoSetupMethod(true);

            break;
         }
         case USEVSCF:
         {
            midas::input::GetLine(Minp, s);
            MidasAssert(midas::input::NotKeyword(s), "A name for the reference VSCF calculation must be provided for #3UseVscf");
            new_vcc.SetVscfReference(s);
            break;
         }
         case USEALLVSCF:
         {
            new_vcc.SetUseAllVscf(true);
            break;
         }
         case OCCUPATION:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            occ_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCUMAX:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            occmax_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCMAXSUMN:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            max_sum_n = midas::util::FromString<In>(s);
            max_sum_n_set=true;
            break;
         }
         case MAXSUMN:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetMaxSumN(midas::util::FromString<In>(s));
            break;
         }
         case OCCMAXEXPRMODE:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            occmaxexprmode_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case MAXEXPRMODE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetMaxExPrMode(midas::util::VectorFromString<In>(s));
            break;
         }
         case OCCMAXEXCI:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            max_exci = midas::util::FromString<In>(s);
            max_exci_set=true;
            break;
         }
         case MAXEXCI:
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("MaxExci should not be modified after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetMaxExciLevel(midas::util::FromString<In>(s));
            break;
         }
         case SEQUENCE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> i_seq_b; 
            input_s >> i_seq_e; 
            sequence = true;
            In i1=I_0; 
            In i2=I_0; 
            if (input_s >> i1 && input_s >> i2)
            {
               i_seq_ord_b = i1; 
               i_seq_ord_e = i2; 
               Mout << " Sequence both exci-level and order " << endl;
            } 
            else 
            {
               Mout << " Sequence exci-level " << endl;
            }
            break;
         }
         case NOTSAVE:
         {
            bool save = false;
            new_vcc.SetSave(save);
            break;
         }
         case RESTART:
         {
            bool restart = true;
            new_vcc.SetRestart(restart);
            // read an optional line containing the excitation level of the previous calculation
            In pos=Minp.tellg();
            GetLine(Minp,s);
            if(s.find("#")!=s.npos)
            {
               Minp.seekg(pos);
               new_vcc.SetRestExciLevel(false);
               Mout << "I will try to restart a VCC calculation with the same excitation level of the previous" << endl;
            }
            else
            {
               In oldexcilevel = midas::util::FromString<In>(s);
               new_vcc.SetRestExciLevel(true,oldexcilevel);
               Mout << "I will try to restart a VCC calculation from the previous VCC[" << oldexcilevel  << "] calculation" << endl;
            }
            break;
         }
         case USEAVAILABLE:
         {
            bool use_available = true;
            new_vcc.SetUseAvailable(use_available);
            break;
         }
         case MAPPREVIOUSVECTORS:
         {
            // Check if restart info is available
            if (InquireFile(gAnalysisDir+"/VCCRESTART.INFO"))
            {
               Mout << " Vcc Input: Restart info file found." << endl;
               bool map_it = true;
               new_vcc.SetMapPreviousVectors(map_it);
            }
            else MIDASERROR(" Vcc Input: Restart info file not found.");
            break;
         }
         case VSCFMAXITER:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case VSCFRESIDTHR:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetResidThr(midas::util::FromString<Nb>(s));
            break;
         }
         case VSCFENERTHR:
         {
            new_vcc.SetVscfInputInVcc();
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEnerThr(midas::util::FromString<Nb>(s));
            break;
         }
         case DIAGMETH:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string diag_meth="";
            input_s >> diag_meth;
            transform(diag_meth.begin(),diag_meth.end(),diag_meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vcc.SetDiagMeth(diag_meth);
            break;
         }
         case DUPLICATE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            n_dup = midas::util::FromString<In>(s);
            break;
         }
         case VSCFSCREENZERO:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetScreenZero(midas::util::FromString<Nb>(s));
            break;
         }
         case ITEQMAXIT:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetItEqMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case ITEQMAXDIM:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetRedDimMax(midas::util::FromString<In>(s));
            break;
         }
         case ITEQBREAKDIM:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetRedBreakDim(midas::util::FromString<In>(s));
            break;
         }
         case ITEQRESIDTHR:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetItEqResidThr(midas::util::FromString<Nb>(s));
            resid_thr_set = true;
            break;
         }
         case ITEQENERTHR:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetItEqEnerThr(midas::util::FromString<Nb>(s));
            ener_thr_set = true;
            break;
         }
         case INTSTORAGE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetIntStorage(midas::util::FromString<In>(s));
            break;
         }
         case VECSTORAGE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            In ist= midas::util::FromString<In>(s);
            if(midas::mpi::GlobalSize() > 1 && ist>1) 
            {
               Mout << "Vecstorage 0 or 1 when using MPI." << std::endl;
               Mout << "Resetting to 1 from " << ist << std::endl;
               ist = I_1;
            }
            new_vcc.SetVecStorage(ist);
            break;
         }
         case FVCIEXPH:
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3FVCIEXPH should not be set after specifying the #3Method keyword!");
            }

            new_vcc.SetFvciExpH(true);
            break;
         }
         case EXPHVCI: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3EXPHVCI should not be set after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetExpHvci(true,midas::util::FromString<In>(s));
            break;
         }
         case VCC: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3VCC should not be set after specifying the #3Method keyword!");
            }

            new_vcc.SetVcc(true);
            break;
         }
         case VCI: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3VCI should not be set after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetVci(true,midas::util::FromString<In>(s));
            break;
         }
         case VCIH2: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3VCIH2 should not be set after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetVciH2(true,midas::util::FromString<In>(s));
            break;
         }
         case H2LS: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetH2LevelShift(true,midas::util::FromString<Nb>(s));
            break;
         }
         case VMP: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3VMP should not be set after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetVmp(true,midas::util::FromString<In>(s));
            break;
         }
         case VAPT: 
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3VAPT should not be set after specifying the #3Method keyword!");
            }

            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetVapt(true,midas::util::FromString<In>(s));
            break;
         }
         case PADE: 
         {
            new_vcc.SetPade(true);
            break;
         }
         case MODALSFILE: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string file_name="";
            input_s >> file_name;
            new_vcc.SetModalFileName(file_name);
            break;
         }
         case PRIMITIVEBASIS: 
         {
            bool primbas = true;
            new_vcc.SetPrimitiveBasis(primbas);
            break;
         }
         case LIMITMODALBASIS:
         {
            MidasAssert(!use_all_vscf_modals, "LIMITMODALBASIS and USEALLVSCFMODALS cannot be used together!");
            GetLine(Minp,s);                                  // Get new line with input
            limits = midas::util::VectorFromString<In>(s);
            limits_set = true;
            break;
         }
         case USEALLVSCFMODALS:
         {
            MidasAssert(!limits_set, "LIMITMODALBASIS and USEALLVSCFMODALS cannot be used together!");
            use_all_vscf_modals = true;
            break;
         }
         case TESTTRANSFORMER: 
         {
            bool testtran = true;
            new_vcc.SetTestTransformer(testtran);
            break;
         }
         case H2START: 
         {
            bool h2= true;
            new_vcc.SetH2Start(h2);
            break;
         }
         case VMPSTART: 
         {
            bool h2= true;
            new_vcc.SetVmpStart(h2);
            break;
         }
         case LAMBDAS: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb lambda=C_0;
            while (input_s >> lambda)
            {
               new_vcc.AddLambda(lambda);
            }
            break;
         }
         case LAMBDASCAN: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            In nlambda=I_0;
            Nb lambdamin=C_0;
            Nb lambdamax=C_0;
            if (input_s >> nlambda)
            {
               if (nlambda> I_0 && input_s >> lambdamin)
               {
                  if (input_s >> lambdamax || nlambda == I_1)
                  {
                     Nb dellam=C_0;
                     if (nlambda == I_1) 
                     {
                        lambdamax = lambdamin;
                        dellam = C_0;
                     }
                     else
                     {
                        dellam = (lambdamax-lambdamin)/(nlambda-I_1);
                     }
                     for (In il = I_0;il<nlambda;il++)
                     {
                        Nb lambda = lambdamin + il*dellam;
                        new_vcc.AddLambda(lambda);
                     }
                  }
               }
            }
            break;
         }
         case ISO: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetIso(true,midas::util::FromString<In>(s));
            break;
         }
         case SUMNPRIM: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetSumN(true,midas::util::FromString<In>(s));
            break;
         }
         case EMAXPRIM: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEmax(true,midas::util::FromString<Nb>(s));
            break;
         }
         case ZEROPRIM: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetZero(true,midas::util::FromString<Nb>(s));
            break;
         }
         case REPVCCASVCI: 
         {
            new_vcc.SetRepVccAsVci(true);
            break;
         }
         case REPVCIASVCC: 
         {
            new_vcc.SetRepVciAsVcc(true);
            break;
         }
         case DIIS: 
         {
            new_vcc.SetDiis(true);
            GetLine(Minp,s);
            new_vcc.SetNSubspaceVecs(midas::util::FromString<In>(s));
            new_vcc.SetAllowBackstep(false);
            break;
         }
         case TIMEIT: 
         {
            new_vcc.SetTimeIt(true);
            break;
         }
         case SIZEIT: 
         {
            new_vcc.SetSizeIt(true);
            break;
         }
         case PREDIAG: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetPreDiag(midas::util::FromString<In>(s));
            break;
         }
         case NEWTONRAPHSON:
         {
            new_vcc.SetNewtonRaphson(true);
            already_read = new_vcc.ReadNewtonRaphsonInput(Minp,s);
            break;
         }
         case CHECKNEWTONRAPHSON:
         {
            new_vcc.SetCheckNewtonRaphson(true);
            break;
         }
         case INTERMEDIATERESTRICTIONS:
         {
            already_read = new_vcc.ReadIntermediateRestrictions(Minp, s);
            break;
         }
         case TARGETSPACE:
         {
            vci_targeting = true;
            new_vcc.SetTargetSpace(true);
            break;
         }
         case REFERENCE:
         {
            vci_targeting = true;
            new_vcc.SetReference(true);
            break;
         }
         case ALLFUNDAMENTALS:
         {
            vci_targeting = true;
            new_vcc.SetAllFundamentals(true);
            break;
         }
         case ALLFIRSTOVERTONES:
         {
            vci_targeting = true;
            new_vcc.SetAllFirstOvertones(true);
            break;
         }
         case FUNDAMENTALS:
         {
            vci_targeting = true;
            fund = true;
            GetLine(Minp,s);
            new_vcc.SetFundamentals(midas::util::VectorFromString<In>(s),true);
            break;
         }
         case FIRSTOVERTONES:
         {
            vci_targeting = true;
            first = true;
            GetLine(Minp,s);
            new_vcc.SetFirstOvertones(midas::util::VectorFromString<In>(s),first);
            break;
         }
         case RSP:
         {
            s = RspInput(Minp,new_vcc,false);
            already_read = true;
            break;
         }
         case OCCGROUNDSTATE:
         {
            new_vcc.SetVscfInputInVcc();
            b_ground_state = true;
            break;
         }
         case OCCALLFUND:
         {
            new_vcc.SetVscfInputInVcc();
            b_all_fund= true;
            break;
         }
         case OCCFUNDAMENTALS:
         {
            new_vcc.SetVscfInputInVcc();
            b_fundamentals = true;
            GetLine(Minp,s);                                  // Get new line with input
            fundamentals = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCALLFIRSTOVER:
         {
            new_vcc.SetVscfInputInVcc();
            b_all_firstover = true;
            break;
         }
         case OCCFIRSTOVER:
         {
            new_vcc.SetVscfInputInVcc();
            b_firstover= true;
            GetLine(Minp,s);                                  // Get new line with input
            first_overtones = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCGENCOMBI:
         {
            new_vcc.SetVscfInputInVcc();
            already_read = new_vcc.ReadGenCombi(Minp, s, gen_combis);

            break;
         }
         case NMODES:
         {
            GetLine(Minp,s);                                  // Get new line with input
            n_modes_inp = midas::util::FromString<In>(s);
            break;
         }
         case NOOLSEN:
         {
            new_vcc.SetOlsen(false);                           
            break;
         }
         case OCCALLWITHHOMAXE:
         {
            new_vcc.SetVscfInputInVcc();
            occ_all_with_ho_maxe = true;
            GetLine(Minp,s);                                  // Get new line with input
            e_occ_all_with_ho_maxe = midas::util::FromString<Nb>(s);
            break;
         }
         case FCCALC: 
         {
            GetLine(Minp,s);      // Get new line with input
            while(s.substr(I_0,I_1)!="#")
            {
               istringstream input_s(s);
               string fc_opername="";
               while (input_s >> fc_opername)
               {
                  new_vcc.SetFcVciCalc(true);
                  new_vcc.AddFcVciCalcOp(fc_opername);
               }
               GetLine(Minp,s);      // Get new line with input
            }
            already_read = true;
            break;
         }
         case SCALESTEP: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            Nb scale=C_1;
            if (input_s >> scale) 
            {
               new_vcc.SetTrustScaleDown(scale);
            }
            if (input_s >> scale) 
            {
               new_vcc.SetTrustScaleUp(scale);
            }
            if (input_s >> scale) 
            {
               new_vcc.SetMinTrustScale(scale);
            }
            break;
         }
         case EIGVALSEQ: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEigValSeq(midas::util::FromString<In>(s));
            break;
         }
         case TRUEHDIAG:
         {
            new_vcc.SetTrueHDiag(true);                           
            break;
         }
         case FRANCKCONDON:
         {
            new_vcc.SetFranckCondon(true);
            ParseFranckCondonArgs(Minp, new_vcc);
            break;
         }
         case TRANSFORMER:
         {
            method_input_given = true;
            if (  auto_method_set
               )
            {
               MIDASERROR("#3TRANSFORMER should not be set after specifying the #3Method keyword!");
            }

            ParseTransformerArgs(Minp, new_vcc);
            break;
         }
         case IMPROVEDPRECOND: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetImprovedPrecond(midas::util::FromString<In>(s));
            break;
         }
         case LEVEL2SOLVER:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vcc.SetLevel2Solver(meth);
            break;
         }
         case TARGETINGMETHOD:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vcc.SetTargetingMethod(meth);
            break;
         }
         case OVERLAPMIN:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetOverlapMin(midas::util::FromString<Nb>(s));
            break;
         }
         case OVERLAPSUMMIN:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetOverlapSumMin(midas::util::FromString<Nb>(s));
            break;
         }
         case ENERGYDIFFMAX:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEnergyDiffMax(midas::util::FromString<Nb>(s)/C_AUTKAYS); // energy conversion cm^{-1} to au
            break;
         }
         case RESIDTHRFOROTHERS:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetResidThrForOthers(midas::util::FromString<Nb>(s));
            break;
         }
         case ENERTHRFOROTHERS:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEnerThrForOthers(midas::util::FromString<Nb>(s));
            break;
         }
         case NRESVECS: 
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetNresvecs(midas::util::FromString<In>(s));
            break;
         }
         case HARMONICRR:
         {
            new_vcc.SetHarmonicRR(true);
            break;
         }
         case ENERSHIFT:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetEnerShift(midas::util::FromString<Nb>(s));
            break;
         }
         case INDIVIDUALMOLMC:
         {
            vector<vector<In> > individual_mol_mc;
            In nr_mol = I_0;
            string line;
            GetLine(Minp,line);
            nr_mol = midas::util::FromString<In>(s);
            for(In i = I_0; i < nr_mol; ++i)
            {
               In nr_modes = I_0;
               vector<In> modes;
               string line;
               GetLine(Minp,line);
               istringstream iss(line);
               if(line.find("#") == string::npos)
               {
                  iss >> nr_modes;
               }
               else
               {
                  break;
               }
               for(In j = I_0; j < nr_modes; ++j)
               {
                  In mode_nr = -I_1;
                  iss >> mode_nr;
                  modes.push_back(mode_nr);
               }
               individual_mol_mc.emplace_back(modes);
            }
            new_vcc.SetMolMCs(individual_mol_mc);
            break;
         }
         case SCREENINTERMEDS:
         {
            GetLine(Minp,s);
            Nb thresh = midas::util::FromString<Nb>(s);
            new_vcc.SetScreenIntermeds(true);
            new_vcc.SetScreenIntermedsThresh(thresh*thresh); // we check against Norm2 for stability
            break;
         }
         case SCREENINTERMEDSAMPS:
         {
            GetLine(Minp,s);
            Nb thresh = midas::util::FromString<Nb>(s);
            new_vcc.SetScreenIntermedsAmps(true);
            new_vcc.SetScreenIntermedsAmpsThresh(thresh*thresh); // we check against Norm2 for stability
            break;
         }
         case CMPV3ORIGVCC:
         {
            new_vcc.SetCmpV3OrigVcc(true);
            break;
         }
         case CMPV3ORIGVCI:
         {
            new_vcc.SetCmpV3OrigVci(true);
            break;
         }
         case CMPV3ORIGVCCJAC:
         {
            new_vcc.SetCmpV3OrigVccJac(true);
            break;
         }
         case CHECKV3LEFT:
         {
            new_vcc.SetCheckV3Left(true);
            break;
         }
         case FLEXCOUP:
         {
            FlexCoupCalcDef tmp_flex_coup_calc_def;
            s = FlexCoupInput(Minp,tmp_flex_coup_calc_def,input_level);
            new_vcc.SetmFlexCoupCalcDef(tmp_flex_coup_calc_def);
            already_read=true;
            break;
         }
         case DISABLECHECKMETHOD:
         {
            new_vcc.SetCheckMethod(false);
            break;
         }
         case USENEWTENSORCONTRACTIONS:
         {
            already_read = new_vcc.ReadUseNewTensorContractions(Minp, s);
            break;
         }
         case USEOLDVCCSOLVER:
         {
            old_vcc_solver = true;
            break;
         }
         case ADAPTIVEDECOMPTHRESHOLD:
         {
            GetLine(Minp,s);
            Nb threshold_scaling = midas::util::FromString<Nb>(s);
            new_vcc.SetAdaptiveDecompThreshold(threshold_scaling);
            break;
         }
         case ADAPTIVETRFDECOMPTHRESHOLD:
         {
            GetLine(Minp, s);
            Nb rel_thresh_scal = midas::util::FromString<Nb>(s);
            new_vcc.SetAdaptiveTrfDecompThreshold(rel_thresh_scal);
            break;
         }
         case ADAPTIVEDECOMPGUESS:
         {
            new_vcc.SetAdaptiveDecompGuess(true);
            break;
         }
         case MAXNORMCONVCHECK:
         {
            new_vcc.SetMaxNormConvCheck(true);
            break;
         }
         case TENSORDECOMPINFO:
         {
            already_read = new_vcc.ReadVccDecompInfoSet(Minp, s);
            break;
         }
         case SAVERANKINFO:
         {
            new_vcc.SetSaveRankInfo(true);
            break;
         }
         case FULLRANKANALYSIS:
         {
            new_vcc.SetFullRankAnalysis(true);
            break;
         }
         case TRANSFORMERTENSORDECOMPINFO:
         {
            already_read = new_vcc.ReadVccTransformerDecompInfoSet(Minp, s); // 
            break;
         }
         case TRANSFORMERALLOWEDRANK:
         {
            already_read = new_vcc.ReadVccTransformerAllowedRank(Minp, s); // 
            break;
         }
         case TENSORPRODUCTSCREENING:
         {
            GetLine(Minp, s);
            auto screens = midas::util::VectorFromString<Nb>(s);
            auto num_args = screens.size();
            if (  num_args == 1  )
            {
               auto tps = screens.at(0);
               new_vcc.SetTensorProductScreening(tps, tps);
            }
            else if  (  num_args == 2  )
            {
               auto tps = screens.at(0);
               auto ts = screens.at(1);
               new_vcc.SetTensorProductScreening(tps, ts);
            }
            else
            {
               MIDASERROR("VccInput: Wrong number of arguments for TensorProductScreening!");
            }

            break;
         }
         case ADAPTIVETENSORPRODUCTSCREENING:
         {
            GetLine(Minp, s);
            auto adapt = midas::util::FromString<Nb>(s);
            new_vcc.SetAdaptiveTensorProductScreening(adapt);

            break;
         }
         case ITEQMAXITMICRO:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetItEqMaxIterMicro(midas::util::FromString<In>(s));
            break;
         }
         case ITEQMICROTHRFAC:
         {
            GetLine(Minp,s);                                  // Get new line with input
            new_vcc.SetItEqMicroThrFac(midas::util::FromString<Nb>(s));
            break;
         }
         case LAPLACEINFO:
         {
            already_read = new_vcc.ReadLaplaceInfoSet(Minp, s);
            break;
         }
         case CHECKLAPLACEFIT:
         {
            new_vcc.SetCheckLaplaceFit(true);
            break;
         }
         case SIMPLEQUASINEWTON:
         {
            simple_qn = true;
            break;
         }
         case IDJA:
         {
            GetLine(Minp, s);
            Nb idja = midas::util::FromString<Nb>(s);
            new_vcc.SetIdja(idja);
            break;
         }
         case IDJAUNITMATRIXGUESS:
         {
            new_vcc.SetIdjaUnitMatrixGuess(true);
            break;
         }
         case CHECKIDJA:
         {
            new_vcc.SetCheckIdja(true);
            break;
         }
         case CROP:
         {
            GetLine(Minp,s);
            In n_vecs = midas::util::FromString<In>(s);
            if(n_vecs > I_0) new_vcc.SetCrop(true);
            else  MIDASERROR("Number of CROP subspace vectors must be > 0!");
            new_vcc.SetNSubspaceVecs(n_vecs);
            new_vcc.SetAllowBackstep(false); // Niels: CROP sometimes needs to oscillate a little before convergence
            break;
         }
         case PRECONSAVEDRESIDUALS:
         {
            new_vcc.SetPreconSavedResiduals(true);
            break;
         }
         case INDIVIDUALMCDECOMPTHRESH:
         {
            new_vcc.SetIndividualMcDecompThresh(true);

            GetLine(Minp,s);
            auto thresholds = midas::util::VectorFromString<Nb>(s);
            auto num_args = thresholds.size();
            if (  num_args == 1  )
            {
               auto abs = thresholds.at(0);
               new_vcc.SetIndividualMcDecompThreshScaling(abs, C_0);
            }
            else if  (  num_args == 2  )
            {
               auto abs = thresholds.at(0);
               auto rel = thresholds.at(1);
               new_vcc.SetIndividualMcDecompThreshScaling(abs, rel);
            }
            else
            {
               MIDASERROR("VccInput: Wrong number of arguments for IndividualMcDecompThresh!");
            }

            break;
         }
         case NOPRECON:
         {
            new_vcc.SetUsePrecon(false);
            break;
         }
         case IMPROVEDPRECONINSUBSPACEMATRIX:
         {
            new_vcc.SetImprovedPreconInSubspaceMatrix(true);
            break;
         }
         case BACKTRACKINGTHRESH:
         {
            GetLine(Minp,s);
            Nb thresh = midas::util::FromString<Nb>(s);
            new_vcc.SetBacktrackingThresh(thresh);
            break;
         }
         case MAXBACKTRACKS:
         {
            GetLine(Minp,s);
            In max = midas::util::FromString<In>(s);
            new_vcc.SetMaxBacktracks(max);
            break;
         }
         case MAXBACKSTEPS:
         {
            GetLine(Minp,s);
            In max = midas::util::FromString<In>(s);
            new_vcc.SetMaxBacksteps(max);
            break;
         }
         case NOBACKSTEP:
         {
            new_vcc.SetAllowBackstep(false);
            break;
         }
         case ALLOWBACKSTEP:
         {
            new_vcc.SetAllowBackstep(true);
         }
         case TENSORORDERTHRESHOLDSCALING:
         {
            GetLine(Minp,s);
            Nb ts = midas::util::FromString<Nb>(s);
            new_vcc.SetTensorOrderThresholdScaling(ts);
            break;
         }
         case TENSORPRODUCTLOWRANKLIMIT:
         {
            GetLine(Minp, s);
            In r = midas::util::FromString<In>(s);
            new_vcc.SetTensorProductLowRankLimit(r);
            break;
         }
         case USECPSCALINGFORINTERMEDS:
         {
            MidasWarning("CP scalings for identification of combined intermediates is experimental!");
            new_vcc.SetUseCpScalingForIntermeds(true);
            break;
         }
         case BREAKBEFORETRANSFORM:
         {
            In it = midas::input::GetIn(Minp, s);
            new_vcc.SetBreakBeforeTransform(it);
            break;
         }
         case CPVCCDEFAULTS:
         {
            cpvcc_defaults = true;

            break;
         }
         case EXTRANGEMAXEFFEXCILEVEL:
         {
            auto i = midas::input::GetIn(Minp, s);
            MidasAssert(i >= I_0, "Expected non-negative EXTRANGEMAXEFFEXCILEVEL, got "+std::to_string(i));
            new_vcc.SetExtRangeMaxEffExciLevel(i);
            break;
         }
         case EXTRANGEMODES:
         {
            midas::input::GetLine(Minp, s);
            auto v = midas::util::VectorFromString<In>(s);
            new_vcc.SetExtRangeModes(std::set<In>(v.begin(), v.end()));
            break;
         }
         case MATREP:
         {
            midas::input::GetParsedLine(Minp, s);
            auto s_vec = midas::util::StringVectorFromString(s);
            try
            {
               auto type = midas::tdvcc::GetMapStringToEnum<midas::tdvcc::CorrType>().at(s_vec.front());

               // MatRep as default
               if (  s_vec.size() == 1 )
               {
                  new_vcc.EnableSubspaceSolverCalc(type, midas::tdvcc::TrfType::MATREP);
               }
               else if (s_vec.size() == 2)
               {
                  auto trf = midas::tdvcc::GetMapStringToEnum<midas::tdvcc::TrfType>().at(s_vec[1]);
                  new_vcc.EnableSubspaceSolverCalc(type, trf);
               }
               else
               {
                  MIDASERROR("Expected one or two strings in first argument line to #3MATREP");
               }

               if (  type == midas::tdvcc::CorrType::TDMVCC
                  )
               {
                  midas::input::GetParsedLine(Minp, s);
                  MidasAssert(!midas::input::IsKeyword(s), "For MVCC calculations, the number of active modals must be given!");
                  auto act_modals = midas::util::VectorFromString<Uin>(s);
                  new_vcc.SetNActiveModals(act_modals);
               }
            }
            catch(const std::out_of_range&)
            {
               std::stringstream ss;
               ss << "Invalid argument to #3MATREP: '" << s << "'.\n"
                  << "Options are:\n"
                  ;
               for(const auto& kv: midas::tdvcc::GetMapStringToEnum<midas::tdvcc::CorrType>())
               {
                  ss << "   " << kv.first << '\n';
               }
               MIDASERROR(ss.str());
            }
            break;
         }
         case MATREPFVCIANALYSIS:
         {
            new_vcc.SetMatRepFvciAnalysis(true);
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << " is not a Vcc level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // If we want to use VSCF input from a separate VSCF block, check that we have not set any VSCF-related keywords here.
   if (  new_vcc.VscfInputInVcc()
      && (  !new_vcc.VscfReference().empty()
         || new_vcc.UseAllVscf()
         )
      )
   {
      MIDASERROR("VSCF-related keywords have been set in #2Vcc input block. This should not be done if setting UseVscf or UseAllVscf under #3Method!");
   }

   if (  !new_vcc.VscfReference().empty()
      && new_vcc.UseAllVscf()
      )
   {
      MIDASERROR("#3USEALLVSCF and #3USEVSCF cannot be used together!");
   }

   // Post-process if given input for extended range/effective exci. level.
   PostProcessExtendedRangeWithEffectiveExciLevelInput(new_vcc);

   // Are we using general mode combis?
   bool use_gen_combi = !gen_combis.empty();

   // If we have set VCI-related targeting keywords, make sure we are using VCI or VCI
   if (  vci_targeting
      && !( new_vcc.Vci()
         || new_vcc.VciH2()
         || new_vcc.ExpHvci()
         || new_vcc.FvciExpH()
         )
      )
   {
      MIDASERROR("A VCI-related targeting keyword (#3Reference, #3AllFundamentals, etc.) has been set under #2Vcc, but we are not using a VCI method!");
   }

   // Set defaults for CP-VCC that depend on solver threshold
   if (  cpvcc_defaults
      )
   {
      // Use the tensor-based solvers
      new_vcc.SetupTensorDataContCalculation();

      // If the thresholds have not been set, choose CP-VCC defaults
      if (  !resid_thr_set
         )
      {
         new_vcc.SetItEqResidThr(C_I_10_6);
      }
      if (  !ener_thr_set
         )
      {
         new_vcc.SetItEqEnerThr(C_I_10_8);
      }

      // Set adaptive decomp threshold for VCC amplitudes in TensorNlSolver
      if (  new_vcc.AdaptiveDecompThreshold() < C_0
         )
      {
         new_vcc.SetAdaptiveDecompThreshold(5.e-3);
      }
      else
      {
         Mout  << " CPVCCDEFAULTS: AdaptiveDecompThreshold already set to " << new_vcc.AdaptiveDecompThreshold() << ". This will not be overwritten!" << std::endl;
      }

      // Set up individual thresholds for each MC in the error vector
      new_vcc.SetIndividualMcDecompThresh(true);

      const auto tscal = new_vcc.IndividualMcDecompThreshScaling();
      if (  libmda::numeric::float_neq(tscal.first, C_1)
         || !libmda::numeric::float_numeq_zero(tscal.second, C_1)
         )
      {
         Mout  << " CPVCCDEFAULTS: Scaling factors for IndividualMcDecompThreshold already set to: abs = " << tscal.first << ", rel = " << tscal.second << ". This will not be overwritten!" << std::endl;
      }

      // Set Laplace info
      if (  new_vcc.GetLaplaceInfo().empty()
         )
      {
         new_vcc.GetLaplaceInfo().emplace("NOOPT", 1.e-5);
      }
      else
      {
         Mout  << " CPVCCDEFAULTS: LaplaceInfo already set. This will not be overwritten!" << std::endl;
      }

      // Save rank statistics
      new_vcc.SetSaveRankInfo(true);

      // Set CP-VCC transformer varables
      if (  new_vcc.GetTransformerAllowedRank() == 0
         )
      {
         new_vcc.SetTransformerAllowedRank(1000);
      }
      else
      {
         Mout  << " CPVCCDEFAULTS: TransformerAllowedRank already set to " << new_vcc.GetTransformerAllowedRank() << ". This will not be overwritten!" << std::endl;
      }
//      new_vcc.SetAdaptiveTensorProductScreening(1.e-2); // Niels: this has not been tested enough to become a default. But it's a nice feature...

      if (  new_vcc.TensorProductLowRankLimit() == I_0
         )
      {
         new_vcc.SetTensorProductLowRankLimit(20);
      }
      else
      {
         Mout  << " CPVCCDEFAULTS: TensorProductLowRankLimit already set to " << new_vcc.TensorProductLowRankLimit() << ". This will not be overwritten!" << std::endl;
      }

      // Solver threshold
      auto thresh = new_vcc.GetItEqResidThr();

      // Set screening
      auto default_prodscreen = thresh * 1.e-4;
      new_vcc.SetTensorProductScreening(default_prodscreen, default_prodscreen);

      // Add TensorDecompInfo
      auto default_ampdecomp = thresh * 1.e-1;
      auto& info = new_vcc.GetDecompInfoSet();
      if (  info.empty()
         )
      {
         info.emplace("VCCSOLVER", default_ampdecomp);
      }
      else if  (  midas::tensor::GetDecompThreshold(info) > default_ampdecomp
               )
      {
         MidasWarning("TensorDecompInfo for CP-VCC amplitudes has looser threshold than the recommended default!");
      }

      auto default_trfdecomp = thresh * 1.e-3;
      auto& trf_info = new_vcc.GetTransformerDecompInfoSet();
      if (  trf_info.empty()
         )
      {
         trf_info.emplace("VCCTRF", default_trfdecomp);
      }
      else if  (  midas::tensor::GetDecompThreshold(trf_info) > default_trfdecomp
               )
      {
         MidasWarning("TensorDecompInfo for CP-VCC transformer has looser threshold than the recommended default!");
      }
   }

   // Set tensor non-linear solver as default
   if (  new_vcc.Vcc()
      && new_vcc.V3trans()
      && !old_vcc_solver
      && (  !new_vcc.DoRspFuncs()
         || new_vcc.TensorRspFuncs()
         )
      )
   {
      if (  gIoLevel > I_3
         )
      {
         Mout  << " Set TensorNlSolver as default for solving VCC ground-state equations." << std::endl;
      }
      new_vcc.SetupTensorDataContCalculation();

      bool conv_acc_set = new_vcc.NewtonRaphson() || new_vcc.Diis() || new_vcc.GetCrop() || !(new_vcc.GetIdja() < C_0);

      MidasAssert(!(simple_qn && conv_acc_set), "SIMPLEQUASINEWTON cannot be used together with other solver methods!");

      if (  !( simple_qn
            || conv_acc_set
            )
         )
      {
         if (  gIoLevel > I_3
            )
         {
            Mout  << " No other solver method has been specified. Use CROP(3) as default!" << std::endl;
         }
         new_vcc.SetCrop(true);
         new_vcc.SetNSubspaceVecs(3);
         new_vcc.SetAllowBackstep(false); // Niels: CROP sometimes needs to oscillate a little before convergence
      }
   }

   // Checks for TensorNlSolver
   if(new_vcc.UseTensorNlSolver())
   {
      if(new_vcc.NewtonRaphson() && new_vcc.GetIdja() > C_0)
         MIDASERROR("Newton-Raphson and IDJA cannot be used together!");
      if(new_vcc.GetCrop() && new_vcc.GetIdja() > C_0)
         MIDASERROR("CROP and IDJA cannot be used together in the current implementation! It requires saving the previous residual separately.");
      if(new_vcc.GetIdja() > C_0 && !new_vcc.UsePrecon())
         MIDASERROR("It makes no sense to use IDJA with NOPRECON!");
      if(new_vcc.Diis() && new_vcc.GetCrop())
         MIDASERROR("You have to choose between DIIS and CROP!");
      if(new_vcc.GetCrop() && new_vcc.AdaptiveDecompGuess())
         MIDASERROR("Adaptive decomp guess cannot be used together with CROP!");
      if(new_vcc.PreDiag())
         MIDASERROR("PreDiag has not been implemented in TensorNlSolver!");
      if(new_vcc.GetImprovedPrecond())
         MIDASERROR("Improved preconditioning in TensorNlSolver is accessed via #3NewtonRaphson and #4JacobianMaxExci!");
      if(!new_vcc.Vcc())
         MIDASERROR("TensorNlSolver only solves the VCC equations!");
      if (  (  new_vcc.PreconSavedResiduals()
            )
         && !  (  new_vcc.GetCrop()
               || new_vcc.Diis()
               )
         )
      {
         MIDASERROR("#3PreconSavedResiduals can only be used with CROP or DIIS!");
      }
      if (  new_vcc.PreconSavedResiduals()
         && new_vcc.IndividualMcDecompThresh()
         )
      {
         MIDASERROR("#3PreconSavedResiduals and #3IndividualMcDecompThresh cannot be used together!");
      }
      // Set default value for mAdaptiveTrfDecompThreshold if IndividualMcDecompThresh is used.
      if (  new_vcc.IndividualMcDecompThresh()
         && new_vcc.AdaptiveTrfDecompThreshold() < C_0
         )
      {
         new_vcc.SetAdaptiveTrfDecompThreshold( C_I_10_4 );
      }
   }
   // Checks for MatRep calc.
   else if (new_vcc.UsingSubspaceSolver())
   {
      // Subspace solver methods.
      if(!new_vcc.Diis() && !new_vcc.GetCrop())
      {
         if (  gIoLevel > I_3
            )
         {
            Mout  << " No other solver method has been specified. Use CROP(3) as default!" << std::endl;
         }
         new_vcc.SetCrop(true);
         new_vcc.SetNSubspaceVecs(3);
      }
      if(new_vcc.Diis() && new_vcc.GetCrop())
         MIDASERROR("Both DIIS and CROP are enabled - choose one!");
      // And a lot of other keywords have no effect for MatRep, but will just
      // silently not use them for anything.
   }
   else
   {
      if(new_vcc.GetCrop())
         MIDASERROR("CROP can only be used with TensorNlSolver!");
      if(new_vcc.GetIdja() > C_0)
         MIDASERROR("IDJA can only be used with TensorNlSolver!");
      if(!new_vcc.GetDecompInfoSet().empty())
         MIDASERROR("Tensor decomposition can only be used with TensorNlSolver");
      if (  new_vcc.FullRankAnalysis()
         || new_vcc.GetSaveRankInfo()
         )
      {
         MIDASERROR("Ranks can only be analyzed with TensorNlSolver (using the CP-VCC algorithm).");
      }
   }

   // If the response input has not set new values of selected CP-VCC parameters, we use the same as for the ground-state calc.
   if (  new_vcc.RspTransformerAllowedRank() == -I_2
      )
   {
      new_vcc.SetRspTransformerAllowedRank(new_vcc.GetTransformerAllowedRank());
   }

   if (  new_vcc.RspTensorProductScreening().first < C_0
      || new_vcc.RspTensorProductScreening().second < C_0
      )
   {
      Nb tps1 = new_vcc.TensorProductScreening().first;
      Nb tps2 = new_vcc.TensorProductScreening().second;
      new_vcc.SetRspTensorProductScreening(tps1, tps2);
   }

   if (  new_vcc.RspTensorProductLowRankLimit() < I_0
      )
   {
      new_vcc.SetRspTensorProductLowRankLimit(new_vcc.TensorProductLowRankLimit());
   }

   // Check for CP-VCC
   {
      MidasAssert(!(old_vcc_solver && (new_vcc.GetTransformerAllowedRank() != 0)), "The old VCC solver cannot be used with CP-VCC!");

      const auto& info = new_vcc.GetDecompInfoSet();
      const auto& trf_info = new_vcc.GetTransformerDecompInfoSet();
      MidasAssert(info.empty() == trf_info.empty(), "Decompinfo has not been set for both solver and transformer!");
      bool decomp = !info.empty();

      if (  new_vcc.DoRsp()
         && decomp
         )
      {
         MidasWarning("Run response calculations with CP-VCC. This is still experimental!");
         MidasAssert(!new_vcc.GetVccRspDecompInfo().empty(), "For CP-VCC the response solver needs decompinfo as well!");
         MidasAssert(!new_vcc.GetVccRspTrfDecompInfo().empty(), "For CP-VCC the response transformer needs decompinfo as well!");
      }
   }


   // Werner: Are the following first and the third line in the following four lines necessary? 
   // the default for mNewtonRaphson in the iterative eq. solver is false.
   if (  new_vcc.PreDiag()
      )
   {
      new_vcc.SetNewtonRaphson(false);
   }
   if (  new_vcc.PreDiag()
      )
   {
      new_vcc.SetDiis(false);
   }
   if (  new_vcc.GetImprovedPrecond()
      )
   {
      new_vcc.SetNewtonRaphson(false);
   }
   if (  new_vcc.GetImprovedPrecond()
      )
   {
      new_vcc.SetDiis(false);
   }
   if (  new_vcc.NlanczosRspFuncs()>0)
   {
      if(new_vcc.Vcc())
      {
         for(In i=0; i<new_vcc.NlanczosRspFuncs(); ++i)
         {
            new_vcc.LanczosRspFuncSetNHermTrue(i);
         }
      }
   }

   // Temporary check to give an Error if a NHerm chain is done on a VCI calc.
   if(new_vcc.NlanczosRspFuncs()>0)
   {
      for(In i=0; i<new_vcc.NlanczosRspFuncs(); ++i)
      {
         if(new_vcc.LanczosRspFuncGetNHerm(i) && !new_vcc.Vcc())
         {
            Mout << " WARNING: In VccInput.cc:: Cannot do VCI with nherm-lanczos only with nherm-band lanczos." << endl;
         }
      }
   }

   if (all_first && first)
   {
      MIDASERROR("Both all first overtones and selected first overtones given. Confusion!"); 
   }
   if (all_fund&& fund)
   {
      MIDASERROR("Both all fundamentals and selected fundamentals given. Confusion!"); 
   }
   if (target && (all_first || all_fund || reference|| fund || first))
   {
      Mout << " Target states set by TargetVectors "<< endl;
      Mout << " At the same time ";
      if (all_fund) 
      {
         Mout << " All fundamentals ";
      }
      if (all_first) 
      {
         Mout << " All first overtones ";
      }
      if (reference) 
      {
         Mout << " Reference ";
      }
      if (fund) 
      {
         Mout << " Selected Fundamentals ";
      }
      if (first) 
      {
         Mout << " Selected first overtones ";
      }
      Mout << " was set " << endl;
      MIDASERROR("Target set together with conflicting keyword ");
   }

   if (occ_vec.size()!=I_0) 
   {
      n_modes_inp = occ_vec.size();
   }
   if (n_modes_inp == I_0) 
   {
      if (gOperatorDefs.GetNrOfOpers() != 0 && new_vcc.Oper() != "") 
      {
         //In oper_nr = gOperatorDefs.GetOperatorNr(new_vcc.Oper());
         //OpDef* op_def = &gOperatorDefs[oper_nr];
         OpDef* op_def = &(gOperatorDefs.GetOperator(new_vcc.Oper()));
         n_modes_inp = op_def->NmodesInOp();
      }
      else if(gOperatorDefs.GetNrOfOpers() == 1)
      {
         Mout  << " Nr. of modes and operator in VCC is still not set. \n"
               << " I will assume the one and only operator available at this stage will be used " 
               << std::endl;
         std::string s_oper = gOperatorDefs[I_0].Name();
         new_vcc.SetOper(s_oper);
         //In oper_nr = gOperatorDefs.GetOperatorNr(new_vcc.Oper());
         //OpDef* op_def = &gOperatorDefs[oper_nr];
         OpDef* op_def = &(gOperatorDefs.GetOperator(new_vcc.Oper()));
         n_modes_inp = op_def->NmodesInOp();
      }
      else
      {
         // Check if all operators have same number of modes.
         MidasAssert(gOperatorDefs.GetNrOfOpers() > 0, " No operators set. Cannot guess number of modes!");

         n_modes_inp = gOperatorDefs[I_0].NmodesInOp();
         for(In iop = I_1; iop < gOperatorDefs.GetNrOfOpers(); ++iop)
         {
            MidasAssert(gOperatorDefs[iop].NmodesInOp() == n_modes_inp, "No operator set in VCC input and given operators have different numbers of modes. Cannot guess number of modes!");
         }
      }
   }

   // Now modes are set, set default limits to 6 VSCF modals per mode
   if (  !limits_set
      && !use_all_vscf_modals
      )
   {
      limits.resize(n_modes_inp);
      for(auto& l : limits)
      {
         l = I_6;
      }
   }

   // Check name of calculation and assign default
   if (  s_name.empty()
      ) 
   {
      s_name = new_vcc.Oper()+"_"+new_vcc.Basis(); // Default name (base) is oper_basis
   }
   new_vcc.SetPrefix(s_name);
   new_vcc.SetName(s_name);

   // Check that the prefix is unique!
   for(In i = I_0; i<gVccCalcDef.size()-1; ++i)
   {
      const auto& vcc = gVccCalcDef[i];
      MidasAssert(vcc.Prefix() != new_vcc.Prefix(), "Vcc calculations name '" + vcc.Prefix() + "' is already occupied");
   }

   // if DUPLICATION of input is wanted then do it 
   if (n_dup > 1)
   {
      Mout << " Creating occupation input in " << n_dup << " duplicates " << endl;
      In noccv  = occ_vec.size();
      In noccm  = occmax_vec.size();
      In noccme = occmaxexprmode_vec.size();
      In nlim   = limits.size();
      for (In i_dup=0;i_dup<n_dup-1;i_dup++)
      {
         for (In i=0;i<noccv;i++) occ_vec.push_back(occ_vec[i]);
         for (In i=0;i<noccm;i++) occmax_vec.push_back(occmax_vec[i]);
         for (In i=0;i<noccme;i++) occmaxexprmode_vec.push_back(occmaxexprmode_vec[i]);
         for (In i=0;i<nlim;i++) limits.push_back(limits[i]);
      }
   }

   // Construct name for Vcc and store the Vcc input information on a VccCalcDef on the
   // global gVccCalcDef vector.
   // If occui was given more vcc inputs are construct.

   In nvcc = 1;
   // check increment maxima 
   bool old_style=false;
   if (!occ_all_with_ho_maxe && occmax_vec.size()>0 )
   {
      old_style=true;
      if (occmax_vec.size() != occ_vec.size()) 
      {
         MIDASERROR("Input error, occupation and occuincrement vector not same size ");
      }
      for (In i=0;i<occmax_vec.size();i++) 
      {
         nvcc  *= occmax_vec[i]+1;
         if (nvcc  >1) 
         {
            break;
         }
      }
      if (gDebug && nvcc  > 1) 
      {
         Mout  << " Constructing vcc inputs from occ & occuin vectors" << endl;
      }
   }
   if (occmaxexprmode_vec.size()>0 )
   {
      if (occmax_vec.size() != occ_vec.size()) 
      {
         MIDASERROR("Input error, occupation and exprmode vector not same size ");
      }
   }

   // Info for new type fundamentals and overtone input 
   if (b_all_fund) 
   {
      fundamentals.resize(n_modes_inp);
      for (In i_f = I_0;i_f<fundamentals.size();i_f++) 
      {
         fundamentals[i_f]=i_f;
      }
   }
   if (b_all_firstover) 
   {
      first_overtones.resize(n_modes_inp);
      for (In i_f = I_0;i_f<first_overtones.size();i_f++) 
      {
         first_overtones[i_f]=i_f;
      }
   }
   In n_auto_types = I_0;
   if (b_ground_state) 
   {
      n_auto_types ++;
   }
   if (b_all_fund || b_fundamentals) 
   {
      n_auto_types += fundamentals.size();
   }
   if (b_all_firstover || b_firstover ) 
   {
      n_auto_types += first_overtones.size();
   }
   nvcc  += n_auto_types;

   nvcc += gen_combis.size();
   n_auto_types += gen_combis.size();

   if (nvcc==1&&n_auto_types==I_0)
   {
      new_vcc.SetOccVec(occ_vec);
      new_vcc.SetModalLimits(limits);
      new_vcc.SetModalLimitsInput(true);
      string s_name2 = s_name +"_"+std::to_string(0)+"."+std::to_string(0); 
      new_vcc.SetName(s_name2);
      AddNewVcc(sequence,false,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
      //if (sequence)
      //{
         //string s_name = new_vcc.Name();
         //new_vcc.SetUseAvailable(true);
         //new_vcc.SetRestart(true);
         //new_vcc.SetSequence(true);
         //for (In i_max_exci=i_seq_b;i_max_exci<=i_seq_e;i_max_exci++)
         //{
            //if (i_max_exci==i_seq_b) new_vcc.SetMaxExciLevel(i_max_exci);
            //if (i_max_exci>i_seq_b) 
            //{
               //gVccCalcDef.push_back(new_vcc);
               //gVccCalcDef[gVccCalcDef.size()-I_1].SetMaxExciLevel(i_max_exci);
            //}
            //string s_name2 = s_name+"_seq_"+std::to_string(i_max_exci); 
            //gVccCalcDef[gVccCalcDef.size()-I_1].SetName(s_name2);
         //}
      //}
   }
   if (old_style && nvcc>1)
   {
      In first_new = gVccCalcDef.size()-I_1;

      gVccCalcDef[first_new].SetOccVec(occ_vec);
      gVccCalcDef[first_new].SetModalLimits(limits);
      gVccCalcDef[first_new].SetModalLimitsInput(true);
      string s_name2 = s_name +"_"+std::to_string(0)+"."+std::to_string(0); 
      gVccCalcDef[first_new].SetName(s_name2);
      AddNewVcc(sequence,false,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
      //if (sequence)
      //{
         //VccCalcDef& new_vcc2 = gVccCalcDef[gVccCalcDef.size()-I_1];  // new_oper is a reference to the new operator
         //string s_name = new_vcc2.Name();
         //new_vcc2.SetUseAvailable(true);
         //new_vcc2.SetRestart(true);
         //new_vcc2.SetSequence(true);
         //for (In i_max_exci=i_seq_b;i_max_exci<=i_seq_e;i_max_exci++)
         //{
            //if (i_max_exci==i_seq_b) new_vcc2.SetMaxExciLevel(i_max_exci);
            //if (i_max_exci>i_seq_b) 
            //{
               //gVccCalcDef.push_back(new_vcc2);
               //gVccCalcDef[gVccCalcDef.size()-I_1].SetMaxExciLevel(i_max_exci);
            //}
            //string s_name2 = s_name+"_seq_"+std::to_string(i_max_exci); 
            //gVccCalcDef[gVccCalcDef.size()-I_1].SetName(s_name2);
         //}
      //}

      // Find max_exci_level from max_exci and/or occmax_vec
      In max_exci_level = 0;
      for (In i=0;i<occmax_vec.size();i++)
      {
         if (occmax_vec[i]>occ_vec[i]) 
         {
            max_exci_level++;
         }
      }
      if (max_exci_set) 
      {
         max_exci_level = min(max_exci,max_exci_level);
      }

      ModeCombiOpRange range = ConstructModeCombiOpRange(max_exci_level,occmaxexprmode_vec,occ_vec.size());
      In n_mode_combi = range.Size();

      // i_pur = 0, count, i_pur = 1, do addition
      // the count round will count the number of vcc and reserve space for the 
      // subsequent addition of the VccCalcDef 
      bool countfirst=true;
      In i_pur_first = 0;
      if (countfirst) 
      {
         i_pur_first=1;
      }
      for (In i_pur=i_pur_first;i_pur<2;i_pur++)
      {
         // count variable.
         In i_vscf_count = 1;
         for (In i_mode_combi=1;i_mode_combi<n_mode_combi;i_mode_combi++)
         {
            const ModeCombi& modes_excited = range.GetModeCombi(i_mode_combi);
            In n_excited = modes_excited.Size();
            if (gIoLevel > 5) 
            {
               Mout << " The modes excited are: ";
               for (In i=0;i<n_excited;i++) 
               {
                  Mout << " " << modes_excited.Mode(i);
               }
               Mout << endl;
            }
   
            vector<In> occ(n_excited);
            vector<In> occmax(n_excited);
   
            for (In i=0;i<n_excited;i++) 
            {
               In mode   = modes_excited.Mode(i);
               occ[i]    = occ_vec[mode]; 
               occmax[i] = occmax_vec[mode];
            }
            if (gIoLevel > 5 || gDebug )
            {
               Mout << " partial occ ";
               for (In i=0;i<n_excited;i++) 
               {
                  Mout << " " << occ[i];
               }
               Mout << endl;
               Mout << " partial occmax ";
               for (In i=0;i<n_excited;i++) 
               {
                  Mout << " " << occmax[i];
               }
               Mout << endl;
            }
   
            MultiIndex mi_for_modecombi(occ,occmax,"LOWHIG",true);
   
            vector<In> occ2(occ);          // The active part.
            vector<In> occ_vec_new(occ_vec);  // The full occupation vector.
   
            In n_vscf = mi_for_modecombi.Size();
            for (In i_vscf=0;i_vscf<n_vscf;i_vscf++)
            {
               mi_for_modecombi.IvecForIn(occ2,i_vscf);
               for (In i=0;i<n_excited;i++) 
               {
                  In mode   = modes_excited.Mode(i);
                  occ_vec_new[mode] = occ2[i]; 
               }
   
               if (max_sum_n_set)
               {
                  In sum_n=0;
                  for (In i=0;i<occ_vec_new.size();i++) 
                  {
                     sum_n += occ_vec_new[i];
                  }
                  if (max_sum_n < sum_n) 
                  {
                     continue;
                  }
               }
   
               if (max_exci_set)
               {
                  In sum_excimodes=0;
                  for (In i=0;i<occ_vec_new.size();i++) 
                  {
                     if (occ_vec_new[i] != occ_vec[i]) 
                     {
                        sum_excimodes++;
                     }
                  }
                  if (max_exci < sum_excimodes) 
                  {
                     continue;
                  }
               }
   
               if (gIoLevel > 5 || gDebug)
               {
                  Mout << " Occ: [";
                  In n_mod = occ_vec_new.size()-1;
                  for (In i_op_mode=0;i_op_mode<n_mod;i_op_mode++)
                  {
                     Mout << occ_vec_new[i_op_mode] << ",";
                  }
                  if (occ_vec_new.size()>0) 
                  {
                     Mout << occ_vec_new[occ_vec_new.size()-1];
                  }
                  Mout << "]" << endl;
               }
   
               if (i_pur ==1)
               {
                  string s_name2 = s_name +"_"+std::to_string(i_mode_combi)+"."+std::to_string(i_vscf); 
                  gVccCalcDef.push_back(gVccCalcDef[first_new]);  
                  // Use direct adressing since reference may gone wrong by now due to push_back
                  In new_add = gVccCalcDef.size()-1;
                  gVccCalcDef[new_add].SetName(s_name2);
                  gVccCalcDef[new_add].Reserve();
                  gVccCalcDef[new_add].SetOccVec(occ_vec_new);
                  gVccCalcDef[new_add].SetModalLimits(limits);
                  gVccCalcDef[new_add].SetModalLimitsInput(true);
                  AddNewVcc(sequence,false,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
                  //if (sequence)
                  //{
                     //VccCalcDef& new_vcc2 = gVccCalcDef[gVccCalcDef.size()-I_1];  // new_oper is a reference to the new operator
                     //string s_name = new_vcc2.Name();
                     //new_vcc2.SetUseAvailable(true);
                     //new_vcc2.SetRestart(true);
                     //new_vcc2.SetSequence(true);
                     //for (In i_max_exci=i_seq_b;i_max_exci<=i_seq_e;i_max_exci++)
                     //{
                        //if (i_max_exci==i_seq_b) new_vcc2.SetMaxExciLevel(i_max_exci);
                        //if (i_max_exci>i_seq_b) 
                        //{
                           //gVccCalcDef.push_back(new_vcc2);
                           //gVccCalcDef[gVccCalcDef.size()-I_1].SetMaxExciLevel(i_max_exci);
                        //}
                        //string s_name2 = s_name+"_seq_"+std::to_string(i_max_exci); 
                        //gVccCalcDef[gVccCalcDef.size()-I_1].SetName(s_name2);
                     //}
                  //}
               }
               i_vscf_count++;
   
            }
         }
         if (i_pur == 0)
         {
            In nvscf_tot = first_new+1+i_vscf_count;
            gVccCalcDef.reserve(nvscf_tot);
            Mout << i_vscf_count << " vcc input will be constructed " << endl;
         }
         if (i_pur == 1)  
         {
            Mout << i_vscf_count << " vcc input has been constructed " << endl;
         }
      }
   }

   if (n_auto_types>I_0)
   {
      if (old_style) 
      {
         MIDASERROR ( " Do not mix input styles - either occumax or other way, not both");
      }
      In i_vscf = I_0;
      In first_new = gVccCalcDef.size()-1;
      gVccCalcDef[first_new].SetModalLimits(limits);
      gVccCalcDef[first_new].SetModalLimitsInput(true);
      if (sequence)
      {
         gVccCalcDef[first_new].SetUseAvailable(true);
         gVccCalcDef[first_new].SetRestart(true);
         gVccCalcDef[first_new].SetSequence(true);
      }
      vector<In> occ_vec_new(n_modes_inp);
      bool first = true;
      //Mout << " n_auto_types " << n_auto_types << endl;
      if (b_ground_state)
      {
         string s_name2 = s_name +"_"+std::to_string(i_vscf); 
         //if (!first) gVccCalcDef.push_back(gVccCalcDef[first_new]);  
         AddNewVcc(sequence,!first,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
         In new_add = gVccCalcDef.size()-1;
         gVccCalcDef[new_add].SetName(s_name2);
         gVccCalcDef[new_add].Reserve();
         for (In i = I_0; i < occ_vec_new.size(); i++)
         {
            occ_vec_new[i] = I_0; // Zero.
         }
         if (gIoLevel > I_10 || gDebug)
         {
            Mout << " Occupation Vector: " << occ_vec_new << std::endl;
         }
         gVccCalcDef[new_add].SetOccVec(occ_vec_new);
         i_vscf++;
         first=false;
      }
      if (b_fundamentals||b_all_fund)
      {
         for (In i_f = I_0;i_f<fundamentals.size();i_f++)
         {
            string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            //if (!first || (first && sequence)) AddNewVcc(sequence,i_seq_b,i_seq_e);
            AddNewVcc(sequence,!first,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
            //if (!first) gVccCalcDef.push_back(gVccCalcDef[first_new]);  
            In new_add = gVccCalcDef.size()-1;
            gVccCalcDef[new_add].SetName(s_name2);
            gVccCalcDef[new_add].Reserve();
            for (In i=I_0;i<occ_vec_new.size();i++) 
            {
               occ_vec_new[i]=I_0; // Zero. 
            }
            if (fundamentals[i_f] > n_modes_inp)
            {
               MIDASERROR(" Mode nr. beyond nr of modes for fundamentals ");
            }
            occ_vec_new[fundamentals[i_f]] = I_1;
            if (gIoLevel > I_10 || gDebug)
            {
               Mout << " Occupation Vector: " << occ_vec_new << std::endl;
            }
            gVccCalcDef[new_add].SetOccVec(occ_vec_new);
            i_vscf++;
            first=false;
         }
      }
      if (b_firstover||b_all_firstover)
      {
         for (In i_f = I_0;i_f<first_overtones.size();i_f++)
         {
            string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            AddNewVcc(sequence,!first,i_seq_b,i_seq_e,i_seq_ord_b,i_seq_ord_e);
            //if (!first) gVccCalcDef.push_back(gVccCalcDef[first_new]);  
            In new_add = gVccCalcDef.size()-1;
            gVccCalcDef[new_add].SetName(s_name2);
            gVccCalcDef[new_add].Reserve();
            for (In i=I_0;i<occ_vec_new.size();i++) 
            {
               occ_vec_new[i]=I_0; // Zero. 
            }
            if (first_overtones[i_f] > n_modes_inp) 
            {
               MIDASERROR(" Mode nr. beyond nr of modes for first overtone inp ");
            }
            occ_vec_new[first_overtones[i_f]] = I_2;
            if (gIoLevel > I_10 || gDebug)
            {
               Mout << " Occupation Vector: " << occ_vec_new << std::endl;
            }
            gVccCalcDef[new_add].SetOccVec(occ_vec_new);
            i_vscf++;
            first=false;
         }
      }
      if (  use_gen_combi
         )
      {
         Mout << " Nr of gen. combis: " << gen_combis.size() << std::endl;

         bool first = true;

         // Loop over input vectors
         for (const auto& gen_combi : gen_combis)
         {
            if (  gen_combi.size() > n_modes_inp
               )
            {
               MIDASERROR("Number of excited modes in gen. combi. is larger than the total number of modes!");
            }

            // Emplace new VCC calculation
            std::string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            AddNewVcc(sequence, !first, i_seq_b, i_seq_e, i_seq_ord_b, i_seq_ord_e);
            gVccCalcDef.back().SetName(s_name2);
            gVccCalcDef.back().Reserve();

            // Zero occ vec
            for(auto& occ : occ_vec_new)
            {
               occ = I_0;
            }

            // Set excitation elements
            for(const auto& exci : gen_combi)
            {
               const auto& exci_mode = exci.first;
               const auto& exci_level = exci.second;

               if (  exci_mode > n_modes_inp - I_1
                  )
               {
                  MIDASERROR("Excitation '" + std::to_string(exci_mode) + "^" + std::to_string(exci_level) + "' is not valid for a system with " + std::to_string(n_modes_inp) + " vibrational modes!");
               }

               if (  occ_vec_new[exci_mode] == I_0
                  )
               {
                  occ_vec_new[exci_mode] = exci_level;
               }
               else
               {
                  MIDASERROR("The same mode cannot be excited multiple times!");
               }
            }

            // Set occ vector in new VCC
            gVccCalcDef.back().SetOccVec(occ_vec_new);

            first = false;
            ++i_vscf;
         }
      }
   }
   if (occ_all_with_ho_maxe)
   {
      In i_vscf = I_0;
      In first_new = gVccCalcDef.size()-1;
      vector<In> max_occ_vec(n_modes_inp);
      if (occmax_vec.size()>0 || occmax_vec.size()== max_occ_vec.size()) 
      {
           max_occ_vec = occmax_vec;
      }
      Mout << " Create VCC input for states with HO energy below " << e_occ_all_with_ho_maxe << endl;
      string oper_name = gVccCalcDef[first_new].Oper();
      MaxHoEner getting_occvecs(max_occ_vec,oper_name,e_occ_all_with_ho_maxe);
      vector<InVector> i_vec_vec;
      getting_occvecs.GetAllOccVecs(i_vec_vec);
      In n_new_states=i_vec_vec.size();
      bool first = true;
      for (In i_state=I_0; i_state<n_new_states;i_state++)
      {
         string s_name2 = s_name +"_"+std::to_string(i_vscf);         
         if (!first) 
         {
            gVccCalcDef.push_back(gVccCalcDef[first_new]);
         }
         In new_add = gVccCalcDef.size()-1;
         gVccCalcDef[new_add].SetName(s_name2);
         gVccCalcDef[new_add].Reserve();
         gVccCalcDef[new_add].SetOccVec(i_vec_vec[i_state]);
         first=false;
         i_vscf++;
      }
   }

   return s;
}
/**
* Initialize Vcc variables
* */
void InitializeVcc()
{
   // Mout << " Initializing vcc variables: " << endl;
   gDoVcc              = false; 
}
