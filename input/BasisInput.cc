#include "input/BasisInput.h"

#include <iostream>
#include <string>

#include "input/Input.h"
#include "input/IsKeyword.h"
#include "input/BasDef.h"
#include "input/IsKeyword.h"
#include "util/Path.h"

/**
 * Check if all One mode bases are the same type.
 *
 * @param aBasis   The basis to check.
 * @param aType    The type to check for.
 *
 * @return         Returns true if all bases are the same type, othrewise false.
 **/
bool CheckBasisAllType
   (  const BasDef& aBasis
   ,  const std::string& aType
   )
{
   for (In i_l_b = I_0; i_l_b < aBasis.GetmNoModesInBas(); ++i_l_b) 
   {
      if (aBasis.OneModeType(i_l_b) != aType) 
      {
         return false;
      }
   }
   return true;
}

/**
 * Set the type of a basis.
 *
 * @param aBasis    The basis to set the type for. 
 *                  On output will have its type set.
 **/
void SetBasisType
   (  BasDef& aBasis
   )
{
   if (CheckBasisAllType(aBasis, "HO") || aBasis.GetmUseHoBasis())
   {
      // In this case all one-mode basis functions are HO
      aBasis.SetmBasSetType("PROD-ALLHO");
   }
   else if (CheckBasisAllType(aBasis, "Gauss") || aBasis.GetmUseGaussianBasis())
   {
      aBasis.SetmBasSetType("PROD-ALLGAUSS");
   }
   else if (CheckBasisAllType(aBasis, "Bsplines") || aBasis.GetmUseBsplineBasis())
   {
      aBasis.SetmBasSetType("PROD-ALLBSPLINES");
   }
   else
   {
      // default basis type is simply product which does not promise too much.
      aBasis.SetmBasSetType("PROD");
   }
}

/**
 * Read and check operator input 
 * Read until next s = "#i..." i<3 which is returned.
 **/
std::string BasisInput(std::istream& Minp)
{
   // Put into global input data 
   gBasis.push_back(BasDef());
   // Local initializations 
   BasDef& new_basis = gBasis[gBasis.size()-1];
   new_basis.Reserve();

   enum INPUT  
      {  ERROR
      ,  IOLEVEL
      ,  NAME
      ,  DEFINE
      ,  DUPLICATE
      ,  HOBASIS
      ,  USESCALINGFREQS
      ,  NPRIMBASISFUNCTIONS
      ,  PRIMBASISDENSITY
      ,  MAXNOPRIMBASISFUNCTIONS
      ,  READBOUNDSFROMFILE
      ,  NOBASBEYONDMAXPOT
      ,  BSPLINEBASIS
      ,  GAUSSIANBASIS
      ,  TURNINGPOINT
      ,  SCALBOUNDS
      ,  GRADSCALBOUNDS
      ,  KEEPUNBOUNDEDBSPLINES
      ,  PLOTBASIS
      ,  BASISGLQUADRATUREPOINTS
      ,  GAUSSLEGENDRERELACCURACY
      };
   const std::map<std::string, INPUT> input_word =
   {
      {"#3IOLEVEL",IOLEVEL},
      {"#3NAME",NAME},
      {"#3DEFINE",DEFINE},
      {"#3DUPLICATE",DUPLICATE},
      {"#3HOBASIS",HOBASIS},
      {"#3USESCALINGFREQS",USESCALINGFREQS},
      {"#3NPRIMBASISFUNCTIONS",NPRIMBASISFUNCTIONS},
      {"#3PRIMBASISDENSITY",PRIMBASISDENSITY},
      {"#3MAXNOPRIMBASISFUNCTIONS",MAXNOPRIMBASISFUNCTIONS},
      {"#3READBOUNDSFROMFILE",READBOUNDSFROMFILE},
      {"#3NOBASBEYONDMAXPOT",NOBASBEYONDMAXPOT},
      {"#3BSPLINEBASIS",BSPLINEBASIS},
      {"#3GAUSSIANBASIS",GAUSSIANBASIS},
      {"#3TURNINGPOINT",TURNINGPOINT},
      {"#3SCALBOUNDS",SCALBOUNDS},
      {"#3GRADSCALBOUNDS",GRADSCALBOUNDS},
      {"#3KEEPUNBOUNDEDBSPLINES",KEEPUNBOUNDEDBSPLINES},
      {"#3PLOTBASIS",PLOTBASIS},
      {"#3BASISGLQUADRATUREPOINTS",BASISGLQUADRATUREPOINTS},
      {"#3GAUSSLEGENDRERELACCURACY", GAUSSLEGENDRERELACCURACY}
   };

   string s="";
   In input_level = 3;
   In n_dup       = 1;

   bool already_read = false;
   
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !( s.substr(I_0,I_1) == "#" 
            && atoi(s.substr(I_1,I_1).c_str()) < input_level
            )
         )    // readin until input levels point up
   {
      already_read = false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            
            gBasisIoLevel = midas::util::FromString<In>(s);
            
            break;
         }
         case NAME:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string basis_name="";
            input_s >> basis_name; 
            new_basis.SetmBasName(basis_name);
            break;
         }
         case DUPLICATE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            n_dup = midas::util::FromString<In>(s);
            break;
         }
         case DEFINE:
         {
            new_basis.SetmUserDefinedBasis(true);
            std::string s_mode = "";
            std::string s_type = "";

            new_basis.SetmBasSetType("Product");
            // get new lines with mode and basis defs.
            while (  midas::input::GetLine(Minp,s)
                  && midas::input::NotKeyword(s)
                  )          
            {
               std::istringstream input_s(s);
               input_s >> s_mode; 
               input_s >> s_type; 
               s.erase(s.find(s_mode), s_mode.size());
               s.erase(s.find(s_type), s_type.size());
               while (s.find(" ") != s.npos)
               {
                  s.erase(s.find(" "), I_1);   // Delete ALL blanks
               }
               
               // now s_mode, s_type, and aux. defs. are available
               // next, get mode nr, (and add to global list if not already there).
               GlobalModeNr i_mode = gOperatorDefs.ModeNr(s_mode, true);

               // create a one-mode basis Def and add to full basis def.
               In add = -I_1;
               if (s_type == "HO")
               {
                  gOneModeHoDefs.push_back(OneModeHoDef(i_mode, s_type, s));
                  add = gOneModeHoDefs.size() - I_1;
               }
               else if (s_type == "Gauss")
               {
                  gOneModeGaussDefs.push_back(OneModeGaussDef(i_mode, s_type, s));
                  add = gOneModeGaussDefs.size() - I_1;
               }
               else if (s_type == "Bsplines")
               {
                  gOneModeBsplDefs.push_back(OneModeBsplDef(i_mode, s_type, s));
                  add = gOneModeBsplDefs.size() - I_1;
               }
               OneModeBasDef bas_def(s_type, add);
               new_basis.AddToBasDef(bas_def, bas_def.GetGlobalModeNr());
            }
            already_read = true;
            break;
         }
         case HOBASIS:
         {
            new_basis.SetmUseHoBasis(true);
            midas::input::GetLine(Minp,s);
            
            if (  midas::input::NotKeyword(s)
               )
            {
               new_basis.SetmHoQnrs(midas::util::VectorFromString<In>(s));
            }
            else
            {
               Mout  << " Using default mHoQnrs: " << new_basis.GetmHoQnrs() << std::endl;
               already_read = true;
            }

            break;
         }
         case GAUSSIANBASIS:
         {
            new_basis.SetmUseGaussianBasis(true);
            new_basis.SetmAutoGenBasis(true);
            midas::input::GetLine(Minp,s);       
            
            if (  midas::input::NotKeyword(s)
               )
            {
               new_basis.SetmGaussianExps(midas::util::VectorFromString<Nb>(s));
            }
            else
            {
               already_read = true;
            }

            break;
         }
         case BSPLINEBASIS:
         {
            new_basis.SetmUseBsplineBasis(true);
            new_basis.SetmAutoGenBasis(true);
            midas::input::GetLine(Minp,s);                   

            if (  midas::input::NotKeyword(s)
               )
            {
               new_basis.SetmBsplineOrders(midas::util::VectorFromString<In>(s));
            }
            else
            {
               already_read = true;
            }

            break;
         }
         case USESCALINGFREQS:
         {
            new_basis.SetmUsingScalingFreqs(true);
            break;
         }
         case NPRIMBASISFUNCTIONS:
         {
            new_basis.SetmAutoGenBasis(true);
            new_basis.SetmUsePrimBasisDens(false);

            midas::input::GetLine(Minp,s);
            new_basis.SetmNoPrimBasisFunctions(midas::util::VectorFromString<In>(s));

            break;

         }
         case PRIMBASISDENSITY:
         {
            new_basis.SetmAutoGenBasis(true);
            new_basis.SetmUsePrimBasisDens(true);
            
            midas::input::GetLine(Minp,s);                      
            new_basis.SetmPrimBasisDens(midas::util::VectorFromString<Nb>(s));
            
            break;

         }
         case MAXNOPRIMBASISFUNCTIONS:
         {
            midas::input::GetLine(Minp,s);                   
            new_basis.SetmMaxNoPrimBasisFunctions(midas::util::FromString<In>(s));
            
            break;
         }
         case READBOUNDSFROMFILE:
         {
            new_basis.SetmReadBoundsFromFile(true);
            midas::input::GetLine(Minp,s);

            // If the next line is not a keyword, it is the path to the grid-bounds file
            if (  !midas::input::IsKeyword(s)
               )
            {
               // Set filename (start from MainDir if it is a relative path)
               auto path = midas::path::IsRelPath(s) ? gMainDir + "/" + s : s;
               new_basis.SetGridBoundsFilePath(path);
            }
            else
            {
               already_read = true;
            }

            break;
         }
         case NOBASBEYONDMAXPOT:
         {
            new_basis.SetNoBasBeyondMaxPot(true);
            break;
         }
         case SCALBOUNDS:
         {
            midas::input::GetLine(Minp, s);
            istringstream input_s(s);
            Nb scal= 1.50;
            input_s >> scal;
            Nb max_ext = 20.0;
            input_s >> max_ext;
            new_basis.SetScalBoundsFromFile(scal, max_ext);
            new_basis.SetScalBounds(true);
            break;
         }
         case GRADSCALBOUNDS: 
         {
            midas::input::GetLine(Minp, s);
            istringstream input_s(s);
            Nb gradscal= 1.50;
            input_s >> gradscal;
            Nb grad_max_ext = 30.0;
            input_s >> grad_max_ext;
            new_basis.SetGradScalBoundsFromFile(gradscal, grad_max_ext);
            new_basis.SetGradScalBounds(true);
            
            break;
         }
         case KEEPUNBOUNDEDBSPLINES:
         {
            new_basis.SetmKeepUnboundedBsplines(true);  
            break;
         }
         case TURNINGPOINT:
         {
            midas::input::GetLine(Minp, s);
            new_basis.SetTurningPoint(midas::util::FromString<In>(s));
            break;
         }
         case BASISGLQUADRATUREPOINTS:
         {
            midas::input::GetLine(Minp, s);
            new_basis.SetmBasGLQuadPnts(midas::util::FromString<In>(s));
            break;
         }
         case GAUSSLEGENDRERELACCURACY:
         {
            midas::input::GetLine(Minp, s);
            new_basis.SetGaussLegendreRelAccuracy(midas::util::FromString<unsigned>(s));
            break;
         }
         case PLOTBASIS:
         {
            midas::input::GetLine(Minp, s);
            if (  midas::input::IsKeyword(s)
               )
            {
               std::set<std::string> sset;
               sset.emplace("ALL");
               new_basis.SetPlotBasisForModes(sset);
               already_read = true;
            }
            else
            {
               auto svec = midas::util::StringVectorFromString(s);

               std::set<std::string> sset(svec.begin(), svec.end());

               new_basis.SetPlotBasisForModes(sset);
            }

            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << " is not a Basis level 3 Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // If user-defined basis, check if we need to plot it
   if (  new_basis.GetmUserDefinedBasis()
      )
   {
      new_basis.PlotBasis();
   }

   // if DUPLICATE of input is wanted then do it
   if (n_dup > 1)
   {
      Mout << " Creating basis input in " << n_dup << " duplicates." << endl;
      In nmodb = new_basis.GetmNoModesInBas();
      for (In i_dup=0;i_dup<n_dup-1;i_dup++)
      {
         for (In i=0;i<nmodb;i++) 
         {
            OneModeBasDef bas_def = new_basis.GetBasDefForLocalMode(i);
            GlobalModeNr i_g_mode = bas_def.GetGlobalModeNr();
            string mode = gOperatorDefs.GetModeLabel(i_g_mode);
            mode = mode + "_" + std::to_string(i_dup+I_1);
            In i_g_mode_new = gOperatorDefs.ModeNr(mode, true);
            OneModeBasDef bas_def2; 
            bas_def2.CloneOneBasisToOtherMode(bas_def,i_g_mode_new); 
            new_basis.AddToBasDef(bas_def2, bas_def2.GetGlobalModeNr());
         }
      }
   }
   
   SetBasisType(new_basis);

   // Check name 
   for (In i_basis = I_0; i_basis < gBasis.size() - I_1; i_basis++)
   {
      if (gBasis[i_basis].GetmBasName() == new_basis.GetmBasName())
      {
         MIDASERROR("Basis set name is already occupied");
      }
   }
   return s;
}
