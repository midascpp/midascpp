#ifndef ISKEYWORD_H_INCLUDED
#define ISKEYWORD_H_INCLUDED

#include<string>

#include "inc_gen/TypeDefs.h"

namespace midas
{
namespace input
{

//! check if string is a keyword
bool IsKeyword(const std::string& str);

//! check if string is not a keyword
bool NotKeyword(const std::string& str);

//! check if string is relatively a lower level keyword
bool IsLowerLevelKeyword(const std::string& str, In input_level);

//! 'throw' an input error
void InputKeywordError(const std::string& str, std::string type, In input_level);

//! Assert that string contains a keyword, else throw error
void AssertKeyword(const std::string& str);

//! Assert that string does not contain a keyword, else throw error
void AssertNotKeyword(const std::string& str);

//! Assert that string contains a specific keyword, else throw error
void AssertSpecificKeyword(const std::string& str, const std::string&& test);

} /* namespace input */
} /* namespace midas */

#endif /* ISKEYWORD_H_INCLUDED */
