#include "Trim.h"

#include<algorithm> // for std::transform
#include<cctype> // for toupper
#include<iostream> // for debug output

namespace midas
{
namespace input
{

/**
 * Trim a string aka remove leading and trailing whitespace
 **/
std::string Trim(const std::string& str
               , const std::string& whitespace)
{
   const auto strBegin = str.find_first_not_of(whitespace);
   
   if (strBegin == std::string::npos)
      return ""; // no content

   const auto strEnd = str.find_last_not_of(whitespace);
   const auto strRange = strEnd - strBegin + 1;

   return str.substr(strBegin, strRange);
}

/**
 * reduce a string aka remove all whitespace replace with filler
 **/
std::string Reduce(const std::string& str
                 , const std::string& fill
                 , const std::string& whitespace)
{
   // trim first
   auto result = Trim(str, whitespace);
   
   // replace sub ranges
   auto beginSpace = result.find_first_of(whitespace);
   while (beginSpace != std::string::npos)
   {
      const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
      const auto range = endSpace - beginSpace;
      
      result.replace(beginSpace, range, fill);
 
      const auto newStart = beginSpace + fill.length();
      beginSpace = result.find_first_of(whitespace, newStart);
   }
  
   return result;
}

std::string DeleteBlanks(std::string str)
{
   while(str.find(" ") != str.npos) 
   {
      str.erase(str.find(" "),1);
   }
   return str;
}

std::string ToUpperCase(std::string str)
{
   std::transform(str.begin(),str.end(),str.begin(),toupper); 
   return str;
}

std::string ToLowerCase(std::string str)
{
   std::transform(str.begin(),str.end(),str.begin(),tolower); 
   return str;
}

std::string RemoveTrailingSlash(std::string str)
{
   for(auto it = str.rbegin(); it != str.rend(); ++it)
   {
      if(*it == '/')
      {
         str.pop_back();
      }
      else
      {
         break;
      }
   }
   return str;
}

} /* namespace input */
} /* namespace midas */
