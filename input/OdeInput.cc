/**
************************************************************************
* 
* @file                OdeInput.cc
*
* 
* Created:             03-04-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input reader for ODE integration
* 
* Last modified: Tue Oct 2, 2012  05:55PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <tuple> // for pair
#include <set>
#include <type_traits>

// midas headers
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "input/KeywordMap.h"
#include "util/FindInMap.h"
#include "libmda/util/any_type.h"
#include "ode/OdeInfo.h"
#include "input/OdeInput.h"
#include "input/Trim.h"

namespace midas
{
namespace util
{

/**
 * Namespace with utility functions for reading ode input.
 **/
namespace detail 
{
///> deduce_key_type for OdeInfo
template<>
struct deduce_key_type<midas::ode::OdeInfo>
{
   using type = typename midas::ode::OdeInfo::key_type;
};

} /* namespace detail */

} /* namespace util */
} /* namespace midas */

using namespace midas;

namespace detail 
{

/**
 * Get map of keywords for each OdeStepperBase type
 *
 * @return Returns the map of needed/allowed keywords with default values for unset keywords.
 **/
const keyword_map& GetOdeStepperKeywordMap
   (
   )
{
   // set-up map of ode stepper types vs which keywords are allowed
   static keyword_map keyword_map_stepper =
   { 
      // DOPR853 default
      {  {"DOPR853"},   {  {"ODESTEPSIZEBETA", static_cast<Nb>(0.)}
                        ,  {"ODESTEPSIZEALPHA", static_cast<Nb>(1./8.)}
                        ,  {"ODESTEPSIZESAFE", static_cast<Nb>(0.9)}
                        ,  {"ODESTEPSIZEMINSCALE", static_cast<Nb>(0.333)}
                        ,  {"ODESTEPSIZEMAXSCALE", static_cast<Nb>(6.0)}
                        ,  {"ODESTEPPERABSTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ODESTEPPERRELTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ERRORTYPE", midas::ode::detail::OdeErrorType::MEAN}
                        ,  {"USEFSAL", true}
                        }
      }

      // DOPR5 default
   ,  {  {"DOPR5"},     {  {"ODESTEPSIZEBETA", static_cast<Nb>(0.0)}
                        ,  {"ODESTEPSIZEALPHA", static_cast<Nb>(0.2)}
                        ,  {"ODESTEPSIZESAFE", static_cast<Nb>(0.9)}
                        ,  {"ODESTEPSIZEMINSCALE", static_cast<Nb>(0.2)}
                        ,  {"ODESTEPSIZEMAXSCALE", static_cast<Nb>(10.0)}
                        ,  {"ODESTEPPERABSTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ODESTEPPERRELTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ERRORTYPE", midas::ode::detail::OdeErrorType::MEAN}
                        ,  {"USEFSAL", true}
                        }
      }

      // TSIT5 default
   ,  {  {"TSIT5"},     {  {"ODESTEPSIZEBETA", static_cast<Nb>(0.0)}
                        ,  {"ODESTEPSIZEALPHA", static_cast<Nb>(0.2)}
                        ,  {"ODESTEPSIZESAFE", static_cast<Nb>(0.9)}
                        ,  {"ODESTEPSIZEMINSCALE", static_cast<Nb>(0.2)}
                        ,  {"ODESTEPSIZEMAXSCALE", static_cast<Nb>(10.0)}
                        ,  {"ODESTEPPERABSTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ODESTEPPERRELTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ERRORTYPE", midas::ode::detail::OdeErrorType::MEAN}
                        ,  {"USEFSAL", true}
                        }
      }

#ifdef ENABLE_GSL
      // GSL default
   ,  {  {"GSL"},       {  {"ODESTEPPERABSTOL", static_cast<Nb>(1.e-16)}
                        ,  {"ODESTEPPERRELTOL", static_cast<Nb>(1.e-16)}
                        ,  {"USEFSAL", false} // makes no sense here...
                        }
      }
#endif   /* ENABLE_GSL */
   };

   return keyword_map_stepper;
}

/**
 * Get map of keywords for each OdeDriverBase type.
 *
 * @return Returns the map of needed/allowed keywords with default values for unset keywords.
 **/
const keyword_map& GetOdeDriverKeywordMap
   (
   )
{
   // set-up map of ode driver types vs which keywords are allowed
   static keyword_map keyword_map_driver =
   { 
      // MIDAS default
      {  {"MIDAS"},  {  {"TIMEINTERVAL", std::pair<Nb,Nb>(C_0, C_1)}
                     ,  {"OUTPUTPOINTS", I_20}
                     ,  {"DATABASESAVEVECTORS", false}
                     ,  {"MINSTEPSIZE", static_cast<Nb>(0.)}
                     ,  {"MAXSTEPS", static_cast<In>(-1)}
                     ,  {"IMPROVEDINITIALSTEPSIZE", true}
                     ,  {"INITIALSTEPSIZE", static_cast<Nb>(1.e-2)}
                     ,  {"FIXEDSTEPSIZE", false}
                     ,  {"MIDASODEDRIVERMAXFAILEDSTEPS", I_3}
                     ,  {"SAVESTEPS", false}
                     ,  {"NAME", std::string("")}
                     }
      }

#ifdef ENABLE_GSL
      // GSL default
   ,  {  {"GSL"},    {  {"TIMEINTERVAL", std::pair<Nb,Nb>(C_0, C_1)}
                     ,  {"OUTPUTPOINTS", I_20}
                     ,  {"DATABASESAVEVECTORS", false}
                     ,  {"MINSTEPSIZE", static_cast<Nb>(0.)}
                     ,  {"MAXSTEPS", static_cast<In>(-1)}
                     ,  {"IMPROVEDINITIALSTEPSIZE", true}
                     ,  {"INITIALSTEPSIZE", static_cast<Nb>(1.e-2)}
                     ,  {"GSLSTEPTYPE", std::string("MSADAMS")}
                     ,  {"GSLERRORAY", 1.}
                     ,  {"GSLERRORADYDT", 0.}
                     ,  {"NAME", std::string("")}
                     }
      }
#endif   /* ENABLE_GSL */
   };

   return keyword_map_driver;
}

/**
 * Validate OdeInfo as created by input.
 * Will throw a MidasWarning if 'extra'/unused keywords are set,
 * which are not used by the given type.
 * Also will set alle default values needed for given type.
 *
 * @param odeinfo 
 *    The OdeInfo to validate. On output every keyword needed that was
 *    not set on input has been given a dfault value.
 **/
void ValidateOdeInput
   (  midas::ode::OdeInfo& odeinfo
   )
{
   // check we have set a driver and a stepper
   auto& driver = util::FindElseError("DRIVER", odeinfo, "Did not find 'DRIVER' in OdeInfo after input. Check 'TYPE' input!").second.get<std::string>();
   auto& stepper = util::FindElseError("STEPPER", odeinfo, "Did not find 'STEPPER' in OdeInfo after input. Check 'TYPE' input!").second.get<std::string>();

   // check for driver and stepper in keyword_map
   auto& keyword_map_driver = GetOdeDriverKeywordMap();
   auto& keyword_map_stepper = GetOdeStepperKeywordMap();

   auto& allowed_driver_keywords =  util::FindElseError
                                       (  driver
                                       ,  keyword_map_driver
                                       ,  "DRIVER: '" + std::string(driver) + "' not found in ode input map."
                                       ).second;

   auto& allowed_stepper_keywords =  util::FindElseError
                                       (  stepper
                                       ,  keyword_map_stepper
                                       ,  "STEPPER: '" + std::string(stepper) + "' not found in ode input map."
                                       ).second;
   
   // check that all set keywords are allowed for the currectly set DRIVER and STEPPER
   for(auto& elem : odeinfo)
   {
      if (  elem.first == "DRIVER"
         || elem.first == "STEPPER"
         || elem.first == "IOLEVEL"
         )
      {
         continue;
      }

      auto driver_iter = allowed_driver_keywords.find(elem);
      auto stepper_iter = allowed_stepper_keywords.find(elem);

      if (  driver_iter == allowed_driver_keywords.end()
         && stepper_iter == allowed_stepper_keywords.end()
         )
      {
         MIDASERROR("Keyword: '" + elem.first + "' is not allowed for type: '" + std::string(driver) + " " + std::string(stepper) + "'.");
      }
   }

   // set default driver values
   for(auto& elem : allowed_driver_keywords)
   {
      auto iter = odeinfo.find(elem.first);
      if(iter == odeinfo.end())
      {
         odeinfo[elem.first] = elem.second;
      }
   }

   // set default stepper values
   for(auto& elem : allowed_stepper_keywords)
   {
      auto iter = odeinfo.find(elem.first);
      if(iter == odeinfo.end())
      {
         odeinfo[elem.first] = elem.second;
      }
   }

   // set other defaults
   {
      auto iter = odeinfo.find("IOLEVEL");
      if (  iter == odeinfo.end()
         )
      {
         odeinfo["IOLEVEL"] = I_1;  // IoLevel is 1 as default (lowest value)
      }
   }
}

} /* namespace detail */

/**
 * Input function for integration of ODEs
 *
 * @param Minp           Input file stream.
 * @param s              Currently read string.
 * @param input_level    Input keyword level
 * @param odeinfo        OdeInfo to add currently read OdeInfo to.
 * @return               Return whether s should be processed by calling function, 
 *                       i.e. has already beed read, but not processed.
 **/
bool OdeInput
   (  std::istream& Minp
   ,  std::string& s
   ,  In input_level
   ,  midas::ode::OdeInfo& odeinfo
   )
{
   // clear input info
   odeinfo.clear();

   // Enum/map with input flags
   enum INPUT  {  ERROR
               ,  TYPE
               ,  NAME
               ,  IOLEVEL
               ,  STEPSIZECONTROL
               ,  TIMEINTERVAL
               ,  MAXSTEPS
               ,  MINSTEPSIZE
               ,  TOLERANCE
               ,  OUTPUTPOINTS
               ,  MIDASODEDRIVERMAXFAILEDSTEPS
               ,  INITIALSTEPSIZE
               ,  ERRORTYPE
               ,  NOFSAL
               ,  DATABASESAVEVECTORS
               ,  FIXEDSTEPSIZE
               ,  SAVESTEPS
#ifdef ENABLE_GSL
               ,  GSLERRORSCALING
#endif   /* ENABLE_GSL */
               };

   const std::map<std::string, INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"TYPE", TYPE}
   ,  {"#"+std::to_string(input_level)+"NAME", NAME}
   ,  {"#"+std::to_string(input_level)+"IOLEVEL", IOLEVEL}
   ,  {"#"+std::to_string(input_level)+"STEPSIZECONTROL", STEPSIZECONTROL}
   ,  {"#"+std::to_string(input_level)+"TIMEINTERVAL", TIMEINTERVAL}
   ,  {"#"+std::to_string(input_level)+"MAXSTEPS", MAXSTEPS}
   ,  {"#"+std::to_string(input_level)+"MINSTEPSIZE", MINSTEPSIZE}
   ,  {"#"+std::to_string(input_level)+"TOLERANCE", TOLERANCE}
   ,  {"#"+std::to_string(input_level)+"OUTPUTPOINTS", OUTPUTPOINTS}
   ,  {"#"+std::to_string(input_level)+"MIDASODEDRIVERMAXFAILEDSTEPS", MIDASODEDRIVERMAXFAILEDSTEPS}
   ,  {"#"+std::to_string(input_level)+"INITIALSTEPSIZE", INITIALSTEPSIZE}
   ,  {"#"+std::to_string(input_level)+"FIXEDSTEPSIZE", FIXEDSTEPSIZE}
   ,  {"#"+std::to_string(input_level)+"ERRORTYPE", ERRORTYPE}
   ,  {"#"+std::to_string(input_level)+"NOFSAL", NOFSAL}
   ,  {"#"+std::to_string(input_level)+"DATABASESAVEVECTORS", DATABASESAVEVECTORS}
#ifdef ENABLE_GSL
   ,  {"#"+std::to_string(input_level)+"GSLERRORSCALING", GSLERRORSCALING}
#endif   /* ENABLE_GSL */
   ,  {"#"+std::to_string(input_level)+"SAVESTEPS", SAVESTEPS}
   };

   const std::map<std::string, midas::ode::detail::OdeErrorType> err_t_map =
   {  {  "MEAN",  midas::ode::detail::OdeErrorType::MEAN}
   ,  {  "MAX",   midas::ode::detail::OdeErrorType::MAX}
   ,  {  "SCALEDMAX",   midas::ode::detail::OdeErrorType::SCALEDMAX}
   };

   // Initialization of default values for quantities read in later &  Local params.
   std::string s_orig;
   bool already_read = false;

   bool init_step_set = false;
   bool fixed_step_set = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s,input_level)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case TYPE:
         {
            midas::input::GetLine(Minp,s);
            auto string_vec = midas::util::StringVectorFromString(s);

            // Transform input to upper case
            for(auto& s_in : string_vec)
            {
               s_in = midas::input::ToUpperCase(s_in);
            }

            auto nargs = string_vec.size();
            
            // If only one argument, we assume the MidasOdeDriver
            if (  nargs == 1
               )
            {
               odeinfo["DRIVER"] = std::string("MIDAS");
               odeinfo["STEPPER"] = string_vec[0];
            }
            // Else, we check for other drivers
            else if  (  nargs == 2
                     )
            {
               const auto& driver = string_vec[0];
               const auto& stepper = string_vec[1];

#ifdef ENABLE_GSL
               // If GSL, we do things a little differently
               if (  driver == "GSL"
                  )
               {
                  odeinfo["DRIVER"] = std::string("GSL");
                  odeinfo["STEPPER"] = std::string("GSL");
                  odeinfo["GSLSTEPTYPE"] = stepper;
               }
               else
#endif   /* ENABLE_GSL */
               {
                  odeinfo["DRIVER"] = driver;
                  odeinfo["STEPPER"] = stepper;
               }
            }
            else
            {
               MIDASERROR("Too many input argumants for OdeInput 'TYPE'");
            }

            break;
         }
         case NAME:
         {  
            midas::input::GetLine(Minp,s);
            odeinfo["NAME"] = s;
            break;
         }
         case IOLEVEL:
         {
            odeinfo["IOLEVEL"] = midas::input::GetIn(Minp, s);
            break;
         }
         case STEPSIZECONTROL:
         {
            midas::input::GetLine(Minp, s);
            auto [alpha, beta, safe, minscale, maxscale] = midas::util::TupleFromString<Nb, Nb, Nb, Nb, Nb>(s);
            odeinfo["ODESTEPSIZEALPHA"] = alpha;
            odeinfo["ODESTEPSIZEBETA"] = beta;
            odeinfo["ODESTEPSIZESAFE"] = safe;
            odeinfo["ODESTEPSIZEMINSCALE"] = minscale;
            odeinfo["ODESTEPSIZEMAXSCALE"] = maxscale;

            break;
         }
         case TIMEINTERVAL:
         {
            midas::input::GetLine(Minp, s);
            auto ts = midas::util::VectorFromString<Nb>(s);

            if (  ts.size() != 2
               )
            {
               MIDASERROR("TIMEINTERVAL must consist of two numbers (in one line)!");
            }

            odeinfo["TIMEINTERVAL"] = std::make_pair<Nb, Nb>(std::move(ts[0]), std::move(ts[1]));

            break;
         }
         case MAXSTEPS:
         {
            odeinfo["MAXSTEPS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case MINSTEPSIZE:
         {
            odeinfo["MINSTEPSIZE"] = midas::input::GetNb(Minp, s);
            break;
         }
         case TOLERANCE:
         {
            midas::input::GetLine(Minp, s);
            auto in_vec = midas::util::VectorFromString<Nb>(s);

            if (  in_vec.size() == 1
               )
            {
               odeinfo["ODESTEPPERABSTOL"] = in_vec[0];
               odeinfo["ODESTEPPERRELTOL"] = in_vec[0];
            }
            else if  (  in_vec.size() == 2
                     )
            {
               odeinfo["ODESTEPPERABSTOL"] = in_vec[0];
               odeinfo["ODESTEPPERRELTOL"] = in_vec[1];
            }
            else
            {
               MIDASERROR("OdeInput: One or two 'TOLERANCE' parameters requested!");
            }
            break;
         }
         case OUTPUTPOINTS:
         {
            odeinfo["OUTPUTPOINTS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case MIDASODEDRIVERMAXFAILEDSTEPS:
         {
            odeinfo["MIDASODEDRIVERMAXFAILEDSTEPS"] = midas::input::GetIn(Minp, s);
            break;
         }
         case INITIALSTEPSIZE:
         {
            init_step_set = true;
            odeinfo["IMPROVEDINITIALSTEPSIZE"] = false;
            odeinfo["INITIALSTEPSIZE"] = midas::input::GetNb(Minp, s);
            break;
         }
         case FIXEDSTEPSIZE:
         {
            fixed_step_set = true;

            // Set bool true
            odeinfo["FIXEDSTEPSIZE"] = true;

            // Get step size.
            midas::input::GetLine(Minp, s);

            if (  midas::input::ToUpperCase(s) == "AUTO"
               )
            {
               // Use improved initial step size all the way through
               odeinfo["IMPROVEDINITIALSTEPSIZE"] = true;
            }
            else
            {
               // Use given step size
               odeinfo["IMPROVEDINITIALSTEPSIZE"] = false;
               odeinfo["INITIALSTEPSIZE"] = midas::util::FromString<Nb>(s);
            }

            break;
         }
         case ERRORTYPE:
         {
            midas::input::GetLine(Minp, s);
            
            odeinfo["ERRORTYPE"] = err_t_map.at(s);

            break;
         }
#ifdef ENABLE_GSL
         case GSLERRORSCALING:
         {
            midas::input::GetLine(Minp, s);
            auto v = midas::util::VectorFromString<double>(s);

            assert(v.size() == 2);

            odeinfo["GSLERRORAY"] = v[0];
            odeinfo["GSLERRORADYDT"] = v[1];

            break;
         }
#endif   /* ENABLE_GSL */
         case NOFSAL:
         {
            odeinfo["USEFSAL"] = false;
            break;
         }
         case DATABASESAVEVECTORS:
         {
            odeinfo["DATABASESAVEVECTORS"] = true;
            break;
         }
         case SAVESTEPS:
         {
            odeinfo["SAVESTEPS"] = true;
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a ODE level " + std::to_string(input_level) + " Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // Sanity check
   if (  init_step_set
      && fixed_step_set
      )
   {
      MIDASERROR("OdeInput: You cannot set both 'INITIALSTEPSIZE' and 'FIXEDSTEPSIZE'.");
   }
   
   // validate input and set default values.
   detail::ValidateOdeInput(odeinfo);

   // we have read, but not processed next keyword
   return true;
}
