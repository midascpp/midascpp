/**
************************************************************************
* 
* @file                BasDef.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
 set* Short Description:   Contains information on the complete primitive basis set 
* 
* Last modified: Tue Jun 02, 2009  03:00PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASDEF_H
#define BASDEF_H

// Standard Headers
#include <map>
#include <string>
#include <vector>
#include <map>
#include <utility>
#include <algorithm>

// Midas Headers
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "input/OpDef.h"
#include "input/OneModeBasDef.h"

// Using declarations
using std::string;
using std::vector;
using std::map;

using std::string;
using std::vector;
using std::map;

// Peter 010807:
// This is needed for compiling using IBM XL compilers (sleipner.cscaa.dk).
// Strange...
//class OpDef; //<- ian: #"¤% this

/**
 * Comparison Function used in BasDef class members.
**/
inline bool OneModeBasDefLess
   (  const OneModeBasDef& aO1
   ,  const OneModeBasDef& aO2
   )
{
   return aO1.GetGlobalModeNr() < aO2.GetGlobalModeNr();
}                   

/**
 *
**/
class BasDef 
{
   private:

      //! Number of modes for which the basis set is defined
      In mNoModesInBas                                   = I_0;                       
      //! Name of basis set
      std::string mBasName                               = "basis";
      //! BasDef type = "Prod"
      std::string mBasSetType                            = "Product";
      //! The basis as a vector of one mode basis Defs.
      std::vector<OneModeBasDef> mVecOneModeBasDef;  
      //! A <globalmode,localmode> map 
      std::map<GlobalModeNr, LocalModeNr> mBasModeMap;
      //! Automatically generate either Distributed Gaussian or B-spline primitive basis
      bool mAutoGenBasis                                 = false;
      //! Use harmonic oscillators functions as primitive basis for wave functions 
      bool mUseHoBasis                                   = false; 
      //! Use distributed Gaussian functions as primitive basis for wave functions 
      bool mUseGaussianBasis                             = false;
      //! Use B-spline functions as primitive basis for wave functions 
      bool mUseBsplineBasis                              = false;
      //! User-defined settings are present
      bool mUserDefinedBasis                             = false;
      //! The quantum number used in construction of the harmonic oscillator functions
      std::vector<In> mHoQnrs                            = {I_20};
      //! The exponent alpha used in construction of the distributed Gaussian functions
      std::vector<Nb> mGaussianExps                      = {C_1};
      //! The order used in construction of the B-spline functions
      std::vector<In> mBsplineOrders                     = {I_10};
      //! The number of distributed Gaussian or B-spline functions used in the primitive basis
      std::vector<In> mNoPrimBasisFunctions              = {I_200};
      //! Generate the number of primitive basis function by means of a basis set density
      bool mUsePrimBasisDens                             = true;
      //! The (primitive) basis set density for use with distributed Gaussian or B-spline functions
      std::vector<Nb> mPrimBasisDens                     = {0.8};
      //! The maximum number of primitive basis functions per mode
      In mMaxNoPrimBasisFunctions                        = 500;
      //! Use scaling frequencies from the potential, (mop file) 
      bool mUsingScalingFreqs                            = false;                      
      //! Read bounds from file
      bool mReadBoundsFromFile                           = false;
      //! Path for grid-bounds file
      std::string mBoundsFilePath                        = "";
      //! Bool for ScalBounds keyword in input
      bool mScaleBounds                                  = false;
      //! Set a scaling factor to extend the localized support over the bounds
      Nb mScalBounds                                     = C_1;
      //! Set a Maximum values for the boundaries extension
      Nb mMaxBoundsExt                                   = C_0;
      //! Bool for GradScalBounds keyword in input
      bool mGradScaleBounds                              = false;
      //! Set a scaling factor for GradScalBounds
      Nb mGradScalBounds                                 = C_3/C_2;
      //! Set a Maximum values for the boundaries extension for the gradient guided basis extension
      Nb mGradMaxBoundsExt                               = C_30;
      //! No basis beyond max potential energy value
      bool mNoBasBeyondMaxPot                            = false;
      //! Do not disregard the first and last B-spline of the set
      bool mKeepUnboundedBsplines                        = false;
      //! Give classical harmonic oscillator turning point for use as basis boundary
      In mTurn                                           = I_10;
      //! The number of points used in numerical integration by means of Gauss-Legendre quadrature
      In mBasGLQuadPnts                                  = I_12;
      //! Accuracy of Gauss-Legendre points relative to numeric epsilon
      unsigned mGaussLegendreRelAccuracy = 10;

      //! Write data for plotting basis functions?
      std::set<std::string> mPlotBasisForModes;


      //! Automatically generate a harmonic oscillator primitive basis
      void HoBasisFromOp(const OpDef& arOpDef);              

      //! Automatically generate a distributed Gaussian or B-spline primitive basis
      void GenBasis(const OpDef& arOpDef);                   

   
   public:
      
      //! Constructor and initialization
      BasDef()
      {
         this->Reserve();
      } 

      //! Reserve heap for STL containers
      void Reserve();
     
      //! Generate a primitive basis
      void InitBasis(const OpDef&);

      //! Plot basis functions
      void PlotBasis
         (  const std::string& = ""
         );

      //! Set the path to the grid-bounds file
      void SetGridBoundsFilePath
         (  const std::string& aPath
         )
      {
         mBoundsFilePath = aPath;
      }

      //! Clear all data
      void Clear() 
      {
         mNoModesInBas = I_0;
         mVecOneModeBasDef.clear();
         mBasModeMap.clear();
      } 

      //! Sort the OneModeBasDef vector 
      void Sort()
      {
         std::sort(mVecOneModeBasDef.begin(), mVecOneModeBasDef.end(), OneModeBasDefLess);
      }

      //! Returns the basis value at a given point in the normal coordinate
      Nb EvalOneModeBasisFunc
         (  const In& aLocalMode
         ,  const In& aIbas
         ,  const Nb& aQval
         )  const 
      {
         return mVecOneModeBasDef[aLocalMode].EvalBasisFunc(aIbas, aQval, mKeepUnboundedBsplines);
      }
      
      //! Returns the basis value at a given point in the normal coordinate
      void EvalOneModeBasisFunc
         (  const In& aLocalMode
         ,  const In& aIbas
         ,  const MidasVector& arQval
         ,  MidasVector& arFuncVal
         )  const
      {
         return mVecOneModeBasDef[aLocalMode].EvalBasisFunc(aIbas, arQval, arFuncVal, mKeepUnboundedBsplines);
      }

      //@{
      //! BasDef class setters
      void SetmBasName(const std::string& aStr)                   {mBasName = aStr;}
      void SetmBasSetType(const std::string& aStr)                {mBasSetType = aStr;}
      void SetmAutoGenBasis(const bool& aBool)                    {mAutoGenBasis = aBool;}
      void SetmUseHoBasis(const bool& aBool)                      {mUseHoBasis = aBool;}
      void SetmUseGaussianBasis(const bool& aBool)                {mUseGaussianBasis = aBool;}
      void SetmUseBsplineBasis(const bool& aBool)                 {mUseBsplineBasis = aBool;}
      void SetmUserDefinedBasis(const bool& aBool)                {mUserDefinedBasis = aBool;}
      void SetmHoQnrs(const std::vector<In>& aVec)                {mHoQnrs = aVec;}
      void SetmGaussianExps(const std::vector<Nb>& aVec)          {mGaussianExps = aVec;}
      void SetmBsplineOrders(const std::vector<In>& aVec)         {mBsplineOrders = aVec;}
      void SetmNoPrimBasisFunctions(const std::vector<In>& aVec)  {mNoPrimBasisFunctions = aVec;}
      void SetmUsePrimBasisDens(const bool& aBool)                {mUsePrimBasisDens = aBool;}
      void SetmPrimBasisDens(const std::vector<Nb>& aVec)         {mPrimBasisDens = aVec;}
      void SetmMaxNoPrimBasisFunctions(const In& aIn)             {mMaxNoPrimBasisFunctions = aIn;}
      void SetmUsingScalingFreqs(const bool& aBool)               {mUsingScalingFreqs = aBool;}
      void SetmReadBoundsFromFile(const bool& aBool)              {mReadBoundsFromFile = aBool;}
      void SetScalBounds(const bool& aBool)                       {mScaleBounds = aBool;}
      void SetScalBoundsFromFile
         (  const Nb& scal
         ,  const Nb& max_ext
         ) 
      {  mScalBounds=scal; 
         mMaxBoundsExt=max_ext;
      }
      void SetGradScalBounds(const bool& aBool)                   {mGradScaleBounds = aBool;}
      void SetGradScalBoundsFromFile
         (  const Nb& gradscal
         ,  const Nb& grad_max_ext
         ) 
      {
         mGradScalBounds = gradscal; 
         mGradMaxBoundsExt = grad_max_ext;
      }
      void SetNoBasBeyondMaxPot(const bool& aBool)                {mNoBasBeyondMaxPot = aBool;} 
      void SetmKeepUnboundedBsplines(const bool& aBool)           {mKeepUnboundedBsplines = aBool;}
      void SetTurningPoint(const In& aI)                          {mTurn = aI;}
      void SetmBasGLQuadPnts(const In& aBasGLQuadPnts)            {mBasGLQuadPnts = aBasGLQuadPnts;}
      void SetPlotBasisForModes
         (  const std::set<std::string>& aSet
         )
      {
         this->mPlotBasisForModes = aSet;
      }
      //@}

      //@{
      //! BasDef class getters
      const In& GetmNoModesInBas() const                       {return mNoModesInBas;}
      const std::string& GetmBasName() const                   {return mBasName;}
      const std::string& GetmBasSetType() const                {return mBasSetType;}
      const bool& GetmAutoGenBasis() const                     {return mAutoGenBasis;}
      const bool& GetmUseHoBasis() const                       {return mUseHoBasis;}
      const bool& GetmUseGaussianBasis() const                 {return mUseGaussianBasis;} 
      const bool& GetmUseBsplineBasis() const                  {return mUseBsplineBasis;} 
      const bool& GetmUserDefinedBasis() const                 {return mUserDefinedBasis;}
      const std::vector<In>& GetmHoQnrs() const                {return mHoQnrs;}
      const std::vector<Nb>& GetmGaussianExps() const          {return mGaussianExps;}
      const std::vector<In>& GetmBsplineOrders() const         {return mBsplineOrders;}
      const std::vector<In>& GetmNoPrimBasisFunctions() const  {return mNoPrimBasisFunctions;}
      const bool& GetmUsePrimBasisDens() const                 {return mUsePrimBasisDens;}
      const std::vector<Nb>& GetmPrimBasisDens() const         {return mPrimBasisDens;}
      const In& GetmMaxNoPrimBasisFunctions() const            {return mMaxNoPrimBasisFunctions;}
      const bool& GetmUsingScalingFreqs() const                {return mUsingScalingFreqs;}
      const bool& GetmReadBoundsFromFile() const               {return mReadBoundsFromFile;}
      const bool& GetGradScaleBounds() const                   {return mGradScaleBounds;}
      const Nb& GetScalBounds() const                          {return mScalBounds;}
      const Nb& GetMaxBoundsExt() const                        {return mMaxBoundsExt;}
      const bool& GetScaleBounds() const                       {return mScaleBounds;}
      const Nb& GetGradScalBounds() const                      {return mGradScalBounds;}
      const Nb& GetGradMaxBoundsExt() const                    {return mGradMaxBoundsExt;}
      const bool& GetNoBasBeyondMaxPot() const                 {return mNoBasBeyondMaxPot;}
      const bool& GetmKeepUnboundedBsplines() const            {return mKeepUnboundedBsplines;}
      const In& GetTurningPoint() const                        {return mTurn;}
      const In& GetmBasGLQuadPnts() const                      {return mBasGLQuadPnts;} 
      //@}
      
      //! The mode number on local list for global mode nr.
      LocalModeNr GetLocalModeNr(const GlobalModeNr& aGlobalModeNr) const;

      //! Get a OneModeBasDef for global mode
      const OneModeBasDef& GetBasDefForGlobalMode(const In& aGlobalMode) const;

      //! Add OneModeBasDef to vector
      void AddToBasDef
         (  const OneModeBasDef& aOneModeBasDef
         ,  GlobalModeNr aBasModeNr
         );

      //@{
      //! One-mode primitive basis set definition getters
      const OneModeBasDef& GetBasDefForLocalMode(In aLocalMode) const {return mVecOneModeBasDef[aLocalMode];}                ///< Get OneModeBasDef for local basis mode 
      GlobalModeNr GetGlobalModeNr(In aLocalModeNr) const {return mVecOneModeBasDef[aLocalModeNr].GetGlobalModeNr();}                                  ///< The mode nr. on global list
      std::string OneModeType(In aLocalMode) const {return mVecOneModeBasDef[aLocalMode].Type();}         ///< Get Type
      In Nbas(In aImode) const {return mVecOneModeBasDef[aImode].Nbas();}                            ///< Return nr. of basis functs for a mode
      In Nint(In aImode) const {return mVecOneModeBasDef[aImode].Nint();}                            ///< Return the nr. of intervals (Gauss, B-splines)
      In Lower(In aImode) const {return mVecOneModeBasDef[aImode].Lower();}                          ///< Return lowest n for HO bas.
      In Higher(In aImode) const {return mVecOneModeBasDef[aImode].Higher();}                        ///< Return highest for HO bas
      Nb GetOmeg(In aImode) const {return mVecOneModeBasDef[aImode].Omeg();}                         ///< Return Omega for HO bas
      Nb GetMass(In aImode) const {return mVecOneModeBasDef[aImode].Mass();}                         ///< Return Mass for HO bas
      In Nord(In aImode) const {return mVecOneModeBasDef[aImode].Nord();}                            ///< Return the order of the Bspline basis
      void GetVecOfExps(In aImode, MidasVector& arVec) const {return mVecOneModeBasDef[aImode].GetVecOfExps(arVec);}
      ///< returns the vector of exponents for gaussian basis
      Nb GetExp(In aImode, In aAdd) const {return mVecOneModeBasDef[aImode].GetExp(aAdd);}           ///< return an exponent of gaussian function
      Nb BasDens(In aImode) const {return mVecOneModeBasDef[aImode].BasDens();}                      ///< return the "density" of the gaussian/B-spline basis 
      void GetVecOfPts(In aImode,MidasVector& arVec) const {return mVecOneModeBasDef[aImode].GetVecOfPts(arVec);}
      ///< return the location of the gaussian basis set for the Imode mode or the grid of knots defining the Bsline basis
      Nb GetGaussPnt(In aImode, In aAdd) const {return mVecOneModeBasDef[aImode].GetGaussPnt(aAdd);} ///< return a particular location for gaussian basis aAdd for Imode mode
      Nb GetKnot(In aImode, In aAdd) const {return mVecOneModeBasDef[aImode].GetKnotValue(aAdd);}    ///< return the position of a given knot 
      In GetLastInd(In aImode,In aInt) const {return mVecOneModeBasDef[aImode].GetLastInd(aInt);}    ///< Return the index of the last B-spline in the interval
      //@}

      bool HasModeMap() const { return !mBasModeMap.empty(); }
      const std::map<GlobalModeNr, LocalModeNr>& ModeMap() const { return mBasModeMap; }

      unsigned GetGaussLegendreRelAccuracy() const { return mGaussLegendreRelAccuracy; }
      void SetGaussLegendreRelAccuracy(unsigned aAcc) { mGaussLegendreRelAccuracy = aAcc; }
};

/**
 * The mode nr. on local list for global mode nr.
**/
inline LocalModeNr BasDef::GetLocalModeNr
   (  const GlobalModeNr& aGlobalModeNr
   )  const
{
   auto elem = mBasModeMap.find(aGlobalModeNr);
   if (  elem != mBasModeMap.end()
      )
   {
      return elem->second;
   }
   else
   {
      MIDASERROR("Could not find global mode '" + std::to_string(aGlobalModeNr) + "' in BasDef.");
      return -I_1;
   }
}                                    

/**
 *
**/
inline const OneModeBasDef& BasDef::GetBasDefForGlobalMode
   (  const In& aGlobalMode
   )  const
{
   return mVecOneModeBasDef[GetLocalModeNr(aGlobalMode)];
}     

/**
 * Add OneModeBasDef to vector 
**/
inline void BasDef::AddToBasDef
   (  const OneModeBasDef& aOneModeBasDef
   ,  GlobalModeNr aBasModeNr
   )
{
   mVecOneModeBasDef.push_back(aOneModeBasDef); 
   mNoModesInBas++;
   mBasModeMap.insert(std::make_pair(aBasModeNr, mNoModesInBas - I_1));
}                                            

#endif
