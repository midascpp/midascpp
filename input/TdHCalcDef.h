/**
************************************************************************
*
* @file                TdHCalcDef.h
*
* Created:             30-06-2011
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a Vcc calculation
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHCALCDEF_H
#define TDHCALCDEF_H

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>

// Midas Headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "input/TdPropertyDef.h"
#include "ode/OdeInfo.h"

#ifdef VAR_MPI
#include "td/tdh/TdHMpi.h"
#endif /* VAR_MPI */

namespace midas::tdh
{
//! Constraint (g operator) for equation of motion of L-TDH
enum class constraintID : int
{
   ZERO = 0
,  FOCK
};


//! TDH parametrizations
enum class tdhID : int
{
   LINEAR = 0
,  EXPONENTIAL
};

} /* namespace midas::tdh */

namespace midas::input
{

/**
* Construct a definition of a TdH calculation
* */
class TdHCalcDef
   :  public TdPropertyDef
{
   public:
      //! Initial wave-packet types
      enum class initType : int
      {  VSCF = 0
      };
      
   //! Name of calculation
   CREATE_VARIABLE(std::string, Name, std::string{""});
   
   //! Operator
   CREATE_VARIABLE(std::string, Oper, std::string{""});

   //! Basis set
   CREATE_VARIABLE(std::string, Basis, std::string{""});
   
   //! Transform to spectral basis for initial state before propagation
   CREATE_VARIABLE(bool, SpectralBasis, false);
   
   //! Type of spectral-basis transform. Empty string will give spectral basis of reference potential.
   CREATE_VARIABLE(std::string, SpectralBasisOper, std::string{""});

   //! Limit the modal-basis size
   CREATE_VARIABLE(std::vector<In>, ModalBasisLimits, std::vector<In>{});
   
   //! Policy for initial wave packet
   CREATE_VARIABLE(initType, InitialWavePacket, initType::VSCF);
   
   //! Name of VSCF calculation for initial modals
   CREATE_VARIABLE(std::string, VscfName, std::string{""});

   //! Use TDH to update one-mode density
   CREATE_VARIABLE(bool, UpdateOneModeDensities, false);
   
   //! After how many steps should the density be updated
   CREATE_VARIABLE(In, UpdateOneModeDensitiesStep, I_0);

   //! Write the one-mode densities to file
   CREATE_VARIABLE(bool, WriteOneModeDensities, false);

   //! Set value of SpaceSmoothening parameter
   CREATE_VARIABLE(In, SpaceSmoothening, I_0);

   //! Set value of TimeSmoothening parameter
   CREATE_VARIABLE(In, TimeSmoothening, I_0);
   
   //! Info on integrator, time interval, etc.
   CREATE_VARIABLE(midas::ode::OdeInfo, OdeInfo, midas::ode::OdeInfo());
   
   //! Do imaginary-time propagation
   CREATE_VARIABLE(bool, ImagTime, false);
   
   //! TDH parametrization
   CREATE_VARIABLE(midas::tdh::tdhID, Parametrization, midas::tdh::tdhID::LINEAR);
   
   //! Constraint operator for linear TDH
   CREATE_VARIABLE(midas::tdh::constraintID, LinearTdHConstraint, midas::tdh::constraintID::ZERO);
   
   //! Normalize modals in linear TDH
   CREATE_VARIABLE(bool, LinearTdHNormalizeVectors, true);
   
   //! Calculate individual projections in Exp. TDH
   CREATE_VARIABLE(bool, ExpTdHDerivativeProjectionTerms, false);
   
   //! Norm threshold for exponential TDH
   CREATE_VARIABLE(Nb, KappaNormThreshold, static_cast<Nb>(0.95));
   
   //! Max failed derivatives for exponential TDH
   CREATE_VARIABLE(In, ExpTdHMaxFailedDerivatives, I_1);
   
   //! Screening threshold based on operator coefficient
   CREATE_VARIABLE(Nb, ScreenZeroCoef, C_0);
   
   //! IO level
   CREATE_VARIABLE(In, IoLevel, I_1);
   
#ifdef VAR_MPI
   //! MPI-communication method
   CREATE_VARIABLE(midas::tdh::mpiComm, MpiComm, midas::tdh::mpiComm::BCAST);
#endif /* VAR_MPI */

   public:
      //! Constructor
      TdHCalcDef() = default;

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read OdeInfo
      bool ReadOdeInfo
         (  std::istream&
         ,  std::string&
         );

      //! Read constraint
      bool ReadLinearTdHConstraint
         (  std::istream&
         ,  std::string&
         );

      //! Read policy for initial wave function
      bool ReadInitialWfPolicy
         (  std::istream&
         ,  std::string&
         );

#ifdef VAR_MPI
      //! Read MPI comm
      bool ReadMpiComm
         (  std::istream&
         ,  std::string&
         );
#endif /* VAR_MPI */

      //! Ostream
      friend std::ostream& operator<<
         (  std::ostream&
         ,  const TdHCalcDef&
         );
};

} /* namespace midas::input */

#endif

