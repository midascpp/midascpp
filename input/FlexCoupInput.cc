/**
************************************************************************
* 
* @file                FlexCoupInput.cc
*
* 
* Created:             10-02-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for flexible couplings 
* 
*
* Last modified:       08-12-2014
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <iostream>


#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FlexCoupCalcDef.h"
#include "input/PesCalcDef.h"

using std::vector;
using std::map;
using std::make_pair;
using std::stringstream;

string GroupCoupInput(std::istream&,FlexCoupCalcDef&,In);
string SystemCoupInput(std::istream&,FlexCoupCalcDef&,In);

/**
* Read and check FlexCoup input.
* */
string FlexCoupInput(std::istream& Minp,FlexCoupCalcDef& arCalcDef, In arInputLevel)
{
   string s;//To be returned when we are done reading this level
   enum INPUT {ERROR,GROUPCOUPLINGS,SYSTEMCOUP,MOLECULEFILE,SCREENESTIMATES,PRESCREENADGA,VCCPERT};
   In input_level = arInputLevel + I_1;
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"GROUPCOUPLINGS",GROUPCOUPLINGS},
      {"#"+std::to_string(input_level)+"SYSTEMCOUP",SYSTEMCOUP},
      {"#"+std::to_string(input_level)+"MOLECULEFILE",MOLECULEFILE},
      {"#"+std::to_string(input_level)+"SCREENESTIMATES",SCREENESTIMATES},
      {"#"+std::to_string(input_level)+"PRESCREENADGA",PRESCREENADGA},
      {"#"+std::to_string(input_level)+"VCCPERT",VCCPERT}    
   };

   arCalcDef.SetmSetFlexCoup(true);
   

   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);
      if (already_read) already_read=false;
      switch(input)
      {
         case GROUPCOUPLINGS:
         {  
            arCalcDef.SetmSetGroupCouplings(true);
            s = GroupCoupInput(Minp,arCalcDef, input_level);
            already_read = true;
            break;
         }
         case SYSTEMCOUP:
         {
            arCalcDef.SetmSetSystemCoup(true);
            s = SystemCoupInput(Minp,arCalcDef,input_level);
            already_read = true;
            break;
         }
         case MOLECULEFILE:
         {
            already_read = arCalcDef.ReadMoleculeFile(Minp,s);
            break;
         }
         case SCREENESTIMATES:
         {
            getline(Minp,s);
            while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
            arCalcDef.SetmMcScreenFile(s);
            getline(Minp,s);
            arCalcDef.SetmMcScreenThr(midas::util::FromString<Nb>(s));
            break;
         }
         case PRESCREENADGA:
         {  
            gPesCalcDef.SetmDoAdgaPreScreen(true);
            getline(Minp,s);
            vector<Nb> vector_thresh;
            vector_thresh=midas::util::VectorFromString<Nb>(s);  
            arCalcDef.SetmAdgaPreScreenThr(vector_thresh);
            break;
         }
         case VCCPERT:
         {
            getline(Minp,s);
            In vcc_order=midas::util::FromString<In>(s);
            arCalcDef.SetmVccPertOrder(vcc_order);

            getline(Minp,s);
            In vcc_mc_order=midas::util::FromString<In>(s);
            arCalcDef.SetmMaxEx(vcc_mc_order);

            In n_lines=I_0;
            if (vcc_order==I_1)
               n_lines=I_2;
            else if (vcc_order==I_2)
               n_lines=I_4;
            else
               MIDASERROR("FlexCoup for VccPert only possible in 1st and 2nd order");


            for (In i=I_0 ; i<n_lines; i++)
            {
               getline(Minp,s);
               while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
               arCalcDef.AddmVccPertFile(s);
            }
            getline(Minp,s);
            arCalcDef.SetmVccPertThr(midas::util::FromString<Nb>(s));

            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a FlexCoup level " << input_level<< " Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
   }
   }
   
   return s;
};
/**
* Read and check GroupCouplings input.
* */
string GroupCoupInput(std::istream& Minp,FlexCoupCalcDef& arCalcDef, In arInputLevel)
{
   string s;//To be returned when we are done reading this level
   In input_level = arInputLevel + I_1;
   enum INPUT {ERROR,MODEGROUPS,ENDMODEGROUPS,GROUPMODECOUPLINGS,ENDGROUPMODECOUPLINGS};
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"MODEGROUPS",MODEGROUPS},
      {"#"+std::to_string(input_level)+"ENDMODEGROUPS",ENDMODEGROUPS},
      {"#"+std::to_string(input_level)+"GROUPMODECOUPLINGS",GROUPMODECOUPLINGS},
      {"#"+std::to_string(input_level)+"ENDGROUPMODECOUPLINGS", ENDGROUPMODECOUPLINGS}
   };


   while (getline(Minp,s) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);
      switch(input)
      {
         case MODEGROUPS:
         {
            getline(Minp,s);  
            while(!(s.find("ENDMODEGROUPS") != s.npos) )
            { 
               arCalcDef.PushBackmModeGroups(s);  
               getline(Minp,s);  
               transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            }
            break;
         }
         case GROUPMODECOUPLINGS:
         {   
            getline(Minp,s);  
            while(!(s.find("ENDGROUPMODECOUPLINGS") != s.npos) )
            {
               arCalcDef.PushBackmGroupCombis(s);      
               getline(Minp,s);  
               transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            }
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a GROUPCOUPLINGS level  "<< input_level << " Input keyword! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
};
/**
* Read and check SystemCoup input.
* */
string SystemCoupInput(std::istream& Minp,FlexCoupCalcDef& arCalcDef, In arInputLevel)
{
   string s;//To be returned when we are done reading this level
   In input_level = arInputLevel + I_1;
   enum INPUT {ERROR,EDIFF,LOCAL};
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"EDIFF",EDIFF},
      {"#"+std::to_string(input_level)+"LOCAL",LOCAL}
   };


   while (getline(Minp,s) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);
      switch(input)
      {
         case EDIFF :
         {
            getline(Minp,s); 
            vector<Nb> vector_ediff;
            vector_ediff=midas::util::VectorFromString<Nb>(s);  
            arCalcDef.SetmEdiffThreshs(vector_ediff);
            break;
         }
         case LOCAL :
         {   
            getline(Minp,s);  
            vector<Nb> vector=midas::util::VectorFromString<Nb>(s);
            arCalcDef.SetmLocalThreshs(vector);
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a SYSTEMCOUP level " << input_level << " Input keyword! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
};
