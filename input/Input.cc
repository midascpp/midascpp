/**
************************************************************************
* 
* @file                Input.cc
*
* 
* Created:             19-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the midas main.
* 
* Last modified:       17-12-2014 (C. Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include<dirent.h>
#include<unistd.h>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "util/Io.h"
#include "input/Input.h"
#include "mmv/MidasMatrix.h"
#include "input/OpInfo.h"
#include "input/VibInput.h"
#include "input/OpDef.h"
#include "input/FindKeyword.h"
#include "input/FileConversionInput.h"
#include "input/InputReader.h"
#include "input/TensorDecompInput.h"
#include "tensor/TensorDecompInfo.h"
#include "input/GlobalData.h"
#include "input/ProgramSettings.h"

//#ifdef VAR_MPI
//#include <mpi.h>
//#include "mpi/Blob.h"
//#include "mpi/Signal.h"
//#endif
#include "mpi/FileToString.h"

using std::vector;
using std::map;
using std::string;
using std::transform;
using std::sort;
using std::unique;
using std::find;
/**
* Local Declarations
* */
typedef map<string,In>::iterator Msii;
typedef vector<string>::iterator Vsi;
typedef string::size_type Sst;

std::istringstream LocalInputCopy(const std::string&);
void Initialize();
void InitializeTest();
void InitializeAnalysis();
void InitializeVib();
void InitializeElec();
void InitializeTdecomp();
void InitializeGeneral();
void InitializeSystem();
void InputAdjustments();
void InputSummary();
std::string PesInput(std::istream&,bool,std::string);
std::string ElecInput(std::istream&,bool,std::string);
std::string TestInput(std::istream&,bool);
std::string GeneralInput(std::istream&);
std::string SysInput(std::istream&,bool,std::string);
std::string GeoOptInput(std::istream&);
std::string MLInput(std::istream&);
std::string TdecompInput(std::istream&);
void ParseSetInfoLine(string&,OpInfo&);

namespace midas::input
{
std::string AnalysisInput(std::istream&,bool,std::string);
} /* namespace midas::input */

/**
* Special call for declaring variables otherwise external
* */
#include "input/InputInit.h"
#include "input/GetLine.h"

using namespace midas::input;

/**
 * Driver for the input module.
 **/
void InputDrv
   (  const std::string& aFileName
   )
{
   Initialize();
   std::istringstream input_file = LocalInputCopy(aFileName); 
   ReadInput(input_file);
   InputAdjustments();
   InputSummary();
}

/**
* Put relevant part of input file into stringstream
* Loop until end of Midas.inp
* Store only lines between sbegin and send (first encounter) 
* and lines not beginning with ! and lines empty in content.
* Ignore also lines according to C++ comment convention.
* Blanks from the end of each line is ignored.
*
* @param aFileName   The filename of the input file.
*
* @return   Returns stringstream with cleaned input.
* */
std::istringstream LocalInputCopy
   (  const std::string& aFileName
   )
{
   // Open the original input file
   std::istringstream Minp = midas::mpi::FileToStringStream(aFileName);

   // Open file for storage of cleaned input
   if (!Minp)
   {
      std::stringstream sstr;
      sstr  << "The Midas.inp input file could not be opened\n"
            << "Cannot open input file, so I might as well stop here, right?";
      MIDASERROR(sstr.str());
   }

   std::ostringstream Minp2;
   std::string s;
   std::getline(Minp,s);
   Sst i_back = s.find("//");          
                                        // remove comments
   if (i_back != s.npos) s.erase(i_back,s.size()-i_back);
                                        // s.npos = std::string::npos Portable?
   Sst i_last = s.find_last_not_of(" ");   
                                        // remove blanks at the end
   if (i_last != s.npos) s.erase(i_last+1,s.size()-i_last-1); 
   if (i_last == s.npos || i_last == 0) while(s.substr(I_0,I_1)==" ") s.erase(I_0,I_1); 
   // Delete leading blanks if there was only blanks left.
  
   bool this_is_input = false;
   bool ccskip        = false;
   if (!ccskip && s.find("/*")!=s.npos) ccskip = true;
   string sbegin = "#0Midas Input";
   string send   = "#0Midas Input End";          
   while (!Minp.eof())          
   {
      if (!ccskip) 
      {
         if (StringNoCaseCompare(s,sbegin)) this_is_input=true;
         if (this_is_input&& s.substr(I_0,I_1)!="!" //&& s.substr(I_0,I_1)!="%" // % accepted.
               && s.size() !=I_0)  
         {
            // transform(s.begin(),s.end(),s.begin(),toupper);         // All to uppercase
            // while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
            Minp2 << s << '\n';          
         }
         if (this_is_input && StringNoCaseCompare(s,send)) break;      
      }
      if (ccskip && s.find("*/")!=s.npos) ccskip = false;
      getline(Minp,s);
      if (!ccskip && s.find("/*")!=s.npos) ccskip = true;
      Sst i_back = s.find("//");
      if (i_back != s.npos) s.erase(i_back,s.size()-i_back);          
      Sst i_last = s.find_last_not_of(" ");          
      if (i_last != s.npos) s.erase(i_last+1,s.size()-i_last-1);      
      if (i_last == s.npos || i_last == 0) while(s.substr(I_0,I_1)==" ") s.erase(I_0,I_1); 
      // Delete leading blanks if there was only blanks left.
   }
   
   return std::istringstream{ Minp2.str() };
}
/**
* Read Input:
* Find the different sections denoted by #1X and pass control
* to appropriate XInput functions to read until next #1Y or end.
*
* Possible input flags are set up as keys in a map with 
* value equal to ENUM values.
* These ENUM values are used to "switch" to appropriate
* sub-input-function based upon case-insensitive 
* reference to the key.
*
* The input-sector flags are lall upper case! With #'s!
* */
void ReadInput
   (  std::istream& Minp
   )
{
   enum SECTOR {ERROR, EMPTY, START, TEST, PES, ELEC, TDECOMP, ANALYSIS, VIB, GENERAL, SYSTEM, TD, INCRPES, FILECONVERSION, SINGLEPOINT, GEOOPT, MLTASK, END};
   const map<string,SECTOR> input_sector
   {  {"#0MIDASINPUT",START}
   ,  {"#1TEST",TEST}
   ,  {"#1VIB",VIB}
   ,  {"#1PES",PES}
   ,  {"#1ELEC",ELEC}
   ,  {"#1ANALYSIS",ANALYSIS}
   ,  {"#1GENERAL",GENERAL}
   ,  {"#1SYSTEM",SYSTEM}
   ,  {"#1TD", TD}
   ,  {"#1INCRPES",INCRPES}
   ,  {"#1FILECONVERSION",FILECONVERSION}
   ,  {"#1SINGLEPOINT",SINGLEPOINT}
   ,  {"#1TENSORDECOMP",TDECOMP}
   ,  {"#1GEOOPT",GEOOPT}
   ,  {"#1MLTASK",MLTASK}
   ,  {"#0MIDASINPUTEND",END}
   };


   string s;
   GetLine(Minp,s);
   bool end_found = false;
   while (!Minp.eof())                          // Loop through the input file
   {
      string s_orig = s;
      //Mout << "string = " << s << endl;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // All to uppercase
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks

      SECTOR input = midas::input::FindKeyword(input_sector,s);

      bool passive = false;                                         // Read serious unless
      //if ((aInputFlag!= "init")&&(input != ihit)) passive = true;

      switch(input)
      {
         case START:
         {
            //Mout << endl << " Start of Midas input analysis. " << endl;
            GetLine(Minp,s);
            break;
         }
         case TEST:
         {
            s = TestInput(Minp,passive);
            break;
         }
         case PES:
         {
            s = PesInput(Minp, passive, "lolflag");
            break;
         }
         case ANALYSIS:
         {
            s = AnalysisInput(Minp, passive, "lolflag");
            break;
         }
         case VIB:
         {
            s = VibInput(Minp, passive);
            break;
         }
         case ELEC:
         {
            s = ElecInput(Minp,passive,"lolflag");
            break;
         }
         case GEOOPT:
         {
            s = GeoOptInput(Minp);
            break;
         }
         case MLTASK:
         {
            s = MLInput(Minp);
            break;
         }
         case TDECOMP:
         {
            s = TdecompInput(Minp);
            break;
         }
         case GENERAL:
         {
            s = GeneralInput(Minp);
            break;
         }
         case SYSTEM:
         {
            s = SysInput(Minp,passive,"lolflag");
            break;
         }
         case FILECONVERSION:
         {
            s = FileConversionInput(Minp);
            break;
         }
         case SINGLEPOINT:
         {
            bool already_read = InputReader(Minp, s, CreateSinglePointCalcDef(2).Map());
            break;
         }
         case END:
         {
            //Mout << " End of Midas input analysis \n \n" << endl;
            end_found = true;
            GetLine(Minp,s);                    // bring Minp in eof state
            break;
         }
         case ERROR://FALLTHROUGH
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a level 1 keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
            //GetLine(Minp,s);
         }
      }
   }

   if (!end_found)
   {
      MIDASERROR("DIDN't FIND END OF INPUT! last thing read was: " + s);
   }

   //
   AfterProcessSinglePointInput();
   
   if (gDoPes)
   {
      // If doing a surface calculation, there need to be some molecule information present
      if (gSystemDefs.size() == I_0)
      {
         auto default_molecule_name = gMainDir + "/Molecule.mmol";

         // Check for default molecule file name 
         if (midas::filesystem::Exists(default_molecule_name))
         {
            Mout << " No information on the molecular system provided but found Molecule.mmol file at: " << std::endl << "  " << default_molecule_name << std::endl;
   
            SystemDef sysdef_calcdef;
            std::istringstream molecule_stream("Midas\n" + default_molecule_name);

            std::string line_string;
            bool already_read;
            auto molecule_info = ReadMoleculeFileInput(molecule_stream, line_string, already_read);
            
            sysdef_calcdef.SetName("default_molecule");
            sysdef_calcdef.SetMoleculeFileInfo(molecule_info);
            gSystemDefs.push_back(sysdef_calcdef);
         }
      }
         
      // If doing a double incremental surface, then the fragments should be found in a seperate folder of the main directory
      if (gPesCalcDef.GetmDincrSurface())
      {
         auto fc_dir = gMainDir + "/FC_savedir";
         if (midas::filesystem::IsDir(fc_dir))
         {
            // Check that the file with information on the fragment combinations is correct
            std::ifstream ifs(fc_dir + "/" + gPesCalcDef.GetmFcInfo(), std::ios_base::in); 
   
            // Check that file exists
            if (!ifs)
            {
               MIDASERROR("Could not open the file indicated under the #2 DOUBLEINCREMENTAL keyword, which contain information on fragment combinations: " + fc_dir + "/" + gPesCalcDef.GetmFcInfo());
            }
              
            std::vector<In> fc_file_numbers;
            std::string s;
            while (std::getline(ifs, s))
            {
               // Check for comment line
               std::size_t found_comment = s.find("#");
               if (found_comment != std::string::npos)
               {
                  continue;
               }
   
               // Extract info from first row of each line
               std::string fc_name;
               std::istringstream iss(s);
               iss >> fc_name;
               auto fc_file_number = fc_name.erase(0,3);
               
               fc_file_numbers.push_back(midas::util::FromString<In>(fc_file_number));
            }

            // If double incremental method is DIF, then we look for the associated FC_X.mmol files
            if (gPesCalcDef.GetmDincrMethod() == "DIF")
            {
               for (In isystem = I_0; isystem < fc_file_numbers.size(); ++isystem)
               {
                  auto fc_file_name = fc_dir + "/FC_" + std::to_string(fc_file_numbers[isystem]) + ".mmol";
                  if (!midas::filesystem::Exists(fc_file_name))
                  {
                     MIDASERROR("Did not find fragment combination FC_" + std::to_string(fc_file_numbers[isystem]) + ", which was supposed to be on file " + fc_file_name);
                  }

                  if (gPesIoLevel > I_4)
                  {
                     Mout << " Will include fragment combination FC_" << std::to_string(fc_file_numbers[isystem]) << " for surface generation via file: " << std::endl << "  " << fc_file_name << std::endl;
                  }
               
                  SystemDef sysdef_calcdef;
                  std::istringstream molecule_stream("Midas\n" + fc_file_name);
             
                  std::string line_string;
                  bool already_read;
                  auto subsystem_info = ReadMoleculeFileInput(molecule_stream, line_string, already_read);
          
                  sysdef_calcdef.SetName("FC_" + std::to_string(fc_file_numbers[isystem]));
                  sysdef_calcdef.SetMoleculeFileInfo(subsystem_info);
                  gSystemDefs.push_back(sysdef_calcdef);
               }
            }
            // If the double incremental method is DIFACT, then we look for the FC_X_AUX_CAPPED.mmol files first, and the FC_X_INTER.mmol, FC_X_REL.mmol and FC_X.mmol files
            else if (gPesCalcDef.GetmDincrMethod() == "DIFACT")
            {
               for (In isystem = I_0; isystem < fc_file_numbers.size(); ++isystem)
               {
                  auto fc_file_name = fc_dir + "/FC_" + std::to_string(fc_file_numbers[isystem]) + "_AUX_CAPPED.mmol";
                  
                  if (!midas::filesystem::Exists(fc_file_name))
                  {
                     MIDASERROR("Did not find fragment combination FC_" + std::to_string(fc_file_numbers[isystem]) + ", which was supposed to be on file " + fc_file_name);
                  }

                  if (gPesIoLevel > I_4)
                  {
                     Mout << " Will include fragment combination FC_" << std::to_string(fc_file_numbers[isystem]) << " for surface generation via file: " << std::endl << "  " << fc_file_name << std::endl;
                  }

                  SystemDef sysdef_calcdef;
                  std::istringstream molecule_stream("Midas\n" + fc_file_name);
             
                  std::string line_string;
                  bool already_read;
                  auto subsystem_info = ReadMoleculeFileInput(molecule_stream, line_string, already_read);
          
                  sysdef_calcdef.SetName("FC_" + std::to_string(fc_file_numbers[isystem]) + "_AUX_CAPPED");
                  sysdef_calcdef.SetMoleculeFileInfo(subsystem_info);
                  gSystemDefs.push_back(sysdef_calcdef);
               }
               
               // Define file extensions for auxiliary molecule files
               std::vector<std::string> file_extensions = {"_INTER.mmol", "_REL.mmol", ".mmol"};

               // Read in auxiliary molecule files
               for (In isystem = I_0; isystem < fc_file_numbers.size(); ++isystem)
               {
                  // Loop over extensions
                  for (In iext = I_0; iext < file_extensions.size(); ++iext)
                  {
                     auto fc_file_name = fc_dir + "/FC_" + std::to_string(fc_file_numbers[isystem]) + file_extensions[iext];
                     
                     if (!midas::filesystem::Exists(fc_file_name))
                     {
                        MIDASERROR("Did not find auxiliary molecule files for fragment combination FC_" + std::to_string(fc_file_numbers[isystem]) + ", which was supposed to be on file " + fc_file_name);
                     }
                   
                     SystemDef sysdef_calcdef;
                     std::istringstream molecule_stream("Midas\n" + fc_file_name);
                   
                     std::string line_string;
                     bool already_read;
                     auto subsystem_info = ReadMoleculeFileInput(molecule_stream, line_string, already_read);
                   
                     sysdef_calcdef.SetName("FC_" + std::to_string(fc_file_numbers[isystem]) + file_extensions[iext]);
                     sysdef_calcdef.SetMoleculeFileInfo(subsystem_info);
                     gSystemDefs.push_back(sysdef_calcdef);
                  }
               }
            }
            else
            {
               MIDASERROR("Double incremental scheme is not recognized");
            }
         }
         else
         {
            Mout << " No information on the molecular system provided, assuming that fragment information will generated later " << std::endl;
         }
      }
         
      // Don't know what to do, throw an error 
      if (gSystemDefs.size() == I_0)
      {
         MIDASERROR("No information on the molecular system is provided, please provide it under the #3 MOLECULEINFO keyword");
      }
      
      // Check the correctness of the pes-module input perform some after-input proccessing
      gPesCalcDef.InputAfterProcessing();
  
      // Print input information and intepretations
      gPesCalcDef.PrintCalcDefInfo();
   }
}

/**
* Read and check Io  input.
* Read until next s = "#1..." which is returned.
* */
std::string GeneralInput(std::istream& Minp)
{
   if (gGeneral)
   {
      Mout << " Warning! I have read General input once " << std::endl;
   }

   gGeneral = true;
   enum INPUT 
      {  ERROR
      ,  IOLEVEL
      ,  TIME
      ,  TECHNICALINFORMATION
      ,  BUFSIZE
      ,  FILESIZE
      ,  DEBUG
      ,  MAINDIR
      ,  SAVEDIR
      ,  ANALYSISDIR
      ,  TEMPERATURE
      ,  NUMLINALG
      ,  RESERVEOPER
      ,  RESERVEMODES
      ,  SEED
      };
   const std::map<std::string, INPUT> input_word =
      {  {"#2IOLEVEL", IOLEVEL}
      ,  {"#2TIME", TIME}
      ,  {"#2TECHNICALINFORMATION", TECHNICALINFORMATION}
      ,  {"#2BUFSIZE", BUFSIZE}
      ,  {"#2FILESIZE", FILESIZE}
      ,  {"#2DEBUG", DEBUG}
      ,  {"#2MAINDIR", MAINDIR}
      ,  {"#2SAVEDIR", SAVEDIR}
      ,  {"#2ANALYSISDIR", ANALYSISDIR}
      ,  {"#2TEMP", TEMPERATURE}
      ,  {"#2NUMLINALG", NUMLINALG}
      ,  {"#2RESERVEOPER" , RESERVEOPER }
      ,  {"#2RESERVEMODES", RESERVEMODES}
      ,  {"#2SEED", SEED}
      };

   // Path variabels to working directories
   std::string main_dir_path = ""; 
   std::string save_dir_path = ""; 
   std::string analysis_dir_path = ""; 

   std::string s;
   auto input_level = I_2;
   while (GetLine(Minp,s) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      std::string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            int iolevel;
            input_s >> iolevel;
            midas::input::gProgramSettings.GetGeneralSettings().SetIoLevel(iolevel);
            break;
         }
         case TIME:      
         {
            gTime = true;
            break;
         }
         case TECHNICALINFORMATION:
         {
            midas::input::gProgramSettings.GetGeneralSettings().SetNumericLimits(true);
            break;
         }
         case RESERVEOPER:
         {
            auto reserve_oper = midas::input::GetNb(Minp, s);
            midas::input::gProgramSettings.GetGeneralSettings().SetReserveOper(reserve_oper);
            break;
         }
         case RESERVEMODES:
         {
            auto reserve_modes = midas::input::GetNb(Minp, s);
            midas::input::gProgramSettings.GetGeneralSettings().SetReserveModes(reserve_modes);
            break;
         }
         case SEED:
         {
            midas::input::GetLine(Minp, s);
            if(midas::input::IsKeyword(s))
            {
               MIDASERROR("No seed sequence given.");
            }
            midas::input::gProgramSettings.GetGeneralSettings().SetSeed(s);
            break;
         }
         case BUFSIZE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> gMaxBufSize;
            break;
         }
         case FILESIZE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> gMaxFileSize;
            break;
         }
         case DEBUG:      
         {
            midas::input::gProgramSettings.GetGeneralSettings().SetDebug(true);
            break;
         }
         case MAINDIR:
         {
            midas::input::GetParsedPathLine(Minp, s);
            main_dir_path = s;
            
            break;
         }
         case SAVEDIR:
         {
            midas::input::GetParsedPathLine(Minp, s);
            save_dir_path = s;
            
            break;
         }
         case ANALYSISDIR:
         {
            midas::input::GetParsedPathLine(Minp, s);
            analysis_dir_path = s;
            
            break;
         }
         case TEMPERATURE:
         {
            GetLine(Minp,s);                                  // Get new line with directory name
            istringstream input_s(s);
            if (input_s >> gTempFirst)
            {
               gTempSet = true;
               if (input_s >> gTempLast) 
               {
                  if (input_s >> gTempSteps)
                  {
                     Mout << " Will output date for " << gTempSteps << " temperaturs" << std::endl;
                  }
               }
            }
            else
            {
               gTempFirst = 298.15;
            }
            break;
         }
         case NUMLINALG:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            gNumLinAlg=meth;
            break;
         }
         case ERROR: //FALLTHROUGH
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a General level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // If directory paths have been indicated, then we overwrite the command-line input
   if (!main_dir_path.empty())
   {
      gMainDir = main_dir_path;
   
      if (save_dir_path.empty())
      {
         gSaveDir = main_dir_path + "/savedir";
      }

      if (analysis_dir_path.empty())
      {
         gAnalysisDir = main_dir_path + "/analysis";
      }
   }
   //
   if (!save_dir_path.empty())
   {
      gSaveDir = save_dir_path;
   }
   //
   if (!analysis_dir_path.empty())
   {
      gAnalysisDir = analysis_dir_path;
   }

   return s;
}

/**
* Initialize 
* The bool initializations are actually not needed here - auto init. 
* is done in the declarations above. 
* */
void Initialize()
{
   // Initialize control variable 
   InitializeGeneral();
   InitializeTest();  
   InitializeAnalysis();
   InitializeVib();
   InitializeElec();
   InitializeSystem(); 
}

/**
 * Initialize general variables
**/
void InitializeGeneral()
{
   gGeneral             = false;
   gTime                = false;
   gTechInfo            = false;
   gDebug               = false;
   gIoLevel             = 0;
   gMaxBufSize          = I_10_7;
   gMaxFileSize         = I_10_7;
   gAtomicData.Init(); 
   gTempSet             = false;
   gTempFirst           = C_0;
   gTempLast            = C_0;
   gTempSteps           = I_1;
   gNumLinAlg           = "LAPACK";
}

/**
 * Set operator type
 **/
void ParseSetInfoLine
   (  std::string& aStr
   ,  OpInfo& op_info
   )
{
   if (gDebug)
   {
      Mout << " String in ParseSetInfoLine: " << aStr << std::endl;
   }
   
   // General search string.
   std::string s_hit;     

   transform(aStr.begin(), aStr.end(), aStr.begin(), (In(*) (In))toupper);

   // Parse Type
   std::string s_type;
   if (GetValueFromKey<std::string>(aStr, "TYPE", s_type) != 0)
   {
      MIDASERROR
         (  "Cannot parse from #3 SETINFO with type: '" + s_type + "'! Check your input please!\n"
         +  "SetInfo line = '" + aStr + "'."
         );
   }
  
   transform(s_type.begin(), s_type.end(), s_type.begin(), (In(*) (In))tolower);
   
   op_info.SetType(oper::StringToPropertyType(s_type));
   
   // Parse FRQ (if any)
   const auto& property_type = op_info.GetType();
   if(property_type.GetOrder() > 1)
   {
      Nb frq;
      for(int i = 0; i < (property_type.GetOrder() - 1); ++i)
      {
         if (GetValueFromKey<Nb>(aStr, "FRQ_" + std::to_string(i), frq) != 0)
         {
            MIDASERROR
               (  "Error: Type *_POL: Cannot get FRQ_" + std::to_string(i) + ".\n"
               +  "Parsing string : '" + aStr + "'."
               );
         }
         op_info.SetFrq(i, frq);
      }
   }
}
