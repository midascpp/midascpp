/*
************************************************************************
*  
* @file                NormalCoordInput.cc
*
* Created:             08-07-2021
*
* Author:              Bo Thomsen (DrBoThomsen@gmail.com)
*
* Short Description:   Functions for reading the normal coordinate
*                      input under the #3ModVibCoord keyword
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <map>
#include <string>

#include "input/Input.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"
#include "input/FindKeyword.h"

/**
 * @param Minp          Input stream
 * @param aCalcDef     CalcDef
 * @param arInputLevel  Input level
 * @return
 *    Last read string
 **/
std::string NormalCoordInput
   (  std::istream& Minp
   ,  input::NormalCoordCalcDef& aCalcDef
   ,  const In& arInputLevel
   )
{
   In input_level = arInputLevel + I_1;
   std::string s; //To be returned when we are done reading this level

   // Keywords
   enum INPUT
   {  ERROR
   ,  PROJECTOUTTRANSROT
   ,  DISABLEFREQUNCYCHECK
   ,  IGNOREGRADIENT
   ,  OUTPUTFILENAME
   };
   
   // Conversion map
   const std::map<std::string,INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"ERROR", ERROR}
   ,  {"#"+std::to_string(input_level)+"PROJECTOUTTRANSROT", PROJECTOUTTRANSROT}
   ,  {"#"+std::to_string(input_level)+"DISABLEHESSIANCHECK", DISABLEFREQUNCYCHECK}
   ,  {"#"+std::to_string(input_level)+"IGNOREGRADIENT", IGNOREGRADIENT}
   ,  {"#"+std::to_string(input_level)+"OUTPUTFILENAME", OUTPUTFILENAME}
   };

   // Read lines
   bool already_read=false;
   std::string s_orig = "";
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case PROJECTOUTTRANSROT:
         {
            midas::input::GetLine(Minp, s);
            if (  midas::input::NotKeyword(s)
               )
            {
               In n_it = midas::util::FromString<In>(s);
               aCalcDef.SetProjectOutTransRot(n_it);
            }
            else
            {
               aCalcDef.SetProjectOutTransRot(I_1);
               already_read = true;
            }

            break;
         }
         case DISABLEFREQUNCYCHECK:
         {
            aCalcDef.SetCheckFrequencies(false);
            break;
         }
         case IGNOREGRADIENT:
         {
            aCalcDef.SetIgnoreGradient(true);
            break;
         }
         case OUTPUTFILENAME:
         {
            midas::input::GetLine(Minp,s); // Get new line with input
            aCalcDef.SetFileName(s);
            break;
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a LHA level " << input_level << " Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Return read string
   return s;
}
