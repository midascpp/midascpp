/**
************************************************************************
* 
* @file                FreqAnaCalcDef.h
*
* 
* Created:             04-05-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Calculation definition frequency analysis
* 
* Last modified:       09-06-2015 (Mads Boettger Hansen)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FREQANACALCDEF_H_INCLUDED
#define FREQANACALCDEF_H_INCLUDED

// std headers
#include <string>

// midas headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"


namespace midas::input
{

/* ************************************************************************
************************************************************************ */
class FreqAnaCalcDef
{
   //General options:
   CREATE_VARIABLE(Nb, DispFactor, C_I_10_2);
   CREATE_VARIABLE(bool, NonSymmetricHessian, false);
   CREATE_VARIABLE(bool, CompareSymAndNonSym, false);
   CREATE_VARIABLE(std::string, SinglePointName, std::string{"SP_FreqAna"});
   CREATE_VARIABLE(In, NumNodes, I_1);
   CREATE_VARIABLE(In, ProcPerNode, I_1);
   CREATE_VARIABLE(In, DumpInterval, I_10);
   CREATE_VARIABLE(Nb, RotationThr, C_I_10_6);
   CREATE_VARIABLE(std::string, MolInfoOutName, std::string{"MoleculeInfo_FreqAna.mmol"});
   
   //Targeting options (below is only used if mTargeting == true:
   CREATE_VARIABLE(bool, Targeting, false);
   CREATE_VARIABLE(Nb, ItEqEnerThr, C_I_10_8);
   CREATE_VARIABLE(Nb, ItEqResThr, C_I_10_8);
   CREATE_VARIABLE(In, ItEqBreakDim, I_10_3);
   CREATE_VARIABLE(In, ItEqMaxIt, I_10_3);
   CREATE_VARIABLE(Nb, OverlapSumMin, C_8 * C_I_10_1 );
   CREATE_VARIABLE(In, OverlapNMax, C_IN_MAX);
   CREATE_VARIABLE(bool, AlreadyMassWeighted, false);
};

} // namespace midas::input

#endif //FREQANACALCDEF_H_INCLUDED
