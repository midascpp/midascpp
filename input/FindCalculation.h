/**
************************************************************************
* 
* @file                FindCalculation.h
*
* 
* @date                06-12-2018
*
* @author              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief               Tool for finding the index of a calculation based on name.
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FINDCALCULATION_H_INCLUDED
#define FINDCALCULATION_H_INCLUDED

namespace midas::input
{

/**
 * Find calculation number
 *
 * @param aCalcDefVec         Vector of calcdefs to search in
 * @param aName               Name of calculation to search for
 * @param aMethodLabel        Label for output
 *
 * @return
 *    Index of the relevant calculation
 **/
template
   <  typename CALCDEF
   >
In FindCalculationIndex
   (  const std::vector<CALCDEF>& aCalcDefVec
   ,  const std::string& aName
   ,  const std::string& aMethodLabel
   )
{
   Mout  << " Find index in of " << aMethodLabel << " calculation with name: '" << aName << "'." << std::endl;

   // Find calc
   In i_calc = -I_1;
   if (  aCalcDefVec.size() > 1
      )
   {
      for(In i=I_0; i<aCalcDefVec.size(); ++i)
      {
         const auto& vn = aCalcDefVec[i].GetName();
         Mout  << " Checking " << aMethodLabel << " with name: " << vn << std::endl;

         if (  vn == aName
            )
         {
            i_calc = i;
            break;
         }
      }
      if (  i_calc == -I_1
         )
      {
         MIDASERROR(aMethodLabel + " calculation '" + aName + "' not found!");
      }
   }
   else
   {
      Mout  << " Only one " << aMethodLabel << " calculation found with name '" << aCalcDefVec.front().GetName() << "'. I guess this is the one!" << std::endl;
      i_calc = I_0;
   }

   return i_calc;
}

} /* namespace midas::input */

#endif /* FINDCALCULATION_H_INCLUDED */
