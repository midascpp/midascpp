/**
************************************************************************
* 
* @file                OdeInput.h
*
* 
* Created:             03-04-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input reader for ODE integration
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEINPUT_H_INCLUDED
#define ODEINPUT_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"

#include "ode/OdeInfo.h"
#include "ode/OdeErrorNorm.h"

///>
bool OdeInput
   (  std::istream&
   ,  std::string&
   ,  In
   ,  midas::ode::OdeInfo&
   );

namespace detail 
{

//! Validate input
void ValidateOdeInput
   (  midas::ode::OdeInfo&
   );

} /* namespace detail */

#endif /* ODEINPUT_H_INCLUDED */
