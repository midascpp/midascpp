/**
************************************************************************
* 
* @file                ProductTerm.h
*
* Created:             18-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for holding an operator product term 
* 
* Last modified: man mar 21, 2005  11:44
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PRODUCTTERM_H_INCLUDED
#define PRODUCTTERM_H_INCLUDED

// Standard headers:
#include<string>
using std::string;

// My headers:
#include "inc_gen/TypeDefs.h"
#include "input/OpDef.h"

class ProductTerm
{
   private:
      Nb mCoef;                         ///< Coefficient for the term
      vector<In> mModeNrs;              ///< Mode nrs.  
      vector<string> mOperLabels;       ///< Operator labels
   public:
      void GiveCoef(Nb aCoef) {mCoef = aCoef;}  ///< Give Coeff;
      void AddModeOperSet(In aMode, string aOper)
      {
         mOperLabels.push_back(aOper); mModeNrs.push_back(aMode);
      }
                                                ///< Add new mode-oper factor 
      Nb Coef() { return mCoef;} 
      In Mode(In i) { return mModeNrs[i];} 
      string Oper(In i) { return mOperLabels[i];} 
      In Nfactors() { return mModeNrs.size();} 
      void GiveNewMode(In i, In aModeNr) { mModeNrs[i] = aModeNr;} 
      friend class OpDef;
};

#endif /* PRODUCTTERM_H_INCLUDED */

