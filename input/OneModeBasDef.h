/**
************************************************************************
* 
* @file                OneModeBasDef.h
*
* Created:             15/11/2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode basis
* 
* Last modified:       15/11/2007
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONEMODEBASDEF_H
#define ONEMODEBASDEF_H

// std headers
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/OneModeHoDef.h"
#include "input/OneModeGaussDef.h"
#include "input/OneModeBsplDef.h"
#include "input/OneModeStateBasis.h"
#include "mmv/MidasVector.h"

// using declarations
using std::string;

class OneModeBasDef
{
   private:
      string mBasType;                                    ///< Basis type = "HO" or "Gauss" or "Bspline"
      In mIadd;                                           ///< Address for the basis in the global vector
   public:
      OneModeBasDef();                                    ///< Constructor as "zero"
      OneModeBasDef(string aType, In aAdd);               ///< Constructor from address
                                                          ///< Constructor
      In Lower() const;                                   ///< Get Global Mode number
      In Higher() const;                                  ///< Get Global Mode number
      Nb Omeg() const;                                    ///< Get Omega
      Nb Mass() const;                                    ///< Get Mass 
      In Nbas() const;                                    ///< Get Global Mode number
      In Nord() const;                                    ///< Get order for the  Bsplines
      GlobalModeNr GetGlobalModeNr() const;               ///< Get Global Mode number
      std::string Type() const;                                ///< Get Type
      std::string Aux() const;                                 ///< Get Aux info 
      void GetVecOfExps(MidasVector& arVec) const;        ///< Get the vector of exponents, for gaussians
      Nb GetExp(In aAdd) const;                           ///< Get a particular exponent, for gaussians
      Nb BasDens() const;                                 ///< Get density of the basis, for gaussians
      void GetVecOfPts(MidasVector& arVec) const;         ///< Get the position of the gaussians
      std::pair<Nb, Nb> GetBasisGridBounds() const;       ///< Get the basis set boundaries
      Nb GetGaussPnt(In aAdd) const;                      ///< Get the position of a particular gaussian
      void ChangeModeNr(In aGlobalModeNr);                ///< Change mode number
      
      //! Evaluate the basis function
      Nb EvalBasisFunc(const In& aIbas, const Nb& aQval, const bool& aKeepUnboundedBsplines) const;
      
      //! Evaluate the basis function
      void EvalBasisFunc(const In& aIbas, const MidasVector& arQvals, MidasVector& arFuncVals, const bool& aKeepUnboundedBsplines) const;

      //@{
      //! B-spline specific functions
      
      //! Get indx for the last B-spline in the interval aInt
      const In GetLastInd(const In& aInt) const;
      
      //! Get the aInd knot value
      const Nb GetKnotValue(const In& aInd) const;
      
      //! Get the nr of intervals
      const In Nint() const;
      //@}
      
      //! Use info from one basis in another modes basis
      void CloneOneBasisToOtherMode(OneModeBasDef& arOneModeBasDef, In aGlobalMode);
};

#endif /* ONEMODEBASDEF_H */
