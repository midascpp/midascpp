/**
************************************************************************
* 
* @file                TdecompInput.cc
*
* 
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Input reader for the Tdecomp class /
*                      user interface for tensor decompositions
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/TdecompCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"

using std::vector;
using std::map;
using std::transform;
using std::sort;
using std::unique;
using std::find;
using std::string;
using std::getline;

/**
* Read and check Tdecomp input.
* Read until next s = "#i..." i<3 which is returned.
* */
string TdecompInput(std::istream& Minp)
{
   if (gDoTdecomp && gDebug) 
      Mout << " I have read one tensor decomposition input - reading a new tensor decomposition input "<< endl;

   gDoTdecomp  = true; 
   gTdecompCalcDef.push_back(TdecompCalcDef());
   TdecompCalcDef& new_tdecomp = gTdecompCalcDef[gTdecompCalcDef.size()-1];  // new_oper is a reference to the new operator

   enum INPUT {ERROR, IOLEVEL, NAME, TENSORFILE, TENSORDECOMPINFO};
   const map<string,INPUT> input_word =
   {
      {"#2IOLEVEL",IOLEVEL},
      {"#2NAME",NAME},
      {"#2TENSORFILE",TENSORFILE},
      {"#2TENSORDECOMPINFO",TENSORDECOMPINFO}
   };
   string s="";
   In input_level=2;
   bool already_read = false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      if (already_read) already_read = false;
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);       // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);             // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                          // Get new line with input
            new_tdecomp.SetIoLevel(midas::util::FromString<In>(s));
            break;
         } 
         case NAME:
         {
            getline(Minp,s);                                          // Get new line with input
            istringstream input_s(s);
            string s_name="";
            input_s >> s_name;
            new_tdecomp.SetName(s_name);
            for (In i_calc =0;i_calc<gTdecompCalcDef.size()-1;i_calc++)
               if (gTdecompCalcDef[i_calc].GetName() == new_tdecomp.GetName()) 
                  MIDASERROR("name for tensor decomposition is already in use");
            break;
         }
         case TENSORFILE:
         {
            // WTF IS THIS?? SHOULD I JUST READ IN TWO STRINGS??? WHY USE STRINGSTREAMS???
            // first get type (e.g. matlab or turbomole)
            getline(Minp,s);
            istringstream input_stype(s);
            string s_type="";
            input_stype >> s_type;
            new_tdecomp.SetTensorFileType(s_type);

            // get path to file
            getline(Minp,s);
            istringstream input_sfile(s);
            string s_file="";
            input_sfile >> s_file;
            new_tdecomp.SetTensorFileName(s_file);

            break;
         }
         case TENSORDECOMPINFO:
         {
            already_read = new_tdecomp.ReadDecompInfoSet(Minp, s);  // Input flags for decomposer
            break; 
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a TensorDecomp level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}

/**
* Initialize Tdecomp variables
* */
void InitializeTdecomp()
{
   Mout << " Initializing tensor decomposition interface variables: " << endl;
   gDoTdecomp  = false; 
}
