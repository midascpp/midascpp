#ifndef COMBINEDRSPFUNC_H_INCLUDED
#define COMBINEDRSPFUNC_H_INCLUDED

#include <iostream>
#include <vector>
#include <string>

#include "ResponseFunction.h"
#include "RspFunc.h"
#include "mmv/DataCont.h"

class CombinedRspFunc: public ResponseFunction
{
   using frq_const_iterator = std::vector<std::vector<Nb> >::const_iterator;
   using files_t = std::vector<std::pair<std::string,std::string> >;

   private:
      std::vector<std::vector<Nb> > mFrq;
      std::vector<Nb> mGamma;
      std::vector<Nb> mReVal;
      std::vector<Nb> mImVal;
      std::vector<std::string> mFiles;

      In IndexFromFrq(const std::vector<Nb>& frq) const;
      std::string FilenameFromIndexAndLabel(In idx, const std::string& label) const;

      bool AlreadyIncluded(const std::vector<Nb>&) const;
      bool AlreadyIncluded(const RspFunc&) const;
      
      void SkipRsp(std::istream&) const;
      void ReadRsp(std::istream&);

      std::vector<Nb> MakeFrq(const std::vector<Nb>& frq) const
      {
         std::vector<Nb> tot_frq(frq.size()+1);
         tot_frq[0] = 0.0;
         for(int i=0; i<frq.size(); ++i)
         {
            tot_frq[i+1] = frq[i];
            tot_frq[0] -= frq[i];
         }
         return tot_frq;
      }
      
      files_t ParseFilesString(const std::string&) const;  
      std::string CreateFilesString(const files_t& file_pairs) const;

   public:
      /**
       *
       **/
      CombinedRspFunc()
         : ResponseFunction()
      {
      }

      /**
       *
       **/
      CombinedRspFunc(const std::string& str)
         : ResponseFunction(str)
      {
      }

      /**
       *
       **/
      CombinedRspFunc(std::string&& str)
         : ResponseFunction(str)
      {
      }

      /**
       *
       **/
      bool AddRspFunc(const RspFunc&); 
      bool AddRspFunc(std::istream&);  // add response function from stream (must have correct format)

      void Sort();
      void GatherFiles();
      
      frq_const_iterator FrqBegin() const
      {
         return mFrq.begin();
      }

      frq_const_iterator FrqEnd() const
      {
         return mFrq.end();
      }

      //Nb ReRsp(const std::vector<Nb>& frq) const
      Nb ReRsp(const std::vector<Nb>& total_frq) const
      {
         //std::vector<Nb> total_frq = MakeFrq(frq);
         auto it = std::find(mFrq.begin(),mFrq.end(),total_frq);
         if(it != mFrq.end())
         {
            size_t index = std::distance(mFrq.begin(),it);
            return mReVal[index];
         }
         else
         {
            MIDASERROR("Did not find value");
         }
         return Nb(0); // should never reach here
      }
      
      //Nb ImRsp(const std::vector<Nb>& frq) const
      Nb ImRsp(const std::vector<Nb>& total_frq) const
      {
         //std::vector<Nb> total_frq = MakeFrq(frq);
         auto it = std::find(mFrq.begin(),mFrq.end(),total_frq);
         if(it != mFrq.end())
         {
            size_t index = std::distance(mFrq.begin(),it);
            return mImVal[index];
         }
         else
         {
            MIDASERROR("Did not find value");
         }
         return Nb(0); // should never reach here
      }
      
      std::string FilenameFromFrqAndLabel(Nb frq, const std::string& label) const;
      DataCont DataContFromFrqAndLabel(Nb frq, const std::string& label, In size) const;

      friend std::ostream& operator<<(std::ostream&, const CombinedRspFunc&);
};

#endif /* COMBINEDRSPFUNC_H_INCLUDED */
