/**
************************************************************************
* 
* @file                MidasOperatorInput.h
* 
* Created:             04-02-2014
*
* Author:              Bo Thomsen (ove@chem.au.dk)
*
* Short Description:   Reader for the midas .mop files
* 
* Last modified: Mon Feb 04, 2014  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASOPERATORINPUT_H_INLCUDED
#define MIDASOPERATORINPUT_H_INLCUDED

#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <utility>

#include "inc_gen/TypeDefs.h"
#include "input/OperatorLessThan.h"

class OpDef;

namespace midas
{
namespace input
{
namespace detail
{

// used by MidasOperatorReader (IAN: why they are not part of the class I don't know)
//bool CheckIfTermShouldBeIgnored(const std::vector<std::string>& aTerm, Nb aVal,In aMcMax, In arModeIn, bool aShouldBeIn);
void OperFromFileReadIn(const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>&, OpDef&,const FunctionContainer<Nb>&, In);
void OperFromInputReadIn(const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>&, OpDef&, const FunctionContainer<Nb>&);
void KeoFromFileReadIn(const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>&, OpDef&, const FunctionContainer<Nb>&);

}/*namespace detail*/

} /*namespace input*/
} /*namespace midas*/

#endif /* MIDASOPERATORINPUT_H_INLCUDED */
