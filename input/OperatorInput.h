#ifndef OPERINPUT_H_INCLUDED
#define OPERINPUT_H_INCLUDED

#include <string>
#include <iostream>
#include <vector>
#include <map>

#include "inc_gen/TypeDefs.h"

class OpDef;

namespace midas
{
namespace input
{

//!
void AddCoriolisTerms
   (  std::vector<Nb>& arInertia
   ,  std::string oper_file_name
   ,  OpDef& arOpDef
   );

//!
void AddOneCorTerm
   (  const In& arI
   ,  const In& arJ
   ,  const In& arK
   ,  const In& arL
   ,  Nb aZ
   ,  OpDef& arOpDef
   );

//!
void AddFullCoriolisOp
   (  OpDef& arOpDef
   ,  std::string aCorFileName
   ,  std::vector<Nb>& arInertia
   ,  bool aInertiaFilesFromIn
   ,  std::vector<std::string>& arInertiaFileNames
   ,  std::map<std::string, std::string> arInertiaFileMap
   ,  bool aRestrictModes
   ,  In aNrestricModes
   ,  bool aRestricModesInWkeo
   ,  In aNrestricModesInWkeo
   ,  bool aScreenZero
   ,  Nb aZeroThr
   );

//!
void SetVibAngMomOp
   (  In aIcomp
   ,  std::string aCorFileName
   ,  OpDef& arOp
   ,  bool aScreenZeroTerms
   ,  Nb aZeroThr
   ,  OpDef& arHamilton
   );

//!
std::string OperatorInput
   (  std::istream& Minp
   );

} /* namespace input */
} /* namespace midas */

#endif /* OPERINPUT_H_INCLUDED */
