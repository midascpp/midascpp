/**
************************************************************************
*
* @file                MidasOperatorWriter.cc
*
* Created:             19-05-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for writing mop files to disk
*
* Last modified: Mon May 19, 2014  03:07PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include <vector>
#include <utility>
#include <map>
#include <string>
#include <fstream>
#include <algorithm>

#include "libmda/numeric/float_eq.h"

#include "input/MidasOperatorWriter.h"
#include "input/MidasOperatorReader.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "mpi/Impi.h"

#include "libmda/util/stacktrace.h"

/**
 *
**/
void MidasOperatorWriter::AddScalings(const MidasVector& aScalings)
{
   if(mScalings.Size() != 0)
   {
      MIDASERROR("Scalings already set elsewhere for file \""+mFileName+"\" check the code");
   }
   if(mFrequencies.Size() != 0)
   {
      MIDASERROR("Frequencies already set for file \""+mFileName+"\". Now trying to set scalings as well, this must be an error, check the code");
   }
   mScalings.SetNewSize(aScalings.Size());
   mScalings = aScalings;
}

/**
 *
**/
void MidasOperatorWriter::AddFrequencies(const MidasVector& aFrequencies)
{
   if(mFrequencies.Size() != 0)
   {
      MIDASERROR("Frequencies already set elsewhere for file \""+mFileName+"\" check the code");
   }
   if(mScalings.Size() != 0)
   {
      MIDASERROR("Scalings already set for file \""+mFileName+"\". Now trying to set frequencies as well, this must be an error, check the code");
   }
   mFrequencies.SetNewSize(aFrequencies.Size());
   mFrequencies = aFrequencies;
}

/**
 *
**/
bool MidasOperatorWriter::CheckScalings(const MidasVector& aNewScal) const
{
   if(aNewScal.Size() != mScalings.Size())
   {
      MIDASERROR("The old and new scaling vector does not match in size");
   }
   if((aNewScal.Size() == 0) && (mScalings.Size() == 0))
   {
      return true;
   }
   for(Uin i = 0; i < aNewScal.Size(); ++i)
   {
      if(aNewScal[i] != mScalings[i])
      {
         return false;
      }
   }
   return true;
}

/**
 *
**/
bool MidasOperatorWriter::CheckFrequencies(const MidasVector& aNewFreq) const
{
   if(aNewFreq.Size() != mFrequencies.Size())
   {
      MIDASERROR("The old and new frequency vector does not match in size");
   }
   if((aNewFreq.Size() == 0) && (mFrequencies.Size() == 0))
   {
      return true;
   }
   for(Uin i = 0; i < aNewFreq.Size(); ++i)
   {
      if(aNewFreq[i] != mFrequencies[i])
      {
         return false;
      }
   }
   return true;
}

/**
 *
**/
void MidasOperatorWriter::WriteFile() const
{
   midas::mpi::OFileStream file(mFileName);
   if (!file)
   {
      MIDASERROR("Could not open file \"" + mFileName + "\" for writing");
   }
   file.setf(ios::scientific);
   file.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, file);
   file << "#0MIDASOPERATOR" << std::endl;

   if (!mInfoLabel.empty())
   {
      file << "#1SETINFO" << std::endl;
      file << mInfoLabel << std::endl;
   }
   else
   {
      file << "#1SETINFO"    << std::endl;
      file << "type=unknown" << std::endl;
   }

   if(mReferenceValue != 0.0)
   {
      file << "#1REFERENCEVALUE" << std::endl;
      file << mReferenceValue << std::endl;
   }

   if (mModeNames.size() != 0)
   {
      file << "#1MODENAMES" << std::endl;
      for (auto it = mModeNames.begin(); it != mModeNames.end(); ++it)
      {
         file << *it << " ";
      }
      file << std::endl;
   }

   if (mScalings.Size() != 0)
   {
      file << "#1SCALEFACTORS" << std::endl;
      for (Uin i = 0; i < mScalings.Size(); ++i)
      {
         file << mScalings[i] << " ";
      }
      file << std::endl;
   }

   if (mFrequencies.Size() != 0)
   {
      file << "#1FREQUENCIES" << std::endl;
      for (Uin i = 0; i < mFrequencies.Size(); ++i)
      {
         file << mFrequencies[i] << " ";
      }
      file << std::endl;
   }

   if (!mFunctions.empty())
   {
      file << "#1FUNCTIONS" << std::endl;
      for (auto it = mFunctions.begin(); it != mFunctions.end(); ++it)
      {
         file << it->first << " " << it->second << std::endl;
      }
   }

   if (!mConstants.empty())
   {
      file << "#1CONSTANTS" << std::endl;
      for (auto it = mConstants.begin(); it != mConstants.end(); ++it)
      {
         file << it->first << " " << it->second << std::endl;
      }
   }

   // Always write out operator terms
   file << "#1OPERATORTERMS" << std::endl;
   for (auto it = mOperators.begin(); it != mOperators.end(); ++it)
   {
      // If postive coefficient then add an extra space for aestetics
      file << ( libmda::numeric::float_pos(it->second) ? " " : "" );
      // Print the linear coefficient
      file << it->second;
      // Print the operator and associated mode
      auto it_3 = it->first.first.begin();
      for (auto it_2 = it->first.second.begin(); it_2 != it->first.second.end(); ++it_2, ++it_3)
      {
         file << " " << *it_2 << "(" << *it_3 << ")";
      }
      file << std::endl;
   }

   file << "#0MIDASOPERATOREND" << std::endl;
}

/**
 * This will add an operator term to the member variable of the MidasOperatorWriter class object, which will then eventually be dumped to file upon destruction of the class object
 *
 * @param aCoeff        Linear property surface coefficient
 * @param aFunctions    Operator(s) term(s)
 * @param aModes        Mode(s) associated with the operator(s)
 **/
void MidasOperatorWriter::AddOperatorTerm
   (  const Nb aCoeff
   ,  const std::vector<std::string>& aFunctions
   ,  const std::vector<std::string>& aModes
   )
{
   if (aFunctions.size() != aModes.size())
   {
      Mout << "Functions vector : " << aFunctions << std::endl;
      Mout << "Modes vector : " << aModes << std::endl;

      MIDASERROR("Functions vector and modes vectors sizes does not match, something is wrong");
   }

   auto it_ins = mOperators.insert(std::make_pair(std::make_pair(aModes, aFunctions), aCoeff));

   // If the operator term already exists in the map, i.e. two terms have identical keys, then we add the linear coefficients together as they belong to the same operator
   if (!it_ins.second)
   {
      it_ins.first->second += aCoeff;
   }
}

/**
 *
 **/
void MidasOperatorWriter::AddFunctions
   (  const std::vector<std::pair<std::string, std::string> >& aVectOfFunc
   )
{
   for(auto it = aVectOfFunc.begin(); it != aVectOfFunc.end(); ++it)
   {
      auto it_ins = mFunctions.insert(make_pair(it->first, it->second));
      //If we did not insert there is a naming conflict and we have to stop
      if(!it_ins.second)
      {
         Mout << "Trying to insert : " << std::endl;
         for(auto it_2 = aVectOfFunc.begin(); it_2 != aVectOfFunc.end(); ++it)
            Mout << *it_2 << std::endl;
         Mout << "Into set : " << std::endl;
         for(auto it_2 = mFunctions.begin(); it_2 != mFunctions.end(); ++it_2)
            Mout << *it_2 << std::endl;
         MIDASERROR("Naming conflict when constructing operator function list");
      }
   }
}

/**
 *
**/
void MidasOperatorWriter::AddFunctions(const std::map<std::string, std::string>& aMapOfFunc)
{
   for(auto it = aMapOfFunc.begin(); it != aMapOfFunc.end(); ++it)
   {
      auto it_ins = mFunctions.insert(make_pair(it->first, it->second));
      //If we did not insert there is a naming conflict and we have to stop
      if(!it_ins.second)
      {
         Mout << "Trying to insert : " << std::endl;
         for(auto it_2 = aMapOfFunc.begin(); it_2 != aMapOfFunc.end(); ++it)
            Mout << *it_2 << std::endl;
         Mout << "Into set : " << std::endl;
         for(auto it_2 = mFunctions.begin(); it_2 != mFunctions.end(); ++it_2)
            Mout << *it_2 << std::endl;
         MIDASERROR("Naming conflict when constructing operator function list");
      }
   }
}

/**
 *
**/
void MidasOperatorWriter::AddFunction
   (  const std::string& aName
   ,  const std::string& aFunc
   )
{
   auto it_ins = mFunctions.insert(make_pair(aName, aFunc));
   //If we did not insert there is a naming conflict and we have to stop
   if (!it_ins.second)
   {
      Mout << "Trying to insert : " << std::endl;
      Mout << aName << ", " << aFunc << std::endl;
      Mout << "Into set : " << std::endl;
      for (auto it_2 = mFunctions.begin(); it_2 != mFunctions.end(); ++it_2)
      {
         Mout << *it_2 << std::endl;
      }
      MIDASERROR("Naming conflict when constructing operator function list");
   }
}

/**
 *
**/
void MidasOperatorWriter::AddConstants
   (  const std::vector<std::pair<std::string, Nb> >& aVectOfConst
   )
{
   for (auto it = aVectOfConst.begin(); it != aVectOfConst.end(); ++it)
   {
      auto it_ins = mConstants.insert(make_pair(it->first, it->second));
      // If we did not insert there is a naming conflict and we have to stop
      if (!it_ins.second)
      {
         Mout << "Trying to insert : " << std::endl;
         for(auto it_2 = aVectOfConst.begin(); it_2 != aVectOfConst.end(); ++it)
         {
            Mout << *it_2 << std::endl;
         }
         Mout << "Into set : " << std::endl;
         for(auto it_2 = mConstants.begin(); it_2 != mConstants.end(); ++it_2)
         {
            Mout << *it_2 << std::endl;
         }
         MIDASERROR("Naming conflict when constructing operator function list");
      }
   }
}

/**
 *
**/
void MidasOperatorWriter::AddConstants
   (  const std::map<std::string, Nb>& aMapOfConst
   )
{
   for (auto it = aMapOfConst.begin(); it != aMapOfConst.end(); ++it)
   {
      auto it_ins = mConstants.insert(make_pair(it->first, it->second));
      // If we did not insert there is a naming conflict and we have to stop
      if (!it_ins.second)
      {
         Mout << "Trying to insert : " << std::endl;
         for (auto it_2 = aMapOfConst.begin(); it_2 != aMapOfConst.end(); ++it)
         {
            Mout << *it_2 << std::endl;
         }
         Mout << "Into set : " << std::endl;
         for (auto it_2 = mConstants.begin(); it_2 != mConstants.end(); ++it_2)
         {
            Mout << *it_2 << std::endl;
         }
         MIDASERROR("Naming conflict when constructing operator function list");
      }
   }
}

/**
 *
**/
void MidasOperatorWriter::AddConstant
   (  const std::string& aName
   ,  Nb aConst
   )
{
   auto it_ins = mConstants.insert(make_pair(aName, aConst));
   //If we did not insert there is a naming conflict and we have to stop
   if (!it_ins.second)
   {
      Mout << "Trying to insert : " << std::endl;
      Mout << aName << ", " << aConst << std::endl;
      Mout << "Into set : " << std::endl;
      for ( auto it_2 = mConstants.begin(); it_2 != mConstants.end(); ++it_2)
      {
         Mout << *it_2 << std::endl;
      }
      MIDASERROR("Naming conflict when constructing operator function list");
   }
}

/**
 *
**/
void MidasOperatorWriter::FromReadPotential
   (  const MidasOperatorReader& aOper
   ,  const Nb& aCoeff
   )
{
   AddReferenceValue(aOper.GetRefefenceValue());
   AddConstants(aOper.GetConstants());
   AddFunctions(aOper.GetFunctions());
   AddModeNames(aOper.GetModeNames());
   AddPropertyLabel(aOper.GetInfoLabel());
   MidasVector temp_scaling_vect;
   aOper.GetScalingVector(temp_scaling_vect);
   AddScalings(temp_scaling_vect);
   std::map<std::pair<std::vector<std::string>, std::vector<std::string> >,Nb, operator_less_than> copy_of_terms = aOper.GetOperators();

   for (auto it = copy_of_terms.begin(); it != copy_of_terms.end(); ++it)
   {
      AddOperatorTerm(it->second*aCoeff, it->first.second, it->first.first);
   }
}

/**
 *
**/
void MidasOperatorWriter::AddPotential
   (  const MidasOperatorReader& aOper
   ,  const Nb& aCoeff
   )
{
   AddConstants(aOper.GetConstants());
   AddFunctions(aOper.GetFunctions());

   // we have to make sure that names and ordering of modes and scaling factors are ok and in the right order, so we use a map..

   std::vector<string> mode_names=aOper.GetModeNames();
   MidasVector temp_scaling_vect;
   aOper.GetScalingVector(temp_scaling_vect);

   std::vector<Nb> new_scalings;
   new_scalings.reserve(temp_scaling_vect.Size());

   MidasAssert((mode_names.size()==temp_scaling_vect.Size() || temp_scaling_vect.Size() == I_0)," the given mode namevector does not fit to the scaling vector");

   for (In i = I_0; i < mode_names.size(); ++i)
   {
      In j = -I_1;
      In n = I_0;
      while (j == -I_1 && n < mModeNames.size())
      {
         if (mode_names.at(i) == mModeNames.at(n))
         {
            j = n;
         }

         ++n;
      }

      if (j == -I_1)
      {
         mModeNames.push_back(mode_names.at(i));
         if (temp_scaling_vect.Size() > I_0)
         {
           new_scalings.push_back(temp_scaling_vect.at(i));
         }
      }
      else if (temp_scaling_vect.Size() > I_0)
      {
         MidasAssert(mScalings[j] - temp_scaling_vect.at(i) < C_I_10_16, "Scaling factors do not fit");
      }
   }

   if (temp_scaling_vect.Size() > I_0)
   {
      MidasVector new_midas_scaling_vec(new_scalings);
      mScalings.Append(new_midas_scaling_vec);
   }
   // and the come to the individual terms:
   std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than > copy_of_terms = aOper.GetOperators();
   for (auto it = copy_of_terms.begin(); it != copy_of_terms.end(); ++it)
   {
      AddOperatorTerm(it->second*aCoeff, it->first.second, it->first.first);
   }
}

/**
 *
**/
void MidasOperatorWriter::FromReadPotentials
   (  const std::vector<MidasOperatorReader>& aOperSet
   )
{
   if (aOperSet.empty())
   {
      MIDASERROR("There should be some operators given to the merging function");
   }

   In MultiLevelCounter = I_1; // Start from multilevel surface 1 and go through all multilevel surfaces
   AddReferenceValue(aOperSet.front().GetRefefenceValue()); // Use the reference value from the first operator in the list
   MidasVector temp_scalings;
   aOperSet.front().GetScalingVector(temp_scalings);
   AddScalings(temp_scalings);
   AddModeNames(aOperSet.front().GetModeNames());
   AddPropertyLabel(aOperSet.front().GetInfoLabel());

   std::vector<std::pair<std::string, Nb> > temp_const; // temporarily hold constants from given operator
   std::vector<std::pair<std::string, std::string> > temp_func; // temporarily hold functions from given operator

   std::map<std::string, Nb> coll_const_map;         // collect constants to be written
   std::map<std::string, std::string> coll_func_map; // collect functions to be written

   std::vector<std::basic_string<char> >::size_type max_mc_lvl = 0;

   for (auto it = aOperSet.begin(); it != aOperSet.end(); ++it)
   {
      // get and check scalings
      it->GetScalingVector(temp_scalings);
      if(!CheckScalings(temp_scalings))
      {
         MIDASERROR("Scaling vectors between \""+aOperSet.front().Name()+"\" and \""+it->Name()+"\" does not match");
      }

      std::vector<std::basic_string<char> >::size_type temp_max_mc_lvl = max_mc_lvl;//This is to store the actual max_mc_level for the current operator

      // Get constants
      temp_const = it->GetConstants();
      for (auto it_const = temp_const.begin(); it_const != temp_const.end(); ++it_const)
      {
         it_const->first = "l" + std::to_string(MultiLevelCounter) + "_" + it_const->first;

         auto it_ins = coll_const_map.insert(*it_const);

         if (!it_ins.second)
         {
            if (it_ins.first->second != it_const->second)
            {
               MIDASERROR("Constants from files \""+it->Name()+"\" and another unknown file contain the same variable label \""+it_const->first+"\" with different values");
            }
         }
      }

      // Get functions
      temp_func = it->GetFunctions();
      for (auto it_func = temp_func.begin(); it_func != temp_func.end(); ++it_func)
      {
         if (MultiLevelCounter > I_1)
         {
            MidasWarning("The functions framework is not adapted for running multilevel calculations with the general fit-basis functions!");
         }
         auto it_ins = coll_func_map.insert(*it_func);
         if (!it_ins.second)
         {
            if (it_ins.first->second != it_func->second)
            {
               MIDASERROR("Functions from files \""+it->Name()+"\" and another unknown file contain the same function label \""+it_func->first+"\" with different function definitions");
            }
         }
      }

      // Get operator terms
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >,Nb,operator_less_than> copy_of_terms = it->GetOperators();
      for (auto it_terms = copy_of_terms.begin(); it_terms != copy_of_terms.end(); ++it_terms)
      {
         std::vector<std::string> output_terms = it_terms->first.second;
         std::vector<std::pair<std::string, Nb> > const_non_appended = it->GetConstants();
         for (auto it_const = const_non_appended.begin(); it_const != const_non_appended.end(); ++it_const)
         {
            for (In i = I_0; i < output_terms.size(); ++i)
            {
               size_t pos = 0;
               pos = output_terms[i].find(it_const->first, pos);
               if (pos != std::string::npos)
               {
                  output_terms[i].insert(pos, "l" + std::to_string(MultiLevelCounter) + "_");
               }
            }
         }

         temp_max_mc_lvl = std::max(temp_max_mc_lvl, it_terms->first.first.size());
         if (it_terms->first.first.size() > max_mc_lvl)
         {
            AddOperatorTerm(it_terms->second, output_terms, it_terms->first.first);
         }
      }
      max_mc_lvl = temp_max_mc_lvl;

      ++MultiLevelCounter; // Go to next Multilevel surface
   }
   AddConstants(coll_const_map);
   AddFunctions(coll_func_map);
}

/**
 *
**/
void MidasOperatorWriter::FromReadAndScaledPotentials
   (  const std::vector<MidasOperatorReader>& aOperSet
   ,  const std::vector<Nb>& aCoeffSet
   )
{
   if(aOperSet.empty())
   {
      MIDASERROR("There should be some operators given to the merging function");
   }
   if(aOperSet.size() != aCoeffSet.size())
   {
      MIDASERROR("Mismatch in sizes of operatorset and coefficient set, check the code");
   }

   AddModeNames(aOperSet.front().GetModeNames());
   MidasVector temp_scalings;
   aOperSet.front().GetScalingVector(temp_scalings);
   AddScalings(temp_scalings);
   AddModeNames(aOperSet.front().GetModeNames());

   std::vector<std::basic_string<char> >::size_type max_mc_lvl = 0;
   std::map<std::pair<std::vector<std::string>, std::vector<std::string> >,Nb, operator_less_than> copy_of_terms = aOperSet.begin()->GetOperators();
   for(auto it = copy_of_terms.begin(); it != copy_of_terms.end(); ++it)
   {
      max_mc_lvl = max(max_mc_lvl, it->first.first.size());
   }


   std::vector<std::pair<std::string, Nb> > temp_const;
   std::vector<std::pair<std::string, std::string> > temp_func; //format name, function

   std::map<std::string, Nb> coll_const_map;
   std::map<std::string, std::string> coll_func_map; //format name, function

   auto it_coeff = aCoeffSet.begin();
   for(auto it = aOperSet.begin(); it != aOperSet.end(); ++it, ++it_coeff)
   {
      it->GetScalingVector(temp_scalings);
      if(!CheckScalings(temp_scalings))
      {
         MIDASERROR("Scaling vectors between \""+aOperSet.front().Name()+"\" and \""+it->Name()+"\" does not match");
      }
      std::vector<std::basic_string<char> >::size_type temp_max_mc_lvl = max_mc_lvl;//This is to store the actual max_mc_level for the current operator
      temp_const = it->GetConstants();
      for(auto it_const = temp_const.begin(); it_const != temp_const.end(); ++it_const)
      {
         auto it_ins = coll_const_map.insert(*it_const);
         if(!it_ins.second)
         {
            if(it_ins.first->second != it_const->second)
            {
               MIDASERROR("Constants from files \""+it->Name()+"\" and another unknown file contain the same variable label \""+it_const->first+"\" with different values");
            }
         }
      }
      temp_func = it->GetFunctions();
      for(auto it_func = temp_func.begin(); it_func != temp_func.end(); ++it_func)
      {
         auto it_ins = coll_func_map.insert(*it_func);
         if(!it_ins.second)
         {
            if(it_ins.first->second != it_func->second)
            {
               MIDASERROR("Functions from files \""+it->Name()+"\" and another unknown file contain the same function label \""+it_func->first+"\" with different function definitions");
            }
         }
      }
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >,Nb,operator_less_than> copy_of_terms = it->GetOperators();
      for(auto it_terms = copy_of_terms.begin(); it_terms != copy_of_terms.end(); ++it_terms)
      {
         if(it_terms->first.first.size() <= max_mc_lvl)
         {
            AddOperatorTerm(it_terms->second*(*it_coeff), it_terms->first.second, it_terms->first.first);
         }
      }
      max_mc_lvl = temp_max_mc_lvl;
   }
   AddConstants(coll_const_map);
   AddFunctions(coll_func_map);
}

/**
 *
**/
void SortTermForOperatorWriter
   (  std::vector<string>& arModes
   ,  std::vector<string>& arFunctions
   )
   {

      MidasAssert(arModes.size()==arFunctions.size()," vectors not same size in SortTermForOperatorWriter");
      std::map<std::string,std::string,modename_less_than> func_map;
      for (In i=I_0; i < arModes.size() ; ++i)
      {
         func_map.emplace(arModes.at(i),arFunctions.at(i));
      }

      arModes.clear();
      arModes.reserve(func_map.size());
      arFunctions.clear();
      arFunctions.reserve(func_map.size());
      for (auto it=func_map.begin(); it!= func_map.end() ; ++it)
      {
         arModes.push_back(it->first);
         arFunctions.push_back(it->second);
      }
   }
