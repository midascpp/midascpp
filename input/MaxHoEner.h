/**
************************************************************************
* 
* @file                MaxHoEner.h 
*
* Created:             8-11-2006
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for finding VSCF occ vectors based on HO energy 
* 
* Last modified: Wed Nov 08, 2006  10:58PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MAXHOENER_H_INCLUDED
#define MAXHOENER_H_INCLUDED

// Standard Headers
#include <string>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/RspCalcDef.h"

/**
* Construct a definition of a  calculation
* */
class MaxHoEner
{
   private:
      //! Occupation vector (???)
      std::vector<Nb> mOmeVec; 
      //! Occ Max vector input
      std::vector<In> mMaxVec; 
      //! Maximum HO energy 
      Nb mEnerMax;

   public:
      //! Constructor
      MaxHoEner(std::vector<In> aInVec, const std::string& aOper, Nb aE); 

      //! Get all occ vecs with low enough HO energy 
      void GetAllOccVecs(std::vector<InVector>&); 
      
      //! Recursice add of ivecs
      void RecursAdd(In aMode, std::vector<InVector>& arIvecVec, Nb aEtot, std::vector<In> aCurrentOccVec); 
};

#endif /* MAXHOENER_H_INCLUDED */
