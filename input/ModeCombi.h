/**
************************************************************************
* 
* @file                ModeCombi.h
*
* Created:             02-01-2003
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for containing mode indices m_0 < m_1..<m_k
* 
* Last modified:       27-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MODECOMBI_H_INCLUDED
#define MODECOMBI_H_INCLUDED

#include<iostream>
#include<set>
#include<unordered_set>
#include<vector>

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"

/**
 * @brief Class to implement a mode combination, i.e. a vector of unique, increasing indices.
 **/
class ModeCombi
{
   private:
      //! Contains the mode numbers.
      std::vector<In> mModeNrs;       
      //! Some adress that is convenient in later contexts.
      In mAddress = -I_1;          
      //! An integer telling where it is in a ref. vector. 
      In mModeCombiNrInRefVec = -I_1; 
      //!
      bool mToBeExtrap = false; 
      //! Is the mode-combi to be neglected due to some prescreening criteria
      bool mToBeScreened = false;     

   public:
      ///> default ctor
      ModeCombi();
      
      ///> Constructor - reserve space only.
      ModeCombi(const In& aN);

      ///> Constructor from set of int integers.
      ModeCombi(const std::set<In>& arV1, const In aAdd);

      ///> Constructor from vector of integers.
      ModeCombi(const std::vector<In>& arV1, const In aAdd);

      ///> Constructor from vector of LocalModeNrs (integers).
      ModeCombi(const std::vector<LocalModeNr>&, const In aAdd);

      ///> default copy ctor
      ModeCombi(const ModeCombi&) = default;

      ///> default move ctor
      ModeCombi(ModeCombi&&) = default;

      ///> default copy assign
      ModeCombi& operator=(const ModeCombi&) = default;
      
      ///> default move assign
      ModeCombi& operator=(ModeCombi&&) = default;

      In Size() const {return mModeNrs.size();}            ///< Number of modes.
      const std::vector<In>& MCVec() const {return mModeNrs;}   ///< returns the mode vector.
      bool Empty() const {return (mModeNrs.size()==I_0);}  ///< Is it empty?
      bool IncludeMode(const In arM) const;                ///< Is mode arM in MC?
      const In& Mode(const In arM) const {return mModeNrs[arM];}  ///< Provide a certain mode.
      void InsertMode(const In arMode);                    ///< Insert a mode.
      void RemoveMode(const In arMode);                    ///< Remove a mode.

      void ReInit(const std::vector<In>& aModes, In aAddr=-I_1, In aRef=-I_1)
      {mModeNrs=aModes; mAddress=aAddr; mModeCombiNrInRefVec=aRef;}
      ///< Reinitialize using vector aModes. Vector is assumed to be sorted.
      
      void AssignAddress(const In& arAddress)              ///< Assign address.
      {mAddress = arAddress;}
      
      void AssignModeCombiRefNr(const In& arAddress)       ///< Assign reference number.
      {mModeCombiNrInRefVec = arAddress;}
      
      In Address() const {return mAddress;}                     ///< Return address.
      In ModeCombiRefNr() const {return mModeCombiNrInRefVec;}  ///< Return ref. no.

      void SetToBeExtrap(bool aB=true){mToBeExtrap=aB;}    ///< Points to be extrapolated.
      bool IsToBeExtrap() const {return mToBeExtrap;}      ///< Return whether is to be
                                                           ///< extrapolated  or not.

      void SetToBeScreened(bool aB=false)       {mToBeScreened=aB;}     ///< Set the mode combi to be screened or not
      bool IsToBeScreened()               const {return mToBeScreened;} /// Return whether is to be screened

      In IdxNrForMode(const In arMode) const;

      void Intersection(const ModeCombi& arOne, const ModeCombi& arOther);
      void Union(const ModeCombi& arOne, const ModeCombi& arOther);
      void InFirstOnly(const ModeCombi& arOne, const ModeCombi& arOther);
      void InOneOnly(const ModeCombi& arOne, const ModeCombi& arOther);

      friend bool ZeroIntersect(const ModeCombi& arOne, const ModeCombi& arOther);
      friend std::ostream& operator<<(std::ostream&, const ModeCombi& ar1);
      
      friend bool operator<(const ModeCombi& ar1,const ModeCombi& ar2);
      ///< Address & ref. no. ignored.
      
      friend bool operator==(const ModeCombi& ar1,const ModeCombi& ar2)
      {
         return ar1.mModeNrs==ar2.mModeNrs;
      }
      ///< Address & ref. no. ignored.
      
      friend bool operator!=(const ModeCombi& ar1,const ModeCombi& ar2)
      {
         return ar1.mModeNrs!=ar2.mModeNrs;
      }
      ///< Address & ref. no. ignored.
      
      //friend bool ModeCombiLess(const  ModeCombi& ar1,const ModeCombi& ar2);   ///< ModeCombiLess 
      In SizeOut() const; ///< Output total estimated size
      bool Contains(const ModeCombi& arModeCombi) const;
};

//@{
//! Num. parameters. for given ModeCombi and num. modals for each mode.
Uin NumParams(const ModeCombi& arMc, const std::vector<In>& arNmodals, const In aAddend = -1);
Uin NumParams(const ModeCombi& arMc, const std::vector<Uin>& arNmodals, const In aAddend = -1);
//@}

namespace std
{
   /************************************************************************//**
    * @brief
    *    Custom hash for std::vector<In> and ModeCombi used to create
    *    std::unordered_set<ModeCombi> and such.
    *
    * @note
    *    Algorithm is taken from boost. -Niels.
    ***************************************************************************/
   //@{
   template<>
   struct hash<std::vector<In>>
   {
      std::size_t operator()(const std::vector<In>& arVec) const
      {
         std::size_t seed = arVec.size();
         for(const auto& m : arVec)
         {
            seed ^= m + 0x9e3779b9 + (seed << 6) + (seed >> 2);
         }
         return seed;
      }
   };
   template<>
   struct hash<ModeCombi>
   {
      std::size_t operator()(const ModeCombi& arMc) const
      {
         return hash<std::vector<In>>()(arMc.MCVec());
      }
   };
   //@}

}  /* namespace std */

#endif //MODECOMBI_H_INCLUDED
