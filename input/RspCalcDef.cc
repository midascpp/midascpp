/**
************************************************************************
* 
* @file                RspCalcDef.cc
*
* Created:             04-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a calculation
* 
* Last modified: Sun May 16, 2010  03:10PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
#include <vector>
#include <map>
#include <algorithm>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/RspCalcDef.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "vcc/LanczosRspFunc.h"
#include "input/CombinedRspFunc.h"
#include "input/TensorDecompInput.h"
#include "mpi/Impi.h"

// using declarations
using std::string;
using std::vector;
using std::map;
using std::find;

/**
* Construct a default definition of a  calculation
* */
RspCalcDef::RspCalcDef
   (
   )
   : mDoTensorDecomp(false)
   , mDoTensorRspBenchmarkDecomp(false)
   , mLambdaThreshold(1e-13)
{
   mDoRsp                                  = false;
   mDoRspEig                               = false;
   mDoRspFuncs                             = false;
   mDoNoPaired                             = false;
   mRspNeig                                = I_0;
   mRspLeftEig                             = false;

   mRspDiagMeth                            = gNumLinAlg;
   //mRspVecStorageLevel                     = 1;  // 0 means all in memory, 1 means most important in memory.

   mRspRestart                             = false;
   mRspUseAvailable                        = false;
   mRspMapPrevVecs                         = false;
   mRspIoLevel                             = I_0;

   mRspItEqNiterMax                        = I_20;
   mRspItEqResidThr                        = C_I_10_6; 
   mRspItEqEnerThr                         = C_NB_MAX; // To max number - turned off as default. 
   mRspRedDimMax                           = I_100;
   mRspNbreakDim                           = I_10_5;
   mRspDiis                                = true;
   mRspMaxDiis                             = I_8;
   mRspPreDiag                             = false;
   mRspTimeIt                              = false;

   mRspFuncs.clear();
   mRspOpers.clear();

   mRspTargetSpace                         = false;         
   mRspDoAllFundamentals                   = false;        
   mRspDoAllFirstOvertones                 = false;       
   mRspDoFundamentals                      = false;      
   mRspDoFirstOvertones                    = false;     
   mRspFundamentals.clear();        
   mRspFirstOvertones.clear();     

   mScreenZeroRsp                          = C_0;            // Screening = 0, i.e. off 
   mScreenZeroRspC                         = C_0;            // Screening = 0, i.e. off 

   mSosFreqSteps                           = 0;
   mSosFreqBegin                           = C_0;
   mSosFreqEnd                             = C_1;

   mQrfFrq1                                = C_0;
   mQrfFrq2                                = C_0;
   mCrfFrq.clear();
   mQrfFrq1                                = C_0;
   mQrfFrq2                                = C_0;

   mNumVccJacobian                         = C_0;     // Num. VCC Jacobian disabled.
   mNumVccF                                = C_0;     // Num. VCC F matrix disabled.
   mDamp                                   = C_0;
   mTotalRsp.clear();
   mSpecialQrfSosFrqs                      = false;
   mSpecialCrfSosFrqs                      = false;

   mRspTrueHDiag                           = false;
   mRspTrueADiag                           = false;
   mRspImprovedPrecond                     = false;
   mRspPrecondExciLevel                    = I_0;
   mRspNresvecs                            = I_0;
   mRspLevel2Solver                        = gNumLinAlg;
   mRspTargetingMethod                     = "BESTHITS";
   mRspOverlapMin                          = C_0;

   mRspEnergyDiffMax                       = C_5/C_AUTKAYS;
   mRspResidThrForOthers                   = C_10_3;
   mRspEnerThrForOthers                    = C_10_3;
   mRspEigValSeqSet                        = false;
   mRspEigValSeq                           = I_0;
   mAsymFreqs.clear();
   mAlsoLinearAsym                         = false;
   mRspHarmonicRR                          = false;
   mRspEnerShift                           = C_0;

   mNApproxRspFunc                         = I_0;
}

RspCalcDef::~RspCalcDef()
{
   for (In i=I_0; i<mLanczosRspFuncs.size(); ++i)
      delete mLanczosRspFuncs[i];
   for (In i=I_0; i<mLanczosIRspects.size(); ++i)
      delete mLanczosIRspects[i];
   for (In i=I_0; i<mBandLanczosRspFuncs.size(); ++i)
      delete mBandLanczosRspFuncs[i];
   for (In i=I_0; i<mBandLanczosIRspects.size(); ++i)
      delete mBandLanczosIRspects[i];
   for (In i=I_0; i<mBandLanczosRamanSpects.size(); ++i)
      delete mBandLanczosRamanSpects[i];
}

/**
* Output a particular response function 
* */
void RspCalcDef::RspFuncOut(In aI)
{
   Mout << " " << mRspFuncs[aI] <<endl;
}
/**
* Output a particular response vector info
* */
void RspCalcDef::RspVecInfOut(In aI)
{
   Mout << " " << mRspVecInf[aI]<<endl;
}
/**
* Find a particular Response vector  
* */
In RspCalcDef::FindRspEq(const RspVecInf& arInf)
{
   std::vector<RspVecInf>::const_iterator i_find = std::find(mRspVecInf.begin(), mRspVecInf.end(), arInf);
   In j=0;
   for (vector<RspVecInf>::const_iterator i= mRspVecInf.begin();
         i!=mRspVecInf.end();i++)
   {
      if (i==i_find) return j;
      else j++;
   }
   return -I_1;      // Seidler warning: Should probably never end here.
}
/**
 * * Find the response operator for a given nr. 
 * * */
string RspCalcDef::GetRspOp(In arN)
{
   for (map<string,In>::const_iterator it = mRspOpers.begin(); it != mRspOpers.end(); it++)
   {
      if (it->second == arN) return it->first;
   }
   return "FAILURE_NR_DOES_NOT_EXIST_ON_RESPONSE_OPERATOR_LIST";
}

/**
 *
 **/
LanczosRspFunc* RspCalcDef::GetLanczosRspFunc(const string& aName) const
{
   for (In i=I_0; i<mLanczosRspFuncs.size(); ++i)
      if (mLanczosRspFuncs[i]->GetName() == aName)
         return mLanczosRspFuncs[i];
   return NULL;
}

/**
 *
 **/
void RspCalcDef::SetRspDecompEig()
{
   SetRspEigType(RSPEIGTYPE::DECOMP);
}


/**
 * Adds TensorDecompInfo to mVccRspDecompInfoSet and sets the EIGTYPE
 * @param Minp
 * @param s
 **/
bool RspCalcDef::ReadVccRspDecompInfoSet(std::istream& Minp, std::string& s)
{
   this->SetRspEigType(RSPEIGTYPE::TENSOR);
   return NewTensorDecompInput(Minp, s, 5, this->mVccRspDecompInfo);
}

/**
 * Adds TensorDecompInfo to mVccRspTrfDecompInfoSet and sets the EIGTYPE
 * @param Minp
 * @param s
 **/
bool RspCalcDef::ReadVccRspTrfDecompInfoSet(std::istream& Minp, std::string& s)
{
   this->SetRspEigType(RSPEIGTYPE::TENSOR);
   return NewTensorDecompInput(Minp, s, 5, this->mVccRspTrfDecompInfo);
}


/**
 *
 **/
std::set<std::string> RspCalcDef::FindUniqueRspTypes(const std::vector<RspFunc>& aRspFuncs) const
{
   std::set<std::string> unique_types;
   for(auto it = aRspFuncs.begin(); it != aRspFuncs.end(); ++it)
   {
      unique_types.insert(it->TypeString());
   }
   return unique_types;
}

/**
 *
 **/
void RspCalcDef::PrintRspToFile() const
{
   analysis::rsp().MakeAnalysisDir();
   midas::mpi::OFileStream rsp_file(analysis::rsp().main_path); // NAME HARDCODED
   Mout << " printing to file: " << analysis::rsp().main_path << std::endl; // HERE !!!!
   auto unique_types = FindUniqueRspTypes(mRspFuncs);

   for(auto it = unique_types.begin(); it != unique_types.end(); ++it)
   {
      CombinedRspFunc cmb_rsp(*it);
      for(int i=0; i<mRspFuncs.size(); ++i)
      {
         cmb_rsp.AddRspFunc(mRspFuncs[i]);
      }
      rsp_file << cmb_rsp << std::endl;
   }
   rsp_file.close();
}

/**
 *
 **/
bool RspCalcDef::ReadItEqRestartVector(std::istream& Minp, std::string& s)
{
   while(midas::input::GetLine(Minp,s) && midas::input::NotKeyword(s))
   {
      mItEqRestartVector.emplace_back(s);
   }
   return true;
}

/**
 *
 **/
bool RspCalcDef::ReadItEqSubspacePreconVector(std::istream& Minp, std::string& s)
{
   while(midas::input::GetLine(Minp,s) && midas::input::NotKeyword(s))
   {
      mItEqSubspacePreconVector.emplace_back(s);
   }
   SetRspComplexSolverType(COMPLEXSOLVER::SUBSPACE);
   return true;
}

/**
 *
 **/
bool RspCalcDef::ReadLambdaThreshold(std::istream& Minp, std::string& s)
{
   mLambdaThreshold = midas::input::GetNb(Minp, s);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspItEqSolThr(std::istream& Minp, std::string& s)
{
   mRspItEqSolThr = midas::input::GetNb(Minp, s);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspPreconItEqResidThr(std::istream& Minp, std::string& s)
{
   mRspPreconItEqResidThr = midas::input::GetNb(Minp, s);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspPreconItEqSolThr(std::istream& Minp, std::string& s)
{
   mRspPreconItEqSolThr = midas::input::GetNb(Minp, s);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspPreconItEqMaxIter(std::istream& Minp, std::string& s)
{
   mRspPreconItEqMaxIter = midas::input::GetIn(Minp, s);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspItEqCheckLinearDependenceOfSpace(std::istream& Minp, std::string& s)
{
   mRspItEqCheckLinearDependenceOfSpace = true;
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspComplexImprovedPrecond(std::istream& Minp, std::string& s)
{
   mRspComplexImprovedPrecond = true;
   mRspComplexImprovedPrecondExciLevel = midas::input::GetIn(Minp, s);
   SetRspComplexSolverType(COMPLEXSOLVER::IMPROVED);
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadVccTensorType(std::istream& Minp, std::string& s)
{
   midas::input::GetLine(Minp,s);
   if(s == "SIMPLE")
   {
      mVccTensorType = VccTensorType::SIMPLE;
   }
   else if(s == "CANONICAL")
   {
      mVccTensorType = VccTensorType::CANONICAL;
   }
   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspUseNewContractions(std::istream& Minp, std::string& s)
{
   mRspUseNewContractions = true;

   return false;
}

/**
 *
 **/
bool RspCalcDef::ReadRspRelConv(std::istream& Minp, std::string& s)
{
   mRspRelConv = true;
   return false;
}

