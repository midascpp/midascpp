/**
************************************************************************
* 
* @file                FlexCouplings.h
*
* Created:             21-03-2014
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Class for Flexible Couplings
* 
* Last modified:       08-12-2014
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FLEXCOUPLINGS_H_INCLUDED
#define FLEXCOUPLINGS_H_INCLUDED

#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <map>

#include "inc_gen/TypeDefs.h"
#include "input/FlexCoupCalcDef.h"
#include "pes/molecule/MoleculeInfo.h"

using std::set;
using std::vector;
using std::string;

// forward declarations
class OpDef;

class McMeasure 
{
   private:
      std::map<vector<In>,Nb>   mMeasureMap;     ///< Contains the mode numbers.
      In                   mMeasureOrder;
      enum                 MeasureTypes{EDIFF,LOCAL,SCREENFROMFILE,ADGAPRESCREEN,VCCPERT1,VCCPERT2,VCCPERT2F, VCCPERT2H};
      MeasureTypes         mType;
      std::map<string,MeasureTypes> mTypeMap
             {{"EDIFF",EDIFF},{"LOCAL",LOCAL},
             {"SCREENFROMFILE",SCREENFROMFILE},{"ADGAPRESCREEN",ADGAPRESCREEN},
             {"VCCPERT1",VCCPERT1}, {"VCCPERT2",VCCPERT2}, 
             {"VCCPERT2H",VCCPERT2H},{"VCCPERT2F",VCCPERT2F}};
      string               mMeasureFile;
      vector<Nb>           mThreshs;
      enum                 ScreenTypes{SCREEN,PRESCREEN,UNKNOWN};
      ScreenTypes          mScreenType;
      std::map<ScreenTypes,string> mScreenMap
             {{SCREEN,"SCREEN"},{PRESCREEN,"PRESCREEN"},
             {UNKNOWN,"UNKNOWN"}};

      
   public:
      McMeasure(); 
      McMeasure(const McMeasure& ar2): mMeasureMap(ar2.mMeasureMap), mMeasureOrder(ar2.mMeasureOrder),
        mType(ar2.mType), mMeasureFile(ar2.mMeasureFile), mThreshs(ar2.mThreshs), mScreenType(ar2.mScreenType) {};
      McMeasure(const std::map<vector<In>,Nb>& arMeasureMap, const In& arOrder, const string& arName)
         {mMeasureMap=arMeasureMap; mMeasureOrder=arOrder; mMeasureFile= arName; }
      McMeasure(const string& arType,const FlexCoupCalcDef arCalcDef);  
      McMeasure(const string& arFilename);
      McMeasure(const string& arIntFileName,const string& arNumDerFile) 
         {CalcEstimatesFromFile(arIntFileName,arNumDerFile);} 
      McMeasure(const vector<string>& arFileNames, const In aLevel) {VccPert1(arFileNames,aLevel);}
      McMeasure(const In& arMaxEx, const string& arFileName) {RecMinEps(arMaxEx,arFileName);}
      ~McMeasure(){;}

      void SetmMeasureMap(const std::map<vector<In>,Nb>& arMeasureMap) 
             {mMeasureMap=arMeasureMap;}
      void SetmMeasureOrder(const In& arMeasureOrder)
             {mMeasureOrder=arMeasureOrder;}
      void SetmThreshs(const vector<Nb>& arThreshs) {mThreshs=arThreshs;}

      std::map<vector<In>,Nb>  GetmMeasureMap() {return mMeasureMap;}
      In GetmMeasureOrder() const {return mMeasureOrder;}
      void PrintMeasuresToFile();
      Nb GetValueForMc(const vector<In>& arModeCombi) const {try{return mMeasureMap.at(arModeCombi);}catch(const std::out_of_range&){return 0.;}}
      vector<Nb> GetThresh() const {return mThreshs;}
      string GetScreenType() const {try{return mScreenMap.at(mScreenType);}catch(const std::out_of_range&){return std::string();}} 

      std::map<vector<In>,Nb> MeasureMapEdiff(const midas::molecule::MoleculeInfo& arMolInfo);
      std::map<vector<In>,Nb> MeasureMapLocal(const midas::molecule::MoleculeInfo& arMolInfo);
      void MeasureFromFile(const string& arFileName);
      void VccPert1(const vector<string>& arFileNames,const  In aLevel); 
      void RecMinEps(const In& arMaxEx, const string& arEpsFileName);
      void VccPert2(const vector<string>& arFileNames,const In aLevel);
      void VccPert2H(const vector<string>& arFileNames,const  In aLevel);
      void VccPert2F(const vector<string>& arFileNames,const  In aLevel);
      string GetmMeasureFile() {return mMeasureFile;}


      Nb MinValue(const vector<In> arModeCombi) const;
      Nb MinRecurse(const vector<In>& arCurrentMc, const vector<In>& arModeCombi, const Nb& arCurrentMax, const In& arStart, const In& arCurrentLevel) const;
      In MaxLevelPreScreen(const vector<In>& arModeCombi,const In& arMaxLevel) const;

      void CalcEstimatesFromFile(const string& arIntFileName,const string& arNumDerFile);
      Nb** ReadInts(const string& arIntFileName);
      void GenScreenEstimates(Nb** const appMaxInt, OpDef& apOpDef);
      void SetScreeningType(const In& arScreenType) {mScreenType=ScreenTypes(arScreenType);}
      In Size() {return mMeasureMap.size();}
      void AddValues(const McMeasure& arMeasure2);
      void SetOneModesToInf();


      friend Nb VccPert2ScalingFactor(const McMeasure& arPert1, const McMeasure& arPert2F);
      friend McMeasure operator*(const McMeasure& ar1, const Nb& arFac);
      friend McMeasure operator+(const McMeasure& ar1, const McMeasure& ar2);
};

class FlexCouplings
{
   private:
      vector<McMeasure>      mFlexCouplings;
      bool                   mCheckAll;
   public:
      FlexCouplings(): mCheckAll(false) {};
      FlexCouplings(const McMeasure& arMcMeasure)
         {mCheckAll=false; AddMeasure(arMcMeasure);}      
      FlexCouplings(const FlexCoupCalcDef& arCalcDef) 
         {mCheckAll=false; GetMeasures(arCalcDef);}
      ~FlexCouplings(){;}
      void GetMeasures(const FlexCoupCalcDef& arCalcDef);
      void AddMeasure(const McMeasure& arMcMeasure) {mFlexCouplings.push_back(arMcMeasure);}
      In MaxLevel(const vector<In>& arModeCombi, const In& arMaxLevel) const;
      bool CheckAll() {return mCheckAll;}
   
   friend FlexCoupCalcDef;
}; 

#endif /* FLEXCOUPLINGS_H_INCLUDED */
