/**
************************************************************************
* 
* @file                RotCoordCalcDef.h
*
* 
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for rotation of coordinates
*                      based on OptVcfCalcDef.h by Bo Thomsen 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ROTCOORDCALCDEF_H
#define ROTCOORDCALCDEF_H

// std headers
#include <string>
#include <vector>
#include <map>

// midas headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Error.h"
#include "libmda/numeric/float_eq.h"

namespace midas::input
{

//! Type of VSCF calculation used for optimizing coordinates
enum class RotCoordVscfCalcType: int
{  GROUNDSTATE = 0
,  STATEAVERAGE
,  HARMONICGROUNDSTATE
};

//! Convergence criterion for coordinate optimization
enum class RotCoordParam: int
{  VALUE = 0      // Energy change
,  GRADIENT       // Max. gradient element
,  GRADIENTNORM   // Norm of gradient
,  GRADHESS       // Gradient and Hessian (to check that a minimum is obtained)
};

/**
 * Definition of grid for optimization of coordinate-rotation parameters.
 **/
class RotCoordGridCalcDef
{
   //! Max. iterations for Newton-Raphson algorithm
   CREATE_VARIABLE(In, MaxNewtIter, I_10);
   
   //! Convergence threshold for Newton-Raphson algorithm
   CREATE_VARIABLE(Nb, NewtConv, 1.e-10);
   
   //! Delta for calculating numerical gradients and Hessians
   CREATE_VARIABLE(Nb, NumDiffDelta, 2.e-2);
   
   //! Number of Fourier points
   CREATE_VARIABLE(In, FourierPt, I_2);
   
   //! Plot points
   CREATE_VARIABLE(In, PlotPoints, I_0);
   
   //! Only make plots - no optimization done.
   CREATE_VARIABLE(bool, PlotOnly, false);
   
   private:
      //! Convergence criterion
      RotCoordParam mOptParam = RotCoordParam::GRADHESS;

   public:
      /* @name Setters and getters */
      //!@{
      void SetOptParam(const std::string& aStr)
      {
         static const std::map<std::string, RotCoordParam> stringtoparam = 
         {  {"VALUE", RotCoordParam::VALUE}
         ,  {"GRADIENT", RotCoordParam::GRADIENT}
         ,  {"GRADIENTNORM", RotCoordParam::GRADIENTNORM}
         ,  {"GRADHESS", RotCoordParam::GRADHESS}
         };
      
         try
         {
            mOptParam = stringtoparam.at(aStr);
         }
         catch (  const std::out_of_range& err
               )
         {
            MIDASERROR("RotCoordParam: '" + aStr + "' not found!");
         }
      }
  
      RotCoordParam GetOptParam() const {return mOptParam;}
      //!@}
};

/**
 * Definition of VSCF calculation for optimizing coordinates
 **/
class RotCoordVscfCalcDef
{
   //! Operator file
   CREATE_VARIABLE(std::string, OperFile, std::string{"NONE"});

   //! Name of this calculation
   CREATE_VARIABLE(std::string, Name, std::string{""});

   //! Fix the maximum coupling level in the transformed potential (NB: will result in transformation errors)
   CREATE_VARIABLE(bool, KeepMaxCoupLevel, false); 

   //! Rotate zero angle in constructor for VscfOptMeasure (Niels: I have no idea why...)
   CREATE_VARIABLE(bool, RotZeroFirst, false);
   
   private:
      //! Type of VSCF calculation
      RotCoordVscfCalcType mCalcType = RotCoordVscfCalcType::GROUNDSTATE;

   public:
      /* @name Setters and getters */
      //!@{
      void SetCalcType(const std::string& aStr)
      {
         static const std::map<std::string, RotCoordVscfCalcType> stringtotype =
         {  {"GROUNDSTATE", RotCoordVscfCalcType::GROUNDSTATE}
         ,  {"STATEAVERAGE", RotCoordVscfCalcType::STATEAVERAGE}
         ,  {"HARMONICGROUNDSTATE", RotCoordVscfCalcType::HARMONICGROUNDSTATE}
         };
      
         try
         {
            mCalcType = stringtotype.at(aStr);
         }
         catch (  const std::out_of_range& err
               )
         {
            MIDASERROR("RotCoordVscfCalcType: '" + aStr + "' not found!");
         }
      }

      RotCoordVscfCalcType GetCalcType() const {return mCalcType;}
      //!@}
};

/**
 * CalcDef for coordinate rotation
 **/
class RotCoordCalcDef
{
   //! Do localization?
   CREATE_VARIABLE(bool, Localize, false);

   //! Weight of localization measure
   CREATE_VARIABLE(Nb, LocWeight, C_0);
   
   //! New suffix of operator
   CREATE_VARIABLE(std::string, NewSuffix, std::string{"_rotated"});
   
   //! Number of sweeps in the algorithm
   CREATE_VARIABLE(In, NumberOfSweeps, I_800);
   
   //! Screen rotations based on max. difference between mode frequencies
   CREATE_VARIABLE(bool, DoFreqScreen, false);
   
   //! Maximum frequency difference for freq. screening.
   CREATE_VARIABLE(Nb, FreqScreenMaxFreq, I_100);
   
   //! Convergence threshold of the Jacobi sweep algorithm.
   CREATE_VARIABLE(Nb, ConvThr, 1.e-10);
   
   //! Minimum rotation angle (-1 disables)
   CREATE_VARIABLE(Nb, MinRotAng, -C_1);
   
   //! Use the parallel algorithm (which performes rotations at the end of the sweep)
   CREATE_VARIABLE(bool, DoRotsAtSweepEnd, false);
   
   //! Name of rotation-matrix file
   CREATE_VARIABLE(std::string, RotMatFile, std::string{""});
   
   //! Do random iterations
   CREATE_VARIABLE(In, RandomIter, I_0);
   
   private:
      //! Definition of grid
      RotCoordGridCalcDef mGridCalcDef;

      //! CalcDefs for VSCF-energy optimizations
      std::vector<RotCoordVscfCalcDef> mVscfCalcDef;

      //! Weights of VSCF energies.
      std::vector<Nb> mEVscfWeights;

      //! Operators that need rotation
      std::vector<std::string> mOpersForRot;

   public:
      /* @name Setters and getters */
      //!@{
      void AddOperForRot(const std::string& aStr) {mOpersForRot.emplace_back(aStr);}
      void EmplaceVscfMeasure
         (  RotCoordVscfCalcDef&& aCalcDef
         ,  Nb aWeight
         )
      {
         this->mVscfCalcDef.emplace_back(aCalcDef);
         this->mEVscfWeights.emplace_back(aWeight);
      }
      
      const auto& GetGridCalcDef() const {return mGridCalcDef;}
      auto& GetGridCalcDef() {return mGridCalcDef;}
      bool GetPrintRotMat() const {return !mRotMatFile.empty();}
      In GetNoOpersForRot() const {return mOpersForRot.size();}
      const std::string& GetOperForRotI
         (  In aIn
         )  const
      {
         MidasAssert(aIn < mOpersForRot.size(), "Requesting operator out of range!");
         return mOpersForRot[aIn];
      }

      const auto& GetVscfCalcDef
         (  In i = I_0
         )  const
      {
         MidasAssert(i < mVscfCalcDef.size(), "Requesting RotCoordVscfCalcDef out of range!");
         return mVscfCalcDef[i];
      }
      auto& GetVscfCalcDef
         (  In i = I_0
         )
      {
         MidasAssert(i < mVscfCalcDef.size(), "Requesting RotCoordVscfCalcDef out of range!");
         return mVscfCalcDef[i];
      }
      Nb GetEvscfWeight(In i = I_0) const
      {
         MidasAssert(i < mEVscfWeights.size(), "Requesting VSCF energy weight out of range!");
         return mEVscfWeights[i];
      } 
      //!@}
      
      //! Do we have a VSCF-optimization?
      bool ContainsVscf() const {return this->mVscfCalcDef.size() > 0; }

      //! How many measures do we have?
      In NMeasures
         (
         )  const
      {
         return mLocalize ? mVscfCalcDef.size() + 1 : mVscfCalcDef.size();
      }
      //! Number of VSCF-opt measures
      In NOptMeasures
         (
         )  const
      {
         return mVscfCalcDef.size();
      }

      //! Sanity check (including normalization of weights)
      void SanityCheck
         (
         )
      {
         // Check that the number of calcdefs and weights is the same
         if (  mVscfCalcDef.size() != mEVscfWeights.size()
            )
         {
            MIDASERROR("Number of EVscf weights does not match number of VscfCalcDefs");
         }

         // Check that weights are positive
         if (  libmda::numeric::float_neg(mLocWeight)
            )
         {
            MIDASERROR("Localization weight is negative!");
         }

         // Normalize weights if needed
         Nb total_weight = mLocalize ? mLocWeight : C_0;
         for(const auto& w : mEVscfWeights)
         {
            if (  libmda::numeric::float_neg(w)
               )
            {
               MIDASERROR("Found negative E_vscf weight!");
            }

            total_weight += w;
         }
         if (  libmda::numeric::float_neq(total_weight, C_1, 10)
            )
         {
            MidasWarning("RotCoordCalcDef: Input weights do not sum to 1. Will normalize!");

            mLocWeight /= total_weight;
            for(auto& w : mEVscfWeights)
            {
               w /= total_weight;
            }
         }
      }
};

} // namespace midas::input

#endif //ROTCOORDCALCDEF_H
