
#include <vector>
#include <map>
#include <string>
#include <fstream>

#include "input/Input.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "input/MidasOperatorReader.h"
#include "input/MidasOperatorWriter.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "input/OpDef.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "input/MidasOperatorInput.h"
#include "input/GetLine.h"
#include "mpi/FileToString.h"
#include "operator/OneModeOper.h"
#include "operator/FunctionOneModeOper.h"

/**
 * Do the actual reading of the operator file.
 * Takes a variable input level.
 * This opens the possibility to insert operator input at any level.
 *
 * @param aInpLevel    Variable input level.
 * @param aOpFile      The file we are reading.
 *
 * @return   Returns the last line read.
 **/
std::string MidasOperatorReader::ReadOperatorImpl
   (  std::istream& aOpFile
   ,  In aInpLevel
   )
{
   // local variables
   std::vector<std::string> ignore_modes;
   std::vector<std::string> inactive_mode_labels;
   enum INPUT {ERROR, FREQUENCIES, CONSTANTS, OPERATORTERMS, FUNCTIONS, SCALEFACTORS, MODENAMES, IGNOREMODES, INACTIVEMODES, ELECTRONICDOF, REFERENCEVALUE, SETINFO};
   const std::map<std::string,INPUT> input_word =
   {  {"#"+std::to_string(aInpLevel) + "CONSTANTS",CONSTANTS}
   ,  {"#"+std::to_string(aInpLevel) + "FUNCTIONS",FUNCTIONS}
   ,  {"#"+std::to_string(aInpLevel) + "FREQUENCIES",FREQUENCIES}
   ,  {"#"+std::to_string(aInpLevel) + "SCALEFACTORS",SCALEFACTORS}
   ,  {"#"+std::to_string(aInpLevel) + "OPERATORTERMS",OPERATORTERMS}
   ,  {"#"+std::to_string(aInpLevel) + "MODENAMES",MODENAMES}
   ,  {"#"+std::to_string(aInpLevel) + "IGNOREMODES",IGNOREMODES}
   ,  {"#"+std::to_string(aInpLevel) + "INACTIVEMODES",INACTIVEMODES}
   ,  {"#"+std::to_string(aInpLevel) + "ELECTRONICDOF",ELECTRONICDOF}
   ,  {"#"+std::to_string(aInpLevel) + "REFERENCEVALUE",REFERENCEVALUE}
   ,  {"#"+std::to_string(aInpLevel) + "SETINFO", SETINFO}
   };

   std::string s;
   std::string s_orig;
   std::vector<std::string> temp_cont;

   bool already_read = false;

   while (  (  already_read
            || midas::input::GetParsedLine(aOpFile, s)
            )
         && midas::input::IsKeyword(s)
         && !midas::input::IsLowerLevelKeyword(s, aInpLevel)
         )
   {
      already_read = false;
      s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case REFERENCEVALUE:
         {
            if (aOpFile.eof() || '#' == std::ws(aOpFile).peek() || !midas::input::GetLine(aOpFile,s))
            {
               MIDASERROR("ReferenceValue is not defined in file " + mName + " Please remove #"+std::to_string(aInpLevel)+"REFERENCEVALUE or add the reference value");
            }
            mReferenceValue = midas::util::FromString<Nb>(s);
            break;
         }
         case MODENAMES:
         {
            while (!aOpFile.eof() && '#' != std::ws(aOpFile).peek())
            {
               if (midas::input::GetLine(aOpFile,s))
               {
                  std::vector<std::string> tmp = midas::util::StringVectorFromString(s);
                  for (auto it = tmp.begin(); it != tmp.end(); ++it)
                  {
                     mModeNames.push_back(*it);
                  }
               }
            }
            break;
         }
         case ELECTRONICDOF:
         {
            midas::input::GetLine(aOpFile, s);
            if (  std::find(mModeNames.begin(), mModeNames.end(), s) == mModeNames.end()
               )
            {
               mModeNames.emplace_back(s);
            }
            mElectronicDof = s;

            break;
         }
         case IGNOREMODES:
         {
            while (!aOpFile.eof() && '#' != std::ws(aOpFile).peek())
            {
               if (midas::input::GetLine(aOpFile, s))
               {
                  std::vector<std::string> tmp = midas::util::StringVectorFromString(s);
                  for (auto it = tmp.begin(); it != tmp.end(); ++it)
                  {
                     ignore_modes.push_back(*it);
                  }
               }
            }
            break;
         }
         case INACTIVEMODES:
         {
            std::string str;
            midas::input::GetLine(aOpFile, str);
            if (midas::input::NotKeyword(str))
            {
               inactive_mode_labels = midas::util::StringVectorFromString(str);
            }

            break;
         }
         case CONSTANTS:
         {
            while(!aOpFile.eof() && '#' != std::ws(aOpFile).peek())
            {
               if(midas::input::GetLine(aOpFile,s))
               {
                  temp_cont = midas::util::StringVectorFromString(s);
                  if(temp_cont.size() != 2)

                     MIDASERROR("Something is wrong with the constant definition : "+s+" in file : "+mName);
                  mConstants.emplace_back(temp_cont[0], midas::util::FromString<Nb>(temp_cont[1]));
               }
            }
            break;
         }
         case FUNCTIONS:
         {
            while(!aOpFile.eof() && '#' != std::ws(aOpFile).peek())
            {
               if(midas::input::GetLine(aOpFile,s))
               {
                  temp_cont = midas::util::StringVectorFromString(s);
                  if(temp_cont.size() != 2)
                     MIDASERROR("Something is wrong with the function definition : "+s+" in file : "+mName+" (too many spaces?)");
                  mFunctions.emplace_back(temp_cont[0], temp_cont[1]);
               }
            }
            break;
         }
         case FREQUENCIES:
         {
            if (aOpFile.eof() || '#' == std::ws(aOpFile).peek() || !midas::input::GetLine(aOpFile,s))
            {
               MIDASERROR("Frequencies are not defined in file " + mName + " Please remove #"+std::to_string(aInpLevel)+"FREQUENCIES or add the frequencies");
            }
            mFrequencies.SetNewSize(midas::util::MidasVectorFromString(s).Size());
            mFrequencies = midas::util::MidasVectorFromString(s);
            break;
         }
         case SCALEFACTORS:
         {
            if (aOpFile.eof() || '#' == std::ws(aOpFile).peek() || !midas::input::GetLine(aOpFile,s))
            {
               MIDASERROR("ScaleFactors are not defined in file " + mName + " Please remove #"+std::to_string(aInpLevel)+"SCALEFACTORS or add the scalefactors");
            }
            mScalings.SetNewSize(midas::util::MidasVectorFromString(s).Size());
            mScalings = midas::util::MidasVectorFromString(s);
            break;
         }
         case OPERATORTERMS:
         {
            while (  midas::input::GetLine(aOpFile, s)
                  && !midas::input::IsKeyword(s)
                  )
            {
               if (  s.empty()
                  )
               {
                  continue;
               }
               temp_cont = midas::util::StringVectorFromString(s);
               if (temp_cont.size() < I_2)
               {
                  MIDASERROR("Something is wrong with the operator definition : "+s+" in file : "+mName+" too few spaces?");
               }
               auto it = temp_cont.begin();
               ++it;

               std::pair<std::string, std::string> sep_ret;
               std::vector<std::string> funcs;
               std::vector<std::string> modes;
               for (;it != temp_cont.end(); ++it)
               {
                  sep_ret = SeperateFunctionAndModeName(*it);
                  funcs.push_back(sep_ret.first);
                  modes.push_back(sep_ret.second);
               }

               //Remember to scale the coeff with the general coef for the operator file
               auto it_ins = mOperatorTerms.insert(std::make_pair(std::make_pair(modes, funcs), midas::util::FromString<long double>(temp_cont[0])));
               if (!it_ins.second)
               {
                  it_ins.first->second += midas::util::FromString<long double>(temp_cont[0]);
               }
            }

            already_read = true;

            break;
         }
         case SETINFO:
         {
            midas::input::GetLine(aOpFile, s);
            mInfoLabel = s;
            break;
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a MidasOperatorInput level " << aInpLevel << " Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   //
   if (ignore_modes.size() > I_0)
   {
      RemoveModes(ignore_modes);
      MidasOperatorWriter mop_write("test_removed.mop");
      mop_write.FromReadPotential(*this);
   }

   //
   if (inactive_mode_labels.size() > I_0)
   {
      SetInactiveModes(inactive_mode_labels);
      MidasOperatorWriter mop_write("inactive_modes_removed.mop");
      mop_write.FromReadPotential(*this);
   }

   return s;
}

/**
 * Construtor from operator name.
 *
 * @param aName  The name of the operator.
 **/
MidasOperatorReader::MidasOperatorReader
   (  const std::string& aName
   )
   :  mName(aName)
   ,  mReferenceValue(0.0)
{
}

/**
 * Read operator from input stream.
 * Searched for start of operator input and tries to read in operator from there.
 * When done checks that we reached the end of the operator input.
 *
 * @param aOpFile               The stream to read.
 * @param aInputLevel           The input level.
 * @param aSkipHeaderAndFooter  Skip reading MIDASMOPINPUT and MIDASMOPINPUTEND?
 *                              This can be set to true if the operator input is part of another file,
 *                              which does not contain the MIDASMOPINPUT and MIDASMOPINPUTEND keywords.
 *
 * @return   Returns last read line.
 **/
std::string MidasOperatorReader::ReadOperator
   (  std::istream& aOpFile
   ,  In aInputLevel
   ,  bool aSkipHeaderAndFooter
   )
{
   std::string str;

   // If not skipped, we search for beginning of input
   if (!aSkipHeaderAndFooter)
   {
      while (midas::input::GetParsedLine(aOpFile, str))
      {
         if (str.find("#" + std::to_string(aInputLevel)  + "MIDASMOPINPUT") != string::npos)
         {
            MidasWarning("Use of #0MIDASMOPINPUT is depreciated. Use #0MIDASOPERATOR instead!");
            break;
         }
         else if(str.find("#" + std::to_string(aInputLevel)  + "MIDASOPERATOR") != string::npos)
         {
            break;
         }
      }

      // Add 1 to input level before reading rest of operator.
      aInputLevel += I_1;
   }

   // Read the operator
   str = this->ReadOperatorImpl(aOpFile, aInputLevel);

   str = midas::input::ParseInput(str);

   if (!aSkipHeaderAndFooter)
   {
      // Subtract 1 from input level
      aInputLevel -= I_1;

      // Check that operator was read to end
      if(str.find("#" + std::to_string(aInputLevel)  + "MIDASMOPINPUTEND") != std::string::npos)
      {
         MidasWarning("Use of #0MIDASMOPINPUTEND is depreciated. Use #0MIDASOPERATOR instead!");
      }
      else if (str.find("#" + std::to_string(aInputLevel)  + "MIDASOPERATOREND") != std::string::npos)
      {
         // Do nothing, everything is fine
      }
      else
      {
         MIDASERROR("Something went wrong while reading operator \"" + mName + "\" check your input. Last read line : '" + str + "'. Operator input level : " + std::to_string(aInputLevel) + ".");
      }
   }

   // Return last read string
   return str;
}

/**
 * Read operator from filename.
 * First opens the file, then tries to read the operator.
 *
 * @param aFileName    The filename.
 *
 * @return  Returns last line.
 **/
std::string MidasOperatorReader::ReadOperator
   (  const std::string& aFileName
   )
{
   // Open file
   std::istringstream op_file = midas::mpi::FileToStringStream(aFileName);
   if(op_file.fail() || op_file.str().empty())
   {
      MIDASERROR("Failed to open file: '" + aFileName + "', it might not exist.");
   }

   // Then read the file
   return this->ReadOperator(op_file);
}

/**
 * Get name of operator.
 *
 * @return   Return name as string.
 **/
const std::string& MidasOperatorReader::Name
   (
   ) const
{
   return mName;
}

/**
 *
 **/
std::vector<std::string> MidasOperatorReader::GetModeNames
   (
   )  const
{
   return mModeNames;
}

/**
 *
**/
void MidasOperatorReader::RemoveModes
   (  const std::vector<std::string>& arModeNames
   )
{
   // first the operator terms
   for (auto it = mOperatorTerms.begin(); it != mOperatorTerms.end();)
   {
      if (std::any_of
            (  arModeNames.begin()
            ,  arModeNames.end()
            ,  [it](const std::string& name)
            {
               return (std::find(it->first.first.begin(), it->first.first.end(), name) != it->first.first.end());
            }
            )
         )
      {
         it = mOperatorTerms.erase(it);
      }
      else
      {
         ++it;
      }
   }

   // Create new scaling midas vectors
   if (mScalings.Size() > I_0)
   {
      MidasVector new_scalfactors(mScalings.Size() - arModeNames.size());
      In n = I_0;
      for (In i = I_0; i < mScalings.Size(); ++i)
      {
          if (std::find(arModeNames.begin(), arModeNames.end(), mModeNames.at(i)) == arModeNames.end())
          {
             new_scalfactors[n++] = mScalings[i];
          }
      }
      MidasAssert(n == new_scalfactors.Size(), "Size of new and old scaling factor vector does not fit");
      mScalings.SetNewSize(n, false);
      mScalings = new_scalfactors;
   }

   //
   if (mFrequencies.Size() > I_0)
   {
      MidasVector new_freqs(mFrequencies.Size() - arModeNames.size());
      In n = I_0;
      for (In i = I_0; i < mFrequencies.Size(); ++i)
      {
          if (std::find(arModeNames.begin(), arModeNames.end(), mModeNames.at(i)) == arModeNames.end())
          {
             new_freqs[n++] = mFrequencies[i];
          }
      }
      MidasAssert(n == new_freqs.Size(), "Size of new and old scaling factor vector does not fit");
      mFrequencies.SetNewSize(n, false);
      mFrequencies = new_freqs;
   }

   // Remove modes from the vectors
   for (In i = I_0; i < arModeNames.size(); ++i)
   {
      // Find index
      auto it_mode = std::find(mModeNames.begin(), mModeNames.end(), arModeNames.at(i));
      In index = std::distance(mModeNames.begin(), it_mode);
      mModeNames.erase(it_mode);
   }
}

/**
 * Function for setting some modes inactive and evaluating associated operator terms
**/
void MidasOperatorReader::SetInactiveModes
   (  const std::vector<std::string>& arInactiveModes
   )
{
   // Loop through the operator terms
   for (auto it = mOperatorTerms.begin(); it != mOperatorTerms.end();)
   {
      if (  std::any_of
            (  arInactiveModes.begin()
            ,  arInactiveModes.end()
            ,  [it](const std::string& name)
            {
               return (std::find(it->first.first.begin(), it->first.first.end(), name) != it->first.first.end());
            }
            )
         )
      {
         // Find the mode in question
         const auto& mode_labels = it->first.first;
         const auto& operator_terms = it->first.second;

         // If the operator term is a one-mode operator term then we can removed it without further considerations
         if (operator_terms.size() == I_1)
         {
            it = mOperatorTerms.erase(it);
         }
         // Search for term that hold some kind of differential operator, these should be deleted if the differential operator belongs to an inactive mode
         else if (   std::any_of
               (  operator_terms.begin()
               ,  operator_terms.end()
               ,  [](std::string ddq_term)
               {
                  return ddq_term.find("DDQ") != std::string::npos;
               }
               )
            )
         {
            auto ddq_term_erased = false;

            for (In imode = I_0; imode < mode_labels.size(); ++imode)
            {
               if (ddq_term_erased)
               {
                  break;
               }

               for (In inact_mode = I_0; inact_mode < arInactiveModes.size(); ++inact_mode)
               {
                  if (ddq_term_erased)
                  {
                     break;
                  }

                  if (arInactiveModes[inact_mode] == mode_labels[imode])
                  {
                     ddq_term_erased = true;

                     it = mOperatorTerms.erase(it);
                  }
               }
            }
         }
         //
         else
         {
            auto modified_term = false;
            auto coeff = C_1;
            auto new_mode_labels = mode_labels;
            auto new_operator_terms = operator_terms;

            // As we are going to erase elements, we need to go backwards
            for (In imode = mode_labels.size() - I_1; imode >= I_0; --imode)
            //for (In imode = I_0; imode < mode_labels.size(); ++imode)
            {
               for (In inact_mode = arInactiveModes.size() - I_1; inact_mode >= I_0; --inact_mode)
               //for (In inact_mode = I_0; inact_mode < arInactiveModes.size(); ++inact_mode)
               {
                  if (arInactiveModes[inact_mode] == mode_labels[imode])
                  {
                     // We are now going to modify this term
                     modified_term = true;

                     // Determine the coefficient generated by the constant term
                     FunctionContainer<Nb> func_cont;
                     GetFunctionContainer(func_cont);

                     OneModeOperBase<Nb>* new_oper = new FunctionOneModeOper<Nb>(operator_terms[imode], I_0, false, func_cont, true);
                     coeff *= new_oper->EvaluateFunc(C_0);

                     new_mode_labels.erase(new_mode_labels.begin() + imode);
                     new_operator_terms.erase(new_operator_terms.begin() + imode);
                  }
               }
            }

            //
            if (modified_term)
            {
               // Delete the old operator term with inactive modes
               it = mOperatorTerms.erase(it);

               // Construct a new operator term and add it to the list of operators. If it is already present, we instead add the coefficient to the coefficient of the existing operator
               if (!new_mode_labels.empty() && !new_operator_terms.empty())
               {
                  auto new_operator_key = std::make_pair(new_mode_labels, new_operator_terms);
                  if (  !mOperatorTerms.insert
                        (  std::make_pair
                           (  new_operator_key
                           ,  it->second * coeff
                           )
                        ).second
                     )
                  {
                     mOperatorTerms[new_operator_key] += coeff;
                  }
               }
            }
         }
      }
      else
      {
         ++it;
      }
   }

   // Create new scaling midas vectors
   if (mScalings.Size() > I_0)
   {
      MidasVector new_scalfactors(mScalings.Size() - arInactiveModes.size());
      In n = I_0;
      for (In i = I_0; i < mScalings.Size(); ++i)
      {
          if (std::find(arInactiveModes.begin(), arInactiveModes.end(), mModeNames.at(i)) == arInactiveModes.end())
          {
             new_scalfactors[n++] = mScalings[i];
          }
      }
      MidasAssert(n == new_scalfactors.Size(), "Size of new and old scaling factor vector does not fit");
      mScalings.SetNewSize(n, false);
      mScalings = new_scalfactors;
   }

   //
   if (mFrequencies.Size() > I_0)
   {
      MidasVector new_freqs(mFrequencies.Size() - arInactiveModes.size());
      In n = I_0;
      for (In i = I_0; i < mFrequencies.Size(); ++i)
      {
          if (std::find(arInactiveModes.begin(), arInactiveModes.end(), mModeNames.at(i)) == arInactiveModes.end())
          {
             new_freqs[n++] = mFrequencies[i];
          }
      }
      MidasAssert(n == new_freqs.Size(), "Size of new and old scaling factor vector does not fit");
      mFrequencies.SetNewSize(n, false);
      mFrequencies = new_freqs;
   }

   // Remove modes from the vectors
   for (In i = I_0; i < arInactiveModes.size(); ++i)
   {
      // Find index
      auto it_mode = std::find(mModeNames.begin(), mModeNames.end(), arInactiveModes.at(i));
      In index = std::distance(mModeNames.begin(), it_mode);
      mModeNames.erase(it_mode);
   }
}

/**
 *
 **/
std::pair<std::string, std::string> MidasOperatorReader::SeperateFunctionAndModeName
   ( const string& aFunc
   )
{
   size_t mdefstart = aFunc.find_last_of("(");
   size_t mdefend = aFunc.find_last_of(")");
   if(mdefstart == string::npos || mdefend == string::npos )
   {
      MIDASERROR("Error no mode name related to function \""+aFunc+"\" in file \""+mName+"\", check your input");
   }
   return std::make_pair(aFunc.substr(0,mdefstart)+aFunc.substr(mdefend+1), aFunc.substr(mdefstart+1,mdefend-mdefstart-1));
}

/**
 *
 **/
void MidasOperatorReader::AddTermsToOpDef
   (  OpDef& arOpDef
   ,  In aFnr
   )  const
{
   FunctionContainer<Nb> func_cont;
   GetFunctionContainer(func_cont);

   if (aFnr == -I_1)
   {
      midas::input::detail::OperFromInputReadIn(mOperatorTerms, arOpDef, func_cont);
   }
   else if (aFnr == -I_2)
   {
      midas::input::detail::KeoFromFileReadIn(mOperatorTerms, arOpDef, func_cont);
   }
   else
   {
      midas::input::detail::OperFromFileReadIn(mOperatorTerms, arOpDef, func_cont, aFnr);
   }
}

/**
 *
 **/
void MidasOperatorReader::GiveScalingAndNamestoOpDef
   (  OpDef& arOpDef
   )  const
{
   // Give each mode a global number
   // And add it to the OpDef
   for(const auto& mode : mModeNames)
   {
      gOperatorDefs.ModeNr(mode, true);
      LocalModeNr tmp(I_0);
      arOpDef.AddMode(mode, tmp);
   }

   // Set electronic DOF (and check that it is consistent between OpDef%s)
   if (  gOperatorDefs.ElectronicDofLabel().empty()
      )
   {
      gOperatorDefs.SetElectronicDof(this->mElectronicDof);
   }
   else
   {
      MidasAssert(gOperatorDefs.ElectronicDofLabel() == this->mElectronicDof, "Name of electronic DOF must be the same in all operators!");
   }

   if (I_0 != mScalings.Size())
   {
      arOpDef.SetScaleFactors(mScalings);
   }
   else if (I_0 != mFrequencies.Size())
   {
      MidasVector temp(mFrequencies.Size());
      for (unsigned int i = 0; i < mFrequencies.Size(); ++i)
      {
         temp[i] = std::sqrt(mFrequencies[i]);
      }
      arOpDef.SetScaleFactors(temp);
   }
}

/**
 *
 **/
void MidasOperatorReader::GetScalingVector
   ( MidasVector& arScalingVector
   ) const
{
   if(I_0 != mScalings.Size())
   {
      arScalingVector.SetNewSize(mScalings.Size());
      arScalingVector = mScalings;
   }
   else if(I_0 != mFrequencies.Size())
   {
      arScalingVector.SetNewSize(mFrequencies.Size());
      for(Uin i = I_0; i < mFrequencies.Size(); ++i)
         arScalingVector[i] = std::sqrt(mFrequencies[i]);
   }
   else
   {
      arScalingVector.SetNewSize(mModeNames.size());
      for(int i = 0; i < arScalingVector.Size(); ++i)
      {
         arScalingVector[i] = 1.0;
      }
   }
}

/**
 * The following is used to interface to GenericBasePot
 **/
std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>::const_iterator MidasOperatorReader::GetFrontOfOperators
   (
   )  const
{
   return mOperatorTerms.begin();
}

/**
 *
 **/
std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>::const_iterator MidasOperatorReader::GetEndOfOperators
   (
   )  const
{
   return mOperatorTerms.end();
}

/**
 *
 **/
std::vector<std::pair<std::string, Nb> > MidasOperatorReader::GetConstants
   (
   )  const
{
   return mConstants;
}

/**
 *
 **/
std::vector<std::pair<std::string, std::string> > MidasOperatorReader::GetFunctions
   (
   )  const
{
   return mFunctions;
}

/**
 *
 **/
std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than> MidasOperatorReader::GetOperators
   (
   )  const
{
   return mOperatorTerms;
}

/***
*
***/
void MidasOperatorReader::ScaleOperator(Nb aScaling)
{
   for(auto it = mOperatorTerms.begin(); it != mOperatorTerms.end(); ++it)
   {
      it->second *= aScaling;
   }
}
