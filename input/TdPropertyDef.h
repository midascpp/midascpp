/**
************************************************************************
*
* @file                TdPropertyDef.h
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines properties of time-dependent
*                      wave functions.
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDPROPERTYDEF_H_INCLUDED
#define TDPROPERTYDEF_H_INCLUDED

#include <set>
#include <string>
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ode/OdeInfo.h"

namespace midas::input {

/**
 * Class that contains and reads input on time-dependent properties of wave functions.
 **/
class TdPropertyDef
{
   //! Properties to calculate
   CREATE_VARIABLE(std::set<std::string>, Properties, std::set<std::string>{});
   
   //! Spectra to construct
   CREATE_VARIABLE(std::set<std::string>, Spectra, std::set<std::string>{});
   
   //! Expectation-value operators
   CREATE_VARIABLE(std::set<std::string>, ExptValOpers, std::set<std::string>{});

   //! Statistics to calculate, e.g. norm of parameters.
   CREATE_VARIABLE(std::set<std::string>, Stats, std::set<std::string>{});
   
   //! Write wave function for all time steps for following modes
   CREATE_VARIABLE(std::set<std::string>, WfOutModes, std::set<std::string>{});
   
   //! Mode pairs to write two-mode densities for
   CREATE_VARIABLE(std::set<std::set<std::string>>, TwoModeDensityPairs, std::set<std::set<std::string>>{});
   
   //! Write primitive basis functions for modes
   CREATE_VARIABLE(std::set<std::string>, WritePrimitiveBasisForModes, std::set<std::string>{});
   
   //! Convolute the autocorrelation function before FFT to prevent Gibbs' phenomenon
   CREATE_VARIABLE(bool, ConvoluteAutoCorr, true);
   
   //! Level of zero-padding of FFT
   CREATE_VARIABLE(In, PaddingLevel, I_16);
   
   //! Shift the spectrum
   CREATE_VARIABLE(UNPACK(std::pair<std::string, Nb>), SpectrumEnergyShift, UNPACK(std::pair<std::string, Nb>("FIXED", C_0)));
   
   //! Screen spectrum output
   CREATE_VARIABLE(Nb, SpectrumOutputScreeningThresh, C_0);
   
   //! Set interval for spectrum output
   CREATE_VARIABLE(UNPACK(std::pair<Nb,Nb>), SpectrumInterval, UNPACK(std::pair<Nb,Nb>(C_0, C_0)));
   
   //! Grid density for plotting wf
   CREATE_VARIABLE(In, WfGridDensity, I_10);
   
   //! Normalize max peak of spectrum to 1
   CREATE_VARIABLE(bool, NormalizeSpectrum, false);
   
   //! Abs all intensities in spectra
   CREATE_VARIABLE(bool, AbsIntensities, false);
   
   //! Life time used for Lorenzian broadening
   CREATE_VARIABLE(Nb, LifeTime, -C_1);

   public:
      //! Default c-tor
      TdPropertyDef() = default;

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (  midas::ode::OdeInfo&
         ,  bool
         );

      //! Read properties
      bool ReadProperties
         (  std::istream&
         ,  std::string&
         );

      //! Read spectra
      bool ReadSpectra
         (  std::istream&
         ,  std::string&
         );

      //! Add property to mProperties
      void AddProperty
         (  std::string
         );

      //! Read operators for expt values
      bool ReadExptValOpers
         (  std::istream&
         ,  std::string&
         );

      //! Read operators for expt values
      bool ReadStats
         (  std::istream&
         ,  std::string&
         );

      //! Read two-mode-density pairs
      bool ReadTwoModeDensityPairs
         (  std::istream&
         ,  std::string&
         );
   
};

} /* namespace midas::input */

#endif /* TDPROPERTYDEF_H_INCLUDED */
