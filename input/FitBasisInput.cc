/**
************************************************************************
*
* @file                FitBasisInput.cc
*
* Created:             04-02-2014
*
* Author:              Bo Thomsen (ove@chem.au.dk)
*
* Short Description:   Reader for everything related to fit-basis functions
*
* Last modified: Mon Feb 04, 2014  03:07PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <string>
#include <vector>
#include <utility>
#include <fstream>
#include <map>
#include <algorithm>
#include <memory>

// midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/FitBasisInput.h"
#include "input/FitBasisCalcDef.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "mpi/FileToString.h"

namespace midas
{
namespace input
{
// forward declaration
std::string ReadGenFitBasisInput
   (  GenFitBasisCalcDef& aCalcDef
   ,  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   );

//forward declaration
std::string ReadFitBasisDef
   (  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   );
// The above functions are defined in GenFitBasisInput.cc

/**
 * MidasCpp input reader for generic fitting functions
**/
std::string ReadFitFunction
   (  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   )
{
   enum INPUT {ERROR, ADDFITFUNCSCONSERV, AUTOFITBASISTYPE, AUTOFITFUNCTIONS,
               DEFFROMFILE, FITALLMCITERGRIDS, FITBASISEFFINERTIAINVMAXORDER,
               FITBASISMAXORDER, FITFUNCTIONSEFFINERTIAINVMAXORDER,
               FITFUNCTIONSMAXORDER, FITMETHOD, FITBASISDEF, IOLEVEL,
               NAME, NTHREADSFIT, SVDFITCUTOFFTHR, TIKHONOVREGUL};

   const std::map<std::string,INPUT> input_word =
   { {"#"+std::to_string(aInpLevel)+"ADDFITFUNCSCONSERV",ADDFITFUNCSCONSERV}
   , {"#"+std::to_string(aInpLevel)+"AUTOFITBASISTYPE",AUTOFITBASISTYPE}
   , {"#"+std::to_string(aInpLevel)+"AUTOFITFUNCTIONS",AUTOFITFUNCTIONS}
   , {"#"+std::to_string(aInpLevel)+"DEFFROMFILE",DEFFROMFILE}
   , {"#"+std::to_string(aInpLevel)+"FITALLMCITERGRIDS",FITALLMCITERGRIDS}
   , {"#"+std::to_string(aInpLevel)+"FITBASISEFFINERTIAINVMAXORDER",FITBASISEFFINERTIAINVMAXORDER}
   , {"#"+std::to_string(aInpLevel)+"FITBASISMAXORDER",FITBASISMAXORDER}
   , {"#"+std::to_string(aInpLevel)+"FITFUNCTIONSEFFINERTIAINVMAXORDER",FITFUNCTIONSEFFINERTIAINVMAXORDER}
   , {"#"+std::to_string(aInpLevel)+"FITFUNCTIONSMAXORDER",FITFUNCTIONSMAXORDER}
   , {"#"+std::to_string(aInpLevel)+"FITMETHOD",FITMETHOD}
   , {"#"+std::to_string(aInpLevel)+"FITBASISDEF",FITBASISDEF}
   , {"#"+std::to_string(aInpLevel)+"IOLEVEL",IOLEVEL}
   , {"#"+std::to_string(aInpLevel)+"NAME",NAME}
   , {"#"+std::to_string(aInpLevel)+"NTHREADSFIT",NTHREADSFIT}
   , {"#"+std::to_string(aInpLevel)+"SVDFITCUTOFFTHR",SVDFITCUTOFFTHR}
   , {"#"+std::to_string(aInpLevel)+"TIKHONOVREGUL",TIKHONOVREGUL}
   };

   std::string s;
   std::string s_orig;
   bool svd_set = false;
   bool tikho_set = false;

   bool already_read = false;
   while ((already_read || GetLine(aFile, s)))
   {
      s_orig = s;
      // Transform to capital letters
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
      // Delete all blank spaces
      while (s.find(" ") != s.npos)
      {
         s.erase(s.find(" "), I_1);
      }
      // Check for empty lines
      if (s.empty())
      {
         continue;
      }

      INPUT input = FindKeyword(input_word, s);
      if (midas::util::FromString<In>(s.substr(1, s.find_first_not_of("#0123456789") - 1)) < aInpLevel)
      {
         break;
      }

      switch(input)
      {
         case ADDFITFUNCSCONSERV:
         {
            gFitBasisCalcDef.SetmAddFitFuncsConserv(true);
            break;
         }
         case AUTOFITBASISTYPE:
         {
            gFitBasisCalcDef.SetmAutomaticFitBasis(true);

            GetLine(aFile, s);
            auto no_inputs = midas::util::FromString<In>(s);

            for (In i = I_0; i < no_inputs; ++i)
            {
               GetLine(aFile, s);
               auto auto_fit_input = midas::util::StringVectorFromString(s);
               if (auto_fit_input.size() < I_2)
               {
                  MIDASERROR("There should be at least two elements given to each entry of the #3 AutomaticFitBasis keyword! Input read as: " + s);
               }
               gFitBasisCalcDef.SetmAutoSimiValue(midas::util::FromString<Nb>(auto_fit_input[I_0]));

               GenFitBasisCalcDef gen_fit_basis_calc_def;
               gFitBasisCalcDef.SetmGenFitBasisCalcDefs(gen_fit_basis_calc_def);

               // Transform to capital letters
               transform(auto_fit_input[I_1].begin(), auto_fit_input[I_1].end(), auto_fit_input[I_1].begin(), (In(*) (In))toupper);
               gFitBasisCalcDef.SetmAutoFitType(auto_fit_input[I_1]);
            }
            break;
         }
         case AUTOFITFUNCTIONS:
         {
            gFitBasisCalcDef.SetmAutomaticFitFuncs(true);
            break;
         }
         case DEFFROMFILE:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("something wrong when reading #" + std::to_string(aInpLevel) + "DEFROMFILE from file " + aFileName + " check your input");
            }

            std::istringstream basis_inp_file = midas::mpi::FileToStringStream(s);
            if (basis_inp_file.str().empty())
            {
               MIDASERROR("It seems that the file " + s + " does not exist, check input under #" + std::to_string(aInpLevel) + "DEFFROMFILE");
            }

            detail::ReadFitFunctionFile(basis_inp_file, s);
            break;
         }
         case FITALLMCITERGRIDS:
         {
            gFitBasisCalcDef.SetmFitAllMcIterGrids(true);
            break;
         }
         case FITBASISEFFINERTIAINVMAXORDER:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("something wrong when reading #" + std::to_string(aInpLevel) + "FITBASISEFFINERTIAINVMAXORDER from file " + aFileName + " check your input");
            }

            std::vector<In> fit_basis_mu_max_order = midas::util::VectorFromString<In>(s);
            gFitBasisCalcDef.SetmFitBasisMuMaxOrder(fit_basis_mu_max_order);
            break;
         }
         case FITBASISMAXORDER:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("something wrong when reading #" + std::to_string(aInpLevel) + "FITBASISMAXORDER from file " + aFileName + " check your input");
            }

            std::vector<In> fit_basis_max_order = midas::util::VectorFromString<In>(s);
            gFitBasisCalcDef.SetmFitBasisMaxOrder(fit_basis_max_order);
            break;
         }
         case FITFUNCTIONSEFFINERTIAINVMAXORDER:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("something wrong when reading #" + std::to_string(aInpLevel) + "FITFUNCTIONSEFFINERTIAINVMAXORDER from file " + aFileName + " check your input");
            }

            std::vector<In> fit_funcs_mu_max_order = midas::util::VectorFromString<In>(s);
            if (fit_funcs_mu_max_order.empty())
            {
               MIDASERROR("Empty vector found from reading line: " + s + " from file " + aFileName);
            }
            gFitBasisCalcDef.SetmFitFuncsMuMaxOrder(fit_funcs_mu_max_order);

            break;
         }
         case FITFUNCTIONSMAXORDER:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("something wrong when reading #" + std::to_string(aInpLevel) + "FITFUNCTIONSMAXORDER from file " + aFileName + " check your input");
            }

            std::vector<In> fit_funcs_max_order = midas::util::VectorFromString<In>(s);
            if (fit_funcs_max_order.empty())
            {
               MIDASERROR("Empty vector found from reading line : " + s + " from file " + aFileName);
            }
            gFitBasisCalcDef.SetmFitFuncsMaxOrder(fit_funcs_max_order);

            break;
         }
         case FITMETHOD:
         {
            GetLine(aFile, s);
            istringstream input_s(s);
            std::string s_new;
            input_s >> s_new;
            transform(s_new.begin(), s_new.end(), s_new.begin(), (In(*) (In))toupper);
            gFitBasisCalcDef.SetmFitMethod(s_new);
            break;
         }
         case FITBASISDEF:
         {
            already_read = true;
            s = ReadFitBasisDef(aFile, aFileName, aInpLevel + I_1);
            break;
         }
         case IOLEVEL:
         {
            GetLine(aFile, s);
            istringstream input_s(s);
            input_s >> gFitBasisIoLevel;
            break;
         }
         case NAME:
         {
            if (aFile.eof() || '#' == aFile.peek() || !GetLine(aFile, s))
            {
               MIDASERROR("Something wrong when reading #" + std::to_string(aInpLevel) + " Name from file " + aFileName+" check your input");
            }

            gFitBasisCalcDef.SetmFitBasName(s);
            break;
         }
         case NTHREADSFIT:
         {
            gFitBasisCalcDef.SetmUseSpecialNthreadsFit(true);
            GetLine(aFile, s);
            gFitBasisCalcDef.SetmNthreadsFit(midas::util::FromString<In>(s));
            break;
         }
         case SVDFITCUTOFFTHR:
         {
            GetLine(aFile, s);
            gFitBasisCalcDef.SetmSvdCut(true); // This is currently in vain as it is default...But must be relaxed if more options.
            gFitBasisCalcDef.SetmSvdCutoffThr(midas::util::FromString<Nb>(s));
            break;
         }
         case TIKHONOVREGUL:
         {
            GetLine(aFile, s);
            gFitBasisCalcDef.SetmTikhonov(true);
            gFitBasisCalcDef.SetmTikhonovParam(midas::util::FromString<Nb>(s));
            gFitBasisCalcDef.SetmSvdCut(false); // Setting Tikhonov-regularization overrides default SVD fit
            break;
         }
         case ERROR: //FALLTHROUGH
         default:
         {
            Mout << " Keyword " << s_orig << " is not a FitBasis level 2 Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input in file: " + aFileName + " please ");
         }
      }
   }

   return s_orig;
}

namespace detail
{
/**
 * Optional reading of the basis set definitions from a seperate input file
**/
void ReadFitFunctionFile
   (  std::istream& aFile
   ,  const std::string& aFileName
   )
{
   enum INPUT {ERROR, BEGIN, END};
   // We keep the old keywords "MIDASFITINFOFILE" and
   // "MIDASFITINFOFILEEND" as options that give a MidasWarning if
   // used for now, BT Jan. 2021
   const std::map<std::string, INPUT> input_word =
   {
      {"#0MIDASFITINFOFILE",BEGIN},
      {"#0MIDASFITINFOFILEEND",END},
      {"#0MIDASFITINFO",BEGIN},
      {"#0MIDASFITINFOEND",END}
   };

   std::string s;
   std::string s_orig;
   while (GetLine(aFile, s))
   {
      s_orig = s;

      // Transform to capital letters
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);
      // Delete all blank spaces
      while (s.find(" ") != s.npos)
      {
         s.erase(s.find(" "), I_1);
      }

      INPUT input = FindKeyword(input_word, s);
      switch(input)
      {
         case BEGIN:
         {
            s = ReadFitBasisDef(aFile, aFileName, I_1);
            break;
         }
         case END: //Emil: Midas NEVAR gets to here?
         {
            return;
            break;
         }
         case ERROR:
         default:
         {
            MIDASERROR("Keyword " + s_orig + " is not a #2 FITBASIS keyword, check your input file please!");
         }
      }
   }
}

} /*namespace detail*/
} /*namespace input*/
} /*namespace midas*/
