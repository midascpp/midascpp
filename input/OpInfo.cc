/**
************************************************************************
* 
* @file                OpInfo.cc
*
* Created:             14-09-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of class OpInfo.
* 
* ???? Last modified: Fri Sep 01, 2006  09:06PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <map>

#include "input/OpInfo.h"
#include "inc_gen/TypeDefs.h"

using std::string;
using std::map;
      
/**
 *
 **/
bool OpInfo::IsDipole() const
{
   return mPropertyType.Is(oper::dipole);
}

/**
 *
 **/
bool OpInfo::IsAlpha() const
{
   return mPropertyType.Is(oper::polarizability2);
}

/**
 *
 **/
bool operator==(const OpInfo& a, const OpInfo& b)
{
   if(a.GetType() != b.GetType())
   {
      return false;
   }
   for(In i = 0; i < (a.GetType().GetOrder() - 1); i++) 
   {
      if(a.GetFrq(i) != b.GetFrq(i))
      {
         return false;
      }
   }
   if (  (a.GetAsymmetric() == I_0 || a.GetAsymmetric() == I_1) 
      && (b.GetAsymmetric() == I_0 || a.GetAsymmetric() == I_1) 
      ) 
   {
      return a.GetAsymmetric() == b.GetAsymmetric();
   }
   return true;
}

/**
 *
 **/
ostream& operator<<(ostream& os, const OpInfo& oi)
{
   os << "OpType = " << oi.GetType()         << "\n";
   os << "Symm.  = " << !oi.GetAsymmetric()  << "\n";
   os << "Frqs   = ";
   for(In i = 0; i < (oi.GetType().GetOrder() - 1); i++) 
   {
      os << oi.GetFrq(i) << " ";
   }
   return os;
}
