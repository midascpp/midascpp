/**
 *******************************************************************************
 * 
 * @file    SymmetryThresholds.h
 * @date    20-09-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Symmetry thresholds for use with point group symmetry determination.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SYMMETRYTHRESHOLDS_H_INCLUDED
#define SYMMETRYTHRESHOLDS_H_INCLUDED

#include "inc_gen/TypeDefs.h"

/***************************************************************************//**
 * @brief
 *    Symmetry thresholds for use with point group symmetry determination.
 *
 * @note
 *    _ULPs_ is short for _Units in the Last Place_ and/or _Units of Least
 *    Precision_ - it's a measure of how close floating point numbers are to
 *    each other. When comparing values close to 1, ULPs are roughly equivalent
 *    to multiples of the machine epsilon. Look it up for more information!
 ******************************************************************************/
struct SymmetryThresholds
{
   //! ULPs threshold when determining characters for symmetry operations.
   unsigned long long mThrGetCharacterFloatEqUlps = 100000000;

   //! ULPs thresh. when determ. whether a vibcoord. is zero, ~units mach. eps.
   unsigned long long mThrVibCoordsNumEqZeroUlps = 10000;

   //! ULPs thresh. when determ. whether two nuclei have same position.
   unsigned long long mThrNucleiIsNumEqualUlps = 100;
};

#endif/*SYMMETRYTHRESHOLDS_H_INCLUDED*/
