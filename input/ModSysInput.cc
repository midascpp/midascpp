/**
************************************************************************
* 
* @file                ModSysInput.cc
*
* 
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the modification of the system
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include "input/Input.h"
#include "input/ModSysCalcDef.h"
#include "input/GetLine.h"
#include "util/conversions/FromString.h"
#include "util/conversions/TupleFromString.h"

/**
 * Forward declaration
 **/
std::string FalconInput
   (  std::istream& Minp
   ,  input::FalconCalcDef* apCalcDef 
   ,  const In& arInputLevel
   );
std::string RotateMoleculeInput
   (  std::istream& Minp
   ,  input::ModSysCalcDef* apCalcDef
   ,  const In& arInputLevel
   );
std::string RotCoordInput
   (  std::istream& Minp
   ,  input::RotCoordCalcDef* apCalcDef 
   ,  const In& arInputLevel
   );
std::string FreqAnaInput
   (  std::istream& Minp
   ,  input::FreqAnaCalcDef* apCalcDef
   ,  const In& arInputLevel
   );
std::string TransformPotInput
   (  std::istream& Minp
   ,  input::TransformPotCalcDef* apCalcDef
   ,  const In& arInputLevel
   );
std::string LhaInput
   (  std::istream& Minp
   ,  input::LhaCalcDef* apCalcDef
   ,  const In& arInputLevel
   );
std::string NormalCoordInput
   (  std::istream& Minp
   ,  input::NormalCoordCalcDef& apCalcDef
   ,  const In& arInputLevel
   );

/**
 *
 **/
std::string ModSysInput(std::istream& Minp, const In& arInputLevel)
{
   gModSysCalcDefs.push_back(input::ModSysCalcDef());
   // new_oper is a reference to the new operator
   input::ModSysCalcDef& calc_def = gModSysCalcDefs[gModSysCalcDefs.size() - I_1];  
   In input_level = arInputLevel + I_1;
   std::string s;    // To be returned when we are done reading this level
   enum INPUT {ERROR,GEOOPT,ROTATEMOLECULE,DEFINESYMMETRY,MODVIBCOORD,REMOVEGLOBALTR,SYSNAMES}; 
   const std::map<std::string, INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"SYSNAMES",SYSNAMES},
      {"#"+std::to_string(input_level)+"GEOOPT",GEOOPT},
      {"#"+std::to_string(input_level)+"ROTATEMOLECULE",ROTATEMOLECULE},
      {"#"+std::to_string(input_level)+"DEFINESYMMETRY",DEFINESYMMETRY},
      {"#"+std::to_string(input_level)+"MODVIBCOORD",MODVIBCOORD},
      {"#"+std::to_string(input_level)+"REMOVEGLOBALTR",REMOVEGLOBALTR}
   };

   bool already_read = false;
   while (  (already_read || midas::input::GetLine(Minp , s))                         // Readin until input levels point up
         && !(s.substr(I_0, I_1) == "#"
         && atoi(s.substr(I_1, I_1).c_str()) < input_level)
         )  
   {
      std::string s_orig = s;
      s = midas::input::ParseInput(s);
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      switch (input)
      {
         case SYSNAMES:
         {
            midas::input::GetLine(Minp, s);
            In n_sys = midas::util::FromString<In>(s);
            calc_def.ReserveSystemNames(n_sys);
            for (In i = I_0 ; i < n_sys ; ++i)
            {
               midas::input::GetLine(Minp,s);
               calc_def.AddSysName(s);
            }
            break;
         }
         case REMOVEGLOBALTR:
         {
            calc_def.SetRemoveGlobalTR(true);
            break;
         }
         case GEOOPT:
         {
            MIDASERROR("Geometry optimization is not implemented");
         }
         case DEFINESYMMETRY: 
         {
            calc_def.SetSymmetryDefined(true);
            midas::input::GetLine(Minp, s);
            std::transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper); // Transform to all upper.
            std::vector<std::string> symmetrydata = midas::util::StringVectorFromString(s);
            MidasAssert
               (  (symmetrydata.size() < I_3)
               ,  "Each line under #3 DefineSymmetry can only have 2 entries in the current implementation, please correct!"
               ); 
            calc_def.SetPointGroup(symmetrydata[I_0]); 
            calc_def.SetSymmetryAxis(symmetrydata[I_1]);
            break;
         }
         case MODVIBCOORD:
         {
            calc_def.SetDoVibCoordOpt(true);
            midas::input::GetLine(Minp, s);
            transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);    // Transform to all upper.
            while (s.find(" ") != s.npos)                                     // Delete ALL blanks
            {
               s.erase(s.find(" "), I_1);
            }
            calc_def.SetVibCoordScheme(s);
            switch (calc_def.GetVibCoordScheme())
            {
               case input::VibCoordScheme::ROTCOORD:
               {
                  s = RotCoordInput(Minp, calc_def.GetpRotCoordCalcDef(), input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::FALCON:
               {
                  s = FalconInput(Minp, calc_def.GetpFalconCalcDef(), input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::ROTATEMOLECULE:
               {
                  s = RotateMoleculeInput(Minp, &calc_def, input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::FREQANA:
               {
                  s = FreqAnaInput(Minp, calc_def.GetpFreqAnaCalcDef(), input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::TRANSFORMPOT:
               {
                  s = TransformPotInput(Minp, calc_def.GetpTransformPotCalcDef(), input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::LHA:
               {
                  s = LhaInput(Minp, calc_def.pLhaCalcDef(), input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::NORMALCOORDINATES:
               {
                  s = NormalCoordInput(Minp, calc_def.GetNormalCoordCalcDef(),input_level);
                  already_read = true;
                  break;
               }
               case input::VibCoordScheme::ERROR:
               default:
               {
                  MIDASERROR("VIBCOORDSCHEME is unknown");
                  break;
               }
            }
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a ModSys level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}


