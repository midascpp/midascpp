/**
************************************************************************
* 
* @file                GeneralInputParser.cc
*
* Created:             11-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   GeneralInputParser datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/GeneralInputParser.h"

// using declarations
using std::string;
using std::vector;
using std::make_pair;
using std::ostream;

/**
* Construct a default definition of a  calculation
**/
GeneralInputParser::GeneralInputParser(In aLevel, istream& aIs, map<string,string>* aGlobalMap, string aSpecKey, vector<map<string,string> >* aLocalMap) : mStream(aIs)
{
   mLevel=aLevel;
   mSpecKey=aSpecKey;
   mGlobalMap=aGlobalMap;
   mLocalMap=aLocalMap;
   
   if(!ConstructRawMaps())
      MIDASERROR("An error occured during General input parsing, see above.");
   if(!ConstructRefinedMaps())
      MIDASERROR("An error occured during General input parsing, see above.");
}
/**
* The work horse
**/
bool GeneralInputParser::ConstructRawMaps()
{
   // 1) Identify keywords in each string, loop until next #
   string list;
   In position=mStream.tellg();
   getline(mStream,list);
   bool keep_going=true;
   while(keep_going) {
      // if line is blank, continue
      if(list.find_first_not_of(" ")==list.npos) continue;
      // remove leading blanks
      while(list.at(I_0)==' ') list.erase(I_0,I_1);
      // check for level
      if(list.find("#")==0) { //
         // ascii characters => 0 -> 48
         if((In)list.at(1)-48<=mLevel) {
            break;
         }
      }
      // check for "(" and ")" in string and match
      // those at same level.
      In start=I_0;
      vector< pair<In,In> > par_vec;
      while(list.find("(",start)!=list.npos) {
         In par_start=list.find("(",start);
         In par_count=I_1;
         In par_end=par_start;
         while(par_count>I_0) {
            par_end++;
            if(par_end>=list.size())
               MIDASERROR("Unbalanced number of ( and ) in: "+list);
            char next_c=list.at(par_end);
            if(next_c=='(')
               par_count++;
            else if(next_c==')')
               par_count--;
         }
         start=par_end;
         par_vec.push_back(make_pair(par_start,par_end));
      }
      // delete blanks inside positions of "(" and ")" in string 
      for(In i=I_0;i<par_vec.size();i++) {
         In par_start=par_vec[i].first;
         In par_end=par_vec[i].second;
         while(list.find(" ",par_start)!=list.npos) {
            In blank_pos=list.find(" ",par_start);
            if(blank_pos>par_start && blank_pos<par_end) {
               Mout  << "\tWarning: found \" \" inside key, currently blanks are "
                     << "not supported as delimiters inside keys." << endl;
               list.erase(blank_pos,I_1);
               par_end--;
               par_vec[i].second--;
               par_start=blank_pos-I_1;
            }
            else if(blank_pos<par_start)
               par_start=blank_pos;
            else
               break;
         }
      }
      // We are now in position to get all key value
      // pairs from the string, separated by " "
      vector<string> key_val_pairs=SplitString(list," ");
      for(In i=0;i<key_val_pairs.size();i++) {
         // key is everything until "="
         In key_end=key_val_pairs[i].find("=");
         if(key_end==key_val_pairs[i].npos) {
            MIDASERROR("Required input: key=value, offending entry: "+key_val_pairs[i]);
         }
         string key=key_val_pairs[i].substr(I_0,key_end);
         string val;
         GetValueFromKey<string>(key_val_pairs[i],key,val);
         transform(key.begin(),key.end(),key.begin(),(In(*) (In)) toupper);
         // Treat specials
         if(StringNoCaseCompare(key,mSpecKey))
            mSpecials.push_back(val);
         else
            mGlobalMap->insert(make_pair(key,val));
      }
      // get new line, but remember current position
      position=mStream.tellg();
      getline(mStream,list);
   }

   // We have a line beginning with "#", return to old position
   mStream.seekg(position);
   return true;
}
/**
* The other work horse
**/
bool GeneralInputParser::ConstructRefinedMaps()
{
   for(In i=0;i<mSpecials.size();i++) {
      map<string,string> local_key_map=(*mGlobalMap);
      // find all key-value pairs in rsp func and
      // add them to local_key_map. The global keywords
      // will be overwritten
      // Do same trick with locating pairs of "(" and ")"
      string spec=mSpecials[i];
      // delete first and last, they are just a ( and ) resp.
      spec.erase(I_0,I_1);
      spec.erase(spec.size()-I_1,I_1);
      vector< pair<In,In> > par_vec;
      In start=I_0;
      while(spec.find("(",start)!=spec.npos) {
         In par_start=spec.find("(",start);
         In par_count=I_1;
         In par_end=par_start;
         while(par_count>I_0) {
            par_end++;
            if(par_end>=spec.size())
               MIDASERROR("Unbalanced number of ( and ) in: "+spec);
            char next_c=spec.at(par_end);
            if(next_c=='(')
               par_count++;
            else if(next_c==')')
               par_count--;
         }
         start=par_end;
         par_vec.push_back(make_pair(par_start,par_end));
      }
      // all "," outside of () pairs are key-val
      // delimiters
      vector<string> key_val_pair;
      start=I_0;
      while(spec.find(",",start)!=spec.npos) {
         In pos=spec.find(",",start);
         start=pos+I_1;
         bool succes=true;
         for(In j=I_0;j<par_vec.size();j++) {
            if(pos>par_vec[j].first && pos<par_vec[j].second) {
               succes=false;
               continue;
            }
         }
         if(succes)
            spec.replace(pos,1," ");
      }
      // a little tedious to reintroduce blanks, but it makes life simple
      vector<string> key_val_pairs=SplitString(spec," ");
      for(In j=I_0;j<key_val_pairs.size();j++) {
         In key_end=key_val_pairs[j].find("=");
         string key=key_val_pairs[j].substr(I_0,key_end);
         string val;
         GetValueFromKey<string>(key_val_pairs[j],key,val);
         transform(key.begin(),key.end(),key.begin(),(In(*) (In))toupper);
         if(local_key_map.find(key)!=local_key_map.end())
            local_key_map[key]=val;
         else
            local_key_map.insert(make_pair(key,val));
      }
      mLocalMap->push_back(local_key_map);
      
      if(gIoLevel > 10)
      {
         Mout << "\tFor spec = \"" << spec << "\" map is:" << endl;
         for(map<string,string>::iterator j=local_key_map.begin();j!=local_key_map.end();j++) {
            Mout << "\t\t" << j->first << " maps to: " << j->second << endl;
         }
      }
   }
   return true;
}
/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const GeneralInputParser& arGenInp)
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   arOut.setf(ios::showpoint);
   midas::stream::ScopedPrecision(16, arOut);
   arOut << "Nothing in GeneralInputParser yet..." << endl;
   return arOut;
}
