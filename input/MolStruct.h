/**
************************************************************************
* 
* @file                MolStruct.h
*
* Created:             18-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*                      Kristian Sneskov (sneskov@chem.au.dk) 
*
* Short Description:   Base class for the input molecular geometry
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MOLSTRUCT_H
#define MOLSTRUCT_H

// Standard Headers
#include <string>
// Standard Headers
#include <vector>
#include <map>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "nuclei/Vector3D.h"
#include "nuclei/Nuclei.h"

using std::vector;
using std::string;
using std::map;

/**
* Construct a definition of a  calculation
* */
class MolStruct
{
   private:
      Vector3D mOrigo;              ///< Origo, ref. point for closest to etc.
      bool   mNucleiExist;
      vector<Nuclei> mNuclei;
      Vector3D mCenOfRefMol;

   public:
      MolStruct();                            

      void GetOrigo(Vector3D& arOrigo) const         { arOrigo=mOrigo; } 
      bool NucleiExist() {return mNucleiExist;} 
      void AddNucleus(Nuclei& arNuc); 
      void GetEndpoints(MidasVector& arEndPoints);
      MidasVector GetNucleusCoordinates(In arINuc) const;
      Vector3D GetNucleusCoordinates3D(In arINuc);
      In NumberOfNuclei() const {return mNuclei.size(); }
      Nb ClosestNuc(string& arAtomLabel, Vector3D arPoint);
      Nb GetCharge(In inuc) {return mNuclei[inuc].Znuc();}

      std::string GetAtomLabel(In arINuc);
};

#endif
