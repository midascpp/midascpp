/**
************************************************************************
*
* @file                GlobalOperatorDefinitions.cc
*
* Created:             16-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for holding operator definitions and the
*                      lists of operators and modes
*
* Last modified: Thu Oct 29, 2009  11:57AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "input/GlobalOperatorDefinitions.h"
#include <vector>
#include <string>
#include "operator/OneModeOper.h"
#include "operator/FunctionOneModeOper.h"
#include "operator/RspTransOneModeOper.h"
#include "operator/SimpleOneModeOper.h"
#include "operator/StateTransferOneModeOper.h"
#include "operator/DeltaFunctionOneModeOper.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OpInfo.h"

/**
 *
 **/
GlobalModeNr GlobalOperatorDefinitions::ModeNr
   (  const std::string& aMode
   ,  bool aAddIfNotFound
   )
{
   std::map<std::string, GlobalModeNr>::iterator it = mModeMap.find(aMode);
   if (it != mModeMap.end())
   {
      return it->second;
   }
   else if  (  aAddIfNotFound
            )
   {
      mModeLabels.push_back(aMode);
      mModeMap.insert(std::make_pair(aMode, mNmodes));

      return mNmodes++;
   }
   else
   {
      MIDASERROR("GlobalOperatorDefinitions::mModeMap does not contain '" + aMode + "'.");
      return -I_1;
   }
}

/**
 *
 **/
GlobalOperNr GlobalOperatorDefinitions::AddRspOperToken
   (  LocalOperNr aLocalOper
   )
{
   for(In i = I_0; i < mOneModeOpers.size(); ++i)
   {
      if(mOneModeOpers[i]->GetOperNrForResp() == aLocalOper)
         return i;
   }
   mOneModeOpers.push_back(std::make_unique<RspTransOneModeOper<Nb>>(aLocalOper));
   return mOneModeOpers.size() - I_1;
}

/**
 * Add a one-mode operator to the set of one-mode operators
 *
 * @param aFuncStr
 *    A string that contains one line of operators, as found in the .mop files.
 * @param aIsKinOper
 *    Indicates if the operator belongs to the kinetic energy
 * @param arStateTransfer
 *    Set to true if this is a state-transfer operator
 * @param aFC
 *    A container for the function and its parameters
**/
GlobalOperNr GlobalOperatorDefinitions::AddOneModeOper
   (  const std::string& aFuncStr
   ,  const bool& aIsKinOper
   ,  bool& arStateTransfer
   ,  const FunctionContainer<Nb>& aFC
   )
{
   if (gOperIoLevel > I_8)
   {
      Mout << "  Processing string: " << aFuncStr << std::endl;
   }

   auto size = aFuncStr.size();
   if  (  aFuncStr.substr(aFuncStr.find_first_not_of(" "), 1) == std::string("|")
       && aFuncStr.substr(aFuncStr.find_last_not_of(" "), 1) == std::string("|")
       )
   {
      // Check if the string makes sense
      MidasAssert(size >= 6, "At least 6 characters should be required for representing a state-transfer operator. aFuncStr = " + aFuncStr);

      // Set bool
      arStateTransfer = true;

      // Extract indices
      auto i_start = 1;
      auto i_end = aFuncStr.find_first_of(">");
      auto j_start = i_end + 2;
      auto j_end = size - 1;

      In i = midas::util::FromString<In>(aFuncStr.substr(i_start, i_end-i_start));
      In j = midas::util::FromString<In>(aFuncStr.substr(j_start, j_end-j_start));

      // Create and check if already included
      auto new_oper = std::make_unique<StateTransferOneModeOper<Nb>>(i, j);
      for (In i = I_0; i < mOneModeOpers.size(); ++i)
         if (new_oper->Compare(mOneModeOpers[i]))
            return i;

      // Add the new one-mode operator
      mOneModeOpers.emplace_back(std::move(new_oper));

      // Return oper nr.
      return mOneModeOpers.size() - I_1;
   }
   // Else, if we have a delta function
   else if  (  aFuncStr.find("DELTA(") != std::string::npos
            )
   {
      auto delta = aFuncStr;
      In rder = I_0;
      bool lder = false;
      // Figure out if any derivatives are present which order they have
      GetDerOrdersForOneModeOper(delta, rder, lder);
      auto newsize = delta.size();

      Mout  << "New string after determining derivative orders: " << delta << std::endl;

      // Check if the string makes sense
      MidasAssert(newsize >= 8, "At least 8 characters should be required for representing a delta-function operator. aFuncStr = " + aFuncStr);

      // Extract position of delta function
      auto x_start = 6;
      auto x_end = delta.find_first_of(")");
      MidasAssert(x_end != std::string::npos, "Did not find end parenthesis in DELTA function.");
      auto x = midas::util::FromString<Nb>(delta.substr(x_start, x_end-x_start));

      // Create and check if already included
      auto new_oper = std::make_unique<DeltaFunctionOneModeOper<Nb>>(rder, lder, x);
      for (In i = I_0; i < mOneModeOpers.size(); ++i)
         if (new_oper->Compare(mOneModeOpers[i]))
            return i;

      // Add the new one-mode operator
      mOneModeOpers.emplace_back(std::move(new_oper));

      // Return oper nr.
      return mOneModeOpers.size() - I_1;
   }
   // Else, we first check for simple polynomial operator and otherwise use general functions
   else
   {
      std::string temp_func = aFuncStr;
      In rder = I_0;
      bool lder = false;
      // Figure out if any derivatives are present which order they have
      GetDerOrdersForOneModeOper(temp_func, rder, lder);

      if (gOperIoLevel > I_8)
      {
         Mout << "  String after determining derivative orders: " << temp_func << std::endl;
      }

      // Figure out the power in which a polynomial operator is raised to
      In pow = GetPolyPow(temp_func);

      if (gOperIoLevel > I_8)
      {
         Mout << "  String after determining polynomial orders: " << temp_func << std::endl;
      }

      // If nothing now remains of the original string representing the one-mode operator, then it is a standard polynomial operator with/without derivatives (Coriolis coupling terms) or a simple derivative term
      if (!temp_func.length())
      {
         return AddOneModeOper(pow, rder, lder, aIsKinOper);
      }
      else
      {
         std::unique_ptr<OneModeOperBase<Nb>> new_oper = std::make_unique<FunctionOneModeOper<Nb>>(temp_func, rder, lder, aFC, aIsKinOper);

         bool isnew = true;
         In o_m_op_store;

         // Check if the operator is already included, and if so return its GlobalOperNr
         for (In i = I_0; i < mOneModeOpers.size(); i++)
            if (new_oper->Compare(mOneModeOpers[i]))
               return i;

         // Add the new one-mode operator
         mOneModeOpers.push_back(std::move(new_oper));

         return mOneModeOpers.size() - I_1;
      }
   }
}

/**
 *
**/
In GlobalOperatorDefinitions::GetPolyPow
   (  std::string& aFuncStr
   )  const
{
   if (I_0 == aFuncStr.find("Q^"))
   {
      std::string rest(aFuncStr.substr(2));
      if (std::string::npos == rest.find_first_not_of("0123456789"))
      {
         aFuncStr.erase();
         return std::atoi(rest.c_str());
      }
   }
   return 0;
}

/**
 * Determine the order and placement of differential operators and write these specification to the list of operators. First transform to one common Midas internal operator format.
 *
 * @param aFuncStr
 *    A string that contains one line of operators, as found in the .mop files.
 * @param aRDer
 *    An integer specifying the order of the derivative to the right of the function operator f(Q).
 * @param aLDer
 *    A bool which specify that there is a derivative to the left of the operator f(Q). Note that this is always a first order derivative.
 **/
void GlobalOperatorDefinitions::GetDerOrdersForOneModeOper
   (  std::string& aFuncStr
   ,  In& aRDer
   ,  bool& aLDer
   )  const
{
   // Transform (DDQ)^1*f(Q)
   if (aFuncStr.find("(DDQ)^1*") != std::string::npos)
   {
      const std::string InputStr = "(DDQ)^1*";
      const std::string MidasStr = "DDQ ";
      size_t StartPos = I_0;

      while ((StartPos = aFuncStr.find(InputStr, StartPos)) != std::string::npos)
      {
         aFuncStr.replace(StartPos, InputStr.length(), MidasStr);
         StartPos += MidasStr.length();
      }
   }

   // Transform f(Q)*(DDQ)^i
   if (aFuncStr.find("*(DDQ)^") != std::string::npos)
   {
      const std::string input_str = "*(DDQ)^";
      const std::string midas_str = " DDQ^";
      size_t str_pos = I_0;

      while ((str_pos = aFuncStr.find(input_str, str_pos)) != std::string::npos)
      {
         aFuncStr.replace(str_pos, input_str.length(), midas_str);
         str_pos += midas_str.length();
      }
   }

   // Transform regular/simple (DDQ)^i to DDQ^i
   if (aFuncStr.find("(DDQ)^") != std::string::npos)
   {
      const std::string input_str = "(DDQ)^";
      const std::string midas_str = "DDQ^";
      size_t str_pos = I_0;

      while ((str_pos = aFuncStr.find(input_str, str_pos)) != std::string::npos)
      {
         aFuncStr.replace(str_pos, input_str.length(), midas_str);
         str_pos += midas_str.length();
      }
   }

   // Determine the type of differential (Coriolis) operator and set derivative identifiers
   bool has_ddq = false;
   bool has_ddq_pow = false;
   if (aFuncStr.find("DDQ^") != std::string::npos)
   {
      has_ddq_pow = true;
   }

   if (aFuncStr.find("DDQ ") != std::string::npos)
   {
      has_ddq = true;
   }

   // If the term is of type f(Q), i.e. contains no differential operators
   if (!has_ddq && !has_ddq_pow)
   {
      aRDer = I_0;
      aLDer = false;
   }
   // If the term is of type (DDQ)^i or f(Q) (DDQ)^i
   else if (!has_ddq && has_ddq_pow)
   {
      size_t ddq_start_pos = I_0;
      ddq_start_pos = aFuncStr.find("DDQ^");
      if (aFuncStr.substr(ddq_start_pos, I_4) == "DDQ^")
      {
         auto oper_vec_str = midas::util::StringVectorFromString(aFuncStr);
         auto ddq_pow_str = oper_vec_str.back().substr(I_4, oper_vec_str.back().size());

         if (ddq_pow_str.find_first_not_of("0123456789") == std::string::npos)
         {
            aRDer = atoi(ddq_pow_str.c_str());
            aLDer = false;

            if (oper_vec_str.size() == I_1)
            {
               if (gOperIoLevel > I_8)
               {
                  Mout << "  Found a non-Coriolis differential operator " << std::endl;
               }

               aFuncStr = "";
            }
            else
            {
               aFuncStr = oper_vec_str.front();
            }
         }
         else
         {
            MIDASERROR("Not inputting term correctly, the following is left : " + aFuncStr);
         }
      }
      else
      {
         MIDASERROR("The derivative is matched with something it should not be matched with " + aFuncStr);
      }
   }
   // If the term is of type (DDQ)^1 f(Q) or (DDQ)^1 f(Q) (DDQ)^i
   else if (has_ddq)
   {
      size_t ddq_start_pos = I_0;
      ddq_start_pos = aFuncStr.find("DDQ");
      if (aFuncStr.substr(ddq_start_pos, I_3) == "DDQ")
      {
         aLDer = true;
         aRDer = I_0;

         auto oper_vec_str = midas::util::StringVectorFromString(aFuncStr);

         if (oper_vec_str.size() == I_2)
         {
            aFuncStr = oper_vec_str.back();
         }
         else
         {
            aFuncStr = oper_vec_str[I_1];
            for (In i = I_2; i < oper_vec_str.size(); i++) // Might not need to go into here
            {
               aFuncStr += " " + oper_vec_str[i];
            }
         }
      }

      if (has_ddq_pow)
      {
         size_t ddq_pow_start_pos = I_0;
         ddq_pow_start_pos = aFuncStr.find("DDQ^");
         if (aFuncStr.substr(ddq_pow_start_pos, I_4) == "DDQ^")
         {
            auto oper_vec_str = midas::util::StringVectorFromString(aFuncStr);
            auto ddq_pow_str = oper_vec_str.back().substr(I_4, oper_vec_str.back().size());

            if (ddq_pow_str.find_first_not_of("0123456789") == std::string::npos)
            {
               aRDer = atoi(ddq_pow_str.c_str());
            }
            aFuncStr = oper_vec_str.front();
         }
      }
   }
   else
   {
      MIDASERROR("I was told to find derivatives but something went wrong for term: " + aFuncStr);
   }
}

/**
 *
**/
GlobalOperNr GlobalOperatorDefinitions::AddOneModeOper
   (  const In& aPolyOrd
   ,  const In& aRDer
   ,  const bool& aLDer
   ,  const bool& aIsKinOper
   )
{
   std::unique_ptr<OneModeOperBase<Nb>> new_oper = std::make_unique<SimpleOneModeOper<Nb>>(aPolyOrd, aRDer, aLDer, aIsKinOper);

   // Check if we already have the operator, and if so return its GlobalOperNr
   for (In i = I_0; i < mOneModeOpers.size(); ++i)
      if (new_oper->Compare(mOneModeOpers[i]))
         return i;
   //Mout << "Adding new operator " << *new_oper << std::endl;
   //There is a new one mode operator
   mOneModeOpers.push_back(std::move(new_oper));
   return mOneModeOpers.size() - I_1;
}

/**
 *
**/
void GlobalOperatorDefinitions::AddToOpInfo
   (  const std::string& aOperName
   ,  std::string& aOpInfoString
   )
{
   std::pair<std::map<std::string, OpInfo>::iterator, bool> is_in = mOpInfo.insert(std::make_pair(aOperName, OpInfo()));

   if (!is_in.second)
   {
      MIDASERROR("Operator " + aOperName + "Already has one OpInfo related to it...");
   }
   ParseSetInfoLine(aOpInfoString, is_in.first->second);
}

/**
 *
**/
OpInfo& GlobalOperatorDefinitions::GetOpInfo(const string& aOper)
{
   map<string,OpInfo>::iterator it = mOpInfo.find(aOper);
   if(it == mOpInfo.end())
      MIDASERROR("Operator "+aOper+" does not have a OpInfo related to it...");
   return mOpInfo.find(aOper)->second;
}

/**
 *
**/
GlobalOperNr GlobalOperatorDefinitions::MultiplyOpers(GlobalOperNr aOper1, GlobalOperNr aOper2)
{
   std::unique_ptr<OneModeOperBase<Nb>> new_oper = mOneModeOpers[aOper1]->Multiply(mOneModeOpers[aOper2]);

   //Check if the operator already exists, and if so return its GlobalOperNr
   for(In i = I_0; i < mOneModeOpers.size(); i++)
      if(new_oper->Compare(mOneModeOpers[i]))
         return i;
   //If we have a new operater we add it and return the number..
   mOneModeOpers.push_back(std::move(new_oper));
   return mOneModeOpers.size() - I_1;
}

/**
 *
 **/
OpDef& GlobalOperatorDefinitions::GetOperator(const std::string& aName)
{
   for(auto& opdef : mOperators)
   {
      if(opdef.Name() == aName)
      {
         return opdef;
      }
   }

   // If not found we throw MIDASERROR.
   MIDASERROR("OpDef not found.");

   // Will never get here
   return mOperators[0];
}

/**
 *
 **/
const OpDef& GlobalOperatorDefinitions::GetOperator(const std::string& aName) const
{
   for(const auto& opdef : mOperators)
   {
      if(opdef.Name() == aName)
      {
         return opdef;
      }
   }

   // If not found we throw MIDASERROR.
   MIDASERROR("OpDef not found.");

   // Will never get here
   return mOperators[0];
}

/**
 *
**/
In GlobalOperatorDefinitions::GetOperatorNr(const std::string& aString) const
{
   //Mout << " GetOperatorNr " << aString << endl;
   auto it = mOperatorNrs.find(aString);
   if(it == mOperatorNrs.end())
   {
      //Mout << " not found " << endl;
      return -1;
   }
   //Mout << " found " << endl;
   return it->second;
}

/**
 *
**/
void GlobalOperatorDefinitions::ReSet()
{
   mModeLabels.clear();
   mModeMap.clear();
   mNmodes=I_0;
   mOneModeOpers.clear();
   mOperators.clear();
   mOperatorNrs.clear();
   mOpInfo.clear();
}

/**
 *
 **/
const std::unique_ptr<OneModeOperBase<Nb>>& GlobalOperatorDefinitions::GetOneModeOper
   (  GlobalOperNr aIn
   )  const
{
   try
   {
      return mOneModeOpers.at(aIn);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      return mOneModeOpers.front(); // To quench compiler warning.
   }
}
