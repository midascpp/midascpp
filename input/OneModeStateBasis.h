/**
************************************************************************
* 
* @file                OneModeStateBasis.h
*
* Created:             04-10-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contain definition of state basis {|I>} for one mode
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONEMODESTATEBASIS_H_INCLUDED
#define ONEMODESTATEBASIS_H_INCLUDED

#include <string>
#include "inc_gen/TypeDefs.h"

/**
 *  Simply a class for containing the number of basis functions in a mode that uses state-transfer operators
 **/
class OneModeStateBasis
{
   private:
      //! Mode number that this basis corresponds to
      GlobalModeNr mMode = -I_1;

      //! Number of basis functions
      In mNBas = I_0;

      //! Aux info
      std::string mAux = "";

   public:
      //! c-tor from number of basis functions
      OneModeStateBasis
         (  GlobalModeNr aMode
         ,  In aNBas
         ,  const std::string& aAux = ""
         )
         :  mMode
               (  aMode
               )
         ,  mNBas
               (  aNBas
               )
         ,  mAux
               (  aAux
               )
      {
      }

      //! Get number of basis functions
      In NBas
         (
         )  const
      {
         return this->mNBas;
      }

      //! Get mode nr
      GlobalModeNr GetGlobalModeNr
         (
         )  const
      {
         return this->mMode;
      }

      //! Get type
      std::string Type
         (
         )  const
      {
         return "StateBasis";
      }

      //! Get Aux info.
      const std::string& Aux
         (
         )  const
      {
         return this->mAux;
      }
};

#endif /* ONEMODESTATEBASIS_H_INCLUDED */
