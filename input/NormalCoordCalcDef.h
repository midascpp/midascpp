/*
************************************************************************
*  
* @file                NormalCoordCalcDef.h
*
* Created:             08-07-2021
*
* Author:              Bo Thomsen (DrBoThomsen@gmail.com)
*
* Short Description:   CalcDef for generating normal coordinates
*                      given a hessian in the molecule input. 
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NORMALCOORDCALCDEF_H_INCLUDED
#define NORMALCOORDCALCDEF_H_INCLUDED

#include <string>
#include "input/Macros.h"

namespace midas::input
{

/**
 * Class for holding the input variables for generating
 * normalcoordinates from a precalculated hessian.
 **/
class NormalCoordCalcDef
{
   CREATE_VARIABLE(In, ProjectOutTransRot, I_1);
   CREATE_VARIABLE(bool, CheckFrequencies, true);
   CREATE_VARIABLE(bool, IgnoreGradient, false);
   CREATE_VARIABLE(std::string, FileName, std::string{"NormCoord.mmol"});
};

} // namespace midas::input

#endif /* NORMALCOORDCALCDEF_H_INCLUDED */
