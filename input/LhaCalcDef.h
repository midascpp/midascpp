/*
************************************************************************
*  
* @file                LhaCalcDef.h
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   CalcDef for Local Harmonic Approximation.
*                      I.e. tools for diagonalizing and transforming 
*                      harmonic potentials (and writing mop files). 
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LHACALCDEF_H_INCLUDED
#define LHACALCDEF_H_INCLUDED

#include "input/Macros.h"

#include <string>

namespace midas::input
{

/**
 *
 **/
class LhaCalcDef
{
   //! Reference system (e.g. different electronic state at same geometry)
   CREATE_VARIABLE(std::string, ReferenceSystem, std::string{""});
   
   //! Project out translation and rotation (how many iterations)
   CREATE_VARIABLE(In, ProjectOutTransRot, I_1);
   
   //! Write bounds for transformed potential using turning points for this HO function
   CREATE_VARIABLE(In, WriteBounds, I_10);
   
   //! Check Hessian for negative eigenvalues
   CREATE_VARIABLE(bool, CheckHessian, false);
   
   //! Ignore gradient in mop files and bounds
   CREATE_VARIABLE(bool, IgnoreGradient, false);
};

} // namespace midas::input

#endif /* LHACALCDEF_H_INCLUDED */
