/**
************************************************************************
* 
* @file                NewtonRaphsonInput.cc
*
* 
* @date                12-09-2016
*
* @author              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief               Input for Newton-Raphson method in TensorNlSolver
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <tuple> // for pair
#include <set>
#include <type_traits>

// midas headers
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "libmda/util/any_type.h"

#include "it_solver/nl_solver/NewtonRaphsonInfo.h"

/**
 * Input for Newton-Raphson method in TensorNlSolver
 *
 * @param Minp           Input file stream.
 * @param s              Currently read string.
 * @param nr_info        NewtonRaphsonInfo struct for holding the input information
 * @return               Return whether s should be processed by calling function, 
 *                       i.e. has already beed read, but not processed.
 **/
bool NewtonRaphsonInput
   (  std::istream& Minp
   ,  std::string& s
   ,  NewtonRaphsonInfo& nr_info
   )
{
   // Enum/map with input flags
   enum INPUT {   ERROR, MAXITER, JACOBIANMAXEXCI, NOADAPTIVELINSYSTHRESHOLD
              ,   ADAPTIVETHRESHOLDSCALING, STEPSIZECONTROL
              ,   NOLINSOLVERPRECON, ADAPTIVEMAXEXCI
              };

   const map<string,INPUT> input_word =
   {  {"#4MAXITER", MAXITER}
   ,  {"#4JACOBIANMAXEXCI", JACOBIANMAXEXCI}
   ,  {"#4NOADAPTIVELINSYSTHRESHOLD", NOADAPTIVELINSYSTHRESHOLD}
   ,  {"#4ADAPTIVETHRESHOLDSCALING", ADAPTIVETHRESHOLDSCALING}
   ,  {"#4STEPSIZECONTROL", STEPSIZECONTROL}
   ,  {"#4NOLINSOLVERPRECON", NOLINSOLVERPRECON}
   ,  {"#4ADAPTIVEMAXEXCI", ADAPTIVEMAXEXCI}
   };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s,4)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case MAXITER:
         {
            midas::input::GetLine(Minp,s);
            nr_info.mMaxIter = midas::util::FromString<In>(s);
            break;
         }
         case JACOBIANMAXEXCI:
         {
            midas::input::GetLine(Minp,s);
            nr_info.mJacobianMaxExci = midas::util::FromString<In>(s);
            break;
         }
         case ADAPTIVEMAXEXCI:
         {
            nr_info.mAdaptiveMaxExci = midas::input::GetNb(Minp, s);
            break;
         }
         case NOADAPTIVELINSYSTHRESHOLD:
         {
            nr_info.mAdaptiveLinSysThreshold = false;
            break;
         }
         case ADAPTIVETHRESHOLDSCALING:
         {
            midas::input::GetLine(Minp,s);
            nr_info.mAdaptiveThresholdScaling = midas::util::FromString<Nb>(s);
            break;
         }
         case STEPSIZECONTROL:
         {
            midas::input::GetLine(Minp,s);
            nr_info.mStepSizeControl = midas::util::FromString<Nb>(s);
            break;
         }
         case NOLINSOLVERPRECON:
         {
            nr_info.mDiagonalPreconditioner = false;
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a Newton-Raphson input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // we have read, but not processed next keyword
   return true;
}
