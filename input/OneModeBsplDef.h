/**
************************************************************************
* 
* @file                OneModeBsplDef.h
*
* Created:             30-08-2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode B-spline basis set
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ONEMODEBSPLDEFS_H
#define ONEMODEBSPLDEFS_H

// Standard Headers
#include <string>
using std::string;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

class OneModeBsplDef
{
   private:
      
      //! Global Mode number 
      GlobalModeNr mImode;                  
      
      //! Basis type = "Bspline" etc. 
      std::string mBasType;
      
      //! Further Basis definition
      std::string mBasAuxDefs;
      
      //! The grid of knots defining the spline functions
      MidasVector mGridOfKnots;
      
      //! Array for holding the index of the last spline not zero on a given interval 
      std::vector<In> mLastInd;
      
      //! Total nr. of basis functions
      In mNbas;
      
      //! Order of the spline functions
      In mIord;
      
      //! Density of the B-spline
      Nb mBasDens;
   
   public:
      
      //! constructor as "zero"
      OneModeBsplDef();                                     
      
      //! constructor
      OneModeBsplDef(In aMode, std::string aType, std::string aAux);  
      
      //! constructor
      OneModeBsplDef
         (  const In& aMode
         ,  const std::string& aType
         ,  const Nb& aLeft
         ,  const Nb& aRight
         ,  const In& aIord
         ,  const In& aNbas
         ,  const bool& aKeepUnboundedBsplines
         );
                                                                    
      In Nbas() const {return mNbas; }                         ///< Get # of basis functions
      In Nord() const {return mIord; }                         ///< Get the order of the B-spline function
      
      //! Get the nr. of intervals in the B-spline grid
      const In Nint() const;
      
      //! return the knot value for the aIndx knot of the grid
      const Nb GetKnotValue (const In& aInd) const {return mGridOfKnots[aInd];}
      
      //! Return the index of the last spline not zero over the aInt interval
      const In GetLastInd(const In& aInt) const       {return mLastInd[aInt];}

      //! Get the vector of knots along the normal coordinate
      void GetGrid(MidasVector& arVec) const 
      {
         arVec.SetNewSize(mGridOfKnots.Size());
         arVec = mGridOfKnots;
      }

      In Nknots() const {return mGridOfKnots.Size();}              ///< Get the size of the grid of knots
      void SetGrid(MidasVector& arVec) {mGridOfKnots=arVec; }  ///< Get the vector of knots along the normal coordinate

      Nb GetBasDens() const {return mBasDens;}                 ///< Get the "density" of the one mode basis
      GlobalModeNr GetGlobalModeNr() const  {return mImode;}   ///< Get Global Mode number
      string Type() const {return mBasType;}                   ///< Get Type
      string Aux() const {return mBasAuxDefs;}                 ///< Get Aux info 
      void ChangeModeNr(In aGlobalModeNr) {mImode = aGlobalModeNr;} ///< Change mode number
      
      //! Generate the grid of knots
      void GenGrid(const Nb& aLeft, const Nb& aRight, const In& aNint);
      
      //! Construct the mLastIndx array
      void Bknots();

      //! Evaluate the B-spline basis in a given point of the support
      Nb EvalSplineBasis(const In& aIbas, const Nb& aQval, const bool& aKeepUnboundedBsplines) const;

      //! Evaluate the B-spline basis in a set of points of the support
      void EvalSplineBasis(const In& aIbas, const MidasVector& aQval, MidasVector&, const bool& aKeepUnboundedBsplines) const;
};

   //! Generate value of all B-spline at a site common to their support
   void Bsplvb(MidasVector& t, const In& jhigh, const In& index, const Nb& x, const In& left, MidasMatrix& biatx);

   //! Generate value and derivatives of all B-splines at a site common to their support
   void Bsplvd(MidasVector& t, const In& k, const Nb& x, const In& left, MidasMatrix& a, MidasMatrix& dbiatx, const In& nderiv);

#endif
