/**
************************************************************************
* 
* @file                OneModeBsplDef.cc
*
* Created:             08-30-2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Contain definition of one-mode B-spline basis
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::transform;

#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OneModeBsplDef.h"
#include "inc_gen/Const.h"

typedef string::size_type Sst;

/**
 * Default constructor
**/
OneModeBsplDef::OneModeBsplDef()
   :  mGridOfKnots()
{
   mImode         = -I_1;
   mBasType       = ""; 
   mBasAuxDefs    = "";
   mNbas          = I_0;
   mIord          = I_0;
   mBasDens       = C_0;
}

/**
 * Constructor
**/
OneModeBsplDef::OneModeBsplDef
   (  const In& aMode
   ,  const std::string& aType
   ,  const Nb& aLeft
   ,  const Nb& aRight
   ,  const In& aIord
   ,  const In& aNbas
   ,  const bool& aKeepUnboundedBsplines
   ) 
   :  mGridOfKnots()
{
   mImode = aMode;
   mBasType = aType; 
   mBasAuxDefs = "";
   mIord = aIord;
   mNbas = aNbas;

   if (mNbas <= I_0)
   {
      MIDASERROR(" Number of basis functions is zero or negative in OneModeBsplDef constructor!");
   }

   // Determine the B-spline basis density
   mBasDens = mNbas/std::fabs(aRight - aLeft);

   In n_int = I_0;
   // Define the number of intervals for the break-point sequence
   if (aKeepUnboundedBsplines)
   {
      n_int = aNbas - aIord + I_1;
   }
   // If the first and last B-splines are to be disregarded later when the EvalSplineBasis function is invoked, then we make sure to include two additional B-splines
   else
   {
      n_int = aNbas - aIord + I_3;
   }

   if (n_int < I_0)
   {
      MIDASERROR("There are no B-spline basis functions to be generated for mode Q" + std::to_string(aMode) + ". \n This issue can be fixed by either increasing the number of B-splines/B-spline density or decreasing the B-spline order");
   }

   // Generate the knot sequence
   GenGrid(aLeft, aRight, n_int);

   // Determine position of first/last non-zero B-splines 
   Bknots();

   if (gDebug || gBasisIoLevel > I_7)
   {
      // List the B-spline function parameters
      Mout.setf(ios::scientific);
      Mout.setf(ios::uppercase);            
      midas::stream::ScopedPrecision(22, Mout);
      Mout << " Parameters obtained for one-mode B-spline construction: " << std::endl;
      Mout << "  Mode number:            " << aMode << std::endl;
      Mout << "  Type:                   " << aType << std::endl;
      Mout << "  Order of the B-splines: " << aIord << std::endl;
      Mout << "  Number of B-splines:    " << aNbas << std::endl;
      Mout << "  Left basis bound:       " << aLeft << std::endl;
      Mout << "  Right basis bound:      " << aRight << std::endl;
      Mout << "  Number of intervals:    " << Nint() << std::endl;
      Mout << "  Number of knots:        " << mGridOfKnots.Size() << std::endl;
      Mout << " Grid of knots generated for mode Q" << aMode << ": " << std::endl;
      Mout << mGridOfKnots << std::endl;
   } 
}

/**
 * Constructor from Aux
**/
OneModeBsplDef::OneModeBsplDef
   (  In aMode
   ,  std::string aType
   ,  std::string aAux
   )
   :  mGridOfKnots()
{
   mImode = aMode;
   mBasType=aType; 
   //mNbas=0;
   //mIord=0;
   //mNdel=0;

   transform(aAux.begin(),aAux.end(),aAux.begin(),(In(*) (In))tolower);  // Transform tolower
   mBasAuxDefs = aAux;
   //debug
   if (gDebug) Mout << " auxiliary definitions: " << aAux << endl;

   //then extract infos from the aAux string
   vector<string> aux_st_vec;

   // Go backwards and find labels;
   do {
      string piece;
      Sst cfind = aAux.rfind(",");
      if (cfind!=aAux.npos) piece = aAux.substr(cfind);
      if (cfind==aAux.npos) piece = aAux;  // No , found take it all.
      Sst l_p = piece.size();
      while(piece.find(",")!= piece.npos) piece.erase(piece.find(","),I_1);
      aux_st_vec.push_back(piece);
      if (cfind!=aAux.npos) aAux.erase(cfind,l_p);
      if (cfind==aAux.npos) aAux.erase(I_0,l_p);
      //debug
      if (gDebug) Mout << " in loop piece = " << piece << " and aAux " << aAux << endl;
   } while (aAux.size()>0);

   Nb leftb=C_0;
   Nb rightb=C_0;
   In iord=I_0;
   //Nb n_del=C_0;
   In n_int=I_0;
   In n_bas=I_0;
   Nb b_dens=C_0;
   for (In i=0;i<aux_st_vec.size();i++)
   {
      if (aux_st_vec[i].find("leftb=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("leftb"),aux_st_vec[i].find("=")+1);
         leftb=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " left radial endpoint found to be: "  << leftb << endl;
      }
      if (aux_st_vec[i].find("rightb=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("rightb"),aux_st_vec[i].find("=")+1);
         rightb=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " right radial endpoint found to be: "  << rightb << endl;
      }
      else if (aux_st_vec[i].find("iord=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("iord"),aux_st_vec[i].find("=")+1);
         iord=InFromString(aux_st_vec[i]);
         if (gDebug) Mout << " iord found to be: "  << iord << endl;
      }
      else if (aux_st_vec[i].find("nbas=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("nbas"),aux_st_vec[i].find("=")+1);
         n_bas=InFromString(aux_st_vec[i]);
         if (gDebug) Mout << " n_bas found to be: "  << n_bas << endl;
      }
      else if (aux_st_vec[i].find("dens=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("dens"),aux_st_vec[i].find("=")+1);
         b_dens=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " dens found to be: "  << b_dens << endl;
      }
      else
      {
         Mout << " No useful information found in basis set auxiliary input:" << aux_st_vec[i] <<endl;
      }
   }
   if ((rightb == C_0 && leftb == C_0) || rightb == leftb)
   {
      MIDASERROR(" No proper boundary given ");
   }
   if (leftb > rightb) //swap them
   {
      Nb tmp = rightb;
      rightb = leftb;
      leftb = tmp;
   }
   if (b_dens == C_0 && n_bas == I_0) 
   {
      MIDASERROR(" No proper no. of basis function given ");
   }
   else if (b_dens != C_0 && n_bas != I_0)
   {
      MIDASERROR(" Too much information provided: both n_bas and n_dens ");
   }

   // Construct from dens
   if (b_dens != C_0)
   {
      In n_bas = In(std::fabs(rightb - leftb) * b_dens + C_I_2);
      mBasDens = b_dens;                              
      mNbas = n_bas;
      n_int = n_bas - iord + I_3; //delete automatically first and last B-spline
      Mout << " for b_dens: " << b_dens << " n_bas: " << n_bas << " n_int: " << n_int << endl;
   }
   // Construct from number of basis functions
   else if (n_bas != I_0)
   {
      mNbas = n_bas;
      b_dens = n_bas/std::fabs(rightb - leftb);
      mBasDens = b_dens;
      n_int = n_bas - iord + I_3;
   }
   mIord = iord;
   
   // Construct the linear grid and index vector
   if (n_int < C_0)
   {
      MIDASERROR("No basis functions inside grid bounds. Increase either number of functions, function density or decrease order!");
   }
   GenGrid(leftb, rightb, n_int);
   Bknots();
   //mNdel=n_del;
   //list the parameters.
   if (gDebug || gBasisIoLevel > I_7)
   {
      Mout.setf(ios::scientific);
      Mout.setf(ios::uppercase);
      midas::stream::ScopedPrecision(22, Mout);
      Mout << " Parameters in OneModeBsplDef constructor " << endl;
      Mout << " Mode nr.: " << aMode << endl;
      Mout << " Type: " << aType << endl;
      Mout << " Order of the B-splines " << iord << endl;
      Mout << " Left bound: " << leftb << endl;
      Mout << " Right bound: " << rightb << endl;
      Mout << " Nr. of basis functions: " << mNbas << endl;
      Mout << " Density of the B-spline basis: " << mBasDens << endl;
      Mout << " Nr. of knots generated: " << mGridOfKnots.Size() << endl; 
      Mout << " Nr. of intervals: " << Nint() << endl;
      Mout << " Grid of knots generated: " << endl << endl;
      Mout << mGridOfKnots << endl;
   }
}
/**
 * Set up the linear grid of knots
**/
void OneModeBsplDef::GenGrid
   (  const Nb& aLeft
   ,  const Nb& aRight
   ,  const In& aNint
   )
{
   // Vector to hold the set of breakpoints
   std::vector<Nb> bps;

   // Build an equidistant/uniform breakpoint sequence by having a constant distance between them
   Nb step = std::fabs(aRight - aLeft) / Nb(aNint); 
   
   // The number of breakpoints is one higher than the number of intervals
   In no_bps = aNint + I_1;

   // Set breakpoints at uniform intervals, starting from the left boundary
   for (In i = I_0; i < no_bps; i++)
   {
      Nb pos = aLeft + step * Nb(i);
      bps.push_back(pos);
   }
   
   // Output the breakpoint sequence if requested
   if (gDebug || gBasisIoLevel > I_7)
   {
      Mout << " Number of breakpoints: " << bps.size() << std::endl;
      Mout << " Breakpoint positions: " << std::endl;
      Mout << "  " << bps << std::endl;
   }

   // Now construct the knot sequence with special boundary conditions
   In no_knots = no_bps + I_2 * mIord - I_2;
   mGridOfKnots.SetNewSize(no_knots);

   // First knot is placed at the left basis boundary
   mGridOfKnots[I_0] = bps[I_0];

   // Add additional knots until there are a total of 'mIord + 1' knots at the left basis boundary. 
   // This is done in order to maximize multiplicity and minize continuity, resulting in discontinous functions at the left basis boundary.
   for (In i = I_1; i < mIord; i++)
   {
      mGridOfKnots[i] = bps[I_0];
   }

   // Add interior knots in uniform sequence  
   for (In i = I_1; i < bps.size(); i++)
   {
      mGridOfKnots[mIord + i - I_1] = bps[i];
   }

   // Add additional knots until there are a total of 'mIord + 1' knots at the right basis boundary. 
   // This is done in order to maximize multiplicity and minize continuity, resulting in discontinous functions at the right basis boundary.
   In k = bps.size() + mIord - I_2;
   for (In i = I_1; i < mIord; i++)
   {
      mGridOfKnots[i + k] = bps[bps.size() - I_1];
   }
}

/**
 * Get the nr. of intervals
**/
const In OneModeBsplDef::Nint() const
{
   In n_int = I_0;
   for (In i = I_0; i < (mGridOfKnots.Size()-mIord); i++)
   {
      if (mGridOfKnots[i + I_1] > mGridOfKnots[i])
      {
         n_int++;
      }
      else if (mGridOfKnots[i + I_1] < mGridOfKnots[i])
      {
         MIDASERROR("Knots sequence out of order ");
      }
   }
   return n_int;
}

/**
* Fill in the mLastInd vector. For interval aInt mLastInd[aInt] gives the
* index of the last B-spline function which is not identically zero over
* the aInt interval. The index of the first spline not equal to zero over interval
* aInt is computed as ibl = mLastInd(aInt) - Iord + 1
***/
void OneModeBsplDef::Bknots()
{
   for (In i = I_1; i <= (mGridOfKnots.Size() - mIord); i++)
   {
      if (mGridOfKnots[i] > mGridOfKnots[i - I_1])
      {
         mLastInd.push_back(i);
      }
      else if (mGridOfKnots[i] < mGridOfKnots[i - I_1])
      {
         MIDASERROR("Knot sequence out of order in Bknots()");
      }
   }
   
   if (gDebug || gBasisIoLevel > I_8)
   {
      Mout << " Number of breakpoints that ends an interval: " << mLastInd.size() << std::endl;
      Mout << " End-of-interval breakpoint indices: " << std::endl;
      Mout << "  " << mLastInd << std::endl;
   }

   return;
}

/**
 * Evaluate the B-spline basis in a given point of the support
 **/
Nb OneModeBsplDef::EvalSplineBasis
   (  const In& aIbas
   ,  const Nb& aQval
   ,  const bool& aKeepUnboundedBsplines
   )  const
{
   MidasVector b_grid;
   GetGrid(b_grid);

   In k_low;
   Nb b_val = C_0;
   In nderiv = I_1;
   In iord = Nord();
   In n_int = Nint();
   MidasMatrix da(iord, C_0);
   MidasMatrix dbi(iord, nderiv, C_0);

   // Run over the intervals
   for (In i_n = I_0; i_n < n_int; i_n++)
   {
      // Get index of the last B-spline that is not zero in the interval i_n 
      In itl = GetLastInd(i_n);
      
      // Get index of the first B-spline that is not zero in the interval i_int
      In ibl = I_0;
      if (aKeepUnboundedBsplines)
      {
         ibl = itl - iord;
      }
      else
      {
         ibl = itl - iord - I_1;
      }
      
      // The lowest order of the B-spline functions is 1
      k_low = I_1;

      // The first interval (specified by the breakpoint sequence) is special in the sense that we want to exclude the first B-spline function as this is unbounded for clamped knot sequences
      if (i_n == I_0 && !aKeepUnboundedBsplines)
      {
         k_low = I_2;
      }

      // Get first and last knot position for the i_n interval
      Nb x1 = GetKnotValue(itl - I_1);
      Nb x2 = GetKnotValue(GetLastInd(i_n));
      
      // The highest order of the B-spline functions is given on input
      In n_dim = iord;

      // The last interval (specified by the breakpoint sequence) is special in the sense that we want to exclude the last B-spline function as this is unbounded for clamped knot sequences
      if (i_n == n_int - I_1 && !aKeepUnboundedBsplines)
      {
         n_dim = iord - I_1;
      }
      
      // Consider only knots for which there are B-splines that have non-zero contributions at the point aQval
      if (aQval >= x1 && aQval <= x2)
      {
         // B-spline inside interval
         if (aIbas >= ibl + k_low - I_1 && aIbas <= ibl + n_dim - I_1) 
         {
            In nderiv = I_1;
            
            // Generate B-spline values and derivatives
            Bsplvd(b_grid, iord, aQval, itl, da, dbi, nderiv);

            // Determine the correct B-spline coefficient value and extract this
            for (In k = k_low; k <= n_dim; k++)
            {
               In k1 = ibl + k - I_1;
               if (k1 == aIbas) 
               {
                  b_val = dbi[k - I_1][I_0];
                  return b_val;
               }
            }
         }
         // B-spline outside interval is set to zero
         else
         {
            b_val = C_0;
            return b_val;
         }
      }
   }

   return b_val;
}

/**
 * Evaluate the B-spline basis in a set of points of the support
 **/
void OneModeBsplDef::EvalSplineBasis
   (  const In& aIbas
   ,  const MidasVector& aQval
   ,  MidasVector& arFuncVals
   ,  const bool& aKeepUnboundedBsplines
   )  const
{
   MidasVector b_grid;
   GetGrid(b_grid);

   // Run over all modes contained in this mode combination
   for (In n_p = I_0; n_p < aQval.Size(); n_p++)
   {
      In k_low;
      In nderiv = I_1;
      In iord = Nord();
      In n_int = Nint();
      MidasMatrix da(iord, C_0);
      MidasMatrix dbi(iord, nderiv, C_0);
     
      // Run over the intervals
      for (In i_n = I_0; i_n < n_int; i_n++)
      {
         // Get index of the last B-spline that is not zero in the interval i_n 
         In itl = GetLastInd(i_n);
         
         // Get index of the first B-spline that is not zero in the interval i_int
         In ibl = I_0;
         if (aKeepUnboundedBsplines)
         {
            ibl = itl - iord;
         }
         else
         {
            ibl = itl - iord - I_1;
         }
         
         //
         k_low = I_1;
         if (i_n == I_0 && !aKeepUnboundedBsplines)
         {
            k_low = I_2;
         }
      
         // Get first and last knot position for the i_n interval
         Nb x1 = GetKnotValue(itl - I_1);
         Nb x2 = GetKnotValue(GetLastInd(i_n));

         //
         In n_dim = iord;
         if (i_n == n_int - I_1 && !aKeepUnboundedBsplines)
         {
            n_dim = iord - I_1;
         }

         // Consider only knots for which there are B-splines that have non-zero contributions at the point aQval
         if (aQval[n_p] >= x1 && aQval[n_p] <= x2)
         {
            // B-spline is inside interval
            if (aIbas >= ibl + k_low - I_1 && aIbas <= ibl + n_dim - I_1)
            {
               In nderiv = I_1;

               // Generate B-spline values and derivatives
               Bsplvd(b_grid, iord, aQval[n_p], itl, da, dbi, nderiv);

               //
               for (In k = k_low; k <= n_dim; k++)
               {
                  In k1 = ibl + k - I_1;
                  if (k1 == aIbas) 
                  {
                     arFuncVals[n_p] = dbi[k - I_1][I_0];
                     break;
                  }
               }
            }
            // B-splines outside the current interval are zero
            else
            {
               arFuncVals[n_p] = C_0;
               break;
            }
         }
      }
   }
}

/**
 * Calculate value and derivatives of all the Spline functions that
 * do not vanish at a given point x
 * Taken from C. De Boor "a Practical Guide to Splines", pag. 252.
 * Or pag. 288, depending on the edition of the book.
**/
void Bsplvd
   (  MidasVector& t
   ,  const In& k
   ,  const Nb& x
   ,  const In& left
   ,  MidasMatrix& a
   ,  MidasMatrix& dbiatx
   ,  const In& nderiv
   )
{
   In mhigh = std::max(std::min(nderiv, k), I_1);
   In kp1 = k + I_1;
   In one = I_1;
   In two = I_2;

   // Generate the B-spline coefficients for the (kp1-mhigh) B-splines of order (kp1-mhigh)
   Bsplvb(t, kp1 - mhigh, one, x, left, dbiatx);

   // Now determine the value of the derivative B-spline coefficients
   if (mhigh != I_1)
   {
      In ideriv=mhigh;
      for (In m=2; m<=mhigh; m++)
      {
         In jp1mid=1;
         for (In j=ideriv; j<=k; j++)
         {
            dbiatx[j-1][ideriv-1]=dbiatx[jp1mid-1][0];
            jp1mid+=1;
         }
         ideriv-=1;
         Bsplvb(t,kp1-ideriv,two,x,left,dbiatx);
      }
      In jlow=1;
      for (In i=1; i<=k; i++)
      {
         for (In j=jlow; j<=k; j++)
         {
            a[j-1][i-1]=C_0;
         }
         jlow=i;
         a[i-1][i-1]=C_1;
      }
      for (In m=2; m<=mhigh; m++)
      {
         In kp1mm=kp1 - m;
         Nb fkp1mm=kp1mm;
         In il=left;
         In i=k;
         for (In ldummy=1; ldummy<=kp1mm; ldummy++)
         {
            Nb factor=fkp1mm/(t[il+kp1mm-1]-t[il-1]);
            for(In j=1; j<=i; j++)
            {
               a[i-1][j-1]=(a[i-1][j-1]-a[i-2][j-1])*factor;
            }
            il-=1;
            i-=1;
         }
         for (In i=1; i<=k; i++)
         {
            Nb sum=C_0;
            In jlow=max(i,m);
            for (In j=1; j<=k-jlow+1; j++)
            {
               sum=a[j-1+jlow-1][i-1]*dbiatx[j-1+jlow-1][m-1]+sum;
            }
            dbiatx[i-1][m-1]=sum;
         }
      }
   }

   return;
}

/**
 * Calculate the value of all possibly non-zero B-splines at point x of order jhigh, (there will be jhigh B-splines that fulfil this). 
 * Knot sequence of points in GridOfKnots.
 * Taken from C. De Boor, "a Practical guide to Splines", pag. 111
 * Or pag. 134 depending on edition of the book.
**/
void Bsplvb
   (  MidasVector& t
   ,  const In& jhigh
   ,  const In& index
   ,  const Nb& x
   ,  const In& left
   ,  MidasMatrix& biatx
   )
{
   // Due to conversion from f77, organize in a tmp container and restore after
   In n_el = biatx.Ncols() * biatx.Nrows();
   MidasVector biatx_tmp(n_el, C_0);
   biatx_tmp.MatrixColByCol(biatx);

   In jmax = I_20;
   static In j = I_1;
   static MidasVector deltal(jmax);
   static MidasVector deltar(jmax);

   if (index == I_1)
   {
      // A B-spline of order j = 1 will have a value of 1 per definition
      j = I_1;
      biatx_tmp[I_0] = C_1;
     
      // If the order is higher than the spcified maximal order then we quit
      if (j >= jhigh) 
      {
         biatx.MatrixFromVector(biatx_tmp, false);
         return;
      }
   }

   // B-spline recurrence relation reformulated into right and left delta quantities, see page 110 in C. de Boor's A Practical Guide to splines, revised edition 
   In jp1 = j + I_1;
   deltar[j - I_1] = t[left + j - I_1] - x;
   deltal[j - I_1] = x - t[left + I_1 - j - I_1];
   Nb saved = C_0;

   // Now forming the B-spline coefficient for the first B-spline of order 1 for use in the recursive relation
   for (In i = I_1; i <= j; i++)
   {
      Nb term = biatx_tmp[i - I_1] / (deltar[i - I_1] + deltal[jp1 - i - I_1]);
      biatx_tmp[i - I_1] = saved + deltar[i - I_1] * term;
      saved = deltal[jp1 - i - I_1] * term;
   }
   biatx_tmp[jp1 - I_1] = saved;
   
   j = jp1;
   // Recursively generate the B-spline functions from order j = 2 to the specified highest order in the B-splines
   while (j < jhigh)
   {
      // B-spline recurrence relation reformulated into right and left delta quantities
      jp1 = j + I_1;
      deltar[j - I_1] = t[left + j - I_1] - x;
      deltal[j - I_1] = x - t[left + I_1 - j  - I_1];
      Nb saved = C_0;

      // Assemble the B-spline coefficients by making use of the previously calculated B-spline coefficients
      for (In i = I_1; i <= j; i++)
      {
         Nb term = biatx_tmp[i - I_1] / (deltar[i - I_1] + deltal[jp1 - i - I_1]);
         biatx_tmp[i - I_1] = saved + deltar[i - I_1] * term;
         saved = deltal[jp1 - i - I_1] * term;
      }
      biatx_tmp[jp1 - I_1] = saved;
      j = jp1;
   }

   biatx.MatrixFromVector(biatx_tmp, false);
   return;
} 
