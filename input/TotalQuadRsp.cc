/**
************************************************************************
* 
* @file                TotalQuadRsp.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TotalQuadRsp datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <exception>
#include <list>

#include "inc_gen/math_link.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/TotalQuadRsp.h"
#include "input/TotalResponseDrv.h"
#include "input/TotalResponseContribution.h"
#include "input/OpInfo.h"

using std::string;
using std::vector;
using std::map;
using std::make_pair;
using std::exception;
using std::list;

#define DBG(ARG) ARG
//#define DBG(ARG) void(0);

static In PRECISION=8;


/**
* Constructor, no action yet...
* */
TotalQuadRsp::TotalQuadRsp(const std::map<string,string>& aM) : TotalResponseDrv(aM)
{
   Mout << "Entering total Quad rsp..." << endl;
   CheckKeys(aM);
   InitFrq(aM);
   InitOrientation(aM);
   InitExperimental(aM);
   InitContributions(aM);
   InitTypes(aM);
   InitRules(aM);
}

void TotalQuadRsp::CheckKeys(const std::map<string,string>& aM)
{
   set<string> all_keys;
   all_keys.insert("ORDER");
   all_keys.insert("PVQ");
   all_keys.insert("PVL");
   all_keys.insert("ZPVC");
   all_keys.insert("FRQ");
   all_keys.insert("CONTRIBUTIONS");
   all_keys.insert("SET_VALUE");
   all_keys.insert("EXPERIMENTAL");
   all_keys.insert("STD_ORIENTATION");
   // now perform check
   map<string,string>::const_iterator map_it;
   set<string>::iterator keys_it;
   bool found_error=false;
   for(map_it=aM.begin();map_it!=aM.end();map_it++) {
      keys_it=all_keys.find(map_it->first);
      if(keys_it==all_keys.end()) {
         found_error=true;
         break;
      }
   }
   if(!found_error)
      return;
   Mout << "Unknown keyword detected: " << map_it->first << endl;
   Mout << "Possible values are:" << endl;
   for(keys_it=all_keys.begin();keys_it!=all_keys.end();keys_it++)
      Mout << *keys_it << endl;
   ConstructionError("Unknown keyword detected, see above.",aM);
}

void TotalQuadRsp::InitOrientation(const map<string,string>& aM)
{
   mStd=false;
   map<string,string>::const_iterator it = aM.find("STD_ORIENTATION");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE")) {
      mStd=true;
   }
}

void TotalQuadRsp::InitExperimental(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("EXPERIMENTAL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE"))
   {
      // experimental quantities
      string iso;
      if(!mStd) {
         iso="(1.0E+00/(5.0E+00*sqrt((X)^(2)+(Y)^(2)+(Z)^(2))))*(";
         iso+="X*(3.0E+00*XXX+XYY+YXY+YYX+XZZ+ZXZ+ZZX)+";
         iso+="Y*(YXX+XYX+XXY+3.0E+00*YYY+YZZ+ZYZ+ZZY)+";
         iso+="Z*(ZXX+XZX+XXZ+ZYY+ZYZ+ZZY+3.0E+00*ZZZ))";
      }
      else {
         iso="(1.0E+00/5.0E+00)*(";
         iso+="Z*(ZXX+XZX+XXZ+ZYY+ZYZ+ZZY+3.0E+00*ZZZ))";
      }
      mTotalRspString.push_back(iso);
   }
}

void TotalQuadRsp::InitContributions(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("CONTRIBUTIONS");
   if(it==aM.end())
      return;
   // look for contributions.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Contributions must be enclosed in (...).", aM);
   string contrib_string = s.substr(1,s.size()-2);
   vector<string> contribs = SplitString(contrib_string, ",");
   for(In i=0;i<contribs.size();i++) {
      if(contribs[i].find_first_of("XYZ")==contribs[i].npos)
         continue;
      mContribSet.insert(contribs[i]);
   }
}

void TotalQuadRsp::InitTypes(const map<string,string>& aM)
{
   mPvl=false;
   mPvq=true;
   mZpvc=true;
   map<string,string>::const_iterator it = aM.find("PVL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE")) {
      mPvl=true;
   }
   it = aM.find("PVQ");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mPvq=false;
   }
   it = aM.find("ZPVA");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mZpvc=false;
   }
   if (!mPvl && !mZpvc) {
      ConstructionError("Either PVQ, PVL, ZPVA, or both must be set. Both set by default.",aM);
   }
}

void TotalQuadRsp::InitFrq(const map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("FRQ");
   if (it == aM.end())
      ConstructionError(" No frequency specified.", aM);
   // frequencies must be enclosed in par, and separated by ",",
   // as: frq=(0.0,0.0656)
   string s=it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Frequencies must be enclosed in (...).", aM);
   s=s.substr(1,s.size()-2);
   vector<string> s_vec=SplitString(s,",");
   if(s_vec.size()!=2)
   {
      ConstructionError(" Quadratic response requires two frequencies.", aM);
   }
   mFrq1 = midas::util::FromString<Nb>(s_vec[0]);
   mFrq2 = midas::util::FromString<Nb>(s_vec[1]);
}

void TotalQuadRsp::InitRules(const map<string,string>& aM)
{
   if(!mIso)
      return;
   map<string,string>::const_iterator it = aM.find("SET_VALUE");
   if (it == aM.end())
      return;
   // look for rules.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Values must be enclosed in (...).", aM);
   string rule_string = s.substr(1,s.size()-2);
   vector<string> rule_vec = SplitString(rule_string, ",");
   for(In i=0;i<rule_vec.size();i++) {
      vector<string> equal_contribs=SplitString(rule_vec[i],"=");
      // for each rule, check for initializing values,
      // i.e. xx=yy=0.0
      bool initial_val=false;
      Nb val = 0.0;
      for(In j=0;j<equal_contribs.size();j++) 
      {
         if(auto optional_val = midas::util::FromStringOptional<Nb>(equal_contribs[j]))
         {
            val = *optional_val;
            Mout << "Found initializing value..." << endl;
            initial_val=true;
            break;
         }
      }
      string s_base="type=";
      if(initial_val) {
         for(In j=0;j<equal_contribs.size()-1;j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq1);
            Contribution pv_contrib(infos,frq_vec);
            pv_contrib.SetHasBeenEval(true);
            pv_contrib.SetValue(val);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq1);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            zpv_contrib.SetHasBeenEval(true);
            zpv_contrib.SetValue(val);
            mDefault.insert(pv_contrib);
            mDefault.insert(zpv_contrib);
         }
      }
      else {
         vector<Contribution> equivalent;
         for(In j=0;j<equal_contribs.size();j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq1);
            Contribution pv_contrib(infos,frq_vec);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq1);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            equivalent.push_back(pv_contrib);
            equivalent.push_back(zpv_contrib);
         }
         mEquivalent.push_back(equivalent);
      }
   }
}

void TotalQuadRsp::ConstructionError(const string aS,
                                    const map<string,string>& aM)
{
   Mout << " TotalQuadRsp construction error:" << endl
        << aS << endl << endl
        << " Constructor parameters:" << endl;

   for (map<string,string>::const_iterator it=aM.begin(); it!=aM.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;

   MIDASERROR("");
}

void TotalQuadRsp::ConstructIntermediateSet()
{
   // given mPvq, mPvl, mZpvc, mTotalRspString, and mContribSet
   // construct the intermediate set of contributions
   set<string> c_set;
   vector<Nb> frq_vec;
   for(In i=0;i<mTotalRspString.size();i++) {
      c_set=ConvertStringToFunctions(mTotalRspString[i]);
      for(set<string>::iterator it=c_set.begin();it!=c_set.end();it++) {
         mContribSet.insert(*it);
      }
   }
   set<Contribution> help_set;
   // now the mContribSet
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++) {
      string c_s=*it;
      OpInfo oi;
      string info_line;
      // first zpv
      info_line="type="+c_s+"_POL frq1="+StringForNb(mFrq1);
      info_line+=" frq2="+StringForNb(mFrq2);
      ParseSetInfoLine(info_line,oi);
      vector<OpInfo> oi_vec;
      oi_vec.push_back(oi);
      frq_vec.clear();
      Contribution c(oi_vec,frq_vec);
      help_set.insert(c);
      // and then pure vib
      oi_vec.clear();
      info_line="type="+c_s.substr(I_0,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_1,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_2,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(mFrq1);
      frq_vec.push_back(mFrq2);
      Contribution c_a(oi_vec,frq_vec);
      help_set.insert(c_a);
      // also add Pure vib linear - more troublesome...
      AddPureVibLinear(help_set,c_s); 
   }
   // values are not set, we do this by searching the basic set
   for(set<Contribution>::iterator it=help_set.begin();it!=help_set.end();it++) {
      set<Contribution>::iterator basic_it=mBasicSet.find(*it);
      Contribution c(*it);
      if(basic_it!=mBasicSet.end())
         c.SetValue(basic_it->GetValue());
      mIntermediateSet.insert(c);
   }
   // now modify this according to mPvl and mZpva
   if(mPvq && mPvl && mZpvc) {
      Mout << "At end of day, intermediate set is:" << endl;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
         Mout << *it << endl;
      Mout << "Done in construct interm. set." << endl;
      return;
   }
   if(!mZpvc) {
      // remove all vib rsp order=1
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_1)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvl) {
      // remove all vib rsp order=2
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_2)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvq) {
      // remove all vib rsp order=3
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_3)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   Mout << "At end of day, intermediate set is:" << endl;
   for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      Mout << *it << endl;
   Mout << "Done in construct interm. set." << endl;
}

void TotalQuadRsp::AddPureVibLinear(set<Contribution>& aSet, const string& aS)
{
   // PVL+ = 1/2*P(X,Y,Z)<< <x>, <<y,z>>_wz >>_(wy+wz)
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),mFrq1+mFrq2));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      string op=perm[0].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      op=perm[1].first+perm[2].first+"_POL";
      info_line="type="+op+" frq="+StringForNb(perm[2].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(perm[1].second+perm[2].second);
      Contribution c(oi_vec,frq_vec);
      aSet.insert(c);
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   Mout << "Pure vib linear added " << count << " response functions." << endl;
}

map<string,TotalResponseContribution> TotalQuadRsp::ProvideMap()
{
   map<string,TotalResponseContribution> result;
   // Need to loop through mContribSet and create
   // TotalResponseContrib's from that
   vector<Nb> frq_vec;
   set<Contribution>::iterator c_it;
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++)
   {
      TotalResponseContribution t_rsp;
      // we have a contribution
      string func=*it;
      if(mZpvc) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func+"_POL frq1="+StringForNb(mFrq1);
         info_line+=" frq2="+StringForNb(mFrq2);
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.clear();
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb zpv=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("ZPVC",zpv);
      }
      if(mPvq) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func.substr(I_0,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_1,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_2,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.push_back(mFrq1);
         frq_vec.push_back(mFrq2);
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb pvq=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("PVQ",pvq);
      }
      if(mPvl) {
         Nb pvl=ConstructPureVibLin(func);
         t_rsp.AddTerm("PVL",pvl);
      }
      // also eq. value
      frq_vec.clear();
      frq_vec.push_back(mFrq1);
      frq_vec.push_back(mFrq2);
      Nb eq_val=SearchForEqVal(func,frq_vec);
      t_rsp.AddTerm("EQ",eq_val);
      result.insert(make_pair(func,t_rsp));
   }
   return result;
}

Nb TotalQuadRsp::ConstructPureVibLin(const string& aS)
{
   // here we construct the pure vib. linear response function
   // this means permutation!
   vector<pair<string,Nb> > perm;
   perm.push_back(make_pair(aS.substr(I_0,I_1),mFrq1+mFrq2));
   perm.push_back(make_pair(aS.substr(I_1,I_1),mFrq1));
   perm.push_back(make_pair(aS.substr(I_2,I_1),mFrq2));
   sort(perm.begin(),perm.end());
   OpInfo oi;
   string info_line;
   In count=0;
   Nb result=C_0;
   // make all permut
   do {
      vector<OpInfo> oi_vec;
      vector<Nb> frq_vec;
      string op=perm[0].first+"_DIPOLE";
      info_line="type="+op;
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      op=perm[1].first+perm[2].first+"_POL";
      info_line="type="+op+" frq="+StringForNb(perm[2].second);
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(perm[1].second+perm[2].second);
      Contribution c(oi_vec,frq_vec);
      set<Contribution>::iterator c_it=mIntermediateSet.find(c);
      if(c_it==mIntermediateSet.end()) {
         Mout << "Did not find contribution" << c << endl;
         MIDASERROR("");
      }
      result+=c_it->GetValue();
      count++;
   } while(next_permutation(perm.begin(),perm.end()));
   result*=(C_6/Nb(count));
   return result;
}

ostream& TotalQuadRsp::Print(ostream& aOut) const
{
   aOut << "Order = 3" << endl;
   aOut << "Frq1  = " << mFrq1 << endl;
   aOut << "Frq2  = " << mFrq2 << endl;
   return aOut;
}
