/**
************************************************************************
* 
* @file                FitBasisCalcDef.h
* 
* Created:             18-05-2018
*
* Author:              Emil Lund Klinting (klintg@chem.au.dk)
*
* Short Description:   Calculation definitions for fit-bases
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef FITBASISCALCDEF_H
#define FITBASISCALCDEF_H

// std headers
#include <map>
#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>

// midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/GenFitBasisCalcDef.h"

extern In gFitBasisIoLevel;

/**
 *
**/
class FitBasisCalcDef
{
   private:

      //! Add fit functions conservative in the Adga
      bool                                mAddFitFuncsConserv;
      //! Decides if the automatic determination of fit-basis should be used
      bool                                mAutomaticFitBasis;
      //! Automatic determination of the number of fit-basis functions
      bool                                mAutomaticFitFuncs;
      //! Value to determine if other fit-bases than polynomial should be used
      std::vector<Nb>                     mAutoSimiValue;
      //! Holds the identifier string for fit-basis types 
      std::vector<std::string>            mAutoFitType;
      //! Name of the fit-basis
      std::string                         mFitBasName;                  
      //! Fit-basis constant from input
      std::map<std::string, Nb>           mFitBasisConstants;           
      //! Carry out fitting routine for all mc grids at all iterations
      bool                                mFitAllMcIterGrids;
      //! Maximum order of fit-basis functions for each mode
      std::vector<In>                     mFitFuncsMaxOrder;            
      //! Maximum order of fit-basis functions for a given mode combination level 
      std::vector<In>                     mFitBasisMaxOrder;            
      //! Maximum order of fit-basis functions used for fitting the inertia momement components for each mode
      std::vector<In>                     mFitFuncsMuMaxOrder;     
      //! Maximum order of fit-basis functions used for fitting the inertia momement components for a given mode combination level
      std::vector<In>                     mFitBasisMuMaxOrder;     
      //! The fitting method
      std::string                         mFitMethod;                   
      //! Fit-basis functions from input
      std::map<std::string, std::string>  mFitFuncs;                    
      //! SVD throw-away threshold
      bool                                mDoSvdCut;       //! Use SVD cutting (detaulf)  
      Nb                                  mSvdCutoffThr;   //! Use specific number for fitting 
      bool                                mDoTikhonov;     //! Use Tikhonov stabilization 
      Nb                                  mTikhonovParam;  //! Use specific number for fitting 
      bool                                mUseSpecialNthreadsFit;
      //! Number of threads used only for fitting. 0 means use all (as per --numthreads cmd-line opt.).
      In                                  mNthreadsFit;
      //! Decides if general fit-basis functions should be used
      bool                                mUseGenFitBasis;              
      //! A pair which specifies which modes have general fit-basis definitions and which GenFitBasisCalcDef they belong to if that is the case
      std::vector<std::vector<In>>    mGenFitBasisModeMap;
      //! Vector of general fit-basis definitions
      std::vector<GenFitBasisCalcDef>     mGenFitBasisCalcDefs;

   public:
      
      //! Constructor
      FitBasisCalcDef(); 
  
      //! Function for validation of input
      void ValidateFitBasisInput(const In& aNoVibs, const bool& aDoFragmentedPesCalcs, const In& aMaxModeCoupling, const std::string& aPesFitBasisName, const bool& aCalcEffInertiaInv);
      
      //@{
      //! FitBasisCalcDef class setters
      void SetmAddFitFuncsConserv(const bool& aBool)                             {mAddFitFuncsConserv = aBool;}
      void SetmAutomaticFitBasis(const bool& aBool)                              {mAutomaticFitBasis = aBool;}
      void SetmAutomaticFitFuncs(const bool& aBool)                              {mAutomaticFitFuncs = aBool;}
      void SetmAutoSimiValue(const Nb& aNb)
      {
         mAutoSimiValue.push_back(aNb);
      }
      void SetmAutoFitType(const std::string& aStr)
      {
         mAutoFitType.push_back(aStr);
      }
      void SetmFitBasName(const std::string& aStr)                               {mFitBasName = aStr;}
      void SetmFitBasisConstants(const std::pair<std::string, Nb> aPair)         {mFitBasisConstants.insert(aPair);}
      void SetmFitAllMcIterGrids(const bool& aBool)                              {mFitAllMcIterGrids = aBool;}
      void SetmFitFuncsMaxOrder(const std::vector<In>& aVec)                     {mFitFuncsMaxOrder = aVec;}
      void SetmFitBasisMaxOrder(const std::vector<In>& aVec)                     {mFitBasisMaxOrder = aVec;}
      void SetmFitFuncsMuMaxOrder(const std::vector<In>& aVec)                   {mFitFuncsMuMaxOrder = aVec;}
      void SetmFitBasisMuMaxOrder(const std::vector<In>& aVec)                   {mFitBasisMuMaxOrder = aVec;}
      void SetmFitMethod(const std::string& aStr)                                {mFitMethod = aStr;}
      void SetmFitFuncs(const std::pair<std::string, std::string> aPair)         {mFitFuncs.insert(aPair);}
      void SetmSvdCut(const bool& aB)                                             {mDoSvdCut  = aB;}
      void SetmTikhonov(const bool& aB)                                           {mDoTikhonov= aB;}
      void SetmSvdCutoffThr(const Nb& aNb)                                       {mSvdCutoffThr = aNb;}
      void SetmTikhonovParam(const Nb& aNb)                                      {mTikhonovParam= aNb;}
      void SetmUseSpecialNthreadsFit(const bool& aBool)                          {mUseSpecialNthreadsFit = aBool;}
      void SetmNthreadsFit(const In& aIn)                                        {mNthreadsFit = aIn;}
      void SetmUseGenFitBasis(const bool& aBool)                                 {mUseGenFitBasis = aBool;}
      void SetmGenFitBasisCalcDefs(const GenFitBasisCalcDef& aGenFitBasisCalcDef)
      {
         mGenFitBasisCalcDefs.push_back(aGenFitBasisCalcDef);
      }
      //@}

      //@{
      //! FitBasisCalcDef class getters
      const bool& GetmAddFitFuncsConserv() const                              {return mAddFitFuncsConserv;}
      const bool& GetmAutomaticFitBasis() const                               {return mAutomaticFitBasis;}
      const bool& GetmAutomaticFitFuncs() const                               {return mAutomaticFitFuncs;}
      const std::vector<Nb>& GetmAutoSimiValue() const                        {return mAutoSimiValue;}
      const std::vector<std::string> GetmAutoFitType() const                  {return mAutoFitType;}
      const std::string& GetmFitBasName() const                               {return mFitBasName;}
      const std::map<std::string, Nb>& GetmFitBasisConstants() const          {return mFitBasisConstants;}
      const bool& GetmFitAllMcIterGrids() const                               {return mFitAllMcIterGrids;}
      const std::vector<In>& GetmFitFuncsMaxOrder() const                     {return mFitFuncsMaxOrder;}
      const std::vector<In>& GetmFitBasisMaxOrder() const                     {return mFitBasisMaxOrder;}
      const std::vector<In>& GetmFitFuncsMuMaxOrder() const                   {return mFitFuncsMuMaxOrder;}
      const std::vector<In>& GetmFitBasisMuMaxOrder() const                   {return mFitBasisMuMaxOrder;}
      const std::string& GetmFitMethod() const                                {return mFitMethod;}
      const std::map<std::string, std::string>& GetmFitFuncs() const          {return mFitFuncs;}
      const bool& DoSvdCut() const                                            {return mDoSvdCut;}
      const bool& DoTikhonov() const                                          {return mDoTikhonov;}
      const Nb& GetmSvdCutoffThr() const                                      {return mSvdCutoffThr;}
      const Nb& GetmTikhonovParam() const                                     {return mTikhonovParam;}
      const bool& GetmUseSpecialNthreadsFit() const                           {return mUseSpecialNthreadsFit;}
      const In& GetmNthreadsFit() const                                       {return mNthreadsFit;}
      const bool& GetmUseGenFitBasis() const                                  {return mUseGenFitBasis;}
      const std::vector<std::vector<In>>& GetmGenFitBasisModeMap() const      {return mGenFitBasisModeMap;}
      const std::vector<GenFitBasisCalcDef>& GetmGenFitBasisCalcDefs() const  {return mGenFitBasisCalcDefs;}
      //@}
};

#endif /* FITBASISCALCDEF_H */
