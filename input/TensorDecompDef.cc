/**
************************************************************************
* 
* @file                TensorDecompDef.h
*
* Created:             28-10-2011
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Store input logical information 
* 
* Last modified: Mon Jul 12, 2011  11:19AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "input/TensorDecompDef.h"

// std headers
#include<iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/MidasStream.h"

// using declarations
using std::ostream;
using std::endl;

// forward declarations
extern MidasStream Mout;

/**
 * initialize maps
 **/
const std::map<DECOMPCALCTYPE,std::string> DECOMPCALCTYPE_MAP::ENUM_TO_STRING =
{
   {DECOMPCALCTYPE::NONE,"NONE"}
 , {DECOMPCALCTYPE::VCC_BENCHMARK,"VCC_BENCHMARK"}
 , {DECOMPCALCTYPE::RSP_BENCHMARK,"RSP_BENCHMARK"}
 , {DECOMPCALCTYPE::RSP_EIG,"RSP_EIG"}
 , {DECOMPCALCTYPE::ERROR,"ERROR"}
};

const std::map<std::string,DECOMPCALCTYPE> DECOMPCALCTYPE_MAP::STRING_TO_ENUM =
{
   {"NONE",DECOMPCALCTYPE::NONE}
 , {"VCC_BENCHMARK",DECOMPCALCTYPE::VCC_BENCHMARK}
 , {"RSP_BENCHMARK",DECOMPCALCTYPE::RSP_BENCHMARK}
 , {"RSP_EIG",DECOMPCALCTYPE::RSP_EIG}
 , {"ERROR",DECOMPCALCTYPE::ERROR}
};

/**
 * default ctor initialises to default values
 **/
TensorDecompDef::TensorDecompDef():
   mType(DECOMPCALCTYPE::NONE),
   mCpRank(1),
   mCpAlsThreshold(1e-5),
   mCpAlsMaxIter(25),
   mFindCpMaxRank(10),
   mFindCpResThreshold(1e-4),
   mDecompToExciLvl(3),
   mPseudoInverseThreshold(1e-12),
   mSingleCpAnalysis(false),
   mSingleCpAnalysisMaxRank(0),
   mSpecificCpAnalysis(false),
   mSpecificCpAnalysisRanks(),
   mDoCgDecomp(false),
   mSaveDecomp(false)
{
}

/**
 * output for debug
 **/
void TensorDecompDef::Output(std::ostream& aOstream) const
{
   aOstream << "TensorDecompDefinition: \n" 
            << " mCpRank                 = " << mCpRank                 << "\n" 
            << " mCpAlsThreshold         = " << mCpAlsThreshold         << "\n"  
            << " mCpAlsMaxIter           = " << mCpAlsMaxIter           << "\n" 
            << " mFindCpMaxRank          = " << mFindCpMaxRank          << "\n" 
            << " mFindCpResThreshold     = " << mFindCpResThreshold     << "\n" 
            << " mDecompToExciLvl        = " << mDecompToExciLvl        << "\n" 
            << " mPseudoInverseThreshold = " << mPseudoInverseThreshold << "\n" 
            << " mSingleCpAnalysis       = " << mSingleCpAnalysis       << "\n" 
            << " mSpecificCpAnalysis     = " << mSpecificCpAnalysis     << "\n" 
            << " mCgDecomp               = " << mDoCgDecomp             << "\n" 
            << " mSaveDecomp             = " << mSaveDecomp;
}

/**
 * friend operator<< overload
 **/
ostream& operator<<(ostream& aOstream, const TensorDecompDef& aTensorDecompDef)
{
   aTensorDecompDef.Output(aOstream);
   return aOstream;
}
