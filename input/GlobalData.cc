#include "input/GlobalData.h"


namespace midas
{
namespace global
{

/**
 * Initialize static container.
 **/
ServiceLocator::service_container_type ServiceLocator::mServiceContainer;

/**
 * Clear service container.
 *
 * @param aServiceContainer   The container to clear.
 **/
void ServiceLocator::Clear
   (  service_container_type& aServiceContainer
   )
{
   aServiceContainer.clear();
}

} /* namespace global */
} /* namespace midas  */
