/**
************************************************************************
* 
* @file                GeoOptCalcDef.cc
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Contains input-info that 
*                      defines a geometry optimization
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/GeoOptCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"


/**
* Construct a default definition of a  calculation
* */
GeoOptCalcDef::GeoOptCalcDef():
   /**
    * General
    **/
   mCalcName("")
   ,mIoLevel (I_0)
   /**
    * Restart 
    **/
   ,mRestart( false )
{
}



