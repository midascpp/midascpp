/**
************************************************************************
* 
* @file                SystemInput.cc
*
* 
* Created:             05-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the iterative mergin of subsystems 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include "input/Input.h"
#include "input/ModSysCalcDef.h"
#include "input/SystemDef.h"
#include "input/ModSysCalcDef.h"


string ModSysInput(std::istream& Minp, const In& arInputLevel);
string SysDefInput(std::istream& Minp, const In& arInputLevel);
string RotCoordInput(std::istream& Minp, input::RotCoordCalcDef*, const In& arInputLevel);
string FalconInput(std::istream& Minp, input::FalconCalcDef*, const In& arInputLevel);

string SysInput(std::istream& Minp, bool passive, string aInputFlag)
{
   
   if (gDoSystem)
   {
      MidasWarning ("System input read twice");
   }
   gDoSystem=true;
   string s;//To be returned when we are done reading this level
   In input_level = I_2;
   enum INPUT {ERROR,SYSDEF,MODSYS}; 
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"SYSDEF",SYSDEF},
      {"#"+std::to_string(input_level)+"MODSYS",MODSYS},
   };


   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      if (passive) continue;                                  // If passive loop simple to next #
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      switch(input)
      {
         case SYSDEF:
         {
            s= SysDefInput(Minp,input_level);
            already_read=true;
            break;
         }
         case MODSYS:
         {
            s= ModSysInput(Minp,input_level);
            already_read=true;
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a System level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}



void InitializeSystem()
{
   gDoSystem          = false; 
   gSystemIoLevel     = I_0;
}
