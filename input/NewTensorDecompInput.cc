/**
************************************************************************
* 
* @file                NewTensorDecompInput.cc
*
* 
* Created:             
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Input reader for tensor decomposition information.
* 
* Last modified: Tue Oct 2, 2012  05:55PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <tuple> // for pair
#include <set>
#include <type_traits>

// midas headers
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "input/KeywordMap.h"
#include "util/FindInMap.h"
#include "libmda/util/any_type.h"
#include "tensor/TensorDecompInfo.h"

#include "tensor/TensorDecomposer.h"
#include "tensor/CpGuesser.h"
#include "tensor/FitNCG.h"
#include "tensor/FitID.h"

namespace midas
{
namespace util
{

/**
 * Namespace with utility functions for reading tensor input.
 **/
namespace detail 
{
///> deduce_key_type for TensorDecompInfo
template<>
struct deduce_key_type<midas::tensor::TensorDecompInfo>
{
   using type = typename midas::tensor::TensorDecompInfo::key_type;
};

} /* namespace detail */

} /* namespace util */
} /* namespace midas */

using namespace midas;

namespace detail 
{

/**
 * Get map of presets
 *
 * @return     The map of preset values
 **/
const keyword_map& GetTensorDecompPresetMap
   (
   )
{
   static keyword_map preset_map =
   {
      // TensorNlSolver default
      {  {"VCCSOLVER"}, {  {"DRIVER", std::string("FINDBESTCP")}
                        ,  {"FITTER", std::string("ALS")}
                        ,  {"DECOMPTOORDER", I_1}
                        ,  {"DECOMPOSECANONICAL", true}
                        ,  {"DECOMPOSEDIRPROD", true}
                        ,  {"DECOMPOSESUM", true}
                        ,  {"MATRIXSVDRELTHRESH", static_cast<Nb>(1.e-2)}
                        ,  {"FULLRANKSVD", false}
                        ,  {"CHECKSVD", false}
                        ,  {"EFFECTIVELOWORDERSVD", false}
                        ,  {"CPALSMAXITER", 100}
                        ,  {"CPALSDISTANCECONVCHECK", false}
                        ,  {"CPALSRELATIVETHRESHOLD", static_cast<Nb>(1.e-3)}
                        ,  {"CPALSBALANCERESULT", true}
                        ,  {"CPALSRELATIVELAMBDA2", static_cast<Nb>(5.e1)}
                        ,  {"CPALSRCOND", static_cast<Nb>(1.e-12)}
                        ,  {"FINDBESTCPRESTHRESHOLD", static_cast<Nb>(1.e-7)}    // Depends on the convergence threshold for the solver
                        ,  {"FINDBESTCPMAXRANK", 500}
                        ,  {"FINDBESTCPRANKINCR", 1}
                        ,  {"FINDBESTCPSTARTINGRANK", 1}
                        ,  {"GUESSER", CpGuesser<Nb>::guessType::RANDOM}
                        ,  {"GUESSINCREMENTER", CpGuesser<Nb>::updateType::FITDIFF}
                        }
      }

      // TransformerV3 default
   ,  {  {"VCCTRF"},    {  {"DRIVER", std::string("FINDBESTCP")}
                        ,  {"FITTER", std::string("ALS")}
                        ,  {"DECOMPTOORDER", I_1}
                        ,  {"DECOMPOSECANONICAL", true}
                        ,  {"DECOMPOSEDIRPROD", true}
                        ,  {"DECOMPOSESUM", true}
                        ,  {"PREPROCESSOR", std::string("C2T")}
                        ,  {"C2TSVDTHRESHOLD", static_cast<Nb>(1.e-10)}
                        ,  {"C2TADAPTIVETHRESHOLD", false}
                        ,  {"MATRIXSVDRELTHRESH", static_cast<Nb>(1.e-2)}
                        ,  {"FULLRANKSVD", false}
                        ,  {"CHECKSVD", false}
                        ,  {"EFFECTIVELOWORDERSVD", false}
                        ,  {"CPALSMAXITER", 10}
                        ,  {"CPALSDISTANCECONVCHECK", true}
                        ,  {"CPALSRELATIVETHRESHOLD", static_cast<Nb>(1.e-6)}
                        ,  {"CPALSBALANCERESULT", false}
                        ,  {"CPALSINITIALERRORCALCULATION", true}
                        ,  {"CPALSRELATIVELAMBDA2", static_cast<Nb>(5.e1)}
                        ,  {"CPALSRCOND", static_cast<Nb>(1.e-12)}
                        ,  {"CHECKITER", 3}
                        ,  {"CHECKTHRESH", static_cast<Nb>(5.)}
                        ,  {"FINDBESTCPRESTHRESHOLD", static_cast<Nb>(1.e-9)}    // Depends on the convergence threshold for the solver
                        ,  {"FINDBESTCPMAXRANK", 800}
                        ,  {"FINDBESTCPRANKINCR", 1}
                        ,  {"FINDBESTCPSTARTINGRANK", 1}
                        ,  {"FINDBESTCPADAPTIVERANKINCR", true}
                        ,  {"GUESSER", CpGuesser<Nb>::guessType::RANDOM}
                        ,  {"GUESSINCREMENTER", CpGuesser<Nb>::updateType::FITDIFF}
                        }
      }
   };

   return preset_map;
}


/**
 * Get map of keywords for each ConcreteCpDecomposerBase type
 *
 * @return Returns the map of needed/allowed keywords with default values for unset keywords.
 **/
const keyword_map& GetTensorDecompFitterKeywordMap
   (
   )
{
   // set-up map of decomp types vs which keywords are allowed
   static keyword_map keyword_map_fitter =
   { 
      // ALS default
      {  {"ALS"}, {  {"CPALSTHRESHOLD", static_cast<Nb>(1e-12)}
                  ,  {"CPALSRELATIVETHRESHOLD", static_cast<Nb>(0.0)}
                  ,  {"CPALSMAXITER", static_cast<In>(100)}
                  ,  {"MAXRANKMAXITER",-1}
                  ,  {"CPALSRCOND", static_cast<Nb>(1e-12)}
                  ,  {"CPALSUSEFITNORMCHANGE", false}
                  ,  {"CPALSTIKHONOVQ", static_cast<Nb>(0.1)}
                  ,  {"CPALSTIKHONOVALPHA", static_cast<Nb>(0.0)}
                  ,  {"CPALSRALS", false}
                  ,  {"PIVOTISEDCPALS", false}
                  ,  {"CPALSGELSD", I_20}
                  ,  {"CPALSNEWTON", false}
                  ,  {"CPALSLINESEARCH", false}
                  ,  {"CHECKITER",-1}
                  ,  {"CHECKTHRESH",static_cast<Nb>(5.)}
                  ,  {"CPALSCONDITIONCHECK", false}
                  ,  {"CPALSSCALELSSYSTEM", false}
                  ,  {"CPALSSCALEFITTENSOR", false}
                  ,  {"CPALSRELATIVELAMBDA2", static_cast<Nb>(0.)}
                  ,  {"CPALSINITIALERRORCALCULATION", false}
                  ,  {"CPALSDISTANCECONVCHECK", true}
                  ,  {"CPALSBALANCERESULT", false}
                  }
      }

      // NCG default
   ,  {  {"NCG"}, {  {"CPNCGTHRESHOLD", static_cast<Nb>(1.e-10)}
                  ,  {"CPNCGRELATIVETHRESHOLD", static_cast<Nb>(0.0)}
                  ,  {"CPNCGMAXITER", 100}
                  ,  {"CPNCGTYPE", CpNCGInput<Nb>::ncgType::HS}
                  ,  {"CPNCGGENERALSOLVER", false}
                  ,  {"CPNCGLINESEARCH", CpNCGInput<Nb>::lsType::WOLFE}
                  ,  {"CPNCGALPHAMAX", static_cast<Nb>(1.e5)}
                  ,  {"CPNCGALPHARESOLUTION", static_cast<Nb>(5.e-2)}
                  ,  {"CPNCGWOLFEC1", static_cast<Nb>(1.e-4)}
                  ,  {"CPNCGWOLFEC2", static_cast<Nb>(0.1)}
                  ,  {"CPNCGMAXELEMENTCONVCHECK", false}
                  ,  {"CPNCGBALANCEMODEVECTORS", false}
                  ,  {"CPNCGDIAGONALPRECONDITIONER", false}
                  ,  {"HAGERZHANGTHETA", static_cast<Nb>(2.)}
                  ,  {"CHECKITER",-1}
                  ,  {"CHECKTHRESH",static_cast<Nb>(5.)}
                  ,  {"CPNCG3PGRELTHRESHOLD", static_cast<Nb>(1.e-3)}
                  ,  {"CPNCGRESTART", static_cast<Nb>(0.1)}
                  }
      }
      
      // ASD default
   ,  {  {"ASD"}, {  {"CPASDTHRESHOLD", static_cast<Nb>(1.e-10)}
                  ,  {"CPASDRELATIVETHRESHOLD", static_cast<Nb>(0.0)}
                  ,  {"CPASDMAXITER", static_cast<In>(500)}
                  ,  {"PIVOTISEDCPASD", false}
                  ,  {"CPASDDIAGONALPRECONDITIONER", false}
                  ,  {"CPPASDMODEMATRIXNORMPIVOT", true}
                  ,  {"CPPASDBALANCEMODEVECTORS", false}
                  ,  {"CHECKITER",-1}
                  ,  {"CHECKTHRESH",static_cast<Nb>(5.)}
                  }
      }
   
      // NOFITTER
   ,  {  {"NOFITTER"},  {
                        }
      }
   };

   return keyword_map_fitter;
}

/**
 * Get map of keywords for each TensorDecompDriver type.
 *
 * @return Returns the map of needed/allowed keywords with default values for unset keywords.
 **/
const keyword_map& GetTensorDecompDriverKeywordMap
   (
   )
{
   // set-up map of decomp types vs which keywords are allowed
   static keyword_map keyword_map_driver =
   { 
      // FIXEDRANKCP default
      {  {"FIXEDRANKCP"},  {  {"CPRANK", static_cast<In>(1)}
                           ,  {"DECOMPTOORDER", I_1}
                           ,  {"GUESSER", CpGuesser<Nb>::guessType::RANDOM}
                           ,  {"GUESSINCREMENTER", CpGuesser<Nb>::updateType::FITDIFF}  // Needed to construct CpGuesser
                           ,  {"SCALEGUESSNORM", true} // Needed to construct CpGuesser
                           ,  {"RESIDUALOVERLAPSCALING", false} // Needed to construct CpGuesser
                           ,  {"RESIDUALNORMSCALING", true} // Needed to construct CpGuesser
                           ,  {"FITDIFFRELATIVETHRESHOLD", static_cast<Nb>(1.e-2)} // Needed to construct CpGuesser
                           ,  {"DEFAULTRESIDUALFITTER", true}  // Needed to construct CpGuesser
                           ,  {"PREPROCESSOR", std::string("NOPREPROCESSOR")}
                           ,  {"C2TSVDTHRESHOLD", static_cast<Nb>(1.e-10)}
                           ,  {"C2TADAPTIVETHRESHOLD", false}
                           ,  {"C2TSAFESVD", false}
                           ,  {"DECOMPOSECANONICAL", true}
                           ,  {"DECOMPOSEDIRPROD", true}
                           ,  {"DECOMPOSESUM", true}
                           }
      }

      // FINDBESTCP default
   ,  {  {"FINDBESTCP"},   {  {"FINDBESTCPMAXRANK", 5}
                           ,  {"FINDBESTCPRANKINCR", 1}
                           ,  {"FINDBESTCPADAPTIVERANKINCR", false}
                           ,  {"FINDBESTCPSCALERANKINCR", false}
                           ,  {"FINDBESTCPRESTHRESHOLD", static_cast<Nb>(1e-2)}
                           ,  {"MATRIXSVDRELTHRESH", static_cast<Nb>(1.e-2)}
                           ,  {"FULLRANKSVD", false}
                           ,  {"CHECKSVD", false}
                           ,  {"EFFECTIVELOWORDERSVD", false}
                           ,  {"DECOMPTOORDER", I_1}
                           ,  {"GUESSER", CpGuesser<Nb>::guessType::RANDOM}
                           ,  {"GUESSINCREMENTER", CpGuesser<Nb>::updateType::FITDIFF}
                           ,  {"SCALEGUESSNORM", true}
                           ,  {"RESIDUALOVERLAPSCALING", false}
                           ,  {"RESIDUALNORMSCALING", true}
                           ,  {"FITDIFFRELATIVETHRESHOLD", static_cast<Nb>(1.e-2)}
                           ,  {"DEFAULTRESIDUALFITTER", false}
                           ,  {"FINDBESTCPSTARTINGRANK", 1}
                           ,  {"FINDBESTCPMAXERRORINCR", 1}
                           ,  {"PREPROCESSOR", std::string("NOPREPROCESSOR")}
                           ,  {"C2TSVDTHRESHOLD", static_cast<Nb>(1.e-10)}
                           ,  {"C2TADAPTIVETHRESHOLD", false}
                           ,  {"C2TSAFESVD", false}
                           ,  {"DECOMPOSECANONICAL", true}
                           ,  {"DECOMPOSEDIRPROD", true}
                           ,  {"DECOMPOSESUM", true}
                           }
      }
      // NESTEDCP default
   ,  {  {"NESTEDCP"},     {  {"NESTEDCPMAXRANK", 5}
                           ,  {"NESTEDCPRANKINCR", 1}
                           ,  {"NESTEDCPSCALERANKINCR", false}
                           ,  {"NESTEDCPADAPTIVERANKINCR", false}
                           ,  {"NESTEDCPRESTHRESHOLD", static_cast<Nb>(1e-2)}
                           ,  {"NESTEDCPSAFERESIDUALNORM", false}
                           ,  {"MATRIXSVDRELTHRESH", static_cast<Nb>(1.e-2)}
                           ,  {"FULLRANKSVD", false}
                           ,  {"CHECKSVD", false}
                           ,  {"EFFECTIVELOWORDERSVD", false}
                           ,  {"DECOMPTOORDER", I_1}
                           ,  {"GUESSER", CpGuesser<Nb>::guessType::RANDOM}
                           ,  {"GUESSINCREMENTER", CpGuesser<Nb>::updateType::FITDIFF}
                           ,  {"SCALEGUESSNORM", true}
                           ,  {"RESIDUALOVERLAPSCALING", false}
                           ,  {"RESIDUALNORMSCALING", true}
                           ,  {"FITDIFFRELATIVETHRESHOLD", static_cast<Nb>(1.e-2)}
                           ,  {"DEFAULTRESIDUALFITTER", false}
                           ,  {"PREPROCESSOR", std::string("NOPREPROCESSOR")}
                           ,  {"C2TSVDTHRESHOLD", static_cast<Nb>(1.e-10)}
                           ,  {"C2TADAPTIVETHRESHOLD", false}
                           ,  {"C2TSAFESVD", false}
                           ,  {"NESTEDCPBALANCEMODEVECTORS", false}
                           ,  {"NESTEDCPALLOWREFIT", false}
                           ,  {"DECOMPOSECANONICAL", true}
                           ,  {"DECOMPOSEDIRPROD", true}
                           ,  {"DECOMPOSESUM", true}
                           }
      }
      // CP-ID default
   ,  {  {"CPID"},         {  {"DECOMPTOORDER", I_1}
                           ,  {"MATRIXSVDRELTHRESH", static_cast<Nb>(1.)}
                           ,  {"FULLRANKSVD", false}
                           ,  {"CHECKSVD", false}
                           ,  {"EFFECTIVELOWORDERSVD", false}
                           ,  {"PREPROCESSOR", std::string("NOPREPROCESSOR")}
                           ,  {"C2TSVDTHRESHOLD", static_cast<Nb>(1.e-10)}
                           ,  {"C2TADAPTIVETHRESHOLD", false}
                           ,  {"C2TSAFESVD", false}
                           ,  {"CPIDRESTHRESHOLD", static_cast<Nb>(1.e-8)}
                           ,  {"CPIDRCOND", static_cast<Nb>(1.e-8)}
                           ,  {"CPIDMAXRANK", 100}
                           ,  {"CPIDADDRANK", 20}
                           ,  {"CPIDMAXRANKINCR", I_0}
                           ,  {"CPIDDISTRIBUTION", CpIdInput<Nb>::distrID::NORMAL}
                           ,  {"CPIDDOSVD", false}
                           ,  {"CPIDRANDOMIZEMATRIXID", false}
                           ,  {"CPIDABSCONV", false}
                           ,  {"DECOMPOSECANONICAL", true}
                           ,  {"DECOMPOSEDIRPROD", true} // Not implemented, so will not happen anyway!
                           ,  {"DECOMPOSESUM", true}
                           }
      }
   };

   return keyword_map_driver;
}

/**
 * Validate TensorDecompInfo as created by input.
 * Will throw a MidasWarning if 'extra'/unused keywords are set,
 * which are not used by the given type.
 * Also will set alle default values needed for given type.
 *
 * @param decompinfo 
 *    The TensorDecompInfo to validate. On output every keyword needed that was
 *    not set on input has been given a dfault value.
 **/
void ValidateTensorDecompInput
   ( midas::tensor::TensorDecompInfo& decompinfo
   )
{
   // check we have set a driver and a fitter
   auto& driver = util::FindElseError("DRIVER", decompinfo, "Did not find 'DRIVER' in TensorDecompInfo after input. Check 'TYPE' input!").second.get<std::string>();
   auto& fitter = util::FindElseError("FITTER", decompinfo, "Did not find 'FITTER' in TensorDecompInfo after input. Check 'TYPE' input!").second.get<std::string>();

   // check for driver and fitter in keyword_map
   auto& keyword_map_driver = GetTensorDecompDriverKeywordMap();
   auto& keyword_map_fitter = GetTensorDecompFitterKeywordMap();

   auto& allowed_driver_keywords =  util::FindElseError
                                       (  driver
                                       ,  keyword_map_driver
                                       ,  "DRIVER: '" + std::string(driver) + "' not found in tensor decomp map."
                                       ).second;

   auto& allowed_fitter_keywords =  util::FindElseError
                                       (  fitter
                                       ,  keyword_map_fitter
                                       ,  "FITTER: '" + std::string(fitter) + "' not found in tensor decomp map."
                                       ).second;
   
   // check that all set keywords are allowed for the currectly set DRIVER and FITTER
   for(auto& elem : decompinfo)
   {
      if (  elem.first == "DRIVER"
         || elem.first == "FITTER"
         || elem.first == "IOLEVEL"
         )
      {
         continue;
      }

      auto driver_iter = allowed_driver_keywords.find(elem);
      auto fitter_iter = allowed_fitter_keywords.find(elem);

      if (  driver_iter == allowed_driver_keywords.end()
         && fitter_iter == allowed_fitter_keywords.end()
         )
      {
         MIDASERROR("Keyword: '" + elem.first + "' is not allowed for type: '" + std::string(driver) + std::string(fitter) + "'.");
      }
   }

   // set default driver values
   for(auto& elem : allowed_driver_keywords)
   {
      auto iter = decompinfo.find(elem.first);
      if(iter == decompinfo.end())
      {
         decompinfo[elem.first] = elem.second;
      }
   }

   // set default fitter values
   for(auto& elem : allowed_fitter_keywords)
   {
      auto iter = decompinfo.find(elem.first);
      if(iter == decompinfo.end())
      {
         decompinfo[elem.first] = elem.second;
      }
   }

   // set other defaults
   {
      auto iter = decompinfo.find("IOLEVEL");
      if (  iter == decompinfo.end()   )
      {
         decompinfo["IOLEVEL"] = I_1;  // IoLevel is 1 as default (lowest value)
      }
   }
}

} /* namespace detail */

/**
 * New input function for tensor decompositions on NiceTensor%s.
 *
 * @param Minp           Input file stream.
 * @param s              Currently read string.
 * @param input_level    Input keyword level (tensor decomposition input can have different levels.)
 * @param decompinfo_set Set of TensorDecompInfo's to add currently read TensorDecompInfo to.
 * @return               Return whether s should be processed by calling function, 
 *                       i.e. has already beed read, but not processed.
 **/
bool NewTensorDecompInput
   (  std::istream& Minp
   ,  std::string& s
   ,  In input_level
   ,  midas::tensor::TensorDecompInfo::Set& decompinfo_set
   )
{
   midas::tensor::TensorDecompInfo decompinfo;
   //decompinfo["TYPE"] = "NONE"; // default type to NONE, will be overwritten if type is set

   // Enum/map with input flags
   enum INPUT { ERROR, TYPE, NAME, IOLEVEL, CPRANK, CPALSTHRESHOLD, CPALSRELATIVETHRESHOLD, CPALSMAXITER
              , CPALSRCOND, CPALSUSEFITNORMCHANGE, FINDBESTCPMAXRANK, FINDBESTCPRANKINCR, FINDBESTCPADAPTIVERANKINCR, FINDBESTCPSCALERANKINCR
              , FINDBESTCPRESTHRESHOLD, DECOMPTOORDER, GUESSER, GUESSINCREMENTER
              , FINDBESTCPSTARTINGRANK, CPNCGTHRESHOLD, CPNCGMAXITER, CPALSTIKHONOV, CPALSRALS
              , C2TSVDTHRESHOLD, MAXRANKMAXITER
              , CHECKITER, CHECKTHRESH, NOGUESSNORMSCALING
              , RESIDUALOVERLAPSCALING, MATRIXSVDRELTHRESH, LOWRANKSVD, FULLRANKSVD, CHECKSVD
              , NORESIDUALNORMSCALING, FITDIFFRELATIVETHRESHOLD, EFFECTIVELOWORDERSVD
              , FINDBESTCPMAXERRORINCR, CPNCGTYPE, CPNCGRELATIVETHRESHOLD, CPASDTHRESHOLD, CPASDRELATIVETHRESHOLD, CPASDMAXITER, CPPASDMAXNORMPIVOT
              , CPASDDIAGONALPRECONDITIONER, CPPASDBALANCEMODEVECTORS, PIVOTISEDCPALS, CPALSGELSD, CPALSNEWTON
              , PIVOTISEDCPASD, CPALSLINESEARCH, CPNCGGENERALSOLVER, CPNCGALPHAMAX, CPNCGALPHARESOLUTION, CPNCGWOLFEC1
              , CPNCGWOLFEC2, CPNCGLINESEARCH, CPNCGMAXELEMENTCONVCHECK, CPNCGBALANCEMODEVECTORS, C2TSAFESVD, CPNCGDIAGONALPRECONDITIONER
              , DEFAULTRESIDUALFITTER, HAGERZHANGTHETA, NESTEDCPRESTHRESHOLD, NESTEDCPRANKINCR, NESTEDCPMAXRANK, NESTEDCPSAFERESIDUALNORM
              , CPNCG3PGRELTHRESHOLD, CPNCGRESTART, NESTEDCPBALANCEMODEVECTORS, CPALSCONDITIONCHECK, NESTEDCPSCALERANKINCR
              , NESTEDCPADAPTIVERANKINCR, NESTEDCPALLOWREFIT, CPALSSCALELSSYSTEM, CPALSSCALEFITTENSOR, CPALSNORMREGULARIZATION 
              , CPALSINITIALERRORCALCULATION, CPALSNODISTANCECONVCHECK, CPALSBALANCERESULT, PRESET, C2TADAPTIVETHRESHOLD
              , CPIDRESTHRESHOLD, CPIDRCOND, CPIDMAXRANK, CPIDADDRANK, CPIDDISTRIBUTION, CPIDDOSVD, CPIDRANDOMIZEMATRIXID, CPIDABSCONV, CPIDMAXRANKINCR
              , NODECOMPOSECANONICAL, NODECOMPOSEDIRPROD, NODECOMPOSESUM
              };

   const map<string,INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"TYPE", TYPE}
   ,  {"#"+std::to_string(input_level)+"NAME", NAME}
   ,  {"#"+std::to_string(input_level)+"IOLEVEL", IOLEVEL}
   ,  {"#"+std::to_string(input_level)+"CPRANK", CPRANK}
   ,  {"#"+std::to_string(input_level)+"CPALSTHRESHOLD", CPALSTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPALSRELATIVETHRESHOLD", CPALSRELATIVETHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPALSMAXITER", CPALSMAXITER}
   ,  {"#"+std::to_string(input_level)+"CPALSRCOND", CPALSRCOND}
   ,  {"#"+std::to_string(input_level)+"CPALSUSEFITNORMCHANGE", CPALSUSEFITNORMCHANGE}
   ,  {"#"+std::to_string(input_level)+"CPALSTIKHONOV", CPALSTIKHONOV}
   ,  {"#"+std::to_string(input_level)+"CPALSRALS", CPALSRALS}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPMAXRANK", FINDBESTCPMAXRANK}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPRANKINCR", FINDBESTCPRANKINCR}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPADAPTIVERANKINCR", FINDBESTCPADAPTIVERANKINCR}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPSCALERANKINCR", FINDBESTCPSCALERANKINCR}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPRESTHRESHOLD", FINDBESTCPRESTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"DECOMPTOORDER", DECOMPTOORDER}
   ,  {"#"+std::to_string(input_level)+"GUESSER" , GUESSER}
   ,  {"#"+std::to_string(input_level)+"GUESSINCREMENTER" , GUESSINCREMENTER}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPSTARTINGRANK", FINDBESTCPSTARTINGRANK}
   ,  {"#"+std::to_string(input_level)+"CPNCGTHRESHOLD", CPNCGTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPNCGRELATIVETHRESHOLD", CPNCGRELATIVETHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPNCGMAXITER", CPNCGMAXITER}
   ,  {"#"+std::to_string(input_level)+"C2TADAPTIVETHRESHOLD", C2TADAPTIVETHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"C2TSVDTHRESHOLD", C2TSVDTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"MAXRANKMAXITER",MAXRANKMAXITER}
   ,  {"#"+std::to_string(input_level)+"CHECKITER",CHECKITER}
   ,  {"#"+std::to_string(input_level)+"CHECKTHRESH",CHECKTHRESH}
   ,  {"#"+std::to_string(input_level)+"NOGUESSNORMSCALING",NOGUESSNORMSCALING}
   ,  {"#"+std::to_string(input_level)+"MATRIXSVDRELTHRESH",MATRIXSVDRELTHRESH}
   ,  {"#"+std::to_string(input_level)+"LOWRANKSVD",LOWRANKSVD}
   ,  {"#"+std::to_string(input_level)+"FULLRANKSVD",FULLRANKSVD}
   ,  {"#"+std::to_string(input_level)+"CHECKSVD",CHECKSVD}
   ,  {"#"+std::to_string(input_level)+"RESIDUALOVERLAPSCALING",RESIDUALOVERLAPSCALING}
   ,  {"#"+std::to_string(input_level)+"NORESIDUALNORMSCALING",NORESIDUALNORMSCALING}
   ,  {"#"+std::to_string(input_level)+"FITDIFFRELATIVETHRESHOLD",FITDIFFRELATIVETHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"FINDBESTCPMAXERRORINCR",FINDBESTCPMAXERRORINCR}
   ,  {"#"+std::to_string(input_level)+"CPNCGTYPE",CPNCGTYPE}
   ,  {"#"+std::to_string(input_level)+"CPASDTHRESHOLD", CPASDTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPASDRELATIVETHRESHOLD", CPASDRELATIVETHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPASDMAXITER", CPASDMAXITER}
   ,  {"#"+std::to_string(input_level)+"CPPASDMAXNORMPIVOT", CPPASDMAXNORMPIVOT}
   ,  {"#"+std::to_string(input_level)+"CPASDDIAGONALPRECONDITIONER", CPASDDIAGONALPRECONDITIONER}
   ,  {"#"+std::to_string(input_level)+"CPPASDBALANCEMODEVECTORS", CPPASDBALANCEMODEVECTORS}
   ,  {"#"+std::to_string(input_level)+"PIVOTISEDCPALS", PIVOTISEDCPALS}
   ,  {"#"+std::to_string(input_level)+"PIVOTISEDCPASD", PIVOTISEDCPASD}
   ,  {"#"+std::to_string(input_level)+"CPALSGELSD", CPALSGELSD}
   ,  {"#"+std::to_string(input_level)+"CPALSNEWTON", CPALSNEWTON}
   ,  {"#"+std::to_string(input_level)+"CPALSLINESEARCH", CPALSLINESEARCH}
   ,  {"#"+std::to_string(input_level)+"CPNCGGENERALSOLVER", CPNCGGENERALSOLVER}
   ,  {"#"+std::to_string(input_level)+"CPNCGLINESEARCH", CPNCGLINESEARCH}
   ,  {"#"+std::to_string(input_level)+"CPNCGALPHAMAX", CPNCGALPHAMAX}
   ,  {"#"+std::to_string(input_level)+"CPNCGALPHARESOLUTION", CPNCGALPHARESOLUTION}
   ,  {"#"+std::to_string(input_level)+"CPNCGMAXELEMENTCONVCHECK", CPNCGMAXELEMENTCONVCHECK}
   ,  {"#"+std::to_string(input_level)+"CPNCGBALANCEMODEVECTORS", CPNCGBALANCEMODEVECTORS}
   ,  {"#"+std::to_string(input_level)+"C2TSAFESVD", C2TSAFESVD}
   ,  {"#"+std::to_string(input_level)+"CPNCGDIAGONALPRECONDITIONER", CPNCGDIAGONALPRECONDITIONER}
   ,  {"#"+std::to_string(input_level)+"DEFAULTRESIDUALFITTER", DEFAULTRESIDUALFITTER}
   ,  {"#"+std::to_string(input_level)+"HAGERZHANGTHETA", HAGERZHANGTHETA}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPRESTHRESHOLD", NESTEDCPRESTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPMAXRANK", NESTEDCPMAXRANK}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPRANKINCR", NESTEDCPRANKINCR}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPSAFERESIDUALNORM", NESTEDCPSAFERESIDUALNORM}
   ,  {"#"+std::to_string(input_level)+"CPNCG3PGRELTHRESHOLD", CPNCG3PGRELTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPNCGRESTART", CPNCGRESTART}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPBALANCEMODEVECTORS", NESTEDCPBALANCEMODEVECTORS}
   ,  {"#"+std::to_string(input_level)+"CPALSCONDITIONCHECK", CPALSCONDITIONCHECK}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPSCALERANKINCR", NESTEDCPSCALERANKINCR}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPADAPTIVERANKINCR", NESTEDCPADAPTIVERANKINCR}
   ,  {"#"+std::to_string(input_level)+"NESTEDCPALLOWREFIT", NESTEDCPALLOWREFIT}
   ,  {"#"+std::to_string(input_level)+"CPALSSCALELSSYSTEM", CPALSSCALELSSYSTEM}
   ,  {"#"+std::to_string(input_level)+"CPALSSCALEFITTENSOR", CPALSSCALEFITTENSOR}
   ,  {"#"+std::to_string(input_level)+"CPALSNORMREGULARIZATION", CPALSNORMREGULARIZATION}
   ,  {"#"+std::to_string(input_level)+"CPALSINITIALERRORCALCULATION", CPALSINITIALERRORCALCULATION}
   ,  {"#"+std::to_string(input_level)+"CPALSNODISTANCECONVCHECK", CPALSNODISTANCECONVCHECK}
   ,  {"#"+std::to_string(input_level)+"CPALSBALANCERESULT", CPALSBALANCERESULT}
   ,  {"#"+std::to_string(input_level)+"PRESET", PRESET}
   ,  {"#"+std::to_string(input_level)+"EFFECTIVELOWORDERSVD", EFFECTIVELOWORDERSVD}
   ,  {"#"+std::to_string(input_level)+"CPIDRESTHRESHOLD", CPIDRESTHRESHOLD}
   ,  {"#"+std::to_string(input_level)+"CPIDRCOND", CPIDRCOND}
   ,  {"#"+std::to_string(input_level)+"CPIDMAXRANK", CPIDMAXRANK}
   ,  {"#"+std::to_string(input_level)+"CPIDADDRANK", CPIDADDRANK}
   ,  {"#"+std::to_string(input_level)+"CPIDMAXRANKINCR", CPIDMAXRANKINCR}
   ,  {"#"+std::to_string(input_level)+"CPIDDISTRIBUTION", CPIDDISTRIBUTION}
   ,  {"#"+std::to_string(input_level)+"CPIDDOSVD", CPIDDOSVD}
   ,  {"#"+std::to_string(input_level)+"CPIDRANDOMIZEMATRIXID", CPIDRANDOMIZEMATRIXID}
   ,  {"#"+std::to_string(input_level)+"CPIDABSCONV", CPIDABSCONV}
   ,  {"#"+std::to_string(input_level)+"NODECOMPOSECANONICAL", NODECOMPOSECANONICAL}
   ,  {"#"+std::to_string(input_level)+"NODECOMPOSEDIRPROD", NODECOMPOSEDIRPROD}
   ,  {"#"+std::to_string(input_level)+"NODECOMPOSESUM", NODECOMPOSESUM}
   };

   // Maps for reading CP guesser, guess updates, and NCG method
   const std::map<std::string, CpGuesser<Nb>::guessType> guesser_map =
   {
      {"SVD",  CpGuesser<Nb>::guessType::SVD}
   ,  {"SCA",  CpGuesser<Nb>::guessType::SCA}
   ,  {"RANDOM",  CpGuesser<Nb>::guessType::RANDOM}
   ,  {"CONSTVAL",  CpGuesser<Nb>::guessType::CONSTVAL}
   };

   const std::map<std::string, CpGuesser<Nb>::updateType> update_map =
   {
      {"REPLACE", CpGuesser<Nb>::updateType::REPLACE}
   ,  {"RANDOM",  CpGuesser<Nb>::updateType::RANDOM}
   ,  {"CONSTVAL",  CpGuesser<Nb>::updateType::CONSTVAL}
   ,  {"SCA",  CpGuesser<Nb>::updateType::SCA}
   ,  {"FITDIFF",  CpGuesser<Nb>::updateType::FITDIFF}
   ,  {"STEPWISEFIT",  CpGuesser<Nb>::updateType::STEPWISEFIT}
   };

   const std::map<std::string, CpNCGInput<Nb>::ncgType> ncg_map =
   {
      {"FR",   CpNCGInput<Nb>::ncgType::FR}
   ,  {"PR",   CpNCGInput<Nb>::ncgType::PR}
   ,  {"FP",   CpNCGInput<Nb>::ncgType::FP}
   ,  {"HS",   CpNCGInput<Nb>::ncgType::HS}
   ,  {"DY",   CpNCGInput<Nb>::ncgType::DY}
   ,  {"HZ",   CpNCGInput<Nb>::ncgType::HZ}
   };

   const std::map<std::string, CpNCGInput<Nb>::lsType> ncg_ls_map = 
   {
      {"WOLFE",   CpNCGInput<Nb>::lsType::WOLFE}
   ,  {"HZ",   CpNCGInput<Nb>::lsType::HZ}
   ,  {"EXACT",   CpNCGInput<Nb>::lsType::EXACT}
   };

   const std::map<std::string, CpIdInput<Nb>::distrID> id_distr_map =
   {
      {"NORMAL",  CpIdInput<Nb>::distrID::NORMAL}
   };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   while ( ( already_read || midas::input::GetLine(Minp,s) )
         && !midas::input::IsLowerLevelKeyword(s,input_level)
         ) 
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);

      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case TYPE:
         {
            midas::input::GetLine(Minp,s);
            auto type_line = midas::util::VectorFromString<std::string>(s);

            auto nargs = type_line.size();

            // Driver and fitter specified
            if (  nargs == 2
               )
            {
               decompinfo["PREPROCESSOR"] = std::string("NOPREPROCESSOR");
               decompinfo["DRIVER"] = type_line[0];
               decompinfo["FITTER"] = type_line[1];
            }
            // Driver, fitter, and preprocessor specified
            else if  (  nargs == 3
                     )
            {
               decompinfo["PREPROCESSOR"] = type_line[0];
               decompinfo["DRIVER"] = type_line[1];
               decompinfo["FITTER"] = type_line[2];
            }
            else
            {
               MIDASERROR("Wrong number of arguments for TensorDecompInfo 'TYPE' keyword!");
            }

            break;
         }
         case NAME:
         {  
            midas::input::GetLine(Minp,s);
            decompinfo["NAME"] = s;
            break;
         }
         case IOLEVEL:
         {
            decompinfo["IOLEVEL"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPRANK:
         {
            decompinfo["CPRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPALSTHRESHOLD:
         {
            decompinfo["CPALSTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPALSRELATIVETHRESHOLD:
         {
            decompinfo["CPALSRELATIVETHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPALSMAXITER:
         {
            decompinfo["CPALSMAXITER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPALSRCOND:
         {
            decompinfo["CPALSRCOND"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPALSUSEFITNORMCHANGE:
         {
            decompinfo["CPALSUSEFITNORMCHANGE"] = true;
            break;
         }
         case CPALSTIKHONOV:
         {
            midas::input::GetLine(Minp, s);
            auto tikhonov_line = midas::util::TupleFromString<Nb, Nb>(s);
            decompinfo["CPALSTIKHONOVALPHA"] = std::get<0>(tikhonov_line);
            decompinfo["CPALSTIKHONOVQ"]     = std::get<1>(tikhonov_line);
            break;
         }
         case CPALSRALS:
         {
            midas::input::GetLine(Minp, s);
            auto tikhonov_line = midas::util::TupleFromString<Nb, Nb>(s);
            decompinfo["CPALSRALS"]          = true;
            decompinfo["CPALSTIKHONOVALPHA"] = std::get<0>(tikhonov_line);
            decompinfo["CPALSTIKHONOVQ"]     = std::get<1>(tikhonov_line);
            break;
         }
         case FINDBESTCPMAXRANK:
         {
            decompinfo["FINDBESTCPMAXRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case FINDBESTCPRANKINCR:
         {
            decompinfo["FINDBESTCPRANKINCR"] = midas::input::GetIn(Minp, s);
            break;
         }
         case FINDBESTCPADAPTIVERANKINCR:
         {
            decompinfo["FINDBESTCPADAPTIVERANKINCR"] = true;
            break;
         }
         case FINDBESTCPSCALERANKINCR:
         {
            decompinfo["FINDBESTCPSCALERANKINCR"] = true;
            break;
         }
         case FINDBESTCPRESTHRESHOLD:
         {
            decompinfo["FINDBESTCPRESTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case MATRIXSVDRELTHRESH:
         {
            decompinfo["MATRIXSVDRELTHRESH"] = midas::input::GetNb(Minp, s);
            break;
         }
         case LOWRANKSVD:
         {
            decompinfo["FULLRANKSVD"] = false;
            break;
         }
         case FULLRANKSVD:
         {
            decompinfo["FULLRANKSVD"] = true;
            break;
         }
         case CHECKSVD:
         {
            decompinfo["CHECKSVD"] = true;
            break;
         }
         case DECOMPTOORDER:
         {
            decompinfo["DECOMPTOORDER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case GUESSER:
         {
            midas::input::GetLine(Minp, s);
            auto found = guesser_map.find(s); //midas::input::FindKeyword(guesser_map,s);
            if(found != guesser_map.end())
            {
               decompinfo["GUESSER"] = found->second;
            }
            else
            {
               MIDASERROR("GUESSER not found!");
            }
            break;
         }
         case GUESSINCREMENTER:
         {
            midas::input::GetLine(Minp,s);
            auto found = update_map.find(s);
            if(found != update_map.end())
            {
               decompinfo["GUESSINCREMENTER"] = found->second;
            }
            else
            {
               MIDASERROR("GUESSINCREMENTER not found!");
            }
            break;
         }
         case FINDBESTCPSTARTINGRANK:
         {
            decompinfo["FINDBESTCPSTARTINGRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPNCGTHRESHOLD:
         {
            decompinfo["CPNCGTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGRELATIVETHRESHOLD:
         {
            decompinfo["CPNCGRELATIVETHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGMAXITER:
         {
            decompinfo["CPNCGMAXITER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case C2TSVDTHRESHOLD:
         {
            decompinfo["C2TSVDTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case C2TADAPTIVETHRESHOLD:
         {
            decompinfo["C2TADAPTIVETHRESHOLD"] = true;
            break;
         }
         case MAXRANKMAXITER:
         {
            decompinfo["MAXRANKMAXITER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CHECKITER:
         {
            decompinfo["CHECKITER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CHECKTHRESH:
         {
            decompinfo["CHECKTHRESH"] = midas::input::GetNb(Minp, s);
            break;
         }
         case NOGUESSNORMSCALING:
         {
            decompinfo["SCALEGUESSNORM"] = false;
            break;
         }
         case RESIDUALOVERLAPSCALING:
         {
            decompinfo["RESIDUALOVERLAPSCALING"] = true;
            break;
         }
         case NORESIDUALNORMSCALING:
         {
            decompinfo["RESIDUALNORMSCALING"] = false;
            break;
         }
         case FITDIFFRELATIVETHRESHOLD:
         {
            decompinfo["FITDIFFRELATIVETHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case FINDBESTCPMAXERRORINCR:
         {
            decompinfo["FINDBESTCPMAXERRORINCR"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPNCGTYPE:
         {
            midas::input::GetLine(Minp,s);
            auto found = ncg_map.find(s);
            if(found != ncg_map.end())
            {
               decompinfo["CPNCGTYPE"] = found->second;
            }
            else
            {
               MIDASERROR("CPNCGTYPE not found!");
            }
            break;
         }
         case CPNCGLINESEARCH:
         {
            midas::input::GetLine(Minp, s);
            auto found = ncg_ls_map.find(s);
            if(found != ncg_ls_map.end())
            {
               decompinfo["CPNCGLINESEARCH"] = found->second;
            }
            else
            {
               MIDASERROR("CPNCGLINESEARCH not found!");
            }
            break;
         }
         case CPNCGALPHAMAX:
         {
            decompinfo["CPNCGALPHAMAX"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGALPHARESOLUTION:
         {
            decompinfo["CPNCGALPHARESOLUTION"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGWOLFEC1:
         {
            decompinfo["CPNCGWOLFEC1"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGWOLFEC2:
         {
            decompinfo["CPNCGWOLFEC2"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPASDTHRESHOLD:
         {
            decompinfo["CPASDTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPASDRELATIVETHRESHOLD:
         {
            decompinfo["CPASDRELATIVETHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPASDMAXITER:
         {
            decompinfo["CPASDMAXITER"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPPASDMAXNORMPIVOT:
         {
            decompinfo["CPPASDMODEMATRIXNORMPIVOT"] = false;
            break;
         }
         case CPASDDIAGONALPRECONDITIONER:
         {
            decompinfo["CPASDDIAGONALPRECONDITIONER"] = true;
            break;
         }
         case CPPASDBALANCEMODEVECTORS:
         {
            decompinfo["CPPASDBALANCEMODEVECTORS"] = true;
            break;
         }
         case PIVOTISEDCPALS:
         {
            decompinfo["PIVOTISEDCPALS"] = true;
            break;
         }
         case PIVOTISEDCPASD:
         {
            decompinfo["PIVOTISEDCPASD"] = true;
            break;
         }
         case CPALSGELSD:
         {
            decompinfo["CPALSGELSD"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPALSNEWTON:
         {
            decompinfo["CPALSNEWTON"] = true;
            break;
         }
         case CPALSLINESEARCH:
         {
            // Line search is only implemented with Newton modification
            decompinfo["CPALSNEWTON"] = true;
            decompinfo["CPALSLINESEARCH"] = true;
            break;
         }
         case CPNCGGENERALSOLVER:
         {
            decompinfo["CPNCGGENERALSOLVER"] = true;
            break;
         }
         case CPNCGMAXELEMENTCONVCHECK:
         {
            decompinfo["CPNCGMAXELEMENTCONVCHECK"] = true;
            break;
         }
         case CPNCGBALANCEMODEVECTORS:
         {
            decompinfo["CPNCGBALANCEMODEVECTORS"] = true;
            break;
         }
         case C2TSAFESVD:
         {
            decompinfo["C2TSAFESVD"] = true;
            break;
         }
         case CPNCGDIAGONALPRECONDITIONER:
         {
            decompinfo["CPNCGDIAGONALPRECONDITIONER"] = true;
            break;
         }
         case DEFAULTRESIDUALFITTER:
         {
            decompinfo["DEFAULTRESIDUALFITTER"] = true;
            break;
         }
         case HAGERZHANGTHETA:
         {
            decompinfo["HAGERZHANGTHETA"] = midas::input::GetNb(Minp, s);
            break;
         }
         case NESTEDCPRESTHRESHOLD:
         {
            decompinfo["NESTEDCPRESTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case NESTEDCPMAXRANK:
         {
            decompinfo["NESTEDCPMAXRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case NESTEDCPRANKINCR:
         {
            decompinfo["NESTEDCPRANKINCR"] = midas::input::GetIn(Minp, s);
            break;
         }
         case NESTEDCPSAFERESIDUALNORM:
         {
            decompinfo["NESTEDCPSAFERESIDUALNORM"] = true;
            break;
         }
         case CPNCG3PGRELTHRESHOLD:
         {
            decompinfo["CPNCG3PGRELTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPNCGRESTART:
         {
            decompinfo["CPNCGRESTART"] = midas::input::GetNb(Minp, s);
            break;
         }
         case NESTEDCPBALANCEMODEVECTORS:
         {
            decompinfo["NESTEDCPBALANCEMODEVECTORS"] = true;
            break;
         }
         case CPALSCONDITIONCHECK:
         {
            decompinfo["CPALSCONDITIONCHECK"] = true;
            break;
         }
         case NESTEDCPSCALERANKINCR:
         {
            decompinfo["NESTEDCPSCALERANKINCR"] = true;
            break;
         }
         case NESTEDCPADAPTIVERANKINCR:
         {
            decompinfo["NESTEDCPADAPTIVERANKINCR"] = true;
            break;
         }
         case NESTEDCPALLOWREFIT:
         {
            decompinfo["NESTEDCPALLOWREFIT"] = true;
            break;
         }
         case CPALSNORMREGULARIZATION:
         {
            decompinfo["CPALSRELATIVELAMBDA2"]  = midas::input::GetNb(Minp, s);
            break;
         }
         case CPALSSCALELSSYSTEM:
         {
            decompinfo["CPALSSCALELSSYSTEM"] = false;
            break;
         }
         case CPALSSCALEFITTENSOR:
         {
            decompinfo["CPALSSCALEFITTENSOR"] = true;
            break;
         }
         case CPALSINITIALERRORCALCULATION:
         {
            decompinfo["CPALSINITIALERRORCALCULATION"] = true;
            break;
         }
         case CPALSNODISTANCECONVCHECK:
         {
            decompinfo["CPALSDISTANCECONVCHECK"] = false;
            break;
         }
         case CPALSBALANCERESULT:
         {
            decompinfo["CPALSBALANCERESULT"] = true;
            break;
         }
         case PRESET:
         {
            // Get name of preset
            midas::input::GetLine(Minp, s);

            // Check that the preset exists, and get the preset info.
            const auto& preset_map = ::detail::GetTensorDecompPresetMap();
            auto& preset = util::FindElseError
                              (  s
                              ,  preset_map
                              ,  "PRESET: '" + std::string(s) + "' not found in preset map."
                              ).second;

            // loop over current decompinfo and set preset values, if they have not been set already.
            // NB: Preset does NOT override previously set values.
            for(auto& elem : preset)
            {
               auto iter = decompinfo.find(elem.first);
               if(iter == decompinfo.end())
               {
                  decompinfo[elem.first] = elem.second;
               }
            }

            break;
         }
         case EFFECTIVELOWORDERSVD:
         {
            decompinfo["EFFECTIVELOWORDERSVD"] = true;
            break;
         }
         case CPIDRESTHRESHOLD:
         {
            decompinfo["CPIDRESTHRESHOLD"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPIDRCOND:
         {
            decompinfo["CPIDRCOND"] = midas::input::GetNb(Minp, s);
            break;
         }
         case CPIDMAXRANK:
         {
            decompinfo["CPIDMAXRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPIDADDRANK:
         {
            decompinfo["CPIDADDRANK"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPIDMAXRANKINCR:
         {
            decompinfo["CPIDMAXRANKINCR"] = midas::input::GetIn(Minp, s);
            break;
         }
         case CPIDDISTRIBUTION:
         {
            midas::input::GetLine(Minp,s);
            auto found = id_distr_map.find(s);
            if (  found != id_distr_map.end()
               )
            {
               decompinfo["CPIDDISTRIBUTION"] = found->second;
            }
            else
            {
               MIDASERROR("CPIDDISTRIBUTION not found!");
            }
            break;
         }
         case CPIDDOSVD:
         {
            decompinfo["CPIDDOSVD"] = true;
            break;
         }
         case CPIDRANDOMIZEMATRIXID:
         {
            decompinfo["CPIDRANDOMIZEMATRIXID"] = true;
            break;
         }
         case CPIDABSCONV:
         {
            decompinfo["CPIDABSCONV"] = true;
            break;
         }
         case NODECOMPOSECANONICAL:
         {
            decompinfo["DECOMPOSECANONICAL"] = false;
            break;
         }
         case NODECOMPOSEDIRPROD:
         {
            decompinfo["DECOMPOSEDIRPROD"] = false;
            break;
         }
         case NODECOMPOSESUM:
         {
            decompinfo["DECOMPOSESUM"] = false;
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a TensorDecomp level " + std::to_string(input_level) + " Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }
   
   // if we read any input, we validate it and push back into set
   if(decompinfo.size() > 0)
   {
      // validate input and set defaault values.
      ::detail::ValidateTensorDecompInput(decompinfo);
      
      // add decomp info to set of decomp info's
      decompinfo_set.emplace(decompinfo);
   }

   // we have read, but not processed next keyword
   return true;
}
