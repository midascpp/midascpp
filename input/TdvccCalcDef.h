/**
 *******************************************************************************
 * 
 * @file    TdvccCalcDef.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCCALCDEF_H
#define TDVCCCALCDEF_H

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "input/TdPropertyDef.h"
#include "ode/OdeInfo.h"
#include "td/tdvcc/TdvccEnums.h"
#include "td/tdvcc/anal/AutoCorrSettings.h"
#include "td/oper/GaussPulse.h"

namespace midas::input
{

/**
 * Construct a definition of a Tdvcc calculation
 **/
class TdvccCalcDef
   :  public TdPropertyDef
{
   public:
      //! Constructor
      TdvccCalcDef() = default;

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read OdeInfo
      bool ReadOdeInfo
         (  std::istream&
         ,  std::string&
         );

      midas::ode::OdeInfo GetOdeInfo() const {return mOdeInfo;}

      bool LoadVccGs() const
      {
         using midas::tdvcc::EnumFromString;
         using midas::tdvcc::StatID;
         using midas::tdvcc::InitType;
         bool val = false;
         for(const auto& id: this->GetStats())
         {
            if (EnumFromString<StatID>(id) == StatID::DNORM2VCCGS)
            {
               val = true;
            }
         }
         if (this->GetInitStateType() == InitType::VCCGS)
         {
            val = true;
         }
         return val;
      }

      /** General/basic settings for TDVCC and TDMVCC **/
      //!@{
      CREATE_VARIABLE(std::string, Name, std::string{"tdvcc"});
      CREATE_VARIABLE(midas::tdvcc::CorrType, CorrMethod, midas::tdvcc::CorrType::VCC);
      CREATE_VARIABLE(Uin, MaxExci, 2);
      CREATE_VARIABLE(std::set<std::string>, ExtRangeModeLabels, std::set<std::string>{});
      CREATE_VARIABLE(midas::tdvcc::TrfType, Transformer, midas::tdvcc::TrfType::VCC2H2);
      CREATE_VARIABLE(std::string, ModalBasisFromVscf, std::string{"vscf"});
      CREATE_VARIABLE(std::vector<Uin>, LimitModalBasis, std::vector<Uin>{6});
      CREATE_VARIABLE(UNPACK(std::pair<bool,std::vector<Uin>>), TdModalBasis, 
                      UNPACK(std::pair<bool,std::vector<Uin>>{false,{1}}));
      CREATE_VARIABLE(std::string, VccGs, std::string{"vcc"});
      CREATE_VARIABLE(midas::tdvcc::InitType, InitStateType, midas::tdvcc::InitType::VSCFREF);
      CREATE_VARIABLE(std::string, Oper, std::string{"oper"});
      CREATE_VARIABLE(std::string, Basis, std::string{"basis"});
      CREATE_VARIABLE(bool, ImagTime, false);
      CREATE_VARIABLE(Nb, ImagTimeHaultThr, 0.0);
      CREATE_VARIABLE(Uin, IoLevel, 1);
      CREATE_VARIABLE(bool, TimeIt, false);
      CREATE_VARIABLE(Nb, PrintoutInterval, 0.0);
      CREATE_VARIABLE(bool, CompareFvciVecs, false);
      CREATE_VARIABLE(bool, UsePulse, false);
      CREATE_VARIABLE(std::vector<std::string>, OperPulse, std::vector<std::string>{});
      CREATE_VARIABLE(UNPACK(std::vector<std::map<midas::td::GaussParamID,Nb>>), PulseSpecs,
                      UNPACK(std::vector<std::map<midas::td::GaussParamID,Nb>>{}));
      CREATE_VARIABLE(UNPACK(std::pair<bool,Uin>), AnalyzeHermPartJacVccGs,
                      UNPACK(std::pair<bool,Uin>{false,0}));
      CREATE_VARIABLE(std::string, V3cFilePrefix, std::string{""});
      //!@}

      /** Writing parameters to file and restarting from file **/
      //!@{
      CREATE_VARIABLE(bool, PrintParamsToFile, false);
      CREATE_VARIABLE(bool, RestartFromFile, false);
      CREATE_VARIABLE(bool, UseInitStateForAutoCorr, false);

      //! Paths to do restart from (Params file, start state for autocorr)
      //CREATE_VARIABLE(UNPACK(std::pair<std::string, std::string>), RestartPaths, UNPACK(std::pair<std::string, std::string>("", "")));

      CREATE_VARIABLE(std::string, RestartPath, std::string(""));
      CREATE_VARIABLE(std::string, InitStateForAutoCorrPath, std::string(""));
      //!@}
      

      /** Modal plots and one-mode densities **/
      //!@{
      CREATE_VARIABLE(bool, SaveTdModalPlots, false);
      CREATE_VARIABLE(bool, UpdateOneModeDensities, false);
      CREATE_VARIABLE(In, UpdateOneModeDensitiesStep, I_0);
      CREATE_VARIABLE(bool, WriteOneModeDensities, false);
      CREATE_VARIABLE(In, SpaceSmoothening, I_0);
      CREATE_VARIABLE(In, TimeSmoothening, I_0);
      //!@}

      // Should this be kept?
      CREATE_VARIABLE(bool, PseudoTDH, false);
      CREATE_VARIABLE(bool, FullActiveBasis, false);
      
      /** Special for TDMVCC **/
      //!@{
      CREATE_VARIABLE(midas::tdvcc::ConstraintType, Constraint, midas::tdvcc::ConstraintType::VARIATIONAL);
      CREATE_VARIABLE(UNPACK(std::pair<midas::tdvcc::RegInverseType, Nb>), InvDensRegularization,
                      UNPACK(std::pair<midas::tdvcc::RegInverseType, Nb>{midas::tdvcc::RegInverseType::XSVD, C_I_10_8}));
      CREATE_VARIABLE(UNPACK(std::pair<midas::tdvcc::RegInverseType, Nb>), GOptRegularization,
                      UNPACK(std::pair<midas::tdvcc::RegInverseType, Nb>{midas::tdvcc::RegInverseType::XSVD, C_I_10_8}));
      CREATE_VARIABLE(bool, NonOrthoProjector, true);
      CREATE_VARIABLE(midas::tdvcc::AutoCorrSettings, AutoCorrSett, midas::tdvcc::AutoCorrSettings());
      CREATE_VARIABLE(midas::tdvcc::NonOrthoProjectorType, NonOrthoProjectorType, midas::tdvcc::NonOrthoProjectorType::ORIGINALBYUW)
      //!@}
      
   private:
      //! Info on integrator, time interval, etc.
      midas::ode::OdeInfo mOdeInfo;

   public:
      bool ReadNonOrthoProjector
         (  std::istream&
         ,  std::string&
         );

      bool ReadAutoCorrSettings
         (  std::istream&
         ,  std::string&
         );

      bool ReadRestartInfo
         (  std::istream&
         ,  std::string&
         );
};

//! Ostream
std::ostream& operator<<
   (  std::ostream&
   ,  const TdvccCalcDef&
   );

} /* namespace midas::input */

#endif /* TDVCCCALCDEF_H */

