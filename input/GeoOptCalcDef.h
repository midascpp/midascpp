/**
************************************************************************
* 
* @file                GeoOptCalcDef.h
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Class for a Elec Ecor wf input-info handler 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GEOOPTCALCDEF_H
#define GEOOPTCALCDEF_H

// Standard Headers
#include <string>
using std::string;
// Standard Headers
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

/**
* Construct a definition of a optimization
* */
class GeoOptCalcDef
{

   private:
      /**
       * General
       **/
      string mCalcName;    ///< Calc name
      In mIoLevel;         ///< Io level for scf calculation.

      /**
       * Restart options 
       **/
       bool     mRestart;               ///< Restart 
      
      /**
       * Info on single points  
       **/ 
      string mDatabase;
      string mCoordFile;

      /**
       * Info on modus for calculations
       **/ 
      bool mDoSample  = false;
      bool mDoEnergy  = false;
      bool mDoGrad    = false;
      bool mDoHessian = false;
      bool mDoOpt     = false;

      bool mAddGrad = false;
      string mFileAddGrad;


      /**
       * Special flags for analysis mode
       **/  
      bool mOnlyCoordAna = false;
      bool mOnlySimCheck = false;

      /**
       * Coordinate type
       **/ 
      CoordType coordtype = MINT;

      /**
       * Damping
       **/    
      Nb mDamp = 1.0;

      /**
       * Reading an storing Covariance matrix
       **/    
      bool mSaveCovar = false;
      bool mReadCovar = false;
      string mFileCovarRead;
      string mFileCovarSave;  

      /**
       * Information on the used kernel
       **/      
      std::string mKernel = "SC52";
 
   public:
      GeoOptCalcDef();                            
      ///< Constructor
//Gets and sets

//General
      void SetName(const string& arName) {mCalcName=arName;}
      ///< Set the calc name
      string GetName() const {return mCalcName;}
      ///< Get Name
      void SetIoLevel(const In& arIoLevel) {mIoLevel =arIoLevel;}
      ///< Set the IoLevel
      In GetIoLevel() const {return mIoLevel;}
      ///< Get the iolevel 

//Restart info 
      void SetRestart(bool aRestart) {mRestart=aRestart;}
      bool Restart() const {return mRestart;}

//Reading single points
      void SetDatabase(const string& arName) {mDatabase=arName;}
      string GetDatabase() const {return mDatabase;}

      void SetCoordFile(const string& arName) {mCoordFile=arName;}
      string GetCoordFile() const {return mCoordFile;}

//Options
      void SetCompModeSample(bool& arDo)   {mDoSample  = arDo;}
      void SetCompModeEnergy(bool& arDo)   {mDoEnergy  = arDo;}
      void SetCompModeGradient(bool& arDo) {mDoGrad    = arDo;}
      void SetCompModeHessian(bool& arDo)  {mDoHessian = arDo;}
      void SetCompModeOptimize(bool& arDo) {mDoOpt     = arDo;}

      bool GetCompModeSample()   const {return mDoSample;}
      bool GetCompModeEnergy()   const {return mDoEnergy;}
      bool GetCompModeGradient() const {return mDoGrad;}
      bool GetCompModeHessian()  const {return mDoHessian;}
      bool GetCompModeOptimize() const {return mDoOpt;}

      void SetAddGrad(bool& arDo) {mAddGrad = arDo;}
      bool GetAddGrad()     const {return mAddGrad;}

      void SetFileAddGrad(string& arfile) {mFileAddGrad = arfile;}
      string GetFileAddGrad() const {return mFileAddGrad;}

      void SetOnlyCoordAna(bool& arDo) {mOnlyCoordAna = arDo;}
      bool GetOnlyCoordAna()     const {return mOnlyCoordAna;}

      void SetOnlySimCheck(bool& arDo) {mOnlySimCheck = arDo;}
      bool GetOnlySimCheck()     const {return mOnlySimCheck;}

      void SetCoordinateType(CoordType arType) {coordtype = arType;}
      CoordType GetCoordinateType() {return coordtype;}

      void SetDamping(Nb arDamp) {mDamp = arDamp;}
      Nb GetDamping()      const {return mDamp;}

      void SetSaveCovar(bool& arDo) {mSaveCovar = arDo;}
      void SetReadCovar(bool& arDo) {mReadCovar = arDo;}
      bool GetSaveCovar() {return mSaveCovar;}
      bool GetReadCovar() {return mReadCovar;}
      void SetFileCovarSave(string& arfile) {mFileCovarSave = arfile;}
      void SetFileCovarRead(string& arfile) {mFileCovarRead = arfile;}
      string GetFileCovarSave() {return mFileCovarSave;}
      string GetFileCovarRead() {return mFileCovarRead;}

      void SetKernel(string& arkern) {mKernel = arkern;}
      string GetKernel() {return mKernel;}

};

#endif
