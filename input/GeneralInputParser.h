/**
************************************************************************
* 
* @file                GeneralInputParser.h
*
* Created:             31-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function datatype
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERALINPUT_H
#define GENERALINPUT_H

// Standard Headers
#include <iostream> 
#include <fstream> 
#include <string>
#include <vector>
#include <map>
using std::vector;
using std::string;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/**
* Construct a definition of a response calculation calculation
* */
class GeneralInputParser
{
   private:
      ///< level of input
      In mLevel;
      ///< stream
      std::istream& mStream;
      ///< Special key (consider making list of spec. keys)
      std::string mSpecKey;
      ///< global map
      std::map<string,string>* mGlobalMap;
      ///< local map list
      std::vector<std::map<string,string> >* mLocalMap;
      ///< The work horses: construct raw maps
      bool ConstructRawMaps();
      ///< ... and construct refined maps
      bool ConstructRefinedMaps();
      ///< specials
      vector<string> mSpecials;

   public:
      ///< Constructor
      GeneralInputParser(In,std::istream&,std::map<string,string>*,string="",vector<std::map<string,string> >* = NULL);
      ///< Output overload
      friend std::ostream& operator<<(std::ostream&, const GeneralInputParser&);
};

#endif

