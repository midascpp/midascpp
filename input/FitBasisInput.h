/**
************************************************************************
* 
* @file                FitBasisInput.cc
* 
* Created:             04-02-2014
*
* Author:              Bo Thomsen (ove@chem.au.dk)
*
* Short Description:   Forward declarations of the reader functions for
*                      reading a midas fit basis
* 
* Last modified: Mon Feb 04, 2014  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FITBASISINPUT_H_INCLUDED
#define FITBASISINPUT_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <utility>
#include <fstream>
#include <map>

// midas headers
#include "input/Input.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"

namespace midas
{
namespace input
{
/**
 * MidasCpp input reader for generic fitting functions. 
**/
std::string ReadFitFunction 
   (  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   ); 

namespace detail
{
/**
 * Optional reading of the basis set definitions from a seperate input file
**/
void ReadFitFunctionFile
   (  std::istream&
   ,  const std::string&
   );

} /*namespace detail*/
} /*namespace input*/
} /*namespace midas*/

#endif /* FITBASISINPUT_H_INCLUDED */
