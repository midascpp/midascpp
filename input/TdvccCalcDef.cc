/**
 *******************************************************************************
 * 
 * @file    TdvccCalcDef.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Midas Headers
#include "input/TdvccCalcDef.h"
#include "input/OdeInput.h"
#include "input/GetLine.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"

namespace midas::input
{

/**
 * Sanity check
 **/
void TdvccCalcDef::SanityCheck
   (
   )
{
   // Check that we have ODE-integrator info.
   if (  mOdeInfo.empty()
      )
   {
      MIDASERROR("No OdeInfo in TdvccCalcDef!");
   }

   // Checks on TdPropertyDef
   TdPropertyDef::SanityCheck(this->mOdeInfo, this->mImagTime);

   // Check that valid enum-strings are given.
   // The EnumFromString causes MIDASERROR if invalid string.
   for(const auto& val: this->GetProperties())
   {
      midas::tdvcc::EnumFromString<midas::tdvcc::PropID>(val);
   }
   for(const auto& val: this->GetStats())
   {
      midas::tdvcc::EnumFromString<midas::tdvcc::StatID>(val);
   }

   // Check that TD modal basis is given if using TEM method.
   if (  midas::tdvcc::IsTdmMethod(this->GetCorrMethod())
      && !this->GetTdModalBasis().first
      )
   {
      MIDASERROR
         (  "Requested '"+midas::tdvcc::StringFromEnum(this->GetCorrMethod())+"' calc., "
         +  "but TdModalBasis not set."
         );
   }

   // Check other TDMVCC input
   if (midas::tdvcc::IsTdmMethod(this->GetCorrMethod()))
   {
      if (this->GetPseudoTDH() && this->GetFullActiveBasis())
      {
         MIDASERROR("Both FullActiveBasis and PseudoTdh are set to true, this is not allowed !!");
      }
      const auto& prim_bas = this->GetLimitModalBasis();
      const auto& td_bas = this->GetTdModalBasis().second;
      if (prim_bas.size() != td_bas.size())
      {
         MIDASERROR("Unequal number of modes for #3 LIMITMODALBASIS and #3 TDMODALBASIS !");
      }
      if (this->GetFullActiveBasis())
      {
         for (In i_mode = I_0; i_mode < td_bas.size(); ++i_mode)
         {
            if (prim_bas[i_mode] != td_bas[i_mode])
            {
               MIDASERROR("There should be an equal number of td- and prim-modals for each mode in full active basis tdmvcc !");
            }
         }

         if (this->GetTransformer() != midas::tdvcc::TrfType::VCC2H2)
         {
            MIDASERROR("#3 FullActiveBasis keyword should not be given for non vcc2h2 transformers.");
         }
      }
   }
}

/**
 * Read OdeInfo
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next keyword?
 **/
bool TdvccCalcDef::ReadOdeInfo
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Overwrite mOdeInfo
   return OdeInput(arInp, arS, 4, this->mOdeInfo);
}

/**
 * Read type of non orthogonal active space projector to be used for Tdmvcc
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next keyword?
 **/
bool TdvccCalcDef::ReadNonOrthoProjector
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   midas::input::GetLine(arInp, arS);
   if (midas::input::IsKeyword(arS))
   {
      SetNonOrthoProjectorType(midas::tdvcc::NonOrthoProjectorType::ORIGINALBYUW);
      return true;
   }
   else
   {
      SetNonOrthoProjectorType(midas::tdvcc::EnumFromString<midas::tdvcc::NonOrthoProjectorType>(midas::input::ToUpperCase(arS)));
      return false;
   }
}


/**
 * Read restart paths
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read?
 **/
bool TdvccCalcDef::ReadRestartInfo
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Keyword input level (#4)
   In input_level = 4;

   // Enum with input flags
   enum INPUT  {  ERROR
               ,  RESTARTPATH
               ,  INITSTATEFORAUTOCORRPATH
               };

   // Map between flag and string
   const std::map<std::string, INPUT> input_word =
   {  
      {"#"+std::to_string(input_level)+"RESTARTPATH"                , RESTARTPATH                },
      {"#"+std::to_string(input_level)+"INITSTATEFORAUTOCORRPATH"   , INITSTATEFORAUTOCORRPATH   }
   };

   // Initialization of default values
   std::string s_orig;
   bool already_read = false;

   // Read until input levels point up
   while ( ( already_read || midas::input::GetLine(arInp, arS) )
         && !midas::input::IsLowerLevelKeyword(arS, input_level)
         ) 
   {
      already_read = false;
      s_orig = arS;
      arS = midas::input::ParseInput(arS);

      INPUT input = midas::input::FindKeyword(input_word, arS);

      switch(input)
      {
         case RESTARTPATH:
         {  
            midas::input::GetLine(arInp, arS);
            SetRestartPath(arS);
            break;
         }
         case INITSTATEFORAUTOCORRPATH:
         {
            SetUseInitStateForAutoCorr(true);
            midas::input::GetLine(arInp, arS);
            SetInitStateForAutoCorrPath(arS);
            break;
         }
         case ERROR:
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a RESTARTFROMFILE level " + std::to_string(input_level) + " Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // We have read, but not processed next keyword
   return true;
}

/**
 * Read settings for autocorrelation function
 *
 * @param arInp    Input file stream.
 * @param arS      Currently read string.
 * @return         Already read next keyword?
 **/
bool TdvccCalcDef::ReadAutoCorrSettings
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Keyword input level (#4)
   In input_level = 4;

   // Struct to hold settings
   midas::tdvcc::AutoCorrSettings settings;

   // Enum with input flags
   enum INPUT  {  ERROR
               ,  ALGORITHM
               ,  MAXLEVEL
               ,  SCREENTHRES
               ,  DELTATHRES
               ,  NUMTHRES
               ,  CALCTYPEA 
               ,  CALCTYPEB
               };

   // Map between flag and string
   const std::map<std::string, INPUT> input_word =
   {  
      {"#"+std::to_string(input_level)+"ALGORITHM"  , ALGORITHM  },
      {"#"+std::to_string(input_level)+"MAXLEVEL"   , MAXLEVEL   },
      {"#"+std::to_string(input_level)+"SCREENTHRES", SCREENTHRES},
      {"#"+std::to_string(input_level)+"DELTATHRES" , DELTATHRES },
      {"#"+std::to_string(input_level)+"NUMTHRES"   , NUMTHRES   },
      {"#"+std::to_string(input_level)+"CALCTYPEA"  , CALCTYPEA  },
      {"#"+std::to_string(input_level)+"CALCTYPEB"  , CALCTYPEB  }
   };

   // Initialization of default values
   std::string s_orig;
   bool already_read = false;
   bool screen = false;
   bool trunc  = false;

   // Read until input levels point up
   while ( ( already_read || midas::input::GetLine(arInp, arS) )
         && !midas::input::IsLowerLevelKeyword(arS, input_level)
         ) 
   {
      already_read = false;
      s_orig = arS;
      arS = midas::input::ParseInput(arS);

      INPUT input = midas::input::FindKeyword(input_word, arS);

      switch(input)
      {
         case ALGORITHM:
         {  
            midas::input::GetLine(arInp, arS);
            settings.mAlgorithm = midas::tdvcc::EnumFromString<midas::tdvcc::AutoCorrAlgo>(arS);
            break;
         }
         case MAXLEVEL:
         {
            settings.mMaxLevel = midas::input::GetIn(arInp, arS);
            trunc = true;
            break;
         }
         case SCREENTHRES:
         {
            settings.mScreenThres = midas::input::GetNb(arInp, arS);
            screen = true;
            break;
         }
         case DELTATHRES:
         {
            settings.mDeltaThres = midas::input::GetNb(arInp, arS);
            screen = true;
            break;
         }
         case NUMTHRES:
         {
            settings.mNumThres = midas::input::GetIn(arInp, arS);
            screen = true;
            break;
         }
         case CALCTYPEA:
         {
            settings.mCalcTypeA = true;
            break;
         }
         case CALCTYPEB:
         {
            settings.mCalcTypeA = true;
            break;
         }
         case ERROR:
         default:
         {
           Mout << " Keyword " << s_orig << 
               " is not a TDMVCC2AUTOCORR level " + std::to_string(input_level) + " Input keyword - input flag ignored! " << endl;
           MIDASERROR(" Check your input please ");
         }
      }
   }

   // Sanity check
   if (  ((settings.mAlgorithm != midas::tdvcc::AutoCorrAlgo::TRUNCATE) && trunc)
      || ((settings.mAlgorithm != midas::tdvcc::AutoCorrAlgo::SCREEN)   && screen)
      || ((settings.mAlgorithm != midas::tdvcc::AutoCorrAlgo::FULL)     && (screen || trunc))
      )
   {
      MIDASERROR("Inconsistent input under #3 TDMVCC2AUTOCORR!");
   }

   // Warning
   if (settings.mAlgorithm == midas::tdvcc::AutoCorrAlgo::SCREEN)
   {
      MidasWarning("The screening algorithm for TDMVCC autocorrelation functions is experimental. Use at own risk!");
   }

   // Set settings
   SetAutoCorrSett(settings);

   // We have read, but not processed next keyword
   return true;
}

/**
 * Ostream overload for TdvccCalcDef
 *
 * @param aOs
 * @param aTcd
 * @return
 *    os
 **/
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const TdvccCalcDef& aTcd
   )
{
   aOs << "TdvccCalcDef::" << aTcd.GetName() << std::endl;
   return aOs;
}

} /* namespace midas::input */
