/**
************************************************************************
* 
* @file                RspFunc.cc
*
* Created:             30-04-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains Response Functions 
* 
* Last modified: Fri Dec 04, 2009  11:15AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <set>
using std::set;

// Link to Standard Headers
#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/RspFunc.h"
#include "input/Contribution.h"


typedef string::size_type Sst;


typedef string::size_type Sst;

/**
* Construct a default definition of a  calculation
* */
RspFunc::RspFunc(In aNorder): mGamma(0), mImValue(0)
{
   mNorder                              = aNorder;
   mRightState                          = -I_1; // unassigned flag
   mLeftState                           = -I_1; // unassigned flag
   mHasBeenEval                         = false; 
   mSos                                 = false; 
   mValue                               = C_NB_MAX; 
   mNoTwoNPlusTwo                       = false;
   mUseMvec                             = true;
   mAsymmetric                          = false; 
   mApproxRspFunc                       = false; 
   Operators().resize(abs(aNorder) % I_10);
   mRspFrqs.resize(abs(aNorder) % I_10);
}

void RspFunc::SetRspOrder(In aNorder)
{
   mNorder                              = aNorder;
   Operators().resize(abs(aNorder) % I_10);
   mRspFrqs.resize(abs(aNorder) % I_10);
}

Contribution RspFunc::ToContribution()
{
   // convert response function to contribution format
   vector<OpInfo> infos;
   for(In i=0;i<Operators().size();i++) {
      OpInfo oi=OpDef::msOpInfo[Operators()[i]];
      infos.push_back(oi);
   }
   vector<Nb> frq_vec;
   for(In i=0;i<Operators().size()-1;i++)
      frq_vec.push_back(mRspFrqs[i+1]);
   Contribution c(infos,frq_vec);
   c.SetValue(mValue);
   if(mAsymmetric)
      c.SetSpecial("Asymmetric");
   return c;
}

/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const RspFunc& arRspFunc)
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, arOut);
   arOut.setf(ios::showpoint);
   
   arOut << arRspFunc.TypeString();
   In n_ord = arRspFunc.GetNorder();
   
   if (abs(n_ord) >I_1 && n_ord!=-I_11)
   { 
      if(arRspFunc.GetDoAsymmetric())
         arOut << "^-";
      arOut << "(";
      for (In i=I_1;i<abs(n_ord);i++)
      {
         arOut << arRspFunc.GetRspFrq(i);
         if (i!=abs(n_ord)-I_1) arOut << ",";
      }
      if(arRspFunc.ComplexRspFunc())
      {
         arOut << ",gamma=" << arRspFunc.mGamma;
      }
      arOut << ")";
   }
   if (arRspFunc.mHasBeenEval)
   {
      arOut << " = " << arRspFunc.mValue;
      if(arRspFunc.ComplexRspFunc())
      {
         arOut << " + i*" << arRspFunc.mImValue;
      }
   }

   if (arRspFunc.mNorder==I_1 && arRspFunc.mNoTwoNPlusTwo)
      arOut << " No 2n+2";
   if (arRspFunc.mNorder==-I_1 && !arRspFunc.UseMvec())
      arOut << " not using M vector";

   return arOut;
}
/**
* overload for <, compare i)order ii)ops iii) frqs 
* */
bool operator<(const RspFunc& ar1,const RspFunc& ar2)
{
   //In s1 = ar1.GetNorder();
   //In s2 = ar2.GetNorder();
   In abss1 = abs(ar1.GetNorder())%10;
   In abss2 = abs(ar2.GetNorder())%10;
   In s1 = ar1.GetNorder();
   In s2 = ar2.GetNorder();
   if (s1 != s2) 
   {
      return (s1 < s2);
   }
   else 
   {
      for (In i=I_0;i<min(abss1,abss2);i++)
         if (ar1.GetRspOp(i) != ar2.GetRspOp(i)) 
            return (ar1.GetRspOp(i) < ar2.GetRspOp(i));
      for (In i=I_0;i<min(abss1,abss2);i++)
      {
         Nb c_tol = C_NB_EPSILON*max(fabs(ar1.GetRspFrq(i)),
                                  fabs(ar2.GetRspFrq(i)))*C_10_2;
         //Mout << fabs(ar1.GetRspFrq(i)-ar2.GetRspFrq(i)) << endl;
         //Mout << c_tol << endl; 
         if (fabs(ar1.GetRspFrq(i)-ar2.GetRspFrq(i)) > c_tol) 
            return (ar1.GetRspFrq(i) < ar2.GetRspFrq(i));
      }
      if (ar1.RightState()!=ar2.RightState())
          return (ar1.RightState() < ar2.RightState());
      if (ar1.LeftState()!=ar2.LeftState())
          return (ar1.LeftState() < ar2.LeftState());
      if (ar1.GetDoAsymmetric() < ar2.GetDoAsymmetric()) 
         return ar1.GetDoAsymmetric() < ar2.GetDoAsymmetric();
      if (ar1.Gamma() < ar2.Gamma()) 
         return ar1.Gamma() < ar2.Gamma();
      return false;
   }
}
/**
* overload for =, compare i)order ii)ops iii) frqs 
*                 iv) special
* */
bool operator==(const RspFunc& ar1,const RspFunc& ar2)
{
   In s1 = abs(ar1.GetNorder())%10;
   In s2 = abs(ar2.GetNorder())%10;
   In order1 = ar1.GetNorder();
   In order2 = ar2.GetNorder();
   if (order1 != order2) 
   {
      return false;
   }
   else 
   {
      for (In i=I_0;i<min(s1,s2);i++)
         if (ar1.GetRspOp(i) != ar2.GetRspOp(i)) return false;
      for (In i=I_0;i<min(s1,s2);i++)
      {
         Nb c_tol = C_NB_EPSILON*max(fabs(ar1.GetRspFrq(i)),
                                  fabs(ar2.GetRspFrq(i)))*C_10_2;
         if (fabs(ar1.GetRspFrq(i)-ar2.GetRspFrq(i)) > c_tol) 
            return false;
      }
      if (ar1.RightState()!=ar2.RightState()) return false;
      if (ar1.LeftState()!=ar2.LeftState()) return false;
      if (ar1.GetDoAsymmetric()!=ar2.GetDoAsymmetric()) return false;
      if (ar1.Gamma()!=ar2.Gamma()) return false;

      return true;
   }
}

std::string RspFunc::TypeString() const
{
   std::string type;
   In n_ord = GetNorder();
   if (n_ord>I_1)
   {
      type += "<<";
   }
   else
   {
      if (LeftState()== -I_1) 
      {
         type += "<";
      }
      else 
      {
         type +=  "<" + std::to_string(LeftState()) + "|";
      }
   }
   
   // n_ord = -11 => <i|X|j> matrix element, only one op.
   In AbsOrder=abs(n_ord);
   if (n_ord == -11) 
   {
      AbsOrder=1;
   }
   for (In i=I_0;i<AbsOrder;i++)
   {
      type += GetRspOp(i);
      if (i!=abs(n_ord)-I_1 && n_ord!=-I_11)
      {
         if (i>I_0)
         {
            type += ",";
         }
         else
         {
            type += ";";
         }
      }
   }
   
   if (n_ord>I_1)
   {
      type += ">>";
   }
   else
   {
      if (RightState()== -I_1) 
      {
         type += ">";
      }
      else 
      {
         type += "|" + std::to_string(RightState()) + ">";
      }
   }

   return type;
}

/**
 *
 **/
void RspFunc::AssociateFile(const std::vector<std::string>& opers
                          , const std::string& label
                          , const std::string& file
                          )
{
   // assert that we actually have the operators
   MidasAssert(HasOperator(opers), "associating file for operators which are not in RspFunc.");
   
   // make the operstring
   std::string oper_string;
   for(auto& oper: opers)
   {
      oper_string += oper + "-";
   }
   oper_string += "_";
   
   // add label 
   oper_string += label;
   
   //check that we do not have other files associated with this specific label
   for(auto& file: mFiles)
   {
      if(file.first == oper_string) return; // label already associated, just return
   }
   
   // and lastly associate file
   mFiles.emplace_back(oper_string,file);
}
