#ifndef ANALYSISCALCDEF_H_INCLUDED
#define ANALYSISCALCDEF_H_INCLUDED

#include "input/GetLine.h"
#include "input/IsKeyword.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/Ranges.h"
#include "analysis/RspIr.h"
#include "analysis/RspMerger.h"
#include "analysis/LinRspRaman.h"

class AnalysisCalcDef
{
   using RspIrInput = RspIr::RspIrInput;

   private:
      std::vector<RspIrInput> mRspIr;
      std::vector<LinRspRaman> mLinRspRaman;
      std::vector<RspMerger> mRspMerger;
      bool mRspMergerGatherFiles = false;
      
      // spectrum analusis
      In mSpectrumAnalysisPoints = 1;
      bool mSpectrumAnalysisInterpolate = false;
      Nb mSpectrumAnalysisInterpolateStep = 0.1; // 0.1 cm^-1
      bool mSpectrumCalculateWeights = true;

   public:
      explicit AnalysisCalcDef(): 
         mRspIr()
       , mLinRspRaman()
       , mRspMerger()
      {
      }
      
      bool CreateAndReadRspIr(std::istream& Minp, std::string& s)
      {
         //// read range (range does nothing at the moment)
         //midas::input::GetLine(Minp,s); 
         //midas::input::AssertNotKeyword(s);
         //auto range = midas::util::CreateRangeFromString<Nb>(s);
         auto range = midas::util::CreateRangeFromString<Nb>("[1..100;1]"); // THIS IS NOT USED LATER, RANGE IS NOT FULLY IMPLEMENTED
         
         // read rsp funcs
         midas::input::GetLine(Minp,s);
         midas::input::AssertNotKeyword(s);
         auto rsp_vec = midas::util::StringVectorFromString(s);
         mRspIr.emplace_back(RspIrInput{std::move(range),std::move(rsp_vec)});
         return false;
      }

      bool CreateAndReadLinRspRaman(std::istream& Minp, std::string& s)
      {
         std::vector<LinRspRamanInput> raman_opers;
         while(midas::input::GetLine(Minp,s) && midas::input::NotKeyword(s))
         {
            raman_opers.emplace_back(ParseLinRspRamanInput(s));
         }
         mLinRspRaman.emplace_back(std::move(raman_opers));
         return true;
      }

      bool CreateAndReadMergeRsp(std::istream& Minp, std::string& s)
      {
         // read rsp vec
         midas::input::GetLine(Minp,s);
         midas::input::AssertNotKeyword(s);
         auto rsp = s;
         // read files
         std::vector<std::string> file_vec;
         while(midas::input::GetLine(Minp,s) && midas::input::NotKeyword(s))
         {
            file_vec.emplace_back(s);
         }
         mRspMerger.emplace_back(std::move(rsp),std::move(file_vec));
         if(mRspMergerGatherFiles) mRspMerger.back().SetGatherFiles();
         return true; // we have read the next keyword
      }

      bool ReadRspMergerGatherFiles(std::istream& Minp, std::string& s)
      {
         mRspMergerGatherFiles = true;
         for(auto& merger: mRspMerger)
         {
            merger.SetGatherFiles();
         }
         return false; 
      }

      bool ReadSpectrumAnalysisPoints(std::istream& Minp, std::string& s)
      {
         midas::input::GetLine(Minp, s);
         midas::input::AssertNotKeyword(s);
         mSpectrumAnalysisPoints = midas::util::FromString<In>(s);
         return false; // we have not read next keyword
      }
      
      bool ReadSpectrumAnalysisInterpolate(std::istream& Minp, std::string& s)
      {
         mSpectrumAnalysisInterpolate = true;
         return false; // we have not read next keyword
      }
      
      bool ReadSpectrumAnalysisInterpolateStep(std::istream& Minp, std::string& s)
      {
         midas::input::GetLine(Minp, s);
         midas::input::AssertNotKeyword(s);
         mSpectrumAnalysisInterpolateStep = midas::util::FromString<Nb>(s);
         return false; // we have not read next keyword
      }
      
      bool ReadSpectrumAnalysisNoCalculateWeights(std::istream& Minp, std::string& s)
      {
         mSpectrumCalculateWeights = false;
         return false; // we have not read next keyword
      }

      bool DoRspIr() const
      {
         return (mRspIr.size() != 0);
      }
      
      bool DoRspMerge() const
      {
         return (mRspMerger.size() != 0);
      }
      
      bool DoLinRspRaman() const
      {
         return (mLinRspRaman.size() != 0);
      }

      friend void RspIrDrv(AnalysisCalcDef&);
      friend void RspMergeDrv(AnalysisCalcDef&);
      friend void LinRspRamanDrv(AnalysisCalcDef&);
};

#endif /* ANALYSISCALCDEF_H_INCLUDED */
