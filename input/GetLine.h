#ifndef GETLINE_H_INCLUDED
#define GETLINE_H_INCLUDED

#include <istream>

#include "inc_gen/TypeDefs.h"
#include "util/conversions/FromString.h"

namespace midas
{
namespace input
{

//! Read next line, trim it and return in str.
std::istream& GetLine(std::istream& is, std::string& str, char delim = '\n');

//!
std::istream& GetLineAsVector(std::istream& is, std::string& str, char delim = '\n');

//! Delete blanks and turn str to uper case.
std::string ParseInput(const std::string& str);

//! Delete blanks.
std::string ParsePathInput(const std::string& str);

//! Read next line, trim it, delete blanks, turn to upper case and return in str.
std::istream& GetParsedLine(std::istream& is, std::string& str, char delim = '\n');

//! Read next line, trim it, delete blanks and return in str.
std::istream& GetParsedPathLine(std::istream& is, std::string& str, char delim = '\n');

//! Read next line and convert to type T
template<class T>
T Get(std::istream& is, std::string& s)
{
   //std::string s;
   if(!GetLine(is,s))
   {
      MIDASERROR("Could not get line from file");
   }
   return midas::util::FromString<T>(s);
}

//! Read next line and convert to Nb
Nb GetNb(std::istream& is, std::string& s);

//! Read next line and convert to In
In GetIn(std::istream& is, std::string& s);

//! Read next line and convert to std::complex<double>
std::complex<double> GetComplexDouble(std::istream& is, std::string& s);

} /* namespace input */
} /* namespace midas */

#endif /* GETLINE_H_INCLUDED */
