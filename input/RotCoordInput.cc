/**
************************************************************************
* 
* @file                RotCoordInput.cc
*
* 
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the RotCoord module based on the
*                      input reader for the OptVscf modul written by 
*                      Bo Thomsen (bothomsen@chem.au.dk)
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/RotCoordCalcDef.h"
#include "input/GeneralInputParser.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "input/Trim.h"

// forward declarations
std::string GridFunctionInput(std::istream&, input::RotCoordGridCalcDef*, const In&);
std::string VscfForOptInput(std::istream&, input::RotCoordVscfCalcDef*, const In&);

/**
 *
 **/
std::string RotCoordInput
   (  std::istream& Minp
   ,  input::RotCoordCalcDef* apCalcDef
   ,  const In& arInputLevel
   )
{

   In input_level = arInputLevel + I_1;
   std::string s;//To be returned when we are done reading this level
   enum INPUT {ERROR, OPERSUFFIX, SWEEPS, CONVTHR, FREQSCREEN, 
          DOROTSATSWEEPEND,OPERSFORROT,PRINTROTMATTOFILE, 
          RANDOMSTART, MINROTANG, MEASURE, GRIDFUNCTION}; 
   
   const map<string,INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"CONVTHR",CONVTHR}
   ,  {"#"+std::to_string(input_level)+"DOROTSATSWEEPEND",DOROTSATSWEEPEND}
   ,  {"#"+std::to_string(input_level)+"OPERSUFFIX",OPERSUFFIX}
   ,  {"#"+std::to_string(input_level)+"SWEEPS",SWEEPS}
   ,  {"#"+std::to_string(input_level)+"OPERSFORROT",OPERSFORROT}
   ,  {"#"+std::to_string(input_level)+"PRINTROTMATTOFILE",PRINTROTMATTOFILE}
   ,  {"#"+std::to_string(input_level)+"RANDOMSTART",RANDOMSTART}
   ,  {"#"+std::to_string(input_level)+"MINROTANG",MINROTANG}
   ,  {"#"+std::to_string(input_level)+"FREQSCREEN",FREQSCREEN}
   ,  {"#"+std::to_string(input_level)+"MEASURE",MEASURE}
   ,  {"#"+std::to_string(input_level)+"GRIDFUNCTION", GRIDFUNCTION}
   };

   bool already_read=false;
   while (  (  already_read
            || midas::input::GetLine(Minp,s)
            )
         && !( s.substr(I_0,I_1)=="#"
            && atoi(s.substr(I_1,I_1).c_str()) < input_level
            )
         )  // readin until input levels point up
   {
      std::string s_orig = s;
      s = midas::input::ParseInput(s);    // Delete blanks and transform to upper-case
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
     
      switch(input)
      {
         case MINROTANG:
         {
            midas::input::GetLine(Minp, s);
            apCalcDef->SetMinRotAng(midas::util::FromString<Nb>(s));
            break;
         }
         case RANDOMSTART:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetRandomIter(midas::util::FromString<In>(s));
            break;
         }
         case OPERSFORROT:
         {
            midas::input::GetLine(Minp,s);
            In nr_lines = midas::util::FromString<In>(s);
            for(In i = I_0; i < nr_lines;++i)
            {
               midas::input::GetLine(Minp,s);
               apCalcDef->AddOperForRot(s);
            }
            break;
         }
         case DOROTSATSWEEPEND:
         {
            apCalcDef->SetDoRotsAtSweepEnd(true);
            break;
         }
         case CONVTHR:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetConvThr(midas::util::FromString<Nb>(s));
            break;
         }
         case PRINTROTMATTOFILE:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetRotMatFile(s);
            break;
         }
         case OPERSUFFIX:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetNewSuffix(s);
            break;
         }
         case SWEEPS:
         { 
            midas::input::GetLine(Minp,s);
            apCalcDef->SetNumberOfSweeps(midas::util::FromString<In>(s));
            break;
         }
         case FREQSCREEN:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetFreqScreenMaxFreq(midas::util::FromString<Nb>(s));
            apCalcDef->SetDoFreqScreen(true);
            break;
         }
         case MEASURE:
         {
            // First determine if it is a non-hybrid or hybrid optimization measure 
            midas::input::GetLine(Minp, s);
            auto measure_setup = midas::util::StringVectorFromString(s);
            MidasAssert(measure_setup.size() == 1 || measure_setup.size() == 2, "Measure argument can only be one or two strings.");

            auto measure = midas::input::ToUpperCase(measure_setup[0]);
            auto weight = (measure_setup.size() == 2) ? midas::util::FromString<Nb>(measure_setup[1]) : C_1;

            // Optimization
            if (  measure == "OPTIMIZATION"
               || measure == "OPT"
               || measure == "EVSCF"
               )
            {
               // Set VSCF input options
               input::RotCoordVscfCalcDef calcdef;
               s = VscfForOptInput(Minp, &calcdef, input_level);
               apCalcDef->EmplaceVscfMeasure(std::move(calcdef), weight);
               already_read = true;
            }
            // Non-hybrid localization shorthand
            else if  (  measure == "LOCALIZATION"
                     || measure == "LOC"
                     )
            {
               if (  apCalcDef->GetLocalize()
                  )
               {
                  MIDASERROR("You can only add localization measure once!");
               }

               apCalcDef->SetLocalize(true);
               apCalcDef->SetLocWeight(weight);
            }
            else
            {
               MIDASERROR("Unknown measure type: '" + measure + "'.");
            }
            break;
         }
         case GRIDFUNCTION:
         {
            s = GridFunctionInput(Minp, &apCalcDef->GetGridCalcDef(), input_level);
            already_read=true;
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a RotCoord level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check the CalcDef
   apCalcDef->SanityCheck();

   return s;
}
/**
* Read Grid function input
* */
std::string GridFunctionInput
   (  std::istream& Minp
   ,  input::RotCoordGridCalcDef* apCalcDef
   ,  const In& arInputLevel
   )
{
   std::string s;//To be returned when we are done reading this level
   enum INPUT{ERROR,FOURIERPT,NEWTONCONV,PLOTPOINTS,PLOTONLY,OPTPARAM,NUMDIFFDELTA};
   In input_level = arInputLevel + I_1;
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"FOURIERPT",FOURIERPT},
      {"#"+std::to_string(input_level)+"NEWTONCONV",NEWTONCONV},
      {"#"+std::to_string(input_level)+"PLOTPOINTS",PLOTPOINTS},
      {"#"+std::to_string(input_level)+"PLOTONLY",PLOTONLY},
      {"#"+std::to_string(input_level)+"OPTPARAM",OPTPARAM},
      {"#"+std::to_string(input_level)+"NUMDIFFDELTA",NUMDIFFDELTA},
   };


   while ((midas::input::GetLine(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))
   {
      std::string s_orig = s;
      s = midas::input::ParseInput(s);
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case FOURIERPT:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetFourierPt(midas::util::FromString<In>(s));
            break;
         }
         case NEWTONCONV:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetNewtConv(midas::util::FromString<Nb>(s));
            break;
         }
         case OPTPARAM:
         {
            midas::input::GetParsedLine(Minp,s);
            apCalcDef->SetOptParam(s);
            break;
         }
         case PLOTPOINTS:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetPlotPoints(midas::util::FromString<In>(s));
            break;
         }
         case PLOTONLY:
         {
            apCalcDef->SetPlotOnly(true);
            break;
         }
         case NUMDIFFDELTA:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetNumDiffDelta(midas::util::FromString<Nb>(s));
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a Grid level " << input_level << " Input keyword! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}
/**
* Read VscfForOpt input
* */
std::string VscfForOptInput
   (  std::istream& Minp
   ,  input::RotCoordVscfCalcDef* apCalcDef
   ,  const In& arInputLevel
   )
{
   std::string s; //To be returned when we are done reading this level
   enum INPUT{ERROR,NAME,CALCULATIONTYPE,KEEPMAXCOUPLEVEL,ROTZEROFIRST};
   In input_level = arInputLevel + I_1;
   const std::map<std::string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"NAME",NAME},
      {"#"+std::to_string(input_level)+"CALCULATIONTYPE",CALCULATIONTYPE},
      {"#"+std::to_string(input_level)+"KEEPMAXCOUPLEVEL",KEEPMAXCOUPLEVEL},
      {"#"+std::to_string(input_level)+"ROTZEROFIRST",ROTZEROFIRST},
   };


   while ((midas::input::GetLine(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))
   {
      std::string s_orig = s;
      s = midas::input::ParseInput(s);
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case NAME:
         {
            midas::input::GetLine(Minp,s);
            apCalcDef->SetName(s);
            break;
         }
         case CALCULATIONTYPE:
         {
            midas::input::GetLine(Minp,s);
            transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);// Transform to all upper.
            while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);//Delete all blanks
            apCalcDef->SetCalcType(s);
            break;
         }
         case KEEPMAXCOUPLEVEL:
         {
            apCalcDef->SetKeepMaxCoupLevel(true);
            break;
         }
         case ROTZEROFIRST:
         {
            apCalcDef->SetRotZeroFirst(true);
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a VscfForOpt level " << input_level << " Input keyword! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}
