/**
************************************************************************
* 
* @file                RspVecInf.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains all input-info that defines a Vscf calculation
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RSPVECINF_H
#define RSPVECINF_H

// Standard Headers
#include <string>
using std::string;
// Standard Headers
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/RspFunc.h"

/**
* Construct a quantity to hold information on rsp vectors 
* */
class RspVecInf: public RspFunc
{
   private:
      ///< Additional member info.
      bool mRight;                           ///< Left or Right vector
      bool mIsNonStandardType;               ///< is it FR,FT etc. 
      string mNonStandardType;               ///< FR,FT etc. 
   public:
      RspVecInf();                           ///< Constructor
      RspVecInf(In aNorder,bool aRight=true,bool mIsNonStandardType=false);       ///< Constructor
      void SetRight(bool aRight) {mRight = aRight;}
      bool GetRight() const {return mRight; }
      void SetNonStandardType(const string& aType)
      {mNonStandardType = aType; mIsNonStandardType=true;}
      
      bool IsNonStandardType() const {return mIsNonStandardType;}
      
      const string& GetNonStandardType() const {return mNonStandardType;}
      
      friend ostream& operator<<(ostream&, const RspVecInf&);  ///< Output overload

      friend bool operator<(const RspVecInf&, const RspVecInf&);  ///< Output overload
};

#endif

