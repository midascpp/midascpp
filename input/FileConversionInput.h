#ifndef FILECONVERIONINPUT_H_INCLUDED
#define FILECONVERIONINPUT_H_INCLUDED

#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeReader.h"
#include "input/GetLine.h"
#include "input/FindKeyword.h"
#include "input/IsKeyword.h"
#include "input/AbsPath.h"

using namespace midas;

/**
 * Driver for reading molecular file input.
 * Can both create a reader and writer dependent on template argument.
 **/
template<class FACTORY = molecule::MoleculeReaderFactory>
molecule::MoleculeFileInfo ReadMoleculeFileInput
   (  std::istream& Minp
   ,  std::string& s
   ,  bool& already_read
   ,  bool assert_file = true
   )
{
   // read file type
   std::string file_type;
   midas::input::GetLine(Minp,file_type);
   file_type = midas::input::ParseInput(file_type); // to upper, and delete blanks
   MidasAssert(FACTORY::check_key(file_type), "#2 MoleculeFile: type " + file_type + " not supported");
            
   // read file name
   std::string file_name;
   midas::input::GetLine(Minp,file_name);
   if(assert_file)
   {
      file_name = input::SearchForFile(file_name);
   }
   MidasAssert(!assert_file || InquireFile(file_name), "#2 MoleculeFile: filename: " + file_name + " not found");
   
   // read fields if any
   molecule::MoleculeFields fields;
   while (midas::input::GetParsedLine(Minp,s) && midas::input::NotKeyword(s))
   {
      fields.InsertMoleculeField(molecule::MoleculeFields::Map(s));
   }

   already_read = true;
   
   return {file_name, file_type, fields};
}

/**
 * Declaration of file conversion input driver
 **/
std::string FileConversionInput(std::istream& Minp);

#endif /*FILECONVERIONINPUT_H_INCLUDED */
