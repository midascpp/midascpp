/**
************************************************************************
* 
* @file                Contribution.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function framework
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef CONTRIBUTION_H
#define CONTRIBUTION_H

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream> 
using std::ostream;
#include <map>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpInfo.h"

/**
* Construct a definition of a total response calculation calculation
* Derived from RspFunc
* */
class Contribution
{
   private:
      In mMainOrder;
      In mVibRspOrder;
      std::vector<OpInfo> mOpInfos;
      std::vector<Nb>  mFrq;
      Nb mValue;
      bool mEval;
      std::string mSpecial;

   public:
      ///< Constructor. Needs a map of keys-values and an order
      ///< keys-values map contains all the remaining stuff
      Contribution(const std::vector<OpInfo>&, const std::vector<Nb>&);
      Contribution(const Contribution&);
      Contribution operator=(const Contribution&);

      In GetMainOrder() const {return mMainOrder;}
      In GetVibRspOrder() const {return mVibRspOrder;}
      void SetHasBeenEval(bool aB) {mEval=aB;}
      bool HasBeenEval() const {return mEval;}
      void SetSpecial(const string& aS) {mSpecial=aS;}
      string GetSpecial() const {return mSpecial;}
      void SetValue(Nb aN) {mValue=aN;}
      Nb GetValue() const {return mValue;}
      Nb GetRefValue() const {return mValue;}
      void SetFrq(const vector<Nb>& aV) {mFrq=aV;}
      vector<Nb> GetFrq() const {return mFrq;}
      Nb GetFrq(In aI) const {return mFrq[aI];}
      OpInfo GetOpInfo(In aI) const {return mOpInfos[aI];}
      vector<OpInfo> GetVecOfOpInfo() const {return mOpInfos;}
      
      friend bool operator<(const Contribution&,const Contribution&);
      friend bool operator==(const Contribution&,const Contribution&);
      friend ostream& operator<<(ostream&,const Contribution&);
};

#endif

