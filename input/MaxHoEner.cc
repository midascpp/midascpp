/**
************************************************************************
* 
* @file                MaxHoEner.cc
*
* Created:             8-11-2006
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for finding VSCF occ vectors based on HO energy 
* 
* Last modified: Mon Jan 22, 2007  09:25AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "input/MaxHoEner.h"

// Standard headers:
#include <vector>
using std::vector;
// Link to standard headers:
#include "inc_gen/math_link.h"
// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/Input.h"

#include "mpi/FileToString.h"
//
//

/**
 * Constructuctor 
 **/
MaxHoEner::MaxHoEner
   (  vector<In> aInVec
   ,  const std::string& aOper
   ,  Nb aE
   ) 
   :  mOmeVec(aInVec.size())
   ,  mMaxVec(aInVec)
   ,  mEnerMax(aE)
{
   Mout << " in MaxHoEner constructor " << endl;
   // find operator
   //In oper_nr = gOperatorDefs.GetOperatorNr(aOper);
   //OpDef* op_def = &gOperatorDefs[oper_nr];
   OpDef* op_def = &(gOperatorDefs.GetOperator(aOper));
   In n_modes_in_oper = op_def->NmodesInOp();
   
   // load in omegas
   for(In i = 0; i < n_modes_in_oper; ++i)
   {
      mOmeVec[i] = op_def->HoOmega(i);
   }

   // do some output
   Mout << " HO frequencies for " << n_modes_in_oper << " modes " << endl;
   for (In i = I_0; i < n_modes_in_oper; i++) 
   {
      Mout << " i " << i << " " << mOmeVec[i] << endl;
   }
}

/**
 * * Get All Ivecs 
 * * */
void MaxHoEner::GetAllOccVecs(vector<InVector>& arIvecVec)
{
   In n_modes = mOmeVec.size();
   if (n_modes!= mMaxVec.size()) MIDASERROR("Size mismatch MaxHoEner::GetAllOccVecs");
   // If mMinMax is given as something different from 0 0 0 then use it else not
   bool non_zero_max=false;
   In i=-I_1;
   while (!non_zero_max && ++i<mMaxVec.size()) if (mMaxVec[i]!=I_0) non_zero_max=true;  
   if (non_zero_max) Mout << " Maximum occopations given " << mMaxVec << endl;
   if (!non_zero_max) Mout << " No non-trivial maximum occopations given " << mMaxVec << endl;
  
   for (In i=I_0;i<n_modes;i++)
   {
      In j=I_0;
      while (fabs(mOmeVec[i])*j < mEnerMax) j++;  // Energy above the ground state 
      if (non_zero_max) mMaxVec[i] = min(--j,mMaxVec[i]);
      else mMaxVec[i] = --j;
   }
   Mout << "Maximum vector " << mMaxVec << endl;

   In i_mode=I_0;
   Nb aEtot=C_0;
   vector<In> a_current_vec(n_modes);
   for (In i=I_0;i<n_modes;i++) a_current_vec[i]=I_0;
   RecursAdd(i_mode,arIvecVec,aEtot,a_current_vec);

   Mout << " Nr of integer vectors = " << arIvecVec.size() << endl;
   for (In i=I_0;i<arIvecVec.size();i++)
   {
      Nb e=C_0;
      for (In j=I_0;j<arIvecVec[i].size();j++) e+=arIvecVec[i][j]*mOmeVec[j];
      Mout << " Ivec " << arIvecVec[i] << " Ho E = " << e << endl; 
   }
   Mout << " All " << arIvecVec.size() << " have HO energies below " << mEnerMax << endl;
}

/**
 * * All i-vecs with low enough energy recursively 
 * * */
void MaxHoEner::RecursAdd(In aMode,vector<InVector>& arIvecVec,Nb aEtot,vector<In> aCurrentOccVec)
{
   for (In v=I_0;v<=mMaxVec[aMode];v++)
   {
      Nb e_tot = aEtot+v*mOmeVec[aMode]; // Energy relative to zero-point level. 
      aCurrentOccVec[aMode]=v;
      //Mout << " aCurrentOccVec : " << aCurrentOccVec << " " << e_tot<< " aMode " << aMode << endl;
      if (aMode+I_1 == mMaxVec.size() && e_tot  < mEnerMax) arIvecVec.push_back(aCurrentOccVec);
      else if (e_tot < mEnerMax) RecursAdd(aMode+I_1,arIvecVec,e_tot ,aCurrentOccVec);
      if (e_tot >mEnerMax) break;
   }
   return; 
}
