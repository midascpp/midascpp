/**
************************************************************************
* 
* @file                BasDef.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains information on the complete primitive basis set 
* 
* Last modified: Mon May 10, 2010  01:52PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

//std headers
#include <vector>
#include <utility>
using std::vector;
using std::make_pair;
//midas headers
#include "input/Input.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "input/OneModeBasDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mpi/Impi.h"
#include "mathlib/Taylor/taylor.hpp"
#include "util/conversions/TupleFromString.h"
#include "input/ProgramSettings.h"

/**
 * Reserve place on STL containers for efficiency and security, see comment in input file.
**/
void BasDef::Reserve()
{
   mVecOneModeBasDef.reserve(midas::input::gProgramSettings.GetGeneralSettings().GetReserveModes());
}

/**
 * Here we figure out if we actually need to generate a new set of basis functions
**/
void BasDef::InitBasis(const OpDef& arOpDef)
{
   // Only one set of primitive basis functions can be used at the same time
   auto check = [](bool key, bool& found)
   {
      if (key)
      {
         if (found) 
         {
            MIDASERROR("Only one of the keywords #3 HOBASIS, #3 GAUSSIANBASIS and #3 BSPLINEBASIS can be set at the same time, check your input please");
         }
         else
         {
            found = true;
         }
      }
   };
   
   bool found = false;
   check(mUseHoBasis,       found);
   check(mUseGaussianBasis, found);
   check(mUseBsplineBasis,  found);

   if (!found && !mUserDefinedBasis)
   {
      MIDASERROR("No primitive basis have been chosen for representing the wave function, please choose one");
   }

   //
   if (mUseHoBasis && mAutoGenBasis) 
   {
      MIDASERROR("Keywords #3 NPRIMBASISFUNCTIONS and #3 PRIMBASISDENSITY cannot be used with the #2 HOBASIS keyword");
   }

   // Use harmonic oscillator functions as primitive basis
   if (mUseHoBasis) 
   {
      HoBasisFromOp(arOpDef); 
   }
   // Use distributed Gaussian or B-spline functions as primitive basis
   else if (mAutoGenBasis) 
   {
      GenBasis(arOpDef);
   }

   // Plot basis for given modes.
   this->PlotBasis("_" + arOpDef.Name());
}

/**
 * Prepare basis set based on HO part of operator 
**/
void BasDef::HoBasisFromOp(const OpDef& arOpDef)
{
   this->Clear();
   SetmBasSetType("Product");
   std::string s_type = "HO";
   std::vector<In> ho_quantum_nrs;
   for (LocalModeNr i_op_mode = I_0; i_op_mode < arOpDef.NmodesInOp(); i_op_mode++)
   {
      // If using state-transfer operators, we simply push_back empty OneModeBasDef%s
      if (  arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_op_mode))
         )
      {
         gOneModeStateBasis.push_back(OneModeStateBasis(arOpDef.GetGlobalModeNr(i_op_mode), arOpDef.StateTransferIJMax(i_op_mode) + I_1));
         In add = gOneModeStateBasis.size() - I_1;
         OneModeBasDef bas_def("StateBasis", add);
         In g_mode = i_op_mode;
         AddToBasDef(bas_def, g_mode);
         continue;
      }

      Nb omeg = C_1; 
      if (mUsingScalingFreqs)
      {
         omeg = std::pow(arOpDef.ScaleFactorForMode(i_op_mode), C_2);
      }
      else 
      {
         Nb c_tot_ho = C_0;
         std::string mode_label = gOperatorDefs.GetModeLabel(arOpDef.GetGlobalModeNr(i_op_mode));
         LocalOperNr q_sqr_oper = arOpDef.GetQSquareOpForMode(i_op_mode);
         for (In i_act_term = I_0; i_act_term < arOpDef.NactiveTerms(i_op_mode); i_act_term++)
         {
            In i_term = arOpDef.TermActive(i_op_mode, i_act_term);
            In n_fac = arOpDef.NfactorsInTerm(i_term);

            if (mode_label == std::to_string(i_op_mode + I_1)) // old .mop format
            {
               if (n_fac > I_1)
               {
                  continue;
               }
            }
            else // new .mop format
            {
               if (n_fac != I_1)
               {
                  continue;
               }
            }

            In i_fac = I_0;
            LocalOperNr i_oper = arOpDef.OperForFactor(i_term, i_fac);
            
            if (mode_label == std::to_string(i_op_mode + I_1)) // old .mop format
            {
               if (i_oper != q_sqr_oper)
               {
                  continue;
               }
            }
            else // new .mop format 
            {
               if (gOperatorDefs.GetOperLabel(arOpDef.GetOneModeOperGlobalNr(i_op_mode, i_oper)) != "Q^2")
               {
                  continue;
               }
            }
            Nb fact = arOpDef.Coef(i_term); // now term is equal to " C*Q^2 "
            c_tot_ho += fact;
         }
         omeg = std::sqrt(C_2 * std::fabs(c_tot_ho));
         
         //
         if (arOpDef.ScaledPot())
         {
            omeg *= arOpDef.ScaleFactorForMode(i_op_mode);
         }

         // Sanity check
         if (omeg == C_0)
         {
            MIDASERROR("No Q^2 operator found for automatic generation of HO basis!");
         }
      }
      
      // Sanity check
      if (omeg < C_0)
      {
         MIDASERROR("Negative harmonic frequency detected for mode Q" + std::to_string(i_op_mode) + ": " + std::to_string(omeg));
      }

      // Output
      if (gBasisIoLevel > I_5)
      {
         Mout << " HoBasisFromOp omega = " << omeg << std::endl;
      }

      // If only one quantum number is given, then we extend this to primitive basis for all modes
      In ho_quantum_nr = I_0;
      if (mHoQnrs.size() == I_1)
      {
         ho_quantum_nr = mHoQnrs[I_0];   
      }
      // If more than one quantum number is given, then we can give individual definitions to primitive bases for different modes
      else if (mHoQnrs.size() > I_1)
      {
         if (mHoQnrs.size() != arOpDef.NmodesInOp())
         {
            MIDASERROR("Size of vector given under the #3 HoBasis keyword is incorrect! This should match the number of modes in your system!");
         }
         ho_quantum_nr = mHoQnrs[i_op_mode];
      }
      else
      {
         MIDASERROR("Something is wrong with the basis definition given under the #3 HoBasis keyword!");
      }

      ho_quantum_nrs.push_back(ho_quantum_nr);
      gOneModeHoDefs.push_back(OneModeHoDef(arOpDef.GetGlobalModeNr(i_op_mode), s_type, ho_quantum_nr, omeg));
      In add = gOneModeHoDefs.size() - I_1;
      OneModeBasDef bas_def(s_type, add);
      AddToBasDef(bas_def, bas_def.GetGlobalModeNr());
   }

   // Make sure that our vector of harmonic oscillator quantum numbers is complete
   if (mHoQnrs.size() != arOpDef.NmodesInOp())
   {
      for (LocalModeNr i_op_mode = I_1; i_op_mode < arOpDef.NmodesInOp(); i_op_mode++)
      {
         if (  !arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_op_mode))
            )
         {
            mHoQnrs.push_back(ho_quantum_nrs[i_op_mode]);
         }
         else
         {
            mHoQnrs.push_back(-I_1);
         }
      }
   }

   // Define type!
   bool allho = true; 
   for (In i_l_b = I_0; i_l_b < mNoModesInBas; i_l_b++) 
   {
      if (  !arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_l_b))
         && OneModeType(i_l_b)!="HO"
         )
      {
         allho=false;
      }
   }

   if (allho)
   {
      // In this case all one-mode basis functions are HO
      SetmBasSetType("PROD-ALLHO");
   }
   else
   {
      // default basis type is simply product which does not promise too much.
      SetmBasSetType("PROD");
   }

   if (gBasisIoLevel > I_3)
   {
      {
         Mout << "\n Summary for basis sets: " << GetmBasName() << std::endl;
         Mout << " Table of one mode basis in basis " << std::endl;
         Out72Char(Mout,'+','=','+');
         Mout << " Mode m | mode-Label | Basis Type |  Nbas | Aux Defs. " << std::endl;
         Out72Char(Mout,'+','=','+');
         for (In i_bas_mode = I_0; i_bas_mode < mNoModesInBas; i_bas_mode++)
         {
            OneModeBasDef bas_def(GetBasDefForLocalMode(i_bas_mode));
            Mout << right;
            Mout.width(7);
            Mout << bas_def.GetGlobalModeNr() << " | ";
            Mout.width(10);
            Mout << gOperatorDefs.GetModeLabel(bas_def.GetGlobalModeNr()) << " | ";
            Mout.width(10);
            Mout << bas_def.Type() << " | ";
            Mout.width(5);
            Mout << bas_def.Nbas() << " | ";
            Mout << left;
            Mout << bas_def.Aux() << std::endl;
         }
         Out72Char(Mout,'+','=','+');
      }
   }
}

/**
 * Generate gaussian or B-spline basis based on info
**/
void BasDef::GenBasis(const OpDef& arOpDef)
{
   this->Clear();
   SetmBasSetType("Product");

   if (!(mUseGaussianBasis || mUseBsplineBasis))
   {
      MIDASERROR("No basis set option trigged on ");
   }

   std::string s_type = "";
   if (mUseGaussianBasis) 
   {
      s_type = "Gauss";
      
      // If only one Gaussian exponent is given, then this number is extend to the primitive basis for all modes
      if (mGaussianExps.size() == I_1)
      {
         for (LocalModeNr i_op_mode = I_1; i_op_mode < arOpDef.NmodesInOp(); i_op_mode++)
         {
            mGaussianExps.push_back(mGaussianExps[I_0]);
         }
      }
      // If more than one Gaussian exponent is given, then we check that the size is correct and otherwise retain the individual definitions to primitive bases for different modes
      else if (mGaussianExps.size() > I_1)
      {
         if (mGaussianExps.size() != arOpDef.NmodesInOp())
         {
            MIDASERROR("Size of vector given under the #3 GaussianBasis keyword is incorrect! This should match the number of modes in your system!");
         }
      }
   }
   else if (mUseBsplineBasis) 
   {
      s_type = "Bsplines";

      // If only one B-spline order is given, then this number is extend to the primitive basis for all modes
      if (mBsplineOrders.size() == I_1)
      {
         for (LocalModeNr i_op_mode = I_1; i_op_mode < arOpDef.NmodesInOp(); i_op_mode++)
         {
            mBsplineOrders.push_back(mBsplineOrders[I_0]);
         }
      }
      // If more than one B-spline order is given, then we check that the size is correct and otherwise retain the individual definitions to primitive bases for different modes
      else if (mBsplineOrders.size() > I_1)
      {
         if (mBsplineOrders.size() != arOpDef.NmodesInOp())
         {
            MIDASERROR("Size of vector given under the #3 BsplineBasis keyword is incorrect! This should match the number of modes in your system!");
         }
      }
   }
   // Look for boundaries from the potential 
   std::map<std::string, std::vector<Nb> > mode_bounds;
   if (mReadBoundsFromFile)
   {
      // Read bounds from file first
      std::string data_file   =  this->mBoundsFilePath.empty()
                              ?  gAnalysisDir + "/" + "one_mode_grids.mbounds"   // Niels: This is the default. It cannot be set in BasisInput, since gAnalysisDir is not necessarily defined yet.
                              :  this->mBoundsFilePath;

      std::istringstream sstream = midas::mpi::FileToStringStream(data_file);
      
      std::string s;
      while (std::getline(sstream, s))
      {
         // The first line of the one_mode_grids.mbounds file is a comment line that we need to skip
         std::size_t found_comment = s.find("#");
         if (found_comment != std::string::npos) 
         {
            continue;
         }
         auto string_vec = midas::util::StringVectorFromString(s);

         if (string_vec.size() < 3)
         {
            MIDASERROR("Too few columns.");
         }

         const auto& mode_label = string_vec[0];
         std::vector<Nb> bounds_vec(8, C_0);
         for(In icol=I_1; icol<string_vec.size(); ++icol)
         {
            bounds_vec[icol-I_1] = midas::util::FromString<Nb>(string_vec[icol]);
         }

         mode_bounds[mode_label] = std::move(bounds_vec);
      }

      In n_state_transfer_modes = I_0;
      In nmodes_with_bounds = arOpDef.NmodesInOp();
      for(In imode=I_0; imode<arOpDef.NmodesInOp(); ++imode)
      {
         if (  arOpDef.StateTransferForMode(imode)
            )
         {
            --nmodes_with_bounds;
         }
      }

      if (  mode_bounds.size() != nmodes_with_bounds
         )
      {
         std::stringstream msg;
         msg   <<  "TOO FEW LINES IN '" << data_file << "' \n"
               <<  "I have        : "  << mode_bounds.size() << "\n"
               <<  "but, expected : "  << nmodes_with_bounds
               ;
         MIDASERROR(msg.str());
      }
   }

   bool print = true;  
   std::map<std::string, std::pair<Nb, Nb> > print_bounds;
   for (LocalModeNr i_op_mode = I_0; i_op_mode < arOpDef.NmodesInOp(); i_op_mode++)
   {
      // If using state-transfer operators, we simply push_back empty OneModeBasDef%s
      if (  arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_op_mode))
         )
      {
         gOneModeStateBasis.push_back(OneModeStateBasis(arOpDef.GetGlobalModeNr(i_op_mode), arOpDef.StateTransferIJMax(i_op_mode) + I_1));
         In add = gOneModeStateBasis.size() - I_1;
         OneModeBasDef bas_def("StateBasis", add);
         In g_mode = i_op_mode;
         AddToBasDef(bas_def, g_mode);
         continue;
      }

      const auto& mode_label = gOperatorDefs.GetModeLabel(arOpDef.GetGlobalModeNr(i_op_mode));
      std::vector<Nb> bounds_thismode;
      if (  mode_bounds.find(mode_label) != mode_bounds.end()
         )
      {
         bounds_thismode = mode_bounds.at(mode_label);
      }
      else if  (  mNoBasBeyondMaxPot
               || mGradScaleBounds
               )
      {
         MIDASERROR("We need to read bounds from file in order to use NoBasBeyondMaxPot or GradScaleBounds.");
      }

      Nb scal = C_1; 
      Nb r_b = C_0; 
      Nb l_b = C_0; 
      
      if (!mReadBoundsFromFile)
      {
         if (mUsingScalingFreqs)
         {
            scal = std::pow(arOpDef.ScaleFactorForMode(i_op_mode), C_2);
         }
         else
         {
            Nb c_tot_ho = C_0;
            LocalOperNr q_sqr_oper = arOpDef.GetQSquareOpForMode(i_op_mode);
            for (In i_act_term = I_0; i_act_term < arOpDef.NactiveTerms(i_op_mode); i_act_term++)
            {
               In i_term = arOpDef.TermActive(i_op_mode, i_act_term);
               {
                  In n_fac = arOpDef.NfactorsInTerm(i_term);
                  if (n_fac > I_1)
                  {
                     continue;
                  }
                  In i_fac = I_0;
                  LocalOperNr i_oper = arOpDef.OperForFactor(i_term, i_fac);
                  if (i_oper != q_sqr_oper)
                  {
                     continue;
                  }
                  Nb fact = arOpDef.Coef(i_term); // now term is equal to " C*Q^2 "
                  c_tot_ho += fact;
               }
            }

            if (c_tot_ho < C_0)
            {
               std::stringstream ss;
               ss << "Found negative harmonic coefficient (" << c_tot_ho << ") for mode " << i_op_mode << ". "
                  << "I will flip the sign before taking the square root! "
                  << "Consider setting #3 UseScalingFreqs under #2 Basis.\n";
               MidasWarning(ss.str()); 
               scal = std::sqrt(C_2*std::abs(c_tot_ho));
            }
            else
            {
               scal = std::sqrt(C_2*c_tot_ho);
            }
            
            if (arOpDef.ScaledPot()) 
            {
               scal *= arOpDef.ScaleFactorForMode(i_op_mode);
            }
         }

         // Sanity check
         if (  scal == C_0
            )
         {
            MIDASERROR("No Q^2 operator found for mode " + std::to_string(i_op_mode) + ". Grid bounds must be specified manually or read from file!");
         }

         //calculate tps hard coded now
         In n_ho = GetTurningPoint();
         r_b = std::sqrt((C_2/scal)*(Nb(n_ho) + C_I_2));
         l_b = -r_b;
      }
      else
      {
         l_b = bounds_thismode[0]; //Left bound from ADGA
         r_b = bounds_thismode[1]; //Right bound from ADGA 
      }
      
      if ((mScaleBounds && mGradScaleBounds) || ((!mScaleBounds && !mGradScaleBounds) && gPesCalcDef.GetmPesAdga()))
      {
         MIDASERROR("In order for basis set construction to make sense either #3 ScalBounds or #3 GradScalBounds need to be specified under the #2 Basis block but they cannot both be specified at the same time");
      }

      // If the keyword #3 GradScalBounds is specified on input, then we do the following
      if (mGradScaleBounds)
      {
         MidasAssert(mReadBoundsFromFile, ("Cannot do GradScaleBounds without bounds read in from file"));

         Nb extension_factor = GetGradScalBounds();
         Nb extension_max = GetGradMaxBoundsExt();
         Nb energy_change = extension_factor * std::min(bounds_thismode[2], bounds_thismode[3]);
         
         MidasAssert(extension_factor > I_0, "Extension factor specified under #3 GradScalBounds does not allow the basis to extend beyond at all"); 
         if (energy_change < I_0)
         {
            MidasWarning("Something is wrong, getting negative energy change! Check potential files in analysis directory for sanity!");
         }

         Nb l_basis_extension = energy_change/bounds_thismode[4];
         Nb r_basis_extension = energy_change/bounds_thismode[5];
       
         if (bounds_thismode[4] < C_I_10_4 || l_basis_extension > extension_max)
         { 
            if (gBasisIoLevel > I_9)
            {
               MidasWarning("Problematic l_basis expansion in #3 GradScalBounds: "+std::to_string(l_basis_extension)); 
            }
            
            l_basis_extension = extension_max;
         }

         if (bounds_thismode[5] < C_I_10_4 || r_basis_extension > extension_max)
         {
            if (gBasisIoLevel > I_9)
            {
               MidasWarning("Problematic r_basis expansion in #3 GradScalBounds: "+std::to_string(r_basis_extension)); 
            }
            
            r_basis_extension = extension_max;
         }
      
         if (gBasisIoLevel > I_9)
         {
            Mout << " Energy change in gradient guided basis set bound determination = " << energy_change << std::endl; 
            Mout << " Left basis extension  = " << l_basis_extension << std::endl; 
            Mout << " Right basis extension = " << r_basis_extension << std::endl;
         }

         l_b -= l_basis_extension;   
         r_b += r_basis_extension;   
      }
      
      // If the keyword #3 ScalBounds is specified on input, then we do the following
      if (mScaleBounds)
      {
         Nb e_fac = GetScalBounds();
         Nb e_add = GetMaxBoundsExt(); // Q-units

         if (print && gBasisIoLevel > I_7)      //common factors, print it only once. 
         {
            Mout << " scaling factor in basis set definition: " << e_fac << endl;
            Mout << " max extension  in  basis set definition: " << e_add << endl;
            print = false;
         }
      
         if (gPesCalcDef.GetmPesAdga() && (e_fac < 1.05 || e_add < 5.0))   
         {  
            Mout << " WARNING: With localized basis set and ADGA procedure the basis set must be allowed to " <<endl;
            Mout << " explore beyond the grid boundaries " <<endl;
            MIDASERROR(" Localized basis set and ADGA procedure");
         }

         if (e_fac < 0.9 || e_fac > 2.0 || e_add < -10.0)
         {
            Mout << " WARNING: the scaling factor #3 ScalBounds (x y) seems unrealistic: " <<endl;
            Mout << " I suggest  0.9 < x < 2.0   (now : " << e_fac << ")" <<endl;
            Mout << " I suggest  y > -10.0       (now : " << e_add << ")" <<endl;
            
            MIDASERROR(" Localized basis set definition");
         }
      
         //Mout << " for i_op_mode: " << i_op_mode  << " omega = " << omeg   << " l_b: " << l_b << " r_b: " << r_b << endl;
     
         if (l_b > r_b )
         {
            //Mout << "l_b > r_b" <<  endl;
            if (fabs(l_b*e_fac) > fabs(l_b + e_add))
            {
               //Mout << "l_b + e_add:" << l_b << "+" << e_add << endl ;
               l_b += e_add;
            }
            else
            {
               //Mout << "l_b * e_fac:" << l_b << "*" << e_fac << endl ;
               l_b *= e_fac;
            }
            if (fabs(r_b*e_fac) > fabs(r_b - e_add))
            {
               //Mout << "r_b - e_add:" << r_b << "-" << e_add << endl ;
               r_b -= e_add;
            }
            else
            {
               //Mout << "r_b * e_fac:" << r_b << "*" << e_fac << endl ;
               r_b *= e_fac;
            }
         }
         else
         {
            //Mout << "l_b < r_b" <<  endl;
            if (fabs(l_b*e_fac) > fabs(l_b - e_add))
            {
               //Mout << " l_b - e_add:" << l_b << "-" << e_add << endl ;
               l_b -= e_add;
            }
            else
            {
               //Mout << " l_b * e_fac:" << l_b << "*" << e_fac << endl ;
               l_b *= e_fac;
            }
            if (fabs(r_b*e_fac) > fabs(r_b + e_add))
            {
               //Mout << " r_b + e_add:" << r_b << "+" << e_add << endl ;
               r_b += e_add;
            }
            else
            {
               //Mout << " r_b * e_fac:" << r_b << "*" << e_fac << endl ;
               r_b *= e_fac;
            }
         }
      }

      // Give a warning if the basis boundaries are too close to the potential boundaries, i.e. that the difference between the two points are not larger than 1 procent of the total span of the potential 
      if (mGradScaleBounds)
      {
         if (std::fabs(bounds_thismode[0] - l_b) < 0.01 * std::fabs(std::fabs(bounds_thismode[0]) - std::fabs(bounds_thismode[1])))
         {
            MidasWarning("The left basis set boundary is very close to the left potential boundary, which makes it difficult to know if vibrational density is present outside of current potential bound");
         }

         if (std::fabs(bounds_thismode[1] - r_b) < 0.01 * std::fabs(std::fabs(bounds_thismode[0]) - std::fabs(bounds_thismode[1])))
         {
            MidasWarning("The right basis set boundary is very close to the right potential boundary, which makes it difficult to know if vibrational density is present outside of current potential bound");
         }
      }

      // If the basis bounds are set further out than the coordinate corresponding to a maximum for the potential, then we reduce the basis bounds to that value
      if (mNoBasBeyondMaxPot)
      {
         if (  std::fabs(l_b) >= std::fabs(bounds_thismode[6])
            && bounds_thismode[6] != C_0
            && std::fabs(bounds_thismode[6]) >= std::fabs(bounds_thismode[0])
            )
         {
            //MidasWarning("The left basis bound is found to be further out than the point corresponding to a maximum in the potential, resulting in reduction of basis bound from: "+std::to_string(l_b));
            l_b = bounds_thismode[6];
         }
         
         if (  std::fabs(r_b) >= std::fabs(bounds_thismode[7]) 
            && bounds_thismode[7] != C_0
            && std::fabs(bounds_thismode[7] >= bounds_thismode[1])
            )
         {
            //MidasWarning("The right basis bound is found to be further out than the point corresponding to a maximum in the potential, resulting in reduction of basis bound from: "+std::to_string(r_b));
            r_b = bounds_thismode[7];
         }
      }

      print_bounds[mode_label] = std::make_pair(l_b, r_b);
      
      // We furthermore need to set the number of ditributed Gaussian or B-spline functions that are to be used in the primitive basis
      In no_of_basis_funcs = I_0;
      // Set the number of primitive basis functions via basis set density/densities 
      if (mUsePrimBasisDens)
      {
         Nb basis_dens = C_0;
         if (mPrimBasisDens.size() == I_1)
         {
            basis_dens = mPrimBasisDens[I_0];
         }
         else
         {
            if (mPrimBasisDens.size() != arOpDef.NmodesInOp())
            {
               MIDASERROR("Size of vector given under the #3 PrimBasisDensity keyword is incorrect! This should match the number of modes in your system!");
            }

            basis_dens = mPrimBasisDens[i_op_mode];
         }

         //
         no_of_basis_funcs = In(std::abs(r_b - l_b) * basis_dens + C_I_2);

         // Ensures that the number of B-splines cannot exceeded the specified maximum number
         if (no_of_basis_funcs > mMaxNoPrimBasisFunctions)
         {
            Mout << "Capped the number of basis functions at: " << mMaxNoPrimBasisFunctions << std::endl;
            no_of_basis_funcs = mMaxNoPrimBasisFunctions;
         }
      }
      // Set the number of primitive basis functions directly via input
      else
      {
         if (mNoPrimBasisFunctions.size() == I_1)
         {
            no_of_basis_funcs = mNoPrimBasisFunctions[I_0];
         }
         else 
         {
            if (mNoPrimBasisFunctions.size() != arOpDef.NmodesInOp())
            {
               MIDASERROR("Size of vector given under the #3 NprimBasisFunctions keyword is incorrect! This should match the number of modes in your system!");
            }
            
            no_of_basis_funcs = mNoPrimBasisFunctions[i_op_mode];
         }
      }

      In add = -I_1;
      if (mUseGaussianBasis)
      {
         gOneModeGaussDefs.push_back(OneModeGaussDef(arOpDef.GetGlobalModeNr(i_op_mode), s_type, mGaussianExps[i_op_mode], l_b, r_b, no_of_basis_funcs));
         add = gOneModeGaussDefs.size() - I_1;
      }
      else if (mUseBsplineBasis)
      {
         gOneModeBsplDefs.push_back(OneModeBsplDef(arOpDef.GetGlobalModeNr(i_op_mode), s_type, l_b, r_b, mBsplineOrders[i_op_mode], no_of_basis_funcs, mKeepUnboundedBsplines));
         add = gOneModeBsplDefs.size() - I_1;
      }
      OneModeBasDef bas_def(s_type, add);
      AddToBasDef(bas_def, bas_def.GetGlobalModeNr());
   }

   // Define type!
   bool allgauss = true; 
   bool allbspl = true;
   for (In i_l_b = I_0; i_l_b < mNoModesInBas; i_l_b++) 
   {
      if (  !arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_l_b))
         && OneModeType(i_l_b) != "Gauss"
         )
      {
         allgauss = false;
      }
   }
   for (In i_l_b = I_0; i_l_b < mNoModesInBas; i_l_b++)
   {
      if (  !arOpDef.StateTransferForMode(arOpDef.GetGlobalModeNr(i_l_b))
         && OneModeType(i_l_b) != "Bsplines"
         )
      {
         allbspl = false;
      }
   }
   if (allgauss)
   {
      // In this case all one-mode basis functions are Gaussians
      SetmBasSetType("PROD-ALLGAUSS");
   }
   else if (allbspl)
   {
      //In this case all one-mode basis functions are Bsplines
      SetmBasSetType("PROD-ALLBSPLINES");
   }
   else
   {
      // default basis type is simply product which does not promise too much.
      SetmBasSetType("PROD");
   }

   if (gBasisIoLevel > I_3)
   {
      //for (In i_basis =0;i_basis<gBasis.size();i_basis++)
      {
         Mout << "\n Summary for basis sets: " << GetmBasName() << std::endl;
         Mout << " Table of one mode basis in basis " << std::endl;
         Out72Char(Mout,'+','=','+');
         Mout << " Mode m | mode-Label | Basis Type |  Nbas | Aux Defs. " << std::endl;
         Out72Char(Mout,'+','=','+');
         for (In i_bas_mode = I_0; i_bas_mode < mNoModesInBas; i_bas_mode++)
         {
            OneModeBasDef bas_def(GetBasDefForLocalMode(i_bas_mode));
            Mout << right;
            Mout.width(7);
            Mout << bas_def.GetGlobalModeNr() << " | ";
            Mout.width(10);
            Mout << gOperatorDefs.GetModeLabel(bas_def.GetGlobalModeNr()) << " | ";
            Mout.width(10);
            Mout << bas_def.Type() << " | ";
            Mout.width(5);
            Mout << bas_def.Nbas() << " | ";
            Mout << left;
            if (bas_def.Aux().empty())
            {
               const auto& label = gOperatorDefs.GetModeLabel(bas_def.GetGlobalModeNr());
               Mout << "leftb=" << print_bounds[label].first << ", rightb=" << print_bounds[label].second << std::endl;
            }
            else 
            {
               Mout << bas_def.Aux() << std::endl;
            }
            //Mout << bas_def.Aux() << "leftb=" << l_b << ", rightb=" << r_b << endl;
         }
         Out72Char(Mout,'+','=','+');
      }
   }
}

/**
 * Plot basis functions for mode labels given in mPlotBasisForModes.
 *
 * @param aSuffix
 *
 **/
void BasDef::PlotBasis
   (  const std::string& aSuffix
   )
{
   auto& modes = this->mPlotBasisForModes;

   // Check if we are plotting all modes
   if (  modes.size() == 1
      && *modes.begin() == "ALL"
      )
   {
      modes.clear();
      const auto& mode_labels = gOperatorDefs.GetModeLabels();
      modes.insert(mode_labels.begin(), mode_labels.end());
   }

   const size_t width = 30;

   // Loop over modes to plot
   for(const auto& mode : modes)
   {
      GlobalModeNr g_mode = gOperatorDefs.ModeNr(mode, false);
      LocalModeNr bas_mode = this->GetLocalModeNr(g_mode);
      auto nbas = this->Nbas(bas_mode);
      auto type = this->OneModeType(bas_mode);

      // Set up output stream
      midas::mpi::OFileStream bas_out(this->GetmBasName() + aSuffix + "_" + mode + ".dat");
      bas_out << std::scientific << std::setprecision(16);

      // Print header
      std::string s1 = "# " + mode;
      bas_out   << std::setw(width) << s1;
      for(In ibas=I_0; ibas<nbas; ++ibas)
      {
         std::string s = "chi_" + std::to_string(ibas) + "(" + mode + ")";
         bas_out   << std::setw(width) << s;
      }
      bas_out   << std::endl;

      // Get bounds
      std::pair<Nb, Nb> bounds(C_0, C_0);
      if (  type == "HO"
         )
      {
         // Use 2 times turning points for highest HO basis function
         Nb scale = C_2;
         auto omega = this->GetOmeg(bas_mode);
         bounds.second = std::sqrt(C_2*(Nb(nbas)+C_I_2)/(omega))*scale;  // 2 times classical turning point for n=nbas in harmonic oscillator
         bounds.first = -bounds.second;
      }
      else
      {
         bounds = this->GetBasDefForLocalMode(bas_mode).GetBasisGridBounds();
      }

      // Set up grid
      auto length = bounds.second - bounds.first;

      In n_pts = I_0;
      if (gPesCalcDef.GetmPscInPes())
      {
         n_pts = std::ceil(length * I_100); 
      }
      else
      {
         n_pts = std::ceil(length * I_10); 
      }

      Nb step = length/Nb(n_pts-I_1);
      MidasVector grid(n_pts);
      for (In i_p=0; i_p<n_pts; ++i_p)
      {
         grid[i_p]=bounds.first + step*i_p;
      }

      // Evaluate
      for(In ipt=I_0; ipt<n_pts; ++ipt)
      {
         const auto& q = grid[ipt];

         bas_out   << std::setw(width) << q;

         for(In ibas=I_0; ibas<nbas; ++ibas)
         {
            bas_out << std::setw(width) << this->EvalOneModeBasisFunc(bas_mode, ibas, q);
         }

         bas_out << std::endl;
      }

      // Close stream
      bas_out.close();
   }
}

