/**
************************************************************************
* 
* @file                FalconCalcDef.h
*
* 
* Created:             10-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for generation of coordinates
* 
* Last modified:       07-07-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FALCONCALCDEF_H
#define FALCONCALCDEF_H

// std headers
#include <string>

// midas headers
#include "input/Macros.h"
#include "inc_gen/TypeDefs.h"
#include "input/FreqAnaCalcDef.h"
#include "input/RotCoordCalcDef.h"

namespace midas::input
{

// using declarations

enum class ModeOptType: int  {ERROR,NO,HESSIAN, ROTCOORD};
enum class HessType: int  {ERROR,NO, GIVEN, CALC};
enum class CoupEstType: int  {ERROR,DIST,DISTACT,NUCCOUPHESSNORM,NUCCOUPHESSNORMACT};

std::ostream& operator<<(std::ostream& os,const ModeOptType& arModeOptType);
std::ostream& operator<<(std::ostream& os,const HessType& arHessType);
std::ostream& operator<<(std::ostream& os,const CoupEstType& arCoupEstType);

/* ************************************************************************
************************************************************************ */
class FalconCalcDef
{
   CREATE_VARIABLE(bool, Restart, false);
   
   // about fusion
   CREATE_VARIABLE(Nb, DegThresh, 0.001e0);
   CREATE_VARIABLE(bool, SlowFusion, true);
   CREATE_VARIABLE(bool, FusionInactive, false);
   CREATE_VARIABLE(bool, Capping, true);
   
   //Thresholds:
   // connection
   CREATE_VARIABLE(Nb, MaxDistForConn, C_NB_MAX);
   // fusion
   CREATE_VARIABLE(In, MaxSubSysForFusion, C_IN_MAX);
   CREATE_VARIABLE(Nb, MinCouplingForFusion, C_0);
   // relaxation
   CREATE_VARIABLE(In, MaxSubSysForRelax, C_IN_MAX);
   CREATE_VARIABLE(Nb, MinCouplingForRelax, C_0);
   //  Addition of modes 
   CREATE_VARIABLE(In, MaxSubSysForFullAdd, C_IN_MAX);
   CREATE_VARIABLE(Nb, MinCouplingForFullAdd, C_0);
   
   // other relaxation setting (more advanced)
   CREATE_VARIABLE(bool, CalcAllSigmas, false);
   CREATE_VARIABLE(bool, RelaxAll, false);
   CREATE_VARIABLE(bool, RotateAll, false);
   CREATE_VARIABLE(bool, PrepRun, false);
   
   //Writing of the Incremental Input
   CREATE_VARIABLE(In, MaxIncrLevel, I_0);
   CREATE_VARIABLE(Nb, MaxIncrDist, C_NB_MAX);
   CREATE_VARIABLE(Nb, IncrBondDist, C_NB_MAX);
   CREATE_VARIABLE(bool, Proj, false);
   CREATE_VARIABLE(bool, AddRotToAux, true);
   
   private:
      ModeOptType             mModeOptType = ModeOptType::NO;
      HessType                mHessType = HessType::NO;
      
      // about fusion 
      CoupEstType             mCoupEstType = CoupEstType::DIST;

      FreqAnaCalcDef          mFreqAnaCalcDef;
      RotCoordCalcDef         mRotCoordCalcDef;

   public:

      //setting
      void SetModeOptType (const std::string& arStr);
      void SetHessType (const std::string& arStr);
      void SetCoupEstType (const std::string& arStr);

      //getting
      ModeOptType GetModeOptType() const {return mModeOptType;}
      HessType GetHessType() const {return mHessType;}
      CoupEstType GetCoupEstType() const {return mCoupEstType;}
      FreqAnaCalcDef* GetpFreqAnaCalcDef() {return &mFreqAnaCalcDef;}
      RotCoordCalcDef* GetpRotCoordCalcDef() {return &mRotCoordCalcDef;}
};

} // namespace midas::input

#endif //FALCONCALCDEF_H
