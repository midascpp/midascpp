/**
************************************************************************
* 
* @file                GenFitBasisCalcDef.h
*
* Created:             29-05-2018
*
* Author:              Emil Lund Klinting (klintg@chem.au.dk)
*
* Short Description:   Calculation definitions for general fit-bases
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef GENFITBASISCALCDEF_H
#define GENFITBASISCALCDEF_H

// std headers
#include <map>
#include <set>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>

// midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"

/**
 *
**/
class GenFitBasisCalcDef
{
   private:
     
      //! The mode(s) for which general fit-basis functions should be used in the fitting
      std::vector<In>               mFitModes;
      //! General fit-basis functions
      std::vector<std::string>      mGenFitFunctions;
      //! Non-linear optimization parameters for the non-linear conjugate gradient method
      std::vector<std::string>      mNonLinOptParams;
      //! Non-linear optimization function for the non-linear conjugate gradient method
      std::string                   mNonLinOptFunc;
      //! Non-linear optimization starting guess for the non-linear conjugate gradient method
      std::vector<Nb>               mNonLinOptStartGuess;
      //! Non-linear optimization threshold for the non-linear conjugate gradient method
      Nb                            mNonLinOptThr;
      //! The property to be fitted by the defined fit-basis functions
      std::vector<std::string>      mFitProperties;    

   public:
      
      //! Constructor
      GenFitBasisCalcDef(); 
  
      //! Reorder the non-linear parameters 
      std::vector<std::string> ReorderParams(const std::string& aFuncName, const std::string& aFileName);

      //! Construct a vector with the start guess for use with the non-linear conjugate gradient method
      std::vector<Nb> GetStartGuess(const std::string& aVecStr, const std::string& aFileName);

      //@{
      //! GenFitBasis setters
      void SetmFitModes(const std::vector<In>& aVec)                       {mFitModes = aVec;}
      void SetmGenFitFunctions(const std::vector<std::string>& aVec)       {mGenFitFunctions = aVec;}
      void SetmNonLinOptParams(const std::vector<std::string>& aVec)       {mNonLinOptParams = aVec;}
      void SetmNonLinOptFunc(const std::string& aStr)                      {mNonLinOptFunc = aStr;}
      void SetmNonLinOptStartGuess(const std::vector<Nb>& aVec)            {mNonLinOptStartGuess = aVec;}
      void SetmNonLinOptThr(const Nb aNb)                                  {mNonLinOptThr = aNb;}
      void SetmFitProperties(const std::vector<std::string>& aVec)         {mFitProperties = aVec;}
      //@}

      //@{
      //! GenFitBasis getters
      const std::vector<In>& GetmFitModes() const                          {return mFitModes;}
      const std::vector<std::string>& GetmGenFitFunctions() const          {return mGenFitFunctions;} 
      const std::vector<std::string>& GetmNonLinOptParams() const          {return mNonLinOptParams;}
      const std::string& GetmNonLinOptFunc() const                         {return mNonLinOptFunc;}
      const std::vector<Nb>& GetmNonLinOptStartGuess() const               {return mNonLinOptStartGuess;}
      const Nb& GetmNonLinOptThr() const                                   {return mNonLinOptThr;}
      const std::vector<std::string>& GetmFitProperties() const            {return mFitProperties;}   
      //@}
};

#endif /* GENFITBASISCALCDEF_H */
