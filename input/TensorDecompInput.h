#ifndef TENSORDECOMPINPUT_H_INCLUDED
#define TENSORDECOMPINPUT_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "input/TensorDecompDef.h"
#include "input/KeywordMap.h"
#include "tensor/TensorDecompInfo.h"

/**
 *
 **/
std::string TensorDecompInput(std::istream& Minp, VscfCalcDef& arCalcDef, In aInputLevel, DECOMPCALCTYPE aType = DECOMPCALCTYPE::NONE);

///>
bool NewTensorDecompInput(std::istream& Minp, std::string& s, In input_level, TensorDecompInfo::Set& decompinfo_set);

namespace detail 
{
void ValidateTensorDecompInput
   ( midas::tensor::TensorDecompInfo& decompinfo
   );

const keyword_map& GetTensorDecompPresetMap
   (
   );
} /* namespace detail */
#endif /* TENSORDECOMPINPUT_H_INCLUDED */
