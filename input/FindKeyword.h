/**
************************************************************************
* 
* @file                FindKeyword.h
*
* 
* Created:             19-02-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Finds keyword in map, if not found returns ERROR
* 
*
* Last modified: Fri Jun 17, 2013  08:53AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FINDKEYWORD_H_INCLUDED
#define FINDKEYWORD_H_INCLUDED

#include <map>

namespace midas
{
namespace input
{

template<class T>
T FindKeyword( const std::map<std::string, T>& aMapToSearchIn
             , const std::string& aStringToFind
             )
{
   auto found = aMapToSearchIn.find(aStringToFind);
   if(aMapToSearchIn.end() == found)
   {
      return T::ERROR;
   }
   return found->second;
}

}/* namespace input */
}/* namespace midas */

#endif /* FINDKEYWORD_H_INCLUDED */
