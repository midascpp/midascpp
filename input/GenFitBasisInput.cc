/**
************************************************************************
* 
* @file                GenFitBasisInput.cc
* 
* Created:             29-05-2018
*
* Author:              Emil Lund Klinting (Klint@chem.au.dk)
*
* Short Description:   Reader for general fit-basis functions
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <vector>

#include "input/Input.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "input/GenFitBasisCalcDef.h"

namespace midas
{
namespace input
{

//Forward declaration
std::string ReadGenFitBasisInput
   (  GenFitBasisCalcDef& arCalcDef
   ,  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   );

std::string ReadFitBasisDef
   (  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   )
{
   enum INPUT {ERROR, SPECFITBASIS, FUNCTIONS, CONSTANTS};
   
   const std::map<std::string, INPUT> input_word =
   {
      {"#"+std::to_string(aInpLevel)+"SPECFITBASIS", SPECFITBASIS},
      {"#"+std::to_string(aInpLevel)+"FUNCTIONS", FUNCTIONS},
      {"#"+std::to_string(aInpLevel)+"CONSTANTS", CONSTANTS}
   };
   
   std::string s, s_orig;
   bool already_read = false;
   while (already_read || std::getline(aFile, s))
   {
      s_orig = s;
      // Transform to capital letters
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);    
      // Delete all blank spaces
      while (s.find(" ") != s.npos) 
      {
         s.erase(s.find(" "), I_1);
      }
      // Check for empty lines
      if (s.empty())
      {
         continue;
      }
      
      INPUT input = FindKeyword(input_word, s);
      if (midas::util::FromString<In>(s.substr(1, s.find_first_not_of("#0123456789") - 1)) < aInpLevel)
      {
         break;
      }
      switch(input)
      {
         case FUNCTIONS:
         {
            while (!aFile.eof() && '#' == aFile.peek())
            {
               if (std::getline(aFile, s))
               {
                  auto functions = midas::util::StringVectorFromString(s);
                  if (functions.size() != I_2)
                  {
                     MIDASERROR("The function definition: " + s + " given under the #" + std::to_string(aInpLevel) + " Functions keyword is invalid, there should be two elements on each line!");
                  }

                  gFitBasisCalcDef.SetmFitFuncs(std::make_pair(functions[I_0], functions[I_1]));
               }
            }
            break;
         }
         case CONSTANTS:
         {
            while (!aFile.eof() && '#' == aFile.peek())
            {
               if (std::getline(aFile,s))
               {
                  auto constants = midas::util::StringVectorFromString(s);
                  if (constants.size() != I_2)
                  {
                     MIDASERROR("The constant definition: " + s + " given under the #" + std::to_string(aInpLevel) + " Constants keyword is invalid, there should be two elements on each line!");
                  }

                  gFitBasisCalcDef.SetmFitBasisConstants(std::make_pair(constants[I_0], midas::util::FromString<Nb>(constants[I_1])));
               }
            }
            break;
         }
         case SPECFITBASIS:
         {
            already_read = true;
            gFitBasisCalcDef.SetmUseGenFitBasis(true);
            
            GenFitBasisCalcDef new_fit_basis_calc_def; 
            s = ReadGenFitBasisInput(new_fit_basis_calc_def, aFile, aFileName, aInpLevel + I_1); 
            gFitBasisCalcDef.SetmGenFitBasisCalcDefs(new_fit_basis_calc_def);
            break;
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a FitBasDef level "+std::to_string(aInpLevel)+" Input keyword !" << std::endl;
            MIDASERROR(" Check your input file : " + aFileName + " please ");
         }
      }
      
   } 
   
   return s;  
}


/**
 * Read a general fit-basis definition input
**/
std::string ReadGenFitBasisInput
   (  GenFitBasisCalcDef& arCalcDef
   ,  std::istream& aFile
   ,  const std::string& aFileName
   ,  const In& aInpLevel
   )
{
   enum INPUT {ERROR, FITFUNCTIONS, MODES, OPTFUNC, OPTFUNCTHR, PROPS};
   
   const std::map<std::string, INPUT> input_word =
   {
      {"#"+std::to_string(aInpLevel)+"FITFUNCTIONS",FITFUNCTIONS},
      {"#"+std::to_string(aInpLevel)+"MODES",MODES},
      {"#"+std::to_string(aInpLevel)+"OPTFUNC",OPTFUNC},
      {"#"+std::to_string(aInpLevel)+"OPTFUNCTHR",OPTFUNCTHR},
      {"#"+std::to_string(aInpLevel)+"PROPS",PROPS},
   };

   std::string s;
   std::string s_orig;

   while (std::getline(aFile, s))
   {
      s_orig = s;
      // Transform to capital letters
      transform(s.begin(), s.end(), s.begin(), (In(*) (In))toupper);    
      // Delete all blank spaces
      while (s.find(" ") != s.npos) 
      {
         s.erase(s.find(" "), I_1);
      }
      // Check for empty lines
      if (s.empty())
      {
         continue;
      }
      
      INPUT input = FindKeyword(input_word, s);
      if (midas::util::FromString<In>(s.substr(1, s.find_first_not_of("#0123456789") - 1)) < aInpLevel)
      {
         break;
      }

      switch(input)
      {
         case FITFUNCTIONS:
         {
            std::getline(aFile, s);
            arCalcDef.SetmGenFitFunctions(midas::util::StringVectorFromString(s));
            break;
         }
         case MODES:
         {
            std::getline(aFile, s);
            arCalcDef.SetmFitModes(midas::util::VectorFromString<In>(s));
            break;
         }
         case OPTFUNC:
         {
            std::getline(aFile, s);
            std::vector<std::string> non_lin_opt_info = midas::util::StringVectorFromString(s);
            if (non_lin_opt_info.size() != I_3)
            {
               Mout << "Read line under #"+std::to_string(aInpLevel)+" OptFunc as : " << non_lin_opt_info << std::endl;
               MIDASERROR("There is too much information given to the #"+std::to_string(aInpLevel)+
                          " OptFunc keyword in the file "+aFileName+" , maybe too many spaces?");
            }

            arCalcDef.SetmNonLinOptParams(arCalcDef.ReorderParams(non_lin_opt_info[I_0], aFileName));
            arCalcDef.SetmNonLinOptFunc(non_lin_opt_info[I_1]);

            arCalcDef.SetmNonLinOptStartGuess(arCalcDef.GetStartGuess(non_lin_opt_info[I_2], aFileName));
            break;
         }
         case OPTFUNCTHR:
         {
            std::getline(aFile, s);
            arCalcDef.SetmNonLinOptThr(midas::util::FromString<Nb>(s));
            break;
         }
         case PROPS:
         {
            std::getline(aFile, s);
            std::vector<std::string> props = midas::util::StringVectorFromString(s);
            transform(props.at(0).begin(), props.at(0).end(), props.at(0).begin(), (In(*) (In))toupper); 
            
            arCalcDef.SetmFitProperties(props);
            break;       
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a FitBas level "+std::to_string(aInpLevel)+" Input keyword to be used under #3 BasDef! " << std::endl;
            MIDASERROR(" Check your input file : " + aFileName + " please ");
         }
      }
   }

   return s;
}

} /*namespace input*/
} /*namespace midas*/
