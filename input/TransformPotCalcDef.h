/**
************************************************************************
* 
* @file                TransformPotCalcDef.h
*
* 
* Created:             07-12-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for transformation of potentials  
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef TRANSFORMPOTCALCDEF_H
#define TRANSFORMPOTCALCDEF_H

#include <string>

#include "input/Macros.h"
#include "pes/molecule/MoleculeFile.h"

using namespace midas;

namespace midas::input
{

class TransformPotCalcDef
{
   CREATE_VARIABLE(std::string, OperatorFileName, std::string{"DUMMY"});
   CREATE_VARIABLE(bool, KeepMaxCoupLevel, false);
   
   private:
      molecule::MoleculeFileInfo mInMolFileInfo = molecule::MoleculeFileInfo("","");
   public:
      virtual ~TransformPotCalcDef() {};

      void SetInMolFileInfo (const molecule::MoleculeFileInfo& arFileInfo) {mInMolFileInfo=arFileInfo;}
      molecule::MoleculeFileInfo GetInMolFileInfo() const {return mInMolFileInfo;}
};

} // namespace midas::input

#endif //TRANSFORMPOTCALCDEF_H
