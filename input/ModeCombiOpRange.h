/**
 *******************************************************************************
 * 
 * @file    ModeCombiOpRange.h
 * @date    14-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Class for containing a range of ModeCombi, and related functions.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODECOMBIOPRANGE_H_INCLUDED
#define MODECOMBIOPRANGE_H_INCLUDED

// Standard headers.
#include <functional>
#include <set>
#include <vector>
#include <unordered_map>

// Midas headers.
#include "input/ModeCombi.h"

// Forward declarations.
class GroupCouplings;
class FlexCouplings;
class FlexCoupCalcDef;

/***************************************************************************//**
 * @brief
 *    Holds a sorted range of ModeCombi%s.
 * 
 * The range is stored contigously in memory for providing fast iterations and
 * random access.
 *
 * The order of the ModeCombi%s is increasing, according to 
 * operator<(const ModeCombi&, const ModeCombi&).
 *
 * The class is designed to always be internally consistent, i.e after each
 * public method call that modifies the range (constructors, insertions,
 * erasures, etc.), the internal statistics are updated to reflect the details
 * of the current range. This makes insertions/erasures _expensive_, so they
 * should be done in batches, i.e. by inserting/erasing ranges of ModeCombi%s
 * (not single ones) at a time.
 *
 * This internal consistency also applies to the ModeCombiRefNr() of the stored
 * ModeCombi%s, which are kept updated, so that they always correspond to the
 * ModeCombi's index in the range.
 * The Address() of the ModeCombi%s are not modified as part of maintaining
 * internal consistency.
 ******************************************************************************/
class ModeCombiOpRange
{
   public:
      //@{
      //! const_iterator (reverse) to ModeCombi. Random-access for efficiency.
      using const_iterator         = std::vector<ModeCombi>::const_iterator;
      using const_reverse_iterator = std::vector<ModeCombi>::const_reverse_iterator;
      //@}

      //@{
      //! Default copy/move constructors/assignments and destructor.
      ModeCombiOpRange(const ModeCombiOpRange&) = default;
      ModeCombiOpRange(ModeCombiOpRange&&) = default;
      ModeCombiOpRange& operator=(const ModeCombiOpRange&) = default;
      ModeCombiOpRange& operator=(ModeCombiOpRange&&) = default;
      ~ModeCombiOpRange() = default;
      //@}

      //! Default constructor; sets up an empty ModeCombiOpRange.
      ModeCombiOpRange();

      //! All ModeCombi%s with given level or below, from given num. modes.
      ModeCombiOpRange(Uin aMaxExciLevel, Uin aNumModes);

      //! All ModeCombi%s of given excitation levels
      ModeCombiOpRange(const std::set<Uin>& aExciLevels, Uin aNumModes);

      //! Construct from max. levels (per mode) an num. modes.
      ModeCombiOpRange(Uin aMaxExciLevel, const std::vector<Uin>& arMaxExPerMode);

      //! Construct from GroupCouplings.
      ModeCombiOpRange(const GroupCouplings&);

      //! Construct from FlexCouplings.
      ModeCombiOpRange(const FlexCouplings&, Uin aMaxExciLevel, Uin aNumModes);

      //! Construct from _effective_ excitation level and counts.
      static ModeCombiOpRange ConstructFromEffectiveExciCount
         (  Uin aMaxEffectiveExciLevel
         ,  const std::vector<Uin>& arEffectiveExciCounts
         );

      /*********************************************************************//**
       * @name Queries
       ************************************************************************/
      //!@{
      //! The number of ModeCombi%s contained.
      Uin Size() const;

      //! The maximum excitation level of the range. (Or 0 if range is empty.)
      Uin GetMaxExciLevel() const;

      //! Number of ModeCombi%s with the given excitation level (i.e. num. modes).
      Uin NumModeCombisWithExcLevel(Uin) const;

      //! Ordered mode numbers contained (in combination) by ModeCombi%s in the range.
      std::vector<In> ModesContained() const;

      //! Number of modes contained (in combination) by ModeCombi%s in the range.
      Uin NumberOfModes() const;

      //! Smallest mode number contained. Hard error if NumberOfModes() is zero.
      In SmallestMode() const;

      //! Largest mode number contained. Hard error if NumberOfModes() is zero.
      In LargestMode() const;

      //! Number of ModeCombi%s whose size satisfies `aMax >= size >= aMin`.
      Uin ReducedSize(Uin aMax, Uin aMin = I_0) const;

      //! Number of empty ModeCombi%s contained. (0 or 1)
      Uin NumEmptyMCs() const;

      //! Calculate and output (an estimate of) the storage size of the object.
      Uin SizeOut(std::ostream&) const;

      //!@}

      /*********************************************************************//**
       * @name ModeCombi access
       * 
       * Only ref-to-const and const_iterator access provided; for modifying
       * ModeCombi%s of the range, use appropriate range modification
       * functions.
       *
       * The iterators conform to the STL interface.
       ************************************************************************/
      //!@{

      //@{
      //! const_iterator to (reverse) begin and end ModeCombi%s.
      const_iterator begin()  const;
      const_iterator end()    const;
      const_iterator cbegin() const;
      const_iterator cend()   const;
      const_reverse_iterator rbegin()  const;
      const_reverse_iterator rend()    const;
      const_reverse_iterator crbegin() const;
      const_reverse_iterator crend()   const;
      //@}

      //! First ModeCombi with at _least_ the given number of modes (or end()).
      const_iterator Begin(Uin) const;

      //! First-beyond-last ModeCombi with at _most_ the given number of modes.
      const_iterator End(Uin) const;

      //! Get the i'th ModeCombi from the range (no out-of-range check).
      const ModeCombi& GetModeCombi(Uin) const;

      //@{
      //! Find the ModeCombi in the range with the given mode numbers.
      bool Find(const std::vector<In>&, const_iterator&) const;
      bool Find(const ModeCombi&, const_iterator&) const;
      bool Find(const ModeCombi&, const_iterator&, In&) const;
      //@}

      //@{
      //! Does the range contain the ModeCombi?
      bool Contains(const std::vector<In>&) const;
      bool Contains(const ModeCombi&) const;
      //@}

      //! If range contains ModeCombi, assign refnum/address from the one in range to argument.
      bool IfContainedAssignAddressAndRefNum(ModeCombi&) const;

      //!@}

      /*********************************************************************//**
       * @name Range modifications
       * 
       * These all modify the range somehow, which also triggers updating of
       * the internal attributes of the object; this has some overhead, and
       * should therefore be done in batches, hence only vectorized versions of
       * Insert/Erase.
       ************************************************************************/
      //!@{

      //@{
      //! Insert vector of ModeCombi%s. Can be unsorted.
      void Insert(const std::vector<ModeCombi>&);
      void Insert(std::vector<ModeCombi>&&);
      //@}

      //@{
      //! Erase ModeCombi%s from the range. Can be unsorted.
      void Erase(const std::vector<ModeCombi>&);
      void Erase(std::vector<ModeCombi>&&);
      void Erase(const std::function<bool(const std::vector<In>&)>&);
      void Erase(const ModeCombiOpRange&);
      void Erase(ModeCombiOpRange&&);
      //@}

      //! Clear all contents of the container.
      void Clear();

      //@{
      //! Assign address to ModeCombi in the range, if contained.
      void AssignAddress(Uin arMc, In aAddress);
      void AssignAddress(const_iterator arMc, In aAddress);
      void AssignAddress(const std::vector<In>& arMc, In aAddress);
      //@}

      //!@}

   private:
      //! The ModeCombi%s; shall be sorted at all times.
      std::vector<ModeCombi> mModeCombisVec;

      //! Hash-map for constant time lookup of ModeCombi.
      std::unordered_map<std::vector<In>, Uin> mModeCombiHashMap;

      //! The number of ModeCombi%s per excitation level.
      std::vector<Uin> mNumModeCombisWithExcLevel;

      //! The (sorted) modes contained (in combination) by all the ModeCombi%s.
      std::vector<In> mModesContained;

      /*********************************************************************//**
       * @name Constructor utilities
       * 
       * Private utility functions used for implementing the public
       * constructors.
       ************************************************************************/
      //!@{
      //! Construct from max. level, num. modes, and aprove/reject ModeCombi functor.
      ModeCombiOpRange(Uin, Uin, const std::function<bool(const std::vector<In>&)>&);

      //! Generates ModeCombi%s from GroupCouplings.
      static std::vector<ModeCombi> GenerateModeCombisFromGroupCouplings(const GroupCouplings&);

      //! Generates ModeCombi%s from FlexCouplings.
      static std::vector<ModeCombi> GenerateModeCombisFromFlexCouplings(const FlexCouplings&, Uin, Uin);

      //! Used by GenerateModeCombisFromGroupCouplings().
      static void RecursAddModeCombiForGroup
         (  std::unordered_set<ModeCombi>& arModeCombis
         ,  std::vector<In> aTmp
         ,  const std::vector<In>& arGroupCombi
         ,  const std::vector<std::vector<In>>& arModesOfGroups
         ,  In aGroupNr
         ,  In aStartModeNr
         ,  In aLevel
         );

      //! The minimum value for MaxExciLevel from either integer or vector.
      static Uin ActualMaxFromMaxExPerMode
         (  Uin aMaxExciLevel
         ,  const std::vector<Uin>& arMaxExPerMode
         );

      //! The actual value for MaxExciLevel when using effective exci. counts.
      static Uin ActualMaxFromEffectiveExciCount
         (  Uin aMaxEffectiveExciLevel
         ,  const std::vector<Uin>& arEffectiveExciCounts
         );
      //!@}

      /*********************************************************************//**
       * @name Internal attributes utilities
       * 
       * Private utility functions used for keeping the internal attributes
       * consistent with the range.
       ************************************************************************/
      //!@{
      //! Go through the unique, sorted range, updating all internal attributes.
      void UpdateAttributes();
      //!@}
};

//! Sort range and make unique.
void SortAndRemoveDuplicates(std::vector<ModeCombi>&);

//! Output ModeCombiOpRange contents.
std::ostream& operator<<(std::ostream&, const ModeCombiOpRange&);

//! Does any ModeCombi in range have common indices with both ModeCombi%s?
bool DoesMcrConnectMcs(const ModeCombiOpRange&, const ModeCombi&, const ModeCombi&);

//! New range with the ModeCombi%s from arg. range that overlaps with arg. ModeCombi.
ModeCombiOpRange BuildCommonMcr(const ModeCombiOpRange&, const ModeCombi&);

//! ModeCombi%s built from same fixed modes and some modes from a source of non-fixed ones.
std::vector<ModeCombi> ModeCombisFixedNonFixedModes
   (  Uin aMinNumNonFixed
   ,  Uin aMaxNumNonFixed
   ,  const std::vector<In>& arSourceOfModes
   ,  const std::vector<In>& arFixedModes
   );

//! ModeCombi%s built from fixed/non-fixed modes, restricted also appear in arg. range.
std::vector<ModeCombi> ModeCombisFixedNonFixedModesRestricted
   (  Uin aMinNumNonFixed
   ,  Uin aMaxNumNonFixed
   ,  const std::vector<In>& arSourceOfModes
   ,  const std::vector<In>& arFixedModes
   ,  const ModeCombiOpRange& arRestrictionRange
   );

//! Set to range that can overlap with <L(combi)|Operator.
void SetToLeftOperRange
   (  std::vector<ModeCombi>& arMcr
   ,  bool& arRemovedSome
   ,  const ModeCombi& arLeftModeCombi
   ,  const ModeCombi& arModeCombiCheck
   ,  const ModeCombiOpRange& arOpRange
   ,  const ModeCombiOpRange& arRestrictRange
   ,  const bool aRestrict = true
   ,  const bool aAddTo = false
   ,  const bool aIso = false
   ,  const In aIsOrder = I_0
   ,  const bool aCheckCouplingWhile = false
   );

//! Find the different partitions of the argument ModeCombi, under certain restrictions.
void SubSetSets
   (  const ModeCombi& arModeCombi
   ,  std::vector<std::vector<ModeCombi>>& arVecVecModeCombis
   ,  const ModeCombiOpRange& arModeCombiOpRange
   ,  In aMinSize = I_1
   );

//! Erase certain ModeCombi%s of the ModeCombiOpRange based on ADGA prescreening estimates.
void UpdateAdga
   (  ModeCombiOpRange& arMcr
   ,  Uin aLevel
   ,  const std::vector<Nb>& arAdgaPreScreenThr
   ,  const std::string& arAnalysisDir
   ,  const std::string& arMultilevelAnalysisIoDir
   ,  const std::string& arMultilevelSaveIoDir 
   );


//! Wrapper for some different legacy constructor types based on what's in FlexCoupCalcDef.
ModeCombiOpRange ConstructModeCombiOpRange
   (  const FlexCoupCalcDef&
   ,  Uin aMaxExciLevel
   ,  Uin aNumModes
   );

//! Wrapper for some different legacy constructor types based on empty/nonempty arMaxExPerMode.
ModeCombiOpRange ConstructModeCombiOpRange
   (  Uin aMaxExciLevel
   ,  const std::vector<In>& arMaxExPerMode
   ,  Uin aNumModes
   );

//! Construct a range where the given modes don't count towards the max. exci. level.
ModeCombiOpRange ConstructModeCombiOpRangeFromExtendedRangeModes
   (  Uin aMaxEffectiveExciLevel
   ,  Uin aNumModes
   ,  const std::set<In>& arExtendedRangeModes
   );

//! Check num.modes, empty ModeCombi, canonical 1-mode ModeCombi%s (`{0},...,{M-1}`).
bool ValidateExciMcr
   (  const ModeCombiOpRange& arMcr
   ,  Uin aNumModesExpected
   ,  std::ostream& arErrShort
   ,  std::ostream& arErrLong
   ,  bool aMustContainEmptyMc = true
   ,  bool aMustContainCanonicalOneModeMcs = true
   );

//@{
//! Set canonical addresses according to given number of modals per mode.
Uin SetAddresses(ModeCombiOpRange& arMcr, const std::vector<Uin>& arNumModals);
Uin SetAddresses(ModeCombiOpRange& arMcr, const std::vector<In>& arNumModals);
//@}

#endif/*MODECOMBIOPRANGE_H_INCLUDED*/
