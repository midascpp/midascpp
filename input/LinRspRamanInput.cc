#include "LinRspRamanInput.h"

#include <string>
#include <map>
#include <vector>

#include "util/conversions/VectorFromString.h"
#include "util/conversions/FromString.h"
#include "input/OpInfo.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"

std::map<std::string,std::string> InputMapFromStringVector(const std::vector<std::string>& str_vec)
{
   std::map<std::string,std::string> input_map;
   
   for(size_t i=0; i<str_vec.size(); ++i)
   {
      auto key_val_vec = midas::util::StringVectorFromString(str_vec[i],'=');
      input_map[key_val_vec[0]] = key_val_vec[1];
   }

   return input_map;
}


LinRspRamanInput ParseLinRspRamanInput(const std::string& input)
{
   auto str_vec = midas::util::StringVectorFromString(input);
   const std::map<std::string,std::string> input_map = InputMapFromStringVector(str_vec);
   
   LinRspRamanInput raman_input
      (  input_map.find("oper")->second
      ,  oper::StringToPropertyType(midas::input::ParseInput(input_map.find("type")->second))
      ,  midas::util::FromString<Nb>(input_map.find("frq")->second)
      );

   return raman_input;
}
