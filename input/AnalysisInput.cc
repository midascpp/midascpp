/**
************************************************************************
* 
* @file                AnalysisInput.cc
*
* 
* Created:             02-02-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the Analysis midas module 
* 
* Last modified: Wed Nov 25, 2009  02:50PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <map>
#include <algorithm>
#include <string>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "test/Test.h"
#include "nuclei/Nuclei.h"
#include "nuclei/Vector3D.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/AnalysisCalcDef.h"
#include "input/IsKeyword.h"
#include "input/GetLine.h"
#include "input/FindKeyword.h"
#include "util/conversions/FromString.h"

namespace midas::input
{

/**
* Read and check Analysis input.
* Read until next s = "#..." which is returned.
* */
std::string AnalysisInput
   (  std::istream& Minp
   ,  bool passive
   ,  std::string aInputFlag
   )
{
   //Mout << " Function: AnalysisInput " << endl;
   gDoAnalysis = true;
   gAnalysisCalcDef.emplace_back();
   AnalysisCalcDef& calc_def = gAnalysisCalcDef.back();

   // enum + map
   enum INPUT  
      {  ERROR, IOLEVEL, HISTBOXES, FWHM, SPECTRALRANGE,NOPS, RSPIR, MERGERSPFUNC, MERGERSPFUNCGATHERFILES
      ,  LINRSPRAMAN, SPECTRUMANALYSISPOINTS, SPECTRUMANALYSISINTERPOLATE, SPECTRUMANALYSISINTERPOLATESTEP
      ,  SPECTRUMANALYSISNOCALCULATEWEIGHTS
      };
   
   const std::map<std::string,INPUT> input_word =
      {  {"#2IOLEVEL",IOLEVEL}
      ,  {"#2HISTBOXES",HISTBOXES}
      ,  {"#2FWHM",FWHM}
      ,  {"#2SPECTRALRANGE",SPECTRALRANGE}
      ,  {"#2NOPS",NOPS}
      ,  {"#2RSPIR",RSPIR}
      ,  {"#2MERGERSPFUNC",MERGERSPFUNC}
      ,  {"#2MERGERSPFUNCGATHERFILES",MERGERSPFUNCGATHERFILES}
      ,  {"#2LINRSPRAMAN",LINRSPRAMAN}
      ,  {"#2SPECTRUMANALYSISPOINTS",SPECTRUMANALYSISPOINTS}
      ,  {"#2SPECTRUMANALYSISINTERPOLATE",SPECTRUMANALYSISINTERPOLATE}
      ,  {"#2SPECTRUMANALYSISINTERPOLATESTEP",SPECTRUMANALYSISINTERPOLATESTEP}
      ,  {"#2SPECTRUMANALYSISNOCALCULATEWEIGHTS",SPECTRUMANALYSISNOCALCULATEWEIGHTS}
      };

   std::string s;
   std::string s_orig;
   bool already_read = false;

   In input_level = 2;
   while (( already_read || GetLine(Minp,s) ))
   {
      if (passive) continue;                                  // If passive loop simple to next #
      
      already_read = false;
      s_orig = s;
      s = ParseInput(s);
      
      if(IsLowerLevelKeyword(s,input_level))
      {
         break;
      }

      INPUT input = FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            GetLine(Minp,s);                                  // Get new line with input
            gAnalysisIoLevel = midas::util::FromString<In>(s);
            break;
         }
         case HISTBOXES:
         {
            GetLine(Minp,s);                                  // Get new line with input
            gExtHistogram = midas::util::FromString<In>(s);
            break;
         }
         case FWHM:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> s;
            gAnalysisFwhm = midas::util::FromString<Nb>(s);
            input_s >> s;
            std::transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            if (s.find("EV")!= s.npos) gAnalysisFwhm /= C_AUTEV;     // Delete ALL blanks
            break;
         }
         case SPECTRALRANGE:
         {
            GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            input_s >> gAnalysisSpeBegin >> gAnalysisSpeEnd;
            std::transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
            if (s.find("EV")!= s.npos) gAnalysisSpeEnd /= C_AUTEV;     // Delete ALL blanks
            if (s.find("EV")!= s.npos) gAnalysisSpeBegin /= C_AUTEV;   // Delete ALL blanks
            break;
         }
         case NOPS:
         {
            gNoPs=true; 
            break;
         }
         case RSPIR:
         {
            already_read = calc_def.CreateAndReadRspIr(Minp,s);
            break;
         }
         case MERGERSPFUNC:
         {
            already_read = calc_def.CreateAndReadMergeRsp(Minp,s);
            break;
         }
         case MERGERSPFUNCGATHERFILES:
         {
            already_read = calc_def.ReadRspMergerGatherFiles(Minp,s);
            break;
         }
         case LINRSPRAMAN:
         {
            already_read = calc_def.CreateAndReadLinRspRaman(Minp,s);
            break;
         }
         case SPECTRUMANALYSISPOINTS:
         {
            already_read = calc_def.ReadSpectrumAnalysisPoints(Minp,s);
            break;
         }
         case SPECTRUMANALYSISINTERPOLATE:
         {
            already_read = calc_def.ReadSpectrumAnalysisInterpolate(Minp,s);
            break;
         }
         case SPECTRUMANALYSISINTERPOLATESTEP:
         {
            already_read = calc_def.ReadSpectrumAnalysisInterpolateStep(Minp,s);
            break;
         }
         case SPECTRUMANALYSISNOCALCULATEWEIGHTS:
         {
            already_read = calc_def.ReadSpectrumAnalysisNoCalculateWeights(Minp,s);
            break;
         }
         default:
         {
            InputKeywordError(s_orig,"Analysis",2);
         }
      }
   } 

   return s; 
}

} /* namespace midas::input */ 

/**
* Initialize Analysis variables
* */
void InitializeAnalysis()
{
   // Mout << " Initializing Analysis variables: " << endl;
   gDoAnalysis               = false; 
   gAnalysisIoLevel          = I_0;
   gExtHistogram             = I_0;
   gAnalysisFwhm             = C_1;
   gAnalysisSpeBegin         = C_0;
   gAnalysisSpeEnd           = C_1;
   gNoPs                     = false;
}
