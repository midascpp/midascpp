/**
************************************************************************
* 
* @file                midascpp.cc
*
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   MAIN for the midas program. passes control on.
* 
* Last modified:       16-11-2016 (Niels)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <sstream>
#include <string>
#include <limits>
#include <iostream>
#include <csignal>

// Link to Standard headers:
#include "inc_gen/time_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "inc_gen/GitVersion.h"
#include "inc_gen/Warnings.h"
#include "inc_gen/Compiler.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "pes/Pes.h"
#include "fileconversion/FileConversionDrv.h"
#include "vscf/VscfDrv.h"
#include "system/System.h"
#include "vcc/VccDrv.h"
#include "tdecomp/Tdecomp.h"
#include "td/tdh/TdHDrv.h"
#include "td/tdvcc/TdvccDrv.h"
#include "td/mctdh/McTdHDrv.h"
#include "ecor/Ecor.h"
#include "geoopt/GeoOpt.h"
#include "mlearn/MLTask.h"
#include "test/Test.h"
#include "analysis/Analysis.h"
#include "util/MidasStream.h"
#include "util/MidasSystemCaller.h"
#include "util/Os.h"
#include "util/SignalHandlers.h"
#include "util/RandomNumberGenerator.h"
#include "util/paral/spawn_process.h"

// MPI headers
#include "mpi/Init.h"
#include "mpi/Info.h"

#include "concurrency/Info.h"

#ifdef MIDAS_MEM_DEBUG
#include "util/midas_mem_check.h"
midas::mem::mem_stat& stat_mem = midas::mem::get_mem_stat();
#endif

/**
* First thing: set up files for input and output in global scope.
* Input/output is now through Midas.inp and Midas.out 
* */
//mbh: For use in MPI, define Mout as MidasStream
std::ofstream MidasOutFile;
MidasStreamBuf Mout_buf(MidasOutFile);
MidasStream Mout(Mout_buf);

/**
 * Output the MidasCpp Logo as ASCII.
 *
 * @param aOs
 *    The output stream.
 **/
void OutputLogo
   (  std::ostream& aOs
   )
{

   aOs << "$                                                                      $\n"
          "$                              ,********,                              $\n"
          "$                          ,****        ****,                          $\n"
          "$                      ,****        @@      ****,                      $\n"
          "$                   ,***           @@@          ***,                   $\n"
          "$                  ,*              @@@             *,                  $\n"
          "$                  **     @@@      @@@   @@@       **                  $\n"
          "$                  **      @@@     @@@    @@@      **                  $\n"
          "$                  **       @@     @@@     @@@     **                  $\n"
          "$                  **       @@     @@@      @@     **                  $\n"
          "$                  **       @@     @@@     @@      **                  $\n"
          "$                  **       @@     @@@    @@       **                  $\n"
          "$                  **        @@@@@@@@@@@@@         **                  $\n"
          "$                  '*              @@@             *'                  $\n"
          "$                   '***           @@@          ***'                   $\n"
          "$                      '****       @@       ****'                      $\n"
          "$                          '****        ****'                          $\n"
          "$                              '********'                              $\n"
          "$             ___  ____     _           _____                          $\n"
          "$             |  \\/  (_)   | |         /  __ \\                         $\n"
          "$             | .  . |_  __| | __ _ ___| /  \\/_ __  _ __               $\n"
          "$             | |\\/| | |/ _` |/ _` / __| |   | '_ \\| '_ \\              $\n"
          "$             | |  | | | (_| | (_| \\__ \\ \\__/\\ |_) | |_) |             $\n"
          "$             \\_|  |_/_|\\__,_|\\__,_|___/\\____/ .__/| .__/              $\n"
          "$                                            | |   | |                 $\n"
          "$                                            |_|   |_|                 $\n"
          "$                                                                      $\n"
          "$----------------------------------------------------------------------$\n"
      ;
}

/**
 * Output Midas Output header 
 *
 * @param aOs   
 *    The output stream to output to.
 **/
void OutputHeader
   (  std::ostream& aOs
   )
{
   int width             = 10;
   std::string delimeter = " : ";

   // output name of program
   Out72Char(aOs,'$', '$', '$');
   
   OutputLogo(aOs);

   Out72Char(aOs,'$', ' ', '$');
   OneArgOut72(aOs, " MIDAS CPP ", '$');
   OneArgOut72(aOs, " Molecular Interactions Dynamics And Simulations", '$');
   OneArgOut72(aOs, " C++/Chemistry Program Package/Completely Pathetic Programming ", '$');
   Out72Char(aOs, '$', ' ', '$');
   
   // output version
   std::ostringstream s;
   s << " " << std::left << std::setw(width) << "Version" << delimeter << MidasVersion();
   std::string s2 = s.str();
   OneArgOut72(aOs, s2, '$');

   // general git info: git repository or not?
   std::ostringstream oss_git_info;
   oss_git_info << " " << std::left << std::setw(width) << "Git info" << delimeter << GIT_INFO_MSG;
   OneArgOut72(aOs, oss_git_info.str(), '$');

   // Only output git info if this is a git repository.
   if constexpr(int(GIT_INFO) == 0)
   {
      // git commit and repository status (clean/dirty)
      std::ostringstream oss_commit;
      oss_commit << " " << std::left << std::setw(width) << "Git commit" << delimeter << GIT_COMMIT;
      if constexpr(int(GIT_REPO_DIRTY) == 0)
      {
         oss_commit << " (clean)";
      }
      else if constexpr(int(GIT_REPO_DIRTY) == 1)
      {
         oss_commit << " (dirty)";
      }
      OneArgOut72(aOs, oss_commit.str(), '$');
      
      // git branch
      std::ostringstream oss_branch;
      oss_branch << " " << std::left << std::setw(width) << "Git branch" << delimeter << GIT_BRANCH;
      OneArgOut72(aOs, oss_branch.str(), '$');
      
      // output date
      std::ostringstream oss_date;
      oss_date << " " << std::left << std::setw(width) << "Git date" << delimeter << GIT_DATE;
      OneArgOut72(aOs, oss_date.str(), '$');
   }

   // Output compiler
   std::ostringstream oss_compiler;
   oss_compiler << " " << std::left << std::setw(width) << "Compiler" << delimeter << COMPILER_STRING;
   OneArgOut72(aOs, oss_compiler.str(), '$');
   
   // Output hostname
   std::ostringstream oss_hostname;
   oss_hostname << " " << std::left << std::setw(width) << "Hostname" << delimeter << midas::os::Gethostname();
   OneArgOut72(aOs, oss_hostname.str(), '$');
   
   //
   Out72Char(aOs, '$', ' ', '$');
   OneArgOut72(aOs, " Ove Christiansen", '$');
   OneArgOut72(aOs, " Department of Theoretical Chemistry", '$');
   OneArgOut72(aOs, " Aarhus University", '$');
   OneArgOut72(aOs, " Email: ove@chem.au.dk", '$');
   Out72Char(aOs, '$', ' ', '$');
   Out72Char(aOs, '$', '$', '$');
   aOs << endl;
}

/**
 * Output information about dirty/unclean git repository, if relevant.
 **/
void DirtyRepositoryHeader
   (  std::ostream& aOs
   )
{
   if constexpr(int(GIT_REPO_DIRTY) == 1)
   {
      std::string dirty_repo = "Repository dirty when compiled; results may be irreproducible.";
      Out72Char(aOs, '+', '-', '+');
      Out72Char(aOs, '|', ' ', '|');
      OneArgOut72(aOs, " "+dirty_repo, '|');
      OneArgOut72(aOs, " Output from 'git status --porcelain' was", '|');
      std::stringstream oss_git_status;
      oss_git_status << GIT_STATUS_PORCELAIN;
      for(std::string line; std::getline(oss_git_status, line);)
      {
         OneArgOut72(aOs, "    "+line, '|');
      }
      Out72Char(aOs, '|', ' ', '|');
      Out72Char(aOs, '+', '-', '+');
      aOs << std::endl;
      MidasWarning(dirty_repo);
      aOs << std::endl;
   }
}

/**
 * Main program
 **/
int main 
   (  int   argc
   ,  char* argv[]
   )
{
   midas::util::SetupSignalHandlers();
   
   // Initialize MPI
   midas::mpi::Initialize(&argc, &argv);
   setup_spawn_process();

   // parse commandline
   auto continue_execution = midas::input::ProgramSettings::CommandLineInput(argc, argv, midas::input::gProgramSettings);

   MidasOutFile.open(midas::input::gProgramSettings.GetOutputFileName(), ios_base::app);
   
   MidasSystemCaller::Init(); // initialize midas system caller
   
   if(midas::mpi::IsMaster())
   {
      OutputHeader(Mout);
      DirtyRepositoryHeader(Mout);
      midas::mpi::OutputMpiHeader(Mout);
      OutputDate(Mout,"\n Midas started at ");
   }
   
   // run input driver
   {
      auto mpi_region_guard = midas::mpi::StartMpiRegion();
      InputDrv(midas::input::gProgramSettings.GetInputFilePath());
   }
   
   midas::util::SeedRng(midas::input::gProgramSettings.GetGeneralSettings().GetSeed());
   midas::concurrency::Initialize(midas::input::gProgramSettings.GetNumThreads());
   if(midas::mpi::IsMaster())
   {
      midas::concurrency::OutputConcurrencyHeader(Mout);
   }
   
   // After input processing we change to rank scratch
   midas::input::gProgramSettings.SetRankScratch(midas::input::gProgramSettings.GetScratch() + "/" + std::to_string(midas::mpi::GlobalRank()));
   int status = 0;
   if((status = filesystem::Mkdir(midas::input::gProgramSettings.GetRankScratch())) != 0)
   {
      filesystem::ErrorCheck("Cannot create rank scratch '" + midas::input::gProgramSettings.GetRankScratch() + "'.");
   }
   os        ::Chdir(midas::input::gProgramSettings.GetRankScratch());

   auto mpi_guard = midas::mpi::Init(midas::mpi::CommunicatorWorld());
   
   // If rank 0 we start the calculation
   if (  midas::mpi::IsMaster()
      )
   {
      Timer time_all;

      if (gDoTest)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::TEST);
         auto guard = midas::mpi::StartMpiRegion();

         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Entering midas::test::Test().");
         }

         midas::test::Test(gTestDrivers);

         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Leaving midas::test::Test().");
         }
      }

      if (gDoTdecomp)
      {
         TdecompDrv();
      }

      if (gDoSystem)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::SYSTEM);
         auto guard = midas::mpi::StartMpiRegion();

         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Entering SystemDrv().");
         }

         SystemDrv();

         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Leaving SystemDrv().");
         }
      }
 
      if (gDoPes)
      {
         if(midas::mpi::GlobalSize() > 1) 
         {
            MidasWarning("PES MPI is still in testing stage.");
         }

         midas::pes::PesDrv();
      }

      if (gDoVscf)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::VSCF);
         auto guard = midas::mpi::StartMpiRegion();
         
         midas::vscf::VscfDrv();
      }

      if (gDoTdH)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::TDH);
         auto guard = midas::mpi::StartMpiRegion();

         midas::tdh::TdHDrv();
      }

      if (gDoVcc)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::VCC);
         auto guard = midas::mpi::StartMpiRegion();
         
         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Entering VccDrv().");
         }

         midas::vcc::VccDrv();
         
         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Exited VccDrv().");
         }
      }

      if (  gDoMcTdH
         )
      {
         midas::mctdh::McTdHDrv();
      }

      if (gDoTdvcc)
      {
         midas::mpi::SendSignal(midas::mpi::CommunicatorWorld(), midas::mpi::signal::TDVCC);
         auto guard = midas::mpi::StartMpiRegion();
         
         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Entering TdvccDrv().");
         }

         midas::tdvcc::TdvccDrv();

         if constexpr (MPI_DEBUG)
         {
            midas::mpi::WriteToLog(" Master :  Exited TdvccDrv().");
         }
      }

      if (gDoEcor)
      {
         EcorDrv();
      }

      if (gDoGeoOpt)
      {
         GeoOptDrv();
      }

      if (gDoMLTask)
      {
         MLDrv();
      }
   
      if (gDoAnalysis)
      {
         Analysis();
      }

      if (gDoFileConversion)
      {
         FileConversionDrv();
      }

      //
      // some timings
      // 
      Mout << '\n';
      bool relative_to_start = true;
      time_all.CpuOut(Mout, " Total CPU  time used in Midas: ", relative_to_start);
      time_all.WallOut(Mout, " Total Wall time used in Midas: ", relative_to_start);
      
      OutputDate(Mout,"\n Midas ended at ");

      PrintMidasWarnings();
      MidasOutFile.close();
   }

   //NB: MPI_FINALIZE is run automatically by the mpi_finalizer...
}
