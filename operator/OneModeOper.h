/**
************************************************************************
*
* @file                OneModeOper.h
*
* Created:             04-02-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   One Mode Operator Defintion for GlobalOperatorDefinitionClass.
*
* Last modified: Mon Feb 04, 2013  04:25PM
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#ifndef ONEMODEOPER_H
#define ONEMODEOPER_H

#include <string>
#include "util/Io.h"

/**
 * Base class for one-mode operators
 **/
template
   <  typename T
   >
class OneModeOperBase
{
   public:
      //! Alias
      using OperPtr = std::unique_ptr<OneModeOperBase<T>>;

      //! Types
      enum class OperType : int
      {  SIMPLE = 0
      ,  FUNCTION
      ,  STATETRANSFER
      ,  RSPTRANS
      ,  DELTAFUNCTION
      ,  DUMMY
      };

   private:
      //! Type
      virtual OperType TypeImpl
         (
         )  const = 0;

      //! Type as string
      virtual std::string ShowTypeImpl
         (
         )  const = 0;

   protected:
      //! The order of the differential operator to the right of a function operator
      In          mRDerOrd = I_0;

      //! The order of a differential operator (always of order 1) to the left of a function operator
      bool        mLDer = false;

      //! Is the one-mode operator a simple constant term
      bool        mIsUnitOperator = false;

      //! Is the one-mode operator part of the simple kinetic energy expression
      bool        mIsSimpleKeTerm = false;

      //! Is the one-mode operator part of Coriolis coupling contribution
      bool        mIsCoriolisTerm = false;

      //! Is the one-mode operator part of the total kinetic energy expression based on polysherical coordinates
      bool        mIsPscKeTerm = false;

      //!
      OneModeOperBase
         (  In aRDer
         ,  bool aLDer
         )
         :  mRDerOrd(aRDer)
         ,  mLDer(aLDer)
      {
         if (mLDer && mRDerOrd > I_1)
         {
            MIDASERROR("Undefined operator");
         }
      }

      //!
      void CheckMult
         (  const std::unique_ptr<OneModeOperBase<T>>& aPtr
         )  const
      {
         if (  ( this->mRDerOrd > I_1 && !aPtr->GetPow() )
            || this->mRDerOrd + aPtr->mRDerOrd > I_2
            || this->CheckMultImpl(aPtr)
            )
         {
            MIDASERROR("Product between One Mode Opers not defined");
         }
      }

      //!
      virtual bool CheckMultImpl
         (  const std::unique_ptr<OneModeOperBase<T>>& aPtr
         )  const
      {
         return false;
      }

      //!
      virtual bool CompareImpl
         (  const OperPtr&
         )  const = 0;

   public:
      //!
      OneModeOperBase(const OneModeOperBase&) = delete;

      //!
      OneModeOperBase& operator=(const OneModeOperBase&) = delete;

      //!
      OneModeOperBase() = delete;

      //@{
      //! OneModeOperBase class virtual functions
      virtual ~OneModeOperBase() = default;

      virtual LocalOperNr GetOperNrForResp
         (
         )  const
      {
         return -I_1;
      }

      virtual T EvaluateFunc
         (  const T&
         )
      {
         MIDASERROR("EvaluateFunc not implemented for operator of type '" + this->ShowType() + "'.");
         return T(0);
      }

      virtual OperPtr Multiply
         (  const OperPtr&
         )  const
      {
         MIDASERROR("Multiply not implemented for operator of type '" + this->ShowType() + "'.");
         return nullptr;
      }
      //@}

      //@{
      //! OneModeOperBase class virtual getters
      virtual const bool IsPoly() const               {return false;}
      virtual In GetPow() const                {return -I_1;}
      //@}

      //! Get the original (operator) function string
      virtual const std::string GetOrigOperString() const = 0;

      //! Writes an one-mode operator as a string
      const std::string OperString() const
      {
         std::string oper_str = "";
         // Write (DDQ)^i type one-mode operators
         if (GetRDer() != I_0 && !GetLDer() && GetPow() == I_0)
         {
            oper_str += GetOrigOperString() + "(DDQ)^" +  std::to_string(GetRDer());
         }
         // Write (DDQ)^i f(Q) (DDQ)^j type one-mode operators
         else if (GetRDer() != I_0 && GetLDer() && GetPow() != I_0)
         {
            oper_str += "(DDQ)^1*" + GetOrigOperString() + "*(DDQ)^" + std::to_string(GetRDer());
         }
         // Write f(Q) (DDQ)^j type one-mode operators
         else if (GetRDer() != I_0 && !GetLDer() && GetPow() != I_0)
         {
            oper_str += GetOrigOperString() + "*(DDQ)^" +  std::to_string(GetRDer());
         }
         // Write (DDQ)^i f(Q) type one-mode operators
         else if (GetRDer() == I_0 && GetLDer() && GetPow() != I_0)
         {
            oper_str += "(DDQ)^1*" + GetOrigOperString();
         }
         // Write f(Q) type one-mode operators
         else
         {
            oper_str += GetOrigOperString();
         }
         return oper_str;
      }

      const In& GetRDer() const                 {return mRDerOrd;}
      const bool& GetLDer() const               {return mLDer;}
      const bool& GetmIsUnitOperator() const    {return mIsUnitOperator;}
      const bool& GetmIsSimpleKeTerm() const    {return mIsSimpleKeTerm;}
      const bool& GetmIsCoriolisTerm() const    {return mIsCoriolisTerm;}
      const bool& GetmIsPscKeTerm() const       {return mIsPscKeTerm;}

      //! Type
      OperType Type
         (
         )  const
      {
         return this->TypeImpl();
      }

      //! Type as string
      std::string ShowType
         (
         )  const
      {
         return this->ShowTypeImpl();
      }

      //! Compare
      bool Compare
         (  const OperPtr& aOther
         )  const
      {
         return   (  this->Type() == aOther->Type() )        // Check type
               && (  this->GetRDer() == aOther->GetRDer() )  // Check right derivative order
               && (  this->GetLDer() == aOther->GetLDer() )  // Check if left derivative
               && (  this->CompareImpl(aOther)  );          // Check derived-class-specific stuff
      }
};
#endif //ONEMODEOPER_H
