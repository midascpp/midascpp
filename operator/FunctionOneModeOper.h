/**
************************************************************************
*
* @file                FunctionOneModeOper.h
*
* Created:             04-02-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   One Mode Operator Defintion for GlobalOperatorDefinitionClass.
*
* Last modified: Mon Feb 04, 2013  04:25PM
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#ifndef FUNCTIONONEMODEOPER_H
#define FUNCTIONONEMODEOPER_H

#include <string>
#include "util/Io.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "operator/OneModeOper.h"

/**
 * Operator of general functions.
 **/
template
   <  typename T
   >
class FunctionOneModeOper
   :  public OneModeOperBase<T>
   ,  public MidasFunctionWrapper<T>
{
   public:
      //! Alias
      using Base = OneModeOperBase<T>;
      using OperPtr = typename Base::OperPtr;
      using type_t = typename Base::OperType;

   private:
      //!
      type_t TypeImpl
         (
         )  const override
      {
         return type_t::FUNCTION;
      }

      //!
      std::string ShowTypeImpl
         (
         )  const override
      {
         return std::string("FunctionOneModeOper");
      }

      //!
      virtual bool CompareImpl(const OperPtr& aPtr) const override
      {
         return this->mPtr->Compare(static_cast<const FunctionOneModeOper<T>*>(aPtr.get())->mPtr);
      }


   public:
      //1
      FunctionOneModeOper() = delete;

      //!
      FunctionOneModeOper(const FunctionOneModeOper&) = delete; //If this is EVER needed it needs to be hooked up to MidasFunctionWrappers copy

      //!
      FunctionOneModeOper& operator=(const FunctionOneModeOper&) = delete;

      //! Constructor
      FunctionOneModeOper
         (  const std::string& aFuncStr
         ,  const In& aRDerOrd
         ,  const bool& aLDer
         ,  const FunctionContainer<T>& aFC
         ,  const bool& aKinOper
         )
         :  OneModeOperBase<T>
            (  aRDerOrd
            ,  aLDer
            )
         ,  MidasFunctionWrapper<T>
            (  aFuncStr
            ,  aFC
            ,  aKinOper
            )
      {
         if (aFuncStr == "")
         {
            MIDASERROR("The Function operator does not accept constant operators");
         }

         if (aKinOper)
         {
            this->mIsPscKeTerm = true;

            if (  aLDer
               || (!aLDer && aRDerOrd != I_0)
               )
            {
               this->mIsCoriolisTerm = true;
            }
         }
      }

      // Niels: Tell the compiler, that we are still using MidasFunctionWrapper::EvaluateFunc,
      // and that the next function declaration should not be an overload.
      using MidasFunctionWrapper<T>::EvaluateFunc;
      T EvaluateFunc(const T& aValue) override
      {
         this->mVariables[I_0] = aValue;
         return this->mPtr->eval(this->mVariables) - this->mZeroVal;
      }

      //@{
      //! FunctionOneModeOper class getters
      const bool IsPoly() const override {return false;}
      const std::string GetOrigOperString() const override
      {
         return this->FuncStr();
      }
      //@}
};

#endif //FUNCTIONONEMODEOPER_H
