/**
************************************************************************
* 
* @file                OperFile.h
*
* Created:             23-10-2013
*
* Author:              Bo Thomsen
*
* Short Description:   Class for holding the name and definitions needed to read in an operator from file
* 
* Last modified: Thu Oct 29, 2013 11:57AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef OPERFILE_H_INCLUDED
#define OPERFILE_H_INCLUDED

// std headers
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

namespace OperTypesForReadIn
{
   enum OperFileType{CORFILE, XXINERTIA, XYINERTIA, XZINERTIA, YYINERTIA, YZINERTIA, ZZINERTIA, GENKINOPER};
}

/**
 * holds information on operator files
 **/
class OperFile
{
   private:
      std::string  mOpFile;      ///< someone should describe me! 
      Nb           mCoef;        ///< someone should describe me!
      In           mModeNr;      ///< someone should describe me!
      In           mModeNr2;     ///< someone should describe me!
      GlobalOperNr mOper;        ///< someone should describe me!
      bool         mShouldBeIn;  ///< someone should describe me!
      In           mMcCoup;      ///< someone should describe me!

   public:
      /**
       *
       **/
      OperFile() = delete;
      
      /**
       *
       **/
      OperFile
         (  std::string aFile
         ,  Nb aCoef
         ,  In aModeNr
         ,  In aModeNr2
         ,  GlobalOperNr aOper
         ,  bool aIn
         ) 
         :  mOpFile(aFile)
         ,  mCoef(aCoef)
         ,  mModeNr(aModeNr)
         ,  mModeNr2(aModeNr2)
         ,  mOper(aOper)
         ,  mShouldBeIn(aIn)
         ,  mMcCoup(I_3) 
      {
      }
      
      /**
       * set stuff
       **/
      void SetMcCoup(const In& aIn)                {mMcCoup = aIn;}
      void SetOperFile(const std::string& aStr)    {mOpFile = aStr;}
      
      /**
       * get stuff
       **/
      In GetMcCoup()            const { return mMcCoup; }
      std::string GetFileName() const { return mOpFile; }
      Nb GetCoef()              const { return mCoef; }
      In GetModeNr()            const { return mModeNr; }
      In GetModeNr2()           const { return mModeNr2; }
      GlobalOperNr GetOper()    const { return mOper; }
      bool GetShouldBeIn()      const { return mShouldBeIn; }
};

#endif /* OPERFILE_H_INCLUDED */
