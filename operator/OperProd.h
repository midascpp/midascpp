/**
************************************************************************
* 
* @file                OperProd.h
*
* Created:             24-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Small Class for holding definition of operator product 
* 
* Last modified: Thu Jul 16, 2009  03:52PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef OPERPROD_H   
#define OPERPROD_H   

// Standard headers:
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <iostream>
using std::ostream;
#include <algorithm>
using std::find;

// My headers:
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

typedef vector<In>::iterator Vii;

class OperProd
{
   private:
      vector<LocalModeNr> mModes;    ///< Modes active in the operator product.
      vector<LocalOperNr> mOpers;    ///< Operators for each active mode in the operator product.
   
   public:
      OperProd(vector<LocalModeNr>& arModes, vector<LocalOperNr>& arOpers) {mOpers=arOpers; mModes=arModes;}
      OperProd(const LocalModeNr arMode, const LocalOperNr arOper)
      {mOpers.push_back(arOper); mModes.push_back(arMode);}

      //! Add a factor.
      void AddModeOperSet(const LocalModeNr arMode, const LocalOperNr arOper)
      {mOpers.push_back(arOper); mModes.push_back(arMode);}
     
      //! Get rid of strings if not needed anymore.
      void ResetIop(const In& arNr,const LocalOperNr arOp)
      {mOpers[arNr] = arOp;}

      //! Give oper for factor number.
      LocalOperNr OperNrLocal(const In& aFactor) const
      {return mOpers[aFactor];}

      //! Give modes for factor number.
      LocalModeNr ModeNrLocal(const In& aFactor) const
      {return mModes[aFactor];}

      //! Give Oper for mode number. 
      LocalOperNr OperForOperMode(const LocalModeNr aModeNrLocal) const
      {
         In i=I_0; bool found = false;
         for (i=0;i<NmodesInProd();i++) 
            if (mModes[i]==aModeNrLocal) {found = true; break;} 
         if (!found) MIDASERROR(" aModeNrLocal  not found in OperForOperMode"); 
         return mOpers[i];
      }  

      void Sort();             ///< Sort modes and operators according to mode numbers.
      
      In NmodesInProd() const  ///< Number of factors/active modes in Product.
      {return mOpers.size();}
      
      const vector<LocalModeNr>& GetModes() const {return mModes;} ///< Get all modes.
      const vector<LocalOperNr>& GetOpers() const {return mOpers;} ///< Get all operators.
      
      friend ostream& operator<<(ostream&, const OperProd&);
};

#endif

