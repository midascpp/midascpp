#ifndef MIDAS_OPERATOR_PROPERTY_TYPE_H_INCLUDED
#define MIDAS_OPERATOR_PROPERTY_TYPE_H_INCLUDED

#include <iostream>
#include <climits>

#include "util/FlatMap.hpp"

namespace midas
{
namespace oper
{

/**
 * Enum for subtypes
 **/
enum class order    : signed char { unknown = -2, match_all, scalar, vector, tensor2, tensor3, tensor4 };
enum class property : signed char { unknown = -2, match_all, energy, dipole, polarizability2, polarizability3, polarizability4 };
enum class axis     : signed char { unknown = -2, match_all, none = 0, x = 'x', y = 'y', z = 'z' };

static auto order_map = util::CreateFlatMap
   (  util::CreatePack(std::string("unknown"),   order::unknown)
   ,  util::CreatePack(std::string("match_all"), order::match_all)
   ,  util::CreatePack(std::string("scalar"),    order::scalar)
   ,  util::CreatePack(std::string("vector"),    order::vector)
   ,  util::CreatePack(std::string("tensor2"),   order::tensor2)
   ,  util::CreatePack(std::string("tensor3"),   order::tensor3)
   ,  util::CreatePack(std::string("tensor4"),   order::tensor4)
   );

static auto property_map = util::CreateFlatMap
   (  util::CreatePack(std::string("unknown"),          property::unknown)
   ,  util::CreatePack(std::string("match_all"),        property::match_all)
   ,  util::CreatePack(std::string("energy"),           property::energy)
   ,  util::CreatePack(std::string("dipole"),           property::dipole)
   ,  util::CreatePack(std::string("polarizability2"),  property::polarizability2)
   ,  util::CreatePack(std::string("polarizability3"),  property::polarizability3)
   ,  util::CreatePack(std::string("polarizability4"),  property::polarizability4)
   );

inline std::string OrderToString(const order& o)
{
   auto order_string_ptr = order_map.Value2ToValue1(o);
   if(order_string_ptr)
   {
      return *order_string_ptr;
   }

   return std::string{""};
}

inline std::string PropertyToString(const property& p)
{
   auto property_string_ptr = property_map.Value2ToValue1(p);
   if(property_string_ptr)
   {
      return *property_string_ptr;
   }

   return std::string{""};
}

/**
 * Property type struct
 **/
struct PropertyType                                                   
{
   static constexpr int axis_size = 6;

   /**
    * Data structures and members. Publicly available for manual manipulation.
    **/
   order     m_order            =  order   ::unknown;
   property  m_property         =  property::unknown;
   axis      m_axis[axis_size]  =  {  axis::unknown 
                                   ,  axis::unknown 
                                   ,  axis::unknown 
                                   ,  axis::unknown 
                                   ,  axis::unknown 
                                   ,  axis::unknown 
                                   };
   
   /**
    * Check sub PropertyTypes. Mostly for private use, but can be used publicly
    **/
   // Check against order.
   bool Is(const order& b) const
   {
      return (b == order::match_all || this->m_order == b);
   }
   
   // Check against property.
   bool Is(const property& s) const
   {
      return (s == property::match_all || this->m_property == s);
   }

   // Check against axis.
   bool Is(const axis a[6]) const
   {
      return   (a[0] == axis::match_all || this->m_axis[0] == a[0])
           &&  (a[1] == axis::match_all || this->m_axis[1] == a[1])
           &&  (a[2] == axis::match_all || this->m_axis[2] == a[2])
           &&  (a[3] == axis::match_all || this->m_axis[3] == a[3])
           &&  (a[4] == axis::match_all || this->m_axis[4] == a[4])
           &&  (a[5] == axis::match_all || this->m_axis[5] == a[5]);
   }
   
   /**
    * Public interface
    **/
   // Get order
   int GetOrder() const
   {
      // -2 is unknown, -1 is match_all
      return static_cast<int>(this->m_order);
   }

   // Get axis for index i
   axis GetAxis(int i) const
   {
      return this->m_axis[i];
   }
   
   // Get axis character for index i
   char GetAxisChar(int i) const
   {
      return static_cast<char>(this->m_axis[i]);
   }

   // Check against PropertyType
   bool Is(const PropertyType& t) const
   {
      return   (this == &t)
            || (  this->Is(t.m_order)
               && this->Is(t.m_property) 
               && this->Is(t.m_axis)
               )
            ;
   }
};

/**
 * Check exact equality. Different from checking with property_PropertyType::is(...).
 **/
inline bool operator ==(const PropertyType& t1, const PropertyType& t2)
{
   return   (&t1 == &t2)
         || (  (t1.m_order == t2.m_order)
            && (t1.m_property == t2.m_property)
            && (  (t1.m_axis[0] == t2.m_axis[0])
               && (t1.m_axis[1] == t2.m_axis[1])
               && (t1.m_axis[2] == t2.m_axis[2])
               && (t1.m_axis[3] == t2.m_axis[3])
               && (t1.m_axis[4] == t2.m_axis[4])
               && (t1.m_axis[5] == t2.m_axis[5])
               )
            )
         ;
}

/**
 * Check exact in-equality.
 **/
inline bool operator !=(const PropertyType& t1, const PropertyType& t2)
{
   return !(t1 == t2);
}

/**
 * Operator less-than.
 *
 * Used for sorting in some STL containers.
 **/
inline bool operator <(const PropertyType& t1, const PropertyType& t2)
{
   bool less_than =  t1.m_order    < t2.m_order
                  || t1.m_property < t2.m_property
                  || t1.m_axis[0]  < t2.m_axis[0]
                  || t1.m_axis[1]  < t2.m_axis[1]
                  || t1.m_axis[2]  < t2.m_axis[2]
                  || t1.m_axis[3]  < t2.m_axis[3]
                  || t1.m_axis[4]  < t2.m_axis[4]
                  || t1.m_axis[5]  < t2.m_axis[5]
                  ;

   return less_than;
}

/**
 * Operator output
 **/
inline std::ostream& operator<<(std::ostream& os, const PropertyType& t)
{
   os << "PropertyType:\n";
   os << "  property : " << PropertyToString(t.m_property) << "\n";
   os << "  order    : " << t.GetOrder() << " (" << OrderToString(t.m_order) << ")\n";
   os << "  axis     : ";
   for(int i = 0; i < t.GetOrder(); ++i)
   {
      os << t.GetAxisChar(i) << " ";
   }

   return os;
}

/**
 * Define types
 **/
static PropertyType unknown             = PropertyType{ order::unknown, property::unknown };
static PropertyType scalar              = PropertyType{ order::scalar,  property::match_all      , {axis::none}};
static PropertyType vector              = PropertyType{ order::vector,  property::match_all      , {axis::match_all}};
static PropertyType tensor2             = PropertyType{ order::tensor2, property::match_all      , {axis::match_all, axis::match_all}};
static PropertyType energy              = PropertyType{ order::scalar,  property::energy         , {axis::none}};
static PropertyType dipole              = PropertyType{ order::vector,  property::dipole         , {axis::match_all}};
static PropertyType dipole_x            = PropertyType{ order::vector,  property::dipole         , {axis::x}};
static PropertyType dipole_y            = PropertyType{ order::vector,  property::dipole         , {axis::y}};
static PropertyType dipole_z            = PropertyType{ order::vector,  property::dipole         , {axis::z}};

static PropertyType polarizability2     = PropertyType{ order::tensor2, property::polarizability2, {axis::match_all, axis::match_all}};
static PropertyType polarizability_xx   = PropertyType{ order::tensor2, property::polarizability2, {axis::x, axis::x}};
static PropertyType polarizability_xy   = PropertyType{ order::tensor2, property::polarizability2, {axis::x, axis::y}};
static PropertyType polarizability_xz   = PropertyType{ order::tensor2, property::polarizability2, {axis::x, axis::z}};
static PropertyType polarizability_yx   = PropertyType{ order::tensor2, property::polarizability2, {axis::y, axis::x}};
static PropertyType polarizability_yy   = PropertyType{ order::tensor2, property::polarizability2, {axis::y, axis::y}};
static PropertyType polarizability_yz   = PropertyType{ order::tensor2, property::polarizability2, {axis::y, axis::z}};
static PropertyType polarizability_zx   = PropertyType{ order::tensor2, property::polarizability2, {axis::z, axis::x}};
static PropertyType polarizability_zy   = PropertyType{ order::tensor2, property::polarizability2, {axis::z, axis::y}};
static PropertyType polarizability_zz   = PropertyType{ order::tensor2, property::polarizability2, {axis::z, axis::z}};

static PropertyType polarizability3     = PropertyType{ order::tensor3, property::polarizability3, {axis::match_all, axis::match_all, axis::match_all}};
static PropertyType polarizability_xxx  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::x, axis::x}};
static PropertyType polarizability_xxy  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::x, axis::y}};
static PropertyType polarizability_xxz  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::x, axis::z}};
static PropertyType polarizability_xyx  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::y, axis::x}};
static PropertyType polarizability_xyy  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::y, axis::y}};
static PropertyType polarizability_xyz  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::y, axis::z}};
static PropertyType polarizability_xzx  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::z, axis::x}};
static PropertyType polarizability_xzy  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::z, axis::y}};
static PropertyType polarizability_xzz  = PropertyType{ order::tensor3, property::polarizability3, {axis::x, axis::z, axis::z}};
static PropertyType polarizability_yxx  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::x, axis::x}};
static PropertyType polarizability_yxy  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::x, axis::y}};
static PropertyType polarizability_yxz  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::x, axis::z}};
static PropertyType polarizability_yyx  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::y, axis::x}};
static PropertyType polarizability_yyy  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::y, axis::y}};
static PropertyType polarizability_yyz  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::y, axis::z}};
static PropertyType polarizability_yzx  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::z, axis::x}};
static PropertyType polarizability_yzy  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::z, axis::y}};
static PropertyType polarizability_yzz  = PropertyType{ order::tensor3, property::polarizability3, {axis::y, axis::z, axis::z}};
static PropertyType polarizability_zxx  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::x, axis::x}};
static PropertyType polarizability_zxy  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::x, axis::y}};
static PropertyType polarizability_zxz  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::x, axis::z}};
static PropertyType polarizability_zyx  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::y, axis::x}};
static PropertyType polarizability_zyy  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::y, axis::y}};
static PropertyType polarizability_zyz  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::y, axis::z}};
static PropertyType polarizability_zzx  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::z, axis::x}};
static PropertyType polarizability_zzy  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::z, axis::y}};
static PropertyType polarizability_zzz  = PropertyType{ order::tensor3, property::polarizability3, {axis::z, axis::z, axis::z}};

static PropertyType polarizability4     = PropertyType{ order::tensor4, property::polarizability4, {axis::match_all, axis::match_all, axis::match_all, axis::match_all}};
static PropertyType polarizability_xxxx = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::x, axis::x, axis::x}};
static PropertyType polarizability_xxyy = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::x, axis::y, axis::y}};
static PropertyType polarizability_xxzz = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::x, axis::z, axis::z}};
static PropertyType polarizability_yyxx = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::y, axis::x, axis::x}};
static PropertyType polarizability_yyyy = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::y, axis::y, axis::y}};
static PropertyType polarizability_yyzz = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::y, axis::z, axis::z}};
static PropertyType polarizability_zzxx = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::z, axis::x, axis::x}};
static PropertyType polarizability_zzyy = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::z, axis::y, axis::y}};
static PropertyType polarizability_zzzz = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::z, axis::z, axis::z}};
static PropertyType polarizability_xyyx = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::y, axis::y, axis::x}};
static PropertyType polarizability_xzzx = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::z, axis::z, axis::x}};
static PropertyType polarizability_yxxy = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::x, axis::x, axis::y}};
static PropertyType polarizability_yzzy = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::z, axis::z, axis::y}};
static PropertyType polarizability_zxxz = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::x, axis::x, axis::z}};
static PropertyType polarizability_zyyz = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::y, axis::y, axis::z}};
static PropertyType polarizability_xyxy = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::y, axis::x, axis::y}};
static PropertyType polarizability_xzxz = PropertyType{ order::tensor4, property::polarizability4, {axis::x, axis::z, axis::x, axis::z}};
static PropertyType polarizability_yxyx = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::x, axis::y, axis::x}};
static PropertyType polarizability_yzyz = PropertyType{ order::tensor4, property::polarizability4, {axis::y, axis::z, axis::y, axis::z}};
static PropertyType polarizability_zxzx = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::x, axis::z, axis::x}};
static PropertyType polarizability_zyzy = PropertyType{ order::tensor4, property::polarizability4, {axis::z, axis::y, axis::z, axis::y}};
	
static PropertyType axis_x              = PropertyType{ order::vector , property::match_all      , {axis::x}};
static PropertyType axis_y              = PropertyType{ order::vector , property::match_all      , {axis::y}};
static PropertyType axis_z              = PropertyType{ order::vector , property::match_all      , {axis::z}};
static PropertyType axis_xx             = PropertyType{ order::tensor2, property::match_all      , {axis::x, axis::x}};
static PropertyType axis_xy             = PropertyType{ order::tensor2, property::match_all      , {axis::x, axis::y}};
static PropertyType axis_xz             = PropertyType{ order::tensor2, property::match_all      , {axis::x, axis::z}};
static PropertyType axis_yx             = PropertyType{ order::tensor2, property::match_all      , {axis::y, axis::x}};
static PropertyType axis_yy             = PropertyType{ order::tensor2, property::match_all      , {axis::y, axis::y}};
static PropertyType axis_yz             = PropertyType{ order::tensor2, property::match_all      , {axis::y, axis::z}};
static PropertyType axis_zx             = PropertyType{ order::tensor2, property::match_all      , {axis::z, axis::x}};
static PropertyType axis_zy             = PropertyType{ order::tensor2, property::match_all      , {axis::z, axis::y}};
static PropertyType axis_zz             = PropertyType{ order::tensor2, property::match_all      , {axis::z, axis::z}};

inline PropertyType copy(const PropertyType& p)
{
   return p;
}

static auto property_type_map = util::CreateFlatMap
   (  util::CreatePack(std::string("unknown"),             copy(unknown))
   ,  util::CreatePack(std::string("scalar"),              copy(scalar))
   ,  util::CreatePack(std::string("vector"),              copy(vector))
   ,  util::CreatePack(std::string("tensor2"),             copy(tensor2))
   ,  util::CreatePack(std::string("energy"),              copy(energy))
   // Dipole
   ,  util::CreatePack(std::string("dipole"),              copy(dipole))
   ,  util::CreatePack(std::string("dipole_x"),            copy(dipole_x))
   ,  util::CreatePack(std::string("dipole_y"),            copy(dipole_y))
   ,  util::CreatePack(std::string("dipole_z"),            copy(dipole_z))
   // Polarizability2
   ,  util::CreatePack(std::string("polarizability2"),     copy(polarizability2))
   ,  util::CreatePack(std::string("polarizability_xx"),   copy(polarizability_xx))
   ,  util::CreatePack(std::string("polarizability_yy"),   copy(polarizability_yy))
   ,  util::CreatePack(std::string("polarizability_zz"),   copy(polarizability_zz))
   ,  util::CreatePack(std::string("polarizability_xy"),   copy(polarizability_xy))
   ,  util::CreatePack(std::string("polarizability_xz"),   copy(polarizability_xz))
   ,  util::CreatePack(std::string("polarizability_yz"),   copy(polarizability_yz))
   // Polarizability3
   ,  util::CreatePack(std::string("polarizability3"),    copy(polarizability3))
   ,  util::CreatePack(std::string("polarizability_xxx"), copy(polarizability_xxx))
   ,  util::CreatePack(std::string("polarizability_xxy"), copy(polarizability_xxy))
   ,  util::CreatePack(std::string("polarizability_xxz"), copy(polarizability_xxz))
   ,  util::CreatePack(std::string("polarizability_xyx"), copy(polarizability_xyx))
   ,  util::CreatePack(std::string("polarizability_xyy"), copy(polarizability_xyy))
   ,  util::CreatePack(std::string("polarizability_xyz"), copy(polarizability_xyz))
   ,  util::CreatePack(std::string("polarizability_xzx"), copy(polarizability_xzx))
   ,  util::CreatePack(std::string("polarizability_xzy"), copy(polarizability_xzy))
   ,  util::CreatePack(std::string("polarizability_xzz"), copy(polarizability_xzz))
   ,  util::CreatePack(std::string("polarizability_yxx"), copy(polarizability_yxx))
   ,  util::CreatePack(std::string("polarizability_yxy"), copy(polarizability_yxy))
   ,  util::CreatePack(std::string("polarizability_yxz"), copy(polarizability_yxz))
   ,  util::CreatePack(std::string("polarizability_yyx"), copy(polarizability_yyx))
   ,  util::CreatePack(std::string("polarizability_yyy"), copy(polarizability_yyy))
   ,  util::CreatePack(std::string("polarizability_yyz"), copy(polarizability_yyz))
   ,  util::CreatePack(std::string("polarizability_yzx"), copy(polarizability_yzx))
   ,  util::CreatePack(std::string("polarizability_yzy"), copy(polarizability_yzy))
   ,  util::CreatePack(std::string("polarizability_yzz"), copy(polarizability_yzz))
   ,  util::CreatePack(std::string("polarizability_zxx"), copy(polarizability_zxx))
   ,  util::CreatePack(std::string("polarizability_zxy"), copy(polarizability_zxy))
   ,  util::CreatePack(std::string("polarizability_zxz"), copy(polarizability_zxz))
   ,  util::CreatePack(std::string("polarizability_zyx"), copy(polarizability_zyx))
   ,  util::CreatePack(std::string("polarizability_zyy"), copy(polarizability_zyy))
   ,  util::CreatePack(std::string("polarizability_zyz"), copy(polarizability_zyz))
   ,  util::CreatePack(std::string("polarizability_zzx"), copy(polarizability_zzx))
   ,  util::CreatePack(std::string("polarizability_zzy"), copy(polarizability_zzy))
   ,  util::CreatePack(std::string("polarizability_zzz"), copy(polarizability_zzz))
   // Polarizability4
   ,  util::CreatePack(std::string("polarizability4"),     copy(polarizability4))
   ,  util::CreatePack(std::string("polarizability_xxxx"), copy(polarizability_xxxx))
   ,  util::CreatePack(std::string("polarizability_xxyy"), copy(polarizability_xxyy))
   ,  util::CreatePack(std::string("polarizability_xxzz"), copy(polarizability_xxzz))
   ,  util::CreatePack(std::string("polarizability_yyxx"), copy(polarizability_yyxx))
   ,  util::CreatePack(std::string("polarizability_yyyy"), copy(polarizability_yyyy))
   ,  util::CreatePack(std::string("polarizability_yyzz"), copy(polarizability_yyzz))
   ,  util::CreatePack(std::string("polarizability_zzxx"), copy(polarizability_zzxx))
   ,  util::CreatePack(std::string("polarizability_zzyy"), copy(polarizability_zzyy))
   ,  util::CreatePack(std::string("polarizability_zzzz"), copy(polarizability_zzzz))
   ,  util::CreatePack(std::string("polarizability_xyyx"), copy(polarizability_xyyx))
   ,  util::CreatePack(std::string("polarizability_xzzx"), copy(polarizability_xzzx))
   ,  util::CreatePack(std::string("polarizability_yxxy"), copy(polarizability_yxxy))
   ,  util::CreatePack(std::string("polarizability_yzzy"), copy(polarizability_yzzy))
   ,  util::CreatePack(std::string("polarizability_zxxz"), copy(polarizability_zxxz))
   ,  util::CreatePack(std::string("polarizability_zyyz"), copy(polarizability_zyyz))
   ,  util::CreatePack(std::string("polarizability_xyxy"), copy(polarizability_xyxy))
   ,  util::CreatePack(std::string("polarizability_xzxz"), copy(polarizability_xzxz))
   ,  util::CreatePack(std::string("polarizability_yxyx"), copy(polarizability_yxyx))
   ,  util::CreatePack(std::string("polarizability_yzyz"), copy(polarizability_yzyz))
   ,  util::CreatePack(std::string("polarizability_zxzx"), copy(polarizability_zxzx))
   ,  util::CreatePack(std::string("polarizability_zyzy"), copy(polarizability_zyzy))
   // Axis
   ,  util::CreatePack(std::string("axis_x"),              copy(axis_x))
   ,  util::CreatePack(std::string("axis_y"),              copy(axis_y))
   ,  util::CreatePack(std::string("axis_z"),              copy(axis_z))
   ,  util::CreatePack(std::string("axis_xx"),             copy(axis_xx))
   ,  util::CreatePack(std::string("axis_xy"),             copy(axis_xy))
   ,  util::CreatePack(std::string("axis_xz"),             copy(axis_xz))
   ,  util::CreatePack(std::string("axis_yx"),             copy(axis_yx))
   ,  util::CreatePack(std::string("axis_yy"),             copy(axis_yy))
   ,  util::CreatePack(std::string("axis_yz"),             copy(axis_yz))
   ,  util::CreatePack(std::string("axis_zx"),             copy(axis_zx))
   ,  util::CreatePack(std::string("axis_zy"),             copy(axis_zy))
   ,  util::CreatePack(std::string("axis_zz"),             copy(axis_zz))
   );

/**
 * A little helper function to convert from string to PropertyType
 **/
inline PropertyType StringToPropertyType(const std::string& id)
{
   auto property_type_ptr = property_type_map.Value1ToValue2(id);
   if(property_type_ptr)
   {
      return *property_type_ptr;
   }

   return unknown;
}

/**
 * 
 **/
inline std::string PropertyTypeToString(const PropertyType& p)
{
   auto string_ptr = property_type_map.Value2ToValue1(p);
   if(string_ptr)
   {
      return *string_ptr;
   }

   return "unknown";
}

} /* namespace oper */
} /* namespace midas */

#endif /* MIDAS_OPERATOR_PROPERTY_TYPE_H_INCLUDED */
