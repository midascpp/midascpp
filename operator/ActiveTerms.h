/**
************************************************************************
* 
* @file                ActiveTerms.h
*
* Created:             05-12-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Small Class for holding a vector of terms 
* 
* Last modified: man mar 21, 2005  11:42
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ACTIVETERMS_H   
#define ACTIVETERMS_H   

// Standard headers:
#include <iostream>
#include <vector>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

class ActiveTerms
{
   private:

      std::vector<In> mActiveTerms;                  ///< Terms with operators in mode

   public:
      
      //! Default Constructor
      ActiveTerms() {}

      ///< Constructor
      ActiveTerms(std::vector<In>& arActiveTerms) 
      {
         mActiveTerms = arActiveTerms;
      }
      
      //! Constructor
      ActiveTerms(const In& aTerm) 
      {
         mActiveTerms.clear(); 
         mActiveTerms.push_back(aTerm);
      }
      
      //! Constructor
      void AddTerm(const In& aTerm)
      {
         mActiveTerms.push_back(aTerm);
      }
      
      //! Nr of active terms 
      In NactiveTerms() const 
      {
         return mActiveTerms.size();
      }  
      
      //! Give the term for an active term
      In TermActive(In aActiveTerm) const 
      {
         return mActiveTerms[aActiveTerm];
      }

      friend std::ostream& operator<<(std::ostream&, const ActiveTerms&);
};

inline std::ostream& operator<<(std::ostream& os, const ActiveTerms& at)
{
   os << at.mActiveTerms << std::endl;
   return os;
}

#endif

