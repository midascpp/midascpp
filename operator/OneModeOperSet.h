/**
************************************************************************
* 
* @file                OneModeOperSet.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for storing the simple one mode operators from OpDef
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ONEMODEOPERSET_H
#define ONEMODEOPERSET_H
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include <vector>
#include <map>

/**
 *
 **/
class OneModeOperSet
{
   private:
      std::vector<GlobalOperNr>           mData;
      std::map<GlobalOperNr, LocalOperNr> mConvert;
      
      //Stuff we need to track for H.O and Gaussian basis
      In   mHeighestQpow = I_0;
      In   mHeighestDDpow = I_0;
      In   mHeighestDDQpow = I_0;
      In   mHeighestQpowDD = I_0;
      In   mHeighestDDQpowDD = I_0;
       
      In   mQPosition = -I_1;
      In   mQSquarePoss = -I_1;
      bool mUnitOpPossEx = false;
      In   mUnitOpPoss = -I_1;

      //! Does the set contain only state-transfer operators?
      bool mStateTransferOnly = false;

      //! Highest I,J for state-transfer operators in this mode
      In mStateTransferIJMax = -I_1;

   public:
      //! Constructor
      OneModeOperSet() = default;
      
      //! Get number of one mode operators.
      In NumberOfOneModeOpers() const;

      //! Get local operator number from global operator number.
      LocalOperNr GetLocalOperNr(GlobalOperNr aGOpNr) const;

      //! Get local operator number from global operator number (integer overload).
      LocalOperNr GetLocalOperNr(In aGOpNr) const;
      
      
      GlobalOperNr GetGlobalOperNr(In aLocalOpNr) const {return mData[aLocalOpNr];}
      void InsertOper(In);
      In operator[](In aIn) const {return mData[aIn];}
      
      LocalOperNr GetUnitOpPoss() const 
      {
         if(!mUnitOpPossEx) MIDASERROR("Something very fishy in calculation, no unit oper exists"); 
         return mUnitOpPoss;
      }
      LocalOperNr GetQSquarePoss() const {return mQSquarePoss;}
      LocalOperNr GetQPosition() const { return mQPosition; }
      
      //!@{
      //! Get some stats...
      In GetHeighestQpow() const {return mHeighestQpow;}
      In GetHeighestDDpow() const {return mHeighestDDpow;}
      In GetHeighestDDQpow() const {return mHeighestDDQpow;}
      In GetHeighestQpowDD() const {return mHeighestQpowDD;}
      In GetHeighestDDQpowDD() const {return mHeighestDDQpowDD;}
      bool StateTransferOnly() const {return mStateTransferOnly;}
      In StateTransferIJMax() const { return mStateTransferIJMax; }
      //!@}
};

#endif /* SETFOROPDEF_H */

