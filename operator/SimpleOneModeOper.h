/**
************************************************************************
*
* @file                SimpleOneModeOper.h
*
* Created:             24-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   One Mode Operator Defintion for GlobalOperatorDefinitionClass.
*
* Last modified: Mon Oct 24, 2013  04:25PM
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#ifndef SIMPLEONEMODEOPER_H_INCLUDED
#define SIMPLEONEMODEOPER_H_INCLUDED

// std headers
#include <string>

// midas headers
#include "operator/OneModeOper.h"

/**
 * Simple one-mode operator consisting of only Q^i and DDQ^i terms.
 **/
template
   <  typename T
   >
class SimpleOneModeOper
   :  public OneModeOperBase<T>
{
   public:
      //! Alias
      using Base = OneModeOperBase<T>;
      using OperPtr = typename Base::OperPtr;
      using type_t = typename Base::OperType;

   private:
      //! The order of a function (non-differential) operator
      In          mPow = I_0;

      //!
      virtual bool CheckMultImpl
         (  const OperPtr& aPtr
         )  const override
      {
         return (aPtr->GetLDer() && (this->mRDerOrd || !this->mPow ) );
      }

      //!
      type_t TypeImpl
         (
         )  const override
      {
         return type_t::SIMPLE;
      }

      //!
      std::string ShowTypeImpl
         (
         )  const override
      {
         return std::string("SimpleOneModeOper");
      }

      //!
      bool CompareImpl
         (  const OperPtr& aPtr
         )  const override
      {
         return this->mPow == static_cast<const SimpleOneModeOper<T>*>(aPtr.get())->mPow;
      }


   public:
      //!
      SimpleOneModeOper(const SimpleOneModeOper&) = delete;

      //!
      SimpleOneModeOper& operator=(const SimpleOneModeOper&) = delete;

      //! Constructor
      SimpleOneModeOper
         (  const In& aPow
         ,  const In& aRDerOrd
         ,  const bool& aLDer
         ,  const bool& aKinOper
         )
         :  OneModeOperBase<T>
            (  aRDerOrd
            ,  aLDer
            )
         ,  mPow(aPow)
      {
         if (aRDerOrd != I_0 && !aLDer && aPow == I_0)
         {
            this->mIsSimpleKeTerm = true;

            if (aKinOper)
            {
               this->mIsPscKeTerm = true;
            }
         }

         if (  (aPow != I_0 && aLDer)
            || (aPow != I_0 && !aLDer && aRDerOrd != I_0)
            )
         {
            this->mIsCoriolisTerm = true;
         }

         if (aPow == I_0 && !aLDer && aRDerOrd == I_0)
         {
            this->mIsUnitOperator = true;
         }
      }

      //!
      T EvaluateFunc(const T& aValue) override
      {
         if (I_0 == this->mPow)
         {
            return C_1;
         }
         return std::pow(aValue, this->mPow);
      }

      //!
      T EvaluateFuncForVar(const T& aValue)
      {
         return C_1;
      }

      //!
      virtual OperPtr Multiply
         (  const OperPtr& aPtr
         )  const override
      {
         //<This checks if the multiplication is possible in our framework, if not it calls Error()...
         this->CheckMult(aPtr);

         const SimpleOneModeOper<T>* ptr = dynamic_cast<const SimpleOneModeOper<T>*>(aPtr.get());

         if (ptr)
         {
            // If both operators are just d/dq operators
            if (!(this->mPow + ptr->GetPow()))
            {
               return std::make_unique<SimpleOneModeOper<T>>(I_0, this->mRDerOrd + ptr->mRDerOrd, false, false);
            }

            return std::make_unique<SimpleOneModeOper<T>>(this->mPow+ptr->mPow, ptr->mRDerOrd, (static_cast<bool>(this->mRDerOrd) || this->mLDer), false);
         }
         else
         {
            MIDASERROR("Not implemented");
         }

         MIDASERROR("should not reach this part??");
         return nullptr;
      }

      //@{
      //! SimpleOneModeOper class getters
      const bool IsPoly
         (
         )  const override
      {
         return true;
      }

      In GetPow
         (
         )  const override
      {
         return mPow;
      }

      const std::string GetOrigOperString
         (
         )  const override
      {
         if (this->mPow != I_0)
         {
            return std::string("Q^" + std::to_string(this->mPow));
         }
         else if  (  !this->GetLDer()
                  && this->GetRDer() == I_0
                  )
         {
            return std::string("1");
         }
         else
         {
            return std::string();
         }
      }
      //@}
};

#endif // SIMPLEONEMODEOPER_H_INCLUDED
