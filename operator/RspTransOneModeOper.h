/**
************************************************************************
*
* @file                RspTransOneModeOper.h
*
* Created:             24-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   One Mode Operator Defintion for GlobalOperatorDefinitionClass.
*
* Last modified: Thu oct 24, 2013  04:25PM
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef RSPTRANSONEMODEOPER_H
#define RSPTRANSONEMODEOPER_H

#include <string>
#include "util/Io.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

/**
 *
 **/
template
   <  typename T
   >
class RspTransOneModeOper
   :  public OneModeOperBase<T>
{
   public:
      //! Alias
      using Base = OneModeOperBase<T>;
      using OperPtr = typename Base::OperPtr;
      using type_t = typename Base::OperType;

   private:
      //!
      LocalOperNr mOper;

      //!
      type_t TypeImpl
         (
         )  const override
      {
         return type_t::RSPTRANS;
      }

      //!
      std::string ShowTypeImpl
         (
         )  const override
      {
         return std::string("RspTransOneModeOper");
      }

      //!
      bool CompareImpl
         (  const OperPtr& aPtr
         )  const override
      {
         const RspTransOneModeOper<T>* ptr = static_cast<const RspTransOneModeOper<T>* >(aPtr.get());
         return mOper == ptr->mOper;
      }


   public:
      //!
      RspTransOneModeOper
         (  LocalOperNr aLocNr
         )
         :  OneModeOperBase<T>
               (  I_0
               ,  false
               )
         ,  mOper
               (  aLocNr
               )
      {
      }

      //!
      RspTransOneModeOper(const RspTransOneModeOper&) = delete;

      //!
      RspTransOneModeOper& operator=(const RspTransOneModeOper&) = delete;


      //!
      LocalOperNr GetOperNrForResp
         (
         )  const override
      {
         return mOper;
      }

      //!
      const std::string GetOrigOperString
         (
         )  const override
      {
         return std::string("Rsp_Oper_" + std::to_string(mOper));
      }
};

#endif /* RSPTRANSONEMODEOPER_H */
