/**
************************************************************************
* 
* @file                OperProd.cc
*
* Created:             24-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Members for OperProd class for holding a operator product 
* 
* Last modified: Thu Jul 16, 2009  03:57PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <string>
#include <algorithm>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "operator/OperProd.h"
#include "util/Io.h"

void OperProd::Sort()
{
   std::vector<std::pair<In,In> > mo_pairs;
   mo_pairs.reserve(mModes.size());

   for (In i=0; i<mModes.size(); ++i)
      mo_pairs.push_back(std::make_pair(mModes[i], mOpers[i]));

   std::sort(mo_pairs.begin(), mo_pairs.end());
   mModes.clear();
   mOpers.clear();

   for (In i=0; i<mo_pairs.size(); ++i)
   {
      mModes.push_back(mo_pairs[i].first);
      mOpers.push_back(mo_pairs[i].second);
   }
}

/**
* << overload for ostream
* */
std::ostream& operator<<(std::ostream& arOut, const OperProd& arOperProd)
{ 
   arOut << "(";
   for (In i=I_0;i<arOperProd.NmodesInProd();i++) arOut << " " << arOperProd.mModes[i]<<":"<<arOperProd.mOpers[i];
   arOut << ")";
   return arOut;
}

