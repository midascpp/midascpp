/**
************************************************************************
*
* @file                StateTransferOneModeOper.h
*
* Created:             20-09-2019
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   State-transfer operator |I><J|
*
* Last modified:
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef STATETRANSFERONEMODEOPER_H_INCLUDED
#define STATETRANSFERONEMODEOPER_H_INCLUDED

#include "operator/OneModeOper.h"

/**
 * Operator of type |I><J|.
 *
 * NB: Integrals of this operator can only be calculated in state basis, {|I>}, which is assumed to be orthonormal.
 **/
template
   <  typename T
   >
class StateTransferOneModeOper
   :  public OneModeOperBase<T>
{
   public:
      //! Alias
      using Base = OneModeOperBase<T>;
      using OperPtr = typename Base::OperPtr;
      using type_t = typename Base::OperType;

   private:
      //! Ket index, I
      In mI = -I_1;

      //! Bra index, J
      In mJ = -I_1;

      //!
      type_t TypeImpl
         (
         )  const override
      {
         return type_t::STATETRANSFER;
      }

      //!
      std::string ShowTypeImpl
         (
         )  const override
      {
         return std::string("StateTransferOneModeOper");
      }

   public:
      //! Delete default c-tor
      StateTransferOneModeOper() = delete;

      //! Delete copy c-tor
      StateTransferOneModeOper
         (  const StateTransferOneModeOper&
         )  = delete;

      //! Delete copy assignment
      StateTransferOneModeOper& operator=
         (  const StateTransferOneModeOper&
         )  = delete;

      //! Constructor
      StateTransferOneModeOper
         (  In aI
         ,  In aJ
         )
         :  OneModeOperBase<T>
               (  I_0
               ,  false
               )
         ,  mI(aI)
         ,  mJ(aJ)
      {
      }

      //! Compare
      virtual bool CompareImpl
         (  const OperPtr& aPtr
         )  const override
      {
         auto ptr = static_cast<const StateTransferOneModeOper*>(aPtr.get());
         return (mI == ptr->GetI()) && (mJ == ptr->GetJ());
      }

      //! Get the original (operator) function string
      const std::string GetOrigOperString
         (
         )  const override
      {
         return std::string("|" + std::to_string(mI) + "><" + std::to_string(mJ) + "|");
      }

      //! Get max I,J
      In MaxIJ
         (
         )  const
      {
         return std::max(mI, mJ);
      }

      //! Get I, J
      In GetI
         (
         )  const
      {
         return this->mI;
      }

      In GetJ
         (
         )  const
      {
         return this->mJ;
      }
};

#endif /* STATETRANSFERONEMODEOPER_H_INCLUDED */
