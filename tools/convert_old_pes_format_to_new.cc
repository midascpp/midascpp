/**
************************************************************************
*  
* @file                convert_old_pes_format_to_new.cc
*
* Created:             22-09-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   convert *_prop.out, *_der.out, ... to new format
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<string>
#include<sstream>
#include<fstream>
#include<sys/stat.h>
#include<cstdlib>
#include<map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "mpi/Interface.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"

#ifdef VAR_MPI
#include "mpi/Finalizer.h"
midas::mpi::Finalizer& mpi_finalize = midas::mpi::get_Finalizer(); // get finalizer object that will call
                                                  // mpi finalize on destruction
#endif /* VAR_MPI */

using namespace std;

bool IssueWarning();
void MakeTarArchive();
map<In,string> ConstructMap();
void TreatDerivatives(map<In,string>,MidasMatrix&);
bool GetNormalCoordinates(const string&,MidasMatrix&);

static In POINT_WITH_DER_FOUND = 0;
static In HESSIAN_FOUND = false;

ofstream outfile("ConverterFile.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

int main(int argc, char* argv[])
{
#ifdef VAR_MPI
   int a;
   char **c;
   MPI_Init(&a,&c);
#endif
   gMaxFileSize=I_10_8;
   if(midas::mpi::get_Interface().GetMpiRank()==0) {
      if (argc != 2) {
         cout << "ONE COMMAND LINE ARGUMENT: The normal coordinate file." << endl;
         exit(10);
      }
   
      MidasMatrix norm(I_1);
   
      string file_name=string(argv[1]);
   
      if(!GetNormalCoordinates(file_name,norm)) {
         cout << "I did not manage to retrieve normal coordinates..." << endl;
         exit(10);
      }
   
      if(!IssueWarning())
      {
         exit(10);
      }
      
      // tar files for safety
      MakeTarArchive();
      // now we construct map!
      map<In,string> id_code_map=ConstructMap();
      // all ok, except derivatives if they are here!
      TreatDerivatives(id_code_map,norm);
   }
#ifdef VAR_MPI
   //MPI_Barrier(MPI_COMM_WORLD);
   //MPI_Finalize();
#endif
   return 1;
}

bool GetNormalCoordinates(const string& file,MidasMatrix& mat)
{
   ifstream ifs(file.c_str());
   if(!ifs.is_open())
      return false;
   // search for FREQ
   string line;
   bool all_done=false;
   In n_atom=I_0;
   while(getline(ifs,line) && !all_done) {
      istringstream iss(line);
      string pattern;
      iss >> pattern;
      if(pattern=="FREQ")
         all_done=true;
      n_atom++;
   }
   n_atom-=3;
   if(!all_done)
      return false;
   // now count number of frequencies
   In n_freq=I_0;
   all_done=false;
   while(getline(ifs,line) && !all_done) {
      istringstream iss(line);
      string pattern;
      iss >> pattern;
      if(pattern=="COORD")
         all_done=true;
      n_freq++;
   }
   if(!all_done)
      return false;
   n_freq-=2;
   cout << "There are " << n_atom << " atoms and " << n_freq
        << " frequencies, is that ok?[yes/no] ";
   string answer;
   cin >> answer;
   if(answer!="yes")
      return false;
   mat.SetNewSize(3*n_atom,n_freq);
   // now coord begin!
   for(In i=0;i<n_freq;i++) {
      for(In j=0;j<n_atom;j++) {
         if(!getline(ifs,line))
            return false;
         istringstream iss(line);
         if(! (iss >> mat[3*j][i] >> mat[3*j+1][i] >> mat[3*j+2][i]) )
            return false;
      }
   }
   // at end of day, normal coordinates are
   cout.setf(ios::scientific);
   midas::stream::ScopedPrecision(12, std::cout);
   for(In i=0;i<n_freq;i++) {
      cout << "NC: " << i << ":" << endl;
      for(In j=0;j<3*n_atom;j++)
         cout << mat[j][i] << "  ";
      cout << endl;
   }
   return true;
}

bool IssueWarning()
{
   string answer;
   cout << "This will convert all properties into binary files." << endl;
   cout << "However, a tar archive will be made before proceeding." << endl << endl;
   cout << "!!!DO NOT!!! interrupt program while running" << endl << endl;
   cout << "Will you continue? [yes/no]: ";
   cin >> answer;
   if(answer=="yes" || answer=="YES")
      return true;
   return false;
}

void MakeTarArchive()
{
   struct stat stFileInfo; 
   int intStat; 

   // Attempt to get the file attributes  
   In i=0;
   string file_name;
   bool all_done=false;
   while(!all_done) {
      file_name="SavedPesArchive_"+std::to_string(i)+".tar.gz";
      intStat = stat(file_name.c_str(),&stFileInfo); 
      if(intStat == -1) { 
         // file does not exist, we continue
         all_done = true;
      }
      else
         cout << "File: " << file_name << " exists, checking next." << endl;
      i++;
   }

   // put all *_prop.out, *_der.out, Lis*, prop_no* in the archive
   string call="tar zcf "+file_name+" *_prop.out *_der.out Lis* prop_no_*";
   cout << "VERY IMPORTANT!!! DO NOT INTERRUPT BEFORE I RETURN" << endl;
   cout << "\"Archive created successfully\"" << endl << endl;
   int sys_ret = system(call.c_str());
   cout << "\t\t\tArhive created successfully" << endl;
}

map<In,string> ConstructMap()
{
   // loop through ListOfCalcs_Done file and add items
   ifstream ifs("ListOfCalcs_Done");
   if(!ifs.is_open()) {
      cout << "I could not open ListOfCalcs_Done for read! I quit" << endl;
   }
   string line;
   map<In,string> result;
   In id;
   string code;
   while(ifs >> id >> code) {
      cout << "Inserting " << id << " -> " << code << endl;
      result.insert(make_pair(id,code));
   }
   return result;
}

void TreatDerivatives(map<In,string> id_code_map,MidasMatrix& mat)
{
   MidasMatrix mat_t=Transpose(mat);
   for(map<In,string>::iterator it=id_code_map.begin();it!=id_code_map.end();it++)
   {
      In id=it->first;
      string code=it->second;
      // get derivatives into midas vector and matrix
      string file_name=std::to_string(id)+"_der.out";
      cout << "Reading from file: " << file_name << endl;
      ifstream der_in(file_name.c_str());
      if(!der_in.is_open()) {
         cout << "No derivatives found for point: " << id << " -> " << code << endl;
         exit(10);
      }
      POINT_WITH_DER_FOUND++;
      string line;
      getline(der_in,line);
      In n_coord;
      istringstream iss(line);
      iss >> n_coord;
      MidasVector grad_cart(n_coord);
      MidasMatrix hess_cart(n_coord);
      // next n_coord numbers are gradient
      In count=0;
      while(count<n_coord)
      {
         if(! (der_in >> grad_cart[count])) {
            cout << "Did not manage to get gradient for point: " << id << " -> " << code << endl;
            exit(10);
         }
         count++;
      }
      // now check for hessian
      count=0;
      while(count<(n_coord*n_coord))
      {
         if(! (der_in >> hess_cart[count/n_coord][count%n_coord])) {
            cout << "Did not manage to get gradient for point: " << id << " -> " << code << endl;
         }
         HESSIAN_FOUND=true;
         count++;
      }
      der_in.close();
      if(POINT_WITH_DER_FOUND>I_1 && !HESSIAN_FOUND) {
         cout << "Could not find 2nd derivative info for all points... Quit!" << endl;
         exit(10);
      }
      // transform gradient and hessian to normal coordinate frame
      MidasVector grad=mat_t*grad_cart;
      MidasMatrix hess_help=(mat_t*hess_cart);
      MidasMatrix hess=hess_help*mat;
      // write gradient: update n_coord to number of normal coord
      n_coord=grad.Size();
      DataCont deriv_cont;
      deriv_cont.SaveUponDecon(true);
      In offset=(POINT_WITH_DER_FOUND-1)*n_coord;
      if(HESSIAN_FOUND)
         offset+=(POINT_WITH_DER_FOUND-1)*n_coord*n_coord;
      string label="prop_no_1_derivatives.mbinary";
      if(offset!=I_0) {
         deriv_cont.GetFromExistingOnDisc(offset, label);
         if (! deriv_cont.ChangeStorageTo("InMem",true)) {
            MIDASERROR("Expected to find derivatives!!");
         }
      }
      else
         deriv_cont.ChangeStorageTo("InMem",true);
      deriv_cont.NewLabel(label);
      In first_offset=deriv_cont.Size();
      deriv_cont.SetNewSize(first_offset+n_coord);
      deriv_cont.DataIo(IO_PUT,grad,grad.Size(),first_offset);
      deriv_cont.SaveUponDecon(true);
      // and possibly hessian
      if (count<(n_coord*n_coord-1)) {
         // And update address file
         ofstream ofs("prop_no_1_derivatives_navigation.mpesinfo",ios::app);
         ofs << code << "   " << first_offset << endl;
         ofs.close();
      }
      else {
         In second_offset=deriv_cont.Size();
         deriv_cont.SetNewSize(second_offset+n_coord*n_coord);
         deriv_cont.DataIo(IO_PUT,hess,n_coord*n_coord,n_coord,n_coord,second_offset);
         deriv_cont.ChangeStorageTo("OnDisc",true);
         // And update address file
         ofstream ofs("prop_no_1_derivatives_navigation.mpesinfo",ios::app);
         ofs << code << "   " << first_offset << "   " << second_offset << endl;
         ofs.close();
      }
   }
}
