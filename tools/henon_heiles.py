#!/usr/bin/python2

##########################################################
#
#  author: Niels Kristian Madsen
#
#  Generate high-dimensional Henon-Heiles Hamiltonian (including kinetic energy)
#
##########################################################

import optparse
import itertools

# make header for .mop file
def make_header(file):
   file.write("#0MIDASOPERATOR\n");
   file.write("#1CONSTANTS\n");
   file.write("#1FUNCTIONS\n");
   file.write("#1OPERATORTERMS\n");

# make footer for .mop file
def make_footer(file, nmodes):
   file.write("#1MODENAMES\n");
   for m in xrange(0, nmodes):
      file.write("Q" + str(m) + " ");
   file.write("\n");
   file.write("#0MIDASOPERATOREND");


# Write Henon-Heiles Hamiltonian
def write_henon_heiles(file, nmodes, lambda_coef, ke, sys):
   for m in xrange(sys*nmodes, (sys+1)*nmodes):
      if ke:
         file.write("-0.5 DDQ^2(Q" + str(m) + ")\n");
      file.write(" 0.5 Q^2(Q" + str(m) + ")\n");

   for m in xrange(sys*nmodes, (sys+1)*nmodes-1):
      file.write( " " + str(lambda_coef) + " Q^2(Q" + str(m) + ") Q^1(Q" + str(m+1) + ")\n");
      file.write( str(-lambda_coef/3.0) + " Q^3(Q" + str(m+1) + ")\n");

# Write reference Hamiltonian
def write_reference_ho(file, nmodes, shift, harm, ke, sys):
   for m in xrange(sys*nmodes, (sys+1)*nmodes):
      if ke:
         file.write("-0.5 DDQ^2(Q" + str(m) + ")\n");
      file.write(" " + str(harm) + " Q^2(Q" + str(m) + ")\n");
      if shift > 1.e-16:
         file.write(str(-shift) + " Q^1(Q" + str(m) + ")\n");

# Write property
def write_prop(file, nmodes, oper, sys):
   for m in xrange(sys*nmodes, (sys+1)*nmodes):
      file.write(" 1.0 " + oper + "(Q" + str(m) + ")\n");


# main function
def main():
   parser = optparse.OptionParser();
   parser.add_option("-f",dest="filename",type="string",help="specify output filename", default="h");
   parser.add_option("-m",dest="nmodes",type="int",help="specify number of modes", default=2);
   parser.add_option("--lambda",dest="lambda_coef",type="float",help="specify lambda coefficient", default=0.111803);
   parser.add_option("--ref_shift", dest="ref_shift", type="float", help="shift factor for reference potential", default="0.0");
   parser.add_option("--ref_harm", dest="ref_harm", type="float", help="coef for Q^2 terms for reference potential", default="0.5");
   parser.add_option("--ke_terms",dest="ke_terms", help="Add kinetic-energy terms", default=False);
   parser.add_option("--duplicate",dest="duplicate",type="int",help="Make duplicate (uncoupled) subsystems", default=1);

   (options,args) = parser.parse_args();
   filename=options.filename;
   nmodes=options.nmodes;
   lambda_coef=options.lambda_coef;
   ref_shift=options.ref_shift;
   ref_harm=options.ref_harm;
   ke=options.ke_terms;
   duplicate=options.duplicate;

   # Henon-Heiles
   hh_file = open(filename + "_henon_heiles.mop", 'w');
   make_header(hh_file);
   for sys in xrange(0,duplicate):
      write_henon_heiles(hh_file, nmodes, lambda_coef, ke, sys);
   make_footer(hh_file, nmodes*duplicate);
   hh_file.close();

   # Reference potential
   ref_file = open(filename + "_ref.mop", 'w');
   make_header(ref_file);
   for sys in xrange(0,duplicate):
      write_reference_ho(ref_file, nmodes, ref_shift, ref_harm, ke, sys);
   make_footer(ref_file, nmodes*duplicate);
   ref_file.close();

   # Properties
   q_file = open(filename + "_q.mop", 'w');
   make_header(q_file);
   for sys in xrange(0,duplicate):
      write_prop(q_file, nmodes, "Q^1", sys);
   make_footer(q_file, nmodes*duplicate);
   q_file.close();

   ip_file = open(filename + "_ip.mop", 'w');
   make_header(ip_file);
   for sys in xrange(0,duplicate):
      write_prop(ip_file, nmodes, "DDQ^1", sys);
   make_footer(ip_file, nmodes*duplicate);
   ip_file.close();

#
if __name__ == "__main__":
   main();
