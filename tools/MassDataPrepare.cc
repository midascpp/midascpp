/*
 * 

MassDataPrepare.cc 

Take data from 

http://physics.nist.gov/PhysRefData/Compositions/index.html

in list format

physics.nist.gov.atmoic_data.lin

and convert into mass and weight file. 

* */
#include<iostream>
#include<fstream>
#include<sstream>
#include<iomanip>
#include<math.h>
#include<vector>
#include<map>
#include<algorithm>

int main()
{
   long double x1,x2;
   std::string s;
   std::cout.setf(std::ios::scientific);
   std::cout.setf(std::ios::showpoint);
   std::cout.setf(std::ios::uppercase);
   std::cout.precision(25);
   while (std::getline(std::cin, s))
   {
      if (s.find("Atomic Number")!=s.npos)
      {
         int i=0;
         int n=-1;
         int a=-1;
         long double m=-1.0e10;
         long double w=-1.0e10;
         do
         {
            s.erase(0,s.find("=")+1);
            if (s.find("(")!=s.npos) s.replace(s.find("("),1," ");
            //cout <<  " s = " << s << endl;
            std::istringstream sin(s);
            if (i==0) sin >> n;
            if (i==2) sin >> a;
            if (i==3) sin >> m;
            if (i==4) sin >> w;
            i++;
         } while (std::getline(std::cin, s)&& s.size()>0);
         std::cout << std::setw(4) << n << " " << std::setw(4) << a << " " << std::setw(30) << m << " " << std::setw(30) << w << " " << std::endl;
      }
      else
      {
         std::cout << " other line " << s << std::endl;
      }
   }

}


