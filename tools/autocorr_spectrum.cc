/**
************************************************************************
* 
* @file    autocorr_spectrum.cc
*
* @date    16-08-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Calculate spectrum from an auto-correlation function
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include<iostream>
#include<fstream>
#include<stdexcept>
#include<string>
#include<cassert>
#include<complex>
#include<tuple>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "util/conversions/VectorFromString.h"
#include "util/Io.h"
#include "util/Error.h"

#include "td/SpectrumCalculator.h"

//! Set up output file
std::ofstream outfile("autocorr_spectrum.mout");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

//! Alias
using param_t = std::complex<Nb>;
using real_t = Nb;
using spec_t = midas::td::SpectrumCalculator<param_t>;

/**
 * Main function
 *
 * Simple for now. Takes one argument which is the name of the file containing the auto-correlation function.
 * The input-file structure is assumed to be:   time    Re[S]    Im[S]    ... (the rest does not matter)
 **/
int main
   (  int argc
   ,  char* argv[]
   )
{
   // Check args
   if (  argc < 2
      )
   {
      MIDASERROR("Expected at least one argument: The name of the file containing the auto-correlation function with columns: 'time' 'Re[S]' 'Im[S]' ...");
   }

   // Read remaining args
   real_t shift = 0.0;
   real_t lifetime = -1.0;
   bool convolute = true;
   In pad = I_16;
   real_t screen = 0.0;
   std::pair<real_t, real_t> interval = {0.0, 0.0};
   bool normalize = false;
   bool absint = false;
   std::string output_prefix = "";
   int iarg = 2;
   while (  iarg < argc
         )
   {
      std::string s(argv[iarg]);

      if (  s == "--shift"
         )
      {
         shift = std::atof(argv[iarg+1]);
         ++iarg;
      }
      else if  (  s == "--lifetime"
               )
      {
         lifetime = std::atof(argv[iarg+1]);
         ++iarg;
      }
      else if  (  s == "--prefix"
               )
      {
         output_prefix = std::string(argv[iarg+1]);
         ++iarg;
      }
      else if  (  s == "--no-convolution"
               )
      {
         convolute = false;
      }
      else if  (  s == "--zeropad"
               )
      {
         pad = std::atoi(argv[iarg+1]);
         ++iarg;
      }
      else if  (  s == "--interval"
               )
      {
         interval.first = std::atof(argv[iarg+1]);
         interval.second = std::atof(argv[iarg+2]);
         iarg += 2;
      }
      else if  (  s == "--normalize"
               )
      {
         normalize = true;
      }
      else if  (  s == "--abs"
               )
      {
         absint = true;
      }
      else
      {
         MIDASERROR("Unrecognized argument: '" + std::string(argv[iarg]) + "'.");
      }
      ++iarg;
   }

   // Get input-file name
   std::string filename(argv[1]);

   // Check that input file exists
   if (  !InquireFile(filename)
      )
   {
      MIDASERROR("File '" + filename + "' not found!");
   }

   Mout  << "Reading auto-correlation function from file: '" << filename << "'." << std::endl;

   // Read in
   std::ifstream input(filename);
   std::string line;

   std::vector<param_t> ac_vec;
   std::vector<real_t> time_vec;
   ac_vec.reserve(10);
   time_vec.reserve(10);

   while (  std::getline(input, line)
         )
   {
      // Skip comments
      if (  line.at(line.find_first_not_of(' ')) == '#'
         )
      {
         continue;
      }

      // Allocate ac_vec and time_vec
      if (  ac_vec.size() == ac_vec.capacity()
         )
      {
         ac_vec.reserve(ac_vec.size()*10);
      }
      if (  time_vec.size() == time_vec.capacity()
         )
      {
         time_vec.reserve(time_vec.size()*10);
      }

      // Split line
      auto s_vec = midas::util::VectorFromString<real_t>(line);

      if (  s_vec.size() < 3
         )
      {
         MIDASERROR("Line in input file must contain at least 3 columns!");
      }

      // Set vector elements
      time_vec.emplace_back(s_vec[0]);
      ac_vec.emplace_back(s_vec[1], s_vec[2]);
   }

   // Output
   Mout  << "Read " << ac_vec.size() << " lines of input." << std::endl;

   // Get path
   size_t last_slash = filename.find_last_of("/") + 1;
   auto path_to_dir = filename.substr(0, last_slash);

   // Add prefix to filename (excluding path)
   auto output_filename = filename;
   output_filename.erase(output_filename.begin(), output_filename.begin() + last_slash);
   output_filename = path_to_dir + output_prefix + output_filename;

   // Remove file extension from filename
   size_t dot_pos = output_filename.find_last_of('.');
   output_filename.erase(output_filename.begin() + dot_pos, output_filename.end());

   Mout  << "Write to file with prefix: '" << output_filename << "'." << std::endl;

   // Calculate spectrum
   spec_t spec(convolute, pad, shift, screen, interval, normalize, absint, lifetime);

   spec.CalculateAndWriteSpectrum(output_filename, ac_vec, time_vec);

   return 0;
}
