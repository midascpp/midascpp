/**********************************************************
 *
 * Small waiter program to be called at end of system calls
 * when using MPI_Comm_spawn() instead of fork().
 *
 * Will interface with the function
 *
 *    spawn_process_mpi_comm_spawn(...)
 *
 * in MidasCpp.
 *
 **********************************************************/
#ifdef VAR_MPI
#include <mpi.h>
#endif /* VAR_MPI */
#include <sched.h>
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <memory>

/**
 * Do fork() + exec().
 *
 * For detailed comments see similar functionality in
 *
 *     tools/midascpp_fork_server.cc
 *
 **/
int fork_exec(char* argv[])
{
   int status;
   pid_t pid;

   if( (pid = fork()) < 0 )
   {
      status = -1;
   }
   else if(!pid) 
   {
      // make call
      if(execvp(argv[0], argv) == -1)
      {
         // error handling
         std::cout << " could not start process " << std::endl;
      }
   } 
   else 
   {
      // Loop over waitpid to wait for child
      while(waitpid(pid, &status, 0) < 0)
      {
         if(errno != EINTR)
         {
            status = -1;
            break;
         }
         
         sched_yield();
      }
   }

   return status;
}

/**
 * Main program.
 **/
int main (int argc, char *argv[])
{
   /****************************************************
    *
    * fork() + exec()
    *
    ****************************************************/
   char command_str[] = "--command";

   for(int i = 0; i < argc; ++i)
   {
      if(!strncmp(argv[i], command_str, strlen(command_str)))
      {
         /* Create argv */
         char* cptr = argv[i] + strlen(command_str) + 1;
         char* pch  = strchr(cptr, ' ');
         int count = strlen(cptr) != 0 ? 2 : 1;
         while (pch != NULL)
         {
            pch = strchr(pch + 1, ' ');
            ++count;
         }
         
         std::unique_ptr<char*[]> argv{new char*[count]};
         argv[0] = cptr;
         argv[count - 1] = nullptr;
         count = 1;
         pch  = strchr(cptr, ' ');
         while (pch != NULL)
         {
            *pch = '\0';
            argv[count] = pch + 1;
            pch = strchr(pch + 1, ' ');
            ++count;
         }

         /* Call fork() + exec() */
         fork_exec(argv.get());
      }
   }

#ifdef VAR_MPI
   /****************************************************
    *
    * MPI 
    *
    * Will initialize MPI and make a barrier with PARENT,
    * as PARENT is waiting on the barrier such that it does
    * not continue before the external program has finished.
    * Afterwards finalize MPI and exit.
    *
    ****************************************************/
   /* Init MPI */
   MPI_Init(&argc, &argv);

   /* Obtain an intercommunicator to the parent MPI job */
   MPI_Comm parent;
   MPI_Comm_get_parent(&parent);

   /* Check if this process is a spawned one and if so enter the barrier */
   if (parent != MPI_COMM_NULL)
   {
      MPI_Barrier(parent);
   }
   
   /* Finalize MPI and exit */
   MPI_Finalize();
#endif /* VAR_MPI */

   return 0;
}
