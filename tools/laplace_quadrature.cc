/**
************************************************************************
* 
* @file    laplace_quadrature.cc
*
* @date    15-11-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Generate tabulated points and weights for energy-denominator quadrature.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include<iostream>
#include<fstream>
#include<stdexcept>
#include<string>
#include<cassert>
#include<vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "tensor/LaplaceQuadrature.h"

ofstream outfile("laplace_quadrature.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

int main
   (  int argc
   ,  char* argv[]
   )
{
   // Get xmin, xmax, numpoints, and (optionally) gamma.
   if (  argc < 4
      || argc > 5
      )
   {
      MIDASERROR("Provide arguments: xmin, xmax, numpoints, and (optionally) gamma!");
   }

   Nb xmin = static_cast<Nb>(atof(argv[1]));
   Nb xmax = static_cast<Nb>(atof(argv[2]));

   In npoints = static_cast<In>(atoi(argv[3]));

   Nb gamma = C_0;
   if (  argc == 5
      )
   {
      gamma = static_cast<Nb>(atof(argv[4]));
   }

   // Construct LaplaceInfo
   midas::tensor::LaplaceInfo lapinfo("FIXLAP", 1.e-5);
   lapinfo["NUMPOINTS"] = npoints;
   lapinfo["COMPLEXSHIFT"] = gamma;
   lapinfo["IOLEVEL"] = I_20;

   // Construct LaplaceQuadrature
   LaplaceQuadrature<Nb> quad(lapinfo, xmin, xmax);

   quad.MakePlot("laplace_data.dat", 1.e-6);

   Mout  << quad << std::endl;
   std::cout << quad << std::endl;

   return 0;
}
