/**
************************************************************************
*  
* @file                comparevibs.cc
*
* Created:             03-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   compares two molecular info files
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*           Compares two molecule.mol files in various ways, needs to be called as
*
*           comparevibs.x ref.mol mol2.mol
*
*           The first molecule file is assumed to be the reference to which mol2 is compared. 
*           E.g., the largest overlap of each coordinate in mol2 to any coordinats in the ref 
*           is calculated. This also works if ref.mol contains a larger set of coordinates. 
*
*           The ``similarity measure'' is only meaningful for the full basis 
*
*           Additionally the correlation matrices are printed with increasing frequencies 
*           of the reference modes (in the rows)
*
*           The columns in the first printed matrix are also ordered with increasing frequency.
*           Quadratic matrices are additionally printed with ordering of the columns according 
*           to maximal overlap reference modes, meaning the diagonal containing the largest contributions. 
*
*           IMPORTANT: this tool assumes the atoms are provided in the same order, if this is not the case, 
*           you will get nonsense results! 
*
*           FINALLY: good luck!
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<ostream>
#include<string>

// midas headers
#include "util/Io.h"
#include "mpi/Impi.h"
#include "inc_gen/Warnings.h"
#include "input/ProgramSettings.h"
#include "util/Os.h"
#include "nuclei/AtomicData.h"
#include "pes/molecule/MoleculeInfo.h"

using namespace midas::molecule;

void NormAnalysis(const MoleculeInfo& aMolecule); 
extern AtomicData gAtomicData;

ofstream outfile("Compare.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

using namespace std;

int main(int argc, char* argv[])
{
   midas::mpi::Initialize(&argc, &argv);
   gAtomicData.Init(); 

   // Niels: Set scratch dir which is used for converting relative to absolute paths.
   midas::input::gProgramSettings.SetScratch(midas::os::Getcwd());

   Mout << " Comparing  Vibrations in " << string(argv[1]) << "  and " << string(argv[2])<< endl;
   Mout << " Assuming same molecule and same order of atoms " << endl;
   Mout << endl;
   Mout << endl;
   Mout << " Start reading in .mol files " << endl;
   MoleculeInfo mol_info1(MoleculeFileInfo(string(argv[1]),"MIDAS"));
   MoleculeInfo mol_info2(MoleculeFileInfo(string(argv[2]),"MIDAS"));
   Mout << " Ready reading in .mol files " << endl;;
   Mout << endl;
   Mout << endl;

   NormAnalysis(mol_info1);
   NormAnalysis(mol_info2);

   cout << " number of nuclei " << mol_info1.GetNumberOfNuclei() << endl;

   if (mol_info1.GetNumberOfNuclei() != mol_info2.GetNumberOfNuclei())
   {
      Mout << " WARNING: Number of nuclei does not fit"   << "\n";
   }
   else 
   {
      Mout << " Number of nuclei fit " << "\n";
   }
   int n_nuclei=mol_info1.GetNumberOfNuclei();
 
   if (mol_info1.GetNoOfVibs() != mol_info2.GetNoOfVibs())
   {
      Mout << " Number of modes does not fit -- quit here " << "\n";
   }
   else 
   {
      Mout << " Number of modes fit " << "\n";
   }
   int n_modes1=mol_info1.GetNoOfVibs();
   int n_modes2=mol_info2.GetNoOfVibs();

   Mout << " Number of modes : " << n_modes1 << endl; 
   Mout << " Number of modes : " << n_modes2 << endl; 


   // sorting according to frequencies  
   multimap<Nb,In> freqmap1;
   multimap<Nb,In> freqmap2;
   for (int i = I_0 ; i < n_modes1 ; ++i)
   {
      freqmap1.emplace(mol_info1.GetFreqI(i),i);
   }
   for (int i = I_0 ; i < n_modes2 ; ++i)
   {
      freqmap2.emplace(mol_info2.GetFreqI(i),i);
   }
 
   MidasVector vec_1(3*n_nuclei);
   MidasVector vec_2(3*n_nuclei);
   MidasMatrix mat(n_modes2,n_modes1,-C_1);

   // calculate all the matrix elements ordered according to frequencies 
   // and figure out quite some info for latter mapping, possible double 

   //info used for the mapping to the reference 
   vector<multimap<Nb,In>> dot_vec2;
   dot_vec2.reserve(n_modes2);
     
   //info used for the mapping the largest overlap 
   multimap<Nb,std::vector<In>> mmap;
   // the measure to capture similarity in one number, only meaningful in case of full basis 
   long double measure=0.0e0;
   int i=I_0;
   std::vector<In> pair(2,-I_1);
   for (multimap<Nb,In>::const_iterator it2=freqmap2.begin() ; it2!=freqmap2.end() ; ++it2)
   {
      pair.at(0)=i;
      int j=I_0;
      multimap<Nb,In> mode_map;
      dot_vec2.emplace_back(mode_map);
      mol_info2.GetNormalMode(vec_2,it2->second);
      //mass-weighting
      for (int k =I_0 ; k < vec_2.size(); ++k)
      {
         vec_2[k]*=sqrt(mol_info2.GetNuclMassi(k/3));
      }
   
      for (multimap<Nb,In>::const_iterator it1=freqmap1.begin() ; it1!=freqmap1.end() ; ++it1)
      {
         pair.at(1)=j;
         //cout << " in the inner loop  " << endl;
         mol_info1.GetNormalMode(vec_1,it1->second);
         //mass-weighting
         for (int k =I_0 ; k < vec_1.size(); ++k)
         {
            vec_1[k]*=sqrt(mol_info1.GetNuclMassi(k/3));
         }
         //calculating dot product
         Nb dot=fabs(Dot(vec_1,vec_2));
         //and setting up all the mapping data
         dot_vec2.at(i).emplace(dot,j);
         mmap.emplace(dot,pair);
         //and the matrix
         mat[i][j++]=dot;
         //and the measure 
         measure+=dot;  
      }
      ++i;
   }

   if (n_modes1==n_modes2)
   {
      measure/=n_modes1;
      Mout << " Measure : " << measure << endl;
      Mout << "This measure is rather meaning less if the two sets of modes do not span the same space" << endl;
   }


   Nb rmsd = C_0;
   Nb max_diff = C_0;
   Nb min_diff = C_NB_MAX;
   // print mapping of modes
   Mout << endl;
   Mout << " Mapping of modes:" << endl;
   Mout << " Here it is listed for the modes in the mol2.mol file, i.e., the second molfile" << endl;
   Mout << " overlap with the reference modes:" << endl;
   Mout << endl;
   for (int i=0 ; i < dot_vec2.size();++i)
   {
      auto freq2_it=freqmap2.begin();
      std::advance(freq2_it,i);
      Mout << " Mode " << i   << " with freq= " << freq2_it->first << endl;
      multimap<Nb,In>::reverse_iterator it=dot_vec2.at(i).rbegin();
      multimap<Nb,In>::reverse_iterator it_end=dot_vec2.at(i).rbegin();
      int end=5;
      end=min(end,n_modes2);
      std::advance(it_end,end);
      while (it != it_end)
      {
         auto freq1_it=freqmap1.begin();
         std::advance(freq1_it,it->second);
         if (it==dot_vec2.at(i).rbegin()) 
         {
            Nb diff = fabs(freq1_it->first-freq2_it->first);
            rmsd += diff*diff;
            max_diff = max(max_diff,diff);
            min_diff = min(min_diff,diff);
         }
         Mout << it->second << " : " << it->first << " freq= " << freq1_it->first << endl;
         ++it;
      }
      Mout << endl;
   }


   Mout << " Root mean square deviation : " << sqrt(rmsd/n_modes2) << endl;
   Mout << " Maximal deviation : " << max_diff << endl;
   Mout << " Minimal deviation : " << min_diff << endl;

   Mout << endl;
   Mout << endl;
   Mout << " Correlation matrix with increasing frequencies " << endl;
   for (int i=0 ; i < mat.Ncols() ; ++i)
   {
      for (int j=0 ; j < mat.Nrows() ; ++j)
         Mout << " " << mat[j][i] << " " ;
      Mout << endl;
   }
   Mout << endl;
   Mout << endl;
 
   if (n_modes1==n_modes2) 
   {
      Mout << " Correlation matrix with increasing frequencies in file 1 and largest overlap to that in file 2" << endl;
      std::vector<In> map_for_mat(n_modes1,-I_1);
      map_for_mat.reserve(n_modes1);
      std::set<In> block_1;
      std::set<In> block_2;
      auto mit = mmap.rbegin();
      while (block_1.size() < n_modes1 || block_2.size() < n_modes2 )
      {
         //cout << mit->first << " " << mit->second.at(0) << " " << mit->second.at(1) << endl; 
         if (block_1.find(mit->second.at(1))==block_1.end() && block_2.find(mit->second.at(0))==block_2.end())
         {
            //cout << " adding " << mit->second.at(1) << " " << mit->second.at(0) << endl; 
            map_for_mat.at(mit->second.at(1))=mit->second.at(0);
            auto empl = block_1.emplace(mit->second.at(1));
            if (!empl.second) MIDASERROR(" Tried to map the same mode twice");
            empl = block_2.emplace(mit->second.at(0));
            if (!empl.second) MIDASERROR(" Tried to map the same mode twice");
         }
         ++mit;
      }
      for (int i=0 ; i < mat.Ncols() ; ++i)
      {
         for (int j=0 ; j < mat.Nrows() ; ++j)
            Mout << " " << mat[map_for_mat.at(j)][i]<< " " ;
         Mout << endl;
      }
   }
   else 
      Mout << " Not printed reordered matrix since number of modes differ " << endl;
   Mout << endl;
   Mout << endl;
   Mout << " END" << endl;
   
   return 0;
}
/**
* Check normal coordinates in MolInfo. Are they orthorgonal ?
*
* @param aMolecule
* */
void NormAnalysis(const MoleculeInfo& aMolecule) 
{
   int non=aMolecule.GetNumberOfNuclei(); 
   int nm=aMolecule.GetNoOfVibs(); 
  
   cout << " number of vibs " << nm << endl;
 
   Nb  eps=C_I_10_6;
   midas::stream::ScopedPrecision(18, Mout);

   MidasVector vec1(3*non);
   MidasVector vec2(3*non);

   for (In i=I_0;i<nm;i++)
   {
      vec1.Zero();
      aMolecule.GetNormalMode(vec1,i);
      for (In j=i;j<nm;j++)
      {
         vec2.Zero();
         aMolecule.GetNormalMode(vec2,j);
         Nb dot_prod=C_0;
         for (In k=I_0;k<3*non;k++)
            dot_prod+=vec1[k]*aMolecule.GetNuclMassi(k/I_3)*vec2[k];
  
         //Mout << " Dot product " << i << " " << j << " " << dot_prod << endl;
         if ( (i==j) && (fabs(dot_prod-C_1)>eps) )
         {
            Mout << " Warning, diagonal dot product is " << dot_prod << " Something might be wrong with displ. coordinates for vector "
                 << i << endl;
         }
         else if ( (i!=j) && (fabs(dot_prod-C_0)>eps) )
         {
             Mout << " Warning, off-diagonal dot product is " << dot_prod << " Something might be wrong with displ.coordinates for vector pair "
                  << i << "," << j << endl;
         }
      }
   }
}

