/**
************************************************************************
* 
* @file    tensordatacont_to_datacont.cc
*
* @date    19-09-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Convert TensorDataCont to DataCont
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include<iostream>
#include<fstream>
#include<stdexcept>
#include<string>
#include<cassert>
#include<vector>

#include "input/Input.h"
#include "vcc/TensorDataCont.h"
#include "mmv/DataCont.h"

std::ofstream outfile("tdc_to_dc.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

int main(int argc, char* argv[])
{
   if (  argc != 2
      )
   {
      throw std::runtime_error("Please provide one argument!");
   }

   auto name = std::string(argv[1]);

   auto out_name = name + "_converted_to_datacont";

   outfile << " Read TensorDataCont from file: " << name << std::endl;
   TensorDataCont tdc;
   tdc.ReadFromDisc(name);

   outfile << " TensorDataCont Norm = " << tdc.Norm() << std::endl;

   {
      outfile << " Converting to DataCont" << std::endl;
      gMaxFileSize = I_10_7;
      outfile << " gMaxFileSize = " << gMaxFileSize << std::endl;
      auto out = DataContFromTensorDataCont(tdc);
      out.NewLabel(out_name, false);
      out.SaveUponDecon(true);

      outfile << " DataCont Norm = " << out.Norm() << std::endl;
   }

   outfile << " DataCont saved as: " << out_name << std::endl;

   return 0;
}
