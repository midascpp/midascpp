#!/bin/bash
#
total_tabs=0
for x in `ls -p | grep '\/'`; do
   command="ls "$x"*.[ch]*"
   for y in `$command`; do
      if [ `awk '/\t/' $y | wc -l` -gt 0 ]; then
         tabs=`awk '/\t/' $y | wc -l`
         echo "$y:    $tabs"
         total_tabs=`echo "$total_tabs + $tabs" | bc`
         sed -e 's/\t/        /g' $y > TABBED_FILE_UNTABBED.TMP
         mv TABBED_FILE_UNTABBED.TMP $y
      fi
   done
done
#
cat << %EOF%
##################################################

   A TOTAL OF: $total_tabs TAB CHARACTERS WERE FOUND!

##################################################
%EOF%
