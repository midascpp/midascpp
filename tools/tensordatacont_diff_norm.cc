/**
************************************************************************
* 
* @file    tensordatacont_diff_norm.cc
*
* @date    20-06-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Calculate diff norm of two TensorDataCont%s
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include<iostream>
#include<fstream>
#include<stdexcept>
#include<string>
#include<cassert>
#include<vector>

#include "vcc/TensorDataCont.h"
#include "tensor/NiceTensor.h"
#include "tensor/BaseTensor.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "tensor/NiceTensor.h"
#include "mmv/Transformer.h"
#include "vcc/TransformerV3.h"
#include "vcc/TensorDataCont.h"
#include "tensor/TensorDecompInfo.h"

ofstream outfile("tdc_diff_norm.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

int main(int argc, char* argv[])
{
   if(argc != 3) throw std::runtime_error("Please provide two arguments!");

   TensorDataCont first;
   first.ReadFromDisc(std::string(argv[1]));

   TensorDataCont second;
   second.ReadFromDisc(std::string(argv[2]));

   Nb safe_result = C_0;
   Nb result = C_0;

   auto size = first.Size();
   assert(size == second.Size());

   Nb max_result = C_0;

   for(In i=I_0; i<size; ++i)
   {
      const auto* const first_tens = first.GetModeCombiData(i).GetTensor();
      const auto* const second_tens = second.GetModeCombiData(i).GetTensor();
      assert_same_shape(*first_tens, *second_tens);

      auto safe_dnorm2 = safe_diff_norm2(first_tens, second_tens);
      auto dnorm2 = diff_norm2_new(first_tens, first_tens->Norm2(), second_tens, second_tens->Norm2());

      safe_result += safe_dnorm2;
      result += dnorm2;

      max_result = std::max(max_result, safe_dnorm2);
   }

   std::cout   << std::setprecision(15)
               << " Diff norm:      " << std::sqrt(result) << "\n"
               << " Safe diff norm: " << std::sqrt(safe_result) << "\n"
               << " Max. diff norm: " << std::sqrt(max_result) << "\n"
               << std::flush;
   outfile << std::setprecision(15) << std::sqrt(safe_result) << std::endl;

   return 0;
}
