/**
************************************************************************
*  
* @file                der_binary_ascii.cc
*
* Created:             09-11-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   new binary format to ascii
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <cstdlib>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "nuclei/AtomicData.h"
#include "mpi/Interface.h"

#ifdef VAR_MPI
#include "mpi/Finalizer.h"
midas::mpi::Finalizer& mpi_finalize = midas::mpi::get_Finalizer(); // get finalizer object that will call
                                                  // mpi finalize on destruction
#endif /* VAR_MPI */


using namespace std;

bool IssueWarning();
void MakeTarArchive();
bool CheckBinary(vector<string>&);
bool CheckAscii(vector<string>&);
void BinaryToAscii(const vector<string>&,MidasMatrix&);
void AsciiToBinary(const vector<string>&,MidasMatrix&);
bool GetNormalCoordinates(const string&,MidasMatrix&);

static In POINT_WITH_DER_FOUND = 0;
static In HESSIAN_FOUND = false;

vector<Nb> mass_vec;

ofstream outfile("ConverterFile.out");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

int main(int argc, char* argv[])
{
#ifdef VAR_MPI
   int a;
   char **c;
   MPI_Init(&a,&c);
#endif
   gMaxFileSize=I_10_8;
   if(midas::mpi::get_Interface().GetMpiGlobalRank()==0) {
      if (argc != 2) {
         cout << "ONE COMMAND LINE ARGUMENT: The normal coordinate file." << endl;
         exit(10);
      }
   
      MidasMatrix norm(I_1);
   
      string file_name=string(argv[1]);
   
      if(!GetNormalCoordinates(file_name,norm)) {
         cout << "I did not manage to retrieve normal coordinates..." << endl;
         exit(10);
      }
   
      if(!IssueWarning())
      {
         exit(10);
      }
      
      // tar files for safety
      MakeTarArchive();
      // check for binary derivatives files
      vector<string> file_names;
      bool binary=false;
      bool ascii=false;
      if(!CheckBinary(file_names)) {
         if(file_names.size()!=0) {
            cout << "I found some binary files, but not all?" << endl;
            return 0;
         }
         binary=false;
      }
      else
         binary=true;
      // only check for ascii if !binary
      if(!binary) {
         if(!CheckAscii(file_names)) {
            if(file_names.size()!=0) {
               cout << "I found some binary files, but not all?" << endl;
               return 0;
            }
            ascii=false;
         }
         else
            ascii=true;
      }
      // all ok, except derivatives if they are here!
      if(binary)
         BinaryToAscii(file_names,norm);
      else if(ascii)
         AsciiToBinary(file_names,norm);
   }
#ifdef VAR_MPI
   //MPI_Barrier(MPI_COMM_WORLD);
   //MPI_Finalize();
#endif
   return 1;
}

bool GetNormalCoordinates(const string& file,MidasMatrix& mat)
{
   ifstream ifs(file.c_str());
   if(!ifs.is_open())
      return false;
   // search for FREQ
   string line;
   In n_atom=I_0;
   string units="au";
   getline(ifs,line);
   istringstream iss_info(line);
   iss_info >> n_atom >> units;
   mass_vec.clear();
   cout.setf(ios::scientific);
   cout.precision(16);
   AtomicData data;
   data.Init();
   gAtomicData.Init();
   for(In i=0;i<n_atom;i++) {
      getline(ifs,line);
      istringstream iss(line);
      string label;
      iss >> label;
      Nuclei nuc(C_0,C_0,C_0,C_0,label);
      nuc.SetQfromGeneralLabel(label);
      In charge=nuc.Znuc();
      In comm=data.MostCommonIsotope(charge);
      cout << "Common: " << comm << " MASS: " << data.GetMass(charge,comm) << endl;
      mass_vec.push_back(data.GetMass(charge,comm));
   }
   bool all_done=false;
   while(getline(ifs,line) && !all_done) {
      istringstream iss(line);
      string pattern;
      iss >> pattern;
      if(pattern=="FREQ")
         all_done=true;
   }
   if(!all_done)
      return false;
   // now count number of frequencies
   In n_freq=I_0;
   all_done=false;
   while(getline(ifs,line) && !all_done) {
      istringstream iss(line);
      string pattern;
      iss >> pattern;
      if(pattern=="COORD")
         all_done=true;
      n_freq++;
   }
   if(!all_done)
      return false;
   n_freq-=2;
   cout << "There are " << n_atom << " atoms and " << n_freq
        << " frequencies, is that ok?[yes/no] ";
   string answer;
   cin >> answer;
   if(answer!="yes")
      return false;
   mat.SetNewSize(3*n_atom,n_freq);
   // now coord begin!
   for(In i=0;i<n_freq;i++) {
      for(In j=0;j<n_atom;j++) {
         if(!getline(ifs,line))
            return false;
         if(line.size()==0) {
            j--;
            continue;
         }
         istringstream iss(line);
         if(! (iss >> mat[3*j][i] >> mat[3*j+1][i] >> mat[3*j+2][i]) )
            return false;
      }
   }
   bool normalize=false;
   for(In i=0;i<n_freq;i++) {
      MidasVector vec(3*n_atom,C_0);
      mat.GetCol(vec,i);
      if(fabs(sqrt(Dot(vec,vec))-C_1)>1.0e-4)
      {
         normalize=true;
      }
      mat.AssignCol(vec,i);
   }
   if(normalize) {
      for(In i=0;i<n_freq;i++) {
         MidasVector vec(3*n_atom,C_0);
         mat.GetCol(vec,i);
         for(In j=0;j<3*n_atom;j++)
            vec[j]*=sqrt(mass_vec[j/3]);
         vec.Scale(C_1/sqrt(Dot(vec,vec)));
         mat.AssignCol(vec,i);
      }
   }
   //cout << "After norm: " << endl << (Transpose(mat))*mat << endl;
   // at end of day, normal coordinates are
   cout.setf(ios::scientific);
   cout.precision(12);
   for(In i=0;i<n_freq;i++) {
      cout << "NC: " << i << ":" << endl;
      for(In j=0;j<3*n_atom;j++)
         cout << mat[j][i] << "  ";
      cout << endl;
   }
   return true;
}

bool IssueWarning()
{
   string answer;
   cout << "This will convert all properties into binary files." << endl;
   cout << "However, a tar archive will be made before proceeding." << endl << endl;
   cout << "!!!DO NOT!!! interrupt program while running" << endl << endl;
   cout << "Will you continue? [yes/no]: ";
   cin >> answer;
   if(answer=="yes" || answer=="YES")
      return true;
   return false;
}

void MakeTarArchive()
{
   struct stat stFileInfo; 
   int intStat; 

   // Attempt to get the file attributes  
   In i=0;
   string file_name;
   bool all_done=false;
   while(!all_done) {
      file_name="SavedDerArchive_"+std::to_string(i)+".tar.gz";
      intStat = stat(file_name.c_str(),&stFileInfo); 
      if(intStat == -1) { 
         // file does not exist, we continue
         all_done = true;
      }
      else
         cout << "File: " << file_name << " exists, checking next." << endl;
      i++;
   }

   // put all *_prop.out, *_der.out, Lis*, prop_no* in the archive
   string call="tar zcf "+file_name+" *derivatives*";
   cout << "VERY IMPORTANT!!! DO NOT INTERRUPT BEFORE I RETURN" << endl;
   cout << "\"Archive created successfully\"" << endl << endl;
   int sys_ret = system(call.c_str());
   cout << "\t\t\tArhive created successfully" << endl;
}

bool CheckBinary(vector<string>& aV)
{
   // loop through midasifc.prop_info file and add items
   ifstream ifs("midasifc.propinfo");
   if(!ifs.is_open()) {
      cout << "I did not find midasifc.propinfo file, make sure" << endl
           << "that you are in a save directory!" << endl;
      ifs.close();
      return false;
   }
   // loop through lines and find derivative-file names
   string line;
   In i_prop=I_0;
   while(getline(ifs,line)) {
      i_prop++;
      string s=line;
      transform(s.begin(),s.end(),s.begin(),(int(*) (int))toupper);
      if(s.find("DER_FILE")==s.npos)
         continue;
      // i_prop has derivatives
      cout << "I think property: " << i_prop << " has derivatives!" << endl;
      std::string filename = "prop_no_" + std::to_string(i_prop) + "_derivatives.mbinary_0";
      ifstream ifs_check(filename.c_str());
      if(!ifs_check.is_open()) {
         cout << "I did not find " << filename << " maybe not binary?" << endl;
         ifs_check.close();
         return false;
      }
      ifs_check.close();
      // binary file exists => add to list!
      filename="prop_no_"+std::to_string(i_prop);
      aV.push_back(filename);
   }
   if(aV.size()>I_0)
      return true;
   return false;
}

bool CheckAscii(vector<string>& aV)
{
   // loop through midasifc.prop_info file and add items
   ifstream ifs("midasifc.propinfo");
   if(!ifs.is_open()) {
      cout << "I did not find midasifc.propinfo file, make sure" << endl
           << "that you are in a save directory!" << endl;
      ifs.close();
      return false;
   }
   // loop through lines and find derivative-file names
   string line;
   In i_prop=I_0;
   while(getline(ifs,line)) {
      i_prop++;
      string s=line;
      transform(s.begin(),s.end(),s.begin(),(int(*) (int))toupper);
      if(s.find("DER_FILE")==s.npos)
         continue;
      // i_prop has derivatives
      cout << "I think property: " << i_prop << " has derivatives!" << endl;
      std::string filename = "prop_no_" + std::to_string(i_prop) + "_derivatives.ascii";
      ifstream ifs_check(filename.c_str());
      if(!ifs_check.is_open()) {
         cout << "I did not find " << filename << " maybe not ascii?" << endl;
         ifs_check.close();
         return false;
      }
      ifs_check.close();
      // binary file exists => add to list!
      filename="prop_no_"+std::to_string(i_prop);
      aV.push_back(filename);
   }
   if(aV.size()>I_0)
      return true;
   return false;
}

void BinaryToAscii(const vector<string>& aV,MidasMatrix& mat)
{
   cout << "Converting to ascii..." << endl;
   MidasMatrix mat_t=Transpose(mat);
   // address file:
   for(In i=0;i<aV.size();i++) {
      std::string filename = aV[i] + "_derivatives_navigation.mpesinfo";
      ifstream addr_ifs(filename.c_str());
      string line;
      In order=I_1;
      In n_points=I_0;
      while(getline(addr_ifs,line)) {
         n_points++;
         istringstream iss(line);
         string junk;
         In addr1;
         In addr2=I_0;
         iss >> junk >> addr1;
         if(iss >> addr2)
            order=I_2;
      }
      addr_ifs.close();
      // we have the number of points and derivative order
      // => transform!
      In n_q=mat.Ncols();
      In n_c=mat.Nrows();
      In n_derivs=n_points*n_q;
      if(order==I_2)
         n_derivs+=n_points*n_q*n_q;
      DataCont derivs;
      derivs.SaveUponDecon(true);
      filename = aV[i] + "_derivatives.mbinary";
      derivs.GetFromExistingOnDisc(n_derivs, filename);
      derivs.NewLabel(filename);
      In der_per_point=n_q;
      if(order==2)
         der_per_point+=n_q*n_q;
      filename = aV[i] + "_derivatives.ascii";
      ofstream ofs(filename.c_str());
      ofs.setf(ios::scientific);
      ofs.precision(22);
      for(In j=0;j<n_points;j++) {
         cout << "Doing point " << j << endl;
         // 1) get first and second derivatives
         In addr1=j*der_per_point;
         In addr2=j*der_per_point+n_q;
         MidasVector first_q(n_q,C_0);
         derivs.DataIo(IO_GET,first_q,n_q,addr1);
         // 2) transform
         MidasVector first_c=mat*first_q;
         // 3) print vector, one number on each line
         for(In k=0;k<n_c;k++)
            ofs << first_c[k] << endl;
         // 4) if second der, do same
         if(order==2) {
            MidasMatrix second_q(n_q,C_0);
            derivs.DataIo(IO_GET,second_q,n_q*n_q,n_q,n_q,addr2);
            MidasMatrix second_c=mat*(second_q*mat_t);
            for(In k=0;k<n_c;k++)
               for(In l=0;l<n_c;l++)
            ofs << second_c[k][l] << endl;
         }
      }
      cout << "Now closing: " << filename << endl;
      ofs.close();
      filename = aV[i] + "_derivatives.mbinary_*[0-9]";
      RmFile(filename);
   }
}

void AsciiToBinary(const vector<string>& aV,MidasMatrix& mat)
{
   cout << "Converting to binary.." << endl;
   MidasMatrix mat_t=Transpose(mat);
   // address file:
   for(In i=0;i<aV.size();i++) 
   {
      std::string filename = aV[i] + "_derivatives_navigation.mpesinfo";
      ifstream addr_ifs(filename.c_str());
      string line;
      In order=I_1;
      In n_points=I_0;
      while(getline(addr_ifs,line)) {
         n_points++;
         istringstream iss(line);
         string junk;
         In addr1;
         In addr2=I_0;
         iss >> junk >> addr1;
         if(iss >> addr2)
            order=I_2;
      }
      addr_ifs.close();
      // we have the number of points and derivative order
      // => transform!
      In n_q=mat.Ncols();
      In n_c=mat.Nrows();
      In n_derivs=n_points*n_q;
      if(order==I_2)
         n_derivs+=n_points*n_q*n_q;
      DataCont derivs;
      derivs.SaveUponDecon(true);
      derivs.SetNewSize(n_derivs,false);
      filename = aV[i] + "_derivatives.mbinary";
      In der_per_point=n_q;
      if(order==2)
         der_per_point+=n_q*n_q;
      filename = aV[i] + "_derivatives.ascii";
      ifstream ifs(filename.c_str());
      for(In i=0;i<n_points;i++) {
         // 1) get first and second derivatives
         In addr1=i*der_per_point;
         In addr2=i*der_per_point+n_q;
         MidasVector first_c(n_c,C_0);
         for(In j=0;j<n_c;j++)
            ifs >> first_c[j];
         // 2) transform
         MidasVector first_q=mat_t*first_c;
         derivs.DataIo(IO_PUT,first_q,n_q,addr1);
         // 4) if second der, do same
         if(order==2) {
            MidasMatrix second_c(n_c,C_0);
            for(In j=0;j<n_c;j++)
               for(In k=0;k<n_c;k++)
                  ifs >> second_c[j][k];
            MidasMatrix second_q=mat_t*(second_c*mat);
            derivs.DataIo(IO_PUT,second_q,n_q*n_q,n_q,n_q,addr2);
         }
      }
      ifs.close();
      RmFile(filename);
   }
}
