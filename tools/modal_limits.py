#!/usr/bin/env python3

import argparse as ap
import numpy as np

def main():
   parser = ap.ArgumentParser(description="Get number of modals for each mode with energy less than input value.");
   parser.add_argument("-f", "--file", type=str, help="Name of binary file containing modal energies, e.g. <vscf-name>_EigVal_0", required=True);
   parser.add_argument("-e", "--max-energy", type=float, help="Energy cutoff. Include modals with energy less than this (relative to lowest energy for mode)", required=True);
   parser.add_argument("-a", "--add", type=int, help="Add a constant to all limits", default=0, required=False);
   args = parser.parse_args();
   max_energy = args.max_energy;
   f = args.file;
   add = args.add;

   # Get data
   data = np.fromfile(f, dtype=np.float64);

   # Get indices of zeros and split
   zeros = np.where(data == 0.0)[0].tolist();
   zeros.pop(0);
   modal_energies = np.split(data, zeros);

   # Debug
   #print(*data.tolist(), sep="\n");

   nmodes = len(modal_energies);
   limits = [];
   for m in range(0,nmodes):
      evec = modal_energies[m];
      min_energy = np.amin(evec);
      limits.append(np.count_nonzero(evec < (max_energy + min_energy)));

   # Print limits
   print(*[x+add for x in limits], sep=' ');




if __name__ == "__main__":
   main();
