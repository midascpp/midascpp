#!/usr/bin/python2

#################################################################################
#
# Generate code Special SimpleTensor Direct products used in VCC Transformer code
#
#################################################################################

import optparse
import copy

#
# make header for source file
#
def make_header(file):
   file.write("#include \"DirProdSpecial.h\"\n")
   file.write("\n")
   file.write("/**\n")
   file.write(" * @function ModesStrictlyLessThan     Check whether all modes in one mc (aMc1) are less than the modes of another (aMc2).\n")
   file.write(" *                                     Used to check whether we can do a specially optimized direct product.\n")
   file.write(" * @param aMc1                         ModeCombi to check.\n")
   file.write(" * @param aMc2                         ModeCombi to check against.\n")
   file.write(" * @return                             True if all modes in aMc1 are less than all modes in aMc2. \n")
   file.write(" **/\n")
   file.write("bool ModesStrictlyLessThan\n")
   file.write("   ( const ModeCombi& aMc1\n")
   file.write("   , const ModeCombi& aMc2\n")
   file.write("   )\n")
   file.write("{\n")
   file.write("   for(int i = 0; i < aMc1.Size(); ++i)\n")
   file.write("   {\n")
   file.write("      for(int j = 0; j < aMc2.Size(); ++j)\n")
   file.write("      {\n")
   file.write("         if(aMc1.Mode(i) > aMc2.Mode(j))\n")
   file.write("         {\n")
   file.write("            return false;\n")
   file.write("         }\n")
   file.write("      }\n")
   file.write("   }\n")
   file.write("   return true;\n")
   file.write("}\n\n")


#
# make direct product function for specific order
#
def make_dirprod_function_order(file, order):
   # documentation section
   file.write("/**\n")
   file.write(" * @function DirProdTensorSimpleSpecial" + str(order) + "  Special direct product for " + str(order) + " SimpleTensor,\n")
   file.write(" *                                        where the MC for one is Strictly less than the other.\n")
   file.write(" * @param aRes                            Result of direct product.\n")
   
   for i in range(0, order):
      file.write(" * @param aInTensor" + str(i) + "                      Tensor" + str(i) + " to be direct product'ed together.\n")
   
   file.write(" * @param aCoef                           Coefficient to be multiplied to each element of result.\n")
   file.write(" **/\n")

   # function declaration
   file.write("void DirProdTensorSimpleSpecial" + str(order) + "\n")
   file.write("   ( NiceTensor<Nb>& aRes\n")
   
   for i in range(0, order):
      file.write("   , const NiceTensor<Nb>& aInTensor" + str(i) + "\n")
   
   file.write("   , const Nb aCoef\n")
   file.write("   )\n")

   # initialize data variables
   file.write("{\n")
   file.write("   Nb* result_ptr = static_cast<SimpleTensor<Nb>* >(aRes.GetTensor())->GetData();\n")
   for i in range(0, order):
      file.write("   Nb* in_ptr" + str(i) + " = static_cast<SimpleTensor<Nb>* >(aInTensor" + str(i) + ".GetTensor())->GetData();\n")
   
   file.write("\n")

   for i in range(0, order):
      file.write("   auto sz" + str(i) + " = aInTensor" + str(i) + ".TotalSize();\n")
   
   file.write("   \n")

   for i in range(0, order):
      file.write("   Nb* in_ptr" + str(i) + "_inner;\n")

   file.write("   \n")

   # expand loops
   tab = "   "
   for i in range(0, order):
      file.write(tab + "in_ptr" +str(i) + "_inner = in_ptr" + str(i) + ";\n")
      file.write(tab + "for (unsigned int i" + str(i) + " = 0; i" + str(i) + " < sz" + str(i) + "; ++i" + str(i) + ")\n")
      file.write(tab + "{\n")
      tab += "   "

   #dir prod
   file.write(tab + "(*result_ptr) += aCoef") 
   for i in range(0, order):
      file.write("* (*in_ptr" + str(i) + "_inner)")
   file.write(";\n")
   file.write(tab + "++result_ptr;\n")
   
   # de-expand loops
   for i in reversed(range(0, order)):
      file.write(tab + "++in_ptr" + str(i) + "_inner;\n")
      tab = tab[:-3]
      file.write(tab + "}\n")
   
   # end function
   file.write("}\n\n")

#
#
#
def recursive_shit(file, i, tab, list1, list2):
   tab += "   ";
   
   for unique in list1:
      list1_new = copy.deepcopy(list1)
      list1_new.remove(unique)
      list2_new = copy.deepcopy(list2)
      list2_new.append(unique)
      if not list1_new:
         file.write(tab + "DirProdTensorSimpleSpecial" + str(i) + "(aRes")
         for il in list2_new:
            file.write(", aInTensor[" + str(il) + "]")
         file.write(", aCoef);\n")
         file.write(tab + "return true;\n")
         tab = tab[:-3]
         return
      else:
         file.write(tab + "if(")
         first = True
         for il1 in list1_new:
            if first:
               file.write("ModesStrictlyLessThan(aInMcs[" + str(unique) + "], aInMcs[" + str(il1) + "]) \n")
               first = False
            else:
               file.write(tab + "  && ModesStrictlyLessThan(aInMcs[" + str(unique) + "], aInMcs[" + str(il1) + "]) \n")
            
         file.write(tab + "  )\n")
         file.write(tab + "{\n")
         
         # do recursive call
         recursive_shit(file, i, tab, list1_new, list2_new)
         
         file.write(tab + "}\n")

   file.write(tab + "else\n")
   file.write(tab + "{\n")
   file.write(tab + "   return false;\n")
   file.write(tab + "}\n")

   tab = tab[:-3]


#
#
#
def make_dirprod_function(file, order):
   # documentation section
   file.write("/**\n")
   file.write(" * @function DirProdTensorSimpleSpecial   Do special direct product of all SimpleTensor's.\n")
   file.write(" * @param aRes                            Result of direct product.\n")
   file.write(" * @param aResMc                          Modecombination of result.\n")
   file.write(" * @param aInTensor                       Vector of tensors to be direct product'ed together.\n")
   file.write(" * @param aInMcs                          Vector of modecombinations corresponding to tensors in aInTensors.\n")
   file.write(" * @param aCoef                           Coefficient to be multiplied to each element of result.\n")
   file.write(" * @param return                          True if a special direct product was carried out, false otherwise.\n")
   file.write(" **/\n")

   # function declaration
   file.write("bool DirProdTensorSimpleSpecial\n")
   file.write("   ( NiceTensor<Nb>& aRes\n")
   file.write("   , const ModeCombi& aResMc\n")
   file.write("   , const std::vector<NiceTensor<Nb> >& aInTensor\n")
   file.write("   , const std::vector<ModeCombi>& aInMcs\n")
   file.write("   , const Nb aCoef\n")
   file.write("   )\n")
   file.write("{\n")

   # fun begins
   for i in range(2, order + 1):
      file.write("   if(aInTensor.size() == " + str(i) + ")\n")
      file.write("   {\n")
      recursive_shit(file, i, "   ", range(0,i), range(0,0))
      file.write("   }\n")

   # done !
   file.write("   \n")
   file.write("   return false;\n")
   file.write("}\n")

#
#
#
def main():
   parser = optparse.OptionParser("usage%prog -H <target_host> -p <target_port[s]>")
   parser.add_option("-f",dest="filename",type="string",help="specify filename")
   parser.add_option("-o",dest="order",type="int",help="specify max combination order")

   (options,args) = parser.parse_args()
   filename = options.filename
   order    = options.order

   # open file
   file = open(filename, 'w')

   # make header
   make_header(file)
   
   # make individual dirprod function
   for i in range(2, order + 1):
      make_dirprod_function_order(file, i)
   
   # make wrapper function
   make_dirprod_function(file, order)

if __name__ == "__main__":
   main()
