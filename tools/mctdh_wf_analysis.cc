/**
************************************************************************
* 
* @file    mctdh_wf_analysis.cc
*
* @date    08-08-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Analyse the difference between two MCTDH wave functions (or a series of pairs)
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include<iostream>
#include<fstream>
#include<string>
#include<cassert>
#include<complex>
#include<tuple>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "libmda/numeric/float_eq.h"

#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "util/Io.h"
#include "util/Error.h"

#include "td/mctdh/container/McTdHVectorContainer.h"

#define WIDTH 30

//! Set up output file
std::ofstream outfile("mctdh_wf_analysis.mout");
MidasStreamBuf Mout_buf(outfile);
MidasStream Mout(Mout_buf);

using param_t = std::complex<Nb>;
using tdc_t = RasTdHVectorContainer<param_t>;
using tens_t = CasTdHVectorContainer<param_t>;

/**
 * Function for calculating dot product
 *
 * @param aFirstTdc           Is first WF an MCTDH[n] WF?
 * @param aFirstName          Name of first WF
 * @param aNModalCoefFirst    Number of modal coefs in first WF
 * @param aSecondTdc          Is second WF an MCTDH[n] WF?
 * @param aSecondName         Name of second WF
 * @param aNModalCoefSecond   Number of modal coefs in second WF
 * @param aNBasVec            Number of primitive basis functions for each mode (if empty, we assume all are equal)
 * @param arFirstNorm2
 * @param arSecondNorm2
 * @param arDot
 * @param aXCS1               Extended-range modes in first WF
 * @param aXCS2               Extended-range modes in second WF
 *
 * @return
 *    Files found?
 **/
bool WaveFunctionOverlap
   (  bool aFirstTdc
   ,  const std::string& aFirstName
   ,  In aNModalCoefFirst
   ,  bool aSecondTdc
   ,  const std::string& aSecondName
   ,  In aNModalCoefSecond
   ,  const std::vector<In>& aNBasVec
   ,  Nb& arFirstNorm2
   ,  Nb& arSecondNorm2
   ,  param_t& arDot
   ,  const std::set<In>& aXCS1
   ,  const std::set<In>& aXCS2
   )
{
   if (  aFirstTdc
      && aSecondTdc
      )
   {
      tdc_t first(aNModalCoefFirst), second(aNModalCoefSecond);
      bool found = first.ReadFromDisc(aFirstName) && second.ReadFromDisc(aSecondName);

      if (  found
         )
      {
         arFirstNorm2 = std::real(midas::mctdh::wave_function_overlap(first, first, aNBasVec, aXCS1, aXCS1));
         arSecondNorm2 = std::real(midas::mctdh::wave_function_overlap(second, second, aNBasVec, aXCS2, aXCS2));
         arDot = midas::mctdh::wave_function_overlap(first, second, aNBasVec, aXCS1, aXCS2);
      }
      return found;
   }
   else if  (  aFirstTdc
            && !aSecondTdc
            )
   {
      tdc_t first(aNModalCoefFirst);
      tens_t second(aNModalCoefSecond);
      bool found = first.ReadFromDisc(aFirstName) && second.ReadFromDisc(aSecondName);

      if (  found
         )
      {
         arFirstNorm2 = std::real(midas::mctdh::wave_function_overlap(first, first, aNBasVec, aXCS1, aXCS1));
         arSecondNorm2 = std::real(midas::mctdh::wave_function_overlap(second, second, aNBasVec, aXCS2, aXCS2));
         arDot = midas::mctdh::wave_function_overlap(first, second, aNBasVec, aXCS1, aXCS2);
      }
      return found;
   }
   else if  (  !aFirstTdc
            && aSecondTdc
            )
   {
      tens_t first(aNModalCoefFirst);
      tdc_t second(aNModalCoefSecond);
      bool found = first.ReadFromDisc(aFirstName) && second.ReadFromDisc(aSecondName);

      if (  found
         )
      {
         arFirstNorm2 = std::real(midas::mctdh::wave_function_overlap(first, first, aNBasVec, aXCS1, aXCS1));
         arSecondNorm2 = std::real(midas::mctdh::wave_function_overlap(second, second, aNBasVec, aXCS2, aXCS2));
         arDot = midas::mctdh::wave_function_overlap(first, second, aNBasVec, aXCS1, aXCS2);
      }
      return found;
   }
   else if  (  !aFirstTdc
            && !aSecondTdc
            )
   {
      tens_t first(aNModalCoefFirst), second(aNModalCoefSecond);
      bool found = first.ReadFromDisc(aFirstName) && second.ReadFromDisc(aSecondName);

      if (  found
         )
      {
         arFirstNorm2 = std::real(midas::mctdh::wave_function_overlap(first, first, aNBasVec, aXCS1, aXCS1));
         arSecondNorm2 = std::real(midas::mctdh::wave_function_overlap(second, second, aNBasVec, aXCS2, aXCS2));
         arDot = midas::mctdh::wave_function_overlap(first, second, aNBasVec, aXCS1, aXCS2);
      }
      return found;
   }
   else
   {
      MIDASERROR("No such case. This should never happen!");
      return false;
   }
}

/**
 * Read info file
 *
 * @param aPrefix          Prefix of filename
 * @param arNBasVec        Number of primitive basis functions for each mode
 * @param arDt             Time between each point (if equidistant)
 * @param arTimes          Times for which we have wave functions (if not equidistant)
 * @param arExtRangeModes  Modes for which the MCR has been extended
 * @return
 *    Number of modal coefs in the WF
 **/
In ReadInfoFile
   (  const std::string& aPrefix
   ,  std::vector<In>& arNBasVec
   ,  Nb& arDt
   ,  std::vector<Nb>& arTimes
   ,  std::set<In>& arExtRangeModes
   )
{
   auto filename = aPrefix + ".info";
   In n_modal_coef = I_0;
   if (  InquireFile(filename)
      )
   {
      std::string line;
      std::ifstream infofile(filename);

      std::getline(infofile, line); // Comment
      std::getline(infofile, line); // Number of modes and electronic states
      auto modes_states = midas::util::VectorFromString<In>(line);
      assert(modes_states.size() == 2);
      auto nmodes = modes_states[0];
      auto nstates = modes_states[1];

      std::getline(infofile, line); // Comment
      std::getline(infofile, line); // Number of primitive basis functions
      arNBasVec = midas::util::VectorFromString<In>(line);

      std::getline(infofile, line); // Comment
      std::getline(infofile, line); // Number of time-dependent modal per mode
      auto nactvec = midas::util::VectorFromString<In>(line);

      assert((nactvec.size() == nmodes) && (arNBasVec.size() == nmodes));
      for(In imode=I_0; imode<nmodes; ++imode)
      {
         n_modal_coef += arNBasVec[imode] * nactvec[imode];
      }

      std::getline(infofile, line); // Comment

      // Get ext-range modes (but also support old info files, where this is not included)
      if (  line.find("Extended") != std::string::npos
         )
      {
         std::getline(infofile, line); // Extended-range modes
         auto ext_range_modes_vec = midas::util::VectorFromString<In>(line);
         arExtRangeModes = std::set<In>(ext_range_modes_vec.begin(), ext_range_modes_vec.end());
         std::getline(infofile, line); // Comment
      }

      if (  line.find("Delta T") != std::string::npos
         )
      {
         std::getline(infofile, line); // Delta T
         arDt = midas::util::FromString<Nb>(line);
      }
      else if  (  line.find("Time points") != std::string::npos
               )
      {
         while (  std::getline(infofile, line)
               )
         {
            auto [idx, time] = midas::util::TupleFromString<In, Nb>(line);
            arTimes.emplace_back(time);
         }
      }
      else
      {
         MIDASERROR("Could not find any useful time info in '" + filename + "'.");
      }

      infofile.close();
   }
   else
   {
      MIDASERROR("Could not find info file for: '" + aPrefix + "'.");
   }

   return n_modal_coef;
}


/**
 * Main function
 **/
int main
   (  int argc
   ,  char* argv[]
   )
{
   gMaxFileSize = I_10_7;

   // Check input
   bool input_error = false;
   if (  (  argc != 4
         && argc != 5
         && argc != 6
         )
      || (  argc == 6
         && std::string(argv[5]) != "-series"
         )
      || (  std::string(argv[1]) != "-tdc"
         && std::string(argv[1]) != "-tens"
         )
      || (  argc != 4
         && std::string(argv[3]) != "-tdc"
         && std::string(argv[3]) != "-tens"
         )
      || (  argc == 4
         && std::string(argv[3]) != "-autocorr"
         )
      )
   {
      input_error = true;
   }

   // Trow error if input is wrong
   if (  input_error
      )
   {
      std::cout   << "Please run as:\n"
                  << "./mctdh_wf_analysis.x <type1> <filename1> <type2> <filename2> (<option>)\n"
                  << "Type may be either '-tdc' for TensorDataCont (MCTDH[n] wave functions) or '-tens' for NiceTensor (full MCTDH wave functions).\n"
                  << "The optional argument may be '-series' for analysis of a series of WFs where the files are named as '<prefix>_i' (i = 0,1,2,...) and only the <prefix> is given as <filename>.\n"
                  << "Note that the program also needs an info file <filename>.info which contains the number of primitive basis functions for each mode, etc.\n"
                  << std::endl;
      MIDASERROR("Input error!");
   }

   // Set precision
   Mout << std::scientific << std::setprecision(16);

   // Are we analysing a series of WFs?
   bool series = (argc == 6) && (std::string(argv[5]) == "-series");
   bool ac = (argc == 4) && (std::string(argv[3]) == "-autocorr");
   
   // Types
   bool first_tdc = (std::string(argv[1]) == "-tdc");
   bool second_tdc = ac ? first_tdc : (std::string(argv[3]) == "-tdc");

   // Names
   std::string name1(argv[2]);
   std::string name2 = ac ? name1 : std::string(argv[4]);

   // Locate info file to get number of basis functions, time points, etc.
   std::vector<In> nbasvec1, nbasvec2;
   Nb dt1 = -C_1, dt2 = -C_1;
   std::vector<Nb> times1, times2;
   std::set<In> xcs1, xcs2;
   In n_coef_1 = ReadInfoFile(name1, nbasvec1, dt1, times1, xcs1);
   In n_coef_2 = -I_1;
   if (  !ac
      )
   {
      n_coef_2 = ReadInfoFile(name2, nbasvec2, dt2, times2, xcs2);

      // Assert all nbas equal
      assert(nbasvec1.size() == nbasvec2.size());
      for(In i=I_0; i<nbasvec1.size(); ++i)
      {
         assert(nbasvec1[i] == nbasvec2[i]);
      }

      // Assert all times equal
      assert(libmda::numeric::float_eq(dt1, dt2));
      assert(times1.size() == times2.size());
      for(In i=I_0; i<times1.size(); ++i)
      {
         assert(libmda::numeric::float_eq(times1[i], times2[i]));
      }
   }

   const auto& nbasvec = nbasvec1;
   const auto& times = times1;
   const auto& dt = dt1;

   // Write header
   if (  ac
      )
   {
      Mout  << std::setw(WIDTH) << "# Time/idx"
            << std::setw(WIDTH) << "Re[S]"
            << std::setw(WIDTH) << "Im[S]"
            << std::setw(WIDTH) << "|S|"
            << std::setw(WIDTH) << "||bra||^2"
            << std::setw(WIDTH) << "||ket||^2"
            << std::setw(WIDTH) << "Diff norm2"
            << std::setw(WIDTH) << "Phase diff"
            << std::setw(WIDTH) << "Phase-corrected diff norm2"
            << std::setw(WIDTH) << "Hilbert-space angle"
            << std::endl;
   }
   else
   {
      Mout  << std::setw(WIDTH) << "# Time/idx"
            << std::setw(WIDTH) << "Re[<1|2>]"
            << std::setw(WIDTH) << "Im[<1|2>]"
            << std::setw(WIDTH) << "||1||^2"
            << std::setw(WIDTH) << "||2||^2"
            << std::setw(WIDTH) << "Diff norm2"
            << std::setw(WIDTH) << "Phase diff"
            << std::setw(WIDTH) << "Phase-corrected diff norm2"
            << std::setw(WIDTH) << "Hilbert-space angle"
            << std::endl;
   }

   // Do analysis
   if (  series
      )
   {
      In idx = I_0;
      Nb first_norm2, second_norm2;
      param_t dot;
      while (  WaveFunctionOverlap(first_tdc, name1 + "_" + std::to_string(idx), n_coef_1, second_tdc, name2 + "_" + std::to_string(idx), n_coef_2, nbasvec, first_norm2, second_norm2, dot, xcs1, xcs2)
            )
      {
         if (  !times.empty()
            )
         {
            Mout  << std::setw(WIDTH) << times[idx];
         }
         else if  (  dt > C_0
                  )
         {
            Mout  << std::setw(WIDTH) << idx*dt;
         }
         else
         {
            Mout  << std::setw(WIDTH) << idx;
         }

         Nb diff2 = first_norm2 + second_norm2 - C_2*std::real(dot);
         Nb phase_err = std::atan2(std::imag(dot), std::real(dot));
         Nb phase_corr_err2 = first_norm2 + second_norm2 - C_2*std::abs(dot);
         Nb hilbert_angle = std::acos(std::abs(dot)/(std::sqrt(first_norm2)*std::sqrt(second_norm2)));
         Mout  << std::setw(WIDTH) << dot.real()
               << std::setw(WIDTH) << dot.imag()
               << std::setw(WIDTH) << first_norm2
               << std::setw(WIDTH) << second_norm2
               << std::setw(WIDTH) << diff2
               << std::setw(WIDTH) << phase_err
               << std::setw(WIDTH) << phase_corr_err2
               << std::setw(WIDTH) << hilbert_angle
               << std::endl;
         ++idx;
      }
   }
   else if  (  ac
            )
   {
      In idx = I_0;
      Nb first_norm2, second_norm2;
      param_t dot;
      while (  WaveFunctionOverlap(first_tdc, name1 + "_0", n_coef_1, second_tdc, name2 + "_" + std::to_string(idx), n_coef_1, nbasvec, first_norm2, second_norm2, dot, xcs1, xcs1)
            )
      {
         if (  !times.empty()
            )
         {
            Mout  << std::setw(WIDTH) << times[idx];
         }
         else if  (  dt > C_0
                  )
         {
            Mout  << std::setw(WIDTH) << idx*dt;
         }
         else
         {
            Mout  << std::setw(WIDTH) << idx;
         }

         Nb diff2 = first_norm2 + second_norm2 - C_2*std::real(dot);
         Nb phase_err = std::atan2(std::imag(dot), std::real(dot));
         Nb phase_corr_err2 = first_norm2 + second_norm2 - C_2*std::abs(dot);
         Nb hilbert_angle = std::acos(std::abs(dot)/(std::sqrt(first_norm2)*std::sqrt(second_norm2)));
         Mout  << std::setw(WIDTH) << dot.real()
               << std::setw(WIDTH) << dot.imag()
               << std::setw(WIDTH) << std::abs(dot)
               << std::setw(WIDTH) << first_norm2
               << std::setw(WIDTH) << second_norm2
               << std::setw(WIDTH) << diff2
               << std::setw(WIDTH) << phase_err
               << std::setw(WIDTH) << phase_corr_err2
               << std::setw(WIDTH) << hilbert_angle
               << std::endl;
         ++idx;
      }
   }
   else
   {
      Nb first_norm2, second_norm2;
      param_t dot;
      if (  WaveFunctionOverlap(first_tdc, name1, n_coef_1, second_tdc, name2, n_coef_2, nbasvec, first_norm2, second_norm2, dot, xcs1, xcs2)
         )
      {
         Nb diff2 = first_norm2 + second_norm2 - C_2*std::real(dot);
         Nb phase_err = std::atan2(std::imag(dot), std::real(dot));
         Nb phase_corr_err2 = first_norm2 + second_norm2 - C_2*std::abs(dot);
         Nb hilbert_angle = std::acos(std::abs(dot)/(std::sqrt(first_norm2)*std::sqrt(second_norm2)));
         Mout  << std::setw(WIDTH) << "0"
               << std::setw(WIDTH) << dot.real()
               << std::setw(WIDTH) << dot.imag()
               << std::setw(WIDTH) << first_norm2
               << std::setw(WIDTH) << second_norm2
               << std::setw(WIDTH) << diff2
               << std::setw(WIDTH) << phase_err
               << std::setw(WIDTH) << phase_corr_err2
               << std::setw(WIDTH) << hilbert_angle
               << std::endl;
      }
      else
      {
         MIDASERROR("Files not found!");
      }
   }

   return 0;
}
