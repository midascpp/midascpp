#ifndef RSPMERGER_H_INCLUDED
#define RSPMERGER_H_INCLUDED

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

//#include "input/Input.h" // for gAnalysisDir
#include "input/CombinedRspFunc.h"
#include "analysis/ResponseAnalysisInterface.h"

extern std::string gAnalysisDir;

class RspMerger
{
   private:
      CombinedRspFunc mRsp;
      std::vector<std::string> mFiles;
      bool mGatherFiles = false;

   public:
      RspMerger(std::string&& rsp, std::vector<std::string>&& files)
       : mRsp(std::move(rsp))
       , mFiles(std::move(files))
      {
      }

      void SetGatherFiles() { mGatherFiles = true; }

      void Merge()
      {
         for(size_t i=0; i<mFiles.size(); ++i)
         {
            std::ifstream rsp_in_file(mFiles[i]);
            mRsp.AddRspFunc(rsp_in_file);
            rsp_in_file.close();
         }

         mRsp.Sort();
         if(mGatherFiles) mRsp.GatherFiles();
         
         analysis::rsp().MakeAnalysisDir();
         std::ofstream rsp_out_file(analysis::rsp().main_path, std::ios::app);
         rsp_out_file << mRsp << std::endl;
         rsp_out_file.close();
      }
};

#endif /* RSPMERGER_H_INCLUDED */
