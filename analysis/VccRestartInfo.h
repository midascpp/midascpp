#ifndef VCCRESTARTINFO_H_INCLUDED
#define VCCRESTARTINFO_H_INCLUDED

#include <fstream>

#include "inc_gen/TypeDefs.h"

class VccRestartInfo
{
   public:
      VccRestartInfo(std::ifstream& restart_file);
      //VccRestartInfo(const std::string& restart_file_name);
      
      In NumModes() const { return mNumModes; }
      const std::vector<In>& ModalsPerMode() const { return mModalsPerMode; }
      In ExciLevel() const { return mExciLevel; }
      In SizeOfVector() const { return mSizeOfVector; }

   private:
      In mNumModes = 0;
      std::vector<In> mModalsPerMode = std::vector<In>(0);
      In mExciLevel = 0;
      In mSizeOfVector = 0;
};

#endif /* VCCRESTARTINFO_H_INCLUDED */
