/**
************************************************************************
* 
* @file                Spectroscopy.h
*
* Created:             11-11-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Generating spectra
* 
* Last modified: ???
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SPECTROSCOPY_H
#define SPECTROSCOPY_H

#include "inc_gen/TypeDefs.h"

/**
* Main function for generating spectra.
* */
void SpectroscopyAnalysis();

#endif // SPECTROSCOPY_H

