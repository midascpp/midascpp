/**
************************************************************************
* 
* @file                RspSpectrum.cc
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of general base class to handle 
*                      different kinds of response spectra (e.g. IR, Raman, etc...).
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "RspSpectrum.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <assert.h>
#include <sstream>

#include "input/VccCalcDef.h"
#include "input/OpDef.h"
#include "util/MultiIndex.h"
#include "vcc/Xvec.h"
#include "input/ModeCombiOpRange.h"
#include "inc_gen/Const.h"
#include "pes/splines/Spline1D.h"
#include "mmv/DataCont.h"
#include "analysis/VccRestartInfo.h"

extern std::string gAnalysisDir;

/**
 * Default constructor.
 **/
RspSpectrum::RspSpectrum
   (
   )
   : mFrqs()
   , mAbs()
   , mPeaks()
   , mMaxAbs(0)
   , mSpectrumAnalysisPoints(1)
{
}

/**
 * Add a data point to the spectrum.
 * @param abs                 Absorption at given point. 
 * @param frq                 Set of frequencies at given point.
 **/
void RspSpectrum::AddAbsorption
   ( Nb abs
   , const std::vector<Nb>& frq
   )
{
   mAbs.emplace_back(abs);
   mFrqs.emplace_back(frq);
}

/**
 * Find the element(frequency) in a vector of frequencies,
 *                            that is nearest to a desired frequency.
 * @param frq                 Desired frequency.
 * @param frqs                Set of frequencies to search.
 * @return                    Return the nearest frequency and its index.
 **/
typename RspSpectrum::SpectrumBlock::index_frq_t RspSpectrum::FindNearestFrq
   ( const Nb frq
   , const std::vector<std::vector<Nb> >& frqs
   ) const
{
   const int ext_frq_idx = 1; // hardcoded to linear rsp
   assert(frqs.size() > 0); // assert that we have some points
   
   if(frq < frqs[0][ext_frq_idx]) // check if frq goes before the first frequency
   {
      return {frqs[0][ext_frq_idx], 0}; // return
   }

   for(int i = 0; i < frqs.size() - 1; ++i)
   {
      if( (frq >= frqs[i][ext_frq_idx]) && (frq < frqs[i + 1][ext_frq_idx]) ) // if problems are encountered change to libmda float_eq
      {
         return {frqs[i][ext_frq_idx], i}; // return
      }
   }
   
   // if we havent hit it yet, frq is after frq range
   return {frqs[frqs.size()-1][ext_frq_idx], frqs.size()-1}; // return
}

/**
 * Historic function to calculate the curvature of a discrete function at a
 * specific point, using 1 point on either side.
 * @param frqs               Set of discrete frequencies(x-axis).
 * @param absorb             Set of discrete absorption(y-axis).
 * @param i                  Point to calculate curvature for.
 * @return                   Curvature at the given point.
 **/
Nb CurvatureOld
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t i
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response.

   const Nb alpha_1 = frqs[i-1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_2 = 0;
   const Nb alpha_3 = frqs[i+1][ext_frq_idx] - frqs[i][ext_frq_idx];

   const Nb term_1 = 2*absorb[i-1]/((alpha_1-alpha_2)*(alpha_1 - alpha_3));
   const Nb term_2 = 2*absorb[i]/((alpha_2-alpha_1)*(alpha_2 - alpha_3));
   const Nb term_3 = 2*absorb[i+1]/((alpha_3-alpha_1)*(alpha_3 - alpha_2));

   return (term_1 + term_2 + term_3);
}

/**
 * Helper for function Curvature().
 **/
Nb Frontfac(Nb a1, Nb a2, Nb a3, Nb a4)
{
   return a1*a2 + a1*a3 + a1*a4 + a2*a3 + a2*a4 + a3*a4;
}

/**
 * Helper for function Curvature().
 **/
Nb Divisorfac(Nb a1, Nb a2, Nb a3, Nb a4, Nb a5)
{
   return (a1-a2)*(a1-a3)*(a1-a4)*(a1-a5);
}

/**
 * Historic function to calculate the curvature of a discrete function at a
 * specific point, using 2 points on either side.
 * @param frqs               Set of discrete frequencies(x-axis).
 * @param absorb             Set of discrete absorption(y-axis).
 * @param i                  Point to calculate curvature for.
 * @return                   Curvature at the given point.
 **/
Nb Curvature
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t i
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response.

   const Nb alpha_1 = frqs[i-2][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_2 = frqs[i-1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_3 = 0;
   const Nb alpha_4 = frqs[i+1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_5 = frqs[i+2][ext_frq_idx] - frqs[i][ext_frq_idx];
   
   const Nb front_1 = Frontfac(alpha_2,alpha_3,alpha_4,alpha_5);
   const Nb front_2 = Frontfac(alpha_1,alpha_3,alpha_4,alpha_5);
   const Nb front_3 = Frontfac(alpha_1,alpha_2,alpha_4,alpha_5);
   const Nb front_4 = Frontfac(alpha_1,alpha_2,alpha_3,alpha_5);
   const Nb front_5 = Frontfac(alpha_1,alpha_2,alpha_3,alpha_4);
   
   const Nb divisor_1 = Divisorfac(alpha_1,alpha_2,alpha_3,alpha_4,alpha_5);
   const Nb divisor_2 = Divisorfac(alpha_2,alpha_1,alpha_3,alpha_4,alpha_5);
   const Nb divisor_3 = Divisorfac(alpha_3,alpha_1,alpha_2,alpha_4,alpha_5);
   const Nb divisor_4 = Divisorfac(alpha_4,alpha_1,alpha_2,alpha_3,alpha_5);
   const Nb divisor_5 = Divisorfac(alpha_5,alpha_1,alpha_2,alpha_3,alpha_4);

   const Nb term_1 = 2*front_1*absorb[i-2]/divisor_1;
   const Nb term_2 = 2*front_2*absorb[i-1]/divisor_2;
   const Nb term_3 = 2*front_3*absorb[i]/divisor_3;
   const Nb term_4 = 2*front_4*absorb[i+1]/divisor_4;
   const Nb term_5 = 2*front_5*absorb[i+2]/divisor_5;

   return (term_1 + term_2 + term_3 + term_4 + term_5);
}

/**
 * Helper for function Derivative().
 **/
Nb FrontfacDer(Nb a1, Nb a2, Nb a3, Nb a4)
{
   return a1*a2*a3 + a1*a2*a4 + a1*a3*a4 + a2*a3*a4;
}

/**
 * Historic function to calculate the derivative of a discrete function at a
 * specific point, using 2 points on either side.
 * @param frqs               Set of discrete frequencies(x-axis).
 * @param absorb             Set of discrete absorption(y-axis).
 * @param i                  Point to calculate derivative for.
 * @return                   Derivative at the given point.
 **/
Nb Derivative
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t i
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response.

   const Nb alpha_1 = frqs[i-2][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_2 = frqs[i-1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_3 = 0;
   const Nb alpha_4 = frqs[i+1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_5 = frqs[i+2][ext_frq_idx] - frqs[i][ext_frq_idx];
   
   const Nb front_1 = FrontfacDer(alpha_2,alpha_3,alpha_4,alpha_5);
   const Nb front_2 = FrontfacDer(alpha_1,alpha_3,alpha_4,alpha_5);
   const Nb front_3 = FrontfacDer(alpha_1,alpha_2,alpha_4,alpha_5);
   const Nb front_4 = FrontfacDer(alpha_1,alpha_2,alpha_3,alpha_5);
   const Nb front_5 = FrontfacDer(alpha_1,alpha_2,alpha_3,alpha_4);
   
   const Nb divisor_1 = Divisorfac(alpha_1,alpha_2,alpha_3,alpha_4,alpha_5);
   const Nb divisor_2 = Divisorfac(alpha_2,alpha_1,alpha_3,alpha_4,alpha_5);
   const Nb divisor_3 = Divisorfac(alpha_3,alpha_1,alpha_2,alpha_4,alpha_5);
   const Nb divisor_4 = Divisorfac(alpha_4,alpha_1,alpha_2,alpha_3,alpha_5);
   const Nb divisor_5 = Divisorfac(alpha_5,alpha_1,alpha_2,alpha_3,alpha_4);

   const Nb term_1 = -front_1*absorb[i-2]/divisor_1;
   const Nb term_2 = -front_2*absorb[i-1]/divisor_2;
   const Nb term_3 = -front_3*absorb[i]/divisor_3;
   const Nb term_4 = -front_4*absorb[i+1]/divisor_4;
   const Nb term_5 = -front_5*absorb[i+2]/divisor_5;

   return (term_1 + term_2 + term_3 + term_4 + term_5);
}

/**
 * Historic function to calculate the derivative of a discrete function at a
 * specific point, using 1 point on either side.
 * @param frqs               Set of discrete frequencies(x-axis).
 * @param absorb             Set of discrete absorption(y-axis).
 * @param i                  Point to calculate derivative for.
 * @return                   Derivative at the given point.
 **/
Nb DerivativeOld
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t i
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response.

   const Nb alpha_1 = frqs[i-1][ext_frq_idx] - frqs[i][ext_frq_idx];
   const Nb alpha_2 = 0;
   const Nb alpha_3 = frqs[i+1][ext_frq_idx] - frqs[i][ext_frq_idx];

   const Nb term1 = (alpha_2 + alpha_3)*absorb[i-1] / ((alpha_1 - alpha_2)*(alpha_1 - alpha_3));
   const Nb term2 = (alpha_1 + alpha_3)*absorb[i]   / ((alpha_2 - alpha_1)*(alpha_2 - alpha_3));
   const Nb term3 = (alpha_1 + alpha_2)*absorb[i+1] / ((alpha_3 - alpha_1)*(alpha_3 - alpha_2));

   return -(term1 + term2 + term3);
}

/**
 * Historic function to calculate the derivative of a discrete function at a
 * specific point, using central difference.
 * @param frqs                   Set of discrete frequencies(x-axis).
 * @param absorb                 Set of discrete absorption(y-axis).
 * @param i                      Point to calculate derivative for.
 * @return                       Derivative at the given point.
 **/
Nb DerivativeCentral
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t i
   )
{
   const In ext_frq_idx = 1;

   Nb deriv = (absorb[i+1] - absorb[i-1])/(frqs[i+1][ext_frq_idx] - frqs[i-1][ext_frq_idx]);

   return deriv;
}

/**
 * Historic function to calculate the numerical derivative of a non-equidistant
 * discrete function at a specific point, using n_half_points on either side of
 * the desired point.
 * @param frqs                     Set of discrete frequencies(x-axis).
 * @param absorb                   Set of discrete absorption(y-axis).
 * @param point                    Point to calculate derivative for.
 * @param n_half_points            Number of points used on each side. (experience shows n_half_points = 1 is most stable)
 * @return                         Derivative at the given point.
 **/
Nb NumericalDerivative
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   , size_t point
   , size_t n_half_points
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response

   const In lower_point = point - n_half_points;
   const In upper_point = point + n_half_points;
   
   Nb deriv = 0.0;
   // d/dx p_j (x) = \sum_i^j d/dx L_{i,j}(x) u_i
   for(In i = lower_point; i <= upper_point; ++i)
   {
      Nb l_deriv = 0.0;
      // d/dx L_{i,j} = \sum_k^j (x-x_0)...(x-x_{k-1})(x-x_{k+1})...(x-x_{i-1})(x-x_{i+1})...(x-x_j)/(x-x_0)..(x-x_{i-1})(x-x_{i+1})...(x-x_j)
      for(In k = lower_point; k <= upper_point; ++k)
      {
         if(k != i)
         {
            Nb numerator = 1.0;
            Nb denominator = 1.0;
            for(In j = lower_point; j <= upper_point; ++j)
            {
               if(j != i)
               {
                  denominator *= (frqs[i][ext_frq_idx] - frqs[j][ext_frq_idx]);
               }
               if((j != i) && (j != k))
               {  
                  numerator *= (frqs[point][ext_frq_idx] - frqs[j][ext_frq_idx]);
               }
            }
            // sum d/dx L_{i,j}(x)
            l_deriv += numerator/denominator;
         }
      }
      // sum d/dx p_j(x)
      deriv += l_deriv * absorb[i];
   }
   return deriv;
}

/**
 * Implementation of IdentifyPeaks.  Identifies peaks of a rsp-spectrum by
 * identifying turning points through numerical derivatives.
 * @param frqs                   Set of discrete frequencies(x-axis).
 * @param absorb                 Set of discrete absorption(y-axis).
 **/
void RspSpectrum::IdentifyPeaksImpl
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   )
{
   assert(mPeaks.size() == 0); // we can only run once
   assert(frqs.size()>0);
   assert(frqs.size() == absorb.size());
   assert(frqs[0].size() == 2); // only implemented for linear response
   
   const In ext_frq_idx = 1; //hardcoded for linear response
   const In points_for_deriv = mSpectrumAnalysisPoints; // set number of points that are needed to calculate numerical derivatives
   
   // setup first block
   mPeaks.emplace_back();
   mPeaks.back().mBlockBegin = SpectrumBlock::index_frq_t{ frqs[0][ext_frq_idx]
                                                         , 0
                                                         };

   // loop over points
   for (In i = points_for_deriv + 1; i < (frqs.size()-points_for_deriv - 1); ++i) 
   {
      SpectrumBlock& current_block = mPeaks.back(); // get reference to 'working block'
      
      // check for turning points
      Nb derm1 = NumericalDerivative(frqs, absorb, i-1, points_for_deriv);
      Nb der0  = NumericalDerivative(frqs, absorb, i  , points_for_deriv);
      Nb derp1 = NumericalDerivative(frqs, absorb, i+1, points_for_deriv);
      
      // identify turning points: maximum, minimum, or saddle
      bool is_turn = (libmda::numeric::float_lt(fabs(der0), fabs(derm1)) && libmda::numeric::float_lt(fabs(der0), fabs(derp1)));
      bool is_max  = (libmda::numeric::float_gt(der0, derp1) && libmda::numeric::float_lt(der0, derm1));
      bool is_min  = (libmda::numeric::float_lt(der0, derp1) && libmda::numeric::float_gt(der0, derm1));
      bool is_saddle = (  (libmda::numeric::float_gt(der0, derm1) && libmda::numeric::float_gt(der0, derp1)) 
                       || (libmda::numeric::float_lt(der0, derm1) && libmda::numeric::float_lt(der0, derp1))
                       );
      
      // handle turning points
      if ( is_turn && (is_max || is_saddle) )
      { 
         // handle maximima and saddle points
         if(!current_block.IsPeakAssigned()) 
         { 
            // if peak is not assigned we assign it
            current_block.mPeak = SpectrumBlock::peak_t{frqs[i][ext_frq_idx], absorb[i]};
            if(is_max)
            {
               current_block.mType = "Peak";
            }
            if(is_saddle)
            {
               current_block.mType = "Shoulder";
            }
         }
         else
         { 
            // else we need to make a new block to hold the peak
            SpectrumBlock new_block;
            new_block.mPeak = SpectrumBlock::peak_t{frqs[i][ext_frq_idx], absorb[i]};
               
            // calculate where to separate blocks
            auto distance = new_block.mPeak.first - current_block.mPeak.first;
            auto factor = current_block.mPeak.second / new_block.mPeak.second;
            auto distance_1 = (factor*distance) / (1 + factor);
            auto distance_2 = distance - distance_1;

            current_block.mBlockEnd = FindNearestFrq(current_block.mPeak.first + distance_1, frqs);
            new_block.mBlockBegin = SpectrumBlock::index_frq_t{ frqs[current_block.mBlockEnd.second + 1][ext_frq_idx]
                                                              , current_block.mBlockEnd.second + 1
                                                              };
            // add new block
            mPeaks.emplace_back(new_block);
         }
      }
      else if( is_turn && is_min)
      { 
         // handle minima
         current_block.mBlockEnd = SpectrumBlock::index_frq_t{frqs[i][ext_frq_idx], i};
         mPeaks.emplace_back();
         mPeaks.back().mBlockBegin = SpectrumBlock::index_frq_t{frqs[i + 1][ext_frq_idx], i + 1};
      }
   }

   // finish last block
   mPeaks.back().mBlockEnd = SpectrumBlock::index_frq_t{ frqs[frqs.size()-1][ext_frq_idx]
                                                       , frqs.size() - 1
                                                       };
   
   // identify max absorption
   for(size_t i=0; i<mPeaks.size(); ++i)
   {
      if (PeakAbs(i) > mMaxAbs) 
      {
         mMaxAbs=PeakAbs(i);
      }
   }
}

/**
 * Implementation of CalcutaIntegratedIntensity.  Calculate the integrate
 * intensity of each peak that has been identified (using function
 * IdentifyPeaks()).
 * @param frqs                                  Set of discrete frequencies(x-axis).
 * @param absorb                                Set of discrete absorption(y-axis).
 **/
void RspSpectrum::CalculateIntegratedIntensityImpl
   ( const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   )
{
   assert(mPeaks.size()>0); // assert we have identified peaks
   assert(frqs.size()>0); // assert we have frqs

   const In ext_frq_idx = 1; //hardcoded for linear response
   const Nb frq_step = (frqs[frqs.size()-1][ext_frq_idx] - frqs[0][ext_frq_idx])/frqs.size();

   // find integrated intensities
   for(auto& peak: mPeaks)
   {
      peak.mIntegratedIntensity = 0.0;
      for(int i = peak.mBlockBegin.second; i < peak.mBlockEnd.second; ++i)
      {
         peak.mIntegratedIntensity += absorb[i] * frq_step;
      }
      // find max integrated intensity
      if(peak.mIntegratedIntensity > mMaxIntegratedIntensity)
      {
         mMaxIntegratedIntensity = peak.mIntegratedIntensity;
      }
   }
   // set max integrated intensity for all Spectrum blocks
   for(auto& peak: mPeaks)
   {
      peak.mMaxIntegratedIntensity = mMaxIntegratedIntensity;
   }
}

/**
 * Implementation of CalculateWeights.  For each identified peak we calculate
 * the weights of each 'configuration'/excitation.  See The Journal of Chemical
 * Physics 143, 134108 (2015); doi: 10.1063/1.4932010, eqs. 50-52.
 * @param f                         The rule for calculating the weight of each configuration.
 * @param frqs                      Grid of frequencies.
 *                                  Must be called with mFrqs, not the interpolated!!!
 **/
void RspSpectrum::CalculateWeightsImpl
   ( const std::function<void(Nb, DataCont&)>& f
   , const std::vector<std::vector<Nb> >& frqs // must be called with mFrqs (not interpolated!)
   )
{
   const In ext_frq_idx = 1; // hardcoded for linear response
   std::ifstream ifs(gAnalysisDir + "/VCCRSPRESTART.INFO");
   VccRestartInfo restart_info(ifs);
   DataCont new_weight(restart_info.SizeOfVector());
   
   // setup first peak that we process
   auto peak_iter = mPeaks.begin();
   peak_iter->mWeights.SetNewSize(restart_info.SizeOfVector());
   peak_iter->mWeights.Zero();
   // loop over frequencies
   for(auto& frq : frqs)
   {
      if(libmda::numeric::float_gt(frq[ext_frq_idx], peak_iter->mBlockEnd.first))
      { // done with block. Scale weights and increment iterator
         peak_iter->mWeights.Scale(C_1/peak_iter->mWeights.SumEle());
         ++peak_iter; // increment iter to point at next block
         
         MidasAssert(peak_iter != mPeaks.end(), "Reached end, which I'm not supposed to do :CC");
         
         peak_iter->mWeights.SetNewSize(restart_info.SizeOfVector());
         peak_iter->mWeights.Zero();
      }
      
      f(frq[ext_frq_idx], new_weight); // WARNING: hardcoded for linear response!
      Axpy(peak_iter->mWeights, new_weight, C_1);
   }
   // 'normalize' last weight
   peak_iter->mWeights.Scale(C_1/peak_iter->mWeights.SumEle());
}

/**
 * Get frequencies. If interpolated will return the interpolated frequencies.
 * @return             Either the interpolated or the original frequencies.
 **/
const std::vector<std::vector<Nb> >& RspSpectrum::GetFrqs
   (
   ) const
{
   if(mInterpolFrqs.size() > 0)
   {
      return mInterpolFrqs;
   }
   else
   {
      return mFrqs;
   }
}

/**
 * Get absorption. If interpolated will return the interpolated absorbtion.
 * @return            Either the interpolated or the original absorptions.
 **/ 
const std::vector<Nb>& RspSpectrum::GetAbs
   (
   ) const
{
   if(mInterpolAbs.size() > 0)
   {
      return mInterpolAbs;
   }
   else
   {
      return mAbs;
   }
}

/**
 * Interface function. Identify peaks and calculate integrated intensities.
 * Will use interpolated values if and interpolation has been done before.
 **/
void RspSpectrum::IdentifyPeaks
   (
   )
{
   IdentifyPeaksImpl(GetFrqs(), GetAbs());
   CalculateIntegratedIntensityImpl(GetFrqs(), GetAbs());
}

/**
 * Interface function. Calculate weights of each configuration.  Must be called
 * after function 'IdentifyPeaks'.  Cannot use interpolated frequencies.
 * @param f                    Rule to calculate weights.
 **/
void RspSpectrum::CalculateWeights
   ( const std::function<void(Nb, DataCont&)>& f
   )
{
   CalculateWeightsImpl(f, mFrqs); // NB: here we cannot pass interpolated frequencies, therefore we directly pass mFrqs

   for(auto& peak : mPeaks)
   {
      if(peak.IsPeakAssigned())
         Mout << peak << std::endl;
   }
}

/**
 * Interpolate the spectrum making it more smooth.  This improves peak
 * identification and calculated peak positions.
 **/
void RspSpectrum::Interpolate
   (
   )
{
   Mout << " Doing interpolation of spectrum for smoothing out peaks\n " << std::endl;
   assert(mInterpolFrqs.size() == 0); // we can only interpolate once
   assert(mInterpolAbs.size() == 0);

   MidasVector xig(mFrqs.size());
   MidasVector yig(mAbs.size());
   for(int i=0; i<xig.Size(); ++i)
   {
      xig[i] = mFrqs[i][1];
      yig[i] = mAbs[i];
   }

   Spline1D spline_1d(SPLINEINTTYPE::NATURAL,xig,yig,0.0,0.0);
   
   Nb step = mInterpolateStep/C_AUTKAYS;
   int nog = (xig[xig.Size()-1] - xig[0])/step;
   MidasVector xog(nog);
   MidasVector yog(nog);
   
   Nb frq = xig[0];
   for(int i=0; i<xog.Size(); ++i)
   {
      xog[i] = frq;
      frq += step;
   }
   
   MidasVector yig_deriv(yig.Size());
   spline_1d.GetYigDerivs(yig_deriv);

   GetYog(yog,xog,xig,yig,yig_deriv,0,1);

   for(int i=0; i<yog.Size(); ++i)
   {
      mInterpolFrqs.emplace_back(std::vector<Nb>{-xog[i],xog[i]});
      mInterpolAbs.emplace_back(yog[i]);
   }
   // insert end point, which is not handled by interpolation
   mInterpolFrqs.emplace_back(mFrqs.back());
   mInterpolAbs.emplace_back(mAbs.back());
}

/**
 * Implementation of PlotSpectrum.  Plot the spectrum to an output file stream.
 * @param data_file             The ofstream to print the data to.
 * @param frqs                  Set of discrete frequencies(x-axis).
 * @param absorb                Set of discrete absorption(y-axis).
 **/
void RspSpectrum::PlotSpectrumImpl
   ( std::ofstream& data_file
   , const std::vector<std::vector<Nb> >& frqs
   , const std::vector<Nb>& absorb
   ) const
{
   int prec = 17;
   int width = 26;
   
   midas::stream::ScopedPrecision(prec, data_file);
   
   for(size_t i=0; i<frqs.size(); ++i)
   {
      data_file << std::fixed << std::setw(width) << frqs[i][1]*C_AUTKAYS
                << std::scientific << std::setw(width) << absorb[i]
                << "\n";
   }
}

/**
 * Plot the spectrum to a file.
 * @param filename           Name of plot file.
 * @param type               Type of spectrum.
 **/
void RspSpectrum::PlotSpectrum
   ( const std::string& filename
   , const std::string& type
   ) const
{
   int prec = 17;
   int width = 26;
   
   std::ofstream data_file(filename);

   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << ("#"+type)
                          << "\n";

   PlotSpectrumImpl(data_file,GetFrqs(),GetAbs());
   
   data_file.close();
}

/**
 * Plot a stick spectrum using (interpolated) peak absoprtion.
 * @param filename       Name of plot file.
 * @param type           Type of spectrum.
 **/
void RspSpectrum::PlotStick
   ( const std::string& filename
   , const std::string& type
   ) const
{
   int prec = 17;
   int width = 26;
   
   std::ofstream data_file(filename);
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << ("#"+type)
                          << "\n";
      
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      if(mPeaks[i].IsPeakAssigned())
      {
         data_file << std::fixed << std::setw(width) << PeakFrq(i)*C_AUTKAYS
                   << std::scientific << std::setw(width) << 0.0 << "\n"
                   << std::fixed << std::setw(width) << PeakFrq(i)*C_AUTKAYS 
                   << std::scientific << std::setw(width) << PeakAbs(i) << "\n"
                   << std::fixed << std::setw(width) << PeakFrq(i)*C_AUTKAYS 
                   << std::scientific << std::setw(width) << 0.0 << "\n";
      }
   }
}

/**
 * Plot a stick spectrum showing the beginning of each SpectBlock.
 * @warning
 *    Used only for DEBUGGING purposes!
 * @param filename          Name of plot file.
 * @param type              Type of spectrum.
 **/
void RspSpectrum::PlotStickMin
   ( const std::string& filename
   , const std::string& type
   ) const
{
   int prec = 17;
   int width = 26;
   Nb fake_intensity = 0.25*mMaxAbs;
   
   std::ofstream data_file(filename);
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << ("#"+type)
                          << "\n";
      
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      data_file << std::fixed << std::setw(width) << mPeaks[i].mBlockEnd.first*C_AUTKAYS
                << std::scientific << std::setw(width) << 0.0 << "\n"
                << std::fixed << std::setw(width) << mPeaks[i].mBlockEnd.first*C_AUTKAYS 
                << std::scientific << std::setw(width) << fake_intensity << "\n"
                << std::fixed << std::setw(width) << mPeaks[i].mBlockEnd.first*C_AUTKAYS 
                << std::scientific << std::setw(width) << 0.0 << "\n";
   }
}

/**
 * Print all peak positions and integrated intensities in a nice format to a
 * std::ostream (e.g. Mout).
 * @param os                                    The std::ostream to print to.
 **/
void RspSpectrum::PrintPeakPositionsAndIntensities
   ( std::ostream& os
   ) const
{
   int prec = 10;
   int width = 26;

   os << " Peak positions and Intensities - abs and relative to max:\n"
      << " ---------------------------------------------------------\n";
   midas::stream::ScopedPrecision(prec, os);
   midas::stream::ScopedIosFlags(std::ios::left | std::ios::scientific, os); // will reset all ios flags to what they were before function call when function return
   
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      if(mPeaks[i].IsPeakAssigned())
      {
         os << " " 
            << std::fixed << std::setw(width) << PeakFrq(i)*C_AUTKAYS 
            << std::scientific << std::setw(width) << PeakAbs(i) 
            << std::fixed << std::setw(width) << PeakAbs(i)/mMaxAbs 
            << "\n";
      }
   }
}

/**
 * Print detailed information on a SpectBlock.  Used to print each spect block
 * if weights have been calculated.
 * @param os               The std::ostream to print to.
 * @param spect_block      The SpectBlock to print.
 **/
std::ostream& operator<<
   ( std::ostream& os
   , const RspSpectrum::SpectrumBlock& spect_block
   )
{
   os << std::setprecision(2) << "    " << spect_block.mBlockBegin.first*C_AUTKAYS << " - " << spect_block.mBlockEnd.first*C_AUTKAYS << " cm-1 \n" 
      << "    Peak                 = " << spect_block.mPeak.first*C_AUTKAYS << " cm-1 \n" 
      << "    Type                 = " << spect_block.mType << "\n" 
      //<< " (" << aBlock.mNstates << " states included)" << endl
      << std::setprecision(16)
      << "    Integrated intensity = " << spect_block.mIntegratedIntensity << " (arb. u.)\n"
      << std::setprecision(3) << "    Integrated intensity = "
      << C_100*spect_block.mIntegratedIntensity/spect_block.mMaxIntegratedIntensity << " (% of largest intensity)\n";
   Mout.Mute(); // mute Mout so we still get pretty outpu in case os := Mout
   
   // setup some stuff
   std::ifstream ifs(gAnalysisDir + "/VCCRSPRESTART.INFO");
   VccRestartInfo restart_info(ifs);
   VccCalcDef vcc_calc_def;
   vcc_calc_def.SetMaxExciLevel(restart_info.ExciLevel());
   vcc_calc_def.SetModalLimits(restart_info.ModalsPerMode());
   vcc_calc_def.SetModalLimitsInput(true);
   std::vector<In> occ_vec(restart_info.ModalsPerMode().size());
   for(int i = 0; i < occ_vec.size(); ++i)
   {
      occ_vec[i] = 0;
   }
   vcc_calc_def.SetOccVec(occ_vec);
   OpDef opdef;

   Xvec xvec;
   xvec.ReInit(&vcc_calc_def, &opdef);
   auto mcr = xvec.GetModeCombiOpRange();
   
   // find largest weights
   const In nout = 5;
   std::vector<Nb> weights(nout);
   std::vector<In> addr(nout);
   spect_block.mWeights.AddressOfExtrema(addr, weights, nout, I_1); // I_1 ??

   std::ostringstream midas_weights;
   midas::stream::ScopedPrecision(3, midas_weights);
   std::ostringstream latex_weights;
   midas::stream::ScopedPrecision(3, latex_weights);
   midas_weights << std::fixed;
   latex_weights << std::fixed;

   for(int i = 0 ; i < nout; ++i)
   {
      midas_weights << weights[i] << " ";
      latex_weights << weights[i] << " ";
      In addrp1 = addr[i] + 1;
      In mcnr = xvec.FindModeCombiForInt(addrp1);
      // find modecombi
      const auto& modes = mcr.GetModeCombi(mcnr);
      const auto nmodes = modes.Size();
      // find exci level for modes
      std::vector<In> occ(nmodes);
      std::vector<In> occmax(nmodes);
      for (In ii=I_0;ii<nmodes;ii++)
      {
         occ[ii]      = I_0;
         In i_op_mode = modes.Mode(ii);
         In nmod      = vcc_calc_def.Nmodals(i_op_mode);
         occmax[ii]   = nmod-I_1;
      }
      std::string str = "LOWHIG";
      MultiIndex mi(occ, occmax, str, true);
      std::vector<In> idx(nmodes);
      In dafuk = addrp1 - xvec.ModeCombiAddress(mcnr);
      mi.IvecForIn(idx, dafuk);

      midas_weights << "(";
      for(int j = 0; j < nmodes; ++j)
      {
         midas_weights << modes.Mode(j) << ":" << idx[j];
         latex_weights << (idx[j]>1 ? std::to_string(idx[j]) : "") << "\\nu_{" << modes.Mode(j) << "}";
         if(j != (nmodes-1)) 
         {
            midas_weights << ", ";
            latex_weights << "\\, ";
         }
      }
      midas_weights << ")";
      if(i != (nout - 1)) 
      {
         midas_weights << " + ";
         latex_weights << " + ";
      }
   }

   Mout.Unmute(); // unmute Mout so we can print to it, in case os := Mout

   // print weights in a nice format
   // Midas: is a nicely read-able format.
   // Lates: can be copy pasted into latex for easy integration into documents/papers.
   os << "    Largest weights:\n"
      << "        Midas: " << midas_weights.str() << "\n"
      << "        Latex: " << latex_weights.str() << "\n"
      << "\n" << std::flush;

   return os;
}
