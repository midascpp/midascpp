/**
************************************************************************
* 
* @file                VscfWfAnalysis.cc
*
* Created:             15-10-2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Generated Wf and 1p density plots for a Vscf Wf
* 
* Last modified: Thu Oct 23, 2008  04:47PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
***********************************************************************
 * */

#include <string>
using std::string;
#include <fstream>
using std::ifstream;
using std::istringstream;

#include "input/Input.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/Plot.h"
#include "mmv/MidasVector.h"
#include "analysis/VscfWfAnalysis.h"

void PlotWaveFunc(const string& arFilename, bool aIsWf);
/**
* Main function for generating plots
* */
void VscfWaveFuncAnalysis()
{
   string file_wf=gAnalysisDir + "/VscfWvFunFiles";
   bool plot_wf=true;
   bool plot_dens=true;
   ifstream flist(file_wf.c_str(),ios_base::in);
   if (!flist) plot_wf=false; // Then no wf plots.
   string file_dens=gAnalysisDir + "/Vscf1pDensFiles";
   ifstream fdlist(file_dens.c_str(),ios_base::in);
   if (!fdlist) plot_dens=false; // Then no potential plots.
   if (!plot_dens && !plot_wf) return;

   if (plot_wf)
   {
      Mout << " Plotting occupied Vscf modals " << endl;
      string s;
      bool is_wf=true;
      while (getline(flist, s))
         PlotWaveFunc(s,is_wf);
      Mout << " Plotting of occupied Vscf modals finished " << endl;
   }
   if (plot_dens)
   {
      Mout << " Plotting occupied Vscf modals " << endl;
      string s;
      bool is_wf=false;
      while (getline(fdlist, s))
         PlotWaveFunc(s,is_wf);
      Mout << " Plotting of 1p densities finished " << endl;
   }
   if (plot_wf) flist.close();
   if (plot_dens) fdlist.close();
}
void PlotWaveFunc(const string& arFilename, bool aIsWf)
{
   ifstream datf(arFilename.c_str());
   if (!datf)
   {
      Error (" Error while reading file ");
      return;
   }
   //fed into coordinates and potential energy values
   vector<Nb> coord;
   vector<Nb> wf;
   string s;
   Nb q_coord;
   Nb wf_val;
   while(getline(datf,s))
   {
      istringstream s_input(s);
      s_input >> q_coord;
      s_input >> wf_val;
      coord.push_back(q_coord);
      wf.push_back(wf_val);
   }
   Plot spc(coord.size());
   // then put in MidasVector containers
   MidasVector q_values;
   MidasVector wf_values;
   q_values.SetNewSize(coord.size());
   wf_values.SetNewSize(coord.size());
   for (In i=0; i<coord.size(); i++)
   {
      q_values[i]=coord[i];
      wf_values[i]=wf[i];
   }
   //debug
   for (In i=0; i<q_values.Size(); i++)
      Mout << "  " << q_values[i] << '\t' << wf_values[i] << endl;
   Mout << endl;
   //Set x and y ranges.
   spc.SetXYvals(q_values, wf_values);
   Nb qmin =spc.GetMinX();
   Nb qmax =spc.GetMaxX();
   qmin -= C_I_10*fabs(qmin);
   qmax += C_I_10*fabs(qmax);
   Nb vmin = spc.GetMinY();
   Nb vmax = spc.GetMaxY();
   vmin -= C_I_10*fabs(vmin);
   vmax += C_I_10*fabs(vmax);
   spc.SetPlotRange(qmin, qmax, vmin, vmax);

   string datafile=arFilename;
   string datafile_sav=datafile;
   string::size_type loc=datafile.find("modal_");
   datafile.erase(0,loc);
   datafile.erase(0,6);
   string modal_nr=datafile;
   Mout << " modal_nr : " << modal_nr << endl;
   string qlabel="Q_{"+modal_nr+"}";
   spc.SetXlabel(qlabel);

   //Test on plot depends on type (bar or full potential).
   // find the Vscf calculation first
   string dir=gAnalysisDir + "/";
   datafile=arFilename;
   In s_sz=dir.size();
   loc=datafile.find("wf");
   if (loc==datafile.npos) loc=datafile.find("1pdens");
   datafile.erase(loc-1);
   datafile.erase(0,s_sz);
   Mout << " datafile: " << datafile << endl;
   string title_lab;
   title_lab="Vscf calculation: " + datafile + " for modal nr." + modal_nr;
   spc.SetTitle(title_lab);
   string y_lab= " Wave function ";
   if (!aIsWf) y_lab=" Density (a.u.) ";
   spc.SetYlabel(y_lab);
   if (!gNoPs) spc.MakePsFile(arFilename.c_str());
}
