/**
************************************************************************
* 
* @file                Analysis.h
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store Analysis declarations. 
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "inc_gen/TypeDefs.h"

/**
* declaration of Analysis functions. 
* */
void    Analysis();

#endif

