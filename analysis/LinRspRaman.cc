/**
************************************************************************
* 
* @file                LinRspRaman.cc
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Handle linear response Raman-spectra.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "LinRspRaman.h"

#include "libmda/util/pow.h"
#include "input/Input.h"
#include "analysis/ResponseAnalysisInterface.h"

/**
 * constructor
 **/
LinRspRaman::LinRspRaman(std::vector<LinRspRamanInput>&& input) 
 : mRspFunc(RspStringFromInput(input,RSP_TYPE::XXXX_IDX)
          , RspStringFromInput(input,RSP_TYPE::YYYY_IDX)
          , RspStringFromInput(input,RSP_TYPE::ZZZZ_IDX)
          , RspStringFromInput(input,RSP_TYPE::XXYY_IDX)
          , RspStringFromInput(input,RSP_TYPE::XXZZ_IDX)
          , RspStringFromInput(input,RSP_TYPE::YYZZ_IDX)
          , RspStringFromInput(input,RSP_TYPE::XYXY_IDX)
          , RspStringFromInput(input,RSP_TYPE::XZXZ_IDX)
          , RspStringFromInput(input,RSP_TYPE::YZYZ_IDX)
          )
 , mOperFrq(OperFrqFromInput(input))
{
   std::ifstream rsp_file(analysis::rsp().main_path);
   XXXX().AddRspFunc(rsp_file);
   YYYY().AddRspFunc(rsp_file);
   ZZZZ().AddRspFunc(rsp_file);
   XXYY().AddRspFunc(rsp_file);
   XXZZ().AddRspFunc(rsp_file);
   YYZZ().AddRspFunc(rsp_file);
   XYXY().AddRspFunc(rsp_file);
   XZXZ().AddRspFunc(rsp_file);
   YZYZ().AddRspFunc(rsp_file);
}

/**
 * get operator string from input
 **/
std::string LinRspRaman::OperString(const std::vector<LinRspRamanInput>& input, oper::PropertyType type) const
{
   for(size_t i=0; i<input.size(); ++i)
   {
      if(input[i].Type() == type)
      {
         return input[i].Name();
      }
   }

   return "Error";
}

/**
 *
 **/
std::string LinRspRaman::RspStringFromInput(const std::vector<LinRspRamanInput>& input, RSP_TYPE type) const
{
   std::string rsp_func = "<<";
   
   switch(type)
   {
      case RSP_TYPE::XXXX_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_xx);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
      case RSP_TYPE::YYYY_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_yy);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
      case RSP_TYPE::ZZZZ_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_zz);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
      case RSP_TYPE::XXYY_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_xx);
         std::string oper_2 = OperString(input, oper::polarizability_yy);
         rsp_func += oper_1 + ";" + oper_2;
         break;
      }
      case RSP_TYPE::XXZZ_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_xx);
         std::string oper_2 = OperString(input, oper::polarizability_zz);
         rsp_func += oper_1 + ";" + oper_2;
         break;
      }
      case RSP_TYPE::YYZZ_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_yy);
         std::string oper_2 = OperString(input, oper::polarizability_zz);
         rsp_func += oper_1 + ";" + oper_2;
         break;
      }
      case RSP_TYPE::XYXY_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_xy);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
      case RSP_TYPE::XZXZ_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_xz);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
      case RSP_TYPE::YZYZ_IDX:
      {
         std::string oper_1 = OperString(input, oper::polarizability_yz);
         rsp_func += oper_1 + ";" + oper_1;
         break;
      }
   }

   rsp_func += ">>";
   return rsp_func;
}

/**
 *
 **/
Nb LinRspRaman::OperFrqFromInput(const std::vector<LinRspRamanInput>& input) const
{
   MidasAssert(input.size()>0,"No input given");
   Nb frq = input[0].Frq();
   for(size_t i=1; i<input.size(); ++i)
   {
      MidasAssert(frq==input[i].Frq(),"Frequencies are not the same");
   }
   return frq;
}

/**
 * calculate Raman scattering spectra
 **/
void LinRspRaman::CalculateAbsorption()
{
   for(auto frq_it=XXXX().FrqBegin(); frq_it!=XXXX().FrqEnd(); ++frq_it)
   {
      Nb imag_xxxx_rsp = (XXXX().ImRsp(*frq_it));
      Nb imag_yyyy_rsp = (YYYY().ImRsp(*frq_it));
      Nb imag_zzzz_rsp = (ZZZZ().ImRsp(*frq_it));
      Nb imag_xxyy_rsp = (XXYY().ImRsp(*frq_it));
      Nb imag_xxzz_rsp = (XXZZ().ImRsp(*frq_it));
      Nb imag_yyzz_rsp = (YYZZ().ImRsp(*frq_it));
      Nb imag_xyxy_rsp = (XYXY().ImRsp(*frq_it));
      Nb imag_xzxz_rsp = (XZXZ().ImRsp(*frq_it));
      Nb imag_yzyz_rsp = (YZYZ().ImRsp(*frq_it));
      
      Nb alpha = (imag_xxxx_rsp 
                + imag_yyyy_rsp
                + imag_zzzz_rsp
                + 2*imag_xxyy_rsp
                + 2*imag_xxzz_rsp
                + 2*imag_yyzz_rsp
                );

      Nb ani = (3*(2*(imag_xyxy_rsp + imag_xzxz_rsp + imag_yzyz_rsp)
                  + imag_xxxx_rsp + imag_yyyy_rsp + imag_zzzz_rsp
                  ) 
              - alpha)/2.0;

      alpha /= 9.0;

      Nb raman_abs = -libmda::util::pow<4>(mOperFrq-(*frq_it)[1]) * (45.0*alpha + 7*ani) / 45.0;

      AddAbsorption(raman_abs, *frq_it); // add absorption for underlying RspSpectrum class
   }
}

/**
 *
 **/
void LinRspRaman::CalculateWeights()
{
   MIDASERROR("Not Implemented");
   //std::ifstream file(gAnalysisDir + "/VCCRSPRESTART.INFO");
   //VccRestartInfo restart_info(file);
   //auto size = restart_info.SizeOfVector();
   //
   //// standard way: |x_\nu|^2
   //std::function<void(Nb, DataCont&)> functor = [this,size](Nb frq, DataCont& w)
   //{
   //   w.Zero(); // first we zero out, as we add up later
   //   // <<x,x>>
   //   DataCont temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp+im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //   temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp-im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //   
   //   // <<y,y>>
   //   temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp+im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //   temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp-im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //   
   //   // <<z,z>>
   //   temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp+im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //   temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp-im",size);
   //   temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
   //   //Axpy(w, temp, C_1);
   //   Axpy(w, temp, frq);
   //};
   //
   //RspSpectrum::CalculateWeights(functor);
}

/**
 * plot the 'absorption' spectrum
 **/
void LinRspRaman::PlotSpectrum() const
{
   RspSpectrum::PlotSpectrum("rsp_raman_spec.dat","RAMAN ABS");
}

/**
 * plot real part of the response
 **/
void LinRspRaman::PlotReRsp() const
{
   int prec = 17;
   int width = 26;
   std::ofstream data_file("rsp_raman_real.dat");
   
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << "#REAL XXXX RSP"
                          << std::setw(width) << "#REAL YYYY RSP"
                          << std::setw(width) << "#REAL ZZZZ RSP" 
                          << std::setw(width) << "#REAL XXYY RSP" 
                          << std::setw(width) << "#REAL XXZZ RSP" 
                          << std::setw(width) << "#REAL YYZZ RSP" 
                          << std::setw(width) << "#REAL XYXY RSP" 
                          << std::setw(width) << "#REAL XZXZ RSP" 
                          << std::setw(width) << "#REAL YZYZ RSP" 
                          << "\n";

   midas::stream::ScopedPrecision(prec, data_file);
   data_file << std::scientific;

   for(auto frq_it=XXXX().FrqBegin(); frq_it!=XXXX().FrqEnd(); ++frq_it)
   {
      data_file << std::fixed << std::setw(width) << (*frq_it)[1]*C_AUTKAYS;
      
      Nb real_xxxx_rsp = (XXXX().ReRsp(*frq_it));
      Nb real_yyyy_rsp = (YYYY().ReRsp(*frq_it));
      Nb real_zzzz_rsp = (ZZZZ().ReRsp(*frq_it));
      Nb real_xxyy_rsp = (XXYY().ReRsp(*frq_it));
      Nb real_xxzz_rsp = (XXZZ().ReRsp(*frq_it));
      Nb real_yyzz_rsp = (YYZZ().ReRsp(*frq_it));
      Nb real_xyxy_rsp = (XYXY().ReRsp(*frq_it));
      Nb real_xzxz_rsp = (XZXZ().ReRsp(*frq_it));
      Nb real_yzyz_rsp = (YZYZ().ReRsp(*frq_it));
      
      
      data_file << std::scientific
                << std::setw(width) << real_xxxx_rsp 
                << std::setw(width) << real_yyyy_rsp 
                << std::setw(width) << real_zzzz_rsp 
                << std::setw(width) << real_xxyy_rsp 
                << std::setw(width) << real_xxzz_rsp 
                << std::setw(width) << real_yyzz_rsp 
                << std::setw(width) << real_xyxy_rsp 
                << std::setw(width) << real_xzxz_rsp 
                << std::setw(width) << real_yzyz_rsp 
                << "\n";
   }
}

/**
 * plot imaginary part of the rsp
 **/
void LinRspRaman::PlotImRsp() const
{
   int prec = 17;
   int width = 26;
   std::ofstream data_file("rsp_raman_imag.dat");
   
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << "#IMAG XXXX RSP"
                          << std::setw(width) << "#IMAG YYYY RSP"
                          << std::setw(width) << "#IMAG ZZZZ RSP" 
                          << std::setw(width) << "#IMAG XXYY RSP" 
                          << std::setw(width) << "#IMAG XXZZ RSP" 
                          << std::setw(width) << "#IMAG YYZZ RSP" 
                          << std::setw(width) << "#IMAG XYXY RSP" 
                          << std::setw(width) << "#IMAG XZXZ RSP" 
                          << std::setw(width) << "#IMAG YZYZ RSP" 
                          << "\n";
   
   midas::stream::ScopedPrecision(prec, data_file);
   data_file << std::scientific;

   for(auto frq_it=XXXX().FrqBegin(); frq_it!=XXXX().FrqEnd(); ++frq_it)
   {
      data_file << std::fixed << std::setw(width) << (*frq_it)[1]*C_AUTKAYS;
      
      Nb imag_xxxx_rsp = (XXXX().ImRsp(*frq_it));
      Nb imag_yyyy_rsp = (YYYY().ImRsp(*frq_it));
      Nb imag_zzzz_rsp = (ZZZZ().ImRsp(*frq_it));
      Nb imag_xxyy_rsp = (XXYY().ImRsp(*frq_it));
      Nb imag_xxzz_rsp = (XXZZ().ImRsp(*frq_it));
      Nb imag_yyzz_rsp = (YYZZ().ImRsp(*frq_it));
      Nb imag_xyxy_rsp = (XYXY().ImRsp(*frq_it));
      Nb imag_xzxz_rsp = (XZXZ().ImRsp(*frq_it));
      Nb imag_yzyz_rsp = (YZYZ().ImRsp(*frq_it));
      
      
      data_file << std::scientific
                << std::setw(width) << imag_xxxx_rsp 
                << std::setw(width) << imag_yyyy_rsp 
                << std::setw(width) << imag_zzzz_rsp 
                << std::setw(width) << imag_xxyy_rsp 
                << std::setw(width) << imag_xxzz_rsp 
                << std::setw(width) << imag_yyzz_rsp 
                << std::setw(width) << imag_xyxy_rsp 
                << std::setw(width) << imag_xzxz_rsp 
                << std::setw(width) << imag_yzyz_rsp 
                << "\n";
   }
}

/**
 * Plot a stick spectrum.
 **/
void LinRspRaman::PlotStick
   (
   ) const
{
   RspSpectrum::PlotStick("rsp_raman_stick.dat","RAMAN ABS");
}
