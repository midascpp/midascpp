/**
************************************************************************
* 
* @file                VscfDrv.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for MidasCPP Vscf calculations
 
* Last modified: Fri Mar 27, 2009  04:57PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <string>
#include <algorithm>

// links to headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "input/VscfCalcDef.h"
#include "inc_gen/Warnings.h"
#include "nuclei/Nuclei.h"
#include "analysis/StatMechanics.h"
#include "mmv/MidasVector.h"
#include "mmv/Diag.h"

using std::vector;
using std::string;

/**
*  Constructor of Statistical mechanics object
*  General scheme, other variables may be defined but
*  so far only for TD properties
**/
StatMechanics::StatMechanics(Nb aT) {
   mVibPartFunc=C_1;
   mRotPartFunc=C_1;
   mTrsPartFunc=C_1;
   mTemp=aT;
   mCoordinates.clear();
}

void StatMechanics::RotPartFunc() {
   // start by rotational part, assume asym. top
   MidasMatrix inertia(I_3,C_0);
   string file="NumDer.mmol";
   ifstream ifs;
   ifs.open(file.c_str());
   string line,units,extra;
   bool isotope=false;
   getline(ifs,line);
   istringstream iss(line);
   In nNuc=I_0;
   iss >> nNuc >> units >> extra;
   std::transform(extra.begin(),extra.end(),extra.begin(),(In(*) (In))toupper);
   if(extra=="ISOTOPES")
      isotope=true;
   Nb scal=C_1;
   std::transform(units.begin(),units.end(),units.begin(),(In(*) (In))toupper);;
   if(units=="AA")
      scal=C_1/C_TANG;
   // read in cartesian coordinates
   for(In i=I_0;i<nNuc;i++) {
      getline(ifs,line);
      istringstream iss1(line);
      In iso;
      Nb x,y,z;
      string type;
      if(isotope)
         iss1 >> type >> iso >> x >> y >> z;
      else
         iss1 >> type >> x >> y >> z;
      Nuclei n(x*scal,y*scal,z*scal,C_0,type);
      n.SetQfromGeneralLabel(type);
      if(isotope)
         n.SetAnuc(iso);
      mCoordinates.push_back(n);
   }
   // 0) Translate to COM
   vector<Nb> com_vector(I_3,C_0);
   Nb total_mass=C_0;
   for(In i=I_0;i<nNuc;i++) {
      com_vector[I_0]+=mCoordinates[i].GetMass()*mCoordinates[i].X();
      com_vector[I_1]+=mCoordinates[i].GetMass()*mCoordinates[i].Y();
      com_vector[I_2]+=mCoordinates[i].GetMass()*mCoordinates[i].Z();
      total_mass+=mCoordinates[i].GetMass();
   }
   com_vector[I_0]/=total_mass;
   com_vector[I_1]/=total_mass;
   com_vector[I_2]/=total_mass;
   for(In i=I_0;i<nNuc;i++) {
      mCoordinates[i].MoveTo(mCoordinates[i].X()-com_vector[I_0],
                      mCoordinates[i].Y()-com_vector[I_1],
                      mCoordinates[i].Z()-com_vector[I_2]);
   }
   // 1) set up inertia tensor
   for(In i=I_0;i<nNuc;i++) {
      inertia(I_0,I_0)+=mCoordinates[i].GetMass()*(mCoordinates[i].Y()*mCoordinates[i].Y()
                        +mCoordinates[i].Z()*mCoordinates[i].Z());
      inertia(I_1,I_1)+=mCoordinates[i].GetMass()*(mCoordinates[i].X()*mCoordinates[i].X()
                        +mCoordinates[i].Z()*mCoordinates[i].Z());
      inertia(I_2,I_2)+=mCoordinates[i].GetMass()*(mCoordinates[i].X()*mCoordinates[i].X()
                        +mCoordinates[i].Y()*mCoordinates[i].Y());
      inertia(I_0,I_1)+=mCoordinates[i].GetMass()*mCoordinates[i].X()*mCoordinates[i].Y();
      inertia(I_0,I_2)+=mCoordinates[i].GetMass()*mCoordinates[i].X()*mCoordinates[i].Z();
      inertia(I_1,I_2)+=mCoordinates[i].GetMass()*mCoordinates[i].Y()*mCoordinates[i].Z();
   }
   inertia(I_1,I_0)=inertia(I_0,I_1);
   inertia(I_2,I_0)=inertia(I_0,I_2);
   inertia(I_2,I_1)=inertia(I_1,I_2);
   // 2) diagonalize
   MidasMatrix e_vec(I_3,C_0);
   MidasVector e_val(I_3,C_0);
   string diag_method; 
   if (gNumLinAlg=="LAPACK")
      diag_method="DSYEVD";
   else
      diag_method="MIDAS_JACOBI";
   Diag(inertia,e_vec,e_val,diag_method);
   // Seidler note: Added a MidasVector cast to get file to compile on "g++ Red Hat 4.1.1-52".
   // e_val*const will return MidasVectorAx which has not << operator.
   Mout << "eigen values[u*angs^2] = " << MidasVector(e_val*(C_TANG*C_TANG)) << endl;
   Mout << "eigen values[u*bohr^2] = " << e_val << endl;
   // we are using atomic units => mass -> m_e
   e_val*=C_FAMU;
   Mout << "eigen values[m_e*bohr^2] = " << e_val << endl;
   // 3) compute rotation partition function
   //         following McQuarrie (SM, pp. 136 top) for
   //         asymmetric top!
   for(In i=I_0;i<e_val.Size();i++) {
      Nb tmp_res=C_2*C_KB*e_val[i]*mTemp;
      mRotPartFunc*=sqrt(tmp_res);
   }
   mRotPartFunc*=sqrt(C_PI);
}

/**
*  compute the translational partition function. Eventually
*  one should be able to specify pressure but for the moment
*  just set the standard 1 atm.
*  (McQuarrie, SM, pp. 82)
**/
void StatMechanics::TrsPartFunc() {
   Nb total_mass=C_0;
   for(In i=I_0;i<mCoordinates.size();i++) {
      total_mass+=mCoordinates[i].GetMass();
   }
   // FROM DALTON: QTRANS = TPFFAC*(TOTMAS**D1P5)*(TEMPI**D2P5)
   // Do it in SI units
   /*
   Nb mass_kg=total_mass/(C_10_3*C_MOL);
   Nb gas_const=C_KB*C_MOL*C_AUTJ;
   Nb tmp_res=C_2*C_PI*mass_kg*mTemp*(gas_const/C_MOL)/(C_PLANCK*C_PLANCK);
   tmp_res*=sqrt(tmp_res);
   tmp_res*=mTemp*gas_const/C_MOL;
   // divide by the pressure: [p] = N/m^2 = 1atm*101325Pa/atm
   tmp_res/=101325.0e0;
   Mout << "Trans PF = " << tmp_res << " R = " << gas_const << endl;
   //Mout << "Trans PF = " << tmp_res*sqrt(tmp_res) << " or = " << tmp_res*sqrt(tmp_res)*mTemp << endl;
   mTrsPartFunc=tmp_res;
   */
   total_mass*=C_FAMU;
   Nb tmp_res=C_KB*total_mass*mTemp/(C_2*C_PI);        
   // Assume a volume of 24.63 l
   Nb volume=24.63e0*C_I_10_3*pow((C_10_10/C_TANG),C_3);
   tmp_res*=sqrt(tmp_res);
   mTrsPartFunc=tmp_res*volume/C_MOL;
   Mout << "TRANS_PF = " << mTrsPartFunc << endl;
}

/**
*  Standard overload output operator
**/
ostream& operator<<(ostream& aOut, StatMechanics& aSm) {
   
   aOut << "Transl. PF = " << aSm.GetTrsPartFunc() << endl;
   aOut << "Rotatl. PF = " << aSm.GetRotPartFunc() << endl;
   aOut << "Vibrtl. PF = " << aSm.GetVibPartFunc() << endl;
   return aOut;
}
