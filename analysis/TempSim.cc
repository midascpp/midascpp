/**
************************************************************************
* 
* @file                TempSim.cc 
*
* Created:             04-11-2005
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Make simple temperatur effects 
* 
* Last modified: Fri Dec 15, 2006  01:07PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <utility>

// links to headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "analysis/TempSim.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
#include "util/Plot.h"
#include "analysis/VscfTempSim.h"
#include "util/MidasStream.h"

using std::vector;
using std::set;
using std::map;
using std::sort;
using std::remove_if;
using std::pair;

/**
* Simulation of temperatur effects
* */
void TemperatureSimulator() 
{
   // Check for different data, Vscf_ss
   In n_types=I_7;
   vector<string> types(n_types);
   types[I_0] ="Vscf_ss";
   types[I_1] ="Vscf_vir";
   types[I_2] ="Vci_gs_fc";
   types[I_3] ="Vci_ss";
   types[I_4] ="Vcc_ss";
   types[I_5] ="unknown";
   types[I_6] ="Vscf_fast_ave";

   Mout.setf(ios_base::uppercase);
   Mout.setf(ios::showpoint);
   for (In i_types=I_0;i_types<n_types;i_types++)
   {
      // for each potential methods construct an object to do temperatur averaging 
      TempSim temp_sim(types[i_types]);
      // If fast Vscf, set that logical to true
      if(types[i_types]=="Vscf_fast_ave") temp_sim.SetIsVscfFast(true);
      // if data exist do something, else try another/do nothing 
      if (temp_sim.mDataFound) 
      {
         // Read in energies or generate one-mode part funcs
         temp_sim.GetEnergies();

         Mout << "\n\n Collecting " << types[i_types] << " information for "<< 
            temp_sim.mNstates << " states and " << temp_sim.mNproperties << " properties " << endl;
   
         if (temp_sim.mNstates <= I_1 && !temp_sim.GetIsVscfFast()) 
         {
            Mout << " Averaging skipped for this type because the number of states = " << temp_sim.mNstates << endl;
            continue; 
         }
         // Loop over different temperatures requested. 
         Nb delta_t = C_0;
         if (gTempSteps>I_1) delta_t = (gTempLast-gTempFirst)/(gTempSteps-I_1);
         for (In i_t_step=I_0;i_t_step<gTempSteps;i_t_step++)
         {
            temp_sim.SetTemp(gTempFirst+delta_t*i_t_step,i_t_step);
            if(temp_sim.GetIsVscfFast())
               temp_sim.AddVscfFast(temp_sim.mBase);
            // Do actual calculations of partition functions, populations, and averaging 
            temp_sim.CalcPartitionFunc(i_t_step);
            Mout.unsetf(ios_base::scientific);
            midas::stream::ScopedPrecision(8, Mout); 
            Mout << " For T = " << temp_sim.GetTemp();
            Mout.setf(ios_base::scientific);
            midas::stream::ScopedPrecision(18, Mout); 
            Mout << " partition function = " << temp_sim.GetPartFunc() << endl;
            if(temp_sim.GetIsVscfFast()) temp_sim.CalcWeights();
            else temp_sim.CalcPopulations();
            if(!temp_sim.GetIsVscfFast()) // not implemented yet...
               temp_sim.CalcPropertyAverages(i_t_step);
            // mbh: do thermodynamic properties
            // NOTE: is energy always the first property?
            Nb curr_t=temp_sim.GetTemp();
            temp_sim.CalculateTD(curr_t,i_t_step);
         }
         // All data generated, write out appropriatetely 
         if(!temp_sim.GetIsVscfFast()) // not implemented yet...
            temp_sim.Summarize();
      }
   }
}
/**
*  Compute some thermodynamic functions, currently implemented is:
*  - Internal energy, U
*  - Entropy, S
*  - Helmholz free energy, A
*  - Heat capacity at const. volume, C_v
*
*  NOTE(mbh)!
*  Due to the use of relative energies in the TempSim part (this)
*  the formulas deviate slightly from the formulas provided in the
*  paper: JCP, xxx, yyyyyy (2008)
**/
void TempSim::CalculateTD(Nb aTemp, In aTstep) {
   Nb curr_t=aTemp;
   In i_t_step=aTstep;
   // reference energy is in BASE_info_Energy
   ifstream props(mEnergyInfoFile.c_str(),ios::in);
   string line;
   getline(props,line);
   string s1,s2,s3,s4;
   Nb ref_e=C_0;
   istringstream iss(line);
   iss >> s1 >> s2 >> s3 >> s4 >> ref_e;
   mIntEnergy[i_t_step]=mAverages[0][i_t_step];
   mEntropy[i_t_step]=C_KB*(log(mPartFunc)-ref_e/(C_KB*curr_t));
   mEntropy[i_t_step]+=mIntEnergy[i_t_step]/curr_t;
   mHelmholz[i_t_step]=(-C_KB*curr_t)*(log(mPartFunc)-ref_e/(C_KB*curr_t));
   Nb diff_e=mIntEnergy[i_t_step]-ref_e;
   mHeatCap[i_t_step]+=(diff_e*diff_e)*(C_M_1/(C_KB*curr_t*curr_t));
}
/**
* Construct the basis info for temperature effects studies 
* */
TempSim::TempSim(string aBase) :mAverages(I_0),mIntEnergy(I_0),mEntropy(I_0),mHelmholz(I_0),mHeatCap(I_0)
{
   mBase           = aBase;
   mInfoFile       = gAnalysisDir+"/"+mBase+"_Info_Prop";
   mEnergyInfoFile = gAnalysisDir+"/"+mBase+"_Info_Energy";
   mPropInfoFile   = gAnalysisDir+"/"+mBase+"_Info_Property_";
   mDataFound      = false;
   mNstates        = I_0;
   mNproperties    = I_0;
   mIsVscfFast           = false;
   mVscfFast.clear();

   ifstream info(mInfoFile.c_str(),ios::in);
   if (info >> mNstates >> mNproperties) 
   {
      mDataFound=true;
      Mout << " Temperature analysis with " << mNstates << " states and " << mNproperties 
         << " properties " << endl;
      mDataFound=true;
   }
   else
   {
      if (gAnalysisIoLevel>I_5) Mout << "Info file " << mInfoFile << " could not be read correct " << endl;
      return;
   }
   mAverages.SetNewSize(mNproperties+I_1,gTempSteps); 
   mIntEnergy.SetNewSize(gTempSteps); 
   mEntropy.SetNewSize(gTempSteps); 
   mHelmholz.SetNewSize(gTempSteps); 
   mHeatCap.SetNewSize(gTempSteps); 
   mTemperature.SetNewSize(gTempSteps);
   // number of proper+1 energy is average for gTempSteps temperaturs
   mPropStr.resize(mNproperties+I_1);
   string name = gAnalysisDir + "/"+mBase+"_StateConvergence";
   mOfstream_stream.open(name.c_str(),ios::out);
}
/**
* Read the energies from "Energy_Info" file 
* */
void TempSim::GetEnergies() 
{
   // mbh: mOfstream is now MidasStream
   MidasStreamBuf mOfstream_buf(mOfstream_stream);
   MidasStream mOfstream(mOfstream_buf);
   ifstream energies(mEnergyInfoFile.c_str(),ios::in);
   string s;
   getline(energies,s);
   if(mIsVscfFast) {
      getline(energies,s);
      getline(energies,s);
   }
   mEnergies.SetNewSize(mNstates);
   mOfstream << "\n\n State nr., relative energy in au. and in Kelvin "  << endl;
   mOfstream << " +========================================================+" << endl;
   mOfstream.setf(ios_base::scientific);
   mOfstream.setf(ios_base::uppercase);
   midas::stream::ScopedPrecision(15, mOfstream);
   mOfstream.setf(ios::showpoint);

   In i=I_0;
   Nb read_in;
   while(energies >> read_in) {mEnergies[i] = read_in; ++i;}
   if ((i != mNstates) && !mIsVscfFast) 
   {
      MIDASERROR("Unexpected number of energies found on Energy_Info file");
   }
   for (i=I_0;i<mNstates;++i) 
   {
      mOfstream << " " << setw(4) << i << " " << setw(23) << mEnergies[i] << " " 
         << setw(23) << mEnergies[i]*C_AUTK << endl;
   }
   mOfstream << " +========================================================+" << endl;
   energies.close();
}
/**
* Calculate partition function and output convergence 
* */
void TempSim::CalcPartitionFunc(In aTstep) 
{
   // mbh: mOfstream is now MidasStream
   MidasStreamBuf mOfstream_buf(mOfstream_stream);
   MidasStream mOfstream(mOfstream_buf);
   mOfstream << "\n\n Current T = " << GetTemp() << endl;
   mOfstream << " +====================================================================+" << endl;
   mOfstream << "\n\n Partition function -  energy, contributions and accumulated value " << endl;
   mOfstream << " +====================================================================+" << endl;
   Nb z=C_0;
   // mbh
   Nb c_v=C_0;
   In iter=I_0;
   if(mIsVscfFast) {
      for(In i=I_0;i<mVscfFast[mVscfFast.size()-I_1].GetNmodes();i++) {
         Nb cont=C_0;
         for(In j=I_0;j<=mVscfFast[mVscfFast.size()-I_1].GetNHoInMode(i);j++) {
            cont+=ExpMinusBetaE(mEnergies[iter]);
            if(gDebug) {
               Mout.setf(ios_base::scientific);
               midas::stream::ScopedPrecision(18, Mout);
               Mout << " E = " << right << setw(23) << mEnergies[iter] << " " << cont << endl;
            }
            iter++;
         }
         mVscfFast[mVscfFast.size()-I_1].SetOneModePartFunc(i,cont);
      }
      mPartFunc=mVscfFast[mVscfFast.size()-I_1].GetPartFunc();
   }
   else {
      for (In i=I_0;i<mNstates;i++)
      {
         Nb zcon=ExpMinusBetaE(mEnergies[i]);
         z+=zcon;
         c_v+=zcon*(mEnergies[i]*mEnergies[i]/(C_KB*mTempCurrent*mTempCurrent));
         mOfstream << " E = " << right << setw(23) << mEnergies[i] << " " << zcon << " " << z << endl;
      }
      mPartFunc=z;
      mHeatCap[aTstep]=c_v/mPartFunc;
   }
   mOfstream << " +====================================================================+" << endl;
}
/**
* Calculate partition function and output convergence 
* */
void TempSim::CalcPopulations() 
{
   // mbh: mOfstream is now MidasStream
   MidasStreamBuf mOfstream_buf(mOfstream_stream);
   MidasStream mOfstream(mOfstream_buf);
   mOfstream << "\n\n       Energy                             Population " << endl;
   mOfstream << " +====================================================================+" << endl;
   for (In i=I_0;i<mNstates;i++)
   {
      Nb zcon=ExpMinusBetaE(mEnergies[i]);
      string s="Pop_"+std::to_string(i);
      mOfstream << "     " << right << setw(23) << mEnergies[i] << " " << setw(10) << left << s << " " 
         << setw(23) << right << zcon/mPartFunc << endl;
   }
}
/**
* Calculate all property averages for a temperature number 
* */
void TempSim::CalcPropertyAverages(In aTemp) 
{
   for (In i_prop =I_0;i_prop<mNproperties;i_prop++) 
   {
      string name=mPropInfoFile+std::to_string(i_prop);
      ifstream props(name.c_str(),ios::in);
      mAverages[i_prop][aTemp]=CalcPropertyAverage(i_prop,props);
   }
   string name=mEnergyInfoFile;
   ifstream props(name.c_str(),ios::in);
   mAverages[mNproperties][aTemp] = CalcPropertyAverage(mNproperties,props);
}
/**
* Calculate property averagen and output converges
* */
void TempSim::GetProperty
   (  MidasVector& aPropVec
   ,  string& aLabel
   ,  In aPropertyNr
   ,  ifstream& arIfStream
   ) 
{
   getline(arIfStream, aLabel);
   if (aPropertyNr<mPropStr.size()) 
   {
      mPropStr[aPropertyNr]=aLabel;
   }
   else 
   {
      MIDASERROR("Unexpected property nr in property read in ");
   }
   aPropVec.SetNewSize(mNstates);
   In i=I_0;
   Nb read_in;
   while(arIfStream>> read_in) 
   {  
      aPropVec[i] = read_in; 
      ++i;
   }
   if (i!=mNstates) 
   {
      MIDASERROR("Unexpected number of properties found on file");
   }
}
/**
* Calculate property average - output convergence and return value 
* */
Nb TempSim::CalcPropertyAverage(In aPropertyNr,ifstream& arIfStream) 
{
   // mbh: mOfstream is now MidasStream
   MidasStreamBuf mOfstream_buf(mOfstream_stream);
   MidasStream mOfstream(mOfstream_buf);
   string name;
   MidasVector prop_vec;
   GetProperty(prop_vec,name,aPropertyNr,arIfStream);
   //Mout << "MBH: Prop_vec: " << endl << prop_vec << endl;
   mOfstream << "\n Temperature averaged Property " << name << endl; 
   mOfstream << "\n State      Property          P. Contrib.       Accum. unnorm. Accum. norm. Value" << endl; 
   mOfstream << " +==================================================================================+" << endl;
   Nb propave=C_0;
   Nb z = C_0;
   MidasVector state_convergence(mNstates);
   for (In i =I_0;i<mNstates;i++)
   {
      Nb zcon=ExpMinusBetaE(mEnergies[i]);
      z+=zcon;
      Nb propavecon=prop_vec[i]*zcon;
      propave+=propavecon;
      mOfstream << setw(8) << left << " P"+std::to_string(aPropertyNr)+"_"+std::to_string(i);
      midas::stream::ScopedPrecision(8, mOfstream);
      mOfstream << " " << right << setw(17) << prop_vec[i] << " " << right << setw(17) << propavecon << " "
         << right << setw(17) << propave<< " ";
      midas::stream::ScopedPrecision(16, mOfstream);
      mOfstream  << right <<  setw(22) << propave/z << endl;
      state_convergence[i]=propave/z;
   }
   mOfstream << " +==================================================================================+" << endl;

   bool plot_it=true;
   if (plot_it)
   {
      MidasVector xvals(mNstates);;
      for (In i=0;i<mNstates;i++) xvals[i]=i+1;
      Plot state_conv_plot(mNstates);
      state_conv_plot.SetXYvals(xvals,state_convergence);
      state_conv_plot.SetXlabel("States");
      state_conv_plot.SetTitle(name+" State Converge");
      state_conv_plot.SetYlabel("Property");
      state_conv_plot.SetPlotRangeRel(C_0,C_0,C_I_10_2,C_I_10_2);
      //state_conv_plot.SetRange(C_1,mNstates); 
      state_conv_plot.SetLineStyle("linesp");
      string file_name=gAnalysisDir+"/"+mBase+"_Prop_"+std::to_string(aPropertyNr)+"_State_Converge_T_"+StringForNb(mTempCurrent);
      if (!gNoPs) {state_conv_plot.MakePsFile(file_name);}
      else state_conv_plot.MakeGnuPlot(file_name);
   }

   return propave/z;
}
/**
* Calculate property average - output convergence and return value 
* */
void TempSim::Summarize()
{
   Mout << "\n\n Summarize temperature calculations " << endl;
   Mout << " Details on state convergence can be found in file\n "<< 
      gAnalysisDir+"/"+mBase+"_StateConvergence" << endl;
   Mout << " and a number of plot files\n " << gAnalysisDir+"/"+mBase+"_Prop_*_State_Convergence_T_*" << endl;
   In n_temps=mTemperature.Size();
   Mout.setf(ios_base::uppercase);
   Mout.setf(ios::showpoint);
   for (In i=0;i<mNproperties+I_1;i++) 
   {
      Mout << "\n\n +============================================================================+" << endl;
      Mout << "              Temperature         Property: " << mPropStr[i] << endl; 
      Mout << " +============================================================================+" << endl;
      Mout << right; 
      for (In i_t=0;i_t<n_temps;i_t++) 
      {
         Mout.unsetf(ios_base::scientific);
         midas::stream::ScopedPrecision(8, Mout);
         Mout << " " << setw(22) << mTemperature[i_t] << "   ";
         Mout.setf(ios_base::scientific);
         midas::stream::ScopedPrecision(16, Mout);
         Mout  << setw(30) << right << mAverages[i][i_t] << endl;
      }
      Mout << " +============================================================================+" << endl;

      MidasVector prop_vec(n_temps);
      mAverages.GetRow(prop_vec,i);

      if (n_temps>I_1)
      {
         Plot temp_plot(n_temps);
         //temp_plot.SetRange(mTemperature[I_0],mTemperature[n_temps-I_1]);
         temp_plot.SetXlabel("T/K");
         temp_plot.SetTitle(mPropStr[i]);
         temp_plot.SetYlabel("Property");
         temp_plot.SetXYvals(mTemperature,prop_vec);
         temp_plot.SetPlotRangeRel(C_0,C_0,C_I_10_2,C_I_10_2);
         temp_plot.SetLineStyle("linesp");
         temp_plot.SetSmooth("csplines");
         string file_name=gAnalysisDir+"/"+mBase+"_Prop_"+std::to_string(i);
         if (!gNoPs) {temp_plot.MakePsFile(file_name);}
         else temp_plot.MakeGnuPlot(file_name);
      }
   }
   // mbh: summarize TD proper
   Mout << "\n\n +============================================================================+" << endl;
   Mout << "  Thermodynamic properties (SI units)" << endl;
   Mout << " +============================================================================+" << endl;
   Mout << " T                        "; 
   Mout << "Internal energy, U (kJ/mol):" << endl;
   for (In i_t=0;i_t<n_temps;i_t++) {
      Mout.unsetf(ios_base::scientific);
      Mout << " " << setw(18) << mTemperature[i_t] << "       ";
      Mout.setf(ios_base::scientific);
      Mout << setw(22) << mIntEnergy[i_t]*C_AUTKJMOL << endl;
   }
   Mout << " T                        ";
   Mout << "Entropy, S (J/Kmol):" << endl;
   for (In i_t=0;i_t<n_temps;i_t++) {
      Mout.unsetf(ios_base::scientific);
      Mout << " " << setw(18) << mTemperature[i_t] << "       ";
      Mout.setf(ios_base::scientific);
      Mout << setw(22) << mEntropy[i_t]*C_AUTKJMOL*C_10_3 << endl;
   }
   Mout << " T                        "; 
   Mout << "Helmholz free-energy, A (kJ/mol):" << endl;
   for (In i_t=0;i_t<n_temps;i_t++) {
      Mout.unsetf(ios_base::scientific);
      Mout << " " << setw(18) << mTemperature[i_t] << "       ";
      Mout.setf(ios_base::scientific);
      Mout << setw(22) << mHelmholz[i_t]*C_AUTKJMOL << endl;
   }
   Mout << " T                        "; 
   Mout << "Heat Capacity (Const. V), C_v (J/Kmol):" << endl;
   for (In i_t=0;i_t<n_temps;i_t++) {
      Mout.unsetf(ios_base::scientific);
      Mout << " " << setw(18) << mTemperature[i_t] << "       ";
      Mout.setf(ios_base::scientific);
      Mout << setw(22) << mHeatCap[i_t]*C_AUTKJMOL*C_10_3 << endl;
   }
   Mout << " +============================================================================+" << endl;
   Mout << endl<<endl;
}
/**
* Calculate property average - output convergence and return value 
* */
Nb TempSim::ExpMinusBetaE(Nb& arE)
{
   if (!(GetTemp()<C_NB_EPSILON)) 
      return exp(-mBeta*arE);
   else if (arE<C_NB_EPSILON) 
      return C_1;
   else 
      return C_0;
}
/**
* Calculate the weights of the individual states when using the new Vscf temp sim
* implementation
**/
void TempSim::CalcWeights() {
   In iter=I_0;
   In aI=mVscfFast.size()-I_1;
   for(In i=I_0;i<mVscfFast[aI].GetNmodes();i++) {
      Nb sum_weight=C_0;
      for(In j=I_0;j<=mVscfFast[aI].GetNHoInMode(i);j++) {
         Nb weight=ExpMinusBetaE(mEnergies[iter])/mVscfFast[aI].GetOneModePartFunc(i);
         mVscfFast[aI].SetWeight(iter,weight);
         sum_weight+=weight;
         if(gDebug)
            Mout << "exp(-b*Energy) = " << ExpMinusBetaE(mEnergies[iter]) << " one-mode PF = " 
                 << mVscfFast[aI].GetOneModePartFunc(i) << " weight = " << weight << " Accumulated = " 
                 << sum_weight << endl; 
         iter++;
      }
   }
}
