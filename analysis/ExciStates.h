/**
************************************************************************
* 
* @file                ExciStates.h
*
* Created:             19-02-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Holds info on excited states
* 
* Last modified: man mar 21, 2005  10:56
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef EXCISTATES_H
#define EXCISTATES_H

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"
#include"util/Property.h"

/**
* Class ExciStates: 
* */
class ExciStates
{
   private:
      In mSym;                                                  
      In mSpin;                                                 
      In mNumber;                                               
      string mModel;
   public:
      ExciStates(In aSym,In aSpin,In aNumber,string aModel="")
          {mSym=aSym;mSpin=aSpin;mNumber=aNumber;mModel=aModel;}
      ExciStates(Property& arProp);
      In Sym() const {return mSym;}
      In Spin() const {return mSpin;}
      In Number() const {return mNumber;}
      string Model() const {return mModel;}
      bool NotZeroState() const {return mSpin!=I_0&&mSym!=I_0;}
      friend bool operator<(const ExciStates& ar1,const ExciStates& ar2); 
      friend bool operator==(const ExciStates& ar1,const ExciStates& ar2); 
           ///< Overload comparison
};

#endif
