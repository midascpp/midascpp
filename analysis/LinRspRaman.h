/**
************************************************************************
* 
* @file                LinRspRaman.h
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Handle linear response Raman-spectra.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LINRSPRAMAN_H_INCLUDED
#define LINRSPRAMAN_H_INCLUDED

#include <vector>
#include <string>
#include <tuple>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Range.h"
#include "input/CombinedRspFunc.h"
#include "input/OpInfo.h"
#include "input/LinRspRamanInput.h"
#include "analysis/RspSpectrum.h"

#define MAKE_RSP_GET(NAME,POS) \
   CombinedRspFunc& NAME() \
   { \
      return std::get<POS>(mRspFunc); \
   } \
   const CombinedRspFunc& NAME() const \
   { \
      return std::get<POS>(mRspFunc); \
   } 

/**
 * Class for handling calculation of Raman spectra using linear response.
 **/
class LinRspRaman
   : public RspSpectrum
{
   private:
      //const Range<Nb> mFrqRange;
      const std::vector<std::string> mRspFuncString;
      std::tuple<CombinedRspFunc,CombinedRspFunc,CombinedRspFunc
               , CombinedRspFunc,CombinedRspFunc,CombinedRspFunc
               , CombinedRspFunc,CombinedRspFunc,CombinedRspFunc> mRspFunc;
      Nb mOperFrq;

      enum RSP_TYPE { XXXX_IDX=0, YYYY_IDX, ZZZZ_IDX, XXYY_IDX, XXZZ_IDX, YYZZ_IDX, XYXY_IDX, XZXZ_IDX, YZYZ_IDX };
      
      MAKE_RSP_GET(XXXX,RSP_TYPE::XXXX_IDX);
      MAKE_RSP_GET(YYYY,RSP_TYPE::YYYY_IDX);
      MAKE_RSP_GET(ZZZZ,RSP_TYPE::ZZZZ_IDX);
      MAKE_RSP_GET(XXYY,RSP_TYPE::XXYY_IDX);
      MAKE_RSP_GET(XXZZ,RSP_TYPE::XXZZ_IDX);
      MAKE_RSP_GET(YYZZ,RSP_TYPE::YYZZ_IDX);
      
      MAKE_RSP_GET(XYXY,RSP_TYPE::XYXY_IDX);
      MAKE_RSP_GET(XZXZ,RSP_TYPE::XZXZ_IDX);
      MAKE_RSP_GET(YZYZ,RSP_TYPE::YZYZ_IDX);
      
      std::string OperString(const std::vector<LinRspRamanInput>&, oper::PropertyType) const;
      std::string RspStringFromInput(const std::vector<LinRspRamanInput>&, RSP_TYPE) const;
      Nb OperFrqFromInput(const std::vector<LinRspRamanInput>&) const;

   public:
      ///> Constructor
      LinRspRaman(std::vector<LinRspRamanInput>&& input);
      
      ///> Calculate absorptions
      void CalculateAbsorption();

      ///> Calculate configuration weights
      void CalculateWeights();
      
      ///> Plotting stuff
      void PlotSpectrum() const;
      void PlotReRsp() const;
      void PlotImRsp() const;
      void PlotStick() const;
};

#endif /* LINRSPRAMAN_H_INCLUDED */
