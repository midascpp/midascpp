/**
************************************************************************
* 
* @file                RspSpectrum.h
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   General base class to handle response spectra.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RSPSPECTRUM_H_INCLUDED
#define RSPSPECTRUM_H_INCLUDED

#include <iostream>
#include <utility>
#include <functional>

#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"

/**
 * General base class to handle different kinds of spectra calculated from response functions.
 **/
class RspSpectrum
{
   private:
      ///> Internal class to handle intervals of a spectrum containing a 'peak'.
      struct SpectrumBlock
      {
         public:
            using index_frq_t = std::pair<Nb, In>;
            using peak_t = std::pair<Nb, Nb>;
            using peak_cont_t = std::vector<peak_t>;
      
         public:
            index_frq_t mBlockBegin = std::make_pair(-1.0, -1); ///< Beginning of block
            index_frq_t mBlockEnd   = std::make_pair(-1.0, -1); ///< End of block
            Nb mIntegratedIntensity = 0.0;    ///< Integrated intensity of whole peak
            Nb mMaxIntegratedIntensity = 0.0; ///< integrated intensity of whole peak ?
            peak_t mPeak = std::make_pair(-1.0,-1.0);; ///< The 'peak' position and intensity at max absorption
            DataCont mWeights; ///< Configuration weights of 'peak'
            std::string mType;
            
            ///> Check if a 'peak' has been assigned (this can be a maximum or a saddle-point).
            bool IsPeakAssigned() const { return (mPeak != std::make_pair(-1.0,-1.0)); }
      };
      
      ///> Befriend output operator for SpectrumBlock's
      friend std::ostream& operator<<(std::ostream& os, const RspSpectrum::SpectrumBlock& spect_block);

      // actual data
      std::vector<std::vector<Nb> >  mFrqs; ///< Discrete frequency grid
      std::vector<Nb>                mAbs;  ///< Discrete absorption at each point in frequency grid
      // interpolated data
      std::vector<std::vector<Nb> >  mInterpolFrqs; ///< Interpolated frequency grid
      std::vector<Nb>                mInterpolAbs;  ///< Interpolated absorption
      // analysis data
      std::vector<SpectrumBlock> mPeaks; ///< Vector of identified peaks
      Nb mMaxAbs                 = 0.0;  ///< Largest absorption in the spectrum
      Nb mMaxIntegratedIntensity = 0.0;  ///< Max intensity
      In mSpectrumAnalysisPoints = 1;    ///< Number of points used in numerical derivatives
      Nb mInterpolateStep        = 0.1;  ///< Interpolation step lenght. Default = 0.1 cm^-1.
      
      ///> Get frequency for peak i
      Nb PeakFrq(int i) const
      {
         return mPeaks[i].mPeak.first;
      }

      ///> Get absorption for peak i
      Nb PeakAbs(int i) const
      {
         return mPeaks[i].mPeak.second;
      }
      
      ///> For a given frequency, find the nearest in a discrete set of frequencies.
      SpectrumBlock::index_frq_t FindNearestFrq(const Nb, const std::vector<std::vector<Nb> >&) const;
      
      ///> Implementation of IdentifyPeaks
      void IdentifyPeaksImpl(const std::vector<std::vector<Nb> >&, const std::vector<Nb>&);

      ///> Implementation of CalculateIntegratedIntensity
      void CalculateIntegratedIntensityImpl(const std::vector<std::vector<Nb> >&, const std::vector<Nb>&);

      ///> Implementation of CalculateWeights
      void CalculateWeightsImpl(const std::function<void(Nb, DataCont&)>&, const std::vector<std::vector<Nb> >&);

      ///> Implementation of PlotSpectrum
      void PlotSpectrumImpl(std::ofstream&, const std::vector<std::vector<Nb> >&, const std::vector<Nb>&) const;
         
      ///> Get frequencies
      const std::vector<std::vector<Nb> >& GetFrqs() const;

      ///> Get absorptions
      const std::vector<Nb>& GetAbs() const;

   public:
      ///> Default constructor
      RspSpectrum();
      
      ///> Add a point to the spectrum
      void AddAbsorption(Nb abs, const std::vector<Nb>& frq);
      
      ///> Interpolate the spectrum
      void Interpolate();

      ///> Identify peaks of the spectrum
      void IdentifyPeaks();

      ///> Analyse the spectrum
      void AnalyzeSpectrum();

      ///> Calculate weights of configurations
      void CalculateWeights(const std::function<void(Nb, DataCont&)>&);
      
      ///> Plotting stuff
      void PlotSpectrum (const std::string&, const std::string&) const;
      void PlotStick    (const std::string&, const std::string&) const;
      void PlotStickMin (const std::string&, const std::string&) const;

      ///> Print nice output to an output stream (e.g. Mout)
      void PrintPeakPositionsAndIntensities(std::ostream& os) const;
      
      ///> Set number of points used for numerical derivatives
      void SetSpectrumAnalysisPoints(In i) { mSpectrumAnalysisPoints = i; }
      
      ///> Set interpolation step length
      void SetSpectrumAnalysisInterpolateStep(Nb step) { mInterpolateStep = step; }
};

///> output operator for SpectrumBlock
std::ostream& operator<<(std::ostream& os, const RspSpectrum::SpectrumBlock& spect_block);

#endif /* RSPSPECTRUM_H_INCLUDED */
