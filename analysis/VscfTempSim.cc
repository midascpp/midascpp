/**
************************************************************************
* 
* @file                VscfTempSim.cc 
*
* Created:             24-07-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Make simple temperatur effects 
* 
* Last modified:       Mon, 24-07-2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
using std::vector;
#include <string>
using std::string;
using std::getline;
#include <fstream>
using std::ifstream;
#include <sstream>
using std::istringstream;
#include <set>
using std::set;
#include <map>
using std::map;
#include <algorithm>
using std::sort;
using std::remove_if;
#include <utility>
using std::pair;
//using std::make_pair;

// links to headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
#include "util/Plot.h"
#include "analysis/VscfTempSim.h"

/**
* Routines for doing the Vscf Temperature simulation
* */
VscfTempSim::VscfTempSim(string aS)
{
   // Initialize

   mHoPrMode.clear();
   mOneModePartFunc.clear();
   mWeights.clear();
   mProperties.clear();
   mPartFunc=C_1;

   if(aS!="Vscf_fast_ave") return;

   // get the number of HOs pr mode
  
   string file=gAnalysisDir+"/"+aS+"_Info_Energy"; 
   ifstream ifs(file.c_str(),ios::in);
   string line;
   getline(ifs,line);
   getline(ifs,line);
   istringstream iss(line);
   In n_modes;
   if(iss >> n_modes) {
      if(gDebug)
         Mout << "The number of modes to be used in calculation: " << n_modes << endl;
   }
   In n_modals=I_0;
   for(In i=I_0;i<n_modes;i++) {
      In n_ho=I_0;
      if(ifs >> n_ho) {
         if(gDebug)
            Mout << "Adding " << n_ho << " to mode " << i << endl;
         mHoPrMode.push_back(n_ho);
         n_modals+=(n_ho+I_1);
      }
      else
         MIDASERROR("Information missing in"+file+", Quitting!");
   }
   ifs.close();

   if(gDebug)
      Mout << "Harm. osc. pr. mode: " << endl << mHoPrMode << endl;

   mOneModePartFunc.assign(n_modes,C_0);
   mWeights.assign(n_modals,C_0);
   mProperties.clear();
}
