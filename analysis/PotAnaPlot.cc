/**
************************************************************************
* 
* @file                PotAnaPlot.cc
*
* Created:             27-05-2007
*
* Author:              Daniele Toffoli
*
* Short Description:   Generating Potential Plot and analysis
* 
* Last modified: Thu Oct 23, 2008  04:45PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <fstream>

// midas headers
#include "input/Input.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Plot.h"
#include "util/Plot2D.h"
#include "analysis/PotAnaPlot.h"

// using declarations
using std::string;
using std::ifstream;
using std::istringstream;

void Plot1DCuts(const string& arFilename);
void Plot2DCuts(const string& arFilename);

/**
 * Main function for generating spectra.
 **/
void PotentialAnalysis
   (
   )
{
   std::string filename = gAnalysisDir + "/" + "ListofPlots";
   std::ifstream flist(filename, ios_base::in);
  
   // If the ListofPlots file is empty, then there are no plots to make
   if (flist.peek() == std::ifstream::traits_type::eof())
   {
      return;
   }
   Mout << " Plotting potential plots" << std::endl;
  
   std::string datafile;
   while (std::getline(flist, datafile))
   {
      std::string::size_type loc = datafile.find("m2");
      if (loc!=string::npos) // this is a bidimensional cut
      {
         Plot2DCuts(datafile);
      }
      else
      {
         Plot1DCuts(datafile);
      }
   }
   Mout << " Potential plotting finished " << std::endl;
}

/**
 * Plot 1d cuts.
 *
 * @param arFilename  File with datapoints.
 **/
void Plot1DCuts
   (  const string& arFilename
   )
{
   ifstream datf(arFilename.c_str());
   if (!datf)
   {
      MIDASERROR(" Error while reading file " + arFilename);
      return;
   }
   //fed into coordinates and potential energy values
   vector<Nb> coord;
   vector<Nb> pot;
   string s;
   Nb q_coord;
   Nb pot_val;
   while(getline(datf,s))
   {
      istringstream s_input(s);
      s_input >> q_coord;
      s_input >> pot_val;
      coord.push_back(q_coord);
      pot.push_back(pot_val);
   }
   Plot spc(coord.size());
   // then put in MidasVector containers
   MidasVector q_values;
   MidasVector pot_values;
   q_values.SetNewSize(coord.size());
   pot_values.SetNewSize(coord.size());
   for (In i=0; i<coord.size(); i++)
   {
      q_values[i]=coord[i];
      pot_values[i]=pot[i];
   }
   //debug
   //for (In i=0; i<q_values.Size(); i++)
      //Mout << "  " << q_values[i] << "    " << pot_values[i] << endl;
   //Mout << endl;
   //Set x and y ranges.
   spc.SetXYvals(q_values, pot_values);
   Nb qmin =spc.GetMinX();
   Nb qmax =spc.GetMaxX();
   qmin -= C_I_10*fabs(qmin);
   qmax += C_I_10*fabs(qmax);
   Nb vmin = spc.GetMinY();
   Nb vmax = spc.GetMaxY();
   vmin -= C_I_10*fabs(vmin);
   vmax += C_I_10*fabs(vmax);
   spc.SetPlotRange(qmin, qmax, vmin, vmax);

   string datafile=arFilename;
   string datafile_sav=datafile;
   string::size_type loc=datafile.find("m1");
   datafile.erase(0,loc);
   datafile.erase(0,3);
   string::size_type loc1=datafile.find(".");
   string mode_nr=datafile.substr(0,loc1);
   //Mout << " mode_nr : " << mode_nr << endl;
   mode_nr.erase(loc1,1);
   //Mout << " Mode number is " << mode_nr << endl;
   string qlabel="Q_{"+mode_nr+"}";
   spc.SetXlabel(qlabel);

// Text on plot depends on type (bar or full potential).
   loc=datafile_sav.find("bar");
   string title_lab;
   if (loc!=string::npos)
      title_lab="Vbar("+qlabel+") potential";
   else 
      title_lab="V("+qlabel+") potential";

   if (datafile_sav.find("fit")!=string::npos)
      title_lab+=" fitted";
   else if (datafile_sav.find("int")!=string::npos)
      title_lab+=" interpolated";
   else
      title_lab+=" on grid";

   spc.SetTitle(title_lab);
   spc.SetYlabel("Potential (a.u.)");
   spc.MakePsFile(arFilename.c_str());
}

/**
 * Plot 2d cuts.
 *
 * @param arFilename  File with datapoints.
 **/
void Plot2DCuts
   (  const string& arFilename
   )
{
   ifstream datf(arFilename.c_str());
   if (!datf)
   {
      MIDASERROR(" Error while reading file " + arFilename);
      return;
   }
   //fed into coordinates and potential energy values
   vector<Nb> coord1;
   vector<Nb> coord2;
   vector<Nb> pot;
   string s;
   Nb q1_coord;
   Nb q2_coord;
   Nb pot_val;
   while(getline(datf,s))
   {
      istringstream s_input(s);
      s_input >> q1_coord;
      s_input >> q2_coord;
      s_input >> pot_val;
      coord1.push_back(q1_coord);
      coord2.push_back(q2_coord);
      pot.push_back(pot_val);
   }
   Plot2D spc(coord1.size());
   // then put in MidasVector containers
   MidasVector q1_values;
   MidasVector q2_values;
   MidasVector pot_values;
   q1_values.SetNewSize(coord1.size());
   q2_values.SetNewSize(coord1.size());
   pot_values.SetNewSize(coord1.size());
   for (In i=0; i<coord1.size(); i++)
   {
      q1_values[i]=coord1[i];
      q2_values[i]=coord2[i];
      pot_values[i]=pot[i];
   }
   //debug
   //for (In i=0; i<q_values.Size(); i++)
      //Mout << "  " << q_values[i] << "    " << pot_values[i] << endl;
   //Mout << endl;
   //Set x and y ranges.
   spc.SetXYZvals(q1_values, q2_values, pot_values);
   Nb q1min =spc.GetMinX();
   Nb q1max =spc.GetMaxX();
   q1min -= C_I_10*fabs(q1min);
   q1max += C_I_10*fabs(q1max);
   Nb q2min =spc.GetMinY();
   Nb q2max =spc.GetMaxY();
   q2min -= C_I_10*fabs(q2min);
   q2max += C_I_10*fabs(q2max);

   Nb vmin = spc.GetMinZ();
   Nb vmax = spc.GetMaxZ();
   vmin -= C_I_10*fabs(vmin);
   vmax += C_I_10*fabs(vmax);
   spc.SetPlotRange(q1min, q1max, q2min, q2max, vmin, vmax);

   string datafile=arFilename;
   string datafile_sav=datafile;
   string::size_type loc=datafile.find("m1");
   datafile.erase(0,loc);
   datafile.erase(0,3);
   string::size_type loc1=datafile.find(".");
   string mode_nr=datafile.substr(0,loc1);
   //Mout << " mode_nr : " << mode_nr << endl;
   mode_nr.erase(loc1,1);
   //Mout << " Mode number is " << mode_nr << endl;
   string qlabel1="Q_{"+mode_nr+"}";
   spc.SetXlabel(qlabel1);

   loc=datafile.find("m2");
   datafile.erase(0,loc);
   datafile.erase(0,3);
   loc1=datafile.find(".");
   mode_nr=datafile.substr(0,loc1);
   //Mout << " mode_nr : " << mode_nr << endl;
   mode_nr.erase(loc1,1);
   //Mout << " Mode number is " << mode_nr << endl;
   string qlabel2="Q_{"+mode_nr+"}";
   spc.SetYlabel(qlabel2);
   //Text on plot depends on type (bar or full potential).
   loc=datafile_sav.find("bar");
   string title_lab;
   if (loc!=string::npos)
      title_lab="Vbar("+qlabel1+qlabel2+") potential";
   else 
      title_lab="V("+qlabel1+qlabel2+") potential";
   //loc=datafile_sav.find("fit");
   if (datafile_sav.find("fit")!=string::npos)
      title_lab+=" fitted";
   else if (datafile_sav.find("int")!=string::npos)
      title_lab+=" interpolated";
   else 
      title_lab+=" on grid";

   spc.SetTitle(title_lab);
   spc.SetZlabel("Potential (a.u.)");
   spc.MakePsFile(arFilename.c_str());
}
