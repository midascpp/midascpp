/**
************************************************************************
* 
* @file                VscfWfAnalysis.h
*
* Created:             15-10-2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Generated Wf and 1p density plots for a Vscf Wf
* 
* Last modified:       15-10-2007
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VSCFWFANA_H
#define VSCFWFANA_H

#include "inc_gen/TypeDefs.h"
#include "analysis/PotAnaPlot.h"

/**
* Main function for generating plotting
* */
void VscfWaveFuncAnalysis();

#endif

