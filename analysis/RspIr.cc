/**
************************************************************************
* 
* @file                RspIr.cc
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Handle response IR-spectra.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "RspIr.h"

#include <iostream>

#include "inc_gen/Const.h"
#include "input/Input.h"
#include "analysis/ResponseAnalysisInterface.h"
#include "analysis/VccRestartInfo.h"
#include "util/Path.h"

using namespace midas;

/**
 * Constructor from a range of frequencies and a vector of response function names.
 * @param range      Frequency range.
 * @param rsp        Vector of response function strings, e.g. ("<<x;x>>", "<<y;y;>>", "<<z;z>>").
 * @param aPrefix    Analysis directory prefix.
 **/
RspIr::RspIr
   (  Range<Nb>&& range
   ,  std::vector<std::string>&& rsp
   ,  const std::string& aPrefix
   )
   :  mFrqRange(std::move(range))
   ,  mRsp(std::move(rsp))
   ,  mPrefix(aPrefix)
   ,  mXYZRsp(mRsp[0], mRsp[1], mRsp[2])
{
   assert(mRsp.size() == 3);
   if(!midas::filesystem::Exists(analysis::rsp().main_path))
   {
      MIDASERROR("File '" + analysis::rsp().main_path + "' does not exist.");
   }
   std::ifstream rsp_file(analysis::rsp().main_path);
   XX().AddRspFunc(rsp_file);
   YY().AddRspFunc(rsp_file);
   ZZ().AddRspFunc(rsp_file);
}

/**
 * Calculate IR absorption spectra.
 **/
void RspIr::CalculateAbsorption
   (
   )
{
   for(auto frq_it=XX().FrqBegin(); frq_it!=XX().FrqEnd(); ++frq_it)
   {
      Nb x_abs = -(*frq_it)[1]*(XX().ImRsp(*frq_it));
      Nb y_abs = -(*frq_it)[1]*(YY().ImRsp(*frq_it));
      Nb z_abs = -(*frq_it)[1]*(ZZ().ImRsp(*frq_it));
      Nb ir_abs = x_abs + y_abs + z_abs;

      AddAbsorption(ir_abs, *frq_it); // add absorption for underlying RspSpectrum class
   }
}

/**
 * Calculate weights of each excitation in all peaks.
 **/
void RspIr::CalculateWeights
   (
   )
{
   std::ifstream file(path::Join(mPrefix, "/VCCRSPRESTART.INFO"));
   VccRestartInfo restart_info(file);
   auto size = restart_info.SizeOfVector();
   
   // standard way: |x_\nu|^2
   std::function<void(Nb, DataCont&)> functor = [this,size](Nb frq, DataCont& w)
   {
      w.Zero(); // first we zero out, as we add up later
      // <<x,x>>
      DataCont temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
      temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
      
      // <<y,y>>
      temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
      temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
      
      // <<z,z>>
      temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
      temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      Axpy(w, temp, frq);
   };
   
   // another way of calculating weight: \eta_\nu * x_\nu (not really working/tested)
   std::function<void(Nb, DataCont&)> functor2 = [this,size](Nb frq, DataCont& w)
   {
      //Mout << " I : " << i << std::endl;
      w.Zero(); // first we zero out, as we add up later
      // <<x,x>>
      DataCont eta = XX().DataContFromFrqAndLabel(frq,"x-_eta",size);
      //Mout << " ETA X SIZE: " << eta.Size() << std::endl;
      //Mout << " ETA X: " << eta << std::endl;
      DataCont temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
      temp = XX().DataContFromFrqAndLabel(frq,"x-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
      
      // <<y,y>>
      eta = YY().DataContFromFrqAndLabel(frq,"y-_eta",size);
      //Mout << " ETA Y SIZE: " << eta.Size() << std::endl;
      //Mout << " ETA Y: " << eta << std::endl;
      temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
      temp = YY().DataContFromFrqAndLabel(frq,"y-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
      
      // <<z,z>>
      eta = ZZ().DataContFromFrqAndLabel(frq,"z-_eta",size);
      //Mout << " ETA Z SIZE: " << eta.Size() << std::endl;
      //Mout << " ETA Z: " << eta << std::endl;
      temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp+im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
      temp = ZZ().DataContFromFrqAndLabel(frq,"z-_p1rsp-im",size);
      temp.HadamardProduct(temp, C_1, DataCont::Product::Normal);
      temp.HadamardProduct(eta, C_1, DataCont::Product::Normal);
      //Axpy(w, temp, C_1);
      Axpy(w, temp, frq);
   };

   RspSpectrum::CalculateWeights(functor);
}

/**
 * Plot ir spectrum.
 **/
void RspIr::PlotSpectrum
   (
   ) const
{
   RspSpectrum::PlotSpectrum(path::Join(mPrefix, "rsp_ir_spec.dat"), "IR ABS");
}

/**
 * Plot real part of <<X;X>>, <<Y;Y>> and <<Z;Z>> rsp.
 **/
void RspIr::PlotReRsp
   (
   ) const
{
   int prec = 17;
   int width = 26;
   std::ofstream data_file(path::Join(mPrefix, "rsp_real.dat"));

   
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << "#REAL X RSP"
                          << std::setw(width) << "#REAL Y RSP"
                          << std::setw(width) << "#REAL Z RSP" << "\n";
   
   midas::stream::ScopedPrecision(prec, data_file);
   data_file << std::scientific;

   for(auto frq_it=XX().FrqBegin(); frq_it!=XX().FrqEnd(); ++frq_it)
   {
      Nb real_x_rsp = (XX().ReRsp(*frq_it));
      Nb real_y_rsp = (YY().ReRsp(*frq_it));
      Nb real_z_rsp = (ZZ().ReRsp(*frq_it));
      
      data_file << std::fixed << std::setw(width) << (*frq_it)[1]*C_AUTKAYS;
      
      data_file << std::scientific
                << std::setw(width) << real_x_rsp 
                << std::setw(width) << real_y_rsp 
                << std::setw(width) << real_z_rsp << "\n";
   }
}

/**
 * Plot imag part of <<X;X>>, <<Y;Y>> and <<Z;Z>> rsp.
 **/
void RspIr::PlotImRsp
   (
   ) const
{
   int prec = 17;
   int width = 26;
   std::ofstream data_file(path::Join(mPrefix, "rsp_imag.dat"));
   
   data_file << std::left << std::setw(width) << "#FRQ(cm^-1)"
                          << std::setw(width) << "#IMAG X RSP"
                          << std::setw(width) << "#IMAG Y RSP"
                          << std::setw(width) << "#IMAG Z RSP" << "\n";
   
   midas::stream::ScopedPrecision(prec, data_file);
   data_file << std::scientific;

   for(auto frq_it=XX().FrqBegin(); frq_it!=XX().FrqEnd(); ++frq_it)
   {
      Nb imag_x_rsp = (XX().ImRsp(*frq_it));
      Nb imag_y_rsp = (YY().ImRsp(*frq_it));
      Nb imag_z_rsp = (ZZ().ImRsp(*frq_it));
      
      data_file << std::fixed << std::setw(width) << (*frq_it)[1]*C_AUTKAYS;
      
      data_file << std::scientific
                << std::setw(width) << imag_x_rsp 
                << std::setw(width) << imag_y_rsp 
                << std::setw(width) << imag_z_rsp << "\n";
   }
}

/**
 * Plot stick spectrum.
 **/
void RspIr::PlotStick
   (
   ) const
{
   RspSpectrum::PlotStick   (path::Join(mPrefix, "rsp_ir_stick.dat"    ),"IR ABS");
   RspSpectrum::PlotStickMin(path::Join(mPrefix, "rsp_ir_stick_min.dat"),"IR FAKE ABS");
}

/**
 * Plot isotropic.
 **/
void RspIr::PlotIsotropic
   (
   ) const
{
   int prec = 17;
   int width = 26;

   std::ofstream data_file(path::Join(mPrefix, "rsp_isotropic.dat"));

   data_file << std::left << std::setw(width) << "#FRQ(cm-1)"
                          << std::setw(width) << "#REAL ISOTROPIC" 
                          << std::setw(width) << "#IMAG ISOTROPIC" << "\n";
   
   midas::stream::ScopedPrecision(prec, data_file);
   data_file << std::scientific;

   for(auto frq_it=XX().FrqBegin(); frq_it!=XX().FrqEnd(); ++frq_it)
   {
      // real
      Nb real_x_rsp = (XX().ReRsp(*frq_it));
      Nb real_y_rsp = (YY().ReRsp(*frq_it));
      Nb real_z_rsp = (ZZ().ReRsp(*frq_it));

      Nb real_isotropic = (real_x_rsp + real_y_rsp + real_z_rsp)/3;
      
      // imag
      Nb imag_x_rsp = (XX().ReRsp(*frq_it));
      Nb imag_y_rsp = (YY().ReRsp(*frq_it));
      Nb imag_z_rsp = (ZZ().ReRsp(*frq_it));

      Nb imag_isotropic = (imag_x_rsp + imag_y_rsp + imag_z_rsp)/3;

      // print file
      data_file << std::fixed << std::setw(width) << (*frq_it)[1]*C_AUTKAYS;
      
      data_file << std::scientific
                << std::setw(width) << real_isotropic
                << std::setw(width) << imag_isotropic << "\n";
   }
}
