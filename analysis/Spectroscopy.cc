/**
************************************************************************
* 
* @file                Spectroscopy.h
*
* Created:             11-11-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Generating spectra
* 
* Last modified: Thu Nov 19, 2009  03:29PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <fstream>
#include <cassert>

// midas headers
#include "input/Input.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/Plot.h"
#include "analysis/Spectroscopy.h"

// using declarations
using std::string;
using std::ifstream;
using std::istringstream;

void PlotIrRaman(const string& aFilename);

/**
* Main function for generating spectra.
* */
void SpectroscopyAnalysis()
{
   string filename = gAnalysisDir + "/SpectrumFiles";
   ifstream flist(filename.c_str());
   if (!flist) return; // Then no spectra is plotted, probably ok.
   Mout << " Plotting spectra..." << endl;
  
   string datafile;
   while (getline(flist, datafile))
   {
      if (datafile.substr(0, 8) == "midas_ir" ||
          datafile.substr(0, 11) == "midas_raman")
         PlotIrRaman(gAnalysisDir + "/" + datafile);
      else
         Mout << "   Reading " << filename << ": "
              << datafile << " is not a recognized type." << endl;
   }
   Mout << " Finished plotting spectra." << endl;
}

void PlotIrRaman(const string& aFilename)
{
   ifstream datf(aFilename.c_str());
   if (!datf)
   {
      Mout << "   Error reading " << aFilename << endl;
      return;
   }

   Plot spc(10000);
   spc.SetXlabel("cm^{-1}");
   
   // Text on plot depends on type (IR/Raman).
   if (aFilename.find("midas_ir") != string::npos)
   {
      spc.SetTitle("IR spectrum");
      spc.SetYlabel("Osc. strength");
   }
   else       // Must be Raman.
   {
      string ext_freq;
      string s;
      getline(datf, s);
      GetValueFromKey<string>(s, "ext_freq", ext_freq);
      spc.SetTitle("Raman spectrum (" + ext_freq + " cm^{-1})");
      spc.SetYlabel("Raman Activity");
   }
 
   // Get data.
   vector<string> labels;
   vector<Nb> energies;
   vector<Nb> heights;
   vector<Nb> widths;

   string dataline;
   while(getline(datf, dataline))
   {
      istringstream ss(dataline);
      In f_state; ss >> f_state;
      In i_state; ss >> i_state;
      labels.push_back(std::to_string(f_state) + "{/Symbol \\254}" + std::to_string(i_state));
      Nb nrg; ss >> nrg; energies.push_back(nrg);
      Nb hgt; ss >> hgt; heights.push_back(hgt);
      Nb wdt = gAnalysisFwhm; ss >> wdt; widths.push_back(wdt);
   }

   // Set x range.
   assert(energies.size() > 0);
   Nb xmin = energies[0];
   Nb xmax = energies.back();
   xmin -= C_I_10*fabs(xmin);
   xmax += C_I_10*fabs(xmax);
   spc.SetRange(xmin, xmax);

   // Put peaks into spectrum.
   Nb ymin = C_NB_MAX;
   for (In i=0; i<energies.size(); i++)
   {
      spc.AddLorentzian(energies[i], heights[i], widths[i], labels[i]);
      if (heights[i] < ymin)
         ymin = heights[i];
   }

   spc.SetLogScaleY(false);
   spc.SetPlotRange(xmin, xmax,
                    ymin-C_I_10*fabs(ymin), spc.GetMaxY() + C_I_10*fabs(spc.GetMaxY()));
   spc.MakePsFile(aFilename.c_str());
}
