/**
************************************************************************
* 
* @file                RspIr.h
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Handle response IR-spectra.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RSPIR_H_INCLUDED
#define RSPIR_H_INCLUDED

#include <vector>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Range.h"
#include "input/CombinedRspFunc.h"
#include "analysis/RspSpectrum.h"

/**
 *
 **/
class RspIr
   : public RspSpectrum
{
   private:
      //! Frequency range of spectrum
      const Range<Nb>                mFrqRange; 
      //! Vector of response function names
      const std::vector<std::string> mRsp; 
      //!
      const std::string              mPrefix;
      //! Response functions
      std::tuple<CombinedRspFunc,CombinedRspFunc,CombinedRspFunc> mXYZRsp; 
      
      //// Functions for getting response functions internally:
      ///> Get XX rsp
      CombinedRspFunc& XX()
      {
         return std::get<0>(mXYZRsp);
      }

      ///> Get XX rsp (const)
      const CombinedRspFunc& XX() const
      {
         return std::get<0>(mXYZRsp);
      }

      ///> Get YY rsp
      CombinedRspFunc& YY()
      {
         return std::get<1>(mXYZRsp);
      }

      ///> Get YY rsp (const)
      const CombinedRspFunc& YY() const
      {
         return std::get<1>(mXYZRsp);
      }

      ///> Get ZZ rsp
      CombinedRspFunc& ZZ()
      {
         return std::get<2>(mXYZRsp);
      }

      ///> Get ZZ rsp (const)
      const CombinedRspFunc& ZZ() const
      {
         return std::get<2>(mXYZRsp);
      }

   public:
      struct RspIrInput
      {
         Range<Nb>                range;
         std::vector<std::string> rsp;
      };

      ///> Constructor
      RspIr
         (  Range<Nb>&& range
         ,  std::vector<std::string>&& rsp
         ,  const std::string& aPrefix
         );
      
      ///> Calculate absroption
      void CalculateAbsorption();

      ///> Calculate weights given IR rule
      void CalculateWeights();

      ///> Plot the spectrum
      void PlotSpectrum() const;

      ///> Plot the real part
      void PlotReRsp() const;
      
      ///> Plot the imag part
      void PlotImRsp() const;

      ///> Plot a stick spectrum
      void PlotStick() const;
      
      ///> Plot isotropic
      void PlotIsotropic() const;
};

#endif /* RSPIR_H_INCLUDED */
