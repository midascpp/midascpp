/**
************************************************************************
* 
* @file                TempSim.h
*
* Created:             5-11-2006 
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Holds info on excited states
* 
* Last modified: Mon Nov 06, 2006  10:59AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TEMPSIM_H   
#define TEMPSIM_H   

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"
#include"util/Io.h"
#include"mmv/MidasVector.h"
#include"mmv/MidasMatrix.h"
#include"analysis/VscfTempSim.h"

/**
* Class ExciStates: 
* */
class TempSim
{
   private:
      bool mDataFound;                  ///< The basic data has been found 
      In mNstates;                      ///< Number of states included 
      In mNproperties;                  ///< Number of properties other than energy 
      MidasVector mEnergies;            ///< Vector of energies 
      MidasMatrix mAverages;            ///< Averaged Properties for different temperatures 
      vector<string> mPropStr;          ///< String describing property
      Nb mPartFunc;                     ///< Partition function 
      Nb mTempCurrent;                  ///< Temperatures 
      MidasVector mTemperature;         ///< Temperatures 
      Nb mBeta;                         ///< Currrent beta = 1/kT 
      string mInfoFile;                 ///< File containing nr of states/props 
      string mEnergyInfoFile;           ///< File containing energies
      string mPropInfoFile;             ///< File"_i" containing properties 
      string mBase;                     ///< Base in name 
      ofstream mOfstream_stream;               ///< Output file stream for detailed info on convergence etc. 
      bool mIsVscfFast;                        ///< Use fast Vscf temp. sim. code
      vector<VscfTempSim> mVscfFast;    ///< Hold intermediate variables and vectors
      MidasVector mIntEnergy;
      MidasVector mEntropy;
      MidasVector mHelmholz;
      MidasVector mHeatCap;

   public:
      TempSim(string aBase);   ///< Constructor, string is the base for readin file names 
      void SetTemp(Nb aTemp,In aItemp) {mTempCurrent=aTemp,mTemperature[aItemp]=aTemp;mBeta=C_AUTK/aTemp;} ///< Set the temperaturs 
      Nb GetTemp() {return mTempCurrent;}       ///< Get the temperaturs 
      Nb GetPartFunc() {return mPartFunc;}      ///< Get the temperaturs 
      Nb ExpMinusBetaE(Nb& arE);                ///< calc exp(-beta E) 
      void GetEnergies();                       ///< Read energies from disc 
      void CalcPartitionFunc(In);                 ///< Calculate partition function 
      void CalcPopulations();                   ///< Calculate populations 
      void GetProperty(MidasVector& arProperty,string& aS,In aProp,ifstream& arIfStream); 
                                                ///< GetProperties from disc 
      Nb CalcPropertyAverage(In aProp,ifstream& arIfStream);  ///< Calculated a property average 
      void CalcPropertyAverages(In aTemp);      ///< Calculated all property averages 
      void Summarize();                         ///< Summarize calculated data 
      friend void TemperatureSimulator(); 
      void SetIsVscfFast(bool aB) {mIsVscfFast=aB;}
      bool GetIsVscfFast() {return mIsVscfFast;}
      void CalcWeights();
      void AddVscfFast(string aS) {mVscfFast.push_back(VscfTempSim(aS));};
      string GetEnergyInfoFile() {return mEnergyInfoFile;}
      void CalculateTD(Nb,In);
      //VscfTempSim GetVscfFast() {return mVscfFast;}
};
void TemperatureSimulator();            ///< Do a simulation of temperature effects on pop and props 

#endif
