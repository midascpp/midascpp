/**
************************************************************************
* 
* @file                VscfTempSim.h
*
* Created:             24-07-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Make simple temperatur effects 
* 
* Last modified:       Mon, 24-07-2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef VSCFTEMPSIM_H
#define VSCFTEMPSIM_H

// Standard headers:
#include <vector>
using std::vector;
#include <set>
using std::set;
#include <map>
using std::map;
#include <algorithm>
using std::sort;
using std::remove_if;
#include <utility>
using std::pair;
//using std::make_pair;

// links to headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"

class VscfTempSim
{
   private:
      vector<In> mHoPrMode;
      vector<Nb> mOneModePartFunc;
      vector<Nb> mWeights;
      vector<Nb> mProperties;
      Nb mPartFunc;
   public:
      VscfTempSim(string aS);                             ///< Default constructor
      void SetOneModePartFunc(In aI, Nb aN) {mOneModePartFunc[aI]=aN; mPartFunc*=aN;}
      Nb GetOneModePartFunc(In aI) {return mOneModePartFunc[aI];}
      In GetNmodes() {return mHoPrMode.size();}
      In GetNHoInMode(In aI) {return mHoPrMode[aI];}
      Nb GetPartFunc() {return mPartFunc;}
      void SetWeight(In aI, Nb aW) {mWeights[aI]=aW;}
};

#endif
