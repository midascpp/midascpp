/**
************************************************************************
* 
* @file                Analysis.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for the Analysis module of MidasCPP 
* 
* Last modified: Wed May 06, 2009  11:19AM ove
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <utility>

// My headers:
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "analysis/Analysis.h"
#include "analysis/ExciStates.h"
#include "util/Io.h"
#include "util/Property.h"
#include "util/MakeTables.h"
#include "util/Gaussian.h"
#include "util/Plot.h"
#include "util/MidasStream.h"
#include "util/Path.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "analysis/Spectroscopy.h"
#include "analysis/PotAnaPlot.h"
#include "analysis/TempSim.h"
#include "analysis/VscfTempSim.h"

using std::vector;
using std::set;
using std::map;
using std::sort;
using std::remove_if;
using std::pair;
using namespace midas;

/**
* Get property from string - get logics and value. 
* */
void GetPropInfo(string& arSin,Property& arPout, Nb& arData)
{
   istringstream s_is(arSin) ;
   In iprpc, isymp, nord, isymex, ispinex, inrex;
   Nb prop, frq_y, frq_z, frq_u;
   string lab_mod, lab_x, lab_y, lab_z, lab_u;
   s_is >> iprpc;
   s_is >> isymp;
   s_is >> nord;
   s_is >> lab_mod;
   s_is >> prop;
   s_is >> lab_x;
   s_is >> lab_y;
   s_is >> lab_z;
   s_is >> lab_u;
   s_is >> frq_y;
   s_is >> frq_z;
   s_is >> frq_u;
   s_is >> isymex;
   s_is >> ispinex;
   s_is >> inrex;
   arData = prop;
   Property tmp(isymp,nord,lab_mod,prop,lab_x,lab_y,lab_z,lab_u,
            frq_y,frq_z,frq_u,isymex,ispinex,inrex);
   arPout = tmp;
}
/**
* Get property from string - get logics and value. 
* */
void TwoPhotonOut(Nb****& arWss)
{
   Nb delta_f=C_0;
   Nb delta_g=C_0;
   Nb delta_h=C_0;
   In n3 = I_3;
   for (In i=I_0;i<n3;i++) 
   {
      for (In j=I_0;j<n3;j++) 
      {
         delta_f += arWss[i][i][j][j];
         delta_g += arWss[i][j][i][j];
         delta_h += arWss[i][j][j][i];
      }
   }
   Out72Char(Mout,'+','-','+');
   Mout << " Delta_f :  " << delta_f << endl;
   Mout << " Delta_g :  " << delta_g << endl;
   Mout << " Delta_h :  " << delta_h << endl;

   Nb f=C_2;
   Nb g=C_2;
   Nb h=C_2;
   Nb delta_p = f*delta_f+g*delta_f+h*delta_h;
   f=-C_1;
   g=C_4;
   h=-C_1;
   Nb delta_o = f*delta_f+g*delta_f+h*delta_h;

   Nb fact = C_AUTIME*C_TANG*C_TANG*C_TANG*C_TANG;
   fact *= (C_2*C_PI)*(C_ALPHA*C_ALPHA);
   fact *= (C_2*C_PI)*(C_2*C_PI);

   Mout << " Rotationally averaged two-photon transition strengths (au) " << endl;
   Mout << " multiplied by w_1*w_2! (non-standard) " << endl;
   Mout << " delta_{TP}_of (par.)  = " << delta_p << endl;
   Mout << " delta_{TP}_of (perp.) = " << delta_o << endl;
   Mout << " Cross sections (cm^4s) (two-photon transition rate constants):" << endl;
   Mout << " sigma_{TP}_of (par.)  = " << delta_p*fact << endl;
   Mout << " sigma_{TP}_of (perp.) = " << delta_o*fact << endl;
   Out72Char(Mout,'+','-','+');
}
/**
* Modify property files. 
* */
void MassagePropertyFiles(vector<string>& arCalcStrings)
{
   Property p_in;
   Nb prop;
   In n_calcs = arCalcStrings.size();
   string s_inp,s_out;
   for (In i_calc = I_0;i_calc<n_calcs;i_calc++)
   {
      string calc_in_name = gAnalysisDir + "/" + arCalcStrings[i_calc];
      string calc_cp_name = gAnalysisDir + "/" + arCalcStrings[i_calc]+"_copy";
      ifstream calc_in(calc_in_name.c_str(),ios_base::in);
      // MidasStream here
      ofstream calc_out_stream(calc_cp_name.c_str(),ios_base::out);
      MidasStreamBuf buffer(calc_out_stream);
      MidasStream calc_out(buffer);
      getline(calc_in,s_inp);
      In i_prop = I_0;
      Mout << " calc: " << i_calc << " file :" << calc_in_name << " out" << calc_cp_name << endl;
      vector<Nb> five_states_exci(5);
      vector<Nb> five_states_osc(5,C_0);
      vector<Nb> five_states_tpa(5,C_0);
      vector<string> five_states_s(5);
      //for (In i=0;i<5;i++) five_states_osc[i]=C_0;
      while (!calc_in.eof()) 
      {
         i_prop++;
         GetPropInfo(s_inp,p_in,prop);
         Mout << " prop: " << i_prop << " inp:" << s_inp << " prop " << prop << endl;
         s_out=s_inp;
         if (i_prop >= 7 && i_prop <= 46) s_out[154]='1';
         if (i_prop >= 7 && i_prop <= 46) s_out[158]='1';
         if (i_prop == 7 || i_prop ==  8 || i_prop == 17 || i_prop ==22 ||i_prop == 27 || (i_prop >= 32&& i_prop<=34) ) s_out[162]='1';
         if (i_prop == 9 || i_prop == 10 || i_prop == 18 || i_prop ==23 ||i_prop == 28 || (i_prop >= 35&& i_prop<=37) ) s_out[162]='2';
         if (i_prop == 11|| i_prop == 12 || i_prop == 19 || i_prop ==24 ||i_prop == 29 || (i_prop >= 38&& i_prop<=40) ) s_out[162]='3';
         if (i_prop == 13|| i_prop == 14 || i_prop == 20 || i_prop ==25 ||i_prop == 30 || (i_prop >= 41&& i_prop<=43) ) s_out[162]='4';
         if (i_prop == 15|| i_prop == 16 || i_prop == 21 || i_prop ==26 ||i_prop == 31 || (i_prop >= 44&& i_prop<=46) ) s_out[162]='5';
         //if (i_prop >= 17 && i_prop <= 31) {s_out[9]='-';s_out[10]='2';s_out[11]='0';} 
         if (i_prop == 32 || i_prop == 35 || i_prop == 38 || i_prop == 41 || i_prop == 44) {s_out[9]='-';s_out[10]='3';s_out[11]='0';} 
         if (i_prop == 33 || i_prop == 36 || i_prop == 39 || i_prop == 42 || i_prop == 45) {s_out[9]='-';s_out[10]='3';s_out[11]='1';} 
         if (i_prop == 34 || i_prop == 37 || i_prop == 40 || i_prop == 43 || i_prop == 46) {s_out[9]='-';s_out[10]='3';s_out[11]='2';} 

         if (i_prop==7) five_states_exci[I_0] = prop;
         if (i_prop==9) five_states_exci[I_1] = prop;
         if (i_prop==11) five_states_exci[I_2] = prop;
         if (i_prop==13) five_states_exci[I_3] = prop;
         if (i_prop==15) five_states_exci[I_4] = prop;
         if (i_prop==17) five_states_s[I_0] = s_out;
         if (i_prop==18) five_states_s[I_1] = s_out;
         if (i_prop==19) five_states_s[I_2] = s_out;
         if (i_prop==20) five_states_s[I_3] = s_out;
         if (i_prop==21) five_states_s[I_4] = s_out;
         if (i_prop==17||i_prop==22||i_prop==27) five_states_osc[I_0] += five_states_exci[I_0]*prop;
         if (i_prop==18||i_prop==23||i_prop==28) five_states_osc[I_1] += five_states_exci[I_1]*prop;
         if (i_prop==19||i_prop==24||i_prop==29) five_states_osc[I_2] += five_states_exci[I_2]*prop;
         if (i_prop==20||i_prop==25||i_prop==30) five_states_osc[I_3] += five_states_exci[I_3]*prop;
         if (i_prop==21||i_prop==26||i_prop==31) five_states_osc[I_4] += five_states_exci[I_4]*prop;
         if (i_prop==32) five_states_tpa[I_0] += five_states_exci[I_0]*five_states_exci[I_0]*prop/C_4;
         if (i_prop==35) five_states_tpa[I_1] += five_states_exci[I_1]*five_states_exci[I_1]*prop/C_4;
         if (i_prop==38) five_states_tpa[I_2] += five_states_exci[I_2]*five_states_exci[I_2]*prop/C_4;
         if (i_prop==41) five_states_tpa[I_3] += five_states_exci[I_3]*five_states_exci[I_3]*prop/C_4;
         if (i_prop==44) five_states_tpa[I_4] += five_states_exci[I_4]*five_states_exci[I_4]*prop/C_4;
         if (i_prop==47)
         {
             string s_osc=s_out;
             for (In i=0;i<5;i++)
             {
                s_osc=five_states_s[i];
                if (i==0) {s_osc[3]='4';s_osc[4]='7';}
                if (i==1) {s_osc[3]='4';s_osc[4]='8';}
                if (i==2) {s_osc[3]='4';s_osc[4]='9';}
                if (i==3) {s_osc[3]='5';s_osc[4]='0';}
                if (i==4) {s_osc[3]='5';s_osc[4]='1';}
                string s_val=StringForNb(five_states_osc[i],22);
                s_osc.replace(24,22,s_val);
                {s_osc[9]='-';s_osc[10]='2';s_osc[11]='1';} 
                calc_out << s_osc << endl;
             } 
             for (In i=0;i<5;i++)
             {
                s_osc=five_states_s[i];
                if (i==0) {s_osc[3]='5';s_osc[4]='2';}
                if (i==1) {s_osc[3]='5';s_osc[4]='3';}
                if (i==2) {s_osc[3]='5';s_osc[4]='4';}
                if (i==3) {s_osc[3]='5';s_osc[4]='5';}
                if (i==4) {s_osc[3]='5';s_osc[4]='6';}
                string s_val=StringForNb(five_states_tpa[i],22);
                s_osc.replace(24,22,s_val);
                {s_osc[9]='-';s_osc[10]='3';s_osc[11]='3';} 
                calc_out << s_osc << endl;
             } 
             s_out[3]='5';s_out[4]='7';
         }
         calc_out << s_out << endl;
         Mout << " prop: " << i_prop << "p_in:" << s_out << endl;
         getline(calc_in,s_inp);
      }
      calc_in.close();
      calc_out_stream.close();
      MvFile(calc_cp_name,calc_in_name);
   }
}
/**
* Get a specif property from a set of files
* */
void GetPropertyFromFiles(Nb& arMin, Nb& arMax, MidasVector& arPropVec, 
   vector<string>& arCalcStrings,Property& arPout,In aPropHit)
{
   Property p_out_old;
   arMin = C_NB_MAX;
   arMax = -C_NB_MAX;
   In n_calcs = arCalcStrings.size();
   string s_inp;
   for (In i_calc = I_0;i_calc<n_calcs;i_calc++)
   {
      string calc_in_name = gAnalysisDir + "/" + arCalcStrings[i_calc];
      ifstream calc_in(calc_in_name.c_str(),ios_base::in);
      getline(calc_in,s_inp);
      In i_prop = I_0;
      while (!calc_in.eof() && i_prop < aPropHit) 
      {
         getline(calc_in,s_inp);
         i_prop++;
      }
      //Mout << " calc " << i_calc << s_inp << endl;
      Nb prop = C_0;
      GetPropInfo(s_inp,arPout,prop);
      arPropVec[i_calc] = prop;
      if (prop > arMax ) arMax = prop; // Construct min/max on the fly
      if (prop < arMin ) arMin = prop;
      if (i_calc != I_0)
      {
         if (!arPout.LogicallyTheSameAs(p_out_old)) 
         {
            Mout << " Potential problem in sort of properties (ok for excited state stuff) " << endl;
         }
      }
      p_out_old = arPout;
   }
}
/**
* Initiate analysis by reading in data on the property files
* and storing properties on vectors 
* */
void SortProperties()
{
   string name_calc_list   = gAnalysisDir + "/calc_list";
   ifstream calc_list(name_calc_list.c_str(),ios_base::in);
   string s_inp;
   if (!getline(calc_list,s_inp)) return;
   Mout << " Name of calc_list file: " << name_calc_list << endl;
   Mout << " Sorting calculated properties " << endl;

   In i_count =I_0;
   // In i_count2 =I_0;  // Seidler warning: This appears unused.
   string name;
   vector<string> prop_strings;
   vector<string> calc_strings;
   while (!calc_list.eof())
   {
      i_count++;
      istringstream input_s(s_inp);
      input_s >> name;
      //input_s >> i_count2 >> name;
      calc_strings.push_back(name);
      getline(calc_list,s_inp);
      //if (i_count2 != i_count) 
         //Mout << "Numbering on calc. list is not followed/ascending ? " <<endl;
   }
   In n_calcs = calc_strings.size();
   Mout << " Analyse data from " << n_calcs << " files " << endl;

   // Massage is a specialized feature for fixing a dalton midas interface bug at some time.
   //MassagePropertyFiles(calc_strings);

   if (n_calcs <=I_0) return; 
   In n_prop = I_0;
   string first_calc_name = gAnalysisDir + "/" + calc_strings[I_0];
   ifstream first_calc(first_calc_name.c_str(),ios_base::in);
   getline(first_calc,s_inp);
   while (!first_calc.eof()) 
   {
      n_prop++;
      getline(first_calc,s_inp);
   }
   first_calc.close();
   Mout << " Number of properties " << n_prop - I_1 
      << " on first file assumed to be the standard " << endl;
   Mout << " (excluding end of calc. label) " << endl;

   //MidasVector prop_vals(n_prop);
   //vector<string> prop_descrips(n_prop);
   vector<Property> aveprops(n_prop);
   set<ExciStates> one_phot_states;
   set<ExciStates> two_phot_states;

   //
   // Inefficient double loop version
   //
   for (In i_prop_hit = I_0;i_prop_hit<n_prop;i_prop_hit++)
   {
      MidasVector prop_res(n_calcs);
      //Mout << " Getting Property " << i_prop_hit << endl;
      Nb min_hist, max_hist;
      Property p_out;
      GetPropertyFromFiles(min_hist,max_hist,prop_res,calc_strings,
                           p_out,i_prop_hit);

      string st_help  = p_out.PropertyModel();
      if (st_help != "THE_END")
      {
         //Mout << " Property vector " << prop_res << endl;
         prop_res.DumpToReadableFileWithName(gAnalysisDir+"/prop_"+std::to_string(i_prop_hit));
         // Calculate standard statistic measures 
         string s_prop;
         p_out.OutPut(s_prop,false);
         In n_hist_size = In(sqrt(C_1*n_calcs));
         if (gExtHistogram != I_0) n_hist_size = gExtHistogram;
         Nb ave,ave_dev,std_dev,var,skew,kurt;
         prop_res.StatAnalysisAndOutPut(ave,ave_dev,std_dev,var,skew,kurt,
                 s_prop,n_hist_size,min_hist,max_hist,gAnalysisDir,
                 "prop_hist_"+std::to_string(i_prop_hit),
                 "plot_prop_"+std::to_string(i_prop_hit));

         // Add property/states to list of properties
         if (p_out.IsThisOnePhotAbsStr()) 
         {
            ExciStates es(p_out);
            one_phot_states.insert(es);
         }
         else if (p_out.IsThisTwoPhotAbsStr()) 
         {
            ExciStates es(p_out);
            two_phot_states.insert(es);
         }
         p_out.SetValue(ave);
         aveprops[i_prop_hit] = p_out;

      }
   }
   //if (gAnalysisIoLevel > I_5) 
   //{
      //Mout << "\n\n Compiled average values " << endl;
      //for (In i_prop_hit = I_0;i_prop_hit<n_prop;i_prop_hit++)
      //{
         //Mout << prop_descrips[i_prop_hit] << " = " << prop_vals[i_prop_hit] << endl;
      //}
   //}
   In n_ops=one_phot_states.size(); 
   In n_tps=two_phot_states.size(); 
   In n_points = I_10_3+I_1;
   Mout << "\n\n Nr. of one-photon abs. str. states " << n_ops << endl;
   if (n_ops>I_0)
   {
      In count = I_0;
      for (set<ExciStates>::const_iterator i=one_phot_states.begin();
         i!=one_phot_states.end();i++)
      {
         count++;
         Mout << " State " << count;
         Mout << " Sym: " << (*i).Sym();
         Mout << " Spin: " << (*i).Spin();
         Mout << " Nr: " << (*i).Number();
         Mout << " Model: " << (*i).Model();
         Mout << endl;
         MidasVector osc_diag(3);
         vector<In> prop_hits;
         vector<In> exci_hits;
         In ia,ib,ic,id;
         // Note, the following should work for both one output of total osc. or as diagonal of matrix. 
         // But of course not both simultaneously 
         for (In i_prop = I_0;i_prop<n_prop;i_prop++)
         {
            if (aveprops[i_prop].IsThisOnePhotAbsStr()||aveprops[i_prop].IsThisExcita()) 
            {
               ExciStates new_exci(aveprops[i_prop]);
               if (new_exci == *i)
               {
                  if (aveprops[i_prop].IsThisOnePhotAbsStr())
                  {
                     //Mout << aveprops[i_prop] << endl;
                     aveprops[i_prop].GetIntForLabs(ia,ib,ic,id);
                     osc_diag[ia] = aveprops[i_prop].Value();
                     prop_hits.push_back(i_prop);
                  }
                  else if (aveprops[i_prop].IsThisExcita()) 
                  {
                     //Mout << aveprops[i_prop] << endl;
                     exci_hits.push_back(i_prop);
                  }
               }
            }
         }
         osc_diag.Scale(C_2/C_3);
         //Mout << " Oscillator strength diagonal: \n " << osc_diag;
         Mout << " Total averaged oscillator strength: " << osc_diag.SumEle() <<endl;
         MidasVector prop_res(n_calcs,C_0);
         //Mout << " Getting Property " << i_prop_hit << endl;
         Nb min_hist, max_hist;
         Property p_out;
         for (In ip=I_0;ip<prop_hits.size();ip++)
         {
            In i_prop = prop_hits[ip];
            MidasVector prop_res2(n_calcs,C_0);
            GetPropertyFromFiles(min_hist,max_hist,prop_res2,calc_strings,
                           p_out,i_prop);
            prop_res += prop_res2;
         }
         prop_res.Scale(C_2/C_3);
         min_hist = prop_res.FindMinValue();
         max_hist = prop_res.FindMaxValue();
         //Mout << " Property vector " << prop_res << endl;
         prop_res.DumpToReadableFileWithName(gAnalysisDir+"/osc_"+std::to_string(count));
         // Calculate standard statistic measures 
         string s_prop = " Oscillator Strength Sym:" + 
            std::to_string((*i).Sym())+" Spin: " + 
            std::to_string((*i).Spin()) + " Nr: " + 
            std::to_string((*i).Number());
         In n_hist_size = In(sqrt(C_1*n_calcs));
         if (gExtHistogram != I_0) n_hist_size = gExtHistogram;
         Nb ave,ave_dev,std_dev,var,skew,kurt;
         prop_res.StatAnalysisAndOutPut(ave,ave_dev,std_dev,var,skew,kurt,
                 s_prop,n_hist_size,min_hist,max_hist,gAnalysisDir,
                 "prop_hist_osc_"+std::to_string(count),"plot_prop_osc_"+std::to_string(count));
      
         // NB There should only be one excitation energy for this oscillator strength.
         if (exci_hits.size() != I_1 )
             MIDASERROR("Dont understand anything else than one exci for one oscillator strength ?");
         else
         {
            In i_prop_exci = exci_hits[I_0];
            MidasVector prop_res2(n_calcs,C_0);
            GetPropertyFromFiles(min_hist,max_hist,prop_res2,calc_strings,
                                 p_out,i_prop_exci);
            vector<Gaussian> all_exci(n_calcs);
            Nb alpha = log(C_2)/(gAnalysisFwhm*gAnalysisFwhm);
            for (In i_gauss=I_0;i_gauss<n_calcs;i_gauss++)
            {
               Nb pre_fac = prop_res[i_gauss]/C_2; // calculate prefactor from exci
               pre_fac *= sqrt(alpha/C_PI);
               all_exci[i_gauss].SetAlpha(alpha);
               all_exci[i_gauss].SetEq(prop_res2[i_gauss]);
               all_exci[i_gauss].SetPreConst(pre_fac);
            }
            string name = "spec_"+std::to_string(count);
            Mout << " Creating gnu.sh and .ps files with name " 
                 << gAnalysisDir +"/"+name << endl; 
            MidasVector xvals(n_points);
            MidasVector yvals(n_points);
            Nb delta = (gAnalysisSpeEnd - gAnalysisSpeBegin)/(n_points-I_1);
            for (In ip=I_0;ip<n_points;ip++)
            {
               Nb xp = gAnalysisSpeBegin + ip*delta;
               xvals[ip] = xp*C_AUTEV;
               yvals[ip] = AveOfValsForVectorOfGaussians(all_exci,xp);
            }
            /* peter
             * obsoleted by Plot class.
            string xlabel = "Excitation energy (eV) ";
            string ylabel = "Absorption strength (arb.units)";
            string label = " Spectrum ";
            MakePsFig(name,label,xvals,yvals,n_points,
                 I_0, I_0, false, false, C_1, C_1, I_1,xlabel,ylabel,
                 gAnalysisDir,I_0);
            */
            // This needs to be tested....
            Plot spc(n_points);
            spc.SetTitle("Spectrum");
            spc.SetXYvals(xvals, yvals);
            spc.SetXlabel("Excitation energy (eV)");
            spc.SetYlabel("Absorption strength (arb. units)");
            spc.SetPlotRangeRel(C_0, C_0, C_I_10, C_I_10);
            spc.MakePsFile(gAnalysisDir + "/" + name);
         }
      }
      bool all_combined=true;
      if (all_combined)
      {
         MidasVector xvals(n_points);
         MidasVector yvals(n_points);
         string name = "spec_combined";
         ofstream combined(name.c_str(),ios_base::out);
         for (In i_c=I_1;i_c<=count;i_c++)
         {
            string name_c = "spec_"+std::to_string(i_c)+"_gnu.data";
            ifstream one_spec(name_c.c_str(),ios_base::out);
            for (In i_p=I_0;i_p<n_points;i_p++)
            {
               Nb xv,yv; 
               one_spec >> xv >> yv;
               if (i_c==I_1) xvals[i_p] = xv;
               else if (fabs(xvals[i_p]-xv)>C_NB_EPSILON*C_10_3)
                  MIDASERROR("Order in spec_combi production not as assumed");
               yvals[i_p] += yv;
            }
         }
         /* peter
          * obsoleted by plot class.
         string xlabel = "Excitation energy (eV) ";
         string ylabel = "Absorption strength (arb.units)";
         string label = " Combined Spectrum ";
         MakePsFig(name,label,xvals,yvals,n_points,
         I_0, I_0, false, false, C_1, C_1, I_1,xlabel,ylabel,
         gAnalysisDir,I_0);
         */
         // This needs to be tested....
         Plot spc(n_points);
         spc.SetTitle("Combined Spectrum");
         spc.SetXYvals(xvals, yvals);
         spc.SetXlabel("Excitation energy (eV)");
         spc.SetYlabel("Absorption strength (arb. units)");
         spc.SetPlotRangeRel(C_0, C_0, C_I_10, C_I_10);
         spc.MakePsFile(gAnalysisDir + "/" + name);
      }
   }
   Mout << "\n\n Nr. of two-photon abs. str. states " << n_tps << endl;
   if (n_tps>I_0)
   {
      In count = I_0;
      for (set<ExciStates>::const_iterator i=two_phot_states.begin();
         i!=two_phot_states.end();i++)
      {
         count++;
         Mout << " State " << count;
         Mout << " Sym: " << (*i).Sym();
         Mout << " Spin: " << (*i).Spin();
         Mout << " Nr: " << (*i).Number();
         Mout << " Model: " << (*i).Model();
         Mout << endl;
         MidasVector osc_diag(3);
         vector<In> prop_hits;
         vector<In> exci_hits;
         In ia,ib,ic,id;
         for (In i_prop = I_0;i_prop<n_prop;i_prop++)
         {
            if (aveprops[i_prop].IsThisTwoPhotAbsStr()||aveprops[i_prop].IsThisExcita()) 
            {
               ExciStates new_exci(aveprops[i_prop]);
               if (new_exci == *i)
               {
                  if (aveprops[i_prop].IsThisTwoPhotAbsStr())
                  {
                     //Mout << aveprops[i_prop] << endl;
                     aveprops[i_prop].GetIntForLabs(ia,ib,ic,id);
                     osc_diag[ia] = aveprops[i_prop].Value();
                     prop_hits.push_back(i_prop);
                  }
                  else if (aveprops[i_prop].IsThisExcita()) 
                  {
                     //Mout << aveprops[i_prop] << endl;
                     exci_hits.push_back(i_prop);
                  }
               }
            }
         }
         //osc_diag.Scale(C_2/C_3);
         Mout << " Total average tpa: " << osc_diag.SumEle() <<endl;
         MidasVector prop_res(n_calcs,C_0);
         //Mout << " Getting Property " << i_prop_hit << endl;
         Nb min_hist, max_hist;
         Property p_out;
         for (In ip=I_0;ip<prop_hits.size();ip++)
         {
            In i_prop = prop_hits[ip];
            MidasVector prop_res2(n_calcs,C_0);
            GetPropertyFromFiles(min_hist,max_hist,prop_res2,calc_strings,
                           p_out,i_prop);
            prop_res += prop_res2;
         }
         //prop_res.Scale(C_2/C_3);
         min_hist = prop_res.FindMinValue();
         max_hist = prop_res.FindMaxValue();
         //Mout << " Property vector " << prop_res << endl;
         prop_res.DumpToReadableFileWithName(gAnalysisDir+"/tpap_"+std::to_string(count));
         // Calculate standard statistic measures 
         string s_prop = " TPA strength linear pol Sym:" + 
            std::to_string((*i).Sym())+" Spin: " + 
            std::to_string((*i).Spin()) + " Nr: " + 
            std::to_string((*i).Number());
         In n_hist_size = In(sqrt(C_1*n_calcs));
         if (gExtHistogram != I_0) n_hist_size = gExtHistogram;
         Nb ave,ave_dev,std_dev,var,skew,kurt;
         prop_res.StatAnalysisAndOutPut(ave,ave_dev,std_dev,var,skew,kurt,
                 s_prop,n_hist_size,min_hist,max_hist,gAnalysisDir,
                 "prop_hist_tpap"+std::to_string(count),"plot_prop_tpap_"+std::to_string(count));
      
         // NB There should only be one excitation energy for this strength.
         if (exci_hits.size() != I_1 )
             MIDASERROR("Dont understand anything else than one exci for one tpa strength ?");
         else
         {
            In i_prop_exci = exci_hits[I_0];
            MidasVector prop_res2(n_calcs,C_0);
            GetPropertyFromFiles(min_hist,max_hist,prop_res2,calc_strings,
                                 p_out,i_prop_exci);
            vector<Gaussian> all_exci(n_calcs);
            Nb alpha = log(C_2)/(gAnalysisFwhm*gAnalysisFwhm);
            for (In i_gauss=I_0;i_gauss<n_calcs;i_gauss++)
            {
               Nb pre_fac = prop_res[i_gauss]/C_2; // calculate prefactor from exci
               pre_fac *= sqrt(alpha/C_PI);
               all_exci[i_gauss].SetAlpha(alpha);
               all_exci[i_gauss].SetEq(prop_res2[i_gauss]);
               all_exci[i_gauss].SetPreConst(pre_fac);
            }
            string name = "tpap_spec_"+std::to_string(count);
            Mout << " peter Creating gnu.sh and .ps files with name " 
                 << gAnalysisDir +"/"+name << endl; 
            MidasVector xvals(n_points);
            MidasVector yvals(n_points);
            Nb delta = (gAnalysisSpeEnd - gAnalysisSpeBegin)/(n_points-I_1);
            for (In ip=I_0;ip<n_points;ip++)
            {
               Nb xp = gAnalysisSpeBegin + ip*delta;
               xvals[ip] = xp*C_AUTEV;
               yvals[ip] = AveOfValsForVectorOfGaussians(all_exci,xp);
            }
            /* peter
             * obsoleted by plot class.
            string xlabel = "Excitation energy (eV) ";
            string ylabel = "Two-photon cross section (arb.units)";
            string label = " Spectrum ";
            MakePsFig(name,label,xvals,yvals,n_points,
                 I_0, I_0, false, false, C_1, C_1, I_1,xlabel,ylabel,
                 gAnalysisDir,I_0);
            */
            // This needs to be tested....
            Plot spc(n_points);
            spc.SetTitle("Spectrum");
            spc.SetXYvals(xvals, yvals);
            spc.SetXlabel("Excitation energy (eV)");
            spc.SetYlabel("Two-photon cross section (arb. units)");
            spc.SetPlotRangeRel(C_0, C_0, C_I_10, C_I_10);
            spc.MakePsFile(gAnalysisDir + "/" + name);
 }
      }
      bool all_combined=true;
      if (all_combined)
      {
         MidasVector xvals(n_points);
         MidasVector yvals(n_points);
         string name = "tpap_spec_combined";
         ofstream combined(name.c_str(),ios_base::out);
         for (In i_c=I_1;i_c<=count;i_c++)
         {
            string name_c = "tpap_spec_"+std::to_string(i_c)+"_gnu.data";
            ifstream one_spec(name_c.c_str(),ios_base::out);
            for (In i_p=I_0;i_p<n_points;i_p++)
            {
               Nb xv,yv; 
               one_spec >> xv >> yv;
               if (i_c==I_1) xvals[i_p] = xv;
               else if (fabs(xvals[i_p]-xv)>C_NB_EPSILON*C_10_3)
                  MIDASERROR("Order in tpap_spec_combi production not as assumed");
               yvals[i_p] += yv;
            }
         }
         /* peter
          * obsoleted by plot class.
         string xlabel = "Excitation energy (eV) ";
         string ylabel = "Absorption strength (arb.units)";
         string label = " Combined Spectrum ";
         MakePsFig(name,label,xvals,yvals,n_points,
         I_0, I_0, false, false, C_1, C_1, I_1,xlabel,ylabel,
         gAnalysisDir,I_0);
         */
         // This needs to be tested....
         Plot spc(n_points);
         spc.SetTitle("Combined Spectrum");
         spc.SetXYvals(xvals, yvals);
         spc.SetXlabel("Excitation energy (eV)");
         spc.SetYlabel("Absorption strength (arb. units)");
         spc.SetPlotRangeRel(C_0, C_0, C_I_10, C_I_10);
         spc.MakePsFile(gAnalysisDir + "/" + name);
      }
   }
}

/**
 *
 **/
void RspIrDrv
   (  AnalysisCalcDef& calc_def
   )
{
   auto& rsp_ir_vec = calc_def.mRspIr;
   for(auto it=rsp_ir_vec.begin(); it!=rsp_ir_vec.end(); ++it)
   {
      // Setup
      auto rsp_ir = RspIr{std::move(it->range), std::move(it->rsp), path::Join(input::gProgramSettings.GetMainDir(), "analysis")};
      rsp_ir.SetSpectrumAnalysisPoints          (calc_def.mSpectrumAnalysisPoints);
      rsp_ir.SetSpectrumAnalysisInterpolateStep (calc_def.mSpectrumAnalysisInterpolateStep);
      
      // Create spectra
      rsp_ir.CalculateAbsorption();
      
      if(calc_def.mSpectrumAnalysisInterpolate)
         rsp_ir.Interpolate();
      
      // Plotting
      rsp_ir.PlotSpectrum();
      rsp_ir.PlotReRsp();
      rsp_ir.PlotImRsp();
      rsp_ir.PlotIsotropic();
      
      rsp_ir.IdentifyPeaks();
      rsp_ir.PlotStick();
      rsp_ir.PrintPeakPositionsAndIntensities(Mout);
      if(calc_def.mSpectrumCalculateWeights) 
      {
         rsp_ir.CalculateWeights();
      }
   }
}

void RspMergeDrv(AnalysisCalcDef& calc_def)
{
   auto& rsp_merge = calc_def.mRspMerger;
   for(auto it=rsp_merge.begin(); it!=rsp_merge.end(); ++it)
   {
      it->Merge();
   }
}

void LinRspRamanDrv(AnalysisCalcDef& calc_def)
{
   auto& rsp_raman = calc_def.mLinRspRaman;
   for(auto it=rsp_raman.begin(); it!=rsp_raman.end(); ++it)
   {
      it->SetSpectrumAnalysisPoints(calc_def.mSpectrumAnalysisPoints);
      it->SetSpectrumAnalysisInterpolateStep(calc_def.mSpectrumAnalysisInterpolateStep);
      it->CalculateAbsorption();
      
      if(calc_def.mSpectrumAnalysisInterpolate)
         it->Interpolate();
      
      it->PlotSpectrum();
      it->PlotReRsp();
      it->PlotImRsp();
      
      it->IdentifyPeaks();
      it->PlotStick();
      it->PrintPeakPositionsAndIntensities(Mout);
   }
}

/**
 * Run analysis studies - pass control to various drivers.
 **/
void Analysis()
{
   // Output header
   // ----------------------------------------------------------------------
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Midas Analysis Program ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
   
   SortProperties();
   PotentialAnalysis();
   SpectroscopyAnalysis();
   
   if (gTempSet) 
   {
      TemperatureSimulator();
   }
   
   // Loop over analysis calculation definitions and do what is requested.
   // ----------------------------------------------------------------------
   for(int i = 0; i < gAnalysisCalcDef.size(); ++i)
   {
      if(gAnalysisCalcDef[i].DoRspMerge()) 
      {
         RspMergeDrv(gAnalysisCalcDef[i]);
      }

      if(gAnalysisCalcDef[i].DoRspIr())
      {
         RspIrDrv(gAnalysisCalcDef[i]);
      }

      if(gAnalysisCalcDef[i].DoLinRspRaman())
      {
         LinRspRamanDrv(gAnalysisCalcDef[i]);
      }
   }
   
   // Output footer
   // ----------------------------------------------------------------------
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Midas Analysis Program ended ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
}
