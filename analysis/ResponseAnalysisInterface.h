/**
************************************************************************
* 
* @file                ResponseAnalysisInterface.h
*
* Created:             15-01-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Interface to handle naming of files and directories in the
*                      response and analysis modules of Midas, such that these separate
*                      parts of the program can communicate 'flawless'-ly.
* 
* Last modified: man mar 21, 2005  10:51
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RESPONSEANALYSISINTERFACE_H_INCLUDED
#define RESPONSEANALYSISINTERFACE_H_INCLUDED

#include <string>

#include "util/Io.h"
#include "util/FileSystem.h"
//#include "input/Input.h"

extern std::string gAnalysisDir;

namespace midas
{
namespace analysis
{

/**
 * Class for handling naming of files and directories for response data
 * to be used in analysis module.
 * This is to avoid hardcoding names everywhere in the code.
 **/
struct rsp_impl
{
   private:
      rsp_impl() = default;
      rsp_impl(const rsp_impl&) = delete;
      rsp_impl& operator=(const rsp_impl&) = delete;

   public:
      // naming strings
      const std::string analysis_name = "rsp";
      const std::string analysis_dir = gAnalysisDir + "/" + analysis_name;
      const std::string main_name = "rsp_func.midas";
      const std::string main_path = analysis_dir + "/" + main_name;
      
      const std::string linear_rsp_vec = "p1rsp";
      const std::string eta_vec = "eta";
      const std::string xi_vec  = "xi";
      
      /**
       *
       **/
      void MakeAnalysisDir()
      {
         if (  !filesystem::IsDir(gAnalysisDir)
            )
         {
            if(filesystem::Mkdir(gAnalysisDir) != 0)
            {
               filesystem::ErrorCheck("Could not create directory '" + gAnalysisDir + "'.", false);
            }
         }

         if (  !filesystem::IsDir(analysis_dir)
            )
         {
            if(filesystem::Mkdir(analysis_dir) != 0)
            {
               filesystem::ErrorCheck("Could not create directory '" + analysis_dir + "'.", false);
            }
         }
      }
      
      // interface for creation
      friend rsp_impl& rsp();
}; /* struct rsp */

/**
 * Interface to get hold of rsp_impl.
 **/
inline rsp_impl& rsp()
{
   static rsp_impl r;
   return r;
}

} /* namespace analysis */
} /* namespace midas */


using namespace midas;

#endif /* RESPONSEANALYSISINTERFACE_H_INCLUDED */
