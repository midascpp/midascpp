/**
************************************************************************
* 
* @file                GenerateMidasPot.h
*
* Created:             12-12-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Generate a MidasPot program.
* 
* Last modified: Wed Nov 18, 2009  01:32PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERATEMIDASPOT_H_INCLUDED
#define GENERATEMIDASPOT_H_INCLUDED

// Forward declarations.
class FileConversionCalcDef;

namespace midas
{
namespace fileconversion
{

//! Generate MidasPot
void GenerateMidasPot(const FileConversionCalcDef&);

//! Generate MidasDynLib
void GenerateMidasDynLib(const FileConversionCalcDef&);

} /* namespace fileconversion */
} /* namespace midas */

#endif /* GENERATEMIDASPOT_H_INCLUDED */
