/**
************************************************************************
* 
* @file                FileConversionDrv.h
*
* Created:             27-02-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Driver for stuff relating to file conversions.
* 
* Last modified: Wed Nov 18, 2009  01:32PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FILECONVERSION_H_INCLUDED
#define FILECONVERSION_H_INCLUDED

void FileConversionDrv();

#endif /* FILECONVERSION_H_INCLUDED */
