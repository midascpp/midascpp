/**
************************************************************************
* 
* @file                FileConversionDrv.cc
*
* Created:             27-02-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Driver for stuff relating to file conversions.
* 
* Last modified: Wed Nov 18, 2009  01:32PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "fileconversion/FileConversionDrv.h"
#include "input/Input.h"
#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeInfo.h"
#include "fileconversion/GenerateMidasPot.h"

/**
 * Driver function for Midas FileConversion module.
 **/
void FileConversionDrv
   (
   )
{
   // output header
   // ----------------------------------------------------------------------
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Midas Molecule File Conversion ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;
   
   // loop over all calc defs in input and run requested conversions
   // ----------------------------------------------------------------------
   for(auto calcdef : gFileConversionCalcDef)
   {
      // determine the type of conversion
      switch(calcdef.GetConversionType())
      {
         case FileConversionCalcDef::ConversionType::MOLECULEFILE:
         {
            Mout << " Converting molecular files\n" << std::flush; 
            molecule::MoleculeInfo molecule(calcdef.InputFiles());
            molecule.WriteMoleculeFile(calcdef.OutputFiles());
            break;
         }
         case FileConversionCalcDef::ConversionType::MIDASPOT:
         {
            Mout << " Generating MidasPot\n" << std::flush;
            midas::fileconversion::GenerateMidasPot(calcdef);
            break;
         }
         case FileConversionCalcDef::ConversionType::MIDASDYNLIB:
         {
            Mout << " Generating MidasDynLib\n" << std::flush;
            midas::fileconversion::GenerateMidasDynLib(calcdef);
            break;
         }
         default:
         {
            MIDASERROR("Unknown FileConversion type.");
         }
      }
   }
   
   // output footer
   // ----------------------------------------------------------------------
   Mout << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Midas Molecule File Conversion ended ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
}
