/**
 *******************************************************************************
 * 
 * @file    GslFunction.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Conversions for the GSL function structs.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifdef ENABLE_GSL

#ifndef GSLFUNCTION_H_INCLUDED
#define GSLFUNCTION_H_INCLUDED

//! @cond
#include <gsl/gsl_roots.h>
//! @endcond

namespace midas::quadrature_wrappers::gsl
{

/***************************************************************************//**
 * @note
 *    The returned `gsl_function` only modifies arFunc if the latter's
 *    operator() does so.
 *
 * @param[in,out] arFunc
 *    Functor (function object) to wrap. Due to the GSL interface cannot be
 *    `const`. But see note above. Cannot be rvalue and remember that the
 *    reference will dangle if arFunc is destructed before the returned
 *    `gsl_function`.
 * @return
 *    A `gsl_function` struct wrapping the argument functor.
 ******************************************************************************/
template<class Func>
gsl_function GslFunctionFromFunctor
   (  Func& arFunc
   )
{
   gsl_function f;
   f.params = &arFunc;
   f.function = 
      [](double x, void* p) -> double
      {
         return (*static_cast<Func*>(p))(x);
      };
   return f;
}

} /* namespace midas::quadrature_wrappers::gsl */

#endif/*GSLFUNCTION_H_INCLUDED*/

#endif   /* ENABLE_GSL */
