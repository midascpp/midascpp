/**
 *******************************************************************************
 * 
 * @file    Integration_Fwd.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Forward declarations of stuff in the namespace pertaining to this
 *    directory.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

namespace midas::quadrature_wrappers
{
   class GaussLegendre;
} /* namespace midas::quadrature_wrappers */
