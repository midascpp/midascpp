/**
 *******************************************************************************
 * 
 * @file    GaussLegendre_Impl.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Wrapper for Gauss-Legendre.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifdef ENABLE_GSL
#include "quadrature_wrappers/function/GslFunction.h"
#endif /* ENABLE_GSL */

namespace midas{
namespace quadrature_wrappers{

/***************************************************************************//**
 * @param[in,out] arFunc
 *    The function to integrate. Must be a functor/function object, i.e. an
 *    object with an implementation of `double operator()(double)`.
 *    arFunc cannot be declared const due to the GSL interface, but is _only_
 *    modified if calling its operator() modifies it.
 * @param[in] aIntervalBegin
 *    Beginning of integration interval.
 * @param[in] aIntervalEnd
 *    End of integration interval.
 * @return
 *    Value of integral.
 ******************************************************************************/
template<class Func>
Nb GaussLegendre::Evaluate
   (  Func& arFunc
   ,  Nb aIntervalBegin
   ,  Nb aIntervalEnd
   )  const
{
#ifdef ENABLE_GSL
   auto f = gsl::GslFunctionFromFunctor(arFunc);
   return gsl_integration_glfixed
      (  &f
      ,  aIntervalBegin
      ,  aIntervalEnd
      ,  mpTable.get()
      );
#else
   Nb bma_half = (aIntervalEnd - aIntervalBegin) * C_I_2;
   Nb bpa_half = (aIntervalEnd + aIntervalBegin) * C_I_2;

   Nb result = C_0;
   for(In i=I_0; i<mNumPoints; ++i)
   {
      const auto& xi = mpTable->first[i];
      const auto& wi = mpTable->second[i];

      result += wi * arFunc(xi*bma_half + bpa_half);
   }
   result *= bma_half;

   return result;
#endif /* ENABLE_GSL */
}
} /* namespace quadrature_wrappers */
} /* namespace midas */
