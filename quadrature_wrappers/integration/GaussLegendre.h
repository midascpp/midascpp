/**
 *******************************************************************************
 * 
 * @file    GaussLegendre.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Wrapper for Gauss-Legendre.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef GAUSSLEGENDRE_H_INCLUDED
#define GAUSSLEGENDRE_H_INCLUDED

#include <memory>

#ifdef ENABLE_GSL
#include <gsl/gsl_integration.h>
#else
#include "mmv/MidasVector.h"
#endif

#include "inc_gen/TypeDefs.h"

namespace midas{
namespace quadrature_wrappers{

/**
 * @brief
 *    Wrapper for Gauss-Legendre fixed order quadrature.
 * 
 * An n-point quadrature will correctly integrate a polynomial of order
 * 2n-1. [GSL manual page](https://www.gnu.org/software/gsl/doc/html/integration.html)
 **/
class GaussLegendre
{
   public:
      //! Construct n-point Gauss-Legendre quadrature rule.
      GaussLegendre(Uin);

      //@{
      //! Default and copy constructor/assignment are deleted.
      GaussLegendre() = delete;
      GaussLegendre(const GaussLegendre&) = delete;
      GaussLegendre& operator=(const GaussLegendre&) = delete;
      //@}

      //@{
      //! Destructor and move constructor/assignment are default.
      GaussLegendre(GaussLegendre&&) = default;
      GaussLegendre& operator=(GaussLegendre&&) = default;
      ~GaussLegendre() = default;
      //@}

      //! Evaluate integral of given functor over the given interval.
      template<class Func> Nb Evaluate(Func&, Nb, Nb) const;

   private:
#ifdef ENABLE_GSL
      //@{
      //! Shorthands for the GSL table struct and associated free function.
      using table_t = gsl_integration_glfixed_table;
      using table_delete_t = void (*)(table_t*);
      //@}
#else
      //@{
      //! Shorthands for the Midas implementation
      using vec_t = MidasVector;
      using table_t = std::pair<vec_t, vec_t>;  // pair<abscissa, weights>
      using table_delete_t = std::default_delete<table_t>;
      //!@}
      
      //! Midas implementation
      std::unique_ptr<table_t> ConstructGaussLegendre
         (
         )  const;
#endif

      //! Number of points of the Gauss-Legendre quadrature.
      Uin mNumPoints;

      //! Table of abscissae and weights for the n-point quadrature.
      std::unique_ptr<table_t, table_delete_t> mpTable;
};

} /* namespace quadrature_wrappers */
} /* namespace midas */

#include "GaussLegendre_Impl.h"

#endif /*GAUSSLEGENDRE_H_INCLUDED*/
