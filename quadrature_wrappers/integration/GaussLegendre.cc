/**
 *******************************************************************************
 * 
 * @file    GaussLegendre.cc
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Wrapper for Gauss-Legendre.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/


#include "quadrature_wrappers/integration/GaussLegendre.h"

#ifndef ENABLE_GSL
#include "util/Math.h"
#endif /* ENABLE_GSL */

using midas::quadrature_wrappers::GaussLegendre;

/***************************************************************************//**
 * Constructs from number of points. Given this number a table of abscissae
 * (x-values) and weights will be allocated and calculated.
 *
 * @param[in] aNumPoints
 *    The number of points for the quadrature rule.
 ******************************************************************************/
GaussLegendre::GaussLegendre
   (  Uin aNumPoints
   )
   :  mNumPoints(aNumPoints)
#ifdef ENABLE_GSL
   ,  mpTable
      (  gsl_integration_glfixed_table_alloc(mNumPoints)
      ,  &gsl_integration_glfixed_table_free
      )
#else
   ,  mpTable
      (  this->ConstructGaussLegendre()
      )
#endif
{
}

#ifndef ENABLE_GSL
/**
 * Initialize abscissa and weights using the Midas GauLeg function.
 *
 * @return
 *    table_t with abscissa and weights
 **/
std::unique_ptr<typename GaussLegendre::table_t> GaussLegendre::ConstructGaussLegendre
   (
   )  const
{
   auto tab = std::make_unique<table_t>();
   tab->first.SetNewSize(mNumPoints);
   tab->second.SetNewSize(mNumPoints);

   midas::math::GauLeg(tab->first, tab->second, -C_1, C_1);

   return tab;
}
#endif /* ENABLE_GSL */
