/*
************************************************************************
*  
* @file                System.cc
*
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Keeps all information about the full system,
*                      including subsystems and submodes 
* 
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "system/System.h"

#include <map>
#include <iostream>
#include <fstream>

#include "pes/molecule/MoleculeInfo.h"
#include "lapack_interface/LapackInterface.h"
#include "mpi/Interface.h"

using namespace midas;

//forward declaration
void EmplaceBackOrthoNormedMidasVector(vector<MidasVector>& arVV, const MidasVector& arV);
set<ModeType> GetSetModeTypeFromGroupModeType(const ModeGroupType& arModeGroupType);
ostream& operator<<(ostream& os,const NucTreatType& arType);

#include "util/conversions/FromString.h"

int extract_first_no(const std::string& aStr)
{
   int exponent_beg_pos = aStr.find_first_of("0123456789");
   int exponent_end_pos = aStr.find_first_not_of("0123456789", exponent_beg_pos);
   exponent_beg_pos = exponent_beg_pos == std::string::npos ? aStr.size() : exponent_beg_pos;
   exponent_end_pos = exponent_end_pos == std::string::npos ? aStr.size() : exponent_end_pos;
   
   int exponent_length = exponent_end_pos - exponent_beg_pos;

   In first_no =(exponent_length ? midas::util::FromString<In>(aStr.substr(exponent_beg_pos, exponent_length))
                                   : 0
                  );
   return first_no;
}         

/* **********************************************************************
*  Construction
********************************************************************** */
System::System()
{
}
/* **********************************************************************
********************************************************************** */
System::System(const SystemDef* const apSysDef)
{
   midas::molecule::MoleculeInfo mol_info(apSysDef->GetMoleculeFileInfo());
  
   In n_nuclei_new=mol_info.GetNumberOfNuclei();
   mNuclei.reserve(n_nuclei_new); 
   vector<Nuclei> nuc=mol_info.GetCoord(); 
   mNuclei.insert(mNuclei.begin(),nuc.begin(),nuc.end());


   switch(apSysDef->GetFragmentType())
   {
      case FragmentType::PREDEFINED:
      {
         InitiateSubSysDefFromNuclei();
         break;
      }  
      case FragmentType::ATOM:
      {
         InitiateAtomSubSysDef();
         break;
      }  
      case FragmentType::ERROR:
      default: 
      {
         MIDASERROR("FragmentType not known");
         break;
      }  
   }
   if (mol_info.HasNormalCoord())
   {
      mModes.reserve(mol_info.GetNoOfVibs());
      std::vector<std::string> tags=mol_info.GetModeNames();
      for (In i =I_0 ; i < mol_info.GetNoOfVibs(); ++i)
      {
         MidasVector full_vec;
         mol_info.GetNormalMode(full_vec,i);
         for (In j=I_0; j< full_vec.size();++j)
         {
            full_vec[j]*=sqrt(mNuclei.at(j/I_3).GetMass());
         }
         mModes.emplace_back(GetFullModeFromVec(full_vec,ModeType::VIB));
         mModes.at(mModes.size()-1).SetTag(tags.at(i));
         mCurrentTagNo=std::max<In>(mCurrentTagNo,extract_first_no(tags.at(i)));
      }
   }
   if (mol_info.HasFreq())
   {  
       MidasAssert(mModes.size()==mol_info.GetNoOfVibs(),"number of found frequencies does not fit to number of modes");
       for (In i = I_0 ; i < mol_info.GetNoOfVibs() ; ++i) 
       {
          mModes.at(i).SetFreq(mol_info.GetFreqI(i)/C_AUTKAYS);
       }
   }
   if (mol_info.HasSigma())
   {
      Mout << " Read in " << mol_info.GetNoOfSigmas() << " sigma vectors " << endl;
      MidasAssert(mol_info.HasNormalCoord(),"Found Sigmas but not corresponding Normal coordinates in System ctor");
      MidasAssert(mol_info.GetNoOfVibs()>=mol_info.GetNoOfSigmas(),"Found more Sigmas than Normal coordinates in System ctor");
      for (In i =I_0 ; i < mol_info.GetNoOfSigmas(); ++i)
      {
         MidasVector full_vec;
         mol_info.GetSigma(full_vec,i);
         for (In j=I_0; j< full_vec.size();++j)
         {
            full_vec[j]*=sqrt(mNuclei.at(j/I_3).GetMass());
         }
         auto sigma_pair=DevideFullVecIntoSubs(full_vec); 
         mModes.at(i).AddSigma(std::move(sigma_pair.first),std::move(sigma_pair.second));
      }
   }
   // If we have a gradient, initialize and mass weight it!
   if (  mol_info.HasGradient()
      )
   {
      auto size = I_3*n_nuclei_new;
      mGradient.SetNewSize(size);
      mGradient = mol_info.GetGradientData().Get();
      for(In i=I_0; i<size; ++i)
      {
         Nb inv_sqrt_mass = C_1 / std::sqrt(mNuclei.at(i/I_3).GetMass()*C_FAMU);
         mGradient[i] *= inv_sqrt_mass;
      }
   }

   // If we have a Hessian, initialize, symmetrize, and mass weight it!
   if (  mol_info.HasHessian()
      )
   {
      mHessian.SetNewSize(I_3*n_nuclei_new,false,true);
      mHessian=mol_info.GetHessianData().Get();
      for (In i =I_0 ; i< mHessian.Nrows(); ++i)
         for (In j = i ; j< mHessian.Ncols(); ++j)
         {
            Nb mass_fac=C_1/(sqrt(mNuclei.at(i/I_3).GetMass()*mNuclei.at(j/I_3).GetMass())*C_FAMU);
            Nb sym=C_1/C_2*(mHessian[i][j]+mHessian[j][i]);
            mHessian[i][j]=sym*mass_fac;
            mHessian[j][i]=sym*mass_fac;
         }
   }

   if (apSysDef->GetCalcModeType() != CalcModeType::NO)
   {
      if (mModes.size() > I_0)
      {
         MidasWarning("Recalculating modes despite read in before - the ones old will be destroyed");
         mModes.clear();
      }
      switch (apSysDef->GetCalcModeType())
      { 
         case CalcModeType::CART:
         {
            InitiateCartesianModes();
            break;
         }
         case CalcModeType::LOCALTRANSROT:
         {
            InitiateLocalTransRot();
            break;
         }
         case CalcModeType::ERROR:
         default: 
         {
            MIDASERROR("ModeType not known");
            break;
         }  
      }     
   }   
   if (apSysDef->AddLocalTransRot())
   {
      AddLocalTransRot();
   }
   if ((apSysDef->GetCalcModeType() != CalcModeType::NO) || (apSysDef->AddLocalTransRot()))
      ReSetModeTags();
}

/* **********************************************************************
*  Initialization
********************************************************************** */
/* **********************************************************************
*  Initialization of the subsystem information from the assignment of the nuclei
********************************************************************** */
void System::InitiateSubSysDefFromNuclei()
{
   mSubSysDef.clear();
   SubSysDefIter it;
   for (In i=I_0; i< mNuclei.size();++i)
   {
      GlobalSubSysNr n_subsys=mNuclei[i].SubSys();
      it=mSubSysDef.find(n_subsys);
      if (it==mSubSysDef.end())
      {
         std::set<GlobalAtomNr> new_set;
         new_set.emplace(i);
         mSubSysDef.emplace(n_subsys,new_set);
      }
      else
      {
         mSubSysDef[n_subsys].emplace(i);
      }
   }   
}
/* **********************************************************************
********************************************************************** */
void System::InitiateAtomSubSysDef()
{
   mSubSysDef.clear();
   for (In i=I_0; i< mNuclei.size();++i)
   {
      std::set<GlobalAtomNr> new_set;
      new_set.emplace(i);
      mNuclei[i].SetSubSys(i);
      mSubSysDef.emplace(i,new_set);
   }   
}
/* **********************************************************************
*  Initialization of the modes as cartesian displacements
********************************************************************** */
void System::InitiateCartesianModes()
{
   mModes.clear();
   mModes.reserve(I_3*mNuclei.size());
   for (SubSysDefIter it_sub=mSubSysDef.begin(); it_sub !=mSubSysDef.end() ; ++it_sub)
   {
      if (ContainsActiveNuc(it_sub->first)) AddCartCoordForSub(it_sub->first);
   } 
    
}

/* **********************************************************************
*  Initialization of local translation and rotations 
********************************************************************** */
void System::InitiateLocalTransRot()
{
   mModes.clear();
   mModes.reserve(I_6*mSubSysDef.size());
   AddLocalTransRot();
}

/* **********************************************************************
*  Modification
********************************************************************** */
void System::AddSubSystem(System&& arrSubSys)
{
   In old_n_no=mNuclei.size();
   // merging subsystemdefintion
   GlobalSubSysNr start_sub_nr=(mSubSysDef.end()->first) + I_1;
   
   
   for (SubSysDefIter it_sub=arrSubSys.mSubSysDef.begin(); it_sub !=arrSubSys.mSubSysDef.end() ; ++it_sub)
   {
      set<GlobalAtomNr> nuc_set;
      for (NucIter it_nuc=it_sub->second.begin(); it_nuc != it_sub->second.end() ; ++it_nuc)
      {
         nuc_set.emplace((*it_nuc)+old_n_no);
      }
      In sub_nr=(it_sub->first)+start_sub_nr;
      auto emplace = mSubSysDef.emplace(sub_nr, nuc_set);
      if (!emplace.second)
      {
         MIDASERROR("Problem occured in the merging of subsystemdefinition");
      }
   } 

   //merging nuclei
   mNuclei.reserve(old_n_no+arrSubSys.mNuclei.size());
   for (In i_nuc=I_0 ; i_nuc < arrSubSys.mNuclei.size() ; ++i_nuc)
   {
      GlobalSubSysNr old_no=arrSubSys.mNuclei[i_nuc].SubSys();
      arrSubSys.mNuclei[i_nuc].SetSubSys(old_no+start_sub_nr);
      mNuclei.emplace_back(std::move(arrSubSys.mNuclei[i_nuc]));
   }
   //merging  modes and frequencies
   mModes.reserve(mModes.size()+arrSubSys.mModes.size());
   for (In i_modes=I_0; i_modes < arrSubSys.mModes.size() ; ++i_modes)
   {
      arrSubSys.mModes[i_modes].ShiftSubSysNrs(start_sub_nr);
      In tag_no = extract_first_no(arrSubSys.mModes[i_modes].Tag());
      /*
      if (tag_no <= mCurrentTagNo)
        MIDASERROR("Possibly same mode numbers ");
      else 
      */
         mCurrentTagNo = tag_no;
      mModes.emplace_back(arrSubSys.mModes[i_modes]);
      //mFrequencies.emplace_back(arrSubSys.mFrequencies[i_modes]);
   }
   //moves hessian if only one system is there
   if (arrSubSys.mHessian.size()>I_0)
   {
      if (old_n_no!=0 || mHessian.size()>I_0)
      {
         MIDASERROR("given Hessians for multiple subsystems not implemented");
      }
      else
      {
         mHessian.SetNewSize(arrSubSys.mHessian.Nrows(),false);
         mHessian.StealData(arrSubSys.mHessian);
      }
   }
   // moves gradient if only one system
   if (  arrSubSys.mGradient.Size() > I_0
      )
   {
      if (old_n_no!=0 || mGradient.Size()>I_0)
      {
         MIDASERROR("given gradients for multiple subsystems not implemented");
      }
      else
      {
         mGradient = std::move(arrSubSys.mGradient);
      }
   }
}
/* **********************************************************************
********************************************************************** */
void System::AddLocalTransRot()
{
  for (SubSysDefIter it_sub=mSubSysDef.begin(); it_sub !=mSubSysDef.end() ; ++it_sub)
  { 
     if (ContainsActiveNuc(it_sub->first))
     {
        Mout << " Add transrot for subsys: " << it_sub->first << endl;
        AddTransForSet(std::set<GlobalSubSysNr>({it_sub->first}));
        AddRotForSet(std::set<GlobalSubSysNr>({it_sub->first}));
     }
  }
}

/* **********************************************************************
********************************************************************** */
void System::AddTransForSet(const set<GlobalSubSysNr>& arSubSysNrs, const string& arName)
{
  vector<Mode> trans_vec = GetTransForSubSys(arSubSysNrs);
  for (In i=I_0 ; i< trans_vec.size(); ++i)
  { 
     InsertMode(std::move(trans_vec[i]),arName);
  }
}
/* **********************************************************************
********************************************************************** */
void System::AddRotForSet(const set<GlobalSubSysNr>& arSubSysNrs, const string& arName)
{
  if (NoOfAtomsInSubs(arSubSysNrs) <= I_1)
  {  
     MidasWarning("Cannot add rotation for one-atomic subsystems, will not try to generate them..");
     return;
  }
  vector<Mode> rot_vec = GetRotForSubSys(arSubSysNrs);
  for (In i=I_0 ; i< rot_vec.size(); ++i)
  { 
     InsertMode(std::move(rot_vec[i]),arName);
  }
  
}
/* **********************************************************************
********************************************************************** */
void System::AddCartCoordForSub(const GlobalSubSysNr& arSubSysNr)
{
   vector<Mode> cart_modes=GetCartesianCoordForSub(arSubSysNr);
   for (In i=I_0 ; i< cart_modes.size(); ++i)
   { 
      InsertMode(std::move(cart_modes[i]));
   } 
}
/* **********************************************************************
********************************************************************** */
void System::IdentifyTRforSubSys(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType)
{
   In atoms_in_subs=NoOfAtomsInSubs(arSubSysNrs);
   set<GlobalModeNr> mode_set=ModesWithinSubs(arSubSysNrs,arModeGroupType);
   set<GlobalModeNr>  vib_set;
   if (atoms_in_subs==I_1)
   {
      for (ModeIter it_mode=mode_set.begin(); it_mode!=mode_set.end(); ++it_mode )
      {
         mModes[*it_mode].SetModeType(ModeType::TRANS);
      }
   }
   
   vector<Mode> trans=GetTransForSubSys(arSubSysNrs); 
   vector<Mode> rot=GetRotForSubSys(arSubSysNrs); 
  
   In n_trans=I_0;
   In n_rot=I_0;
   for (ModeIter it_mode=mode_set.begin(); it_mode!=mode_set.end(); ++it_mode )
   {
       if (std::any_of(trans.begin(),trans.end(),[this,it_mode](Mode mode)
              {return (fabs(ModeDotMode(mode,mModes.at(*it_mode))-C_1)< C_I_10_10);}))
       {
          Mout << " Mode " << *it_mode << " has been identified as local translation " << endl;
          mModes.at(*it_mode).SetModeType(ModeType::TRANS);
          ++n_trans;
       }
       if (std::any_of(rot.begin(),rot.end(),[this,it_mode](Mode mode)
              {return (fabs(ModeDotMode(mode,mModes.at(*it_mode))-C_1)< C_I_10_10);}))
       {
          Mout << " Mode " << *it_mode << " has been identified as local rotation " << endl;
          mModes.at(*it_mode).SetModeType(ModeType::ROT);
          ++n_rot;
       }
       MidasAssert(n_trans+n_rot < I_7, "To many local translations and rotations found");
   }
} 
/* **********************************************************************
********************************************************************** */
set<set<GlobalSubSysNr>> System::RigidSubSysWithinModes(const set<GlobalModeNr>& arModeNr)
{
   set<set<GlobalSubSysNr>> rigid_fragment_groups;
   for (ModeIter it_mode=arModeNr.begin(); it_mode!=arModeNr.end(); ++it_mode )
   { 
      if (mModes.at(*it_mode).GetModeType()==ModeType::VIB || mModes.at(*it_mode).GetModeType()==ModeType::VRT)
         MIDASERROR("RigidSubSysWithinModes does only work for (local) translations and/or rotations");
      set<set<GlobalSubSysNr>> rigid_in_mode=mModes.at(*it_mode).GetRigid();
      rigid_fragment_groups.insert(rigid_in_mode.begin(),rigid_in_mode.end());
   } 
   for (auto it1=rigid_fragment_groups.begin(); it1!=rigid_fragment_groups.end(); ++it1)
   {
      auto it2=it1;
      ++it2;
      while(it2!=rigid_fragment_groups.end())
      {
         vector<GlobalSubSysNr> tst_vec; 
         std::set_intersection(it1->begin(),it1->end(),it2->begin(),it2->end(),std::back_inserter(tst_vec));
         MidasAssert(tst_vec.size()==I_0,"Cannot handle overlaping rigid modes in RigidSubSysWithinModes");
         ++it2;
      }
   }
   return rigid_fragment_groups;
}
/* **********************************************************************
********************************************************************** */
set<GlobalModeNr> System::SeparateOutTRforSubSys(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType, const bool& arKeepTR)
{

   In atoms_in_subs=NoOfAtomsInSubs(arSubSysNrs);
   set<GlobalModeNr> mode_set=ModesWithinSubs(arSubSysNrs,arModeGroupType);
   set<set<GlobalSubSysNr>> rigid_set;
   if(arModeGroupType==ModeGroupType::TRANSROT || arModeGroupType==ModeGroupType::TRANS || arModeGroupType==ModeGroupType::ROT)
       rigid_set=RigidSubSysWithinModes(mode_set);
   set<GlobalModeNr>  vib_set;
   if (atoms_in_subs==I_1)
   {
      for (ModeIter it_mode=mode_set.begin(); it_mode!=mode_set.end(); ++it_mode )
      {
         mModes[*it_mode].SetModeType(ModeType::TRANS);
      }
      Mout << " Do nothing for subsystems of only one atom" << endl;
      return vib_set ; 
   }
   
   // Generate the translations and rotations and set up new matrix

   vector<MidasVector> new_modes;
   new_modes.reserve(I_3*atoms_in_subs);
   vector<GlobalSubSysNr> subsys_vec(arSubSysNrs.begin(),arSubSysNrs.end());
  
   // translations
   vector<Mode> trans=GetTransForSubSys(arSubSysNrs); 
   In n_trans=trans.size();
   for (In i_mode = I_0 ; i_mode < I_3 ; ++i_mode)
   {
      new_modes.emplace_back(GetModeVectorForSubSys(trans[i_mode],subsys_vec));
   }

   // rotations
   vector<Mode> rot=GetRotForSubSys(arSubSysNrs); 
   In n_rot=rot.size();
   for (In i_mode = I_0 ; i_mode < rot.size() ; ++i_mode)
   {
      new_modes.emplace_back(GetModeVectorForSubSys(rot[i_mode],subsys_vec));
   }
 
   // check orthonormality of translation and rotation
   for (In i=I_0 ; i < new_modes.size() ; ++i)
   { 
      if (fabs(new_modes[i].Norm() - C_1) > C_I_10_10)
      {
         MidasWarning (" Renormalize translations or rotations ");
         new_modes[i]/=new_modes[i].Norm();
      }
      for (In j=I_0  ; j < i ; ++j)
      {
         if (fabs(Dot(new_modes[i],new_modes[j])) > C_I_10_10)
         {
            MIDASERROR (" Translations and/or rotations not orthogonal ");
         }
      }
   } 
 
   // orthogonalize vector by vector
   // First try: classic Gram Schmidt
   
   for (ModeIter it_mode=mode_set.begin(); it_mode!=mode_set.end(); ++it_mode )
   {
      //Mout << " Orthogonalizing mode " << *it_mode << endl;
      MidasVector mode=GetModeVectorForSubSys(mModes.at(*it_mode),subsys_vec);
      EmplaceBackOrthoNormedMidasVector(new_modes,std::move(mode)); 
      for (In i=I_0 ; i < new_modes.size()-I_1 ; ++i)
      {
         if (fabs(Dot(new_modes.at(new_modes.size()-I_1),new_modes[i])) > C_I_10_10)
         {
             new_modes.pop_back();
            break;
         } 
      }
   }

   // check orthonormality of translation and rotation
   for (In i=I_0 ; i < new_modes.size() ; ++i)
   { 
      if (fabs(new_modes[i].Norm() - C_1) > C_I_10_10)
      {
         MidasWarning (" Renormalize translations or rotations ");
         new_modes[i]/=new_modes[i].Norm();
      }
      for (In j=I_0  ; j < i ; ++j)
      {
         if (fabs(Dot(new_modes[i],new_modes[j])) > C_I_10_10)
         {
            MIDASERROR (" new modes not orthogonal ");
         }
      }
   } 
 

   if ((new_modes.size() != mode_set.size()))
   {
      Mout << " Number of modes before " << mode_set.size() << std::endl;
      Mout << " Number of new modes  " << new_modes.size() << std::endl;

      MIDASERROR (" Could not obtain the same number of modes in separation of translation and rotation"); 
   }

   In no_of_vibs=mode_set.size();
   //replace the original modes first vibration than translation and rotation
   In n_tr=I_0;
   In n_vib=n_rot+n_trans;
   In n_erased=I_0;
   for (ModeIter it_mode=mode_set.begin(); it_mode!=mode_set.end(); ++it_mode )
   {
      if (mModes[*it_mode].HasSigma())
         MidasWarning("Perform separation of translations and rotations on modes with sigma and thereby loose information");
      if (n_vib  <  no_of_vibs)
      {
         std::string tag=mModes[*it_mode].Tag();
         mModes[*it_mode] = GetModeForSubSys(new_modes[n_vib++],subsys_vec,ModeType::VIB);
         mModes.at(*it_mode).SetRigidSet(rigid_set);
         mModes[*it_mode].SetTag(tag);
         //Mout << " Mode:  " << *it_mode << endl << mModes.at(*it_mode) << endl;
         vib_set.emplace(*it_mode);
      }
      else if (n_tr< n_trans && !arKeepTR) //remove translations and rotations if asked for
      { 
         mModes.erase(mModes.begin() + (In)*it_mode - n_erased++);
      }
      else if (n_tr< n_trans)
      {
         std::string tag=mModes[*it_mode].Tag();
         mModes[*it_mode] = GetModeForSubSys(new_modes[n_tr++],subsys_vec,ModeType::TRANS);
         mModes[*it_mode].SetTag(tag);
      }
      else if (n_tr< n_vib)
      {
         std::string tag=mModes[*it_mode].Tag();
         mModes[*it_mode] = GetModeForSubSys(new_modes[n_tr++],subsys_vec,ModeType::ROT);
         mModes[*it_mode].SetTag(tag);
      }
      else 
      {
         MIDASERROR("Something went wrong in the writing of the orthonormal modes");
      }
  
   }

  return vib_set; 

}

/* **********************************************************************
*  Cartesian coordinates
********************************************************************** */

std::vector<Mode> System::GetCartesianCoordForSub(const GlobalSubSysNr& arSubSysNr)
{
   In n_nuclei=mSubSysDef[arSubSysNr].size();
   vector<Mode> mode_vector;
   mode_vector.reserve(I_3*n_nuclei);
   In n=I_0;
   for (NucIter it_nuc=mSubSysDef[arSubSysNr].begin(); it_nuc !=mSubSysDef[arSubSysNr].end() ; ++it_nuc)
   {
      for (In i=I_0 ; i < I_3 ; ++i)
      { 
         MidasVector mode(I_3*n_nuclei,C_0);
         mode[3*n+i]= C_1;
         mode_vector.emplace_back(arSubSysNr,std::move(mode));
      }
      ++n;
   }
   return  mode_vector;
}

/* **********************************************************************
*  Translation 
********************************************************************** */
vector<Mode> System::GetTransForSubSys(const set<GlobalSubSysNr>& arSubSysNr)  const
{
   Nb total_mass=C_0;
   for (SubSysIter it=arSubSysNr.begin(); it !=arSubSysNr.end(); ++it)
   {
      total_mass+=SubSysMass(*it);
   }

   Nb sqrt_total_mass=sqrt(total_mass);

   vector<Mode> mode_vec;
   mode_vec.reserve(I_3);
   for (In i=I_0 ; i < I_3 ; ++i) //xyz
   { 
      for (SubSysIter it=arSubSysNr.begin(); it !=arSubSysNr.end(); ++it)
      {
         MidasVector mode(I_3*mSubSysDef.at(*it).size(),C_0);
         In n_nuclei=I_0;
         for (NucIter it_nuc=mSubSysDef.at(*it).begin(); it_nuc !=mSubSysDef.at(*it).end() ; ++it_nuc)
         {
            Nb entry=sqrt(mNuclei.at(*it_nuc).GetMass()*C_FAMU)/sqrt_total_mass;
            mode[3*n_nuclei+i]= entry;
            ++n_nuclei;
         }
         if (mode_vec.size()==i) 
         { 
            mode_vec.emplace_back(*it, std::move(mode));
         }
         else if (mode_vec.size()== (i+I_1)) 
         { 
            mode_vec[i].AddSubSystems({*it}, (std::vector<MidasVector>){mode});
            //mode_vec[i].AddSubSystems({*it}, std::move((std::vector<MidasVector>){mode}));
            //^warning: moving a temporary object prevents copy elision [-Wpessimizing-move]
            //-MBH, Feb 2017
         }
      }
      // again to make rigid
      mode_vec[i].SetModeType(ModeType::TRANS);
   }
   return mode_vec;
}
/* **********************************************************************
*  Rotation 
********************************************************************** */
vector<Mode> System::GetRotForSubSys(const set<GlobalSubSysNr>& arSubSysNrs) const 
{
   vector<Mode> mode_vec;
   mode_vec.reserve(I_3);

   MidasMatrix mom=GetIMomForSubSys(arSubSysNrs);
   MidasMatrix evec(mom.Nrows());
   MidasVector eval(mom.Nrows());
   Diag(mom,evec,eval,"DSYEVD");
 
   vector<In> dir_vec;
   dir_vec.reserve(I_3);
   for (In j=I_0; j < eval.size() ; ++j)
   {
      if (fabs(eval[j]) > C_I_10_10)
      {
         dir_vec.emplace_back(j);
      }
      else 
      {
         MidasWarning("Removed a rotation, linear system??");
      }
   }
   
   if (dir_vec.size() < I_2 || dir_vec.size() > I_3 )
   {
      MIDASERROR("Number of rotations does not fit, obtained  " + std::to_string(dir_vec.size()) + " rotations");
   }
   else if (dir_vec.size()==I_2)
   {
      MidasWarning("Taking only 2 rotations into account -- linear system??");
   }
 
   evec.Transpose(); 

   vector<Nb> com=SubSysCom(arSubSysNrs);
   for (SubSysIter it_sub=arSubSysNrs.begin(); it_sub !=arSubSysNrs.end(); ++it_sub)
   {  
      MidasMatrix rot(3,3*mSubSysDef.at(*it_sub).size(),C_0);
      In i=I_0;
      for (NucIter it=mSubSysDef.at(*it_sub).begin(); it !=mSubSysDef.at(*it_sub).end(); ++it)
      { 
         Nb sqrt_mass= sqrt(mNuclei.at(*it).GetMass()*C_FAMU);
         rot[0][I_3*i+I_1]=C_M_1*sqrt_mass*(mNuclei.at(*it).Z()-com[I_2]);
         rot[0][I_3*i+I_2]=sqrt_mass*(mNuclei.at(*it).Y()-com[I_1]);
         rot[1][I_3*i]=sqrt_mass*(mNuclei.at(*it).Z()-com[I_2]);
         rot[1][I_3*i+I_2]=C_M_1*sqrt_mass*(mNuclei.at(*it).X()-com[I_0]);
         rot[2][I_3*i]=C_M_1*sqrt_mass*(mNuclei.at(*it).Y()-com[I_1]);
         rot[2][I_3*i+I_1]=sqrt_mass*(mNuclei.at(*it).X()-com[I_0]);
         ++i;
      }
      MidasMatrix rot2=evec*rot;
      for (In j=I_0; j < dir_vec.size(); ++j)
      {
         Nb scale_fac=C_1/sqrt(eval.at(dir_vec[j]));
         rot2.ScaleRow(scale_fac,dir_vec[j]);
      }
      for (In j=I_0; j < dir_vec.size(); ++j)
      {
         MidasVector mode(3*mSubSysDef.at(*it_sub).size());
         rot2.GetRow(mode,dir_vec[j]);
         if (mode_vec.size() < dir_vec.size()) 
         { 
            mode_vec.emplace_back(*it_sub, std::move(mode));
         }
         else 
         { 
            mode_vec[j].AddSubSystems({*it_sub}, (std::vector<MidasVector>){mode});
            //mode_vec[j].AddSubSystems({*it_sub}, std::move((std::vector<MidasVector>){mode}));
            //^warning: moving a temporary object prevents copy elision [-Wpessimizing-move]
            //-MBH, Feb 2017
         }
      }
   }
 
   //set mode type again in order to update rigid sets
   for_each(mode_vec.begin(),mode_vec.end(),[](Mode& mode){mode.SetModeType(ModeType::ROT);});

   return mode_vec;
}

/* **********************************************************************
********************************************************************** */
MidasMatrix System::GetIMomForSubSys(const set<GlobalSubSysNr>& arSubSysNrs) const 
{

   vector<Nb> com=SubSysCom(arSubSysNrs);
   MidasMatrix mom(I_3,C_0);
   for(In i=0;i<I_3;++i) 
   {
      for(In j=i;j<I_3;++j) 
      {
         for (SubSysIter it_sub=arSubSysNrs.begin(); it_sub !=arSubSysNrs.end(); ++it_sub)
         {  
            for (NucIter it=mSubSysDef.at(*it_sub).begin(); it !=mSubSysDef.at(*it_sub).end(); ++it)
            {
               Nb r_i=C_0;   //initialized
               Nb r_j=C_0;   //initialized
               if(i==0)
                  r_i=mNuclei.at(*it).X();
               else if(i==1)
                  r_i=mNuclei.at(*it).Y();
               else if(i==2)
                  r_i=mNuclei.at(*it).Z();
               if(j==0)
                  r_j=mNuclei.at(*it).X();
               else if(j==1)
                  r_j=mNuclei.at(*it).Y();
               else if(j==2)
                  r_j=mNuclei.at(*it).Z();
               r_i-=com[i];
               r_j-=com[j];
               Nb term=C_M_1*r_i*r_j;
               if (i==j)
               {
                  Nb x=mNuclei.at(*it).X()-com[I_0];
                  Nb y=mNuclei.at(*it).Y()-com[I_1];
                  Nb z=mNuclei.at(*it).Z()-com[I_2];
                  term+= x*x;
                  term+= y*y;
                  term+= z*z;
               }
               term*=C_FAMU*mNuclei.at(*it).GetMass();
               mom[i][j] += term;
               if (i!=j)
                  mom[j][i] += term;
            }
         }
      }
   }
  return mom;
}
/* **********************************************************************
********************************************************************** */
ModeType System::GetGeneralModeType(const set<GlobalModeNr>& arModeNrs)
{
   ModeIter mode_iter=arModeNrs.begin();
   ModeType mode_type=mModes.at(*(mode_iter++)).GetModeType();
   while (mode_iter!=arModeNrs.end())
   {
      if (mode_type!= mModes.at(*(mode_iter++)).GetModeType())
      {
         return ModeType::VRT;
      }
   }
   return mode_type;
}
/* **********************************************************************
* Optimizations
********************************************************************** */
void System::DiagRedHess(const set<GlobalModeNr>& arModeNrs)
{
   ModeType mode_type = GetGeneralModeType(arModeNrs);
   if (mode_type==ModeType::VRT)
      MidasWarning("Mixing modes in optimization");
   HessianType red_hess=GetReducedHessian(arModeNrs);
  
   MidasMatrix evec(red_hess.Nrows());
   MidasVector eval(red_hess.Nrows());
   Diag(static_cast<MidasMatrix&>(red_hess),evec,eval,"DSYEVD");

   //generate rotated modes
   vector<Mode> new_mode_vec;
   new_mode_vec.reserve(arModeNrs.size());
   for (In i=I_0; i < arModeNrs.size() ; ++i)
   {
      In j=I_0;
      ModeIter it=arModeNrs.begin();
      Mode mode=evec[j++][i]*mModes.at(*it++);
      while (it!= arModeNrs.end())
      {
         mode.Add(evec[j++][i]*mModes.at(*it++));
      }
      mode.SetFreq(copysign(sqrt(fabs(eval[i])),eval[i]));
      new_mode_vec.emplace_back(std::move(mode));     
   }
   
    
   In j=I_0;
   for (ModeIter it=arModeNrs.begin(); it!= arModeNrs.end() ; ++it, ++j)
   {
      std::string tag=mModes.at(*it).Tag();
      mModes.at(*it).Clear();
      mModes.at(*it)= new_mode_vec.at(j);
      mModes.at(*it).SetTag(tag);
   }

}
/* **********************************************************************
********************************************************************** */
void System::ReplaceModes(const set<GlobalModeNr>& arModeNrs, const molecule::MoleculeInfo& arMolInfo)
{
   MidasAssert(arModeNrs.size()==arMolInfo.GetNoOfVibs(),"Number of Modes does not fit in Replace Modes");
 
   In k=I_0;
   for (ModeIter it=arModeNrs.begin(); it!= arModeNrs.end() ; ++it, ++k)
   {
      std::string tag=mModes.at(*it).Tag();
      mModes.at(*it).Clear();

      MidasVector full_vec;
      arMolInfo.GetNormalMode(full_vec,k);
      for (In j=I_0; j< full_vec.size();++j)
      {
         full_vec.at(j)*=sqrt(mNuclei.at(j/I_3).GetMass());
      }
      mModes.at(*it) = Mode(GetFullModeFromVec(full_vec,ModeType::VIB));
      mModes.at(*it).SetTag(tag);
      Mout << " set new frequency " << arMolInfo.GetFreqI(k)/C_AUTKAYS << endl;
      mModes.at(*it).SetFreq(arMolInfo.GetFreqI(k)/C_AUTKAYS);
   }
   
}
/* **********************************************************************
********************************************************************** */
MidasVector System::CalcSigmaForMode(const GlobalModeNr& arModeNr)
{
   MidasAssert((mHessian.NumElem()!=I_0)," No Hessian known");

   MidasVector sigma = GetOrderedModeVector(arModeNr);
   sigma.MultiplyWithMatrix(static_cast<MidasMatrix&>(mHessian));
   return sigma;
}
/* **********************************************************************
********************************************************************** */
void System::AddSigmaToMode(const GlobalModeNr& arModeNr, MidasVector&& arSigma, const bool& arMassWeighted)
{
   if (!arMassWeighted)
   {
     for (In j=I_0; j< arSigma.size();++j)
     {
        arSigma.at(j)/=sqrt(mNuclei.at(j/I_3).GetMass())*C_FAMU; 
     }
   } 
   std::pair<std::vector<GlobalSubSysNr>,std::vector<MidasVector>> subs_pair = DevideFullVecIntoSubs(arSigma);
   mModes.at(arModeNr).AddSigma(std::move(subs_pair.first),std::move(subs_pair.second));
}
/* **********************************************************************
********************************************************************** */
void System::UpdateSigmas(const std::set<GlobalModeNr>& arModeNrs)
{
   for (auto it=arModeNrs.begin(); it!=arModeNrs.end(); ++it)
   { 
      //cout << " sigma in mode " <<*it <<  endl;
      if (!mModes.at(*it).HasSigma()) 
      {
         //Mout << mModes.at(*it)  << endl;
         AddSigmaToMode(*it,CalcSigmaForMode(*it));
      }
      else 
      {
         Mout << " sigma found for mode " << *it << endl;
      }
   }
}

/* **********************************************************************
* Sorting
********************************************************************** */
void System::SortModesAfterFreq()
{
   std::multimap<Nb,In> freq_map;
   for (In i = I_0 ; i < mModes.size() ; ++i )
   {
      Nb freq = C_NB_MIN;
      if (mModes.at(i).HasFreq())
      {
         freq = mModes.at(i).Freq();
      }
      freq_map.emplace(freq,i);
   }
  // Sort frequencies and write whether the modes are IC (inter-connecting) or INTRA(intra-fragment) in the output
   Mout << std::endl;
   Out72Char(Mout,'-','-','-');
   Mout << "  Sorting FALCON coordinates after frequencies: " << std::endl;  
   Out72Char(Mout,'-','-','-');
   
   std::vector<Mode> new_mode;
   new_mode.reserve(mModes.size());
   In i = I_0;
   for (std::multimap<Nb, In>::const_iterator it = freq_map.begin(); it != freq_map.end(); ++it)
   {
      Mout << " SubSysInMode().size() is: " << mModes.at(it->second).SubSysInMode().size() << std::endl;
      Mout << " GetRigid().size() is:     " << mModes.at(it->second).GetRigid().size() << std::endl;
      
      std::ofstream ofs;
      ofs.open ("IC_or_INTRA.txt", std::ofstream::out | std::ofstream::app);
      if (mModes.at(it->second).GetRigid().size() >= 2)
      {
         if (i > 5)
         {  
            ofs << i - 5 << "\t" << " IC \n";
         }
         Mout << " Q" << i++ << "  " << it->first << " IC" << std::endl; 
      }
      else
      {
         if (i > 5)
         {  
            ofs << i - 5 << "\t" << " INTRA \n";
         }
         Mout << " Q" << i++ << "  " << it->first << " INTRA" << std::endl; 
      }
      ofs.close();
      new_mode.emplace_back(std::move(mModes.at(it->second))); 
      Out72Char(Mout,'-','-','-');
   }
   mModes = std::move(new_mode);
} 
/* **********************************************************************
********************************************************************** */
void System::SortModes(const vector<set<GlobalSubSysNr>>& arSubSysSetVec)
{
   vector<Nb> new_freq;
   vector<Mode> new_mode;
   new_mode.reserve(mModes.size());
   set<In> mode_nrs_set;
   for (In j=I_0; j < mModes.size(); ++j)
   {
      mode_nrs_set.emplace(j);
   }

   for (In i=I_0 ; i < arSubSysSetVec.size() ; ++i )
   {
      set<In>::iterator it = mode_nrs_set.begin();
      while (it != mode_nrs_set.end())
      {
         set<GlobalSubSysNr> mode_set=mModes.at(*it).SubSysInMode();
         if (std::includes(arSubSysSetVec.at(i).begin(),arSubSysSetVec.at(i).end(),
                mode_set.begin(),mode_set.end()))
         {
            new_mode.emplace_back(std::move(mModes.at(*it)));
            mode_nrs_set.erase(it++);
         }
         else 
         {
            ++it;
         }
      }
   }

   if ((new_mode.size() !=mModes.size()))
   {
      Mout << "modes " << mModes.size() << " " << new_mode.size() <<  endl;
      MIDASERROR("Sorting did not yield the same number of modes ");
   }
   mModes=std::move(new_mode);
   
}
/* **********************************************************************
********************************************************************** */
void System::UpdateSubSys(const std::vector<std::set<GlobalSubSysNr>>& arFGVec)
{
   
   In start_sub_nr=(mSubSysDef.end()->first) + I_1;

   MidasAssert(mModes.empty(),"Cannot update SubSystemNumbers with modes present");
   for (In i=I_0 ; i < mNuclei.size(); ++i)
   {
      for (In j=I_0 ; j < arFGVec.size() ; ++j)
      {
         if (arFGVec.at(j).find(mNuclei.at(i).SubSys())!= arFGVec.at(j).end())
         {
            mNuclei.at(i).SetSubSys(start_sub_nr+j); 
         }   
      }
   }
}
/* **********************************************************************
********************************************************************** */
void System::RemoveTransRotContrNotOrthonorm(const std::set<GlobalSubSysNr>& arSubSysSet, const bool& arKeepRigidInfo) 
{


   // only works if all the modes are really in:
   MidasAssert(ModesWithinSubs(arSubSysSet)==GetGlobalModeSet(),"Cannot remove TR for modes that span more.");

   std::vector<GlobalSubSysNr> subsys_vec(arSubSysSet.begin(),arSubSysSet.end());
   MidasMatrix PrMat=SetUpTRProjectionMatrix(subsys_vec, false);

   for (In i=I_0; i < mModes.size(); ++i)
   {
      //Mout << " --- mode " << i << " ---" << endl; 
      MidasVector mode_vec=GetModeVectorForSubSys(mModes[i],subsys_vec);
      //Mout << " norm before: " << mode_vec.Norm()<< endl; 
      //mode_vec /= mode_vec.Norm();
      mode_vec.MultiplyWithMatrix(PrMat);
      //Mout << " norm after: " << mode_vec.Norm()<< endl; 
      //mode_vec /= mode_vec.Norm();
      auto type=mModes[i].GetModeType();
      std::string tag=mModes[i].Tag();
      std::set<std::set<GlobalSubSysNr>> rigid_set=mModes[i].GetRigid();
      Nb freq=mModes[i].Freq();
      //Mout << " mode before "  << endl;
      //Mout << mModes.at(i) << endl;
      mModes[i]=GetModeForSubSys(std::move(mode_vec),subsys_vec,type);
      mModes[i].SetTag(tag); 
      mModes[i].SetFreq(freq);
      //Mout << " mode after "  << endl;
      //Mout << mModes.at(i) << endl;
      if (arKeepRigidInfo && type!=ModeType::ROT && type!=ModeType::TRANS)
           mModes[i].SetRigidSet(rigid_set);
      else
        MidasWarning("Rigid info got lost on transformation");
   }

}
/* **********************************************************************
*  Analysis
********************************************************************** */
/* **********************************************************************
*  CalcAngles  
********************************************************************** */
void System::CalcAngles() const
{
   Mout << " mode 1     mode 2      |a*b|/|a|*|b| " << endl;
   std::set<GlobalSubSysNr> subsys_set=GetGlobalSubSysSet();
   std::vector<GlobalSubSysNr> subsys_vec(subsys_set.begin(),subsys_set.end());
   for (In i=I_0; i < mModes.size(); ++i)
   {
      MidasVector mode_vec1=GetModeVectorForSubSys(mModes[i],subsys_vec);
      mode_vec1 /=mode_vec1.Norm();
      for (In j=i; j < mModes.size(); ++j)
      {
          MidasVector mode_vec2=GetModeVectorForSubSys(mModes[j],subsys_vec);
          mode_vec2 /=mode_vec2.Norm();
          Mout << i << " " << j << " " << acos(fabs(Dot(mode_vec1,mode_vec2))/mode_vec1.Norm()/mode_vec2.Norm()) << endl;
      }
   }
}
/* **********************************************************************
*  Set up projection matrix following the order in arSubSysVec  
********************************************************************** */
MidasMatrix System::SetUpTRProjectionMatrix
   (  const std::vector<GlobalSubSysNr>& arSubSysVec
   ,  bool aOrthogonalize
   )  const
{
   std::set<GlobalSubSysNr> subsys_set(arSubSysVec.begin(),arSubSysVec.end());

   std::vector<Mode> trans=GetTransForSubSys(subsys_set); 
   std::vector<Mode> rot=GetRotForSubSys(subsys_set); 

   In length= I_3*NoOfAtomsInSubs(subsys_set);
   std::vector<MidasVector> trans_rot;
   trans_rot.reserve(trans.size()+rot.size());
   for (In i=I_0; i < trans.size(); ++i)
   {
      trans_rot.emplace_back(GetModeVectorForSubSys(trans[i],arSubSysVec));
   }
   for (In i=I_0; i < rot.size(); ++i)
   {
      trans_rot.emplace_back(GetModeVectorForSubSys(rot[i],arSubSysVec));
   }

   // Orthonormalize all columns
   if (  aOrthogonalize
      )
   {
      std::vector<MidasVector> ortho_modes;
      ortho_modes.reserve(trans_rot.size());

      for(auto& mode : trans_rot)
      {
         EmplaceBackOrthoNormedMidasVector(ortho_modes, std::move(mode));
         for(In i=I_0; i<ortho_modes.size()-I_1; ++i)
         {
            if (  std::fabs(Dot(ortho_modes.back(), ortho_modes[i])) > C_I_10_10
               )
            {
               ortho_modes.pop_back();
               MidasWarning("Linear dependency in translation/rotation part!");
               break;
            }
         }
      }
      trans_rot = std::move(ortho_modes);
   }

   MidasMatrix PrMat(length,C_0);
   PrMat.Unit();

   for (In i=I_0; i < length ; ++i)
   {
      for (In j=I_0; j < length ; ++j)
      {
          for (In k=I_0; k < trans_rot.size(); ++k)
          {
             PrMat[i][j] -= trans_rot[k][i]*trans_rot[k][j];
          }
      } 
   } 

   return PrMat;
}
MidasMatrix System::SetUpTotalTRProjectionMatrix
   (  bool aOrthogonalize
   )  const
{
   std::vector<GlobalSubSysNr> subsys_vec;
   subsys_vec.reserve(mSubSysDef.size());
   for(const auto& ss : mSubSysDef)
   {
      subsys_vec.emplace_back(ss.first);
   }

   return this->SetUpTRProjectionMatrix(subsys_vec, aOrthogonalize);
}
/* **********************************************************************
********************************************************************** */
LocalAtomNr System::GetLocalAtomNr(const GlobalAtomNr& arGlobalNr) const
{
   LocalAtomNr local_at_no=-I_1;
   GlobalSubSysNr subsys_no=mNuclei.at(arGlobalNr).SubSys();
   In n=I_0;
   for (NucIter it_nuc=mSubSysDef.at(subsys_no).begin(); it_nuc!=  mSubSysDef.at(subsys_no).end(); ++it_nuc)
   {
      if (*it_nuc==arGlobalNr)
      {
         local_at_no=n;
         return local_at_no;
      }   
      ++n;
   }
   
   MIDASERROR("Did not find a local atom number "+std::to_string(arGlobalNr));
   return local_at_no;
}
/* **********************************************************************
********************************************************************** */
Nb System::SubSysMass(const GlobalSubSysNr& arSubSysNr) const
{
   Nb mass_subsys=C_0;
   for (NucIter it=mSubSysDef.at(arSubSysNr).begin(); it !=mSubSysDef.at(arSubSysNr).end(); ++it)
   {
      mass_subsys += mNuclei.at(*it).GetMass()*C_FAMU; 
   }

   return mass_subsys;
}
/* **********************************************************************
********************************************************************** */

vector<Nb> System::SubSysCom(const set<GlobalSubSysNr>& arSubSysNr) const
{
   Nb total_mass=C_0;
   vector<Nb> com{C_0,C_0,C_0};
   for (SubSysIter it=arSubSysNr.begin(); it !=arSubSysNr.end(); ++it)
   {  
      Nb mass=SubSysMass(*it);
      total_mass+=mass;
      vector<Nb> sub_com=SubSysCom(*it);
      for (In i=I_0 ; i < I_3 ; ++i)
      {
         com[i]+=mass*sub_com[i];
      }
   }
   for (In i=I_0 ; i < I_3 ; ++i)
   {
      com[i]/=total_mass;
   }
   return com;
}

/* **********************************************************************
********************************************************************** */

vector<Nb> System::SubSysCom(const GlobalSubSysNr& arSubSysNr) const
{
   
   vector<Nb> com={C_0,C_0,C_0};
   for (NucIter it=mSubSysDef.at(arSubSysNr).begin(); it !=mSubSysDef.at(arSubSysNr).end(); ++it)
   {
      Nb mass=mNuclei.at(*it).GetMass()*C_FAMU;
      com[I_0] += mass*mNuclei.at(*it).X();
      com[I_1] += mass*mNuclei.at(*it).Y();
      com[I_2] += mass*mNuclei.at(*it).Z();
   }

   Nb mass=SubSysMass(arSubSysNr); 

   for (In i=I_0; i < I_3 ; ++i)
   {
      com[i]/=mass;
   }
   return com;
}

/* **********************************************************************
********************************************************************** */

In System::NoOfAtomsInSubs(const set<GlobalSubSysNr>& arSubSysNrs) const
{
   In n_atoms=I_0;
   for (SubSysIter it = arSubSysNrs.begin(); it !=arSubSysNrs.end() ; ++it)
   {
      n_atoms+=mSubSysDef.at(*it).size();
   }
   return n_atoms;
}

/* **********************************************************************
********************************************************************** */

set<GlobalModeNr> System::ModesWithSubs(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType) const
{
   std::set<GlobalModeNr> mode_set;
   std::set<ModeType> type_set=GetSetModeTypeFromGroupModeType(arModeGroupType);
   for (In i=I_0; i < mModes.size() ; ++i)
   {
      ModeType type = mModes[i].GetModeType();
      std::set<ModeType>::const_iterator it=type_set.find(type);
      if (it != type_set.end()) 
      {
         for (SubSysIter it = arSubSysNrs.begin(); it !=arSubSysNrs.end() ; ++it)
         {
            if (mModes[i].IsSubSysIn(*it))
            {
               mode_set.emplace(i);
            }
         }
      }
   }
   return mode_set;
}
/* **********************************************************************
********************************************************************** */

set<GlobalModeNr> System::ModesWithinSubs(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType) const
{
   std::set<GlobalModeNr> mode_set;
   std::set<ModeType> type_set=GetSetModeTypeFromGroupModeType(arModeGroupType);
   for (In i=I_0; i < mModes.size() ; ++i)
   {
      ModeType type = mModes[i].GetModeType();
      std::set<ModeType>::const_iterator it=type_set.find(type);
      if (it != type_set.end()) 
      {  
         set<GlobalSubSysNr> subs_in_mode = mModes[i].SubSysInMode(); 
         if (std::includes(arSubSysNrs.begin(),arSubSysNrs.end(),subs_in_mode.begin(),subs_in_mode.end()))
         {
            mode_set.emplace(i);
         }
      }
   }
   return mode_set;
}
/* **********************************************************************
********************************************************************** */
std::set<std::set<GlobalSubSysNr>> System::SubSysGroups() const
{
   std::set<std::set<GlobalSubSysNr>> group_set;
   // check modes 
   for (In i = I_0 ; i < mModes.size(); ++i)
   {
      std::set<GlobalSubSysNr> set= mModes[i].SubSysInMode();
      auto it_gs=group_set.begin();
      while (it_gs!=group_set.end())
      {
         std::set<GlobalSubSysNr> intersect;
         std::set_intersection(it_gs->begin(),it_gs->end(),
             set.begin(),set.end(), std::inserter(intersect,intersect.end()));
         if (intersect.size()!=I_0)
         {
            set.insert(it_gs->begin(),it_gs->end());
            it_gs = group_set.erase(it_gs);
         }
         else
         {
            ++it_gs;
         }
      }
      group_set.emplace(set);
   }
   // then whether we have all subsystems
   for (SubSysDefIter it=mSubSysDef.begin(); it!=mSubSysDef.end(); ++it)
   {
      if (none_of(group_set.begin(),group_set.end(),[it](std::set<GlobalSubSysNr> set)
             {return (set.find(it->first)!=set.end());}))
      {
         std::set<GlobalSubSysNr> set={it->first};
         group_set.emplace(set);
      }      
   }
   return group_set;
}
/* **********************************************************************
* returns mode vector blocked for the different subsystems
********************************************************************** */

MidasVector System::GetModeVectorForSubSys(const Mode& arMode, const vector<GlobalSubSysNr>& arSubVec) const
{
   //getting also set of subsystems !! all ordered things needs to be done with the vector!
   set<GlobalSubSysNr> sub_set(arSubVec.begin(),arSubVec.end());
   // check whether mode really only spans the subsyss given 
   set<GlobalSubSysNr> subs_in_mode=arMode.SubSysInMode();
   if (!std::includes(sub_set.begin(),sub_set.end(),subs_in_mode.begin(),subs_in_mode.end()))
   {
      MIDASERROR("Coordinates are not localized");
   }

   // generate vector 
   In n_atoms=NoOfAtomsInSubs(sub_set);
   MidasVector mode_vec(I_3*n_atoms,C_0);
   In start=I_0;
   for (In i=I_0 ; i < arSubVec.size() ; ++i)
   { 
      if (arMode.IsSubSysIn(arSubVec[i]))
      {
         mode_vec.FillPart(arMode.GetSubMode(arSubVec[i]),start);
      }
      start += I_3*mSubSysDef.at(arSubVec[i]).size();
   }
   return mode_vec;
}

/**
 * Returns subvector in the order of the nuclei vector
 *
 * @param arMode
 * @param arSubSet
 * @param arMassWeight        If true, return column vector of Y = M^{-1/2} L, where L is the set of orthonormal vibrational coordinates.
 *                            Else, return column of L.
 * @return
 *    Returns mode vector (perhaps weighted with inverse sqrt of mass)
 **/
MidasVector System::GetOrderedModeVectorForSubSys(const Mode& arMode, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight) const
{
   set<GlobalSubSysNr> subs_in_mode=arMode.SubSysInMode();
   if (!std::includes(arSubSet.begin(),arSubSet.end(),subs_in_mode.begin(),subs_in_mode.end()))
   {
      //MIDASERROR("Coordinates are not localized");
   }

   // generate vector
   In n_atoms=NoOfAtomsInSubs(arSubSet);
   MidasVector mode_vec(I_3*n_atoms,C_0);
   In entry=I_0;
   for (In i=I_0 ; i < mNuclei.size() ; ++i)
   { 
      if (arSubSet.find(mNuclei[i].SubSys())!= arSubSet.end())
      {
         for (In j=I_0;j<I_3;++j)
         {
            mode_vec[entry++]=arMode.Entry(mNuclei[i].SubSys(),GetLocalAtomNr(i),j);
            if (!arMassWeight) mode_vec[entry-I_1]/=sqrt(mNuclei.at(i).GetMass());
         }
      }
   }
   return mode_vec;
}
/* **********************************************************************
********************************************************************** */
MidasVector System::GetOrderedSigmaVectorForSubSys(const Mode& arMode, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight) const
{
   set<GlobalSubSysNr> subs_in_mode=arMode.SubSysInMode();
   if (!std::includes(arSubSet.begin(),arSubSet.end(),subs_in_mode.begin(),subs_in_mode.end()))
   {
      MIDASERROR("Sigma is not localized");
   }

   // generate vector
   In n_atoms=NoOfAtomsInSubs(arSubSet);
   MidasVector mode_vec(I_3*n_atoms,C_0);
   In entry=I_0;
   for (In i=I_0 ; i < mNuclei.size() ; ++i)
   { 
      if (arSubSet.find(mNuclei[i].SubSys())!= arSubSet.end())
      {
         for (In j=I_0;j<I_3;++j)
         {
            mode_vec[entry++]=arMode.SigmaEntry(mNuclei[i].SubSys(),GetLocalAtomNr(i),j);
            if (!arMassWeight) mode_vec[entry-I_1]/=sqrt(mNuclei.at(i).GetMass());
         }
      }
   }
   return mode_vec;
}
/* **********************************************************************
* DivideFullVecIntoSubs 
********************************************************************** */
std::pair<std::vector<GlobalSubSysNr>,std::vector<MidasVector>> System::DevideFullVecIntoSubs(const MidasVector& arModeVec) const
{
   if (arModeVec.size()!= I_3*mNuclei.size())
   {
      MIDASERROR("Dimension of Mode and System does not match "+std::to_string(arModeVec.size())+" "+std::to_string(I_3*mNuclei.size()));
   }
   std::vector<MidasVector> sub_modes;
   std::vector<GlobalSubSysNr> subsys;
   sub_modes.reserve(mSubSysDef.size());
   subsys.reserve(mSubSysDef.size());
   for (SubSysDefIter it_sys=mSubSysDef.begin(); it_sys!=mSubSysDef.end() ; ++it_sys)
   {
      MidasVector sub_mode_vec(I_3*it_sys->second.size());
      In entry=I_0;
      for (NucIter it_nuc=(it_sys->second).begin(); it_nuc!=(it_sys->second).end(); ++it_nuc)
      {
         for (In i=I_0;i<I_3;++i)
         {
            sub_mode_vec[entry++]=arModeVec.at(I_3*(*it_nuc)+i);
         }
      }
      if (sub_mode_vec.Norm()> C_I_10_16)
      {
         sub_modes.emplace_back(std::move(sub_mode_vec));
         subsys.emplace_back(it_sys->first);
      }
      else
      {
         MidasWarning("Neglected contributions form subsystem "+std::to_string(it_sys->first));
      }
   } 
   return std::make_pair(subsys,sub_modes);
}
 
/* **********************************************************************
* GetFullModeFromVec
********************************************************************** */
Mode System::GetFullModeFromVec(const MidasVector& arModeVec, const ModeType& arModeType)
{
   std::pair<std::vector<GlobalSubSysNr>,std::vector<MidasVector>> sub_mode= DevideFullVecIntoSubs(arModeVec);
   return Mode(sub_mode.first, std::move(sub_mode.second),arModeType );
}
 
/* **********************************************************************
* GetModeForSubSys -- only works for nicely ordered MidasVectors
********************************************************************** */
Mode System::GetModeForSubSys(const MidasVector& arModeVec, const vector<GlobalSubSysNr>& arSubVec, const ModeType& arModeType)
{
   //getting also set of subsystems !! all ordered things needs to be done with the vector!
   set<GlobalSubSysNr> mode_set(arSubVec.begin(),arSubVec.end());
   In n_atoms=NoOfAtomsInSubs(mode_set);

   // generate vector 
   Mode mode(arModeType);
   In start=I_0;
   for (In i=I_0 ; i < arSubVec.size() ; ++i)
   {  
     In end= start+(I_3*mSubSysDef[arSubVec[i]].size())-I_1;
     mode.AddSubSystems({arSubVec[i]},{arModeVec.GetSubVec(start,end)});
     start += I_3*mSubSysDef[arSubVec[i]].size();
   }
   // reset the mode type so that trans and rot get correct rigid grouping
   mode.SetModeType(arModeType);
   return mode;
}

/* **********************************************************************
********************************************************************** */

Nb System::GetNormForModeNoMassWeight(const GlobalModeNr& arModeNr)
{
   Nb norm=C_0;
   set<GlobalSubSysNr> subsys_set=mModes[arModeNr].SubSysInMode();
   for (SubSysIter it_sub = subsys_set.begin(); it_sub !=subsys_set.end() ; ++it_sub)
   {
      In entry=C_0;
      MidasVector sub_vec= mModes[arModeNr].GetSubMode(*it_sub);
      for(NucIter it_nuc=mSubSysDef[*it_sub].begin(); it_nuc!=mSubSysDef[*it_sub].end(); ++it_nuc)
      {  
         for (In i = I_0 ; i < I_3 ; ++i )
         {
            sub_vec[entry]=sub_vec[entry]/sqrt(mNuclei[*it_nuc].GetMass()*C_FAMU);
            ++entry;
         }
      }
      norm += sub_vec.Norm();
   }
   return norm;
}
/* **********************************************************************
********************************************************************** */
Nb System::MinNucDistAct(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const
{
   Nb min_dist=C_NB_MAX;
   for (NucIter it1 = mSubSysDef.at(arSys1).begin(); it1 != mSubSysDef.at(arSys1).end() ; ++ it1)
   {
      for (NucIter it2 = mSubSysDef.at(arSys2).begin(); it2 != mSubSysDef.at(arSys2).end() ; ++ it2)
      { 
         if (mNuclei.at(*it1).IsActive() || mNuclei.at(*it2).IsActive())
         {
           Nb dist= mNuclei.at(*it1).Distance(mNuclei.at(*it2));
           min_dist=min(dist,min_dist);  
         }
      }
   }
   return min_dist;
}
/* **********************************************************************
********************************************************************** */
Nb System::MinNucDist(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const
{
   Nb min_dist=C_NB_MAX;
   for (NucIter it1 = mSubSysDef.at(arSys1).begin(); it1 != mSubSysDef.at(arSys1).end() ; ++ it1)
   {
      for (NucIter it2 = mSubSysDef.at(arSys2).begin(); it2 != mSubSysDef.at(arSys2).end() ; ++ it2)
      {
         Nb dist= mNuclei.at(*it1).Distance(mNuclei.at(*it2));
         min_dist=min(dist,min_dist);  
      }
   }
   return min_dist;
}
/* **********************************************************************
********************************************************************** */
Nb System::MaxNucCoupNormAct(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const
{
   Nb max_norm=C_0;
   for (NucIter it1 = mSubSysDef.at(arSys1).begin(); it1 != mSubSysDef.at(arSys1).end() ; ++ it1)
   {
      for (NucIter it2 = mSubSysDef.at(arSys2).begin(); it2 != mSubSysDef.at(arSys2).end() ; ++ it2)
      {
         if (mNuclei.at(*it1).IsActive() || mNuclei.at(*it2).IsActive())
         {
            Nb norm= NucCoupNorm(*it1,*it2);
            max_norm=max(norm,max_norm);  
         }
      }
   }
   return sqrt(max_norm);
}
/* **********************************************************************
********************************************************************** */
Nb System::MaxNucCoupNorm(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const
{
   Nb max_norm=C_0;
   for (NucIter it1 = mSubSysDef.at(arSys1).begin(); it1 != mSubSysDef.at(arSys1).end() ; ++ it1)
   {
      for (NucIter it2 = mSubSysDef.at(arSys2).begin(); it2 != mSubSysDef.at(arSys2).end() ; ++ it2)
      {
         Nb norm= NucCoupNorm(*it1,*it2);
         max_norm=max(norm,max_norm);  
      }
   }
   return sqrt(max_norm);
}
/* **********************************************************************
********************************************************************** */
Nb System::NucCoupNorm(const GlobalAtomNr& arAt1, const GlobalAtomNr& arAt2) const
{
    MidasAssert((mHessian.NumElem()!=0),"Hessian Information not available to calculate norm");
    Nb norm=C_0;
    for (In i = I_0; i < I_3 ; ++i)
       for (In j = I_0; j < I_3 ; ++j)
       { 
          Nb elem=mHessian[(arAt1)*I_3+i][(arAt2)*I_3+j];
          norm+= elem*elem; 
       }
    return sqrt(norm);
}
/* **********************************************************************
********************************************************************** */
HessianType System::GetReducedHessian(const set<GlobalModeNr>& arModeNrs) 
{
   HessianType red_hess(MidasMatrix(arModeNrs.size()));
   Nb max_diff=C_0;
   
   In i=I_0;
   In j=I_0;

   for (ModeIter it=arModeNrs.begin(); it!= arModeNrs.end() ; ++it)
   {
      MidasAssert(mModes.at(*it).HasSigma(),"Cannot generate Reduced Hessian without sigma");
      j=i;
      for (ModeIter it2=it ; it2!= arModeNrs.end()  ; ++it2)
      {
         Nb entry1=ModeDotSigma(mModes.at(*it2),mModes.at(*it));
         Nb entry2=ModeDotSigma(mModes.at(*it),mModes.at(*it2));
         Nb diff = fabs(entry1-entry2);
         max_diff=max(diff,max_diff);
         MidasAssert(diff<C_I_10_4," Unsymmetric Hessian > 1.0e-4 ");
         Nb entry=0.5e0*(entry1+entry2);
         red_hess[i][j]= entry; 
         red_hess[j][i]= entry; 
         ++j;
      }
      ++i;
   }
   Mout << " Maximal abs(Hij-Hji) = " <<  max_diff << endl;
   return red_hess;
}
/* **********************************************************************
********************************************************************** */
vector<MidasVector> System::GetOrderedModeVectors(const set<GlobalModeNr>& arModeNrs, const bool& global, const bool& arMassWeight) const
{
   set<GlobalSubSysNr> subsys_set;
   if (global)
   {
      subsys_set=GetGlobalSubSysSet();
   }
   else
   {
      subsys_set=GetSubSysInModeSet(arModeNrs);
   }
   vector<MidasVector> mode_vec;
   mode_vec.reserve(arModeNrs.size());
   for (ModeIter it=arModeNrs.begin(); it!=arModeNrs.end() ; ++it)
   {
      mode_vec.emplace_back(GetOrderedModeVectorForSubSys(mModes.at(*it), subsys_set, arMassWeight));
   }
   return mode_vec;
}
/* **********************************************************************
********************************************************************** */
set<GlobalSubSysNr> System::GetSubSysInModeSet(const set<GlobalModeNr>& arModeNrs) const
{
   set<GlobalSubSysNr> subsys_set;
   for (ModeIter it=arModeNrs.begin(); it!=arModeNrs.end() ; ++it)
   {
      set<GlobalSubSysNr> subsys_set_mode=mModes.at(*it).SubSysInMode();
      subsys_set.insert(subsys_set_mode.begin(),subsys_set_mode.end());
   }
   return subsys_set;
}
/* **********************************************************************
*  Printing
********************************************************************** */
void System::PrintSubSysDef()  const
{
   for (SubSysDefIter it=mSubSysDef.begin(); it != mSubSysDef.end(); ++it)
   {
      Mout << "SubSystem " << it->first << " contains the nuclei ";
      for (NucIter it2=it->second.begin(); it2 !=it->second.end(); ++it2)
         Mout << "  " <<  *it2;
      Mout << endl;
   }
}
/* **********************************************************************
*  Writing information 
********************************************************************** */
void System::WriteRedHess(const set<GlobalModeNr>& arModeNrs,const string& arFileName)
{
   if (NoOfModesWithSigma()==I_0 || (midas::mpi::get_Interface().GetMpiGlobalRank() != I_0) ) return;
   ofstream output(arFileName);
   output.setf(ios::scientific);
   output.precision(I_12);

   // plain writing it out now. it would probably more elegant to make an opdef from it and write this??
   ofstream outputop(arFileName+".mop");
   outputop.setf(ios::scientific);
   outputop.precision(I_12);

   outputop << "#0MIDASOPERATOR" << endl; 
   outputop << "#1MODENAMES" << endl; 
   for (In i=0 ; i < arModeNrs.size() ; i++)
      outputop << "Q" << i << " " ;
   outputop << endl; 
   outputop << "#1SCALEFACTORS" << endl; 
   for (In i=0 ; i < arModeNrs.size() ; i++)
      outputop << 1.0e0 << " " ;
   outputop << endl; 
   outputop << "#1OPERATORTERMS" << endl; 
   HessianType red_hess=GetReducedHessian(arModeNrs);
   for (int i=0 ; i < red_hess.Ncols() ; ++i)
   {
      for (int j=0 ; j < red_hess.Nrows() ; ++j)
      {
         output << " " << red_hess[i][j] << " " ;
         if (i==j)
         {
            outputop<< 0.5e0 * red_hess[i][j] << " Q^2(Q"<<i<<")" << endl;
         }
         else if (i<j)
         {
            outputop<< red_hess[i][j] << " Q^1(Q"<<i<<") Q^1(Q" << j << ")" << endl;
         }
      }
      output << endl;
   }
   
   outputop << "#0MIDASOPERATOREND" << endl; 
}
/* **********************************************************************
*  Writing information  -- Midas format
********************************************************************** */
void System::Write
   (  const std::string& arFileName
   ,  const std::string& arFileType
   ,  const bool& arOnlyVibs
   )
{
   if (midas::mpi::get_Interface().GetMpiGlobalRank() == I_0)
   {
      std::set<GlobalModeNr> mode_set;
      if (arOnlyVibs)
      {
         mode_set = GetGlobalVibSet();
      }
      else  
      {
         mode_set = GetGlobalModeSet();
      }
      midas::molecule::MoleculeInfo mol_info(*this, GetGlobalSubSysSet(), mode_set);
      
      mol_info.WriteMoleculeFile(molecule::MoleculeFileInfo(arFileName, arFileType));
   }
}

/* **********************************************************************
*  Limit to a subsystem
********************************************************************** */
System System::LimitToSubSys
   (  const std::set<GlobalSubSysNr>& arSubSysSet
   ,  const bool& KeepRot
   )  const
{
   //first nuclei 
   std::vector<Nuclei> nuc_vec;
   nuc_vec.reserve(NoOfAtomsInSubs(arSubSysSet));  

   for (auto it=arSubSysSet.begin(); it!=arSubSysSet.end(); ++it)
   {
      std::for_each(mSubSysDef.at(*it).begin(),mSubSysDef.at(*it).end(),[this, &nuc_vec] (GlobalAtomNr no)
             {nuc_vec.emplace_back(mNuclei.at(no));});
   }

   std::vector<Mode> mode_vec;
   mode_vec.reserve(mModes.size());
   for (In i=I_0; i < mModes.size(); ++i)
   {
       //bool include =!mModes.at(i).IsRigidInFc(arSubSysSet);
       bool include = (std::any_of(arSubSysSet.begin(),arSubSysSet.end(),[this, i](const GlobalSubSysNr& no){return mModes[i].IsSubSysIn(no);}) && !mModes[i].IsRigidInFc(GetGlobalSubSysSet()));
       if (KeepRot && mModes[i].GetModeType()==ModeType::ROT)
         include=true;
       if (include) 
       {
          //Mout << " Non-rigid mode : " << i << endl;
          mode_vec.emplace_back(mModes[i]);
          //Mout <<  mode_vec.at(mode_vec.size()-I_1) << endl;
          mode_vec.at(mode_vec.size()-I_1).LimitToSubSysSet(arSubSysSet);
          //Mout <<  mode_vec.at(mode_vec.size()-I_1) << endl;
       }
   }
   System subsystem(std::move(nuc_vec),std::move(mode_vec));
   return subsystem;
}
/* **********************************************************************
* Capping subsystems 
********************************************************************** */
System System::GetNRCappedSubSystem
   (   const std::set<GlobalSubSysNr>& arSubSysSet
   ,   const bool                      aDoCapping
   ,   const bool                      aProjectOutTR
   ,   const bool                      aKeepRot 
   ) const
{
   
   System subsystem=LimitToSubSys(arSubSysSet,aKeepRot);
  
   if (aProjectOutTR)
   {
      subsystem.RemoveTransRotContrNotOrthonorm(arSubSysSet,true); 
      MidasWarning("Removing translational and rotational parts.");
   }

   std::vector<GlobalAtomNr> nuc_no_vec;
   nuc_no_vec.reserve(NoOfAtomsInSubs(arSubSysSet));  
   
   // nuclei ordered according to subsystems 
   for (auto it=arSubSysSet.begin(); it!=arSubSysSet.end(); ++it)
   {
      nuc_no_vec.insert(nuc_no_vec.end(),mSubSysDef.at(*it).begin(),mSubSysDef.at(*it).end());
   }

   // Capping might fail for non-covalently bonded molecules, it is nice to be able to turn it off
   if (aDoCapping)
   {
      std::vector<Nuclei> cap_nuc_vec= CapNucVec(nuc_no_vec);
      // prepare the capping subsystem:
      if (cap_nuc_vec.size()>I_0)
      {
         std::set<GlobalAtomNr> cap_no_set;
         for (In i = subsystem.mNuclei.size() ; i < subsystem.mNuclei.size()+cap_nuc_vec.size(); ++i )
         {
            cap_no_set.emplace(i);
         }
         
         subsystem.mNuclei.insert(subsystem.mNuclei.end(),cap_nuc_vec.begin(),cap_nuc_vec.end());
         subsystem.mSubSysDef.emplace(cap_nuc_vec[I_0].SubSys(),cap_no_set);

         subsystem.AddCappingMotions(cap_nuc_vec[I_0].SubSys());
      }
   }
   
   return subsystem;
}
/* **********************************************************************
********************************************************************** */
void System::AddCappingMotions(const GlobalSubSysNr& arNo)
{
   const Nb bond_thresh=C_3;
   std::map<GlobalAtomNr,std::vector<GlobalAtomNr>> connections;
   //first find connections
   for (auto it=mSubSysDef.at(arNo).begin(); it !=mSubSysDef.at(arNo).end(); ++it)
   {
      Mout << " finding connections for " << *it << endl;
      std::vector<GlobalAtomNr> conn_at;
      for (In i=I_0 ; i< mNuclei.size(); ++i)
      {
         if (mNuclei.at(i).SubSys()==arNo) continue;
         if (mNuclei.at(*it).Distance(mNuclei.at(i)) < bond_thresh)
         {
            conn_at.emplace_back(i);
            Mout <<  i << endl;
         }
      } 
      connections.emplace(*it,conn_at); 
   }

   //check those without direct connections
   for (auto it_conn=connections.begin(); it_conn != connections.end() ; ++it_conn)
   {
      if (it_conn->second.size()>I_0) continue;
      for (auto it=mSubSysDef.at(arNo).begin(); it !=mSubSysDef.at(arNo).end(); ++it)
      {
         if (it_conn->first==*it) continue;
         if (mNuclei.at(it_conn->first).Distance(mNuclei.at(*it)) < bond_thresh)
         {
            MidasAssert(connections.at(*it).size()>I_0,"Cannot handle the capping situtation");
            it_conn->second.insert(it_conn->second.end(),connections.at(*it).begin(),connections.at(*it).end());
         }
      }
   }

   // then loop over the modes and get the stuff in
   for (In mode=I_0; mode < mModes.size(); ++mode)
   {
      MidasVector capping_motion(I_3*mSubSysDef.at(arNo).size(),C_0);
      In conn=I_0;
      for (auto it_conn=connections.begin(); it_conn != connections.end() ; ++it_conn)
      {
         if (mModes[mode].IsSubSysIn(arNo))
            MIDASERROR("Subsystem already has modes, sure that it is capping??");
         for(auto ca=it_conn->second.begin(); ca!=it_conn->second.end(); ++ca)
         {
            for (In i = I_0; i < I_3 ;++i)
               capping_motion[I_3*conn+i]+=mModes[mode].Entry(mNuclei.at(*ca).SubSys(),GetLocalAtomNr(*ca),i)/sqrt(mNuclei.at(*ca).GetMass()); 
         }
         if (it_conn->second.size() < I_1)
         {
            MidasWarning("Found no connections to a capped atom, FIXME!!!");
         }
         else 
         {
            for (In i = I_0; i < I_3 ;++i)
               capping_motion[I_3*conn+i]*=sqrt(mNuclei.at(it_conn->first).GetMass())/it_conn->second.size();
         }
         ++conn;
      }
      mModes[mode].AddSubSystems({arNo}, (std::vector<MidasVector>){capping_motion});
      //^warning: moving a temporary object prevents copy elision [-Wpessimizing-move]
      //-MBH, Feb 2017
   }
}
/* **********************************************************************
********************************************************************** */
std::vector<Nuclei> System::CapNucVec(const std::vector<GlobalAtomNr>&  arNucSet) const
{
   // bond lenghts from J. Chem. Soc. Perkin Trans. II 1987.
   const std::map<In,Nb> bond_length
   {
      {6,1.059/C_TANG},  // csp3-H 
      {7,1.009/C_TANG},  // H-N(3)
      {8,0.967/C_TANG},  // H-O(2) for alcohols
   };
   Nb bond_thresh=C_3;
   //for the water octamer:
   //Nb bond_thresh=1.2e0;
   std::vector<Nuclei> nuc_vec;
   nuc_vec.reserve(arNucSet.size());  // also here, no particular reason for that number, too big??
   for (In j=I_0 ; j < mNuclei.size(); j++)
   {
      if (std::find(arNucSet.begin(),arNucSet.end(), j) != arNucSet.end()) 
      {
         continue;
      }
      //if (std::any_of(arNucSet.begin(),arNucSet.end(),[this, j, bond_thresh](GlobalAtomNr no)
      //{return (mNuclei.at(no).Distance(mNuclei.at(j)) < bond_thresh);}))
      std::vector<GlobalAtomNr> con_vec;
      for (In k=I_0; k < arNucSet.size(); ++k)
      {
         if (mNuclei.at(arNucSet[k]).Distance(mNuclei[j]) < bond_thresh)
           con_vec.emplace_back(arNucSet[k]);
      }
      if (con_vec.size()==I_1)
      {
         nuc_vec.emplace_back(mNuclei[j]);
         nuc_vec.at(nuc_vec.size()-I_1).SetSubSys((mSubSysDef.rbegin()->first) + I_1);
         nuc_vec.at(nuc_vec.size()-I_1).SetNucTreatType(NucTreatType::CAP);
         // Mout << " before change " << endl;
         // Mout <<  nuc_vec.at(nuc_vec.size()-I_1) << endl;
         nuc_vec.at(nuc_vec.size()-I_1).SetZnuc(I_1);
         nuc_vec.at(nuc_vec.size()-I_1).SetQ(C_1);
         string h="h";
         nuc_vec.at(nuc_vec.size()-I_1).SetAtomLabel(h);
         nuc_vec.at(nuc_vec.size()-I_1).SetAnuc(0);
         // adapt bond length
         if (mNuclei.at(j).Znuc()!=I_1)
         {
            Nb dx=mNuclei[j].X()- mNuclei.at(con_vec.at(I_0)).X();
            Nb dy=mNuclei[j].Y()- mNuclei.at(con_vec.at(I_0)).Y();
            Nb dz=mNuclei[j].Z()- mNuclei.at(con_vec.at(I_0)).Z();
            
            // We don't know how to cap atoms other than C,O,N!
            //Nb fac=bond_length.at(mNuclei.at(con_vec.at(I_0)).Znuc());
            
            Nb fac = C_0;
            auto pos = bond_length.find(mNuclei.at(con_vec.at(I_0)).Znuc());
            if (pos != bond_length.end())
            {
               fac=pos->second;
            }
            else 
            {
               MIDASERROR
                  (   "I don't know how to cap a nucleus with the charge of " 
                  +   std::to_string(mNuclei.at(con_vec.at(I_0)).Znuc())
                  + ". Turn off capping or teach me how!"
                  );
            } 
            
            fac/=mNuclei.at(con_vec.at(I_0)).Distance(mNuclei[j]);
            nuc_vec.at(nuc_vec.size()-I_1).SetX(mNuclei.at(con_vec.at(I_0)).X()+fac*dx);
            nuc_vec.at(nuc_vec.size()-I_1).SetY(mNuclei.at(con_vec.at(I_0)).Y()+fac*dy);
            nuc_vec.at(nuc_vec.size()-I_1).SetZ(mNuclei.at(con_vec.at(I_0)).Z()+fac*dz);
         }
         // Mout << " after change " << endl;
         //Mout <<  nuc_vec.at(nuc_vec.size()-I_1) << endl;
      }
      // if there is more than one connection, we add the original atom and then redo the whole thing..
      else if(con_vec.size() > I_1)
      {
         MidasWarning("Capping atom with more than one neighboring atom");
         std::vector<GlobalAtomNr> new_nuc_set;
         new_nuc_set.assign(arNucSet.begin(),arNucSet.end());
         new_nuc_set.emplace_back(j);
         nuc_vec.clear();
         nuc_vec=CapNucVec(new_nuc_set);
         nuc_vec.emplace_back(mNuclei[j]);
         nuc_vec.at(nuc_vec.size()-I_1).SetSubSys((mSubSysDef.rbegin()->first) + I_1);
         nuc_vec.at(nuc_vec.size()-I_1).SetNucTreatType(NucTreatType::CAP);
         break;
      }
   } 
   return nuc_vec;
}
/* **********************************************************************
********************************************************************** */

bool System::IsOrthonormal(const Nb& arThresh) const 
{
   Mout << " Checking orthonormality of the system " << endl;
   bool orthn=true;
   set<GlobalSubSysNr> subsys_set=GetGlobalSubSysSet();
   vector<GlobalSubSysNr> subsys_vec(subsys_set.begin(),subsys_set.end());

   vector<MidasVector> modevec_vec;
   modevec_vec.reserve(mModes.size());

   for (In i=I_0 ; i < mModes.size() ; ++i)
   { 
      modevec_vec.emplace_back(GetModeVectorForSubSys(mModes[i],subsys_vec));
      //Mout << " Norm-1 " << i << " " << fabs(modevec_vec[i].Norm() - C_1) << endl;
      if (fabs(modevec_vec[i].Norm() - C_1) > arThresh)
      {
         Mout << " Norm-1 " << i << " " << fabs(modevec_vec[i].Norm() - C_1) << endl;
         MidasWarning (" Mode Nr.  " + std::to_string(i) + " not normalized ");
         orthn = false;
      }
      for (In j=I_0  ; j < i ; ++j)
      {
         //Mout << " Dot  " << i << " " << j << " " << fabs(Dot(modevec_vec[i],modevec_vec[j])) << endl;
         if (fabs(Dot(modevec_vec[i],modevec_vec[j])) > arThresh)
         {
            Mout << " Dot  " << i << " " << j << " " << fabs(Dot(modevec_vec[i],modevec_vec[j])) << endl;
            MidasWarning (" Modes Nr.  " + std::to_string(i) + " and " +  std::to_string(j)  + " not orthogonal ");
            orthn = false;
         }
      }
   }
   return orthn; 
}

/* **********************************************************************
* checks all modes and returns a map collecting the mode numbers that have 
* the same rigid fragments  
********************************************************************** */
std::map<std::set<std::set<GlobalSubSysNr>>, std::set<std::string> > System::GetModeNameMapRigid() const
{
   std::map<std::set<std::set<GlobalSubSysNr>>, std::set<std::string> > out_map;
   for (In i=I_0 ; i < mModes.size() ; ++i)
   {
      std::set<std::set<GlobalSubSysNr>> rigid=mModes.at(i).GetRigid();
      if (out_map.find(rigid) == out_map.end())
      {
         std::set<std::string> mode_set = {mModes.at(i).Tag()};
         out_map.emplace(rigid,mode_set);
      }
      else if (!rigid.empty())
      {
         out_map.at(rigid).emplace(mModes.at(i).Tag());
      }
   }

   // print map 
   /*
   for (auto it1=out_map.begin(); it1!=out_map.end(); ++it1)
   {
      Mout << " ----------------------- " << endl;
      Mout << " Rigid sets: " << endl;
      for (auto it2=it1->first.begin(); it2!=it1->first.end(); ++it2)
      {
         for_each(it2->begin(),it2->end(),[](GlobalSubSysNr no){Mout << no << " ";});
         Mout << endl;
      }  
      Mout << " Modes: " << endl;
      for_each(it1->second.begin(),it1->second.end(),[](std::string no){Mout << no << " " << endl;});
   }
   */

   return out_map;
}
/* **********************************************************************
********************************************************************** */
std::map<std::set<std::set<GlobalSubSysNr>>, std::set<GlobalModeNr> > System::GetModeNoMapRigid() const
{
   std::map<std::set<std::set<GlobalSubSysNr>>, std::set<GlobalModeNr> > out_map;
   for (In i=I_0 ; i < mModes.size() ; ++i)
   {
      std::set<std::set<GlobalSubSysNr>> rigid=mModes.at(i).GetRigid();
      if (out_map.find(rigid) == out_map.end())
      {
         std::set<GlobalModeNr> mode_set = {i};
         out_map.emplace(rigid,mode_set);
      }
      else
      {
         out_map.at(rigid).emplace(i);
      }
   }

   // print map 
   /*
   for (auto it1=out_map.begin(); it1!=out_map.end(); ++it1)
   {
      Mout << " ----------------------- " << endl;
      Mout << " Rigid sets: " << endl;
      for (auto it2=it1->first.begin(); it2!=it1->first.end(); ++it2)
      {
         for_each(it2->begin(),it2->end(),[](GlobalSubSysNr no){Mout << no << " ";});
         Mout << endl;
      }  
      Mout << " Modes Numbers: " << endl;
      for_each(it1->second.begin(),it1->second.end(),[](GlobalModeNr no){Mout << no << " " << endl;});
   }
   */

   return out_map;
}
/* **********************************************************************
********************************************************************** */
std::set<GlobalModeNr> System::GetModeNrFromTags(const std::set<std::string>& arTagSet) const
{
   std::set<GlobalModeNr> mode_no_set;
   for (In i=I_0 ; i < mModes.size() ; ++i)
   {
      if (arTagSet.find(mModes[i].Tag())!=arTagSet.end())
      {
        mode_no_set.emplace(i);
      }
   }

   MidasAssert(mode_no_set.size()<=arTagSet.size(),"More mode numbers found than names, double mode tags??");
   return mode_no_set;
}
/* **********************************************************************
********************************************************************** */
std::vector<std::string> System::GetModeTags() const 
{ 
   std::vector<std::string> mode_tags(mModes.size(),"");
   for (In i=I_0 ; i < mModes.size() ; ++i)
   {
      mode_tags.at(i)=mModes[i].Tag();
   }
   return mode_tags;
}
/* **********************************************************************
********************************************************************** */
//void System::ReInitforAux(const std::set<GlobalSubSysNr>& arCombi, const bool& AddRot) 
void System::ReInitforAux(const std::set<GlobalSubSysNr>& arCombi) 
{
   std::set<GlobalModeNr> modes_within = ModesWithinSubs(arCombi); 
   std::vector<Mode> old_modes=mModes;
   mModes.clear();
   mModes.reserve(old_modes.size());
   std::set<std::set<GlobalSubSysNr>> fg_set; // to keep track what kind of auxilliary we need
   for (const GlobalSubSysNr &no: arCombi)
   {
      std::set<GlobalSubSysNr> f_set={no};
      fg_set.emplace(f_set);
   }


   for(In i=I_0; i < old_modes.size(); ++i)
   {
      bool nr_within = (modes_within.find(i)!=modes_within.end()) && !old_modes[i].IsRigidInFc(arCombi); // includes everything that is non-rigid within (also relative motions)
      std::set<GlobalSubSysNr> fc = old_modes[i].SubSysInMode();
      if (nr_within && fg_set.find(fc)==fg_set.end())
      {
         bool new_included_in_old = false;
         for (auto it_fg=fg_set.begin() ; it_fg!=fg_set.end();)
         {
            new_included_in_old = new_included_in_old || std::includes(it_fg->begin(),it_fg->end(),fc.begin(),fc.end());
            if (std::includes(fc.begin(),fc.end(),it_fg->begin(),it_fg->end()))
            {
               it_fg = fg_set.erase(it_fg);
            }
            else
            {
               ++it_fg;
            }
         }
         if (!new_included_in_old)
         {
            fg_set.emplace(fc);
         }
      }
      //bool nr_span_more = std::any_of(arCombi.begin(),arCombi.end(),[old_modes,i](GlobalSubSysNr no){return !old_modes.at(i).IsRigidInFc({no});}); //includes everything that that is non-rigid in an individual fragment.
      bool nr_span_more = false;
      if (nr_within || nr_span_more)
      //(old_modes.at(i).GetRigid().size()==I_0 && std::any_of(arCombi.begin(),arCombi.end(),
      //        [old_modes,i](GlobalSubSysNr no){return old_modes.at(i).IsSubSysIn(no);}))) // this will likely go wrong if we start using mixed modes?! 
      {
          mModes.emplace_back(old_modes[i]);
      }
   }

   // add the required coordinates
   for (auto& fg: fg_set)
   {
      AddTransForSet(fg,"AUX");
      AddRotForSet(fg,"AUX");
   } 
   //set all other atoms frozen
   for(auto& subsys : mSubSysDef)
   {
      if(arCombi.find(subsys.first)!=arCombi.end()) continue;
      for (const GlobalAtomNr &nuc_no : subsys.second)
      {
         mNuclei.at(nuc_no).SetNucTreatType(NucTreatType::FROZEN);
      }
   } 
}

/*****************************************************************
*  Tools
********************************************************************** */

void EmplaceBackOrthoNormedMidasVector(vector<MidasVector>& arVV, const MidasVector& arV)
{
   MidasVector new_vec=arV;
   for (In i=I_0; i < arVV.size() ; ++i)
   {
     Nb r=C_M_1*Dot(arVV[i],arV);
     StandardAxpy(new_vec.size(),r,arVV[i],I_1,new_vec,I_1); 
   }
   Nb rec_norm=C_1/new_vec.Norm();
   new_vec.Scale(rec_norm);
 
   arVV.emplace_back(new_vec);
}

/* **********************************************************************
********************************************************************** */

set<ModeType> GetSetModeTypeFromGroupModeType(const ModeGroupType& arModeGroupType)
{
   set<ModeType> mode_type_set;
   switch(arModeGroupType)
   {
      case ModeGroupType::ALL:
      {
         mode_type_set.emplace(ModeType::VRT);
         mode_type_set.emplace(ModeType::VIB);
         mode_type_set.emplace(ModeType::TRANS);
         mode_type_set.emplace(ModeType::ROT);
         break;
      }
      case ModeGroupType::VRT:
      {
         mode_type_set.emplace(ModeType::VRT);
         break;
      }
      case ModeGroupType::VIB:
      {
         mode_type_set.emplace(ModeType::VIB);
         break;
      }
      case ModeGroupType::TRANS:
      {
         mode_type_set.emplace(ModeType::TRANS);
         break;
      }
      case ModeGroupType::ROT:
      {
         mode_type_set.emplace(ModeType::ROT);
         break;
      }
      case ModeGroupType::TRANSROT:
      {
         mode_type_set.emplace(ModeType::TRANS);
         mode_type_set.emplace(ModeType::ROT);
         break;
      }
   }
   return mode_type_set;
}

/* **********************************************************************
********************************************************************** */
MidasMatrix RotationMatrix(const System& system1,const System& system2)
{
   MidasAssert(system2.IsOrthonormal(),"reference system is not orthonormal in Rotation matrix"); 
   MidasAssert(SameNuclei(system1,system2),"Nuclei vector is not the same in rotation matrix"); 
   MidasAssert(system1.mModes.size()==system2.mModes.size()," Number of modes not the same in rotation matrix"); 

   MidasMatrix rot_mat(system1.mModes.size(),C_0);

   MidasMatrix mat1=system1.GetOrderedModeMatrix();
   MidasMatrix mat2=system2.GetOrderedModeMatrix();

   for (In i=I_0; i < system1.mModes.size() ; ++i)
   {
      MidasVector vec1(I_3*system1.mNuclei.size(),C_0);
      mat1.GetCol(vec1,i);
      for (In j=I_0; j < system2.mModes.size() ; ++j)
      {
         MidasVector vec2(I_3*system2.mNuclei.size(),C_0);
         mat2.GetCol(vec2,j);
         rot_mat[i][j]=Dot(vec1,vec2);
         //Mout << i << " " << j << " " << rot_mat[i][j] << endl;
      }
   }


   // check  
   In n=I_0;
   while (n < I_3*system1.mNuclei.size())
   {
      In i=I_0;
      while ((i < rot_mat.Nrows())&& n < I_3*system1.mNuclei.size()) 
      {
         MidasVector vec_rot(rot_mat.Ncols(),C_0);
         rot_mat.GetRow(vec_rot,i);
         MidasVector vec_2(mat2.Ncols(),C_0);
         mat2.GetRow(vec_2,n);
         Nb dot=Dot(vec_rot,vec_2);
         Mout << " n = " << n << ";  i = " << i <<   endl;
         Mout << dot  -  mat1(n,i) << endl;
         ++i;
         ++n;
      }
   }

   return rot_mat;
  
}
