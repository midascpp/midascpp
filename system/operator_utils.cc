/*
************************************************************************
*  
* @file                operator_utils.cc
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Utilities for transforming and writing mop files 
*                      from gradients and Hessians
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "operator_utils.h"
#include "lapack_interface/SYEVD.h"
#include "input/MidasOperatorWriter.h"
#include "input/Input.h"
#include "util/Path.h"

namespace midas::system::util
{

/**
 * Transform gradient and Hessian:
 *    g_trans = M^T g
 *    H_trans = M^T H M
 *
 * @note
 *    This can be used for projecting out translation and rotation if aTrans = (1-LL^T).
 *    It can also be used for obtaining the reduced gradient and Hessian if aTrans is the set of vibrational modes (non-square matrix).
 *
 * @param arGradient       Gradient vector
 * @param arHessian        Hessian matrix
 * @param aM               Transformation matrix
 **/
void transform_grad_hess
   (  MidasVector& arGradient
   ,  MidasMatrix& arHessian
   ,  const MidasMatrix& aM
   )
{
   // Check dimensions
   MidasAssert(arHessian.IsSquare(), "Hessian must be square.");
   auto dim = arHessian.Nrows();
   MidasAssert(dim == aM.Nrows(), "H * M: dimensions mismatch. H_rows = " + std::to_string(dim) + ", M_cols = " + std::to_string(aM.Ncols()));
   auto newdim = aM.Ncols();
   auto grad_size = arGradient.Size();
   MidasAssert(!grad_size || grad_size == dim, "Gradient must have size " + std::to_string(dim) + " or be emtpy.");

   // Transform gradient
   if (  grad_size
      )
   {
      arGradient.LeftMultiplyWithMatrix(aM); // Matrix to the right g^T * M (Niels: strange naming of the function)
   }

   // Transform Hessian
   MidasMatrix half_trans = arHessian * aM;
   MidasMatrix m_t = Transpose(aM);
   arHessian.SetNewSize(newdim, newdim);
   arHessian = m_t * half_trans;
}
void transform_hessian
   (  MidasMatrix& arHessian
   ,  const MidasMatrix& aM
   )
{
   MidasVector grad;
   return transform_grad_hess(grad, arHessian, aM);
}

/**
 * Diagonalize mass-weighted Hessian to obtain normal modes and frequencies
 *
 * @note
 *    We assume the Hessian is symmetric
 *
 * @param aMwHessian             Mass-weighted Hessian
 * @param aRmZeroFreqComponents  Remove vectors with zero eigenvalue
 * @return
 *    Eigenvalues and eigenvectors (frequencies and normal coordinates)
 **/
std::pair<MidasVector, MidasMatrix> diagonalize_hessian
   (  const MidasMatrix& aMwHessian
   ,  bool aRmZeroFreqComponents
   )
{
   // Sanity checks
   MidasAssert(aMwHessian.IsSquare(), "Hessian must be square.");

   // Symmetrize if needed
   MidasMatrix sym_hess;
   bool is_sym = aMwHessian.IsSymmetric();
   if (  !is_sym
      )
   {
      MidasWarning("Hessian is not symmetric. Will symmetrize before calling SYEVD.");
      sym_hess.SetNewSize(aMwHessian.Nrows());
      sym_hess = aMwHessian;
      sym_hess.Symmetrize();
   }
   const auto& hess = is_sym ? aMwHessian : sym_hess;

   // Do diagonalization
   auto eig_sol = SYEVD(hess);
   auto n = eig_sol._n;

   // Load solution (normalize eigenvectors)
   MidasVector eigval;
   LoadEigenvalues(eig_sol, eigval);
   MidasVector freq(eigval.Size());
   // Take square roots of eigenvalues to get frequencies. Copy sign if negative.
   for(In i=I_0; i<freq.Size(); ++i)
   {
      freq[i] = std::copysign(std::sqrt(std::abs(eigval[i])), eigval[i]);
   }
   MidasMatrix eigvecs;
//   NormalizeEigenvectors(eig_sol, ilapack::normalize);
   LoadEigenvectors(eig_sol, eigvecs);

   if (  aRmZeroFreqComponents
      )
   {
      // Count zero frequencies (stop when hitting a non-zero one)
      In n_zero = I_0;
      auto maxeig = eigval[n-1];
      for(In ieig=I_0; ieig<n; ++ieig)
      {
         if (  libmda::numeric::float_numeq_zero(eigval[ieig], maxeig)
            )
         {
            ++n_zero;
         }
         else
         {
            break;
         }
      }
      
      // Check count
      if (  n_zero != I_5
         && n_zero != I_6
         )
      {
         MidasWarning("Found " + std::to_string(n_zero) + " zero eigenvalues of Hesian matrix.");
      }

      // Remove zeros
      auto nnz = n-n_zero;
      MidasVector nz_freq(nnz);
      for(In i=I_0; i<nnz; ++i)
      {
         nz_freq[i] = freq[i+n_zero];
      }
      MidasMatrix vib_eigvecs(n, nnz);
      for(In irow=I_0; irow<n; ++irow)
      {
         for(In icol=I_0; icol<nnz; ++icol)
         {
            vib_eigvecs[irow][icol] = eigvecs[irow][icol+n_zero];
         }
      }

      // Return
      return std::make_pair(std::move(nz_freq), std::move(vib_eigvecs));
   }
   else
   {
      // Return
      return std::make_pair(std::move(freq), std::move(eigvecs));
   }
}

/**
 * Write operator file from mass-weighted, reduced gradient and Hessian
 *
 * @param aMwGradient         Mass-weighted gradient
 * @param aMwHessian          Mass-weighted Hessian
 * @param aFileName           Name of output file
 * @param aModeNames          Mode names (must be in correct order w.r.t. gradient and Hessian indexing)
 **/
void write_operator
   (  const MidasVector& aMwGradient
   ,  const MidasMatrix& aMwHessian
   ,  const std::string& aFileName
   ,  const std::vector<std::string>& aModeNames
   )
{
   bool ignore_gradient = (aMwGradient.Size() == I_0);

   // Sanity checks
   MidasAssert(aMwHessian.IsSquare(), "Hessian matrix must be square!");
   auto dim = aMwHessian.Nrows();
   auto grad_dim = aMwGradient.Size();
   MidasAssert(!grad_dim || grad_dim == dim, "Gradient must be empty or same size as Hessian.");
   MidasAssert(aModeNames.empty() || aModeNames.size() == dim, "Mode names must be empty or have same size as gradient/Hessian.");

   // Construct default mode names if needed
   std::vector<std::string> default_mode_names;
   if (  aModeNames.empty()
      )
   {
      default_mode_names.reserve(dim);
      for(In i=I_0; i<dim; ++i)
      {
         default_mode_names.emplace_back("Q"+std::to_string(i));
      }
   }
   const auto& mode_names = aModeNames.empty() ? default_mode_names : aModeNames;

   // Symmetrize Hessian if needed
   MidasMatrix sym_hess;
   bool is_sym = aMwHessian.IsSymmetric();
   if (  !is_sym
      )
   {
      MidasWarning("Non-symmetric Hessian matrix detected. Will symmetrize before writing operator.");
      sym_hess.SetNewSize(aMwHessian.Nrows());
      sym_hess = aMwHessian;
      sym_hess.Symmetrize();
   }
   const auto& hessian = is_sym ? aMwHessian : sym_hess;

   // Initialize writer
   std::string filename    =  midas::path::IsRelPath(aFileName)   // Check if relative path
                           ?  gMainDir + "/" + aFileName          // Place in main dir if relative path
                           :  aFileName;                          // Absolute path given
   MidasOperatorWriter writer(filename);
   writer.AddModeNames(mode_names);

   // Add gradient terms
   if (  !ignore_gradient
      )
   {
      for(In i=I_0; i<dim; ++i)
      {
         const std::vector<std::string> functions = { "Q^1" };

         auto coef = aMwGradient[i];
         std::vector<std::string> modes = { mode_names.at(i) };

         writer.AddOperatorTerm(coef, functions, modes);
      }
   }

   // Add Hessian terms
   for(In i=I_0; i<dim; ++i)
   {
      // Add diagonal terms
      std::vector<std::string> functions = { "Q^2" };
      auto coef = 0.5 * hessian[i][i];
      std::vector<std::string> modes = { mode_names.at(i) };
      writer.AddOperatorTerm(coef, functions, modes);

      // Off-diagonal terms
      for(In j=i+1; j<dim; ++j)
      {
         functions = { "Q^1", "Q^1" };
         coef = hessian[i][j];
         modes = { mode_names.at(i), mode_names.at(j) };
         writer.AddOperatorTerm(coef, functions, modes);
      }
   }

   // Write file
   writer.WriteFile();
}
void write_operator
   (  const MidasMatrix& aMwHessian
   ,  const std::string& aFileName
   ,  const std::vector<std::string>& aModeNames
   )
{
   return write_operator(MidasVector(), aMwHessian, aFileName, aModeNames);
}

} /* namespace midas::system::util */
