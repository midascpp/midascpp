/*
************************************************************************
*  
* @file                System.h
*
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Keeps all information about the full system,
*                      including subsystems and submodes 
* 
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SYSTEM_H
#define SYSTEM_H
#include "inc_gen/TypeDefs.h"
#include "system/Mode.h"
#include "input/SystemDef.h"
#include "pes/molecule/MoleculeData.h"


void SystemDrv();
class MoleculeInfo; 

enum class ModeGroupType: int  {ALL,VIB,TRANS,ROT,VRT,TRANSROT};

using SubSysDefIter = std::map<GlobalSubSysNr, std::set<GlobalAtomNr>>::const_iterator;
using NucIter = std::set<GlobalAtomNr>::const_iterator;
using ModeIter = std::set<GlobalModeNr>::const_iterator;
using SubSysIter = std::set<GlobalSubSysNr>::const_iterator;

namespace midas
{
namespace molecule
{
   class MoleculeInfo;
}
}


class System 
{
   protected:
      In                                                mCurrentTagNo = -I_1;
      std::vector<Nuclei>                               mNuclei;          
      std::map<GlobalSubSysNr,std::set<GlobalAtomNr>>   mSubSysDef;
      std::vector<Mode>                                 mModes; 
      HessianType                                       mHessian;
      GradientType                                      mGradient;

      // contructor from itself.. more for smaller fractions
      System(std::vector<Nuclei>&& arNuclei, std::vector<Mode>&& arModes)
         :  mNuclei(arNuclei)
         ,  mModes(arModes) 
               {  InitiateSubSysDefFromNuclei();
               }

      //Initialization
      void InitiateSubSysDefFromNuclei();
      void InitiateAtomSubSysDef();
      void InitiateCartesianModes();
      void InitiateLocalTransRot();


      //Modification
      std::vector<Mode> GetTransForSubSys(const set<GlobalSubSysNr>& arNSub) const;
      std::vector<Mode> GetRotForSubSys(const set<GlobalSubSysNr>& arSubSysNr) const;
      std::vector<Mode> GetCartesianCoordForSub(const GlobalSubSysNr& arSubSysNr);
      MidasMatrix GetIMomForSubSys(const set<GlobalSubSysNr>& arSubSysNr) const;
      void InsertMode(Mode&& arrMode, const string& arName="Q")
            {if (mModes.size()>= I_3* mNuclei.size()) MidasWarning("Added more than 3N modes");
             mModes.emplace_back(std::move(arrMode));
             mModes.at(mModes.size()-I_1).SetTag(GetFreeModeTag(arName));
             }

      //Analysis
      std::string GetFreeModeTag(const std::string aPreFix ) {return aPreFix+std::to_string(++mCurrentTagNo);}
      LocalAtomNr GetLocalAtomNr(const GlobalAtomNr& arGlobalNr) const;
      Nb SubSysMass(const GlobalSubSysNr&) const;
      vector<Nb> SubSysCom(const GlobalSubSysNr&) const;
      vector<Nb> SubSysCom(const set<GlobalSubSysNr>&) const;
      Nb GetNormForModeNoMassWeight(const GlobalModeNr&);
      MidasVector GetOrderedModeVectorForSubSys(const GlobalModeNr& arModeNr, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight=true) const
             {return GetOrderedModeVectorForSubSys(mModes.at(arModeNr),arSubSet, arMassWeight);}     
      MidasVector GetOrderedModeVectorForSubSys(const Mode& arMode, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight=true) const;     
      MidasVector GetOrderedSigmaVectorForSubSys(const GlobalModeNr& arModeNr, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight=true) const
             {return GetOrderedSigmaVectorForSubSys(mModes.at(arModeNr),arSubSet, arMassWeight);}     
      MidasVector GetOrderedSigmaVectorForSubSys(const Mode& arMode, const set<GlobalSubSysNr>& arSubSet, const bool& arMassWeight=true) const;     
      Nb CalcOverallDisp(const GlobalModeNr& no)
            {Nb disp=I_0; MidasVector mode_vec=GetOrderedModeVector(no);
             for (In i=I_0; i < mNuclei.size(); ++i)
                for (In j=I_0; j < I_3; ++j)
                   disp+=sqrt(mNuclei.at(i).GetMass())*mode_vec[I_3*i+j];
             return disp;
            }
      ModeType GetGeneralModeType(const set<GlobalModeNr>& arModeNrs);

      MidasVector GetModeVectorForSubSys(const Mode&, const vector<GlobalSubSysNr>&) const;
      Mode GetModeForSubSys(const MidasVector& arModeVec, const vector<GlobalSubSysNr>& arSubVec, const ModeType& arModeType=ModeType::VRT );
      Mode GetFullModeFromVec(const MidasVector& arModeVec, const ModeType& arModeType=ModeType::VRT ); 
      vector<MidasVector> GetOrderedModeVectors(const set<GlobalModeNr>& arModeNrs, const bool& global=true, 
             const bool& arMassWeight=true) const;
      HessianType GetReducedHessian(const set<GlobalModeNr>& arModeNr);
      set<set<GlobalSubSysNr>> RigidSubSysWithinModes(const set<GlobalModeNr>& arModeNr);
      std::vector<Nuclei> CapNucVec(const std::vector<GlobalAtomNr>& arNucSet) const;
      void AddCappingMotions(const GlobalSubSysNr& arNo);
      MidasMatrix SetUpTRProjectionMatrix(const std::vector<GlobalSubSysNr>& arSubSysVec, bool aOrthogonalize)  const;

   public:
      System();
      System(const SystemDef* const);

      //! Trans-Rot projection matrix for all sub-systems in System
      MidasMatrix SetUpTotalTRProjectionMatrix(bool aOrthogonalize = true) const;

      //Information
      std::set<GlobalAtomNr> AtomsInSubSys(const std::set<GlobalSubSysNr>& arSubSysSet) const
            {std::set<GlobalAtomNr> set;
               for (auto it=arSubSysSet.begin(); it != arSubSysSet.end(); ++it) 
                  set.insert(mSubSysDef.at(*it).begin(),mSubSysDef.at(*it).end()); 
               return set;
            }

      std::set<GlobalSubSysNr> GetGlobalSubSysSet() const 
             {set<GlobalSubSysNr> set; 
              for (SubSysDefIter it=mSubSysDef.begin();it!=mSubSysDef.end(); ++it) set.emplace(it->first);
              return set; }
      std::set<GlobalSubSysNr>GetSubSysInModeSet(const std::set<GlobalModeNr>& arModeSet) const;
      std::set<GlobalModeNr> GetGlobalModeSet() const 
             {set<GlobalModeNr> set; 
              for (In i=I_0; i< mModes.size(); ++i) set.emplace(i);
              return set; }
      std::set<GlobalModeNr> GetGlobalVibSet() const 
             {set<GlobalModeNr> set; 
              for (In i=I_0; i< mModes.size(); ++i) if (mModes.at(i). GetModeType()== ModeType::VIB) set.emplace(i);
              return set; }
      std::set<GlobalModeNr> GetGlobalRotSet() const 
             {set<GlobalModeNr> set; 
              for (In i=I_0; i< mModes.size(); ++i) if (mModes.at(i). GetModeType()== ModeType::ROT) set.emplace(i);
              return set; }
      std::set<GlobalModeNr> GetGlobalTransSet() const 
             {set<GlobalModeNr> set; 
              for (In i=I_0; i< mModes.size(); ++i) if (mModes.at(i). GetModeType()== ModeType::TRANS) set.emplace(i);
              return set; }
      std::set<GlobalModeNr> GetGlobalRelSet() const 
             {set<GlobalModeNr> set; 
              auto mode_no_map=GetModeNoMapRigid();
              for (auto m: mode_no_map)
              {
                 //if(!m.first.empty() && m.first.size()!=mModes.size())
                 if(!m.first.empty() && m.first.size()!=mSubSysDef.size())
                     set.insert(m.second.begin(),m.second.end());
              }
              return set; }
      std::set<GlobalModeNr> GetGlobalNameSet(const string& arName) const 
             {set<GlobalModeNr> set; 
              for (In i=I_0; i< mModes.size(); ++i) if (mModes.at(i).Tag().find(arName)!= std::string::npos) set.emplace(i);
              return set; }
      //std::set<GlobalModeNr> GetInterSet(const std::set<GlobalSubSysNr>& arCombi) const; 
               
      std::set<GlobalModeNr> ModesWithSubs(const set<GlobalSubSysNr>&, const ModeGroupType& arModeGroupType=ModeGroupType::ALL) const;
      std::set<GlobalModeNr> ModesWithinSubs(const set<GlobalSubSysNr>&, const ModeGroupType& arModeGroupType=ModeGroupType::ALL) const;
      Nuclei GetNuclei(const GlobalAtomNr& arNo) const  {return mNuclei.at(arNo);}
      const std::vector<Nuclei>& GetNucleiVec() const { return mNuclei; }
      In GetNucleiNr() const {return mNuclei.size();}
      In GetModeNr() const {return mModes.size();}

      //Modification
      void AddSubSystem(System&& arrSubSys);
      void AddLocalTransRot();
      void AddTransForSet(const set<GlobalSubSysNr>& arSubSysNrs, const string& arName="Q");
      void AddRotForSet(const set<GlobalSubSysNr>& arSubSysNrs, const string& arName="Q");
      void AddRot() {AddRotForSet(GetGlobalSubSysSet(),"ROT");}
      void AddCartCoordForSub(const GlobalSubSysNr& arSubSysNr);

      void IdentifyTRforSubSys(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType=ModeGroupType::ALL);
      set<GlobalModeNr> SeparateOutTRforSubSys(const set<GlobalSubSysNr>& arSubSysNrs, const ModeGroupType& arModeGroupType=ModeGroupType::ALL, const bool& arKeepTR=true);
      void RemoveGlobalTransRot() 
            { set<GlobalModeNr> mode_set = SeparateOutTRforSubSys(GetGlobalSubSysSet(),ModeGroupType::ALL,false); }
      void RotateMolecule();

      void RemoveTransRotContrNotOrthonorm(const std::set<GlobalSubSysNr>& arSubSysSet, const bool& arKeepRigidInfo=false) ;
      void UpdateSigmas(const set<GlobalModeNr>& arModeNrs);
      void DiagRedHess(const set<GlobalModeNr>& arModeNrs);
      void ReplaceModes(const set<GlobalModeNr>& arModeNrs, const molecule::MoleculeInfo& arMolInfo);
      MidasVector CalcSigmaForMode(const GlobalModeNr& arModeNr) ;
      void AddSigmaToMode(const GlobalModeNr& arModeNr, MidasVector&&, const bool& arMw=true);
      void AddSigmaToMode(const GlobalModeNr& arModeNr, std::vector<GlobalSubSysNr>&& arSubSysNrs, std::vector<MidasVector>&& arSigma)
         {mModes.at(arModeNr).AddSigma(std::forward<std::vector<GlobalSubSysNr>&&>(arSubSysNrs), std::forward<std::vector<MidasVector>&&>(arSigma));}
      void RemoveModes(const set<GlobalModeNr>& arModeNrs)
         {for (auto it = arModeNrs.rbegin() ; it != arModeNrs.rend(); ++it)  // we have to do it backwards!!!
            {
               mModes.erase(mModes.begin() + (In)*it);
            }    
         }
      

      void SortModesAfterFreq();
      void SortModes(const vector<set<GlobalSubSysNr>>&);
      void UpdateSubSys(const std::vector<std::set<GlobalSubSysNr>>&);
      void ReSetModeTags() 
      {
         for (In i=I_0; i < mModes.size(); ++i)
         {
            mModes.at(i).SetTag("Q" + std::to_string(i));
         }
         mCurrentTagNo = mModes.size() - I_1; 
         MidasWarning("Mode tags have been reset");
      }
      void ReInitforAux(const std::set<GlobalSubSysNr>& arCombi);
      void AddCappingMotions()
         {for (auto it=mSubSysDef.begin(); it != mSubSysDef.end(); ++it) 
             {
                Mout << it->first << endl;
                MidasAssert(ContainsCapNuc(it->first)==AllCapNuc(it->first),"subsystem contains capping atoms, but not exclusively - I give up here");
                if (ContainsCapNuc(it->first))
                   AddCappingMotions(it->first);
             }   
         }
      void ScaleFreqs(const std::set<GlobalModeNr>& arModeSet, const Nb& arScalFac) 
      {
         for (auto i: arModeSet)
         {
            MidasAssert(i<mModes.size(),"Mode to scale outside range");
            mModes.at(i).ScaleFreq(arScalFac);
         }
      }

      //Analysis
      void CalcAngles() const;
      Nb MinNucDist(const GlobalSubSysNr&, const GlobalSubSysNr&) const;
      Nb MinNucDistAct(const GlobalSubSysNr&, const GlobalSubSysNr&) const;
      Nb MaxNucCoupNorm(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const;
      Nb MaxNucCoupNormAct(const GlobalSubSysNr& arSys1, const GlobalSubSysNr& arSys2) const;
      Nb NucCoupNorm(const GlobalAtomNr& arAt1, const GlobalAtomNr& arAt2) const;
      bool ContainsActiveNuc(const GlobalSubSysNr& arSubSysNr) const
             {return any_of(mSubSysDef.at(arSubSysNr).begin(), mSubSysDef.at(arSubSysNr).end(),
                     [this](GlobalAtomNr nuc_no) {return mNuclei.at(nuc_no).IsActive();});}
      bool ContainsFrozenNuc(const GlobalSubSysNr& arSubSysNr) const
             {return any_of(mSubSysDef.at(arSubSysNr).begin(), mSubSysDef.at(arSubSysNr).end(),
                     [this](GlobalAtomNr nuc_no) {return mNuclei.at(nuc_no).IsFrozen();});}
      bool ContainsCapNuc(const GlobalSubSysNr& arSubSysNr) const
             {return any_of(mSubSysDef.at(arSubSysNr).begin(), mSubSysDef.at(arSubSysNr).end(),
                     [this](GlobalAtomNr nuc_no) {return mNuclei.at(nuc_no).IsCapping();});}
      bool AllCapNuc(const GlobalSubSysNr& arSubSysNr) const
             {return all_of(mSubSysDef.at(arSubSysNr).begin(), mSubSysDef.at(arSubSysNr).end(),
                     [this](GlobalAtomNr nuc_no) {return mNuclei.at(nuc_no).IsCapping();});}
      std::set<GlobalSubSysNr> ActiveSubSys() const
             {std::set<GlobalSubSysNr> set; 
              for (auto i : mSubSysDef)
                 {if (ContainsActiveNuc(i.first)) set.emplace(i.first) ;}  
              return set;}
      bool HasHessian() const {return (mHessian.Ncols()!=I_0);}
      bool HasGradient() const { return mGradient.Size() != I_0; }
      const MidasMatrix& GetHessian() const { return mHessian; }
      const MidasVector& GetGradient() const { return mGradient; }
      In HessianDim() const { return mHessian.Ncols(); }
      In GradientDim() const { return mGradient.Size(); }
      std::set<std::set<GlobalSubSysNr>> SubSysGroups() const;
      In NoOfAtomsInSubs(const set<GlobalSubSysNr>&) const;
      In NoOfModes() const {return mModes.size();}
      In NoOfSubSys() const {return mSubSysDef.size();}
      In NoOfModesWithSigma() const 
             {In n=I_0; for_each(mModes.begin(),mModes.end(),[&n](Mode mode)
                    {if (mode.HasSigma()) ++n;});  return n;}
      In NoOfModesWithSigmaInSet(const std::set<GlobalModeNr>& arModeSet ) const 
             {In n=I_0; for_each(arModeSet.begin(),arModeSet.end(),[this,&n](GlobalModeNr no)
                    {if (mModes.at(no).HasSigma()) ++n;});  return n;}
      std::set<GlobalModeNr> ModesWithSigma() const 
             {std::set<GlobalModeNr> set; 
              for (In i=I_0; i < mModes.size();++i)
                 {if (mModes.at(i).HasSigma()) set.emplace(i) ;}  
              return set;}
      bool ModeHasSigma(const GlobalModeNr& arNo) const {return mModes.at(arNo).HasSigma();}
      std::pair<std::vector<GlobalSubSysNr>,std::vector<MidasVector>> DevideFullVecIntoSubs(const MidasVector& arModeVec) const ;
      std::map<std::set<std::set<GlobalSubSysNr>>, std::set<std::string> > GetModeNameMapRigid() const;
      std::map<std::set<std::set<GlobalSubSysNr>>, std::set<GlobalModeNr> > GetModeNoMapRigid() const;
      std::set<GlobalModeNr> GetModeNrFromTags(const std::set<std::string>& arTagSet) const;
      GlobalModeNr GetModeNrFromTag(const std::string& arTag) const {std::set<GlobalModeNr> set=GetModeNrFromTags({arTag}); return *set.begin();}
      std::vector<std::string> GetModeTags() const; 

      MidasMatrix GetOrderedModeMatrix(bool aMassWeight=true) const
      {
         MidasMatrix mode_matrix(I_3*mNuclei.size(),mModes.size(),C_0);
         for (In i=I_0;i<mModes.size(); ++i)
         {
            MidasVector vec=GetOrderedModeVector(i, aMassWeight);
            mode_matrix.AssignCol(vec,i);
         }
         return mode_matrix;
      }
      MidasVector GetOrderedModeVector(const GlobalModeNr& arModeNr, const bool& arMassWeight=true) const
             {return GetOrderedModeVectorForSubSys(arModeNr, GetGlobalSubSysSet(), arMassWeight);}     
      Nb GetFreq(In i) const { return mModes.at(i).Freq(); }

      //Printing
      void PrintNuclei() const
             {for (In i = I_0; i < mNuclei.size(); ++i) Mout << i << "  " <<  mNuclei[i] << endl;  Mout << endl;}
      void PrintSubSysDef() const ;
      void PrintModes() const
             {for (In i = I_0; i < mModes.size(); ++i) {Mout << "Mode : " << i << endl; Mout << "  " <<  mModes[i] << endl;  Mout << endl;}}

      // Write out  molecule information
      void Write(const std::string& arFileName, const std::string& arFileType, const bool& arOnlyVib=false);
      void WriteRedHess(const set<GlobalModeNr>& ,const string& arFileName);

      System LimitToSubSys(const std::set<GlobalSubSysNr>& arSubSysSet,const bool& KeepRot) const; 
      System GetNRCappedSubSystem
         (   const std::set<GlobalSubSysNr>& arSubSysSet
         ,   const bool                      aDoCapping
         ,   const bool                      aProjectOutTR=false
         ,   const bool                      aKeepRot=false
         ) const;

      //checking
      bool IsOrthonormal(const Nb& = C_10_10) const;

      //! THIS SHOULD BE REMOVED !!!!!!!!!1111one ITS AN ABOMINATION
      friend class midas::molecule::MoleculeInfo;

      friend bool SameNuclei(const System& system1,const System& system2){return system1.mNuclei==system2.mNuclei;}

      friend MidasMatrix RotationMatrix(const System& system1,const System& system2);
};
#endif //SYSTEM_H
