/*
************************************************************************
*  
* @file                Mode.cc
*
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Keeps all information about one mode,
*                      including subsystems  
* 
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "inc_gen/TypeDefs.h"
#include "system/Mode.h"
#include "util/Io.h"

/* **********************************************************************
*  Modification
********************************************************************** */
void Mode::AddSubSystems(const std::vector<GlobalSubSysNr>& arSubSysNr, std::vector<MidasVector>&& arrMode, const bool& arKeepSigma) 
{
   if (mSubSysNrsSigma.size() != I_0 && !arKeepSigma)
   {
      MidasWarning("Modification of the mode without knowing sigma, I will delete sigma");
      mSubSysNrsSigma.clear();
      mSigma.clear();
   } 
   std::copy(arSubSysNr.begin(),arSubSysNr.end(),std::back_inserter(mSubSysNrs)); 
   std::move(static_cast<std::vector<MidasVector>&&>(arrMode).begin(),static_cast<std::vector<MidasVector>&&>(arrMode).end(),std::back_inserter(mSubModes));
}
/* **********************************************************************
********************************************************************** */
void Mode::AddSubSystems(const std::vector<GlobalSubSysNr>& arSubSysNr,const  std::vector<MidasVector>& arMode, const bool& arKeepSigma) 
{
   if (mSubSysNrsSigma.size() != I_0 && !arKeepSigma)
   {
      MidasWarning("Modification of the mode without knowing sigma, I will delete sigma");
      mSubSysNrsSigma.clear();
      mSigma.clear();
   }  
   std::copy(arSubSysNr.begin(),arSubSysNr.end(),std::back_inserter(mSubSysNrs)); 
   std::copy(arMode.begin(),arMode.end(),std::back_inserter(mSubModes)); 
}
/* **********************************************************************
********************************************************************** */
void Mode::ShiftSubSysNrs(const In& arShift)
{
   for (In i = I_0; i < mSubSysNrs.size(); ++i)
      mSubSysNrs.at(i) = mSubSysNrs.at(i) + arShift;
   for (In i = I_0; i < mSubSysNrsSigma.size(); ++i)
      mSubSysNrsSigma.at(i) = mSubSysNrsSigma.at(i)+ arShift;
   set<set<GlobalSubSysNr>> new_rigid;
   for (auto it= mRigid.begin(); it!=mRigid.end(); ++it)
   {
      set<GlobalSubSysNr> rigid_set;
      for (auto it2=it->begin();it2!=it->end(); ++it2)
      {
         rigid_set.emplace(*it2+arShift);
      } 
      new_rigid.emplace(rigid_set);
   }
   mRigid=new_rigid;
}
/* **********************************************************************
********************************************************************** */
void Mode::Add(const Mode& arMode)
{
   // first have to update the rigid modes at it needs an unchanges this
   mRigid=GetCommonRigid(*this,arMode);
   // first modes
   for (In i=I_0; i < arMode.mSubModes.size() ; ++i )
   {
      if (IsSubSysIn(arMode.mSubSysNrs[i]))
      {
         GlobalSubSysNr sub_no=arMode.mSubSysNrs[i];
         StandardAxpy(mSubModes[GetLocalSubSysNr(sub_no)].size(),C_1,
                arMode.mSubModes[i],I_1,mSubModes[GetLocalSubSysNr(sub_no)],I_1); 
      }
      else 
      {
         AddSubSystems({arMode.mSubSysNrs[i]},{arMode.mSubModes[i]},true);
      }
   }
   // then sigma 
   for (In i=I_0; i < arMode.mSigma.size() ; ++i )
   {
      if (IsSubSysInSigma(arMode.mSubSysNrsSigma[i]))
      {
         GlobalSubSysNr sub_no=arMode.mSubSysNrsSigma[i];
         StandardAxpy(mSigma[GetLocalSubSysNrSigma(sub_no)].size(),C_1,
                arMode.mSigma[i],I_1,mSigma[GetLocalSubSysNrSigma(sub_no)],I_1); 
      }
      else
      {
         Mout << " Subsystem " << arMode.mSubSysNrs[i] << " is not contained in Sigma ?! " << endl;
         MidasWarning("Adding mode that is not contained in Sigma");
         AddSubSysToSigma(arMode.mSubSysNrsSigma.at(i),arMode.mSigma.at(i));   
      }
   }
}
/* **********************************************************************
********************************************************************** */
void Mode::LimitToSubSysSet(const std::set<GlobalSubSysNr>& arNewSysSet)
{

   std::vector<GlobalSubSysNr> new_SubSysNrs;
   new_SubSysNrs.reserve(mSubSysNrs.size());
   std::vector<MidasVector>  new_SubModes;
   new_SubModes.reserve(mSubSysNrs.size());


   for (auto it=mSubSysNrs.begin();it!=mSubSysNrs.end();++it)
   {
      auto it2=std::find(arNewSysSet.begin(),arNewSysSet.end(),*it);
      if (it2!=arNewSysSet.end())
      {
         new_SubSysNrs.emplace_back(*it);
         new_SubModes.emplace_back(mSubModes.at(GetLocalSubSysNr(*it)));
      }
      else 
      {
         RemoveSubSysFromRigid(*it);
      }
   }
   mSubSysNrs.clear();
   mSubSysNrs=std::move(new_SubSysNrs);
   mSubModes.clear();
   mSubModes=std::move(new_SubModes);
   //Mout << " Removing Sigma information " << endl;
   mSubSysNrsSigma.clear();
   mSigma.clear();
}
/* **********************************************************************
********************************************************************** */
void Mode::RemoveSubSysFromRigid(const GlobalSubSysNr& arNo)
{
   //Mout << "rigid searched for " << arNo << endl;
   bool end=false;
   std::set<GlobalSubSysNr> new_set;
   auto it = mRigid.begin();
   while (!end && it!=mRigid.end())
   {
      //Mout << " Checking "<< endl;
      auto it_find=it->find(arNo);
      if (it_find!=it->end())
      {
         //Mout << " rigid subset found " << *it_find <<  endl;
         end=true;
         auto new_set=*it;
         ++it;
         mRigid.erase(new_set);  // both erase complained when trying the iterator..
         new_set.erase(*it_find);
         if (!new_set.empty())
            mRigid.emplace(new_set);
      }  
      else 
      {
         ++it;
      }
   }
}
/* **********************************************************************
*  Analysis
********************************************************************** */
bool Mode::IsSubSysIn(const GlobalSubSysNr& arSubNr) const
{
   bool in=false;
   for (In i =I_0; i < mSubSysNrs.size() ; ++i)
   {
      if (mSubSysNrs.at(i)==arSubNr) 
      {
         in=true;
      }
   }
   return in;
}
/* **********************************************************************
********************************************************************** */
bool Mode::IsSubSysInSigma(const GlobalSubSysNr& arSubNr) const
{
   bool in=false;
   for (In i =I_0; i < mSubSysNrsSigma.size() ; ++i)
   {
      if (mSubSysNrsSigma.at(i)==arSubNr) 
      {
         in=true;
      }
   }
   return in;
}
/* **********************************************************************
********************************************************************** */
bool Mode::IsRigidInFc(const std::set<GlobalSubSysNr>& arSubSysSet) const
{
   bool rigid=false; 
   if (none_of(arSubSysSet.begin(),arSubSysSet.end(),[this](GlobalSubSysNr no){return IsSubSysIn(no);}))
      rigid=true;
   else
   {
      for(auto it=mRigid.begin(); it!=mRigid.end(); ++it) 
      {
         if (std::includes(it->begin(), it->end(), arSubSysSet.begin(), arSubSysSet.end()))
         {
             MidasAssert(!rigid,"Found the same rigid set more than once");
             rigid=true;
         } 
      } 
   }
   return rigid;
}
/* **********************************************************************
********************************************************************** */
set<GlobalSubSysNr> Mode::SubSysInMode() const
{
   set<GlobalSubSysNr> subsys_set;
   for (In i =I_0; i < mSubSysNrs.size() ; ++i)
   {
      subsys_set.emplace(mSubSysNrs.at(i));
   }
   return subsys_set;
}
/* **********************************************************************
********************************************************************** */
Nb Mode::Entry(const GlobalSubSysNr& arSubSys, const LocalAtomNr& arNucl, const In& arXYZ) const 
{  
   In n=-I_1;
   for (In i =I_0; i < mSubSysNrs.size(); ++i)
   {
      if (mSubSysNrs[i]==arSubSys) n=i;
   }
   Nb entry=C_0;
   if (n!=-I_1) entry=mSubModes.at(n).operator[](I_3*arNucl+arXYZ);     
   return entry;
}
/* **********************************************************************
********************************************************************** */
Nb Mode::SigmaEntry(const GlobalSubSysNr& arSubSys, const LocalAtomNr& arNucl, const In& arXYZ) const 
{  
   In n=-I_1;
   for (In i =I_0; i < mSubSysNrsSigma.size(); ++i)
   {
      if (mSubSysNrsSigma[i]==arSubSys) n=i;
   }
   Nb entry=C_0;
   if (n!=-I_1) entry=mSigma.at(n).operator[](I_3*arNucl+arXYZ);     
   return entry;
}
/* **********************************************************************
********************************************************************** */
In Mode::GetLocalSubSysNr(const GlobalSubSysNr& arNr) const
{
    In no=-I_1;
    for (In i = I_0; i < mSubSysNrs.size(); ++i)
       if (mSubSysNrs[i]==arNr) no=i;

    if(no==-I_1)
       MIDASERROR("Subsystem not found in mode");

    return no;
}
/* **********************************************************************
********************************************************************** */
In Mode::GetLocalSubSysNrSigma(const GlobalSubSysNr& arNr) const
{
    In no=-I_1;
    for (In i = I_0; i < mSubSysNrsSigma.size(); ++i)
       if (mSubSysNrsSigma[i]==arNr) no=i;

    if(no==-I_1)
       MIDASERROR("Subsystem not found in Sigma");

    return no;
}
/* **********************************************************************
********************************************************************** */
std::set<GlobalSubSysNr> Mode::ActiveSet( ) const
{
   std::set<GlobalSubSysNr> active_set;
   for (auto it=mSubSysNrs.begin(); it!=mSubSysNrs.end(); ++it)
   {
      if (all_of(mRigid.begin(),mRigid.end(),[it](std::set<GlobalSubSysNr> rigid_set)
             {return (rigid_set.find(*it)==rigid_set.end());}))
      {
         active_set.emplace(*it);
      }
   }
   return active_set;
}
/* **********************************************************************
********************************************************************** */
std::set<GlobalSubSysNr> Mode::GetRigidForSubSys(const GlobalSubSysNr& arNo) const
{
   std::set<GlobalSubSysNr> rigid_for_sub;
   bool found=false;
   for (auto it=mRigid.begin(); it!=mRigid.end() ; ++it)
   {
      if (it->find(arNo)!=it->end())
      {
          MidasAssert(!found,"Found the same subsystem in two rigid groups?!");
          rigid_for_sub=*it;          
      }
   }
   return rigid_for_sub;
}
/* **********************************************************************
*  Friends
********************************************************************** */
ostream& operator<<(ostream& os,const Mode& arMode) 
{
   Out72Char(os,'+','-','+');
   Mout << arMode.mModeType;
   if (arMode.mFreqInitialized)
      Mout << "Frequency: " << arMode.mFreq << " hartree " << endl;
   if (arMode.mRigid.size() > I_0)
      Mout << "Rigid groups :"  << endl;
   for (auto it=arMode.mRigid.begin(); it!=arMode.mRigid.end();++it)
   {
      std::for_each(it->begin(),it->end(),[](GlobalSubSysNr no){Mout << no << ", ";}) ;
      Mout << endl;
   }
   for (In i= I_0; i < arMode.mSubModes.size(); ++i)
   {
      Out72Char(os,'+','-','+');
      os << " Displacements for subsystem " << arMode.mSubSysNrs.at(i) << endl ;
      os <<  arMode.mSubModes.at(i) << endl;
      Out72Char(os,'+','-','+');
   }
   os << endl;
   for (In i= I_0; i < arMode.mSigma.size(); ++i)
   {
      Out72Char(os,'+','-','+');
      os << " Sigma for subsystem " << arMode.mSubSysNrsSigma.at(i) << endl ;
      os <<  arMode.mSigma.at(i) << endl;
      Out72Char(os,'+','-','+');
   }
   return os;
}
/* **********************************************************************
********************************************************************** */
ostream& operator<<(ostream& os,const ModeType& arModeType)
{
   switch (arModeType)
   {
      case ModeType::VIB:
      {
         os<< "Mode is a vibration" << endl;
         break;
      }
      case ModeType::TRANS:
      {
         os<< "Mode is a (local) translation" << endl;
         break;
      }
      case ModeType::ROT:
      {
         os<< "Mode is a (local) rotation" << endl;
         break;
      }
      case ModeType::VRT:
      {
         os<< "Mode is mixed (translation, rotation and translation)" << endl;
         break;
      }
      default:
      {
         MIDASERROR("Unknown mode type");
         break;
      }
   }
   return os;
}
/* **********************************************************************
********************************************************************** */
Mode operator*(const Nb& arFac,const Mode& arMode) 
{ 
   Mode mode = arMode;
   for (In i=I_0; i < mode.mSubModes.size() ; ++i)
   {
      mode.mSubModes.at(i).Scale(arFac); 
   } 
   for (In i=I_0; i < mode.mSigma.size() ; ++i)
   {
      mode.mSigma.at(i).Scale(arFac); 
   } 
   return mode; 
}
/* **********************************************************************
********************************************************************** */
Nb ModeDotSigma(const Mode& arMode1, const Mode& arMode2) 
{ 
   Nb dot=I_0;
   for (In i=I_0; i < arMode1.mSubModes.size() ; ++i)
   {
      for (In j=I_0; j < arMode2.mSigma.size() ; ++j)
      {
         if (arMode1.mSubSysNrs.at(i)==arMode2.mSubSysNrsSigma.at(j))
            dot +=  Dot(arMode1.mSubModes.at(i),arMode2.mSigma.at(j));
      }
   } 
   return dot; 
}
/* **********************************************************************
********************************************************************** */
Nb ModeDotMode(const Mode& arMode1, const Mode& arMode2) 
{ 
   Nb dot=I_0;
   for (In i=I_0; i < arMode1.mSubModes.size() ; ++i)
   {
      for (In j=I_0; j < arMode2.mSubModes.size() ; ++j)
      {
         if (arMode1.mSubSysNrs.at(i)==arMode2.mSubSysNrs.at(j))
            dot +=  Dot(arMode1.mSubModes.at(i),arMode2.mSubModes.at(j));
      }
   } 
   return dot; 
}
/* **********************************************************************
********************************************************************** */
std::set<std::set<GlobalSubSysNr>> GetCommonRigid(const Mode& arMode1, const Mode& arMode2)
{
   if (gDebug)
   {
      Mout << " ----------------------- " << endl;
      Mout << " Find common rigid sets for " << endl;
      Mout << " Mode 1 " << endl;
      Mout << " Subsys: " ;
      std::for_each(arMode1.mSubSysNrs.begin(),arMode1.mSubSysNrs.end(),[](GlobalSubSysNr no){Mout << no << ", ";}) ;
      Mout << "    Rigid: " ; 
      for (auto it=arMode1.mRigid.begin(); it!=arMode1.mRigid.end();++it)
      {
         std::for_each(it->begin(),it->end(),[](GlobalSubSysNr no){Mout << no << ", ";}) ;
         Mout << " | " ;
      }
      Mout << endl;
      Mout << " Mode 2 " << endl;
      Mout << " Subsys: " ;
      std::for_each(arMode2.mSubSysNrs.begin(),arMode2.mSubSysNrs.end(),[](GlobalSubSysNr no){Mout << no << ", ";}) ;
      Mout << "    Rigid: " ; 
      for (auto it=arMode2.mRigid.begin(); it!=arMode2.mRigid.end();++it)
      {
         std::for_each(it->begin(),it->end(),[](GlobalSubSysNr no){Mout << no << ", ";}) ;
         Mout << " | " ;
      }
      Mout << endl;
      Mout << " ----------------------- " << endl;
   } 
   
   // get union of subsystems
   std::set<GlobalSubSysNr> subsys_union;
   std::set_union(arMode1.mSubSysNrs.begin(),arMode1.mSubSysNrs.end(),
          arMode2.mSubSysNrs.begin(),arMode2.mSubSysNrs.end(),
          std::inserter(subsys_union,subsys_union.end())); 

   std::set<std::set<GlobalSubSysNr>> common_rigid;
   for (auto it=subsys_union.begin(); it!=subsys_union.end(); ++it)
   {
      //if (active_union.find(*it)!=active_union.end()) continue;
      std::set<GlobalSubSysNr> rigid_set1 = arMode1.GetRigidForSubSys(*it);       
      std::set<GlobalSubSysNr> rigid_set2 = arMode2.GetRigidForSubSys(*it);       
      std::set<GlobalSubSysNr> common;       
      // if it is contained in the rigid groups of both, then we take the intersection 
      if (rigid_set1.size()>I_0 && rigid_set1.size())
      {
         std::set_intersection(rigid_set1.begin(),rigid_set1.end(),
             rigid_set2.begin(),rigid_set2.end(), std::inserter(common,common.end()));
      }
      //else we look at the one that is non-zero and remove all that are contained in the other
      else if (rigid_set1.size()>I_0)
      {
         common=rigid_set1;
         auto it=common.begin();
         while (it!=common.end())
         {
            if (std::find(arMode2.mSubSysNrs.begin(),arMode2.mSubSysNrs.end(),*it) !=arMode2.mSubSysNrs.end())
               common.erase(it++);
            else
               ++it;
         }
      }
      else if (rigid_set2.size()>I_0)
      {
         common=rigid_set2;
         auto it=common.begin();
         while (it!=common.end())
         {
            if (std::find(arMode1.mSubSysNrs.begin(),arMode1.mSubSysNrs.end(),*it) !=arMode1.mSubSysNrs.end())
               common.erase(it++);
            else
               ++it;
         }
      }
      if (common.size()>I_0)
      {
         common_rigid.emplace(common);
         if (gDebug)
         {
            Mout << " Found common rigid set: ";
            for_each(common.begin(),common.end(),[](GlobalSubSysNr no) {Mout << no << "  " ;});
            Mout << endl;
            Mout << " ----------------------- " << endl;
         }
      }
   }
   return common_rigid;
}
/* **********************************************************************
********************************************************************** */
