/**
************************************************************************
* 
* @file                SystemDrv.cc
*
* Created:             16-01-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Driver for system generation and modification
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/RotCoordCalcDef.h"
#include "input/TransformPotCalcDef.h"
#include "input/ModSysCalcDef.h"
#include "system/System.h"
#include "pes/molecule/MoleculeInfo.h"
#include "inc_gen/Const.h"
#include "coordinates/rotatemolecule/RotateMolecule.h"
#include "coordinates/lha/LhaDrv.h"
#include "coordinates/rotcoord/RotCoordDrv.h"
#include "coordinates/normalcoord/NormalCoordDrv.h"

namespace midas
{
namespace molecule
{
class MoleculeInfo;
}
}
void TransformPotDrv(input::TransformPotCalcDef*,System*);
void FalconDrv(input::FalconCalcDef*,System*);
void FreqAnaDrv(molecule::MoleculeInfo& arMolInfo, const input::FreqAnaCalcDef& arCalcDef);

/**
 * Run coordinate generation
**/
void SystemDrv()
{

   Mout << std::endl << std::endl;
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin System Generation ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << std::endl;

   for (In i = I_0; i < gModSysCalcDefs.size(); ++i) 
   {
      System system;
      std::vector<std::string> system_names = gModSysCalcDefs[i].GetSysNames();
      bool has_system= (!system_names.empty());
      for (In n = I_0 ; n < system_names.size(); ++n)
      {  
         In sys_no = -I_1;
         for (In s = I_0; s < gSystemDefs.size(); ++s)
         {
            if (gSystemDefs[s].Name() == system_names[n])
            {
               sys_no = s;
               System subsystem(&gSystemDefs[s]);
               system.AddSubSystem(std::move(subsystem));
            }
         }
         if (sys_no == -I_1) 
         {
            MIDASERROR("System not found");
         }
      }

      if (has_system)
      {
         system.Write(gMainDir + "/" + "Input.molden", "MOLDEN");
      }

      Out72Char(Mout,'$','$','$');
      OneArgOut72(Mout, " Begin System Modification of Subsystem " + std::to_string(i), '$');
      Out72Char(Mout,'$','$','$');

      if (gModSysCalcDefs[i].GetDoGeoOpt())
      {
         MIDASERROR("Geometry optimization not implemented");
      } 

      if (gModSysCalcDefs[i].GetRemoveGlobalTR())
      {
         MidasAssert(has_system, "No system given for RMGLOBALTR");
         system.RemoveGlobalTransRot();
         //system.Write("removed_tr.molden","MOLDEN");
      }
      
      if (gModSysCalcDefs[i].GetDoVibCoordOpt())
      {
         switch (gModSysCalcDefs[i].GetVibCoordScheme())
         {
            case input::VibCoordScheme::ROTCOORD:
            {
               if (has_system)
               {
                  midas::molecule::MoleculeInfo mol_info(system,system.GetGlobalSubSysSet(), system.GetGlobalModeSet());
                  RotCoordDrv(gModSysCalcDefs[i].GetpRotCoordCalcDef(), &mol_info);
               }
               else 
               {
                  RotCoordDrv(gModSysCalcDefs[i].GetpRotCoordCalcDef(), nullptr);
               }
               break;
            }
            case input::VibCoordScheme::TRANSFORMPOT:
            {
               TransformPotDrv(gModSysCalcDefs[i].GetpTransformPotCalcDef(),&system);
               break;
            }
            case input::VibCoordScheme::FREQANA:
            {
               midas::molecule::MoleculeInfo mol_info(system,system.GetGlobalSubSysSet(),system.GetGlobalModeSet());
               FreqAnaDrv(mol_info, *gModSysCalcDefs[i].GetpFreqAnaCalcDef());
               break;
            }
            case input::VibCoordScheme::FALCON:
            {
               MidasAssert(has_system,"No system given for FALCON");
               FalconDrv(gModSysCalcDefs[i].GetpFalconCalcDef(),&system);
               break;
            }
            case input::VibCoordScheme::ROTATEMOLECULE:
            {
               if (has_system)
               {
                  DoRotateMolecule(gModSysCalcDefs[i], system); // Passing ModSysCalcDef and system
               }
               break;
            }
            case input::VibCoordScheme::LHA:
            {
               MidasAssert(has_system, "No system given for LHA");
               LhaDrv(gModSysCalcDefs[i], system);
               break;
            }
            case input::VibCoordScheme::NORMALCOORDINATES:
            {
               MidasAssert(has_system, "No system given for normal coordinate generation");
               NormalCoordDrv(gModSysCalcDefs[i], system);
               break;
            }
            case input::VibCoordScheme::ERROR:
            default:
            {
               MIDASERROR("VibCoordScheme unknown");
               break;
            }
         }
      }
   }
}
