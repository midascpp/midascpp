/*
************************************************************************
*  
* @file                operator_utils.h
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Utilities for transforming and writing mop files 
*                      from gradients and Hessians
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SYSTEM_OPERATOR_UTILS_H_INCLUDED
#define SYSTEM_OPERATOR_UTILS_H_INCLUDED

#include <string>
#include <utility>

#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"

namespace midas::system::util
{

/**
 * Transform gradient and Hessian:
 *    g_trans = M^T g
 *    H_trans = M^T H M
 *
 * @note
 *    This can be used for projecting out translation and rotation if aTrans = (1-LL^T).
 *    It can also be used for obtaining the reduced gradient and Hessian if aTrans is the set of vibrational modes (non-square matrix).
 **/
void transform_grad_hess
   (  MidasVector& arGradient
   ,  MidasMatrix& arHessian
   ,  const MidasMatrix& aM
   );
void transform_hessian
   (  MidasMatrix& arHessian
   ,  const MidasMatrix& aM
   );

/**
 * Diagonalize mass-weighted Hessian to obtain normal modes and frequencies
 **/
std::pair<MidasVector, MidasMatrix> diagonalize_hessian
   (  const MidasMatrix& aMwHessian
   ,  bool aRmZeroFreqComponents = false
   );

/**
 * Write operator file from mass-weighted gradient and Hessian
 **/
void write_operator
   (  const MidasVector& aMwGradient
   ,  const MidasMatrix& aMwHessian
   ,  const std::string& aFileName
   ,  const std::vector<std::string>& aModeNames = {}
   );
void write_operator
   (  const MidasMatrix& aMwHessian
   ,  const std::string& aFileName
   ,  const std::vector<std::string>& aModeNames = {}
   );

} /* namespace midas::system::util */

#endif /* SYSTEM_OPERATOR_UTILS_H_INCLUDED */
