#ifndef LIBMDA_UTIL_UNIQUE_ANY_TYPE_H_INCLUDED
#define LIBMDA_UTIL_UNIQUE_ANY_TYPE_H_INCLUDED

#include <type_traits>
#include <typeinfo>
#include <typeindex>
#include <stdexcept>
#include <string>

namespace libmda
{
namespace util
{
namespace detail
{
/**
 *
 **/
class unique_any_type_base
{
   public:
      /**
       *
       **/
      unique_any_type_base() = default;

      /**
       *
       **/
      virtual ~unique_any_type_base() = default;

   private:
};

/**
 *
 **/
template<class T>
class unique_any_type_explicit
   : public unique_any_type_base
{
   public:
      /**
       *
       **/
      unique_any_type_explicit(const T& t)
         : m_data(t)
      {
      }
      
      /**
       *
       **/
      unique_any_type_explicit(T&& t)
         : m_data(std::move(t))
      {
      }

      /**
       *
       **/
      const T& get() const
      {
         return m_data;
      }
      
      /**
       *
       **/
      T& get()
      {
         return m_data;
      }

   private:
      /**
       *
       **/
      T m_data;
};

} /* namespace detail */

/**
 *
 **/
class unique_any_type
{
   public:
      using type_index = std::type_index;

      /**
       *
       **/
      unique_any_type()
         : m_data(nullptr)
         , m_type(typeid(nullptr))
      {
      }
      
      /**
       *
       **/
      template< class T
              , class T_decayed = typename std::decay<T>::type
              , typename std::enable_if<!std::is_same<T_decayed,unique_any_type>::value>::type* = nullptr 
              >
      unique_any_type(T&& t)
         : m_data(new detail::unique_any_type_explicit<T_decayed>(std::forward<T>(t)))
         , m_type(typeid(T_decayed))
      {
      }
      
      /**
       *
       **/
      unique_any_type(unique_any_type&& at)
         : m_data(nullptr)
         , m_type(typeid(nullptr))
      {
         std::swap(m_data, at.m_data);
         std::swap(m_type, at.m_type);
      }

      /**
       *
       **/
      ~unique_any_type()
      {
         if(m_data)
            delete m_data;
      }


      /**
       *
       **/
      unique_any_type& operator=(unique_any_type&& at)
      {
         std::swap(m_data, at.m_data);
         std::swap(m_type, at.m_type);
         return *this;
      }

      
      /**
       *
       **/
      template<class T
             , class T_decayed = typename std::decay<T>::type
             , typename std::enable_if<!std::is_same<T_decayed,unique_any_type>::value>::type* = nullptr
             >
      unique_any_type& operator=(T&& t)
      {
         if(m_type == std::type_index(typeid(T)))
         {
            static_cast<detail::unique_any_type_explicit<T_decayed>*>(m_data)->get() = std::forward<T>(t);
         }
         else
         {
            if(m_data)
               delete m_data;
            m_data = new detail::unique_any_type_explicit<T_decayed>(std::forward<T>(t));
            m_type = std::type_index(typeid(T_decayed));
         }
         return *this;
      }

      
      /**
       *
       **/
      template<class T>
      const T& get() const
      {
         assert_type_index<T>();
         return static_cast<const detail::unique_any_type_explicit<T>*>(m_data)->get();
      }
      
      /**
       *
       **/
      template<class T>
      T& get()
      {
         assert_type_index<T>();
         return static_cast<detail::unique_any_type_explicit<T>*>(m_data)->get();
      }

      /**
       *
       **/
      const std::type_index& type() const
      {
         return m_type;
      }
      
      /**
       *
       **/
      std::string type_name() const
      {
         return std::string(m_type.name());
      }

   private:
      /**
       * 
       **/
      template<class T>
      void assert_type_index() const
      {
         if(std::type_index(typeid(T)) != m_type)
            throw std::runtime_error("bad cast: trying get type '" + std::string(typeid(T).name()) + "' from 'unique_any_type<" + m_type.name() + ">'");
      }
      
      /**
       *
       **/
      detail::unique_any_type_base* m_data;
      /**
       *
       **/
      std::type_index m_type;
};

// relational operators
inline bool operator<(const unique_any_type& a1, const unique_any_type& a2)
{
   return a1.type() < a2.type();
}

// Disable an erroneous infinte recursion warning from clang.
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winfinite-recursion"
#endif /* __clang__ */
// output operator
inline std::ostream& operator<<
   (  std::ostream& os
   ,  const unique_any_type& a
   )
{
   if(a.type() == std::type_index(typeid(double)))
   {
      os << " unique_any_type: type = double, value = " << a.template get<double>();
   }
   else if(a.type() == std::type_index(typeid(int)))
   {
      os << " unique_any_type: type = int, value = " << a.template get<int>();
   }
   else if(a.type() == std::type_index(typeid(std::string)))
   {
      os << " unique_any_type: type = std::string, value = " << a.template get<std::string>();
   }
   else if(a.type() == std::type_index(typeid(bool)))
   {
      os << " unique_any_type: type = bool, value = " << a.template get<bool>();
   }
   else
   {
      os << " unique_any_type: operator<< not implemented for type = " << a.type_name();
   }
   return os;
}
// Enable the compiler warning again.
#ifdef __clang__
#pragma clang diagnostic pop
#endif /* __clang__ */

} /* namespace util */
} /* namespace libmda */

#endif /* LIBMDA_UTIL_UNIQUE_ANY_TYPE_H_INCLUDED */
