#ifndef LIBMDA_IMDA_ASSIGN_H_INCLUDED
#define LIBMDA_IMDA_ASSIGN_H_INCLUDED

#include "imda_access.h"
#include "../util/for_loop_expand.h"

namespace libmda
{

namespace imda
{

// make structs with operators:
// =, +=, -=, *=, /=
// for element by element operations
// these are used by assign mixins
#define LIBMDA_CREATE_EQUALOPER(NAME,OP) \
struct NAME \
{ \
   template<typename V, typename U, typename... ints> \
   static void apply(V& v, const U& u, const ints... i) \
   { v.at(i...) OP u.at(i...); } \
  \
   /*template<typename V, typename U, class meta::Enable_if<util::has_vec_at<V>::value && util::has_vec_at<U>::value> > */\
   /*static void apply_vec(V& v, const U& u, const typename V::size_type i) */\
   /*{ v.vec_at(i) OP u.vec_at(i); }*/ \
};

LIBMDA_CREATE_EQUALOPER(op_equal,=)
LIBMDA_CREATE_EQUALOPER(op_plus_equal,+=)
LIBMDA_CREATE_EQUALOPER(op_sub_equal,-=)
LIBMDA_CREATE_EQUALOPER(op_mult_equal,*=)
LIBMDA_CREATE_EQUALOPER(op_div_equal,/=)
#undef LIBMDA_CREATE_EQUALOPER

struct equal_functor
{
   template<class V, class U, class... ints>
   void operator()(V&& v, U&& u, ints... i) const
   { v.at(i...) = u.at(i...); }
};

//
// mixin that provides = operator overloads
//
template<class dims_check, class A>
struct elem_assign: A
{
   //
   // operator=
   //
   // does below function work/get called???!??? (ian: I think maybe not)
   //template<class C = A>
   //auto operator=(const type& v)
   //   -> decltype(std::declval<C>().self())
   //{
   //   // CHECK FOR SELF ASSIGNMENT?? :O
   //   if(this!=&v)
   //   {
   //      dims_check::apply(*this,v);
   //      util::for_loop_expand<op_equal>::apply((*this),v);
   //   }
   //   return this->self();
   //}
   
   //
   // operator=
   //
   template<class B
          , class C = A
          >
   auto operator=(const access_combined<B>& v)
      -> decltype(std::declval<C>().self())
   {
      dims_check::apply(*this,v);
      //util::for_loop_expand<op_equal>::apply((*this),v);
      util::for_each_elem(equal_functor(),(*this),v);
      return this->self();
   }
   //template<typename B, 
   //   meta::iEnable_if<(util::has_vec_at<A>::value && util::has_vec_at<B>::value)> = 0 >
   //type& operator=(const IMDAVecAccessComb<B>& v)
   //{
   //   //cout << " using VEC ASSIGNER " << endl;
   //   dims_check::apply(*this,v);
   //   for(size_type i = 0; i < this->self().size(); ++i)
   //      this->self().vec_at(i) = v.vec_at(i);
   //   return this->self();
   //}
};

//
// mixin that provides += operator overloads
//
template<class dims_check, class A>
struct elem_assign_add: A
{
   template<class B
          , class C = A
          >
   auto operator+=(const access_combined<B>& v)
      -> decltype(std::declval<C>().self())
   {
      dims_check::apply(*this,v);
      util::for_loop_expand<op_plus_equal>::apply((*this),v);
      return this->self();
   }
};

//
// mixin that provides -= operator overloads
//
template<class dims_check, class A>
struct elem_assign_sub: A
{
   template<class B
          , class C = A
          >
   auto operator-=(const access_combined<B>& v)
      -> decltype(std::declval<C>().self())
   {
      dims_check::apply(*this,v);
      util::for_loop_expand<op_sub_equal>::apply((*this),v);
      return this->self();
   }
};

//
// mixin that provides *= operator overload
//
template<class dims_check, class A>
struct elem_assign_mul: A
{
   template<class B
          , class C = A
          >
   auto operator*=(const access_combined<B>& v)
      -> decltype(std::declval<C>().self())
   {
      dims_check::apply(*this,v);
      util::for_loop_expand<op_mult_equal>::apply((*this),v);
      return this->self();
   }
};
   
//
// mixin that provides /= operator overload
//
template<class dims_check, class A>
struct elem_assign_div: A
{
   template<class B
          , class C = A
          >
   auto operator/=(const access_combined<B>& v)
      -> decltype(std::declval<C>().self())
   {
      dims_check::apply(*this,v);
      util::for_loop_expand<op_div_equal>::apply((*this),v);
      return this->self();
   }
};

} // namespace imda
} // namespace libmda

#endif /* LIBMDA_IMDA_ASSIGN_H_INCLUDED */
