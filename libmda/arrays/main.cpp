#include <iostream>
#include "MultiDimArray.h"
using libmda::MDA;
using namespace std;

int main()
{
    ///// MDA access timing /////
    clock_t start, end;
    int N = 16;
    //MDA<double,3> a(N,N,N); both work now ;)
    MDA<double,3> a(16,16,16); 
    start = clock();
    for(int count=0;count<200000;++count)
        for(int i=0;i<a.dim<0>();++i)                    // First two same and little slower than att below - last much slower
            for(int j=0;j<a.dim<1>();++j)
                for(int k=0;k<a.dim<2>();++k)
                    a(i,j,k)=count;
                    //a[i][j][k]=count;
                    //a.at(i,j,k)=count;
    end = clock();
    cout << end - start << '\n';

    int NN = N*N*N;
    start = clock();
    for(int count=0;count<200000;++count)
        for(int i=0;i<NN;i++)
            a.att(i) = count;
    end = clock();
    cout << end - start << '\n';

  return(0);
}
