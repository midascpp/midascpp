#ifndef LIBMDA_UTIL_SIGNUM_H_INCLUDED
#define LIBMDA_UTIL_SIGNUM_H_INCLUDED

/////
// Disclaimer: signum() originally taken from Stack Overflow http://stackoverflow.com/a/4609795/2964487
//             all credit goes to user79758
//
/////

#include <type_traits>
#include "../meta/std_wrappers.h"

namespace libmda
{
namespace numeric
{

// sign enum
enum sign { neg = -1, zero = 0, pos = 1 };

//
// signum 
//
template <typename T> 
inline constexpr int signum(T x, std::false_type is_signed) 
{
   return T(0) < x;
}

template <typename T> 
inline constexpr int signum(T x, std::true_type is_signed) 
{
   return (T(0) < x) - (x < T(0));
}

template <typename T> 
inline constexpr int signum(T x) 
{
   return signum(x, std::is_signed<T>());
}

//
// pos and neg for floating point (just redirects to float_pos)
//
template<class T
       , iEnable_if<std::is_floating_point<T>::value> = 0
       >
inline bool positive(T t)
{
   return float_pos(t);
}

template<class T
       , iEnable_if<std::is_floating_point<T>::value> = 0
       >
inline bool negative(T t)
{
   return !pos(t);
}

//
// pos and neg for integral types 
//
template<class T
       , iEnable_if<std::is_integral<T>::value> = 0
       >
inline bool positive(T t)
{
   return (signum(t) != sign::neg); // zero is also "positive"
}

template<class T
       , iEnable_if<std::is_integral<T>::value> = 0
       >
inline bool negative(T t)
{
   return !pos(t);
}

} /* namespace numeric */
} /* namespace libmda */

#endif /* LIBMDA_UTIL_SIGNUM_H_INCLUDED */
