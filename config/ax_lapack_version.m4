# ===========================================================================
#         http://www.gnu.org/software/autoconf-archive/ax_lapack.html
# ===========================================================================
#
# SYNOPSIS
#
#   AX_LAPACK_VERSION()
#
# DESCRIPTION
#
#serial 7

AC_DEFUN([AX_LAPACK_VERSION], [

# Get fortran linker name of LAPACK function to check for.
dnl AC_LANG(C)
AC_LANG_CONFTEST([dnl
      AC_LANG_PROGRAM(dnl
         [[
#include <stdio.h>
extern void ilaver_(int* major,int* minor,int* patch);
         ]],
         [[
int major=0;
int minor=0;
int patch=0;
ilaver_(&major,&minor,&patch);
printf("%d.%d.%d\n",major,minor,patch);
//FILE* fptr = fopen("conftest.out","w");
//fprintf(fptr, "%d.%d.%d\n",major,minor,patch);
//fclose(fptr);
         ]])
])
g++ $LAPACK_LIBS $BLAS_LIBS $LIBS $FLIBS conftest.cpp  
dnl AC_LANG_POP(C)

])dnl AX_LAPACK_VERSION
