#!/bin/sh
#
# Simple script to update the version number in configure.ac and
# regenerate the configure script.
#
# JJ07
#

test -f configure.ac || { echo "Not in top level source dir!"; exit 1; }

if [ "x$1" != "x" ]; then
	version=$1
	echo "$1" | grep -q '^[0-9]\+\.[0-9].*'
	[ $? != 0 ] && { echo "Invalid version format: $1"; exit 1;}
else
	echo "No version specified!"
	exit 1
fi

tmpfile=/tmp/configure.ac.$$

sed "s/AC_INIT(\[\(.*\)\],\[.*\],\[\(.*\)\])/\
AC_INIT([\\1],[$version],[\\2])/" configure.ac > $tmpfile
mv $tmpfile configure.ac
autoconf

