#ifndef SCALAR_H_INCLUDED
#define SCALAR_H_INCLUDED

// declaration
#include "Scalar_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// implementation
#include "Scalar_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* SCALAR_H_INCLUDED */
