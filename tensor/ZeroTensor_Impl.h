#ifndef ZEROTENSOR_IMPL_H_INCLUDED
#define ZEROTENSOR_IMPL_H_INCLUDED

#include "util/Io.h"
#include "tensor/Scalar.h"
#include "tensor/SimpleTensor.h"

/**
 * Implementation of Contract for ZeroTensor.
 * @param indices_inner_this
 * @param indices_outer_this
 * @param other_in
 * @param indices_inner_other
 * @param indices_outer_other
 * @param dims_result
 * @param indices_left_result
 * @param indices_right_result
 * @param conjugate_left
 * @param conjugate_right
 * @result                      Contracted tensor.
 **/
template <class T>
BaseTensor<T>* ZeroTensor<T>::Contract_impl
   (  const std::vector<unsigned int>& indices_inner_this
   ,  const std::vector<unsigned int>& indices_outer_this
   ,  const BaseTensor<T>&             other_in
   ,  const std::vector<unsigned int>& indices_inner_other
   ,  const std::vector<unsigned int>& indices_outer_other
   ,  const std::vector<unsigned int>& dims_result
   ,  const std::vector<unsigned int>& indices_left_result
   ,  const std::vector<unsigned int>& indices_right_result
   ,  bool conjugate_left
   ,  bool conjugate_right
   )  const
{
   if(dims_result.size() == 0)
   {
      return new Scalar<T>(static_cast<T>(0.0));
   }
   return new ZeroTensor<T>(dims_result);
}

/**
 * Contructor from dimensions.
 * @param aDims          Dimenstions of tensor.
 **/
template<class T>
ZeroTensor<T>::ZeroTensor
   ( const std::vector<unsigned>& aDims
   )
   : BaseTensor<T>(aDims)
{
}

/**
 * Assign. Copy dimensions and return true if assigned to another ZeroTensor, else return false.
 *
 * @param aOther
 * @return
 *    True if assignment is successful
 **/
template
   <  typename T
   >
bool ZeroTensor<T>::Assign
   (  const BaseTensor<T>* const aOther
   )
{
   if (  aOther->Type() == BaseTensor<T>::typeID::ZERO
      )
   {
      this->mDims = aOther->GetDims();
      return true;
   }
   
   return false;
}

/**
 * Clone the tensor.
 * @return          The cloned tensor.
 **/
template<class T>
ZeroTensor<T>* ZeroTensor<T>::Clone
   (
   ) const
{
   return new ZeroTensor(*this);
}

/**
 * Get type.
 * @return          Type of tensor, i.e. BaseTensor<T>::typeID::ZERO.
 **/
template<class T>
typename BaseTensor<T>::typeID ZeroTensor<T>::Type
   (
   ) const
{
   return BaseTensor<T>::typeID::ZERO;
}

/**
 * Get type as string.
 * @return              Type of tensor as string, i.e. "ZeroTensor".
 **/
template<class T>
std::string ZeroTensor<T>::ShowType
   (
   ) const
{
   return {"ZeroTensor"};
}

/**
 * Contract away one index of tensor with a vector (1D tensor).
 * @param aIdx              Index to contract.
 * @param aTensor           Vector (1D tensor) to contract with.
 * @return                  Returns the contracted result (remember to delete).
 **/
template<class T>
BaseTensor<T>* ZeroTensor<T>::ContractDown
   ( const unsigned int aIdx
   , const BaseTensor<T>* aTensor
   ) const
{
   try
   {
      // Ensure aTensor is a SimpleTensor instance
      MidasAssert(aTensor->NDim() == 1, "ContractDown: input Tensors must be 1D, i.e. a vector.");
      const SimpleTensor<T>& vect = dynamic_cast<const SimpleTensor<T>&>(*aTensor);
      /* Compute output dimensions and initialize output tensor
      *        * new_dims = [ old_dims[:aIdx-1],  old_dims[aIdx+1:] ] */
      std::vector<unsigned int> newdims;
      for(auto elem=this->mDims.begin();elem<this->mDims.end();elem++)
      {
         if(elem == this->mDims.begin()+aIdx)
         {
            MidasAssert((*elem == aTensor->GetDims()[0]),"Contraction extents are not equal");
         }
         else
         {
            newdims.push_back(*elem);
         }
      }

      return new ZeroTensor(newdims);
   }
   catch(...)
   {
      throw; // whatever we catch we just rethrow to get an error.
   }
}

/**
 * Contract forward a single index.
 * @param aIdx                Index to contract.
 * @param aTensor             Maxtrix (2D tensor) to constract with.
 * @return                    Returns the contracted result (remember to delete).
 **/
template<class T>
BaseTensor<T>* ZeroTensor<T>::ContractForward
   ( const unsigned int aIdx
   , const BaseTensor<T>* aTensor
   ) const
{
   try
   {
      // Ensure aTensor is a SimpleTensor instance
      MidasAssert(aTensor->NDim() == 2, "ContractForward: input Tensor must be 2D, i.e. a matrix.");
      const SimpleTensor<T>& matrix = dynamic_cast<const SimpleTensor<T>&>(*aTensor);
      /* Compute output dimensions and initialize output tensor
       * new_dims = [ old_dims[:aIdx-1], matrix.dims[1], old_dims[aIdx+1:] ] */
      std::vector<unsigned int> newdims(0);
      for(auto elem = this->mDims.begin(); elem < this->mDims.end(); elem++)
      {
         if(elem == this->mDims.begin()+aIdx)
         {
            MidasAssert((*elem == matrix.GetDims()[1]), "Contraction extents are not equal");
            newdims.push_back(matrix.GetDims()[0]);
         }
         else
         {
            newdims.push_back(*elem);
         }
      }
      return new ZeroTensor(newdims);
   }
   catch(...)
   {
      throw; // whatever we catch we just rethrow to get an error.
   }
}

/**
 * Contract forward a single index.
 * @param aIdx           Index to contract. All indices coming after are 'pushed back' one place.
 * @param aTensor        Vector (1D tensor) to constract with.
 * @return               Returns the contracted result (remember to delete).
 **/
template <class T>
BaseTensor<T>* ZeroTensor<T>::ContractUp
   ( const unsigned int aIdx
   , const BaseTensor<T>* aTensor
   ) const
{
   try
   {
      // Ensure aTensor is a SimpleTensor instance
      MidasAssert(aTensor->NDim() == 1, "ContractUp: input Tensor must be 1D, i.e. a vector.");
      const SimpleTensor<T>& vect=dynamic_cast<const SimpleTensor<T>&>(*aTensor);
      /* Compute output dimensions and initialize output tensor
      * new_dims = [ old_dims[:idx] vector.dims[0], old_dims[idx:] ] */
      std::vector<unsigned int> newdims(0);
      for(unsigned int i=0;i<this->NDim();i++)
      {
         if(i == aIdx)
         {
            newdims.push_back(vect.GetDims()[0]);
         }
         newdims.push_back(this->mDims[i]);
      }
      if(aIdx == this->NDim())
      {
         newdims.push_back(vect.GetDims()[0]);
      }
      return new ZeroTensor(newdims);
   }
   catch(...)
   {
      throw; // whatever we catch we just rethrow to get an error.
   }
}

/**
 * Zero the tensor. In this case we do nothing as the tensor is defined as a ZeroTensor.
 **/
template<class T>
void ZeroTensor<T>::Zero
   (
   )
{
   // do nothing.
}

/**
 * Scale the tensor. In this case we do nothing as the tensor is defined as a ZeroTensor.
 * @param aScalar    Scalar to scale with.
 **/
template<class T>
void ZeroTensor<T>::Scale
   ( const T& aScalar
   )
{
   // do nothing.
}

/**
 * Set to absolute value
 **/
template
   <  class T
   >
void ZeroTensor<T>::Abs
   (
   )
{
}

/**
 *
 **/
template<class T>
typename DotEvaluator<T>::dot_t ZeroTensor<T>::Dot
   (  const BaseTensor<T>* const other
   )  const
{
   return midas::math::Conj(other->DotDispatch(this)); // could also just short-circuit and return: 'static_cast<dot_t>(0.0)'
}

/**
 * Axpy, y = a*x + y, y being the calling tensor. In this case it only works
 * when the argument is another ZeroTensor, in which case nothing happens.
 * @param arX       The other tensor, which is multiplied and added.
 * @param arA       The scalar used for multiplication.
 **/
template<class T>
void ZeroTensor<T>::Axpy(const BaseTensor<T>& arX, const T& arA)
{
   // First assert that the two tensors have identical shapes.
   assert_same_shape(*this, arX);
      
   // Then switch according to type of the other tensor.
   // Cases, other is
   //  - ZeroTensor; do nothing.
   //  - everything else; error, since it would make the calling tensor
   //    non-zero. Can't convert tensor class in-place...
   switch(arX.Type())
   {
      case BaseTensor<T>::typeID::ZERO:
      {
         // Adding a zero-tensor, i.e. do nothing...
         break;
      }
      default:
      {
         MIDASERROR("In ZeroTensor<T>::Axpy: Not impl. for other tensor being of type '"+arX.ShowType()+"'.");
         break;
      }
   }
}

#endif /* ZEROTENSOR_IMPL_H_INCLUDED */
