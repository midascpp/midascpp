#include<sstream>
#include<algorithm>
#include"ArrayHopper.h"

/* ================================ TGVertex ================================ */

template <class T>
TGVertex<T>::TGVertex(const unsigned int aRank):
   mRank{aRank}
  ,mModeMatrices{}
  ,mIndexPositions{}
{}

template <class T>
TGVertex<T>::TGVertex(
      const unsigned int aRank
     ,const std::vector<std::shared_ptr<TGMatrix<T>>>& aModeMatrices
     ,const std::vector<unsigned int>&                 aIndexPositions):
   mRank          {aRank}
  ,mModeMatrices  {aModeMatrices}
  ,mIndexPositions{aIndexPositions}
{
   // Check that all matrices had the correct rank
   for(const auto& mode_matrix: mModeMatrices)
   {
      if(mode_matrix->GetNRow() != mRank)
      {
         std::stringstream message;
         message << "Attempting to add a Mode Matrix of rank " << mode_matrix->GetNRow();
         message << " to a vertex of rank " << mRank;
         throw std::runtime_error(message.str());
      }
   }
}

template <class T>
unsigned int TGVertex<T>::GetRank() const
{
   return mRank;
}

template <class T>
const std::vector<std::shared_ptr<TGMatrix<T>>>& TGVertex<T>::GetModeMatrices() const
{
   return mModeMatrices;
}

template <class T>
const std::vector<unsigned int>& TGVertex<T>::GetIndexPositions() const
{
   return mIndexPositions;
}

template<class T>
std::string TGVertex<T>::Prettify() const
{
   std::stringstream output;
   output << "Rank=" << this->mRank;
   for(std::vector<unsigned int>::size_type i=0;i<this->mModeMatrices.size();++i)
   {
      output << std::endl << "Dimension #" << this->mIndexPositions[i];
      output << std::endl << this->mModeMatrices[i]->Prettify();
   }
   return output.str();
}

//> Keep only a set of indices and change their locations.
//! From a set of vertices, create a new set of vertices keeping only those dimensions
//! whose indices are present in old_indices, and change their locations to the ones
//! given in new_indices.
template <class T>
std::vector<TGVertex<T>> filter_and_swap_indices(
      const std::vector<TGVertex<T>>&  vertices
     ,const std::vector<unsigned int>& old_indices
     ,const std::vector<unsigned int>& new_indices)
{
   std::vector<TGVertex<T>> new_vertices;
   for(const auto& vertex: vertices)
   {
      std::vector<std::shared_ptr<TGMatrix<T>>> new_mode_matrices;
      std::vector<unsigned int>                 new_index_positions;

      auto mode_matrices  (vertex.GetModeMatrices());
      auto index_positions(vertex.GetIndexPositions());
      for(std::vector<unsigned int>::size_type i=0;i<mode_matrices.size();++i)
      {
         unsigned int loc=0;
         for(const auto& old_index: old_indices)
         {
            if( index_positions[i]==old_index )
            {
               new_mode_matrices  .push_back(mode_matrices[i]);// push old matrix
               new_index_positions.push_back(new_indices[loc]);// new index position
            }
            ++loc;
         }
      }
      new_vertices.push_back(TGVertex<T>( vertex.GetRank(), new_mode_matrices, new_index_positions ));
   }
   return new_vertices;
}

/* ================================= TGEdge ================================= */
template <class T>
TGEdge<T>::TGEdge(const unsigned int           aFrom
                 ,const unsigned int           aTo
                 ,std::shared_ptr<TGMatrix<T>> aMatrix):
   mFrom  {aFrom}
  ,mTo    {aTo  }
  ,mMatrix{aMatrix}
{
}

template <class T>
std::string TGEdge<T>::Prettify() const
{
   std::stringstream output;
   output << " ("<<this->mFrom<<"->"<<this->mTo<<"):";
   output << std::endl << this->mMatrix->Prettify();

   return output.str();
}

template <class T>
std::vector<TGEdge<T>> shift_vertex_refs(
      const std::vector<TGEdge<T>>&  edges
     ,const unsigned int shift)
{
   std::vector<TGEdge<T>> new_edges;
   for(const auto& edge: edges)
   {
      new_edges.push_back( TGEdge<T>( edge.GetFrom() + shift,
                                      edge.GetTo()   + shift,
                                      edge.GetMatrix() ) );
   }
   return new_edges;
}

template <class T>
unsigned int TGEdge<T>::GetFrom() const
{
   return mFrom;
}

template <class T>
unsigned int TGEdge<T>::GetTo() const
{
   return mTo;
}

template <class T>
const std::shared_ptr<TGMatrix<T>>& TGEdge<T>::GetMatrix() const
{
   return mMatrix;
}

/* ============================== TensorGraph ============================== */
template<class T>
std::vector<unsigned int> collect_vertices_dims(const std::vector<TGVertex<T>>& vertices)
{
   typename std::vector<std::shared_ptr<TGMatrix<T>>>::size_type ndim=0;
   for(const auto& vertex: vertices)
      ndim += vertex.GetModeMatrices().size();
   std::vector<unsigned int> dims(ndim);

   for(const auto& vertex: vertices)
   {
      auto mode_matrices  (vertex.GetModeMatrices());
      auto index_positions(vertex.GetIndexPositions());
      for(std::vector<unsigned int>::size_type i=0;i<mode_matrices.size();++i)
      {
         dims[index_positions[i]]=mode_matrices[i]->GetNCol();
      }
   }
   return dims;
}

template <class T>
std::vector<std::pair<unsigned int,unsigned int>> TensorGraph<T>::LocateIndices() const
{
   std::vector<std::pair<unsigned int,unsigned int>> index_loc(this->NDim());
   unsigned int vertex_id=0;
   for(const auto& vertex: mVertices)
   {
      unsigned int matrix_id=0;
      for(const auto& index_pos: vertex.mIndexPositions)
      {
         index_loc[index_pos]=std::pair<unsigned int, unsigned int>( vertex_id, matrix_id );
         ++matrix_id;
      }
      ++vertex_id;
   }
   return index_loc;
}

template <class T>
TensorGraph<T>::TensorGraph(const std::vector<TGVertex<T>> aVertices
                           ,const std::vector<TGEdge<T>>   aEdges   ):
   BaseTensor<T>( collect_vertices_dims(aVertices) )
  ,mVertices{aVertices}
  ,mEdges   {aEdges   }
{
   for(const auto& edge: mEdges)
   {
      if( edge.mMatrix->GetNRow() != mVertices[edge.mFrom].mRank ||
          edge.mMatrix->GetNCol() != mVertices[edge.mTo  ].mRank )
      {
         std::stringstream message;
         message << "Dimensions of edge matrix were ";
         message << edge.mMatrix->GetNRow()     << " X " << edge.mMatrix->GetNCol();
         message << ", expected ";
         message << mVertices[edge.mFrom].mRank << " X " << mVertices[edge.mTo  ].mRank;
         throw std::runtime_error(message.str());
      }
   }
}

template<class T>
BaseTensor<T>* TensorGraph<T>::Contract_impl(
             const std::vector<unsigned int>& indices_inner_this,
             const std::vector<unsigned int>& indices_outer_this,
             const BaseTensor<T>&             other_in,
             const std::vector<unsigned int>& indices_inner_other,
             const std::vector<unsigned int>& indices_outer_other,
             const std::vector<unsigned int>& dims_result,
             const std::vector<unsigned int>& indices_result_this,
             const std::vector<unsigned int>& indices_result_other) const
{
   switch(other_in.Type())
   {
      case BaseTensor<T>::typeID::GRAPH:
      {
         // cast other_in as a CanonicalTensor instance
         const TensorGraph<T>& other=dynamic_cast<const TensorGraph<T>&>(other_in);

         // FIRST PASS: Create vertices of the result tensor (pick outer indices)
         // Create the new set of vertices, keeping only outer dimensions, changing
         // their locations according to where they will be in the resulting tensor
         std::vector<TGVertex<T>> new_vertices(
                                 filter_and_swap_indices(this->mVertices,
                                                         indices_outer_this,
                                                         indices_result_this));
         for(const auto& vertex:filter_and_swap_indices(other.mVertices,
                                                         indices_outer_other,
                                                         indices_result_other))
         {
            new_vertices.push_back(vertex);
         }
         // OK, the new vertices are ready

         // Create the new set of edges.
         // First add the old edges, changing the "tos" and the "froms" as necessary.
         // The old edges from "this" stay the same, though (the vertices came in the same order).
         std::vector<TGEdge<T>> new_edges(this->mEdges);
         for(const auto& edge: shift_vertex_refs(other.mEdges, this->mVertices.size()) )
         {
            new_edges.push_back(edge);
         }

         // Now add the new edges that appear due to contractions.
         // Locate in which vertex is each index
         auto indices_loc_this =this->LocateIndices();
         auto indices_loc_other=other.LocateIndices();

         for(std::vector<unsigned int>::size_type i=0;i<indices_inner_this.size();++i)
         {
            auto loc_this =indices_loc_this [ indices_inner_this [i] ];
            auto loc_other=indices_loc_other[ indices_inner_other[i] ];

            std::shared_ptr<TGMatrix<T>> new_edge_matrix( new TGMatrix<T>(
                  matmul(*( this->mVertices[loc_this .first].mModeMatrices[loc_this .second] ), 'N',
                         *( other.mVertices[loc_other.first].mModeMatrices[loc_other.second] ), 'T')
            ));

                                                                             // shift mTo
            new_edges.push_back( TGEdge<T>( loc_this.first, loc_other.first + this->mVertices.size(),
                                            new_edge_matrix ) );
         }
         // We are done!

         TensorGraph<T>* result( new TensorGraph<T>(new_vertices, new_edges) );
         if(result->NDim()==0)
         {
            auto actual_result=result->ToFullTensor();
            delete result;
            return actual_result;
         }
         else
         {
            return result;
         }
      }
      default:
      {
         MIDASERROR("In TensorGraph<T>::Contract_impl: Not impl. for other tensor being of type '"+other_in.ShowType()+"'");
      }
   }
   MIDASERROR("Not implemented.");
}

template<class T>
TensorGraph<T>* TensorGraph<T>::Reorder_impl(const std::vector<unsigned int>&,
                                             const std::vector<unsigned int>&) const
{
   throw std::runtime_error("Not implemented");

}

template<class T>
BaseTensor<T>* TensorGraph<T>::ContractInternal_impl(
      const std::vector<std::pair<unsigned int, unsigned int>>&,
      const std::vector<unsigned int>&) const
{
   throw std::runtime_error("Not implemented");
   
}

template<class T>
TensorGraph<T>* TensorGraph<T>::Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>&) const
{
   throw std::runtime_error("Not implemented");
}

template<class T>
TensorGraph<T>* TensorGraph<T>::Clone() const
{
   throw std::runtime_error("Not implemented");
}

template<class T>
BaseTensor<T>* TensorGraph<T>::ContractDown   (const unsigned int, const BaseTensor<T>*) const
{
   throw std::runtime_error("Not implemented");
}

template<class T>
TensorGraph<T>*   TensorGraph<T>::ContractForward(const unsigned int, const BaseTensor<T>*) const
{
   throw std::runtime_error("Not implemented");
}

template<class T>
TensorGraph<T>*   TensorGraph<T>::ContractUp     (const unsigned int, const BaseTensor<T>*) const
{
   throw std::runtime_error("Not implemented");
}


template<class T>
TensorGraph<T>* TensorGraph<T>::operator*=(const T)
{
   throw std::runtime_error("Not implemented");
}

template<class T>
BaseTensor<T>* TensorGraph<T>::operator+=(const BaseTensor<T>&)
{
   throw std::runtime_error("Not implemented");
}

template<class T>
TensorGraph<T>* TensorGraph<T>::operator*=(const BaseTensor<T>&)
{
   throw std::runtime_error("Not implemented");
}

template<class T>
TensorGraph<T>* TensorGraph<T>::operator/=(const BaseTensor<T>&)
{
   throw std::runtime_error("Not implemented");
}

template<class T>
std::ostream& TensorGraph<T>::write_impl(std::ostream& output) const
{
   throw std::runtime_error("Not implemented");
}

template<class T>
std::string TensorGraph<T>::Prettify() const
{
   std::stringstream output;

   output << "       ----- TensorGraph -----" << std::endl;
   output << this->NDim() << "   [ " ;
   if(this->NDim()>0)
   {
      output << this->mDims[0];
      for (unsigned int i=1;i<this->NDim();i++)
      {
         output << ", " << this->mDims[i];
      }
   }
   output << " ]" << std::endl;

   unsigned int vertex_counter=0;
   for(const auto& vertex: mVertices)
   {
      output << "Vertex #" << vertex_counter++ << ":" << std::endl;
      output << vertex.Prettify() << std::endl;
   }

   unsigned int edge_counter=0;
   for(const auto& edge: mEdges)
   {
      output << "Edge #" << edge_counter++ << ":" << std::endl;
      output << edge.Prettify() << std::endl;
   }
   output << "       -----------------------";
   
   return output.str();
}

template<class T>
typename DotEvaluator<T>::dot_t TensorGraph<T>::Dot
   ( const BaseTensor<T>* const other
   ) const
{
   return midas::math::Conj(other->DotDispatch(this));
}

//! Convert to either SimpleTensor or Scalar.
/*! The algorithm to reconstruct the full tensor goes as follows:
 *
 *  * Construct the ``core tensor'' from the edge matrices:
 *
 *  \f[
 *  C_{\alpha_1, \alpha_2, \dots} =
 *     \prod_{e\in\mathrm{edges}} Z^e_{\alpha_{e_1}\alpha_{e_2}}
 *  \f]
 *
 *  * For each vertex \f$v\f$, construct a tensor
 *
 *  \f[
 *  T^v_{u_{i_1}, u_{i_2}, \dots, \alpha_v} =
 *     \prod_{i\in v}t^{v,i}_{\alpha_v u_i}
 *  \f]
 *
 *  * And then carry out the contractions over rank indices (alphas) to reconstruct the final tensor:
 *
 *  \f[
 *     t_{\mathbf{i}}=\sum_{\mathrm{Rank~indices}}
 *         C_{\alpha_1, \alpha_2, \dots}
 *         \prod_{v\in\mathrm{vertices}} T^v_{u_{i_1}, u_{i_2}, \dots, \alpha_v}
 *  \f]
 *
 *  The tricky part in this last bit is keeping track of where the indeces are going...
 */
template <class T>
BaseTensor<T>* TensorGraph<T>::ToFullTensor() const
{
   std::vector<unsigned int> core_dims;
   size_t core_size=1;

   for(const auto& vertex: mVertices)
   {
      core_dims.push_back(vertex.mRank);
   }
   SimpleTensor<T> core_tensor(core_dims, (T) 1.0);

   for(const auto& edge: mEdges)
   {
      std::vector<unsigned int> slice_dims{ edge.mTo, edge.mFrom };
      std::vector<unsigned int> outer_dims;
      for(unsigned int dim=mVertices.size();dim!=0;--dim)
      {
         unsigned int actual_dim=dim-1;
         if(actual_dim!=slice_dims[0] && actual_dim!=slice_dims[1] )
         {
            outer_dims.push_back(actual_dim);
         }
      }

      ArrayHopper<T> slice_hopper(core_tensor.GetData(), core_dims, core_dims, outer_dims);
      for (auto slice=slice_hopper.begin(); slice!=slice_hopper.end(); ++slice)
      {
         auto edge_matrix_ptr=edge.mMatrix->GetMatrix().get();
         ArrayHopper<T> elem_hopper(slice, core_dims, core_dims, slice_dims);
         for (auto elem=elem_hopper.begin(); elem!=elem_hopper.end(); ++elem)
         {
            *elem *= *edge_matrix_ptr;
            ++edge_matrix_ptr;
         }
      }

   }
   std::vector<unsigned int> empty_vertices;
   for(unsigned int ivertex=0;ivertex<mVertices.size();++ivertex)
   {
      if( mVertices[ivertex].mModeMatrices.size()==0 ) // If vertex is empty
      {
         empty_vertices.push_back(ivertex);
      }
   }

   // Collapse over ranks corresponding to empty vertices
   BaseTensor<T>* next_tensor( core_tensor.CollapseDimensions(empty_vertices) );

   // If the collapsed core tensor has dimension 0, it became a scalar and it is the
   // result we are after
   if(next_tensor->NDim()==0)
   {
      return dynamic_cast<Scalar<T>*>( next_tensor );
   }
   
   // Otherwise, we carry on
   BaseTensor<T>*  old_tensor=next_tensor;

   std::vector<unsigned int> old_tensor_indices;
   for(unsigned int i=this->NDim();i<this->NDim()+next_tensor->NDim();++i)
      old_tensor_indices.push_back(i);

   // Transform with mode matrices. We want to go through vertices in reverse order,
   // for efficiency
   for(auto pvertex=mVertices.rbegin();pvertex!=mVertices.rend();++pvertex)
   {
      if(old_tensor!=next_tensor)
         delete old_tensor;
      old_tensor=next_tensor;

      if(pvertex->mModeMatrices.size()==0)
         continue; // Skip empty vertices

      // --- Prepare vertex tensor and its indices ---
      // Figure out order in which we add the dimensions in this vertex
      std::vector<unsigned int> order;
      for(unsigned int i=0;i<(unsigned int)pvertex->mModeMatrices.size();++i)
         order.push_back(i);
      std::sort(order.begin(), order.end(),
                [pvertex](const unsigned int x, const unsigned int y)
                { return pvertex->mIndexPositions[x] < pvertex->mIndexPositions[y];});

      // Initialize vertex tensor filled with ones
      std::vector<unsigned int> vertex_tensor_dims;
      for(auto i: order)
      {
         vertex_tensor_dims.push_back(pvertex->mModeMatrices[i]->GetNCol());
      }
      vertex_tensor_dims.push_back(pvertex->mRank);
      SimpleTensor<T> vertex_tensor(vertex_tensor_dims, (T) 1.0);

      // We will also prepare the contraction indices
      std::vector<unsigned int> vertex_tensor_indices;
      // Multiply with matrices
      for(auto i: order)
      {
         std::vector<unsigned int> block_indices;
         for(unsigned int index=vertex_tensor.NDim()-1;index!=0;--index)
         {
            unsigned int actual_index=index-1;
            if(actual_index != i)
               block_indices.push_back(index-1);
         }
         std::vector<unsigned int>
               slice_indices(std::vector<unsigned int>{i, vertex_tensor.NDim()-1});
                                                      

         ArrayHopper<T> slice_hopper(vertex_tensor.GetData(),
                                     vertex_tensor_dims, vertex_tensor_dims,
                                     block_indices);
         for (auto slice=slice_hopper.begin(); slice!=slice_hopper.end(); ++slice)
         {
            auto matrix_ptr=pvertex->mModeMatrices[i]->GetMatrix().get();
            ArrayHopper<T> elem_hopper(slice, vertex_tensor_dims, vertex_tensor_dims, slice_indices);
            for (auto elem=elem_hopper.begin(); elem!=elem_hopper.end(); ++elem)
            {
               *elem *= *matrix_ptr;
               ++matrix_ptr;
            }
         }
         vertex_tensor_indices.push_back(pvertex->mIndexPositions[i]);
      }
      // The last contraction index is along the rank dimension
      vertex_tensor_indices.push_back( old_tensor_indices.back() );
      // vertex_tensor is now ready

      // Contract
      next_tensor=contract( vertex_tensor, vertex_tensor_indices,
                            *old_tensor,   old_tensor_indices );

      // Now we need to figure out the indices of the output we just obtained...
      // OK, quick and dirty solution: all the indices from both input tensors, reordered,
      // dropping the last two indices
      std::vector<unsigned int> new_tensor_indices(vertex_tensor_indices);
      new_tensor_indices.insert(new_tensor_indices.end(), old_tensor_indices.begin(),
                                                          old_tensor_indices.end());
      std::sort(new_tensor_indices.begin(), new_tensor_indices.end());
      new_tensor_indices.pop_back();
      new_tensor_indices.pop_back();
      old_tensor_indices=new_tensor_indices;

   }

   if(old_tensor!=next_tensor)
      delete old_tensor;
   return next_tensor;
}
