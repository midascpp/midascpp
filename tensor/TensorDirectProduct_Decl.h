#ifndef TENSORDIRECTPRODUCT_DECL_H_INCLUDED
#define TENSORDIRECTPRODUCT_DECL_H_INCLUDED

#include <memory>
#include <vector>

#include "tensor/BaseTensor.h"

/**
 * Class to handle direct product of tensors, that are to be recompressed.
 **/
template<class T>
class TensorDirectProduct
   : public BaseTensor<T>
{
   public: 
      friend DotEvaluator<T>;

      using connection_t = std::vector<std::pair<unsigned int, unsigned int> >;
      using reverse_connection_t = std::vector<std::vector<unsigned int> >;
      using dot_t = typename DotEvaluator<T>::dot_t;
      using real_t = typename BaseTensor<T>::real_t;

   private:
      // maybe do shared_ptr
      T mCoef;
      std::vector<std::unique_ptr<BaseTensor<T> > > mTensors;
      connection_t mIndexMap;
      reverse_connection_t mReverseConnection;
      
      ///> Contract implementatation
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indicespos_inner_left
         ,  const std::vector<unsigned int>& indicespos_outer_left
         ,  const BaseTensor<T>&             right
         ,  const std::vector<unsigned int>& indicespos_inner_right
         ,  const std::vector<unsigned int>& indicespos_outer_right
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indicespos_result_left
         ,  const std::vector<unsigned int>& indicespos_result_right
         ,  bool = false
         ,  bool = false
         )  const override;

      ///> Called by ContractInternal().
      BaseTensor<T>* ContractInternal_impl
         (  const std::vector<std::pair<unsigned int, unsigned int> >&
         ,  const std::vector<unsigned int>&
         )  const override;

      ///> Reorder implementation
      BaseTensor<T>* Reorder_impl
         (  const std::vector<unsigned int>& neworder
         ,  const std::vector<unsigned int>& newdims
         )  const override;

      //! Called by Slice().
      BaseTensor<T>* Slice_impl
         (  const std::vector<std::pair<unsigned int,unsigned int> >&
         )  const override;

      //!
      void ConjugateImpl() override;
      
      //!
      //std::vector<std::vector<unsigned> > Overlap(const TensorDirectProduct<T>* const) const;

   public:
      ///> ctor from indices
      TensorDirectProduct(const std::vector<unsigned>&, T, std::vector<std::unique_ptr<BaseTensor<T> > >&&, connection_t&&);
      
      ///> Get relevant typeID
      typename BaseTensor<T>::typeID Type() const override; 
      
      ///> Get relevant type as string
      std::string ShowType() const override; 

      ///> Clone the tensor
      BaseTensor<T>* Clone() const override;

      ///>
      real_t Norm2() const override;

      //! Calculate value of element
      T Element
         (  const std::vector<unsigned>&
         )  const override;

      ///> If all are canonical, we calculate and return the rank, else we return 0
      unsigned Rank() const;

      ///> get coefficient
      T Coef() const { return mCoef; }

      ///>
      void NormalizeTensors();

      //! Normalize tensors using pre-calculated norm2's
      void NormalizeTensors
         (  const std::vector<real_t>&
         );

      //! Construct mode matrices
      BaseTensorPtr<T> ConstructModeMatrices
         (  bool=false
         )  const;

      /*============== CONTRACTIONS ==============*/
      ///> Contract one dimension away with a vector. See BaseTensor::ContractDown.
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const override;
      ///> Transform a dimension with a matrix. See BaseTensor::ContractForward.
      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override;
      ///> Insert a dimension with a vector. See BaseTensor::ContractUp.
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override;
            
      /*============== OPERATORS ==============*/
      BaseTensor<T>* operator*= (const T) override { MIDASERROR("Not implemented"); return nullptr; }
      //! Add another tensor (in place).
      BaseTensor<T>* operator+=(const BaseTensor<T>&) override { MIDASERROR("Not implemented"); return nullptr; }
      //! Element-wise multiplication (in-place).
      BaseTensor<T>* operator*=(const BaseTensor<T>&) override { MIDASERROR("Not implemented"); return nullptr; }
      //! Element-wise division (in-place)
      BaseTensor<T>* operator/=(const BaseTensor<T>&) override { MIDASERROR("Not implemented"); return nullptr; }
      
      /*   */
      const std::vector<std::unique_ptr<BaseTensor<T> > >& GetTensors() const { return mTensors; }
      std::vector<std::unique_ptr<BaseTensor<T> > >& GetTensors() { return mTensors; }
      const reverse_connection_t& GetReverseConnection() const { return mReverseConnection; }
      reverse_connection_t& GetReverseConnection() { return mReverseConnection; }
      const connection_t& GetConnection() const { return mIndexMap; }
      
      /*============= QUERY ============*/
      bool AllCanonical() const; ///< Are all tensors Canonical?

      bool SanityCheck() const override;

      /*============== IO ==============*/
      std::string Prettify() const override;
      std::ostream& write_impl(std::ostream& output) const override { MIDASERROR("Not implemented"); return output; }
      
      void DumpInto(T*) const override;
      
      /*============== DOT ==============*/
      dot_t Dot(const BaseTensor<T>* const) const override;

      dot_t DotDispatch(const CanonicalTensor<T>* const) const override;
      dot_t DotDispatch(const TensorDirectProduct<T>* const) const override;
};

#endif /* TENSORDIRECTPRODUCT_DECL_H_INCLUDED */
