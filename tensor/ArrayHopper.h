/**
 ************************************************************************
 * 
 * @file                ArrayHopper.h
 *
 * Created:             01.11.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               ArrayHopperIterator: iterator to navigate elements
 *                      of multi-dimensional arrays in an arbitrary number
 *                      of dimensions, to be used with SimpleTensor.
 *                      ArrayHopper: factory for the iterators.
 * 
 * Detailed Description: Avoid calculating position of the elements at
 *                       every access, instead calculate memory jumps
 *                       for one element to the next one.
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef ARRAYHOPPER_H_INCLUDED
#define ARRAYHOPPER_H_INCLUDED

#include<vector>
#include<iostream>
#include<cstddef>
#include<string>
#include<sstream>
#include<stdexcept>

using UIVec = std::vector<unsigned int>;

//! Return the product of the elements starting from the `idim`-th element.
inline ptrdiff_t dimsproduct(UIVec dims, unsigned int idim)
{
   ptrdiff_t result=1;
   for(auto i=dims.begin()+idim+1; i<dims.end(); ++i)
   {
      result*=*i;
   }
   return result;
}

//! Calculate jumps for the selected hopping parameters. The magic happens here.
/*! Let \f$\left\{D_1,D_2,\dots,D_N\right\}\f$ be the dimensions (shape) of a tensor,
 *  and \f$\mathbf{u}=\left\{u_1,u_2,\dots,u_N| 0\le u_i< D_i\right\}\f$ the position of
 *  an arbitrary element. For a row-major (C) array ordering, the offset of 
 *  the element is given by
 *
 *  \f[
 *     p(\mathbf{u})=\sum_{i=1}^N u_i \prod_{j=i+1}^N D_j =
 *      \left( \left( \dots \left( u_1 D_2 + u_2 \right) \dots D_{N-2} + u_{N-2}  \right) D_{N-1} + u_{N-1}  \right) D_N + u_N
 *  \f]
 *
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  **Example**: For a tensor \f$A\f$ with shape \f$5\times2\times4\times5\f$, element
 *               \f$a_{3103}\f$ has an offset of
 *               \f$\left(\left(3 \cdot 2 + 1 \right) 4 + 0 \right)5 +3 = 143\f$.
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *
 *  A "hopping direction" is a sequence of dimensions
 *  \f$\{v_1, v_2, \dots, v_K | 1 \le v_i \le D_i; v_i\ne v_j ~\mathrm{if}~ i\ne j\}\f$
 *  that we loop over. The \f$v_i\f$ indices are the "jump indices". A hopping direction
 *  defines a "hopping sequence": all possible values for the indices in the dimensions
 *  of the hopping direction and some fixed values for the rest, in the order given by
 *  the hopping direction. A "jump" is the offset between two adjacent elements in a
 *  hopping sequence. For each jumping index $v_i$ there is a corresponding hop $h_i$.
 *
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  **Example**: For the tensor in the example above, the hopping direction \f$\{4,2\}\f$
 *               first iterates over all elements along the fourth dimension, then
 *               along all elements along the second dimension. For any fixed \f$i\f$ and
 *               \f$j\f$ goes through the elements
 *
 *  \f[
 *     a_{i0j0}\rightarrow
 *     a_{i0j1}\rightarrow
 *     a_{i0j2}\rightarrow
 *     a_{i0j3}\rightarrow
 *     a_{i0j4}\rightarrow
 *     a_{i1j0}\rightarrow
 *     a_{i1j1}\rightarrow
 *     a_{i1j2}\rightarrow
 *     a_{i1j3}\rightarrow
 *     a_{i1j4}
 *  \f]
 *
 *  E.g. starting at \f$a_{0000}\f$:
 *
 *  \f[
 *     a_{0000}\rightarrow
 *     a_{0001}\rightarrow
 *     a_{0002}\rightarrow
 *     a_{0003}\rightarrow
 *     a_{0004}\rightarrow
 *     a_{0100}\rightarrow
 *     a_{0101}\rightarrow
 *     a_{0102}\rightarrow
 *     a_{0103}\rightarrow
 *     a_{0104}
 *  \f]
 *
 *  The offsets and the jumps are:
 *
 *  \f[
 *      0 \overset{+1} {\longrightarrow}
 *      1 \overset{+1} {\longrightarrow}
 *      2 \overset{+1} {\longrightarrow}
 *      3 \overset{+1} {\longrightarrow}
 *      4 \overset{+16}{\longrightarrow}
 *     20 \overset{+1} {\longrightarrow}
 *     21 \overset{+1} {\longrightarrow}
 *     22 \overset{+1} {\longrightarrow}
 *     23 \overset{+1} {\longrightarrow}
 *     24
 *  \f]
 *
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *
 *  When we hop in a the \f$k\f$-th hopping index \f$u_{v_k}\f$, all previous hopping
 *  indices (all \f$u_{v_j}\f$ with \f$j\le k\f$) had maximum values \f$D_{v_k}-1\f$
 *  and \f$v_k<D_k-1\f$) and are reset to 0, e.g.
 *
 *  \f{align*}{
 *     \{\dots,  & u_{v_0}=D_{v_0}-1 ,  &  \dots, &  u_{v_1}=D_{v_1}-1 , & \dots, & u_{K-1}=D_{v_{K-1}}-1 , & \dots, & u_{K} \}
 *       \rightarrow\\
 *      \{\dots, & u_{v_0}=0  , & \dots,& u_{v_1}=0 , & \dots, & u_{K-1}=0 , & \dots, & u_{K}+1 \}
 *  \f}
 *
 *  The \f$k\f$-th hop \f$h_k\f$ difference between the offsets of those two elements is given by
 *
 *  \f[
 *      h_k=\sum_{j=1}^{k-1} (1-D_{v_j}) \prod_{i=v_j+1}^N D_i + \prod_{i=v_k+1}^N D_i
 *  \f]
 *
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  **Example:** The formula correctly reproduces the offsets shown above:
 *
 *  \f{align*}{
 *     h_1&=1\\
 *     h_2&=(1-5) + 4\times 5 =16
 *  \f}
 *
 *  If we were yet to hop in the third axis (i.e. the hopping direction would be \f$\{4,2,3\}\f$)
 *  the hop would be
 *
 *  \f[
 *      h_3=(1-5)+(1-2)\times4\times5+5=-19
 *  \f]
 *
 *  This corresponds with the hop from \f$a_{0104}\f$ (offset 24) to \f$a_{0010}\f$ (offset 5).
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *
 *  Subarray hopping
 *  ----------------
 *  get_jumps() can be also used for hopping along continuous sub-sections of the array.
 *  For a certain dimension $k$, let us denote the first index \f$e_k\f$ and the length of
 *  the sub-array \f$d_k\f$. The positions of two consecutive elements become
 *
 *  \f{align*}{
 *     \{\dots,  & u_{v_0}=e_{v_0}+d_{v_0}-1, & \dots, & u_{v_1}=e_{v_1}+d_{v_1}-1, & \dots, & u_{K-1}=e_{v_K}+d_{v_K}-1,  & \dots, & u_{K} \}
 *       \rightarrow\\
 *      \{\dots, & u_{v_0}=e_{v_0}          , & \dots, & u_{v_1}=e_{v_1},           & \dots, & u_{K-1}=e_{v_K},            & \dots, & u_{K}+1 \}
 *  \f}
 *
 *  The expression for the jumps is easily generalized as
 *
 *  \f[
 *      h_k=\sum_{j=1}^{k-1} (1-d_{v_j}) \prod_{i=v_j+1}^N D_i + \prod_{i=v_k+1}^N D_i
 *  \f]
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  **Example:** For the previous example, let's suppose we want to browse through the
 *               sub-array \f$A_{1:3,0:1,2:4,0:2}\f$, with dimensions
 *               \f$3\times2\times3\times3\f$. Starting at the first element 
 *               \f$a_{1020}\f$, the hopping sequence is
 *  \f[
 *     a_{1020}\rightarrow
 *     a_{1021}\rightarrow
 *     a_{1022}\rightarrow
 *     a_{1120}\rightarrow
 *     a_{1121}\rightarrow
 *     a_{1122}\rightarrow
 *  \f]
 *
 *  The offsets and the jumps are:
 *
 *  \f[
 *    50 \overset{+1} {\longrightarrow}
 *    51 \overset{+1} {\longrightarrow}
 *    52 \overset{+18}{\longrightarrow}
 *    70 \overset{+1} {\longrightarrow}
 *    71 \overset{+1} {\longrightarrow}
 *    72
 *  \f]
 *
 *  \f{align*}{
 *     h_1&=1\\
 *     h_2&=(1-3) + 4\times 5 =18
 *  \f}
 *
 *  If we also hop along the third dimension,
 *
 *  \f[
 *      h_3=(1-3)+(1-2)\times4\times5+5=-17
 *  \f]
 *
 *  which corresponds to the jump \f$a_{1122}\f$ (offset 72) to \f$a_{1030}\f$ (offset 55).
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  \see ArrayHopper
 */
inline std::vector<ptrdiff_t> get_jumps(const UIVec& dims,
                                        const UIVec& subdims,
                                        const UIVec& jump_indices)
{
   if(jump_indices.size() > dims.size())
      throw std::runtime_error("You fool! You cannot hop over more dimensions than there are!");

   int njumps=jump_indices.size();
   std::vector<ptrdiff_t> jumps(njumps);
   for(int m=0;m<njumps;++m)
   {
      jumps[m] = dimsproduct(dims, jump_indices[m]);
//      *(jumps.end()-1) *= (dims[jump_indices[m]] - subdims[jump_indices[m]] + 1);
      for(int n=0;n<m;++n)
      {
         jumps[m] += (1-static_cast<ptrdiff_t>(subdims[jump_indices[n]]) ) * 
                              dimsproduct(dims, jump_indices[n]);
      }
   }
   return jumps;
}

template <class T>
class ArrayHopperIterator;

//! Factory to make ArrayHopperIterator's.
/*! Usage
 *  -----
 *  Suppose a \f$3\times4\times2\times3\f$ tensor A. We want to iterate over
 *  all the elements along dimensions 4 and 2 (in that order), starting with
 *  element \f$a_{0000}\f$, that is:
 *  \f[
 *     a_{0000}\rightarrow a_{0001}\rightarrow a_{0002}\rightarrow
 *     a_{0100}\rightarrow a_{0101}\rightarrow a_{0102}\rightarrow
 *     a_{0200}\rightarrow a_{0201}\rightarrow a_{0202}\rightarrow
 *     a_{0300}\rightarrow a_{0301}\rightarrow a_{0302}
 *  \f]
 *
 *  The following piece of code would print all those elements:
 *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 *  std::vector<unsigned int> dims{3,4,2,3};
 *  SimpleTensor<double> tensor(..., dims);
 *  ArrayHopper<double> hopper(
 *                          tensor.GetData(), // This returns a pointer to element a_{0000}.
 *                          tensor.GetDims(),
 *                          tensor.GetDims(), // Redundant here, needed for sub-array iteration.
 *                          std::vector<unsigned int>{3,1});
 *  for(auto i: hopper)
 *     std::cout << i << std::endl;
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  ### Nesting ###
 *
 *  Hoppers can be nested say we want to carry out the operation
 *  \f$b_{jk}=\sum_i a_{ijk}\f$. We need to iterate on one hand over index \f$i\f$
 *  to carry out the summation, and this needs to be performed for each
 *  and every \f$j,k\f$ combination. This could be done as follows:
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 *  std::vector<unsigned int> dims{3,4,2};
 *  SimpleTensor<double> tensor(..., dims);
 *  ArrayHopper<double> outer_hopper(
 *                          tensor.GetData(), // This returns a pointer to element a_{0000}.
 *                          tensor.GetDims(),
 *                          tensor.GetDims(), // Redundant here, needed for sub-array iteration.
 *                          std::vector<unsigned int>{2,1}); // The order is in principle irrelevant,
 *                                                           // but this follows the memory ordering.
 *  // The resulting tensor.
 *  std::vector<unsigned int> result_dims{4,2};
 *  SimpleTensor<double> result(result_dims);
 *  ArrayHopper<double> result_hopper(
 *                          result.GetData(),
 *                          result.GetDims(),
 *                          result.GetDims(),
 *                          std::vector<unsigned int>{1,0}); // To obtain b_{kj} instead of b_{jk}
 *                                                           // we'd use {0,1} instead.
 *  auto result=result_hopper.begin(); // We need a handle for navigating the result
 *
 *  for(auto outer: outer_hopper)
 *  {
 *      ArrayHopper<double> inner_hopper(
 *                              outer, // The starting element is now given by the outer iterator.
 *                              tensor.GetDims(),
 *                              tensor.GetDims(),
 *                              std::vector<unsigned int>{0});
 *      for(auto inner: inner_hopper)
 *      {
 *          *result += *inner;
 *      }
 *      ++result; // We need to advance result by hand.
 *  }
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *  Implementation details
 *  ----------------------
 *  The key thing is the calculation of the jumps (the distance in memory
 *  between one element and the next). This is done by get_jumps(). The second
 *  is the bookkeeping about which dimension is to be hopped over
 *  next, which is taken care of by ArrayHopperIterator::operator++().
 */
template <class T>
class ArrayHopper
{
   private:
      T* const                  mFirst;       //< Position of the first element to be iterated over.
      const UIVec               mDims;        //< Dimensions of the whole array.
      const UIVec               mSubDims;     //< Dimensions of the sub-array being iterated over.
      const UIVec               mJumpIndices; //< Indices being iterated over.
   protected:
      std::vector<ptrdiff_t>    mJumps;       //< The jumps.

      friend ArrayHopperIterator<T>;

   public:

      //! Basic constructor.
      ArrayHopper(T* const     aArray,
                  const UIVec& aDims,
                  const UIVec& aSubDims,
                  const UIVec& aJumpIndices):
         mFirst      {aArray}
        ,mDims       {aDims}
        ,mSubDims    {aSubDims}
        ,mJumpIndices{aJumpIndices}
        ,mJumps      {get_jumps(aDims, aSubDims, aJumpIndices)}
      {
      }

      //! Basic constructor, uses the element pointed by an ArrayHopperIterator as starting point.
      ArrayHopper(const ArrayHopperIterator<T>& iterator,
                  const UIVec& aDims,
                  const UIVec& aSubDims,
                  const UIVec& aJumpIndices):
         ArrayHopper{&(*iterator), aDims, aSubDims, aJumpIndices}
      {
      }

      //! Make an ArrayHopperIterator that points at ArrayHopper::mFirst.
      ArrayHopperIterator<T> begin()
      {
         return ArrayHopperIterator<T>(*this);
      }

      //! Dummy function: exit condition is checked by ArrayHopperIterator::operator!=() independently.
      ArrayHopperIterator<T> end()
      {
         return begin();
      }

      virtual ~ArrayHopper ()
      {
      }

};

//! Pointer to an array element with some magic wrapped around it.
template <class T>
class ArrayHopperIterator
{
   private:
      const ArrayHopper<T>& mMaster;   //< Pointer to the ArrayHopper that contains the Jumps.
      UIVec                 mCounter;  //< Keeping track of how much we've advanced in each dimension.
      T*                    mCurrent;  //< Element we are pointing to.
      unsigned int          mLastTick; //< To query which dimension ticked last, might be needed to nest loops.

   public:
      //! Construct for a ArrayHopper, will point at aMaster.mFirst.
      ArrayHopperIterator(const ArrayHopper<T>& aMaster):
         mMaster(aMaster)
        ,mCounter{UIVec(aMaster.mJumpIndices.size(), 0)}
        ,mCurrent{aMaster.mFirst}
//        ,mLastTick{aMaster.mJumpIndices[0]}
        ,mLastTick(0)
      {
      }

      //! Move pointer to next element.
      ArrayHopperIterator& operator++()
      {
         // Attempt to increase all hopping dimensions.
         for(mLastTick=0;mLastTick<mMaster.mJumpIndices.size();++mLastTick)
         {
            if(mCounter[mLastTick]+1 < mMaster.mSubDims[mMaster.mJumpIndices[mLastTick]])
            {
               mCounter[mLastTick]++;
               for(unsigned int m=0;m<mLastTick;++m)
               {
                  mCounter[m]=0;
               }
               // Increase mCurrent...
               mCurrent+=mMaster.mJumps[mLastTick];
               return *this;
            };
         }
         // The last element was visited. Set mCurrent to null to trigger operator!=()
         // returning true (ends the loop).
         mCurrent=nullptr;
         return *this;
      }

      // Only allowed along the first hopping dimension
      ArrayHopperIterator& operator+=(unsigned int count)
      {
         if(mCounter[0]+count < mMaster.mSubDims[mMaster.mJumpIndices[0]])
         {
            mCounter[0]+=count;
            mCurrent+= count * mMaster.mJumps[0];
            return *this;
         }
         else
         {
            mCurrent=nullptr;
            return *this;
         }
      }

      unsigned int WhoTickedLast()
      {
         return mMaster.mJumpIndices[mLastTick];
      }

      T& operator*() const
      {
         return *mCurrent;
      }

      // This will make the iterator not stop until the very end! Not possible to stop
      // except by hand
      bool operator!=(const ArrayHopperIterator& other)
      {
          return mCurrent!=nullptr;
      }

      bool operator<(const ArrayHopperIterator& other)
      {
         return *this!=other;
      }

      std::string ShowCounter()
      {
         std::stringstream outstream;
         outstream << "(";
         for(auto i: mCounter)
         {
            outstream << i << " ";
         }
         outstream << ")";
         return outstream.str();
      }
};
#endif /* ARRAYHOPPER_H_INCLUDED */
