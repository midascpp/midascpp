#include "EnergyDenominatorTensor.h"

std::vector<unsigned int> edt_dims
   (  const unsigned int order
   ,  const unsigned int nvir
   ,  const unsigned int nocc
   )
{
   assert(order > 0);
   std::vector<unsigned int> result;
   for(unsigned int i=0;i<order;++i)
   {
      result.push_back(nvir);
      result.push_back(nocc);
   }
   return result;
}

std::vector<unsigned int> edt_dims
   (  const std::vector<In>& aModes
   ,  const std::vector<In>& aNmodals
   )
{
   std::vector<unsigned int> result;
   result.reserve(aModes.size());

   // Loop over modes in the given ModeCombi. Push the number of virtual modals to result.
   for(const auto& mode : aModes)
   {
      result.emplace_back(aNmodals.at(mode)-I_1);
   }

   return result;
}

#ifndef DISABLE_PRECOMPILED_TEMPLATES
// define
#define INSTANTIATE_ENERGYDENOMINATORTENSOR(T) \
template class EnergyDenominatorTensor<T>; \
template class ExactEnergyDenominatorTensor<T>;

INSTANTIATE_ENERGYDENOMINATORTENSOR(float);
INSTANTIATE_ENERGYDENOMINATORTENSOR(double);
//INSTANTIATE_ENERGYDENOMINATORTENSOR(std::complex<float>);
//INSTANTIATE_ENERGYDENOMINATORTENSOR(std::complex<double>);

#undef INSTANTIATE_ENERGYDENOMINATORTENSOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
