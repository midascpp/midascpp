/**
************************************************************************
* 
* @file    C2tPreprocessor.h
*
* @date    25-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Preprocessor using the C2T/T2C rank-reduction algoritm.
*     Algorithm is based on G_BTA and C_BTA algorithms presented in the following article:
*
*        Multigrid Accelerated Tensor Approximation of Function Related Multidimensional Arrays
*    
*        B. N. Khoromskij and V. Khoromskaia
*    
*     SIAM J. Sci. Comput., 31(4), 3002–3026. (25 pages)
*     DOI:10.1137/080730408
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef C2TPREPROCESSOR_H_INCLUDED
#define C2TPREPROCESSOR_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"

#include <vector>
#include <memory>

namespace midas::tensor
{

/**
 * C2T preprocessor class.
 **/
class C2tPreprocessor
   :  public detail::ConcreteCpPreprocessorBase
{
   private:
      //! U matrices
      std::vector<std::unique_ptr<Nb[]>> mU;

      //! Dimensions of original tensor
      std::vector<unsigned> mOriginalExtents;

      //! Threshold for truncating SVD
      Nb mSvdThreshold;

      //! Use GESVD instead of GESDD to be safe
      bool mSafeSvd;

      //! Rank reduction on CanonicalTensor
      bool PreprocessCanonical
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         );

      //! Rank reduction on TensorDirectProduct
      bool PreprocessDirProd
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         );

      //! Rank reduction on TensorSum
      bool PreprocessSum
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         );

      //! Perform SVDs on TensorDirectProduct
      CanonicalTensor<Nb>* ProjectedTensorCanonicalPtr
         (  const CanonicalTensor<Nb>*
         ,  std::vector<std::unique_ptr<Nb[]>>&
         ,  bool&
         );

      //! Preprocess the tensor
      bool PreprocessImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         )  override;

      //! Preprocess the tensor and guess
      bool PreprocessImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         )  override;

      //! Postprocess the tensor after decomposition
      bool PostprocessImpl
         (  NiceTensor<Nb>&
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! ModifiesTargetNorm
      bool ModifiesTargetNormImpl
         (
         )  const override;

   public:
      //! Constructor from TensorDecompInfo
      C2tPreprocessor
         (  const TensorDecompInfo&
         );
};

} /* namespace midas::tensor */

#endif /* C2TPREPROCESSOR_H_INCLUDED */
