#ifndef ZEROTENSOR_H_INCLUDED
#define ZEROTENSOR_H_INCLUDED

///> include declaration
#include "tensor/ZeroTensor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
///> include implementation
#include "tensor/ZeroTensor_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* ZEROTENSOR_H_INCLUDED */
