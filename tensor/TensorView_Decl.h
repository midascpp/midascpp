#ifndef TENSORVIEW_DECL_H_INCLUDED
#define TENSORVIEW_DECL_H_INCLUDED

#include<vector>
#include<utility>
#include<stdexcept>
#include<cassert>

#include"BaseTensor.h"
#include"MultiIndex.h"

template
   <  class T
   >
class TensorView
   :  public BaseTensor<T>
{
   private:
      const BaseTensor<T>&            mTensor;
            std::vector<unsigned int> mOrder; // mOrder[i] -> in which position the i-th
                                              // index of the underlying tensor will be requested

   public:
      TensorView(const TensorView& other):
         BaseTensor<T>(other.mDims)
        ,mTensor      (other.mTensor)
        ,mOrder       (other.mOrder )
      {
      }

      TensorView(const BaseTensor<T>& aTensor,
                 const std::vector<indexpermutation_t>& aPermutations):
         BaseTensor<T>(aTensor.GetDims())
        ,mTensor      (aTensor)
      {
         for(int idim=0;idim<this->NDim();++idim)
         {
            mOrder.push_back(idim);
         }

         for(auto perm: aPermutations)
         {
            for(auto swap: perm)
            {
               std::swap(mOrder[swap.first], mOrder[swap.second]);
            }
         }
      }

      typename BaseTensor<T>::typeID Type() const override {return BaseTensor<T>::typeID::VIEW;};
      std::string ShowType() const override {return "TensorView";};

      std::vector<unsigned int> GetDims() const
      {
         multiindex_t newdims(this->NDim());
         unsigned int j=0;
         for(auto i: this->mOrder)
         {
            newdims[i]=this->mDims[j++];
         }
         return newdims;
      }

      template <class B>
      void SwapVector(std::vector<B>& input) const
      {
         std::vector<B> copy=input;

         unsigned int i=0;
         for(auto newpos:mOrder)
         {
            input[newpos]=copy[i++];
         }
      }

      const BaseTensor<T>* GetTensor() const
      {
         return &mTensor;
      }

      BaseTensor<T>* Clone() const override
      {
         return mTensor.Reorder(mOrder);
      }

   private:

      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indices_inner_this
         ,  const std::vector<unsigned int>& indices_outer_this
         ,  const BaseTensor<T>&             other
         ,  const std::vector<unsigned int>& indices_inner_other
         ,  const std::vector<unsigned int>& indices_outer_other
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indices_left_result
         ,  const std::vector<unsigned int>& indices_right_result
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override
      {
         MIDASERROR("Mmmmkey this function should have never been called.");
         return nullptr;
      }

      BaseTensor<T>* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const override
      {
         MIDASERROR("Mmmmkey this function should have never been called.");
         return nullptr;
      }

      BaseTensor<T>* Reorder_impl(const std::vector<unsigned int>& neworder,
                                  const std::vector<unsigned int>& newdims) const override
      {
         TensorView<T>* result(new TensorView<T>(*this));

         std::vector<unsigned int> newindices(this->NDim());
         {
            unsigned int j=0;
            for(auto i: this->mOrder)
               newindices[i]=j++;
         }
         {
            unsigned int j=0;
            for(auto i: neworder)
               result->mOrder[j++]=newindices[i];
         }

         return result;
      }

      BaseTensor<T>* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>& limits) const override
      {
         throw std::runtime_error("Mmmmkey this function should have never been called");
         return nullptr;
      }

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;

      BaseTensor<T>* ContractDown   (const unsigned int idx, const BaseTensor<T>* other) const override
      {
         return mTensor.ContractDown(mOrder[idx], other);
      }

      BaseTensor<T>* ContractForward(const unsigned int idx, const BaseTensor<T>* other) const override
      {
         return mTensor.ContractForward(mOrder[idx], other);
      }

      BaseTensor<T>* ContractUp     (const unsigned int idx, const BaseTensor<T>* other) const override
      {
         return mTensor.ContractUp(mOrder[idx], other);
      }

      TensorView* operator*=(const T k) override
      {
         throw std::runtime_error("TensorView cannot be modified");
         return nullptr;
      }

      TensorView* operator/=(const T k)
      {
         throw std::runtime_error("TensorView cannot be modified");
         return nullptr;
      }

      TensorView* operator*=(const BaseTensor<T>& other_in) override
      {
         throw std::runtime_error("TensorView cannot be modified");
         return nullptr;
      }

      TensorView* operator/=(const BaseTensor<T>& other_in) override
      {
         throw std::runtime_error("TensorView cannot be modified");
         return nullptr;
      }

      TensorView* operator+=(const BaseTensor<T>& other_in) override
      {
         throw std::runtime_error("TensorView cannot be modified");
         return nullptr;
      }

      BaseTensor<T>* operator+ (const BaseTensor<T>& other) const
      {
         return *(this->Clone()) + other;
      }

      std::string Prettify() const override
      {
         std::string result;
         
         BaseTensor<T>* temp(mTensor.Reorder(mOrder));
         result = temp->Prettify();
         delete temp;
         return result;
      }

      void DumpInto(T*) const override { /* not implemented yet*/ assert(false); }

      std::ostream& write_impl(std::ostream& output) const override
      {
         BaseTensor<T>* temp(mTensor.Reorder(mOrder));
         temp->Write(output);
         delete temp;
         return output;
      }
      

      dot_t Dot(const BaseTensor<T>* const) const override;
};
#endif /* TENSORVIEW_DECL_H_INCLUDED */
