#ifndef CONTRACTION_MATRIX_IMPL_H_INCLUDED
#define CONTRACTION_MATRIX_IMPL_H_INCLUDED

#include <iostream>

/**
 *
 **/
template<class T>
ContractionMatrix<T>::ContractionMatrix
   ( std::pair<unsigned, unsigned> aIndices
   , std::pair<unsigned, unsigned> aExtents
   )
   : mIndices{std::move(aIndices)}
   , mExtents{std::move(aExtents)}
   , mSize{mExtents.first * mExtents.second}
   , mData{new T[mSize]}
{
}

/** 
 * Get the indices of the ContractionMatrix.
 * @return             The indices.
 **/
template<class T>
const std::pair<unsigned, unsigned>& ContractionMatrix<T>::Indices
   (
   ) const
{
   return mIndices;
}

/** 
 * Get the extents of the ContractionMatrix.
 * @return             The extents.
 **/
template<class T>
const std::pair<unsigned, unsigned>& ContractionMatrix<T>::Extents
   (
   ) const
{
   return mExtents;
}

/**
 * Get the size of the matrix.
 * @return          The size.
 **/
template<class T>
unsigned ContractionMatrix<T>::Size
   (
   ) const
{
   return mSize;
}

/**
 * Get the internal data pointer for manipualtion.
 * @warning
 *    Use with caution.
 **/
template<class T>
T* ContractionMatrix<T>::Get
   (
   ) const
{
   return mData.get();
}

/**
 * Get element of matrix
 * @param aRows         Row index
 * @param aCols         Column index
 * @return              The actual element
 **/
template<class T>
T ContractionMatrix<T>::GetElem
   ( unsigned aRows
   , unsigned aCols
   ) const
{
   return *(Get() + aRows + (aCols * Extents().first));
}

/**
 * Overload for operator<< for ContractionMatrix class.
 **/
template<class T>
std::ostream& operator<<(std::ostream& os, const ContractionMatrix<T>& mat)
{
   os << " Contracion matrix "
      << "    idx: (" << mat.mIndices.first << ", " << mat.mIndices.second << ")"
      << "    ext: (" << mat.mExtents.first << ", " << mat.mExtents.second << ")\n";
   for(int i = 0; i < mat.mExtents.first; ++i)
   {
      for(int j = 0; j < mat.mExtents.second; ++j)
      {
         auto index = j*mat.mExtents.first + i;
         os << mat.mData[index] << " ";
      }
      os << "\n";
   }
   return os;
}

#endif /* CONTRACTION_MATRIX_IMPL_H_INCLUDED */
