/**
 ************************************************************************
 * 
 * @file                Scalar_Decl.h
 *
 * Created:             03.02.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * Short Description:   Class for scalars inheriting from BaseTensor
 * 
 * Detailed Description: Contracting a tensor completely ("dot product")
 *                       yields a scalar
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef SCALAR_DECL_H_INCLUDED
#define SCALAR_DECL_H_INCLUDED

// std headers
#include<vector>
#include<typeinfo>
#include<sstream>

#include "tensor/BaseTensor.h"
#include "util/read_write_binary.h"
#include "util/Io.h"
#include "util/Math.h"
#include "util/SanityCheck.h"

template <class T>
class Scalar
   : public BaseTensor<T>
{
   private:
      T mValue;
      bool mEvaluateUpToCanonical = false;

   public:     
      using dot_t = typename DotEvaluator<T>::dot_t;

      ///>
      Scalar
         ( T aValue
         , bool aEvaluateUpToCanonical = false
         )
         : BaseTensor<T>{std::vector<unsigned int>{}}
         , mValue{aValue}
         , mEvaluateUpToCanonical{aEvaluateUpToCanonical}
      {
      }
      
      ///>
      Scalar():
         Scalar{static_cast<T>(0.0)}
      {
      }
      
      ///>
      Scalar(const Scalar& other) = default;
      
      ///>
      Scalar(Scalar&& other) = default;
      
      ///>
      Scalar(std::istream& input):
         Scalar()
      {
         // This should read a 0 and leave mDims empty
         this->ReadDims(input);

         read_binary(mValue, input);
      }
      
      ///>
      typename BaseTensor<T>::typeID Type() const override
      {
         return BaseTensor<T>::typeID::SCALAR;
      };
      
      ///>
      std::string ShowType() const override
      {
         return "Scalar";
      };
      
      ///>
      ~Scalar()
      {
      }
         
      ///>
      Scalar* Clone() const override
      {
         return new Scalar(*this);
      }
      
      ///>
      T GetValue() const
      {
         return mValue;
      }

      //! Set value
      void SetValue
         (  const T& aVal
         )
      {
         this->mValue = aVal;
      }
      
      //!
      void SetEvaluateUpToCanonical
         (  bool aCp
         )
      {
         this->mEvaluateUpToCanonical = aCp;
      }

      //!
      bool SanityCheck() const override
      {
         return midas::util::IsSane(this->mValue);
      }

   private:    
      ///>
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indices_inner_this
         ,  const std::vector<unsigned int>& indices_outer_this
         ,  const BaseTensor<T>&            other
         ,  const std::vector<unsigned int>& indices_inner_other
         ,  const std::vector<unsigned int>& indices_outer_other
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indices_left_result
         ,  const std::vector<unsigned int>& indices_right_result
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override
      {
         if constexpr   (  midas::type_traits::IsComplexV<T>
                        )
         {
            auto left_val = conjugate_left ? midas::math::Conj(mValue) : mValue;
            auto right = other.Clone();

            if (  conjugate_right
               )
            {
               right->Conjugate();
            }

            return left_val * *right;
         }
         else
         {
            return this->mValue * other;
         }
      }
      
      ///>
      BaseTensor<T>* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const override
      {
         MIDASERROR("Contracting internally a scalar doesn't make much sense, does it?");
         return nullptr;
      }
      
      ///>
      Scalar* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>& limits) const override
      {
         MIDASERROR("Slicing a scalar doesn't make much sense, does it?");
         return nullptr;
      }

      //!
      void ConjugateImpl
         (
         )  override
      {
         this->mValue = midas::math::Conj(mValue);
      }

   public:
      
      ///>
      BaseTensor<T>* ContractDown(const unsigned int idx, const BaseTensor<T>* other) const override
      {
         MIDASERROR("Contracting down a scalar doesn't make much sense, does it?");
         return nullptr;
      }
      
      ///>
      Scalar* ContractForward(const unsigned int idx, const BaseTensor<T>* other) const override
      {
         MIDASERROR("Contracting forward a scalar doesn't make much sense, does it?");
         return nullptr;
      }
      
      ///>
      BaseTensor<T>* ContractUp     (const unsigned int idx, const BaseTensor<T>* other) const override;
      
      ///>
      Scalar* operator*=(const T k) override
      {
         this->mValue*=k;
         return this;
      }
      ///>
      Scalar* operator/=(const T k)
      {
         this->mValue/=k;
         return this;
      }
      //
      Scalar* operator*=(const BaseTensor<T>& other_in) override
      {
         try
         {
            const Scalar<T>& other=dynamic_cast<const Scalar<T>&>(other_in);
            *this*=other.mValue;
            return this;
         }
         catch(const std::bad_cast&)
         {
            MIDASERROR("ERROR: Trying to multiply tensors of different classes");
         }
         return nullptr;
      }

      Scalar* operator/=(const BaseTensor<T>& other_in) override
      {
         try
         {
            const Scalar<T>& other=dynamic_cast<const Scalar<T>&>(other_in);
            *this/=other.mValue;
            return this;
         }
         catch(const std::bad_cast&)
         {
            throw std::runtime_error("ERROR: Trying to element-wise divide tensors of different classes");
         }
         return nullptr;
      }

      Scalar* operator+=(const BaseTensor<T>& other_in) override
      {
         try
         {
            const Scalar<T>& other=dynamic_cast<const Scalar<T>&>(other_in);
            this->mValue+=other.mValue;
            return this;
         }
         catch(const std::bad_cast&)
         {
            MIDASERROR("ERROR: Trying to add tensors of different classes: <"
                     +this->ShowType()+"> and <"+other_in.ShowType()+">");
         }
         return nullptr;
      }

      ///>
      Scalar* Reorder_impl(const std::vector<unsigned int>&,
                           const std::vector<unsigned int>&) const override
      {
         return new Scalar(*this);
      }
      
      ///>
      std::string Prettify() const override
      {
         std::stringstream out;
         out   << std::setprecision(7) << std::scientific << mValue;
         return out.str();
      }
      
      ///>
      std::ostream& write_impl(std::ostream& output) const override
      {
         return write_binary(mValue, output);
      }
      
      ///> Dump the Scalar into a ptr
      void DumpInto(T* aPtr) const override
      {
         if(!aPtr) MIDASERROR("Pointer is NULL");
         *aPtr = mValue;
      }

      ///> Axpy with another Scalar and a "regular" scalar.
      void Axpy(const BaseTensor<T>&, const T&) override;

      ///> Zero the Scalar
      void Zero() override
      {
         mValue = static_cast<T>(0.0);
      }
      
      ///> Scale the Scalar
      void Scale(const T& aScalar) override
      {
         mValue *= aScalar;
      }

      //! Absolute value
      void Abs() override
      {
         mValue = std::abs(mValue);
      }

      ///> 
      void ElementwiseScalarAddition(const T& aScalar) override
      {
         mValue += aScalar;
      }

      dot_t Dot(const BaseTensor<T>* const) const override;

   /*============== MPI  ==============*/
#ifdef VAR_MPI
      void MpiSend(In aRecievingRank) const override;
      void MpiRecv(In aSendingRank) override;
      void MpiBcast(In aRoot) override;
#endif /* VAR_MPI */
};


#endif /* SCALAR_DECL_H_INCLUDED */
