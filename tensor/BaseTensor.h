#ifndef BASETENSOR_H_INCLUDED
#define BASETENSOR_H_INCLUDED

// Include forward declaration of all tensors in heirarchy
#include "tensor/ForwardDecl.h"
#include "DotEvaluator.h"

// Include declaration
#include "BaseTensor_Decl.h"

//// CAN THESE BE REMOVED ?????
//#include "SimpleTensor.h"
//#include "CanonicalTensor.h"
//#include "TensorDirectProduct.h"
//#include "TensorSum.h"
//#include "ZeroTensor.h"
//#include "CanonicalIntermediate.h"
//#include "Scalar.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "BaseTensor_Impl.h"
#include "DotEvaluator_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* BASETENSOR_H_INCLUDED */
