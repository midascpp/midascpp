/**
************************************************************************
* 
* @file    FindBestCpDriver.h
*
* @date    23-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Driver for fitting a tensor to CP format with a given accuracy.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef FINDBESTCPDRIVER_H_INCLUDED
#define FINDBESTCPDRIVER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/FitReport.h"

namespace midas
{
namespace tensor
{

/**
 * Driver class for fixed-accuracy CP decomposition.
 **/
class FindBestCpDriver
   :  public detail::TensorDecompDriverBase
{
   private:
      //! Max rank
      In mFindBestCpMaxRank;

      //! Rank incr
      In mFindBestCpRankIncr;

      //! Starting rank
      In mStartingRank;

      //! Scale rank incr with tensor order (starting from 3rd-order tensors)
      bool mFindBestCpScaleRankIncr;

      //! Adapt rank incr to tensor order and smallest dimension
      bool mFindBestCpAdaptiveRankIncr;

      //! Requested accuracy of decomposed tensor
      Nb mFindBestCpResThreshold;

      //! Maximum number of times the error is allowed to increase without finding a new minimum
      In mMaxErrorIncr;

      //! Print FitReport after decomposition
      bool mPrintFitReport;


      //! Check decomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Decompose
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Decompose with argument guess
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Check if tensor should be fitted to rank-0
      bool ScreenImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Fit CP tensor to given accuracy
      FitReport<Nb> FitCpTensor
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  In
         ,  In
         ,  In
         ,  Nb
         )  const;

      //! Get threshold
      Nb GetThreshold
         (
         )  const override;

   public:
      //! Constructor from TensorDecompInfo
      FindBestCpDriver
         (  const TensorDecompInfo&
         );
};

} /* namespace tensor */
} /* namespace midas */

#endif /* FINDBESTCPDRIVER_H_INCLUDED */
