#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "TensorSum.h"
#include "TensorSum_Impl.h"

// define
#define INSTANTIATE_TENSORSUM(T) \
template class TensorSum<T>;

// concrete instantiations
INSTANTIATE_TENSORSUM(float)
INSTANTIATE_TENSORSUM(double)
INSTANTIATE_TENSORSUM(std::complex<float>)
INSTANTIATE_TENSORSUM(std::complex<double>)

#undef INSTANTIATE_TENSORSUM

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
