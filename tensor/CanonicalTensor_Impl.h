#include<sstream>
#include<algorithm>
#include<iomanip>
#include<utility>
#include<cmath>
#include<numeric>
#include<cstring>
#include<cassert>

#include "SimpleTensor.h"
#include "ZeroTensor.h"
#include "Scalar.h"
#include "TensorDirectProduct.h"
#include "CanonicalIntermediate.h"
#include "lapack_interface/math_wrappers.h"
#include "util/Io.h"
#include "util/CallStatisticsHandler.h"
#include "util/SanityCheck.h"
#include "util/RandomNumberGenerator.h"

#include "util/read_write_binary.h"
#include "libmda/numeric/signum.h"
#include "print_unique_ptr.h"
#include "tensor/FitALS.h"

#include "mpi/Impi.h"

/**
 * Allocate modematrices for canonical tensor.
 **/
template<class T>
void CanonicalTensor<T>::Allocate
   (
   )
{ 
   // Allocate T** 
   auto order = this->NDim();
   mModeMatrices = Base::Allocator_Tpp::Allocate(order);
   
   // Allocate for each order a T*
   const auto& extents = this->GetDims();
   for(unsigned int idim = 0; idim < order; ++idim)
   {
      auto size = mRank * extents[idim];
      mModeMatrices[idim] = Base::Allocator_Tp::Allocate(size);
   }
}

/**
 *
 **/
template<class T>
void CanonicalTensor<T>::Deallocate
   (
   )
{
   if(mModeMatrices)
   {
      auto order = this->NDim();
      const auto& extents = this->GetDims();
      for(unsigned int idim = 0; idim < order; ++idim)
      {
         if(mModeMatrices[idim])
         {
            auto size = mRank * extents[idim];
            Base::Allocator_Tp::Deallocate(mModeMatrices[idim], size);
            mModeMatrices[idim] = nullptr;
         }
      }
      Base::Allocator_Tpp::Deallocate(mModeMatrices, order);
      mModeMatrices = nullptr;
   }
}

/**
 * Set new rank of CanonicalTensor (can be called by EnergyDenominatorTensor and EnergyDifferencesTensor)
 *
 * @param aRank      New rank
 *
 **/
template <class T>
void CanonicalTensor<T>::SetNewRank
   (  unsigned aRank
   )
{
   if (  this->mRank != aRank
      )
   {
      this->Deallocate();
      this->mRank = aRank;
      this->Allocate();
   }
}

/// Default constructor (no elements)
template <class T>
CanonicalTensor<T>::CanonicalTensor
   (
   )
   : BaseTensor<T>()
   , mRank{0}
   , mModeMatrices{nullptr}
{
};

/// Explicit constructor. Appropriates \p aModeMatrices.
template <class T>
CanonicalTensor<T>::CanonicalTensor
   ( const std::vector<unsigned int>& aDims
   , const unsigned int aRank
   , T **aModeMatrices
   )
   : BaseTensor<T>{aDims}
   , mRank        {aRank}
   , mModeMatrices{aModeMatrices}
{
}

/// Allocate new constructor, set to 0.
template <class T>
CanonicalTensor<T>::CanonicalTensor
   ( const std::vector<unsigned int>& aDims 
   , const unsigned int               aRank 
   )
   :  BaseTensor<T>{aDims}
   ,  mRank        {aRank}
   ,  mModeMatrices{nullptr}
{
   Allocate();

   for(unsigned int idim=0; idim<this->NDim(); ++idim)
   {
      auto size = mRank*this->mDims[idim];
      for(size_t i = 0; i < size ; i++)
      {
         mModeMatrices[idim][i] = static_cast<T>(0.0);
      }
   }
};

/// Read new tensor from binary istream.
/*! The format is
 *     mDims (see BaseTensor::ReadDims())
 *     mRank
 *     mModeMatrices[0]
 *     mModeMatrices[1]
 *     ...
 */
template<class T>
CanonicalTensor<T>::CanonicalTensor(std::istream& input):
   CanonicalTensor()
{
   this->ReadDims(input);

   read_binary(mRank, input);
   
   Allocate();
   for(unsigned int idim=0;idim<this->NDim();++idim)
   {
      size_t blocksize=this->mDims[idim]*mRank;
      read_binary_array( mModeMatrices[idim], blocksize, input );
   }
}

/**
 * Constructor from SVD truncated at given threshold
 *
 * @param arDims
 *    Dimensions.
 * @param arSVD
 *    The SVD struct containing SVD of the matrix
 * @param aRank
 *    The rank of the result
 **/
template <class T>
CanonicalTensor<T>::CanonicalTensor
   (  const std::vector<unsigned int>& arDims
   ,  const SVD_struct<T>& arSVD
   ,  unsigned aRank
   )
   :  CanonicalTensor<T>( arDims, aRank )
{
   if (  aRank > 0  )
   {
      auto ndim = this->NDim();

      real_t power = static_cast<real_t>(1.) / ndim;

      assert( ndim >= 2 );

      if (  ndim > 2
         && std::count_if(arDims.begin(), arDims.end(), [](unsigned i) { return (i != 1); }) != 2
         )
      {
         MIDASERROR("Wrong dimensions for SVD of CP tensor!");
      }

      // Find indices with extents != 1. Set remaining mode matrices to one.
      std::vector<unsigned> idx_pos;
      idx_pos.reserve(2);
      for(unsigned idim=0; idim<ndim; ++idim)
      {
         if (  this->Extent(idim) != 1
            )
         {
            idx_pos.emplace_back(idim);
         }
         else
         {
            T* ptr = this->mModeMatrices[idim];
            for(int i=0; i<this->mRank; ++i)
            {
               *(ptr++) = std::pow( arSVD.s[i], power );
            }
         }
      }

      // Load in mode matrices.
      // Put U in the first and V in the second.
      unsigned idx0 = idx_pos[0];
      unsigned idx1 = idx_pos[1];
      auto vt_stride = arSVD.Min();

      T* u_in = arSVD.u.get();
      T* mm_one = this->mModeMatrices[idx0];
      T* mm_two = this->mModeMatrices[idx1];
      for(int irank=0; irank<this->mRank; ++irank)
      {
         real_t coeff = std::pow( arSVD.s[irank], power );

         // Load in U as first mode matrix
         for(int j=0; j<this->mDims[idx0]; ++j)
         {
            *(mm_one++) = *(u_in++) * coeff;
         }

         // Load in V as second mode matrix.
         // We have V^T in as N x N matrix, where N >= mRank.
         T* vt_in = arSVD.vt.get() + irank;
         for(int i=0; i<this->mDims[idx1]; ++i)
         {
            *(mm_two++) = *vt_in * coeff;
            vt_in += vt_stride;
         }
      }
   }
}

/// Dump in binary into ostream.
/*! See CanonicalTensor(std::istream&) for description of format.
 */
template<class T>
std::ostream& CanonicalTensor<T>::write_impl(std::ostream& output) const
{
   write_binary(mRank, output);
   for(unsigned int idim=0;idim<this->NDim();++idim)
   {
      size_t blocksize=this->mDims[idim]*mRank;
      output.write( reinterpret_cast<char*>(mModeMatrices[idim]), blocksize*sizeof(T) );
   }
   return output;
}

/**
 *
 **/
template<class T>
CanonicalTensor<T>::CanonicalTensor
   ( const std::vector<unsigned int>& arDims
   , const std::vector<unsigned int>& arIndex
   )
   : CanonicalTensor(arDims, I_1)
{
   // Dimensions have been set, rank is 1, and all elements are 0.
   // Now assert that index matches dimensions.
   // Also modify mode matrices to make the tensor a unit tensor.
   MidasAssert(arDims.size() == arIndex.size(), "Unequal numbers of dimensions and indices.");
   for(In i = I_0; i < arDims.size(); ++i)
   {
      // Assertion.
      MidasAssert((I_0 <= arIndex[i]) && (arIndex[i] < arDims[i]), "Index doesn't match dimensions.");

      // Modification.
      mModeMatrices[i][arIndex[i]] = C_1;
   }
}

/**
 * Copy constructor
 **/
template <class T>
CanonicalTensor<T>::CanonicalTensor
   ( const CanonicalTensor<T>& other
   )
   : CanonicalTensor(other.mDims, other.mRank)
{
   for(unsigned int idim = 0; idim < this->NDim(); ++idim)
   {
      for(size_t i = 0; i<this->mRank*this->mDims[idim]; ++i)
      {
         mModeMatrices[idim][i] = other.mModeMatrices[idim][i];
      }
   }
};

/**
 * Move constructor
 **/
template <class T>
CanonicalTensor<T>::CanonicalTensor
   ( CanonicalTensor<T>&& other
   )
   : BaseTensor<T>{std::move(other.mDims)}
   , mRank        {0}
   , mModeMatrices{nullptr}
{
   std::swap(mRank, other.mRank);
   std::swap(mModeMatrices, other.mModeMatrices);
};

/**
 * Destructor
 **/
template <class T>
CanonicalTensor<T>::~CanonicalTensor
   (
   )
{
   Deallocate();
}

/**
 * @result             A pointer to a copy of this object
 **/
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::Clone
   (
   ) const
{
   return new CanonicalTensor<T>(*this);
}

/// Copy assignment
template <class T>
CanonicalTensor<T>& CanonicalTensor<T>::operator=
   ( const CanonicalTensor<T>& other
   )
{
   // Only deallocate if we have a mismatch
   if (  this->mDims != other.mDims
      || this->mRank != other.mRank
      || !this->mModeMatrices
      )
   {
      Deallocate();
      this->mDims = other.mDims;
      this->mRank = other.mRank;
      Allocate();
   }
   
   unsigned ndim = this->NDim();

   for(unsigned idim=0; idim<ndim; ++idim)
   {
      auto size = this->mRank*this->mDims[idim];
      for(size_t i = 0 ; i < size; ++i)
      {
         mModeMatrices[idim][i] = other.mModeMatrices[idim][i];
      }
   }
   return *this;
}

/**
 * Move assignment
 **/
template <class T>
CanonicalTensor<T>& CanonicalTensor<T>::operator=(CanonicalTensor<T>&& other)
{
   std::swap(this->mDims,   other.mDims);
   std::swap(mRank,         other.mRank);
   std::swap(mModeMatrices, other.mModeMatrices);
   return *this;
}

/**
 *
 **/
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::Reorder_impl(const std::vector<unsigned int>& neworder,
                                                     const std::vector<unsigned int>& newdims) const
{
   CanonicalTensor<T>* result(new CanonicalTensor<T>(newdims, this->mRank));

   unsigned int idim_in=0;
   for(auto idim_out: neworder)
   {
      T* ptr_in= this  ->mModeMatrices[idim_in];
      T* ptr_out=result->mModeMatrices[idim_out];
      for(size_t i=0;i<this->mRank*this->mDims[idim_in];i++)
      {
         *(ptr_out++) = *(ptr_in++);
      }
      ++idim_in;
   }
   return result;
}

/**
 * Dump CanonicalTensor into T pointer. 
 * @warning 
 *    NOT the most efficient implementation.
 *
 * @param ptr 
 *    Pointer to dump result into (must be pre-allocated)
 **/
template<class T>
void CanonicalTensor<T>::DumpInto
   (  T* ptr
   )  const
{
   auto totsize = this->TotalSize();
   auto ndim = this->NDim();
   const auto& dims = this->GetDims();

   // Special case for ndim = 1
   if (  ndim == 1
      )
   {
      for(unsigned ielem=0; ielem < totsize; ++ielem)
      {
         ptr[ielem] = static_cast<T>(0.);
         for(unsigned irank=0; irank<mRank; ++irank)
         {
            ptr[ielem] += mModeMatrices[0][ielem + irank*totsize];
         }
      }
   }
   else
   {
      std::vector<unsigned> indices(ndim);
      for(unsigned i = 0; i < indices.size(); ++i)
      {
         indices[i] = 0;
      }
      
      for(unsigned ielem = 0; ielem < totsize; ++ielem)
      {
         ptr[ielem] = static_cast<T>(0.0);
         for(unsigned irank = 0; irank < mRank; ++irank)
         {
            auto prod_elem = static_cast<T>(1.0);
            for(unsigned idim = 0; idim < ndim; ++idim)
            {
               prod_elem *= mModeMatrices[idim][indices[idim] + irank*dims[idim]];
            }
            ptr[ielem] += prod_elem;
         }
         
         for (int i = indices.size() - 1; i >= 0; --i)
         {
            if (++indices[i] == dims[i])
            {
               indices[i] = 0;
            }
            else
            {
               break;
            }
         }
      }
   }
}

/**
 *
 **/
template <class T>
T** CanonicalTensor<T>::GetModeMatrices
   (
   ) const
{
   return mModeMatrices;
}

/**
 *
 **/
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::Slice_impl
   ( const std::vector<std::pair<unsigned int, unsigned int> >& limits
   ) const
{
   std::vector<unsigned int> newdims;
   for(auto l: limits)
   {
      newdims.push_back(l.second-l.first);
   }

   CanonicalTensor<T>* result(new CanonicalTensor<T>( newdims, this->mRank ));

   for(unsigned int idim=0;idim<this->NDim();++idim)
   {
      T* ptr_out =result->mModeMatrices[idim];
      T* ptr_in=this  ->mModeMatrices[idim] + limits[idim].first;
      for(unsigned int irank=0;irank<this->mRank;++irank)
      {
         for(unsigned int i=limits[idim].first;i<limits[idim].second;++i)
         {
            *(ptr_out++)=*(ptr_in++);
         }
         ptr_in+=this->mDims[idim]-newdims[idim];
      }
   }
   return result;
}

/**
 *
 **/
template
   <  class T
   >
std::vector<T> CanonicalTensor<T>::PartialContract
   (  const BaseTensor<T>* const aOther
   ,  const std::vector<unsigned>& aIndices
   )  const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      MIDASERROR("This has not been adapted to complex numbers yet!");
      return std::vector<T>();
   }
   else
   {
      std::vector<T> result(this->GetRank());

      switch(aOther->Type())
      {
         case BaseTensor<T>::typeID::SIMPLE:
         {
            MIDASERROR("Partial contract not implemted for simpletensor");
            break;
         }
         case BaseTensor<T>::typeID::CANONICAL:
         {
            auto ptr = static_cast<const CanonicalTensor<T>* const>(aOther);
            PartialGammaIntermediates<T> gamma_intermed(this, ptr, aIndices);
            auto gamma = Base::Allocator_Tp::AllocateUniqueArray(this->GetRank() * ptr->GetRank());
            gamma_intermed.FullGammaMatrix(gamma);
            auto gamma_ptr = gamma.get();
            for(int rtilde = 0; rtilde < this->GetRank(); ++rtilde)
            {
               result[rtilde] = static_cast<T>(0.0);
            }
            for(int rmark = 0; rmark < ptr->GetRank(); ++rmark)
            {
               for(int rtilde = 0; rtilde < this->GetRank(); ++rtilde)
               {
                  result[rtilde] += *(gamma_ptr++);
               }
            }

            break;
         }
         default:
         {
            MIDASERROR("Partial contract not implemented");
         }
      }
      return result;
   }
}

/**
 *
 **/
template<class T>
template<class U>
typename CanonicalTensor<T>::Base::UniqueArray CanonicalTensor<T>::PartialSqueeze
   (  const BaseTensor<T>* const aOther
   ,  const std::vector<unsigned>& aIndices
   ,  const int idim
   ,  bool transposed
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
//   LOGCALL("calls");
   auto result = Base::Allocator_Tp::AllocateUniqueArray(this->GetDims()[idim] * this->GetRank());
   switch(aOther->Type())
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         MIDASERROR("PartialSqueeze not implemented for SimpleTensor");
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         auto ptr = static_cast<const CanonicalTensor<T>* const>(aOther);
         int reverse_idx = -1;
         for(int i = 0; i < aIndices.size(); ++i)
         {
            if(aIndices[i] == idim)
            {
               reverse_idx = i;
               break;
            }
         }
         if(reverse_idx == -1) MIDASERROR("could not find reverse index");

         auto mixed_gamma = Base::Allocator_Tp::AllocateUniqueArray(this->GetRank() * ptr->GetRank());
         PartialGammaIntermediates<T> gamma_intermed(this, ptr, aIndices);
         gamma_intermed.GammaMatrix(idim, mixed_gamma);
         
         if(transposed)
         {
            char nc = 'N';
            char nt = 'T';
            T alpha = static_cast<T>(1.0);
            T beta  = static_cast<T>(0.0);
            int m = mRank;
            int n = this->mDims[idim];
            int k = ptr->GetRank();
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                         , mixed_gamma.get(), &lda
                                         , const_cast<T*>(ptr->mModeMatrices[reverse_idx]), &ldb
                                         , &beta, result.get(), &ldc
                                         );
         }
         else
         {
            char nc = 'N';
            char nt = 'T';
            T alpha = static_cast<T>(1.0);
            T beta  = static_cast<T>(0.0);
            int m = this->mDims[idim];
            int n = mRank;
            int k = ptr->GetRank();
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                         , const_cast<T*>(ptr->mModeMatrices[reverse_idx]), &lda
                                         , mixed_gamma.get(), &ldb
                                         , &beta, result.get(), &ldc
                                         );
         }
         
         break;
      }
      default:
      {
         MIDASERROR("Partial squeeze not implemented");
      }
   }
   return result;
}



/**
 *
 **/
template<class T>
T CanonicalTensor<T>::compute_condition_number
   (
   ) const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      MIDASERROR("This has not been adapted to complex numbers yet!");
      return T(0.);
   }
   else
   {
      T condition_number = 0;
      for(int i = 0; i < mRank; ++i)
      {
         T lambda_r = 1.0;
         for(int j = 0; j < this->NDim(); ++j)
         {
            T lambda_r_dim = 0;
            for(int k = 0; k < this->GetDims()[j]; ++k)
            {
               lambda_r_dim += mModeMatrices[j][k + i*this->GetDims()[j]] * mModeMatrices[j][k + i*this->GetDims()[j]];
            }
            lambda_r *= std::sqrt(lambda_r_dim);
         }
         condition_number += lambda_r * lambda_r;
      }
      return std::sqrt(condition_number);
   }
}

/**
 *
 **/
template<class T>
std::vector<std::vector<T> > CanonicalTensor<T>::compute_norm_matrix
   (
   ) const
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      MIDASERROR("This has not been adapted to complex numbers yet!");
      return std::vector<std::vector<T> >();
   }
   else
   {
      std::vector<std::vector<T> > mat(this->NDim()); 
      for(int i = 0; i < mat.size(); ++i)
      {
         mat[i].resize(this->mRank);
         for(int j = 0; j < mat[i].size(); ++j)
         {
            mat[i][j] = 0.0;
            for(int k = 0; k < this->GetDims()[i]; ++k)
            {
               mat[i][j] += 
                  mModeMatrices[i][k + j*this->GetDims()[i]] * mModeMatrices[i][k + j*this->GetDims()[i]];
            }
            mat[i][j] = std::sqrt(mat[i][j]);
         }
      }
      return mat;
   }
}

/**
 * Value of element given by indices.
 *
 * @param arIndices
 *    The indices
 * @return
 *    The value of the tensor at arIndices.
 **/
template <class T>
T CanonicalTensor<T>::Element
   (  const std::vector<unsigned>& arIndices
   )  const
{
   // Perform checks
   const auto& ndim = this->NDim();
   assert( ndim == arIndices.size() );

   const auto& dims = this->GetDims();
   for( size_t i=0; i<ndim; ++i )
   {
      assert( arIndices[i] <= dims[i] );
   }

   // Initialize result
   T result = static_cast<T>(0.);
   T tmp = static_cast<T>(1.);

   for( unsigned irank=0; irank<this->mRank; ++irank )
   {
      for( unsigned idim=0; idim<ndim; ++idim )
      {
         tmp *= this->mModeMatrices[idim][arIndices[idim]+irank*dims[idim]];
      }
      result += tmp;
      tmp = static_cast<T>(1.);
   }

   return result;
}

/**
 * Check that none of the mode-matrix elements are nan or inf.
 *
 * @return     True if something is wrong
 **/
template
   <  class T
   >
bool CanonicalTensor<T>::CheckModeMatrices
   (
   )  const
{
   auto ndim = this->NDim();
   for(unsigned idim=0; idim < ndim; ++idim)
   {
      auto size = this->Extent(idim)*this->mRank;
      for(unsigned ielem=0; ielem < size; ++ielem)
      {
         if (  !midas::util::IsSane(this->mModeMatrices[idim][ielem])
            )
         {
            return true;
         }
      }
   }

   return false;
}

/**
 * Sanity check
 *
 * @return True if the tensor is sane
 **/
template <class T>
bool CanonicalTensor<T>::SanityCheck() const
{
   return !this->CheckModeMatrices();
}

/// Print the Mode matrices
template <class T>
std::string CanonicalTensor<T>::Prettify() const
{
   std::stringstream out;

   out << this->NDim() << "   [ " ;
   if(this->NDim() > 0)
   {
      out << this->mDims[0];
      for (unsigned int i=1;i<this->NDim();i++)
      {
         out << ", " << this->mDims[i];
      }
   }
   out << " ]" << std::endl;
   out << "Rank: " << this->mRank << std::endl;

   if(this->mRank>0)
   {
      for(int idim=0;idim<this->NDim();++idim)
      {
         // Copy the mModeMatrix, SimpleTensor will destroy it...
         T* copy(new T[mRank * this->mDims[idim]]);
         for(size_t i=0;i<mRank * this->mDims[idim];++i)
         {
            copy[i]=mModeMatrices[idim][i];
         }

         out <<
             SimpleTensor<T>(std::vector<unsigned int> {mRank, this->mDims[idim]},
                             copy).Prettify();
         if(idim<this->NDim()-1)
         {
            out << std::endl;
         }
      }
   }

   return out.str();
}

//!
template <class T>
unsigned int CanonicalTensor<T>::GetRank() const
{
   return mRank;
}

template<class T>
typename BaseTensor<T>::real_t CanonicalTensor<T>::Norm2
   (
   ) const
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
   const auto size = mRank * mRank;
   real_t result(0.0);

   auto tot_size = this->TotalSize();
   // Simplify for vectors
   if (  this->NDim() == 1
      || this->MaxExtent() == tot_size
      )
   {
      auto dumped_tensor = Base::Allocator_Tp::AllocateUniqueArray(tot_size);
      this->DumpInto(dumped_tensor.get());

      for(size_t i = 0; i<tot_size; ++i)
      {
         result += std::real(midas::math::Conj(dumped_tensor[i])*dumped_tensor[i]);  // complex conj
      }

      // Check sanity of result
      if (  !midas::util::IsSane(result)
         || (  libmda::numeric::float_neg(result)
            && !libmda::numeric::float_numeq_zero(result, static_cast<real_t>(1.), 1000)
            )
         )
      {
         Mout  << " Bad tensor:\n" << *this << std::endl;
         MIDASERROR("Norm2 for 1st-order CanonicalTensor failed!!! result = " + std::to_string(result));
      }
   }
   else
   {
      // initialize result array to 1, so we can multiply into it
      auto result_array = Base::Allocator_Tp::AllocateUniqueArray(size);
      auto result_ptr = result_array.get();
      for(int i = 0; i < size; ++i)
      {
         *(result_ptr++) = static_cast<T>(1.0);
      }
      auto intermed_array = Base::Allocator_Tp::AllocateUniqueArray(size);
      
      // calculate result_array
      const auto ndim = this->NDim();
      const auto mode_matrices = mModeMatrices;
      for(int idim = 0; idim < ndim; ++idim)
      {
         int m = mRank;
         int n = mRank;
         int k = this->Extent(idim);
         int lda = std::max(1, k);
         int ldb = std::max(1, k);
         int ldc = std::max(1, m);
         char tc = is_complex ? 'C' : 'T';   // complex conj
         char nc = 'N';
         T alpha = static_cast<T>(1.0);
         T beta  = static_cast<T>(0.0);
         midas::lapack_interface::gemm( &tc, &nc, &m, &n, &k, &alpha
                                      , mode_matrices[idim], &lda
                                      , mode_matrices[idim], &ldb
                                      , &beta, intermed_array.get(), &ldc
                                      );
         
         result_ptr = result_array.get();
         auto intermed_ptr = intermed_array.get();
         for(int i = 0; i < size; ++i)
         {
            *(result_ptr++) *= *(intermed_ptr++);
         }
      }
      
      // collect result
      result_ptr = result_array.get();

      T aux_result(0.);

      for(int i = 0; i < size; ++i)
      {
         aux_result += *(result_ptr++);
      }

      if (  !libmda::numeric::float_numeq_zero(std::imag(aux_result), std::real(aux_result))
         )
      {
         Mout  <<  " CanonicalTensor::Norm2() = " << aux_result << std::endl;
         MIDASERROR("CanonicalTensor::Norm2 has non-zero imaginary part!");
      }

      result = std::real(aux_result);

      // Check sanity
      if (  !midas::util::IsSane(result)
         )
      {
         MidasWarning("Safe norm2");
         result = this->SafeNorm2();
      }
      if (  libmda::numeric::float_neg(result)
         )
      {
         if (  libmda::numeric::float_numeq_zero(result, static_cast<real_t>(1.), 1000)
            )
         {
            result = -result;
         }
         else
         {
            // sort result (abs values) to minimize rounding error
            MidasWarning("Sort in CanonicalTensor::Norm2");
            Mout << " Norm2:  " << result << std::endl;
            result = static_cast<real_t>(0.0);
            result_ptr = result_array.get();
            std::sort(result_ptr, result_ptr+size, [](T i, T j) { return std::abs(i) < std::abs(j); });
            for(int i = 0; i < size; ++i)
            {
               result += std::real(*(result_ptr++));
            }

            // Check sanity of sorted norm2
            if (  !midas::util::IsSane(result)
               || (  libmda::numeric::float_neg(result)
                  && !libmda::numeric::float_numeq_zero(result, static_cast<real_t>(1.), 1000)
                  )
               )
            {
               MidasWarning("Safe norm2");
               result = this->SafeNorm2();
            }

            // If the result was very small and negative, we take abs value.
            result = std::abs(result);
         }
      }
   }
   
   return result;
}


/**
 * Total number of elements in CP tensor
 **/
template<class T>
unsigned CanonicalTensor<T>::NElem() const
{
   const auto& dims = this->GetDims();
   return this->GetRank()*std::accumulate(dims.begin(), dims.end(), 0);
}


/*! Scale CanonicalTensor with a scalar.
 *  This is done by scaling the mode matrix for dimension 0,
 *  as there is no lambda coefficients.
 **/
template<class T>
void CanonicalTensor<T>::Scale(const T& aScalar)
{
   if(this->NDim() > 0)
   {
      auto phase = aScalar / std::abs(aScalar);
      for(int i = 0; i < this->NDim(); ++i)
      {
         auto size = mRank * this->mDims[i];
         for(int size_idx = 0; size_idx < size; ++size_idx)
         {
            mModeMatrices[i][size_idx] *= std::pow(std::abs(aScalar), 1.0/this->NDim());
            if(i == 0)
            {
               mModeMatrices[i][size_idx] *= phase;
            }
         }
      }
   }
}

/**
 * Balance the tensor by distributing the norm equally among the 
 * vectors in each direct product.
 **/
template <class T>
void CanonicalTensor<T>::BalanceModeVectors
   (
   )
{
   auto ndim = this->NDim();
   real_t t0(0.);
   real_t t1(1.);
   real_t t2(2.);

   // Allocate one vector on each thread
   std::vector<real_t> norm2s(ndim);

   // Declare variables
   real_t norm2_tot = t1;
   real_t vec_norm = t0;
   real_t scaling = t0;

   // Loop over rank-1 tensors
   #pragma omp parallel for firstprivate(norm2s, norm2_tot, vec_norm, scaling)
   for(unsigned irank=0; irank<this->mRank; ++irank)
   {
      // Calculate individual mode-vector norms
      for(unsigned idim=0; idim<ndim; ++idim)
      {
         norm2s[idim] = this->ModeVectorNorm2(idim, irank);
      }
      
      // Calculate norm2 of rank-1 tensor
      norm2_tot = t1;
      for(const auto& inorm2 : norm2s)
      {
         norm2_tot *= inorm2;
      }

      // Vector norm must be the ndim'th root of the sqrt of norm2_tot.
      vec_norm = std::pow(norm2_tot, t1/(ndim*t2));

      // Check sanity
      if (  libmda::numeric::float_neg(vec_norm)
         || !midas::util::IsSane(vec_norm)
         )
      {
         Mout  << " ERROR IN BALANCE MODE VECTORS!\n"
               << *this
               << std::endl;
         MIDASERROR("CanonicalTensor::BalanceModeVectors: vec_norm = " + std::to_string(vec_norm));
      }

      // Scale mode vectors
      scaling = t0;
      if (  norm2_tot > C_0   // Otherwise we get scaling = 0/0
         )
      {
         for(unsigned jdim=0; jdim<ndim; ++jdim)
         {
            scaling = vec_norm / std::sqrt(norm2s[jdim]);
            
            // Check sanity
            if (  !midas::util::IsSane(scaling)
               )
            {
               Mout  << " !!! ERROR in BalanceModeVectors !!!\n"
                     << " vec_norm     = " << vec_norm << "\n"
                     << " norm2s[jdim] = " << norm2s[jdim] << "\n"
                     << " scaling      = " << scaling << "\n"
                     << std::flush;
               MIDASERROR("CanonicalTensor::BalanceModeVectors: scaling = " + std::to_string(scaling));
            }

            this->ScaleModeVector(jdim, irank, scaling);
         }
      }
   }
}

/**
 * Calculate squared norm of vector in mode matrix
 *
 * @param aDim    The dimension index of the vector (which mode).
 * @param aRank   The rank index of the vector (which rank-1 tensor).
 *
 * @return
 *    The squared norm of the vector.
 **/
template <class T>
typename BaseTensor<T>::real_t CanonicalTensor<T>::ModeVectorNorm2
   (  unsigned aDim
   ,  unsigned aRank
   )  const
{
   // Check input
   auto ndim = this->NDim();
   assert( aDim < ndim );
   assert( aRank < this->mRank );

   // Declare variables
   real_t result(0.);
   auto length = this->mDims[aDim];
   T* mat_ptr = this->mModeMatrices[aDim];

   // Skip to relevant mode vector
   mat_ptr += aRank*length;

   T value = C_0;
   for(unsigned i=0; i<length; ++i)
   {
      value = *(mat_ptr++);
      result += std::real(midas::math::Conj(value)*value);
   }

   // Check sanity
   if (  libmda::numeric::float_neg(result)
      || !midas::util::IsSane(result)
      )
   {
      Mout  << " ERROR in ModeVectorNorm2 for tensor:\n" << *this << std::endl;
      MIDASERROR("CanonicalTensor::ModeVectorNorm2 calculates: " + std::to_string(result));
   }

   return result;
}

/**
 * Scale mode vector by constant.
 *
 * @param aDim       The dimension index of the vector (which mode).
 * @param aRank      The rank index of the vector (which rank-1 tensor).
 * @param aScaling   The scaling factor.
 **/
template 
   <  class T
   >
template
   <  typename U
   >
void CanonicalTensor<T>::ScaleModeVector
   (  unsigned aDim
   ,  unsigned aRank
   ,  U aScaling
   )
{
   // Check input
   auto ndim = this->NDim();
   assert( aDim < ndim );
   assert( aRank < this->mRank );

   // Declare variables
   auto length = this->mDims[aDim];
   T* mat_ptr = this->mModeMatrices[aDim];

   // Skip to relevant mode vector
   mat_ptr += aRank*length;

   for(unsigned i=0; i<length; ++i)
   {
      *(mat_ptr++) *= aScaling;
   }
}

/**
 * Scale mode matrix
 *
 * @param aDim       Dimension index of mode matrix.
 * @param aScaling   Scaling factor
 **/
template
   <  class T
   >
template
   <  typename U
   >
void CanonicalTensor<T>::ScaleModeMatrix
   (  unsigned aDim
   ,  U aScaling
   )
{
   auto ndim = this->NDim();

   assert( aDim < ndim );

   auto size = this->mDims[aDim] * this->mRank;
   T* mat_ptr = this->mModeMatrices[aDim];

   for(unsigned i=0; i<size; ++i)
   {
      *(mat_ptr++) *= aScaling;
   }
}

/// In-place addition.
/*! Only works if \p other is also a CanonicalTensor, in which case the new mModeMatrices
 *  are appended.
 */
template <class T>
BaseTensor<T>* CanonicalTensor<T>::operator+=(const BaseTensor<T>& other)
{
   try
   {
      assert_same_shape(*this, other);

      switch(other.Type())
      {
         case BaseTensor<T>::typeID::CANONICAL:
         {
            const CanonicalTensor<T>& other_cast=dynamic_cast<const CanonicalTensor<T>&>(other);

            // NB: We need to implement the case of this->mRank > 1
            // If we have a rank-1 vector, addition is much more simple
            if (  this->NDim() == 1
               && this->mRank == 1
               )
            {
               T* other_matrix = other_cast.mModeMatrices[0];

               for(size_t r = 0; r < other_cast.mRank; ++r)
               {
                  T* ptr = this->mModeMatrices[0];
                  for(size_t i = 0; i < this->mDims[0]; ++i)
                  {
                     *(ptr++) += *(other_matrix++);
                  }
               }
            }
            else  // We need to allocate a new tensor
            {
               // Allocate new mode matrices
               T** new_matrices = Base::Allocator_Tpp::Allocate(this->NDim());
               for(int idim=0;idim<this->NDim();++idim)
               {
                  new_matrices[idim] = Base::Allocator_Tp::Allocate(this->mDims[idim] * (this->mRank + other_cast.mRank));

                  T* ptr_out = new_matrices[idim];

                  // Copy this.mModeMatrices
                  T* ptr_in1 = this->mModeMatrices[idim];
                  for(int irank=0;irank<this->mRank;++irank)
                  {
                     for(int i=0;i<this->mDims[idim];++i)
                     {
                        *(ptr_out++)=*(ptr_in1++);
                     };
                  }

                  // Copy other.mModeMatrices
                  T* ptr_in2=other_cast.mModeMatrices[idim];
                  for(int irank=0;irank<other_cast.mRank;++irank)
                  {
                     for(int i=0;i<this->mDims[idim];++i)
                     {
                        *(ptr_out++)=*(ptr_in2++);
                     };
                  }
                  // Delete the matrix we just copied
                  Base::Allocator_Tp::Deallocate(mModeMatrices[idim], this->mDims[idim] * this->mRank);
               }
               // Done copying, delete previous matrices
               Base::Allocator_Tpp::Deallocate(mModeMatrices, this->NDim());

               // Increase rank
               this->mRank += other_cast.mRank;
               // Re-assign new matrices
               this->mModeMatrices = new_matrices;
            }
            return this;
            break;
         }
         default:
         {
            MIDASERROR("CanonicalTensor += "+other.ShowType()+" not supported.");
         }
      }

   }
   catch(const std::bad_cast&)
   {
      MIDASERROR("ERROR: Trying to add tensors of different classes: <"+this->ShowType()+"> and <"+other.ShowType()+">");
   }
   return nullptr; // should not reach here
}

/// In-place element-wise multiplication.
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::operator*=(const BaseTensor<T>& other)
{
   try
   {
      const CanonicalTensor<T>& other_cast=dynamic_cast<const CanonicalTensor<T>&>(other);
      
      assert_same_shape( *this, other_cast );

      T** new_matrices = Base::Allocator_Tpp::Allocate(this->NDim());
      for(int idim=0;idim<this->NDim();++idim)
      {
         new_matrices[idim] = Base::Allocator_Tp::Allocate(this->mDims[idim] * (this->mRank * other_cast.mRank));

         T* ptr_out=                new_matrices[idim];
         T* ptr_in2_row=other_cast.mModeMatrices[idim];
         T* ptr_in2 = nullptr;

         for(int irank_other=0;irank_other<other_cast.mRank;++irank_other)
         {
            T* ptr_in1=this->mModeMatrices[idim];
            for(int irank_this=0;irank_this<this->mRank;++irank_this)
            {
               ptr_in2=ptr_in2_row;
               for(int i=0;i<this->mDims[idim];++i)
               {
                  *(ptr_out++)=*(ptr_in1++) * *(ptr_in2++) ;
               };
            };
            ptr_in2_row=ptr_in2;
         }
         //
         Base::Allocator_Tp::Deallocate(this->mModeMatrices[idim], this->mDims[idim] * this->mRank);
      }
      Base::Allocator_Tpp::Deallocate(this->mModeMatrices, this->NDim());

      this->mRank *= other_cast.mRank;
      this->mModeMatrices = new_matrices;

      return this;
   }
   catch(const std::bad_cast&)
   {
      MIDASERROR("ERROR: cannot perform << A *= B >> with A Canonical and B non-Canonical.\n"
                               "       Swap arguments order if doing A*B, convert B to Canonical or\n"
                               "       convert A to SimpleTensor.");
   }
   return nullptr; // should not reach here
}

/**
 *
 **/
template
   <  class T
   >
CanonicalTensor<T>* CanonicalTensor<T>::PartialHadamardProduct
   (  unsigned aIdx
   ,  const CanonicalTensor<T>& aTensor
   )
{
   // Sanity check
   if (  aTensor.NDim() != 1
      && aTensor.GetRank() != 1
      )
   {
      MIDASERROR("CanonicalTensor::PartialHadamardProduct is only implemented for aTensor being a rank-1 vector!");
   }

   // Get mode matrices
   T* tens_ptr = this->GetModeMatrices()[aIdx];
   auto extent = this->Extent(aIdx);
   auto rank = this->GetRank();

   assert(extent == aTensor.TotalSize());

   for(size_t r = 0; r < rank; ++r)
   {
      T* vec_ptr = aTensor.GetModeMatrices()[0];
      for(size_t i = 0; i < extent; ++i)
      {
         *(tens_ptr++) *= *(vec_ptr++);
      }
   }

   return this;
}

/// In-place element-wise division.
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::operator/=(const BaseTensor<T>& other)
{
   MIDASERROR("CanonicalTensor::operator/=() NOT IMPLEMENTED YET!");  
   return nullptr;
}

// Axpy
template<class T>
void CanonicalTensor<T>::Axpy(const BaseTensor<T>& arX, const T& arA)
{
   // First assert that the two tensors have identical shapes.
   assert_same_shape(*this, arX);

   // Then switch according to type of the other tensor.
   // Cases, other is
   //  - CanonicalTensor; add a*x as new terms in the CP expansion, i.e.
   //    increase rank. Maybe recompress afterwards if above a given rank
   //    threshold.
   //  - ZeroTensor; do nothing.
   // (- SimpleTensor; error (default), since it would require implicit
   //    conversion to Canonical format.)
   switch(arX.Type())
   {
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // Const-references for the data needed.
         const CanonicalTensor<T>& x_cast = static_cast<const CanonicalTensor<T>&>(arX);

         // NB: We need to implement the case of this->mRank > 1
         // If we have a rank-1 vector, addition is much more simple
         if (  this->NDim() == 1
            && this->mRank == 1
            )
         {
            T* x_matrix = x_cast.mModeMatrices[0];

            for(size_t r = 0; r < x_cast.mRank; ++r)
            {
               T* ptr = this->mModeMatrices[0];
               for(size_t i = 0; i < this->mDims[0]; ++i)
               {
                  *(ptr++) += *(x_matrix++) * arA;
               }
            }
         }
         else
         {
            // Allocate new (enlarged) mode matrices.
            T** y_new_matrices = Base::Allocator_Tpp::Allocate(this->NDim());
            const unsigned int y_new_rank = this->GetRank() + x_cast.GetRank();

            // Loop through modes, copy old and new mode matrices (do axpy on the
            // latter).
            for(std::size_t mode = I_0; mode < this->NDim(); ++mode)
            {
               // Allocate new mode matrix for this mode.
               y_new_matrices[mode] = Base::Allocator_Tp::Allocate(y_new_rank * this->GetDims()[mode]);
               T* ptr_y_new         = y_new_matrices[mode];

               // Copy the this.mModeMatrices, i.e. the initial y tensor.
               T* ptr_y_init           = this->mModeMatrices[mode];
               std::size_t y_init_size = this->GetRank()*this->GetDims()[mode];
               T* ptr_y_new_end        = ptr_y_new + y_init_size;
               
               while(ptr_y_new != ptr_y_new_end)
               {
                  *(ptr_y_new++) = *(ptr_y_init++);
               }

               // Copy x.mModeMatrices, i.e. the x tensor in the axpy.
               // For the first mode we have special treatment since the axpy
               // scalar needs to be multiplied somewhere.
               T* ptr_x_init           = x_cast.GetModeMatrices()[mode];
               std::size_t x_init_size = x_cast.GetRank()*this->GetDims()[mode];
               ptr_y_new_end           += x_init_size;

               if(mode == I_0)
               {
                  while(ptr_y_new != ptr_y_new_end)
                  {
                     *(ptr_y_new++) = arA * (*(ptr_x_init++));
                  }
               }
               else
               {
                  while(ptr_y_new != ptr_y_new_end)
                  {
                     *(ptr_y_new++) = *(ptr_x_init++);
                  }
               }

               // Delete the matrix (y_init) we just copied.
               Base::Allocator_Tp::Deallocate(mModeMatrices[mode], this->mDims[mode] * this->mRank);
            }

            // Done copying, delete previous matrices.
            Base::Allocator_Tpp::Deallocate(mModeMatrices, this->NDim());

            // Increase rank.
            this->mRank = y_new_rank;

            // Re-assign new matrices.
            this->mModeMatrices = y_new_matrices;
         }
         break;
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         // Adding a zero-tensor, i.e. do nothing...
         break;
      }
      default:
      {
         MIDASERROR("In CanonicalTensor<T>::Axpy: Not impl. for other tensor being of type '"+arX.ShowType()+"'.");
         break;
      }
   }
}

/// Create a new CanonicalTensor by keeping the first \p newrank mModeMatrices and dropping the rest.
template <class T>
CanonicalTensor<T> CanonicalTensor<T>::Truncate
   (  const unsigned int newrank
   )  const
{
   if (  newrank > this->mRank   )
   {
      MIDASERROR("CanonicalTensor::Truncate() Cannot truncate to higher rank");
      return *this;
   }
   else if  (  newrank == this->mRank  )
   {
      return *this;
   }
   else
   {
      CanonicalTensor<T> result(this->mDims, newrank);
   
      for(unsigned int idim=0;idim<this->NDim();idim++)
      {
         for(size_t i=0;i<result.mRank*this->mDims[idim];i++)
         {
            result.mModeMatrices[idim][i]=mModeMatrices[idim][i];
         }
      }
      return result;
   }
}

/**
 *Return rank-1 tensor having maximum overlap with target tensor
 *@return         Rank-1 tensor in this with maximum overlap
 **/
template <class T>
CanonicalTensor<T> CanonicalTensor<T>::BestRankOneTensor
   (
   )  const
{
   // Initializations
   CanonicalTensor<T> result(this->mDims, 1);   // The result tensor
   real_t max_overlap = C_0;                        // The current maximum overlap
   real_t tmp_overlap = C_0;                        // The overlap for a given rank-1 tensor
   unsigned int best_term = 0;                  // The number of the best rank-1 tensor in the expansion
   T* ptr = nullptr;                            // Pointer used for copying data to result

   // Calculate best_term
   for(unsigned int irank=0; irank<this->mRank; ++irank)
   {
      tmp_overlap = std::abs(this->RankOneOverlap(irank));
      if(tmp_overlap > max_overlap)
      {
         max_overlap = tmp_overlap;
         best_term = irank;
      }
   }

   // Copy data of best rank-1 tensor to result
   for(unsigned int iindex=0; iindex<this->NDim(); ++iindex) // Iterate over mode matrices (index number)
   {
      ptr = result.mModeMatrices[iindex]; // pointer to first element in mode matrix

      // Iterate over vector elements for the dimension corresponding to iindex
      // The dimension index is leading in the array (rank index sub-leading),
      // so we copy the numbers between "best_term*dims" and "(best_term+1)*dims"
      for(unsigned int idim=best_term*this->mDims[iindex]; idim<(best_term+1)*this->mDims[iindex]; ++idim)
      {
         *(ptr++) = this->mModeMatrices[iindex][idim];
      }
   }

   return result;
}

/**
 *Return the overlap of the rank-1 tensor with the given rank index with the whole tensor
 *@param       aRank                The number of the rank-1 tensor in the CP expansion
 **/
template <class T>
T CanonicalTensor<T>::RankOneOverlap(const unsigned int aRank) const
{
   // Initializations
   T result = 0.;       ///< The resulting overlap
   T* ptr = nullptr;    ///< Pointer to mode matrix used in loop

   // Calculate overlap
   for(unsigned int iindex=0; iindex<this->NDim(); ++iindex)
   {
      // Set pointer to first element of relevant mode matrix
      ptr = this->mModeMatrices[iindex];

      for(unsigned int irank=0; irank<this->mRank; ++irank)
      {
         for(unsigned int idim=0; idim<this->mDims[iindex]; ++idim)
         {
            result += midas::math::Conj(*(ptr++))*this->mModeMatrices[iindex][idim+aRank*this->mDims[iindex]];
         }
      }
   }

   return result;
}

/**
 * Optimize pivot by searching for large element of tensor
 *
 * @param arPivot    The input pivot
 * @param aMaxSteps  Maximum number of optimization steps
 *
 * @return
 *    AbsValue at optimized pivot
 **/
template
   <  class T
   >
typename BaseTensor<T>::real_t CanonicalTensor<T>::OptimizePivot
   (  std::vector<unsigned>& arPivot
   ,  const unsigned aMaxSteps
   )  const
{
   // Declarations
   const auto& dims = this->mDims;
   auto ndim = dims.size();
   auto rank = this->mRank;

   const auto pivot_start = arPivot;

   // Find a good pivot
   auto pivot_value = std::abs( this->Element(pivot_start) );
   for( unsigned irank=0; irank<rank; ++irank )
   {
      auto tmp_indices = pivot_start;

      // Optimize pivot indices for given rank
      for( unsigned idim=0; idim<ndim; ++idim )
      {
         real_t maxval(0.);
         for( unsigned i=0; i<dims[idim]; ++i )
         {
            real_t value = std::abs( this->mModeMatrices[idim][i+dims[idim]*irank] );
            if (  value > maxval
               )
            {
               tmp_indices[idim] = i;
               maxval = value;
            }
         }
      }
      
      const auto new_value = std::abs( this->Element(tmp_indices) );

      if (  new_value > pivot_value
         )
      {
         arPivot = tmp_indices;
         pivot_value = new_value;
      }
   }

   if (  aMaxSteps > 0  )
   {
      MidasWarning("CanonicalTensor::CrossApproximation: Pivot search not implemented!");
   }

   return pivot_value;
}


/******** HELPER FUNCTIONS ****************/

template <class T>
typename CanonicalTensor<T>::Base::UniqueArray CanonicalTensor<T>::calculate_inner_product_matrix
   (  const unsigned int left_rank
   ,  const std::vector<unsigned int>& left_dims
   ,  T** left_matrices
   ,  const std::vector<unsigned int>& left_indices
   ,  const unsigned int               right_rank
   ,  const std::vector<unsigned int>& right_dims
   ,  T** right_matrices
   ,  const std::vector<unsigned int>& right_indices
   ,  bool conjugate_left
   )  const
{
   auto result = Base::Allocator_Tp::AllocateUniqueArray(left_rank*right_rank);
   for(size_t i=0;i<left_rank*right_rank;i++)
   {
      result[i]=1.0;
   }

   auto left_index =left_indices .begin();
   auto right_index=right_indices.begin();
   auto temp = Base::Allocator_Tp::AllocateUniqueArray(left_rank * right_rank);

   while(left_index != left_indices.end())
   {
      transposematrix_times_matrix<T>  (  left_rank
                                       ,  left_dims[*left_index]
                                       ,  right_rank
                                       ,  left_matrices [*left_index ]
                                       ,  right_matrices[*right_index]
                                       ,  temp.get()
                                       ,  conjugate_left
                                       );
      element_wise_multiply<T>
         (  result
         ,  temp
         ,  left_rank*right_rank 
         );
      ++left_index;
      ++right_index;
   }
   return result;
}

/************ END OF HELPER FUNCTIONS **************/

template <class T>
template <class U>
typename CanonicalTensor<T>::Base::UniqueArray CanonicalTensor<T>::PartialGammaMatrix
   (  const CanonicalTensor<T>* const aOther
   ,  const std::vector<unsigned>& aIndices
   ,  const unsigned int idim
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
   size_t totalsize = this->GetRank() * aOther->GetRank();
   auto gamma = Base::Allocator_Tp::AllocateUniqueArray(totalsize);
   for(size_t i = 0; i < totalsize; i++)
   {
      gamma[i]=1.0;
   }

   auto temp = Base::Allocator_Tp::AllocateUniqueArray(aOther->GetRank() * this->GetRank());

   for(int jdim = 0; jdim < aIndices.size(); jdim++)
   {
      if(aIndices[jdim] != idim)
      {
         transposematrix_times_matrix<T>  (  aOther->GetRank()
                                          ,  aOther->GetDims()[jdim]
                                          ,  this->GetRank()
                                          ,  aOther->GetModeMatrices()[jdim]
                                          ,  this->GetModeMatrices()[aIndices[jdim]]
                                          ,  temp.get()
                                          ,  true
                                          );

         element_wise_multiply<T>( gamma, temp, totalsize );
      }
   }
   return gamma;
}

/**
 *
 **/
template <class T>
template <class U>
CanonicalTensor<T> CanonicalTensor<T>::DirectIncrease
   (  const CanonicalTensor<T>& increment
   ,  const T factor
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
   assert_same_shape(*this, increment);

   CanonicalTensor<T> copy=*this;

   for(unsigned int idim=0;idim<this->NDim();++idim)
   {
      T* this_ptr=     copy.mModeMatrices[idim];
      T* incr_ptr=increment.mModeMatrices[idim];
      for(unsigned int irank=0;irank<mRank;++irank)
      {
         for(unsigned int i=0;i<this->mDims[idim];++i)
         {
            *(this_ptr++)+= factor * *(incr_ptr++);
         }
      }
   }
   return copy;
}

/**
 *
 **/
template <class T>
void CanonicalTensor<T>::PutNewTransposeModeMatrix
   (  T* matrix
   ,  const unsigned int idim
   )
{
// Copy to the right place
   T *out = mModeMatrices[idim];
   for (int irank = 0; irank < mRank; ++irank)
   {
      T *in = matrix + irank;
      auto dimension = this->mDims[idim];
      for (int i = 0; i < dimension; ++i)
      {
         *(out++) = *in;
         in += mRank;
      }
   }
   return;
}

/**
 *
 **/
template <class T>
void CanonicalTensor<T>::PutNewTransposeModeMatrix
   (  T* matrix
   ,  T* coeffs
   ,  const unsigned int idim
   )
{
// Copy to the right place
   T *out = mModeMatrices[idim];
   for (int irank = 0; irank < mRank; ++irank)
   {
      T *in = matrix + irank;
      for (int i = 0; i < this->mDims[idim]; ++i)
      {
         *(out++) = *in * coeffs[irank];
         in += mRank;
      }
   }
   return;
}

/**
 *
 **/
template <class T>
void CanonicalTensor<T>::PutNewModeMatrix
   (  T* matrix
   ,  const unsigned int idim
   )
{
   T* out = mModeMatrices[idim];
   for(int irank=0; irank<mRank; ++irank)
   {
      for(int i=0; i<this->mDims[idim]; ++i)
      {
         *(out++) = *(matrix++);
      }
   }
}

/**
 *
 **/
template <class T>
void CanonicalTensor<T>::PutNewModeMatrix
   (  T* matrix
   ,  T* coeffs
   ,  const unsigned int idim
   )
{
   T* out = mModeMatrices[idim];
   for(int irank=0; irank<mRank; ++irank)
   {
      for(int i=0; i<this->mDims[idim]; ++i)
      {
         *(out++) = *(matrix++) * coeffs[irank];
      }
   }
}

/// Down contract.

/** Contract the tensor along the `idx`-th dimension with vector `vect`.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{jk}=A_{ijk}h_{i}\f$,
 *   - With `idx=1`, return a second-order tensor with elements 
 *     \f$B_{ik}=A_{ijk}h_{j}\f$,
 *   - With `idx=2`, return a second-order tensor with elements 
 *     \f$B_{ij}=A_{ijk}h_{k}\f$.                                       
 **/
template <class T>
BaseTensor<T>* CanonicalTensor<T>::ContractDown
   ( const unsigned int idx
   , const BaseTensor<T>* vect_in
   ) const
{
   try
   {
      // Ensure vect_in is a CanonicalTensor instance
      const SimpleTensor<T>& vect=dynamic_cast<const SimpleTensor<T>&>(*vect_in);
      /* Compute output dimensions and initialize output tensor
       * new_dims = [ old_dims[:idx-1],  old_dims[idx+1:] ] */
      std::vector<unsigned int> newdims(this->NDim() - 1);
      unsigned counter = 0;
      for(unsigned i = 0; i < this->NDim(); ++i)
      {
         if(i != idx)
         {
            newdims[counter] = this->mDims[i];
            ++counter;
         }
      }

      /* Carry out contraction */
      // Calculate new coefficients resulting from the contraction
      std::vector<T> factors(mRank);
      std::vector<T> phase(mRank);
      T *tensin_ptr = mModeMatrices[idx];
      for(unsigned int irank = 0; irank < mRank; irank++)
      {
         const T *vect_ptr=vect.GetData();
         T dotproduct=0.0;
         for(unsigned int i = 0; i < this->mDims[idx]; i++)
         {
            dotproduct += *(tensin_ptr++) * *(vect_ptr++) ;
         }
         factors[irank] = dotproduct;
      }
      
      // If we contracted away all indices we return a Scalar instead of a CanonicalTensor
      // Check if we should return a Scalar by checking size of newdims
      if(newdims.size() == 0)
      {
         auto scalar = static_cast<T>(0.0);
         for(unsigned int irank = 0; irank < mRank; irank++)
         {
            scalar += factors[irank];
         }
         return new Scalar<T>(scalar);
      }
      
      // if we contracted away all except for 1 index, we utilize the fact that all ranks can be
      // summed into a vector, and return result as a rank 1 CanonicalTensor
      if(newdims.size() == 1)
      {
         CanonicalTensor<T>* result = new CanonicalTensor<T>(std::move(newdims), 1);
         auto idim_in = (idx == 0 ? 1 : 0);
         
         T* tensout_ptr = result->mModeMatrices[0];
         // zero output ptr
         for(unsigned int i = 0; i < this->mDims[idim_in]; ++i)
         {
            *(tensout_ptr++) = static_cast<T>(0.0);
         }
         // then add up result
         T* tensin_ptr  = mModeMatrices[idim_in];
         for(unsigned int irank = 0; irank < mRank; ++irank)
         {
            tensout_ptr = result->mModeMatrices[0];
            for(unsigned int i = 0; i < this->mDims[idim_in]; ++i)
            {
               *(tensout_ptr++) += factors[irank] * *(tensin_ptr++);
            }
         }
         return result;
      }
      
      // if none of the above cases where hit, we return the general result
      // first: scale factors depending on number of dimensions
      for(unsigned int irank = 0; irank < mRank; ++irank)
      {
         phase[irank] = factors[irank] / std::abs(factors[irank]);
         factors[irank] = std::pow(std::abs(factors[irank]), 1.0/newdims.size()); // value
      }

      // Copy all other dimensions
      CanonicalTensor<T>* result = new CanonicalTensor<T>(std::move(newdims), mRank);

      unsigned int idim_out = 0;
      bool phase_applied = false;
      for(unsigned int idim_in = 0; idim_in < this->NDim(); idim_in++)
      {
         if(idim_in!=idx)
         {
            T *tensin_ptr  =        mModeMatrices[idim_in];
            T *tensout_ptr = result->mModeMatrices[idim_out];
            for(unsigned int irank=0;irank<mRank;irank++)
            {
               for(unsigned int i = 0; i < this->mDims[idim_in]; i++)
               {
                  if(!phase_applied) // we should only apply phase once
                  {
                     *(tensout_ptr++) = phase[irank] * factors[irank] * *(tensin_ptr++);
                  }
                  else
                  {
                     *(tensout_ptr++) = factors[irank] * *(tensin_ptr++);
                  }
               }
            }
            phase_applied = true; // we have now been through the loop one time, so phase has been applied
            idim_out++;
         }
      }
      return result;
   }
   catch(const std::bad_cast&)
   {
      MIDASERROR("ERROR: In CanonicalTensor<T>::ContractDown:  vect_in must be a SimpleTensor<T> instance.");
   }
   return nullptr;
}

/// Forward contract.
/** Contract the tensor along the `idx`-th dimension with the second index of a matrix `matrix`.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{i'jk}=A_{ijk}h_{i'i}\f$,
 *   - With `idx=1`, return a second-order tensor with elements 
 *     \f$B_{ij'k}=A_{ijk}h_{j'j}\f$,
 *   - With `idx=2`, return a second-order tensor with elements 
 *     \f$B_{ijk'}=A_{ijk}h_{k'k}\f$.                                       
 **/
template <class T>
BaseTensor<T>* CanonicalTensor<T>::ContractForward
   ( const unsigned int idx
   , const BaseTensor<T>* matrix_in
   ) const
{
   try
   {
      // Ensure matrix_in is a SimpleTensor instance
      const SimpleTensor<T>& matrix=dynamic_cast<const SimpleTensor<T>&>(*matrix_in);
      /* Compute output dimensions and initialize output tensor
       * new_dims = [ old_dims[:idx-1], matrix.dims[1], old_dims[idx+1:] ] */
      std::vector<unsigned int> newdims(this->NDim());
      for(unsigned i = 0; i < newdims.size(); ++i)
      {
         if(i == idx)
         {
            newdims[i]= matrix.mDims[0];
         }
         else
         {
            newdims[i] = this->mDims[i];
         }
      }

      /* Carry out contraction */
      CanonicalTensor<T>* result=new CanonicalTensor<T>(std::move(newdims), mRank);
      // Copy all other dimensions
      auto this_ndim = this->NDim();
      for(unsigned int idim = 0; idim < this_ndim; idim++)
      {
         if(idim != idx)
         {
            T *tensin_ptr  =         mModeMatrices[idim];
            T *tensout_ptr = result->mModeMatrices[idim];
            auto size = mRank*this->mDims[idim];
            for(size_t i = 0; i < size; i++)
            {
               *(tensout_ptr++) = *(tensin_ptr++);
            }
         }
      }
      // Calculate new matrix for this dimension, copy coefficients
      T *tensout_ptr = result->mModeMatrices[idx];
      T *tensin_ptr  = this->mModeMatrices[idx];
      auto result_dim = result->mDims[idx];
      auto this_dim = this->mDims[idx];
      for(unsigned int irank = 0; irank < mRank; irank++)
      {
         const T *matrix_ptr = matrix.GetData();
         for(unsigned int j = 0; j < result_dim; ++j)
         {
            T* tensin_inner_ptr = tensin_ptr;
            for(unsigned int i = 0; i < this_dim; ++i)
            {
               *(tensout_ptr) += *(tensin_inner_ptr++) * *(matrix_ptr++);
            };
            ++tensout_ptr;
         }
         tensin_ptr += this_dim;
      }
      return result;
   }
   catch(const std::bad_cast&)
   {
      MIDASERROR("ERROR: In CanonicalTensor<T>::ContractForward: matrix_in must be a SimpleTensor<T> instance.");
   }
   return nullptr;
}

/// Up "contract".

/** Outer product of tensor with `vect`, along the `idx`-th dimension.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{pijk}=A_{ijk}h_{p}\f$,
 *   - With `idx=1`, return a second-order tensor with elements
 *     \f$B_{ipjk}=A_{ijk}h_{p}\f$,
 *   - With `idx=2`, return a second-order tensor with elements
 *     \f$B_{ijpk}=A_{ijk}h_{p}\f$,
 *   - With `idx=3`, return a second-order tensor with elements
 *     \f$B_{ijkp}=A_{ijk}h_{p}\f$.                                         */
template <class T>
BaseTensor<T>* CanonicalTensor<T>::ContractUp
   ( const unsigned int idx
   , const BaseTensor<T>* vect_in
   ) const
{
   try
   {
      // Ensure vect_in is a CanonicalTensor instance
      const SimpleTensor<T>& vect=dynamic_cast<const SimpleTensor<T>&>(*vect_in);
      /* Compute output dimensions and initialize output tensor
       * new_dims = [ old_dims[:idx] vector.dims[0], old_dims[idx:] ] */
      std::vector<unsigned int> newdims(this->NDim() + 1);
      unsigned counter = 0;
      for(unsigned int i = 0; i < this->NDim(); ++i)
      {
         if(i == idx)
         {
            newdims[counter] = vect.mDims[0];
            ++counter;
         }
         newdims[counter] = this->mDims[i];
         ++counter;
      }
      if(idx == this->NDim())
      {
         newdims[counter] = vect.mDims[0];
         ++counter;
      }

      /* Carry out contraction */
      CanonicalTensor<T>* result=new CanonicalTensor<T>(std::move(newdims), mRank);
      // Copy all other dimensions
      unsigned int idim_in = 0;
      for(unsigned int idim_out = 0; idim_out < result->NDim(); idim_out++)
      {
         if(idim_out != idx)
         {
            T *tensin_ptr =        mModeMatrices[idim_in];
            T *tensout_ptr=result->mModeMatrices[idim_out];
            auto i_size = mRank*this->mDims[idim_in];
            for(size_t i = 0; i < i_size; i++)
            {
               *(tensout_ptr++)=*(tensin_ptr++);
            }
            idim_in++;
         }
      }
      // Copy new "matrix" (mRank copies of the vector) for this dimension,
      // copy coefficients
      T *tensout_ptr=result->mModeMatrices[idx];
      for(unsigned int irank=0;irank<mRank;irank++)
      {
         const T *vect_ptr=vect.GetData();
         for(unsigned int i=0;i<result->mDims[idx];i++)
         {
            *(tensout_ptr++)=*(vect_ptr++);
         }
      }

      return result;
   }
   catch(const std::bad_cast&)
   {
      MIDASERROR("ERROR: In CanonicalTensor<T>::ContractUp: vect_in must be a SimpleTensor instance.");
   }
   return nullptr;
}

/// See BaseTensor<T>::ContractInternal_impl().
template <class T>
BaseTensor<T>* CanonicalTensor<T>::ContractInternal_impl(
      const std::vector<std::pair<unsigned int, unsigned int>>& inner_indices_pairs,
      const std::vector<unsigned int>& outer_indices) const
{
   // Calculate inner products for each value of irank
   std::vector<T> inner_products(this->mRank, 1.0);
   for(auto inner_indices_pair: inner_indices_pairs)
   {
      T* m0=this->mModeMatrices[inner_indices_pair.first];
      T* m1=this->mModeMatrices[inner_indices_pair.second];
      for(unsigned int irank=0;irank<this->mRank;++irank)
      {
         T contrib=0.0;
         for(unsigned int i=0;i<this->mDims[inner_indices_pair.first];++i)
         {
            contrib+= *(m0++) * *(m1++);
         }
         inner_products[irank]*=contrib;
      }
   }

   std::vector<unsigned int> result_dims;
   for(auto i: outer_indices)
   {
      result_dims.push_back(this->mDims[i]);
   }

   if(result_dims.size()==0)
   {
      T sum=std::accumulate(inner_products.begin(), inner_products.end(), static_cast<T>(0.0));
      return new Scalar<T>(sum);
   }
   else
   {
      CanonicalTensor<T>* result(new CanonicalTensor<T>(result_dims, this->mRank));
      unsigned int iout=0;
      
      // Copy untouched matrices, in the right order
      for(auto iin: outer_indices)
      {
         T* ptr_in= this  ->mModeMatrices[iin];
         T* ptr_out=result->mModeMatrices[iout];
         for(std::size_t i=0;i<this->mDims[iin]*this->mRank;++i)
         {
            *(ptr_out++) = *(ptr_in++);
         }
         ++iout;
      }
   
      T* ptr=result->mModeMatrices[0];
      for(unsigned int irank=0;irank<result->mRank;++irank)
      {
         // Multiply irank row of the first matrix with the inner product
         for(unsigned int i=0;i<result->mDims[0];++i)
         {
            *(ptr++)*=inner_products[irank];
         }
      }
      return result;
   }
   return nullptr;
}

/// See BaseTensor<T>::Contract_impl().
template <class T>
BaseTensor<T>* CanonicalTensor<T>::Contract_impl
   (  const std::vector<unsigned int>& indices_inner_this
   ,  const std::vector<unsigned int>& indices_outer_this
   ,  const BaseTensor<T>&             other_in
   ,  const std::vector<unsigned int>& indices_inner_other
   ,  const std::vector<unsigned int>& indices_outer_other
   ,  const std::vector<unsigned int>& dims_result
   ,  const std::vector<unsigned int>& indices_result_this
   ,  const std::vector<unsigned int>& indices_result_other
   ,  bool conjugate_left
   ,  bool conjugate_right
   )  const
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
   bool one_conj = conjugate_left ^ conjugate_right;

   switch(other_in.Type())
   {
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // cast other_in as a CanonicalTensor instance
         const CanonicalTensor<T>& other=dynamic_cast<const CanonicalTensor<T>&>(other_in);

         // In here one should look for mistakes in idx vectors, e.g. 
         // does any entry repeat in the same vector

         size_t new_rank=this->mRank * other.mRank;
         if(new_rank==0)
         {
            if(dims_result.size()>0)
            {
               return new CanonicalTensor<T>(dims_result, 0);
            }
            else
            {
               return new Scalar<T>(0.0);
            }
         }

         auto inner_product_matrix 
            =   calculate_inner_product_matrix
                   (  this->GetRank()
                   ,  this->GetDims()
                   ,  this->mModeMatrices
                   ,  indices_inner_this
                   ,  other.mRank
                   ,  other.mDims
                   ,  other.mModeMatrices
                   ,  indices_inner_other
                   ,  one_conj // calculate left^H * right if only ONE of the tensors should be conj
                   );

         // If conjugate_right is true, we need to conjugate the result
         if (  is_complex
            && conjugate_right
            )
         {
            for(size_t i=0; i<new_rank; ++i)
            {
               inner_product_matrix[i] = midas::math::Conj(inner_product_matrix[i]);
            }
         }

         if(dims_result.size()==0)
         {
            T sum=0.;
            T* matrix_ptr=inner_product_matrix.get();
            for(size_t i=0;i<new_rank;++i)
            {
               sum+=*(matrix_ptr++);
            }
            return new Scalar<T>(sum);
         }
         else
         {
            // OK, now we take care of those dimensions we did not contract over.
            // Make intermediate
            CanonicalIntermediate<T> foo(this->mModeMatrices
                                        ,other.mModeMatrices
                                        ,indices_outer_this
                                        ,indices_result_this
                                        ,indices_outer_other
                                        ,indices_result_other
                                        ,this->mRank
                                        ,other.mRank
                                        ,this->mDims
                                        ,other.mDims
                                        ,inner_product_matrix.get());

            CanonicalTensor<T>* result=new CanonicalTensor<T>(foo.ToCanonical());
            return result;

         }
      }
      // Mixed Canonical-Simple contraction
      case BaseTensor<T>::typeID::SIMPLE:
      {
         const SimpleTensor<T>& other=dynamic_cast<const SimpleTensor<T>&>(other_in);

         // Intermediate: one internal contraction per irank
         std::vector<BaseTensor<T>*> tmp;
         // BUG: when indices_inner_other is empty, there is a bug... this will terminate in that case
         if(indices_inner_other.size() == 0) MIDASERROR("BUG, FIX ME!");
         for(int irank=0;irank<this->mRank;++irank)
         {
            // We need to copy the indices because they might change as the dimensionalities
            // of the intermediates change
            std::vector<unsigned int> indices_inner_other_copy(indices_inner_other);
            BaseTensor<T>* next = nullptr;
            const BaseTensor<T>* prev=&other;
            auto this_index =indices_inner_this .begin();
            auto other_index=indices_inner_other_copy.begin();
            while(this_index != indices_inner_this.end())
            {
               // Put row of CanonicalTensor modematrix as a vector
               T* column_copy = new T[this->mDims[*this_index]];
               T* column = this->mModeMatrices[*this_index]+irank*this->mDims[*this_index];
               for(int icol = 0; icol < this->mDims[*this_index]; ++icol)
               {
                  column_copy[icol] = one_conj ? midas::math::Conj(column[icol]) : column[icol]; // complex conj left tensor if only one conjugate. If both, we conjugate at the end!
               }
               SimpleTensor<T> vector  (  std::vector<unsigned int>{this->mDims[*this_index]}
                                       ,  column_copy 
                                       );
               // Contract Down with vector
               next=prev->ContractDown(*other_index, &vector);
               // Because the intermediate is one dimension smaller, we need to decrease all
               // contraction indices that are above the one that just disappeared
               for(auto next_index=other_index+1;next_index!=indices_inner_other_copy.end();++next_index)
               {
                  if(*next_index>*other_index)
                  {
                     --(*next_index);
                  }
               }
               // Clean up previous iteration, unless it was the first iteration
               if(prev!=&other) delete prev;
               // Assign pointers
               prev=next;
               ++this_index;
               ++other_index;
            }
            tmp.push_back(next);
         }

         // OK, now we have calculated the intermediate tensor
         // Ready to pack the result
         // NOTE: we don't return inside the if-block, we need to destroy tmp!
         BaseTensor<T>* result = nullptr;

         // Case #1: all dimensions from both tensors were contracted away: return Scalar
         if(dims_result.size()==0)
         {
            result=new Scalar<T>();
            for(auto t: tmp)
               *result += *t;
         }
         else
         {
            // Case #2: Not all dimensions are contracted away, the result is a tensor.
            // Regardless of what we do, we need to copy the remaining matrices from the
            // CanonicalTensor (*this)
            T** modematrices = Base::Allocator_Tpp::Allocate(dims_result.size());
            auto i_in=indices_outer_this.begin();
            for(auto i_out: indices_result_this)
            {
               modematrices[i_out] = Base::Allocator_Tp::Allocate(this->mDims[*i_in] * this->mRank);
               for(int idx = 0; idx < this->mDims[*i_in]*this->mRank; ++idx)
               {
                  modematrices[i_out][idx] = this->mModeMatrices[*i_in][idx];
               }
               ++i_in;
            }
            // Now, depending on how many dimensions were left uncontrated from the
            // SimpleTensor (other) we have three cases: 0, 1, or more

            // Case #2.1: All dimensions from the SimpleTensor have been contracted away:
            // construct CanonicalTensor with remaining matrices, reweigh the first matrix
            if(indices_result_other.size()==0)
            {
               // Reweigh first matrix
               T* ptr=modematrices[0];
               for(auto coeff: tmp) // There are mRank elements in tmp
               {
                  T coeff_cast(dynamic_cast<Scalar<T>*>(coeff)->GetValue());
                  for(unsigned int i=0;i<dims_result[0];++i)
                  {
                     *(ptr++) *= coeff_cast;
                  }
               }

               result=new CanonicalTensor<T>(dims_result, this->mRank, modematrices);
            }
            // Case #2.2: All dimensions but one from the SimpleTensor have been contracted away:
            // construct CanonicalTensor with remaining mode matrices from "this", the intermediate
            // becomes a new mode matrix. The intermediate is a set of vectors, we repack as a matrix
            else if(indices_result_other.size()==1)
            {
               unsigned int i_in =indices_outer_other[0];
               unsigned int i_out=indices_result_other[0];
               modematrices[i_out] = Base::Allocator_Tp::Allocate(other.mDims[i_in]*this->mRank);
               T* ptr_out=modematrices[i_out];
               for(unsigned int irank=0;irank<this->mRank;++irank)
               {
                  memcpy( reinterpret_cast<void*>(ptr_out)
                        , reinterpret_cast<void*>(dynamic_cast<SimpleTensor<T>*>( tmp[irank] )->GetData())
                        , other.mDims[i_in]*sizeof(T)
                        );
                  ptr_out+=other.mDims[i_in];
               }

               result=new CanonicalTensor<T>(dims_result, this->mRank, modematrices);
            }
            // Case #2.3: At least two dimensions from the SimpleTensor survive. Well, something else
            // needs to be done...
            else
            {
               MIDASERROR("The current implementation only supports contracting away all indices from the SimpleTensor. Please convert the CanonicalTensor into a SimpleTensor first. Or figure out how to implement it in general!");
            }
         }

         // Destroy intermediates
         for(auto& t: tmp)
            delete t;

         // If conjugate_right, we need to conjugate the whole thing
         if (  is_complex
            && conjugate_right
            )
         {
            result->Conjugate();
         }

         // And we are done!
         return result;
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         // We call the ZeroTensor implementation of Contract_impl with
         // swapped arguments
         const ZeroTensor<T>& other=dynamic_cast<const ZeroTensor<T>&>(other_in);
         return other.Contract_impl(indices_inner_other,
                                    indices_outer_other,
                                    *this,
                                    indices_inner_this,
                                    indices_outer_this,
                                    dims_result,
                                    indices_result_other,
                                    indices_result_this,
                                    conjugate_right,
                                    conjugate_left);
      }
      default:
      {
         MIDASERROR("In CanonicalTensor<T>::Contract_impl: Not impl. for other tensor being of type '"+other_in.ShowType()+"'");
      }
   }
   return nullptr;
}

/**
 *
 **/
template <class T>
CanonicalTensor<T>* CanonicalTensor<T>::operator*=
   ( const T k
   )
{
   this->Scale(k);
   return this;
}

/**
 *
 **/
template<class T>
void CanonicalTensor<T>::Zero
   (
   )
{
   Deallocate();
   
   //
   // initialize zero canonical tensor as a rank 0 tensor
   //
   auto ndim = this->NDim();
   mRank = 0;
   Allocate();
}

/**
 *
 **/
template
   <  class T
   >
void CanonicalTensor<T>::ConjugateImpl
   (
   )
{
   auto ndim = this->NDim();
   for(int idim=0; idim<ndim; ++idim)
   {
      T* modematrix = this->mModeMatrices[idim];
      auto size = this->mRank * this->Extent(idim);
      for(int ielem=0; ielem<size; ++ielem)
      {
         modematrix[ielem] = midas::math::Conj(modematrix[ielem]);
      }
   }
}


/**
 * Compute the rank-1 cross approximation to the tensor
 * Ref. [Espig, Hackbusch: Regularized Newton CP decomp](https://link.springer.com/article/10.1007/s00211-012-0465-9)
 *
 * @param apTensor
 *    The tensor to fit
 * @param arPivot
 *    The initial pivot to start from
 * @param aMaxSteps
 *    The maximum number of steps for the pivot search
 * @return
 *    The rank-1 cross approximation
 **/
template <class T>
BaseTensorPtr<T> cross_approximation
   (  const BaseTensor<T>* const apTensor
   ,  const std::vector<unsigned>& arPivot
   ,  const unsigned aMaxSteps
   )
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      MIDASERROR("This has not been adapted to complex numbers yet!");
      return nullptr;
   }
   else
   {
      // Declarations
      const auto& dims = apTensor->GetDims();
      auto ndim = dims.size();
      auto pivot_indices = arPivot;

      // Optimize pivot
      auto pivot_value = apTensor->OptimizePivot( pivot_indices, aMaxSteps );

      // Construct cross approximation
      auto result = std::unique_ptr<BaseTensor<T>>( new CanonicalTensor<T>( dims, 1 ) );
      CanonicalTensor<T>& result_ref = *static_cast<CanonicalTensor<T>*>(result.get());

      auto aux_indices = pivot_indices;

      // Loop over dimensions
      for( size_t idim=0; idim<ndim; ++idim )
      {
         // Set pointer to mode matrix (vector for rank-1 tensor)
         T* result_ptr = result_ref.GetModeMatrices()[idim];

         auto n = dims[idim];

         for( size_t j=0; j<n; ++j )
         {
            aux_indices[idim] = j;

            auto val = apTensor->Element(aux_indices);

            *(result_ptr++) = val;
         }
         aux_indices = pivot_indices;
      }

      auto scaling = static_cast<T>(1.) / std::pow(pivot_value, ndim-1);
      result->Scale(scaling);

      return result;
   }
}



/**
 *
 **/
template
   <  class T
   >
BaseTensorPtr<T> random_canonical_tensor_explicit
   (  const std::vector<unsigned int>& dims
   ,  const unsigned int rank
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
   BaseTensorPtr<T> result_ptr(new CanonicalTensor<T>(dims, rank));
   CanonicalTensor<T>& result = *static_cast<CanonicalTensor<T>*>(result_ptr.get());

   for(int i = 0; i < dims.size(); i++)
   {
      T *matrices_ptr=result.mModeMatrices[i];
      auto j_size = rank*dims[i];
      for(decltype(j_size) j = 0; j < j_size; ++j)
      {
         if constexpr   (  !is_complex
                        )
         {
            *(matrices_ptr++)= midas::util::RandomSignedFloat<T>();
         }
         else
         {
            *(matrices_ptr++)= T(midas::util::RandomSignedFloat<real_t>(), midas::util::RandomSignedFloat<real_t>());
         }
      }
   }

   return result_ptr;
}


/**
 *
 **/
template <class T>
BaseTensorPtr<T> const_value_cptensor
   (  const std::vector<unsigned int>& arDims
   ,  const T aValue
   ,  const unsigned int aRank
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   BaseTensorPtr<T> result_ptr(new CanonicalTensor<T>(arDims, aRank));
   CanonicalTensor<T>& result = *static_cast<CanonicalTensor<T>*>(result_ptr.get());
   
   // Set each element in the tensor to (|aValue|/aRank)^(1/order)
   // Put phases on first mode matrix
   const T value = std::pow(std::abs(aValue/static_cast<real_t>(aRank)),C_1/arDims.size());
   const T phase = aValue / std::abs(aValue);

   T* ptr = nullptr;
   for(unsigned int imode = 0; imode<arDims.size(); ++imode)
   {
      ptr = result.mModeMatrices[imode];
      for(unsigned int irank = 0; irank<aRank; ++irank)
      {
         for(unsigned int idim = 0; idim<arDims[imode]; ++idim)
         {
            // Multiply potential phase on first mode-matrix
            if(imode==0) *(ptr++) = value * phase;
            else         *(ptr++) = value;
         }
      }
   }

   return result_ptr;
}


/**
 *
 **/
template
   <  class T
   ,  class U
   ,  midas::type_traits::DisableIfComplexT<U>*
   >
CanonicalTensor<T> compress_to_canonical
   (  const BaseTensor<T>& target
   ,  typename BaseTensor<T>::real_t maxerror
   ,  std::function<CanonicalTensor<T> (const unsigned int)> guesser
   ,  FitReport<U>(CanonicalTensor<T>::* fitter)(const BaseTensor<T>&, const unsigned int, const typename BaseTensor<T>::real_t, midas::type_traits::DisableIfComplexT<U>*)
   )
{
   // If guesser function was not passed, decide which guesser to use based on the
   // type of target
   if(!guesser)
   {
      switch(target.Type())
      {
         case BaseTensor<T>::typeID::CANONICAL:
            {
               const CanonicalTensor<T>& target_cast=dynamic_cast<const CanonicalTensor<T>&>(target);
               guesser=[target_cast](const unsigned int rank)
                       {
                          return SimpleTensor<T>(target_cast).CanonicalSVDGuess(rank);
                       };                  
               break;
            }
         default:
            {
               const SimpleTensor<T>& target_cast=dynamic_cast<const SimpleTensor<T>&>(target);
               guesser=[target_cast](const unsigned int rank)
                       {
                          return target_cast.CanonicalSVDGuess(rank);
                       };
               break;
            }
      }
   }
   CanonicalTensor<T> result;
   // Estimate for maxrank... No idea if it's valid in general. It works up to order 3 for sure.
   auto dims=target.GetDims();
   unsigned int maxrank = target.TotalSize() /
         *(std::max_element(dims.begin(), dims.end()));
   for(unsigned int rank = 0; rank <= maxrank; ++rank)
   {
      result = guesser(rank);
      auto report = (result.*fitter)(target, 50, maxerror, nullptr);
      if(report.error<maxerror)
      {
         break;
      }
   }
   return result;
}

/*! Dot interface
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t CanonicalTensor<T>::Dot
   ( const BaseTensor<T>* const other
   ) const
{
   return midas::math::Conj(other->DotDispatch(this));   // arguments swapped
}

/*! Dot interface
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t CanonicalTensor<T>::DotDispatch
   ( const SimpleTensor<T>* const other
   ) const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this)); // arguments swapped
}

/*! Dot interface
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t CanonicalTensor<T>::DotDispatch
   ( const CanonicalTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

/*! Dot interface
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t CanonicalTensor<T>::DotDispatch
   ( const ZeroTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

/*! Dot interface
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t CanonicalTensor<T>::DotDispatch
   ( const TensorDirectProduct<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

/**
 * Calculate dot product between two mode matrices.
 *
 * @param dim
 *    The index of the mode matrix
 * @param other
 *    The other CanonicalTensor
 *
 * @return
 *    Dot between mModeMatrices[dim] of the two tensors
 **/
template<class T>
T CanonicalTensor<T>::ModeMatrixDot
   (  const unsigned dim
   ,  const CanonicalTensor<T>* const other
   )  const
{
   // Assert same shape of mode matrix
   assert( this->mRank == other->mRank );
   assert( this->mDims[dim] == other->mDims[dim] );

   unsigned size = this->mRank * this->mDims[dim];

   T result = 0.;

   T* left_ptr = this->mModeMatrices[dim];
   T* right_ptr = other->mModeMatrices[dim];

   for(unsigned i=0; i<size; ++i)
   {
      result += midas::math::Conj(left_ptr[i])*right_ptr[i];
   }

   return result;
}

/**
 * Perform Axpy on one mode matrix
 *
 * @param idim       The mode to update
 * @param update     The tensor holding the mode matrix to add
 * @param alpha      Scaling factor for the update
 **/
template <class T>
void CanonicalTensor<T>::ModeMatrixAxpy
   (  const unsigned idim
   ,  const CanonicalTensor<T>& update
   ,  const T alpha
   )
{
   // Assert same shape of mode matrix
   assert_same_shape(*this, update);
   assert( this->mRank == update.mRank );

   size_t matrix_size = this->mRank * this->mDims[idim];

   T* matrix_ptr = this->mModeMatrices[idim];
   T* update_ptr = update.mModeMatrices[idim];

   // Perform matrix axpy
   for(size_t i=0; i<matrix_size; ++i)
   {
      *(matrix_ptr++) += alpha * *(update_ptr++);
   }
}


/**
 * Perform Axpy on mode matrix. Enable transposed input pointer.
 *
 * @param idim       The mode matrix to update
 * @param update     Pointer to update
 * @param alpha      Scaling factor
 * @param transpose  True if update is a pointer to the transposed mode matrix
 **/
template<class T>
void CanonicalTensor<T>::ModeMatrixAxpy
   (  unsigned idim
   ,  T* update
   ,  T alpha
   ,  bool transpose
   )
{
   T* matrix_ptr = this->mModeMatrices[idim];

   // Perform axpy
   if (  transpose   )
   {
      auto dimension = this->mDims[idim];

      for (size_t irank = 0; irank < this->mRank; ++irank)
      {
         T* in = update + irank;
         for (size_t i = 0; i < dimension; ++i)
         {
            *(matrix_ptr++) += alpha * *in;
            in += this->mRank;
         }
      }
   }
   else
   {
      T* update_ptr = update;
      size_t matrix_size = this->mRank * this->mDims[idim];
      for(size_t i=0; i<matrix_size; ++i)
      {
         *(matrix_ptr++) += alpha * *(update_ptr++);
      }
   }
}


/**
 * Find the maximum element of all elements in mModeMatrices.
 *
 * @return
 *    Pair containing dimension and abs value of largest element
 **/
template
   <  class T
   >
std::pair<unsigned, typename BaseTensor<T>::real_t> CanonicalTensor<T>::MaxModeMatrixElement
   (
   )  const
{
   const unsigned ndim = this->NDim();

   std::pair<unsigned, real_t> result = { 0, static_cast<real_t>(0.)};
   real_t tmp_abs_val(0.);

   for(unsigned idim=0; idim<ndim; ++idim)
   {
      unsigned size = this->mRank*this->mDims[idim];
      for(unsigned j=0; j<size; ++j)
      {
         tmp_abs_val = std::abs(this->mModeMatrices[idim][j]);
         if (  tmp_abs_val > std::abs(result.second)
            )
         {
            result.first = idim;
            result.second = tmp_abs_val;
         }
      }
   }

   return result;
}

/**
 * Find the maximum squared norm of the mode matrices.
 *
 * @return
 *    Pair containing dimension and value of largest norm
 **/
template
   <  class T
   >
std::pair<unsigned, typename BaseTensor<T>::real_t> CanonicalTensor<T>::MaxModeMatrixNorm2
   (
   )  const
{
   const unsigned ndim = this->NDim();

   std::pair<unsigned, real_t> result = { 0, static_cast<real_t>(0.)};
   real_t tmp_norm2 = static_cast<real_t>(0.);

   for(unsigned idim=0; idim<ndim; ++idim)
   {
      tmp_norm2 = std::real(this->ModeMatrixDot(idim, this));
      if (  tmp_norm2 > result.second  )
      {
         result.first = idim;
         result.second = tmp_norm2;
      }
   }

   return result;
}

/**
 * Dot product between elements in mode matrices
 *
 * @param other   The tensor to dot with
 * @return        Dot product
 **/
template <class T>
T CanonicalTensor<T>::ElementwiseDot
   (  const CanonicalTensor<T>& other
   )  const
{
   assert_same_shape(*this, other);

   T result = static_cast<T>(0.);

   auto ndim = this->NDim();
   auto dims = this->GetDims();

   auto rank = this->GetRank();
   assert(rank == other.GetRank());

   unsigned size = 0;
   T** mode_matrices = this->mModeMatrices;
   T** other_mode_matrices = other.mModeMatrices;

   for(unsigned idim=0; idim<ndim; ++idim)
   {
      T* mode_matrix = mode_matrices[idim];
      T* other_mode_matrix = other_mode_matrices[idim];
      size = rank*dims[idim];
      for(unsigned j=0; j<size; ++j)
      {
         result += mode_matrix[j]*other_mode_matrix[j];
      }
   }

   return result;
}


/**
 * Return Norm2 of the mode matrices seen as one array.
 *
 * @return  Element-wise norm2
 **/
template <class T>
T CanonicalTensor<T>::ElementwiseNorm2
   (
   )  const
{
   return this->ElementwiseDot(*this);
}

/**
 * Elementwise norm (see mode matrices as one array)
 *
 * @return Elementwise norm
 **/
template <class T>
T CanonicalTensor<T>::ElementwiseNorm
   (
   )  const
{
   return std::sqrt(this->ElementwiseNorm2());
}

/**
 * Perform axpy on all elements of mModeMatrices
 *
 * @param update        The update tensor
 * @param coef          The coefficient
 **/
template <class T>
void CanonicalTensor<T>::ElementwiseAxpy
   (  const CanonicalTensor<T>& update
   ,  const T coef
   )
{
   assert_same_shape(*this, update);

   auto rank = this->mRank;
   assert(rank == update.GetRank());

   auto ndim = this->NDim();

   for(unsigned idim = 0; idim<ndim; ++idim)
   {
      T* this_ptr = this->mModeMatrices[idim];
      T* update_ptr = update.mModeMatrices[idim];
      unsigned mat_size = this->Extent(idim) * rank;

      for(unsigned ielem = 0; ielem<mat_size; ++ielem)
      {
         *(this_ptr++) += coef * *(update_ptr++);
      }
   }
}

/**
 * Scale all elements of mModeMatrices
 *
 * @param coef       The scaling factor
 **/
template <class T>
void CanonicalTensor<T>::ScaleModeMatrices
   (  const T coef
   )
{
   unsigned ndim = this->NDim();
   for(unsigned idim=0; idim<ndim; ++idim)
   {
      T* mode_matrix = this->mModeMatrices[idim];
      unsigned mat_size = this->Extent(idim) * this->mRank;

      for(unsigned ielem=0; ielem<mat_size; ++ielem)
      {
         *(mode_matrix++) *= coef;
      }
   }
}

/**
 * Change a rank-1 term in the CP tensor
 *
 * @param aRankOut   The index of the term to change
 * @param aRankIn    The index of the term in aTensIn
 * @param aTensIn    The input tensor storing the 
 * @param aCoef      Scaling coefficient
 **/
template
   <  class T
   >
void CanonicalTensor<T>::SetRankOneTerm
   (  unsigned aRankOut
   ,  unsigned aRankIn
   ,  const CanonicalTensor<T>& aTensIn
   ,  T aCoef
   )
{
   assert_same_shape(*this, aTensIn);

   assert(aRankOut < this->mRank);
   assert(aRankIn < aTensIn.mRank);

   const auto& dims = this->GetDims();
   auto ndim = this->NDim();

   T** mms_out = this->mModeMatrices;
   T** mms_in = aTensIn.mModeMatrices;

   T phase = aCoef / std::abs(aCoef);
   real_t coef_pow = std::pow(std::abs(aCoef), C_1/ndim);

   for(size_t idim=0; idim<ndim; ++idim)
   {
      auto len = dims[idim];

      // Set pointers to start vectors of in and out
      T* mm_out = mms_out[idim] + aRankOut*len;
      T* mm_in = mms_in[idim] + aRankIn*len;

      auto c = ( idim == 0 ) ? phase*coef_pow : coef_pow;

      for(size_t i=0; i<len; ++i)
      {
         *(mm_out++) = *(mm_in++) * c;
      }
   }
}

/**
 * Get vector of norms of the individual rank-1 terms (lambdas)
 *
 * @return
 *    Norms
 **/
template
   <  class T
   >
std::vector<typename CanonicalTensor<T>::real_t> CanonicalTensor<T>::GetRankOneNorms
   (
   )  const
{
   std::vector<real_t> result;
   result.reserve(mRank);

   real_t norm(0.);

   // Loop over ranks
   for(In irank=I_0; irank<mRank; ++irank)
   {
      real_t norm2(1.);
      // Calculate norm
      for(In idim=I_0; idim<this->NDim(); ++idim)
      {
         norm2 *= this->ModeVectorNorm2(idim, irank);
      }

      norm = std::sqrt(norm2);

      if (  midas::util::IsSane(norm)
         )
      {
         result.emplace_back(norm);
      }
      else
      {
         MIDASERROR("Norm of rank-1 tensor = " + std::to_string(norm));
      }
   }

   // Return result
   return result;
}

#ifdef VAR_MPI
/**
 * Overload of MpiSend for SimpleTensor
 *
 * @param aRecievingRank  The rank to send the SimpleTensor to.
 **/
template<class T>
void CanonicalTensor<T>::MpiSend
   (  In aRecievingRank
   )  const
{
   // send basetensor stuff
   BaseTensor<T>::MpiSendDimensions(aRecievingRank);
   
   // send canonicaltensor stuff
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   midas::mpi::detail::WRAP_Send(&mRank, 1, MPI_UNSIGNED, aRecievingRank, 0, MPI_COMM_WORLD);

   const auto& extents = this->GetDims();
   for(In i = 0; i < this->NDim(); ++i)
   {
      int nelem = extents[i] * mRank;
      midas::mpi::detail::WRAP_Send(mModeMatrices[i], nelem, midas::mpi::DataTypeTrait<T>::Get(), aRecievingRank, 0, MPI_COMM_WORLD);
   }
}

/**
 * Overload of MpiRecv for CanonicalTensor
 *
 * @param aSendingRank  The rank to receive the CanonicalTensor from.
 **/
template<class T>
void CanonicalTensor<T>::MpiRecv
   (  In aSendingRank
   )
{
   CanonicalTensor<T>::Deallocate();

   // recv basetensor stuff
   BaseTensor<T>::MpiRecvDimensions(aSendingRank);
   
   // recv canonicaltensor stuff
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   MPI_Status status;

   midas::mpi::detail::WRAP_Recv(&mRank, 1, MPI_UNSIGNED, aSendingRank, 0, MPI_COMM_WORLD, &status);

   CanonicalTensor<T>::Allocate();

   const auto& extents = this->GetDims();
   for(In i = 0; i < this->NDim(); ++i)
   {
      int nelem = extents[i] * mRank;
      midas::mpi::detail::WRAP_Recv(mModeMatrices[i], nelem, midas::mpi::DataTypeTrait<T>::Get(), aSendingRank, 0, MPI_COMM_WORLD, &status);
   }
}

/**
 * Bcast CanonicalTensor
 *
 * @param aRoot      Root rank to send from
 **/
template
   <  class T
   >
void CanonicalTensor<T>::MpiBcast
   (  In aRoot
   )
{
   auto dims_old = this->GetDims();

   // Bcast dimensions
   BaseTensor<T>::MpiBcastDimensions(aRoot);

   midas::mpi::detail::WRAP_Bcast(&mRank, 1, midas::mpi::DataTypeTrait<unsigned int>::Get(), aRoot, MPI_COMM_WORLD);

   // Re-allocate data if we have different dims
   if (  dims_old != this->mDims
      )
   {
      if (  midas::mpi::GlobalRank() == aRoot
         )
      {
         MIDASERROR("MPI reallocation in root! This should not happen!");
      }
      // Deallocate
      // Niels: We need to do it this way, since the dims have changed after the first Bcast
      if (  mModeMatrices
         )
      {
         auto order = dims_old.size();
         const auto& extents = dims_old;
         for(unsigned int idim = 0; idim < order; ++idim)
         {
            if (  mModeMatrices[idim]
               )
            {
               auto size = mRank * extents[idim];
               Base::Allocator_Tp::Deallocate(mModeMatrices[idim], size);
               mModeMatrices[idim] = nullptr;
            }
         }
         Base::Allocator_Tpp::Deallocate(mModeMatrices, order);
         mModeMatrices = nullptr;
      }

      // Re-allocate
      CanonicalTensor<T>::Allocate();
   }

   // Bcast data
   const auto& extents = this->GetDims();
   for(In i = 0; i < this->NDim(); ++i)
   {
      int nelem = extents[i] * mRank;
      midas::mpi::detail::WRAP_Bcast(mModeMatrices[i], nelem, midas::mpi::DataTypeTrait<T>::Get(), aRoot, MPI_COMM_WORLD);
   }
   
   // Check that we received the correct tensor
   if constexpr (MPI_DEBUG)
   {
      auto norm = this->Norm();
      midas::mpi::detail::WRAP_Bcast(&norm, 1, midas::mpi::DataTypeTrait<T>::Get(), aRoot, MPI_COMM_WORLD);

      if(norm != this->Norm())
      {
         MIDASERROR("Did not receive correct CanonicalTensor.");
      }
   }
}

#endif /* VAR_MPI */
