/**
************************************************************************
* 
* @file    CpAsdDecomposer.cc
*
* @date    25-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of CpAsdDecomposer methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/CpAsdDecomposer.h"
#include "tensor/TensorFits.h"
#include "tensor/FitASD.h"

namespace midas
{
namespace tensor
{

//! register CpAsdDecomposer with ConcreteCpDecomposerFactory
detail::ConcreteCpDecomposerRegistration<CpAsdDecomposer> registerCpAsdDecomposer("ASD");

/** 
 * Check if tensor is decomposed by this decomposer.
 * @param aTensor                    Tensor to check.
 * @return                           Returns whether tensor will be decomposed.
 **/
bool CpAsdDecomposer::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool type_ok  = ( (aTensor.Type() == BaseTensor<Nb>::typeID::SIMPLE) 
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::CANONICAL)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::SUM)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::DIRPROD)
                   );
   return type_ok;
}

/**
 * Decompose NiceTensor using CP-ASD algorithm.
 * @param aTensor        Tensor to decompose.
 * @param aNorm2         Squared norm of the target tensor
 * @param aFitTensor     Fitting tensor and starting guess
 * @param aThresh        The requested accurcy
 * @return               FitReport of the decomposition
 **/
FitReport<Nb> CpAsdDecomposer::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   // Set threshold if the driver does not require a specific accuracy
   Nb thresh = aThresh > C_0 ? aThresh : this->mCpAsdThreshold;
   Nb check_thresh = thresh * this->mCpAsdRelativeCheckThresh;
   if (  this->mCpAsdRelativeThreshold > C_0 )
   {
      thresh *= this->mCpAsdRelativeThreshold;
   }

   CpAsdInput<Nb> input(mInfo);
   input.SetMaxerr(thresh);
   input.SetCheckThresh(check_thresh);

   auto fit_report   =  this->mPivotised
                     ?  FitCPPASD( aTensor, aNorm2, aFitTensor, input )
                     :  FitCPASD ( aTensor, aNorm2, aFitTensor, input );

   return fit_report;
}


/**
 * Get type of decomposer as a std::string.
 * @return          String with type.
 **/
std::string CpAsdDecomposer::TypeImpl
   (
   )  const
{
   return std::string("AsdDecomposer");
}



/**
 * Constructor from TensorDecompInfo.
 * @param aInfo                TensorDecompInfo to construct from.
 **/
CpAsdDecomposer::CpAsdDecomposer
   ( const TensorDecompInfo& aInfo
   )
   :  mPivotised                 (const_cast<TensorDecompInfo&>(aInfo)["PIVOTISEDCPASD"].get<bool>()) 
   ,  mCpAsdThreshold            (const_cast<TensorDecompInfo&>(aInfo)["CPASDTHRESHOLD"].get<Nb>())
   ,  mCpAsdRelativeThreshold    (const_cast<TensorDecompInfo&>(aInfo)["CPASDRELATIVETHRESHOLD"].get<Nb>())
   ,  mCpAsdRelativeCheckThresh  (const_cast<TensorDecompInfo&>(aInfo)["CHECKTHRESH"].get<Nb>())
   ,  mInfo                      (aInfo)
{
}

} /* namespace tensor */
} /* namespace midas */
