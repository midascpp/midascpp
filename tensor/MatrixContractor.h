#ifndef MATRIX_CONTRACTOR_H_INCLUDED
#define MATRIX_CONTRACTOR_H_INCLUDED

///> declaration
#include "tensor/MatrixContractor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
///> implementation
#include "tensor/MatrixContractor_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* MATRIX_CONTRACTOR_H_INCLUDED */
