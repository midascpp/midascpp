/**
************************************************************************
* 
* @file    CpGuesser_Impl.h
*
* @date    01-12-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementations for CpGuesser class.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef CPGUESSER_IMPL_H_INCLUDED
#define CPGUESSER_IMPL_H_INCLUDED

#include "tensor/FitALS.h"
#include "tensor/TensorFits.h"
#include "tensor/CpGuesser_Decl.h"
#include "util/CallStatisticsHandler.h"
#include "tensor/TensorDecomposer.h"

#include <string>

template<class T>
NiceTensor<T> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);

/**
 * Constructor from TensorDecompInfo.
 *
 * @param aInfo  TensorDecompInfo to construct from.
 **/
template
   <  class T
   >
CpGuesser<T>::CpGuesser
   (  const TensorDecompInfo& aInfo
   )
   :  mStartGuesser              (const_cast<TensorDecompInfo&>(aInfo)["GUESSER"].get<guessType>())
   ,  mGuessUpdater              (const_cast<TensorDecompInfo&>(aInfo)["GUESSINCREMENTER"].get<updateType>())  
   ,  mScaleGuessNorm            (const_cast<TensorDecompInfo&>(aInfo)["SCALEGUESSNORM"].get<bool>())
   ,  mResidualOverlapScaling    (const_cast<TensorDecompInfo&>(aInfo)["RESIDUALOVERLAPSCALING"].get<bool>())
   ,  mUseResidualNorm           (const_cast<TensorDecompInfo&>(aInfo)["RESIDUALNORMSCALING"].get<bool>())
   ,  mFitDiffRelativeThreshold  (const_cast<TensorDecompInfo&>(aInfo)["FITDIFFRELATIVETHRESHOLD"].get<Nb>())
   ,  mDefaultResidualFitter     (const_cast<TensorDecompInfo&>(aInfo)["DEFAULTRESIDUALFITTER"].get<bool>())
   ,  mIoLevel                   (const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].get<In>())
   ,  mInfo                      (aInfo)
{
}


/** 
 * Make a starting guess
 *
 * @param aTensor      Target tensor.
 * @param aNorm        Norm of target tensor.
 * @param aFitTensor   Fitting tensor.
 * @param aRank        Rank of start guess
 **/
template
   <  class T
   >
void CpGuesser<T>::MakeStartGuess
   (  const NiceTensor<T>& aTensor
   ,  const T aNorm
   ,  NiceTensor<T>& aFitTensor
   ,  const In aRank
   )  const
{
   switch(this->mStartGuesser)
   {
      case guessType::RANDOM:
      {
         aFitTensor = this->RandomGuess( aTensor, aNorm, aRank );
         break;
      }
      case guessType::CONSTVAL:
      {
         aFitTensor = this->ConstValueGuess( aTensor, aNorm, aRank );
         break;
      }
      case guessType::SVD:
      {
         aFitTensor = this->SvdGuess( aTensor, aNorm, aRank );
         break;
      }
      case guessType::SCA:
      {
         aFitTensor = this->ScaGuess( aTensor, aNorm, aRank );
         break;
      }
      default:
      {
         MIDASERROR("CpGuesser: No match for guesser.");
         break;
      }
   }
}


/**
 * Update the guess for a given rank
 *
 * @param aFitTensor       Current guess
 * @param aTarget          The tensor to decompose
 * @param aRank            New rank
 * @param aTargetNorm      Norm of target tensor
 * @param aResidualNorm    Difference norm of aFitTensor and aTarget
 **/
template
   <  class T
   >
void CpGuesser<T>::UpdateGuess
   (  NiceTensor<T>& aFitTensor
   ,  const NiceTensor<T>& aTarget
   ,  const In aRank
   ,  const T aTargetNorm
   ,  const T aResidualNorm
   )  const
{
   // Calculate rank to add
   auto add_rank = aRank - static_cast<CanonicalTensor<T>*>(aFitTensor.GetTensor())->GetRank();

   if (  add_rank == 0
      )
   {
      MidasWarning("CpGuesser::UpdateGuess: FitTensor has requested rank already!");
      return;
   }
   else if  (  add_rank < 0
            )
   {
      MIDASERROR("CpGuesser::UpdateGuess: FitTensor has higher rank than requested!");
      return;
   }

   // Choose scaling factor
   T scaling = this->mUseResidualNorm ? aResidualNorm : aTargetNorm;

   switch(this->mGuessUpdater)
   {
      case updateType::REPLACE:
      {
         this->MakeStartGuess( aTarget, aTargetNorm, aFitTensor, aRank );
         break;
      }
      case updateType::RANDOM:
      {
         aFitTensor += this->RandomUpdate( aTarget, aFitTensor, scaling, add_rank );
         break;
      }
      case updateType::CONSTVAL:
      {
         aFitTensor += this->ConstValueUpdate( aTarget, aFitTensor, scaling, add_rank );
         break;
      }
      case updateType::SCA:
      {
         aFitTensor += this->ScaUpdate( aTarget, aFitTensor, scaling, add_rank );
         break;
      }
      case updateType::FITDIFF:
      {
         aFitTensor += this->FitResidual( aTarget, aFitTensor, aResidualNorm, scaling, add_rank );
         break;
      }
      case updateType::STEPWISEFIT:
      {
         aFitTensor += this->StepwiseResidualFit( aTarget, aFitTensor, aResidualNorm, aTargetNorm, add_rank );
         break;
      }
      default:
      {
         MIDASERROR("CpGuesser: No match for guess incrementer.");
      }
   }
}


/**
 * Improve the guess by fitting each rank-1 tensor
 * to the aTarget - "all other rank-1 tensors in aFitTensor".
 * Adapted from [Espig, Hackbusch: Regularized Newton CP decomp](https://link.springer.com/article/10.1007%2Fs00211-012-0465-9)
 *
 * @param aFitTensor    The guess
 * @param aTarget       The target tensor
 **/
template
   <  class T
   >
void CpGuesser<T>::ImproveGuess
   (  NiceTensor<T>& aFitTensor
   ,  const NiceTensor<T>& aTarget
   )  const
{
   MIDASERROR("Not implemented!");
}


/**
 * Create a random guess for CP decomposition.
 *
 * @param aTensor    Target tensor
 * @param aNorm      Norm of target tensor
 * @param aRank      Rank of the guess
 *
 * @return           Random guess
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::RandomGuess
   (  const NiceTensor<T>& aTensor
   ,  const T aNorm
   ,  const In aRank
   )  const
{
   auto result = random_canonical_tensor<T>( aTensor.GetDims(), aRank );

   if (  this->mScaleGuessNorm   )
   {
      result.Scale( aNorm );
   }

   return result;
}

/**
 * Create guess with all elements equal to a constant value.
 *
 * @param aTensor    Target tensor
 * @param aNorm      Norm of target tensor
 * @param aRank      Rank of guess
 *
 * @return           Constant-valued guess
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::ConstValueGuess
   (  const NiceTensor<T>& aTensor
   ,  const T aNorm
   ,  const In aRank
   )  const
{
   if (  aRank > I_1 )
   {
      MidasWarning("Constant-valued guess with rank > 1 introduces linear dependency!");
   }

   auto value = static_cast<T>(1.) / std::sqrt( static_cast<T>(aTensor.TotalSize()) );
   auto result = NiceTensor<T>( const_value_cptensor<T>(aTensor.GetDims(), value, aRank) );

   if (  this->mScaleGuessNorm   )
   {
      result.Scale( aNorm );
   }

   return result;
}

/**
 * Create SVD-based start guess for SimpleTensor.
 * Calls ConstValueGuess for other types.
 *
 * @param aTensor    The target tensor
 * @param aNorm      Norm of target tensor
 * @param aRank      Rank of the result
 *
 * @return           SVD guess
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::SvdGuess
   (  const NiceTensor<T>& aTensor
   ,  const T aNorm
   ,  const In aRank
   )  const
{
   switch(  aTensor.Type() )
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         auto* simple = static_cast<SimpleTensor<T>*>(aTensor.GetTensor());
         return NiceCanonicalTensor<T>( simple->CanonicalSVDGuess(aRank) );
      }
      default:
      {
         return this->RandomGuess( aTensor, aNorm, aRank );
      }
   }
}


/**
 * Create SCA start guess for CanonicalTensor.
 * Calls ConstValueGuess for other types.
 *
 * @param aTensor    The target tensor
 * @param aNorm      Norm of target tensor
 * @param aRank      Rank of the result
 *
 * @return           SCA guess
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::ScaGuess
   (  const NiceTensor<T>& aTensor
   ,  const T aNorm
   ,  const In aRank
   )  const
{
   return aTensor.SuccessiveCrossApproximation( aRank );
}

/**
 * Update guess for CP decomposition using random rank-1 tensors.
 *
 * @param aTensor    Target tensor
 * @param aFitTensor The current guess
 * @param aScaling   Scaling factor for generated guess
 * @param aRank      Rank of the guess
 *
 * @return           Random guess update
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::RandomUpdate
   (  const NiceTensor<T>& aTensor
   ,  const NiceTensor<T>& aFitTensor
   ,  const T aScaling
   ,  const In aRank
   )  const
{
   auto result = random_canonical_tensor<T>( aTensor.GetDims(), aRank );

   if (  this->mScaleGuessNorm   )
   {
      T scaling = static_cast<T>(1.);

      if (  this->mResidualOverlapScaling )
      {
         auto new_norm2 = result.Norm2();
         auto residual_overlap   =  dot_product( result, aTensor )
                                 -  dot_product( result, aFitTensor );

         scaling = residual_overlap / new_norm2;
      }
      else
      {
         scaling = aScaling;
      }

      result.Scale( scaling );
   }

   return result;
}

/**
 * Create guess update with constant value.
 *
 * @param aTensor    Target tensor
 * @param aFitTensor The current guess
 * @param aScaling   Scaling factor for update.
 * @param aRank      Rank of guess
 *
 * @return           Constant-valued guess update
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::ConstValueUpdate
   (  const NiceTensor<T>& aTensor
   ,  const NiceTensor<T>& aFitTensor
   ,  const T aScaling
   ,  const In aRank
   )  const
{
   if (  aRank > I_1 )
   {
      MidasWarning("Constant-valued guess update with rank > 1 introduces linear dependency!");
   }

   auto value = static_cast<T>(1.) / std::sqrt( static_cast<T>(aTensor.TotalSize()) );
   auto result = NiceTensor<T>( const_value_cptensor<T>(aTensor.GetDims(), value, aRank) );

   if (  this->mScaleGuessNorm   )
   {
      T scaling = static_cast<T>(1.);

      if (  this->mResidualOverlapScaling )
      {
         auto new_norm2 = result.Norm2();
         auto residual_overlap   =  dot_product( result, aTensor )
                                 -  dot_product( result, aFitTensor );

         scaling = residual_overlap / new_norm2;
      }
      else
      {
         scaling = aScaling;
      }

      result.Scale( scaling );
   }

   return result;
}

/**
 * Create SCA guess update for CanonicalTensor.
 *
 * @param aTensor    The target tensor
 * @param aFitTensor The fitting tensor
 * @param aScaling   Scaling factor
 * @param aRank      Rank of the result
 *
 * @return           SCA guess
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::ScaUpdate
   (  const NiceTensor<T>& aTensor
   ,  const NiceTensor<T>& aFitTensor
   ,  const T aScaling
   ,  const In aRank
   )  const
{
   // Construct residual tensor
   auto diff = this->ConstructResidual(aTensor, aFitTensor);

   // Approximate residual
   auto result = diff.SuccessiveCrossApproximation( aRank );

   // Calculate scaling factor (Espig & Hackbusch)
   auto new_norm2 = result.Norm2();
   auto residual_overlap   =  dot_product( diff, result );
   T scaling = residual_overlap / new_norm2;

   result.Scale( scaling );

   return result;
}

/**
 * Fit the residual to a given rank.
 * This is used to update the CP guess.
 *
 * @param aTensor       The target tensor
 * @param aFitTensor    The current guess
 * @param aDiffNorm     The difference norm between aTensor and aFitTensor
 * @param aScaling      Scaling factor for default update
 * @param aRank         The rank of the result
 *
 * @result  The fitted residual
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::FitResidual
   (  const NiceTensor<T>& aTensor
   ,  const NiceTensor<T>& aFitTensor
   ,  const T aDiffNorm
   ,  const T aScaling
   ,  const In aRank
   )  const
{
//   LOGCALL("calls");

   if (  this->mIoLevel >= I_10  )
   {
      Mout << " ## CpGuesser::FitResidual to rank " << aRank << " ##" << std::endl;
   }

   // Construct residual
   auto diff = this->ConstructResidual(aTensor, aFitTensor);

   const auto diff_norm = aDiffNorm;
   const auto diff_norm2 = diff_norm*diff_norm;

   // Make start guess
   NiceTensor<T> result;
   this->MakeStartGuess( diff, diff_norm, result, aRank );

   // Fit residual
   auto fit_report = this->FitResidualTensor( diff, diff_norm2, result );

   if (  fit_report.error > diff_norm
      || static_cast<CanonicalTensor<T>*>(result.GetTensor())->CheckModeMatrices()
      )
   {
      MidasWarning("Poor fit of residual tensor for guess update. Add random vectors!");
      Mout  << " Err:      " << fit_report.error << "\n"
            << " Thresh:   " << diff_norm << std::endl;

      result = this->RandomUpdate( aTensor, aFitTensor, aScaling, aRank );
   }

   if (  this->mIoLevel >= I_10  )
   {
      Mout << " ## End CpGuesser::FitResidual ##\n" << std::endl;
   }

   return result;
}


/**
 * Fit the residual to a given rank by subsequently fitting rank-1 tensors.
 * This is used to update the CP guess.
 *
 * @param aTarget       The target tensor
 * @param aFitTensor    The current guess
 * @param aDiffNorm     The difference norm between aTensor and aFitTensor
 * @param aTargetNorm   The norm of the target tensor
 * @param aRank         The rank of the result
 *
 * @result  The fitted residual
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::StepwiseResidualFit
   (  const NiceTensor<T>& aTarget
   ,  const NiceTensor<T>& aFitTensor
   ,  const T aDiffNorm
   ,  const T aTargetNorm
   ,  const In aRank
   )  const
{
//   LOGCALL("calls");

   if (  this->mIoLevel >= I_10  )
   {
      Mout << " ## CpGuesser::StepwiseResidualFit to rank " << aRank << " ##" << std::endl;
   }

   // Construct residual
   auto diff = this->ConstructResidual(aTarget, aFitTensor);

   // Declare result and rank-1 fit
   NiceTensor<T> update( new CanonicalTensor<T>(aTarget.GetDims(), 0) );
   NiceTensor<T> rank_one_fit;

   // Declare and initialize norms
   auto diff_norm = aDiffNorm;
   auto diff_norm2 = diff_norm*diff_norm;
   auto target_norm2 = aTargetNorm*aTargetNorm;
   T fit_norm2;

   for(In r=I_1; r<=aRank; ++r)
   {
      // Update norms
      if (  r != I_1 )
      {
         fit_norm2 = aFitTensor.Norm2();
         diff_norm = diff_norm_new( aTarget.GetTensor(), target_norm2, aFitTensor.GetTensor(), fit_norm2 );
         diff_norm2 = diff_norm*diff_norm;
      }

      // Make start guess
      this->MakeStartGuess( diff, diff_norm, rank_one_fit, I_1 );

      // Fit rank-1 tensor
      auto fit_report = this->FitResidualTensor( diff, diff_norm2, rank_one_fit );
      
      if (  fit_report.error > diff_norm
         || static_cast<CanonicalTensor<T>*>(rank_one_fit.GetTensor())->CheckModeMatrices()
         )
      {
         MidasWarning("Poor fit of residual tensor for guess update. Add random vectors!");
         Mout  << " Err:      " << fit_report.error << "\n"
               << " Thresh:   " << diff_norm << std::endl;

         rank_one_fit = this->RandomUpdate( aTarget, aFitTensor, diff_norm, I_1 );
      }

      // Update tensors (diff needs no update at the last rank)
      update += rank_one_fit;
      if (  r != aRank  )
      {
         diff -= rank_one_fit;
         diff_norm2 = diff.Norm2();
         diff_norm = std::sqrt(diff_norm2);
      }
   }

   if (  this->mIoLevel >= I_10  )
   {
      Mout << " ## End CpGuesser::StepwiseResidualFit ##\n" << std::endl;
   }

   return update;
}


/**
 * Construct the residual (target - fit)
 *
 * @param aTarget       The target tensor
 * @param aFit          The fit tensor
 * @return              Residual
 **/
template
   <  class T
   >
NiceTensor<T> CpGuesser<T>::ConstructResidual
   (  const NiceTensor<T>& aTarget
   ,  const NiceTensor<T>& aFit
   )  const
{
   NiceTensor<T> diff;

   switch(aTarget.Type())
   {
      // Fit residual for canonical
      case BaseTensor<T>::typeID::CANONICAL:
      {
//         LOGCALL("Canonical residual");
         // Create residual
         diff = NiceTensor<T>( new TensorSum<T>(aTarget.GetDims()) );
         diff += aTarget;
         diff -= aFit;
         break;
      }
      case BaseTensor<T>::typeID::DIRPROD:
      {
//         LOGCALL("DirProd residual");
         // Create residual as TensorSum
         diff = NiceTensor<T>( new TensorSum<T>(aTarget.GetDims()) );
         diff += aTarget;
         diff -= aFit;
         break;
      }
      case BaseTensor<T>::typeID::SUM:
      {
//         LOGCALL("Sum residual");
         diff = aTarget;
         diff -= aFit;
         break;
      }
      case BaseTensor<T>::typeID::SIMPLE:
      {
//         LOGCALL("Simple residual");
         diff = aTarget;
         diff -= aFit;
         break;
      }
      default:
      {
         MIDASERROR("CpGuesser::ConstructResidual not implemented for "+aTarget.ShowType());
      }
   }

   return diff;
}


/**
 * Fit residual tensor to CP format.
 *
 * @param aResidual        The residual tensor
 * @param aNorm2           Squared residual norm
 * @param aGuess           Start guess for the fitting
 * @return                 FitReport
 **/
template
   <  class T
   >
FitReport<T> CpGuesser<T>::FitResidualTensor
   (  const NiceTensor<T>& aResidual
   ,  T aNorm2
   ,  NiceTensor<T>& aGuess
   )  const
{
   // Determine threshold relative to norm of residual tensor
   auto thresh = std::sqrt(aNorm2)*this->mFitDiffRelativeThreshold;

   FitReport<T> fit_report(0., std::sqrt(aNorm2), 0);

   if (  this->mDefaultResidualFitter  )     // Use hard-coded default
   {
      // Set relative threshold
      thresh *= C_I_10_2;

      CpAlsInput<T> input;
      input.SetMaxerr(thresh);
      input.SetMaxiter(5);
      input.SetRcond(1.e-12);
      input.SetIoLevel(this->mIoLevel);

      fit_report = FitCPALS(aResidual, aNorm2, aGuess, input);
   }
   else
   {
      const auto fitter_type = this->mInfo.at("FITTER").template get<std::string>();

      if (  fitter_type == "NCG" )
      {
         auto rel_thresh = this->mInfo.at("CPNCGRELATIVETHRESHOLD").template get<Nb>();
         thresh *= (rel_thresh != static_cast<Nb>(0.)) ? rel_thresh : C_I_10_2;

         auto input = CpNCGInput<T>(this->mInfo);
         input.SetMaxerr(thresh);
         input.SetIoLevel(this->mIoLevel);
         
         fit_report = FitCPNCG(aResidual, aNorm2, aGuess, input);
      }
      else if  (  fitter_type == "ASD" )
      {
         auto rel_thresh = this->mInfo.at("CPASDRELATIVETHRESHOLD").template get<Nb>();
         thresh *= (rel_thresh != static_cast<Nb>(0.)) ? rel_thresh : C_I_10_2;

         auto input = CpAsdInput<T>(this->mInfo);
         input.SetMaxerr(thresh);
         input.SetIoLevel(this->mIoLevel);
         
         fit_report = FitCPPASD(aResidual, aNorm2, aGuess, input);
      }
      else  // Use CP-ALS as default
      {
         auto rel_thresh = this->mInfo.at("CPALSRELATIVETHRESHOLD").template get<Nb>();
         thresh *= (rel_thresh != static_cast<Nb>(0.)) ? rel_thresh : C_I_10_2;

         auto input = CpAlsInput<T>(this->mInfo);
         input.SetMaxerr(thresh);
         input.SetIoLevel(this->mIoLevel);
         
         fit_report = FitCPALS(aResidual, aNorm2, aGuess, input);
      }
   }

   return fit_report;
}


#endif /* CPGUESSER_IMPL_H_INCLUDED */
