/**
 ************************************************************************
 * 
 * @file                CanonicalTensor_SolverWrapper.h
 *
 * Created:             25.11.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               Enables optimization of CanonicalTensor's with
 *                      Ian's tensors in .
 * 
 * Last modified: Tue Dec 16, 2014  15:02
 * 
 * Detailed Description: Provides interfaces to +, *, and the target 
 *                       function which implements () and first_derivative
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */
#ifndef CANONICALTENSOR_SOLVERWRAPPER_H_INCLUDED
#define CANONICALTENSOR_SOLVERWRAPPER_H_INCLUDED
#include <iostream>

#include"CanonicalTensor.h"
#include "../solver/function_wrapper.h"

template <class T>
class CanonicalTensor_SolverWrapper;

template <class T>
T dot(const CanonicalTensor_SolverWrapper<T>&
     ,const CanonicalTensor_SolverWrapper<T>&);

template <class T>
T norm(const CanonicalTensor_SolverWrapper<T>&);

template <class T>
CanonicalTensor_SolverWrapper<T> operator*(const T,
                                           const CanonicalTensor_SolverWrapper<T>&);

template <class T>
bool converged(const CanonicalTensor_SolverWrapper<T>&, const T);

template <class T>
std::ostream& operator<<(std::ostream&, const CanonicalTensor_SolverWrapper<T>&);

/// Wraps CanonicalTensor to use in the solvers in `solver`, see e.g. CanonicalTensor<T>::FitSteepestDescent().
/*! The methods provided in `solver` can all be summarized as
 *
 *     Find the \f$x\f$ such that \f$f(x)\f$ is minimized.
 *
 * In our case, the \f$f(x)\f$ is given by
 *
 * \f[
 *     f(x)=||T-x||
 * \f]
 *
 * where \f$T\f$ is some target tensor.
 *
 * CanonicalTensor_SolverWrapper enables using CanonicalTensor as \f$x\f$. The
 * function \f$f(x)\f$ is enabled by CTEvaluator.
 *
 * The methods in solver take an argument `x` whose type must implement the following interface:
 *
 *                                                                  \cond 
 * THIS TABLE IS VERY WIDE. Expand window or check documentation
 * if you want to read it properly.
 *                                                                  \endcond
 *
 * <table>
 *   <tr><td>`F y(x)`        </td><td> Copy constructor.      </td><td> CanonicalTensor_SolverWrapper(const CanonicalTensor_SolverWrapper<T>&)                  </td></tr>
 *   <tr><td>`y=x`           </td><td> Copy assignment.       </td><td> operator+(const CanonicalTensor_SolverWrapper<T>&) const                                </td></tr>
 *   <tr><td>`x+y`           </td><td> Addition               </td><td> operator-(const CanonicalTensor_SolverWrapper<T>&) const                                </td></tr>
 *   <tr><td>`x-y`           </td><td> Subtraction            </td><td> operator*=(const T)                                                                     </td></tr>
 *   <tr><td>`x*=k`          </td><td> Scale with factor.     </td><td> operator*(const T) const                                                                </td></tr>
 *   <tr><td>`x*k`           </td><td> Scale with factor.     </td><td> operator-() const                                                                       </td></tr>
 *   <tr><td>`-x`            </td><td> Unary minus.           </td><td> operator=(const CanonicalTensor_SolverWrapper&)                                         </td></tr>
 *   <tr><td>`dot(x, y)`     </td><td> Dot product.           </td><td> dot<>(const CanonicalTensor_SolverWrapper<T>&, const CanonicalTensor_SolverWrapper<T>&) </td></tr>
 *   <tr><td>`norm(x)`       </td><td> Norm.                  </td><td> norm<>(const CanonicalTensor_SolverWrapper<T>&)                                         </td></tr>
 *   <tr><td>`converged(x,t)`</td><td> Convergence (0) check. </td><td> converged<>(const CanonicalTensor_SolverWrapper<T>&, const T)                           </td></tr>
 * </table>   
 *
 * IMPORTANT NOTE: The overriden methods DO NOT correspond to the usual tensor
 * operations.  As is usual with multi-dimensional optimizations, each
 * parameter to be optimized (i.e. each and every element of the mode matrices)
 * is considered another component of the multi-dimensional variable to be
 * optimized.  For example, additions correspond to update each elements of
 * each of the mode matrices of a tensor with a different value. Likewise,
 * scaling with a constant multiplies all elements of the mode matrices, etc.
 *
 */

template <class T>
class CanonicalTensor_SolverWrapper
{
   public:
      CanonicalTensor<T> mTensor;

   public:
      CanonicalTensor_SolverWrapper(const CanonicalTensor<T>&);

      CanonicalTensor_SolverWrapper(const CanonicalTensor_SolverWrapper<T>&);

      CanonicalTensor_SolverWrapper operator+(const CanonicalTensor_SolverWrapper<T>&) const;

      CanonicalTensor_SolverWrapper operator-(const CanonicalTensor_SolverWrapper<T>&) const;

      CanonicalTensor_SolverWrapper& operator*=(const T);

      CanonicalTensor_SolverWrapper operator*(const T) const;

      CanonicalTensor_SolverWrapper operator-() const;

      CanonicalTensor_SolverWrapper& operator=(const CanonicalTensor_SolverWrapper&);

      friend T dot<>(const CanonicalTensor_SolverWrapper<T>&
                    ,const CanonicalTensor_SolverWrapper<T>&);

      friend T norm<>(const CanonicalTensor_SolverWrapper<T>&);

      friend bool converged<>(const CanonicalTensor_SolverWrapper<T>&, const T);

      friend std::ostream& operator<< <>(std::ostream&, const CanonicalTensor_SolverWrapper&);
};

//! Wrapper to calculate \f$||T-x||\f$ and \f$\nabla ||T-x||\f$ for a target tensor \f$T\f$ and an arbitrary tensor \f$x\f$. 
template <class U, class T>
class CTEvaluator
{
   private:
      //! The target tensor we are fitting to.
   public: 
      const U& mTarget;
      const T& mTargetNorm2;
      mutable int i = 0;

   public:
      //! Constructor (wraps a target tensor in a CTEvaluator).
      CTEvaluator
         (  const U& aTarget
         ,  const T& aTargetNorm2
         )
         :  mTarget(aTarget)
         ,  mTargetNorm2(aTargetNorm2)
      {
      }

      //! Evaluate \f$||T-x||\f$
      /*! NOTE: This is the actual tensor difference norm, and it is
       * completely unrelated to norm(const CanonicalTensor_SolverWrapper&).
       */
//      template <class T>
      T operator()(const CanonicalTensor_SolverWrapper<T>& x) const
      {
         auto copy = std::unique_ptr<BaseTensor<T> >(mTarget.Clone());
//         *(copy) -= x.mTensor;
//         auto result = 0.5*copy->Norm2();

//         T diff_norm2 = diff_norm2_new(&x.mTensor, x.mTensor.Norm2(), &this->mTarget, this->mTargetNorm2, this->mTarget.Dot(&x.mTensor), 10);
//         T result = static_cast<T>(0.5)*diff_norm2;
         
         T result = static_cast<T>(0.5)*safe_diff_norm2(&x.mTensor, &this->mTarget);

         return result;
      }

      //! Evaluate \f$\nabla||T-x||\f$
//      template <class T>
      void first_deriv(const CanonicalTensor_SolverWrapper<T>& x,
                             CanonicalTensor_SolverWrapper<T>& p) const
      {
         p = CanonicalTensor_SolverWrapper<T>(x.mTensor.Gradient(mTarget));
         return;
      }
};

#endif /* CANONICALTENSOR_SOLVERWRAPPER_H_INCLUDED */
