#ifndef TENSORSUM_H_INCLUDED
#define TENSORSUM_H_INCLUDED

// definition
#include "tensor/TensorSum_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// implementation
#include "tensor/TensorSum_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TENSORSUM_H_INCLUDED */
