#include "tensor/TensorDecomposer.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorDirectProduct.h"
#include "tensor/TensorSum.h"
#include "util/CallStatisticsHandler.h"

#include <iostream>
#include <cassert>

namespace midas
{
namespace tensor
{
namespace detail
{

/**
 * Construct TensorDecompDriverBase from TensorDecompInfo.
 *
 * @param aInfo      TensorDecompInfo
 **/
TensorDecompDriverBase::TensorDecompDriverBase
   (  const TensorDecompInfo& aInfo
   )
   :  mPreprocessor
         (  ConcreteCpPreprocessorBase::Factory(aInfo)
         )
   ,  mDecomposeCanonical
         (  const_cast<TensorDecompInfo&>(aInfo)["DECOMPOSECANONICAL"].template get<bool>()
         )
   ,  mDecomposeDirProd
         (  const_cast<TensorDecompInfo&>(aInfo)["DECOMPOSEDIRPROD"].template get<bool>()
         )
   ,  mDecomposeSum
         (  const_cast<TensorDecompInfo&>(aInfo)["DECOMPOSESUM"].template get<bool>()
         )
   ,  mCpFitter
         (  ConcreteCpDecomposerBase::Factory(aInfo)
         )
   ,  mGuesser
         (  const_cast<TensorDecompInfo&>(aInfo)["DRIVER"].template get<std::string>() == "CPID"   // CP-ID does not need a guesser
         ?  nullptr
         :  std::make_unique<CpGuesser<Nb>>(aInfo)
         )
   ,  mInfo
         (  aInfo
         )
   ,  mDecompToOrder
         (  const_cast<TensorDecompInfo&>(aInfo)["DECOMPTOORDER"].template get<In>()
         )
   ,  mIoLevel
         (  const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].template get<In>()
         )
   ,  mMatrixSvdRelThresh
         (  const_cast<TensorDecompInfo&>(aInfo)["MATRIXSVDRELTHRESH"].template get<Nb>()
         )
   ,  mFullRankSvd
         (  const_cast<TensorDecompInfo&>(aInfo)["FULLRANKSVD"].template get<bool>()
         )
   ,  mCheckSvd
         (  const_cast<TensorDecompInfo&>(aInfo)["CHECKSVD"].template get<bool>()
         )
   ,  mEffectiveLowOrderSvd
         (  const_cast<TensorDecompInfo&>(aInfo)["EFFECTIVELOWORDERSVD"].template get<bool>()
         )
{
}


/**
 * Fit vectors and matrices (default implementation).
 *
 * @param aTensor       Target tensor
 * @param aFitTensor    Fitting tensor
 * @param aThresh       Decomposition threshold
 * @return              True if tensor has been decomposed
 **/
bool TensorDecompDriverBase::DecomposeLowOrderTensorsImpl
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   //LOGCALL("calls");
   auto thresh =  aThresh < C_0
               ?  this->GetThreshold()
               :  aThresh;

   assert(libmda::numeric::float_pos(thresh));

   auto ndim = aTensor.NDim();
   const auto& dims = aTensor.GetDims();

   // Dump vectors to rank-1 CanonicalTensor
   if (  ndim == 1
      || aTensor.MaxExtent() == aTensor.TotalSize()
      )
   {
      aFitTensor = NiceTensor<Nb>( aTensor.GetTensor()->DumpVectorToCanonical() );
      return true;
   }
   // Do SVD rank reduction on matrices
   else if  (  (  ndim == 2
               || (  this->mEffectiveLowOrderSvd
                  && std::count_if(dims.begin(), dims.end(), [](unsigned i){ return (i != 1); }) == 2
                  )
               )
            && (  this->mMatrixSvdRelThresh > C_0
               || this->mFullRankSvd
               )
            )
   {
      if (  this->mFullRankSvd
         )
      {
         unsigned min_extent = aTensor.MaxExtent();
         for(const auto& d : dims)
         {
            if (  d > 1
               )
            {
               min_extent = std::min(min_extent, d);
            }
         }

         aFitTensor = aTensor.GetTensor()->SvdRankReduction(thresh*this->mMatrixSvdRelThresh, min_extent);
      }
      else
      {
         aFitTensor = aTensor.GetTensor()->SvdRankReduction(thresh*this->mMatrixSvdRelThresh);
      }

      // CHECK
      if (  this->mCheckSvd
         )
      {
         auto err = diff_norm_new( *aFitTensor.GetTensor(), *aTensor.GetTensor() );
         if (  err > thresh
            )
         {
            Mout  << " Bad SVD Fit of " << aTensor.ShowType() << ":\n"
                  << "    Dims:  " << aTensor.ShowDims() << "\n"
                  << "    Error  = " << err << "\n"
                  << "    Thresh = " << thresh << "\n"
                  << "    SVD rank = " << aFitTensor.StaticCast<CanonicalTensor<Nb>>().GetRank() << "\n"
                  << std::endl;
         }
      }

      return true;
   }
   else
   {
      return false;
   }
}


/**
 * Check that a NiceTensor will be decomposed.
 * @param aTensor                NiceTensor to check.
 * @return                       Return whether the NiceTensor will be decomposed by the TensorDecompDriver obj.
 **/
bool TensorDecompDriverBase::CheckDecomposition
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   // Check that driver is able to decompose aTensor
   bool decompose = this->CheckDecompositionImpl(aTensor);

   // Check that we allow the driver to decompose this type
   switch   (  aTensor.Type()
            )
   {
      case BaseTensor<Nb>::typeID::CANONICAL:
      {
         decompose = decompose && mDecomposeCanonical;
         break;
      }
      case BaseTensor<Nb>::typeID::DIRPROD:
      {
         decompose = decompose && mDecomposeDirProd;
         break;
      }
      case BaseTensor<Nb>::typeID::SUM:
      {
         decompose = decompose && mDecomposeSum;
         break;
      }
      default:
      {
         // Do not change decompose
      }
   }

   // Check that fitter is able to fit the tensor
   if (  this->mCpFitter
      )
   {
      decompose = decompose && this->mCpFitter->CheckDecomposition(aTensor);
   }

   return decompose;
}


/**
 * Decompose a NiceTensor.
 * @param aTensor       NiceTensor to decompose.
 * @param aThresh       The requested accuracy
 * @return              Return result of decompostion.
 **/
NiceTensor<Nb> TensorDecompDriverBase::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aThresh
   )  const
{
   //LOGCALL("calls");

   auto target_rank = this->TargetRank(aTensor);

   if (  this->mIoLevel > I_5
      )
   {
      Mout  << " TensorDecompDriverBase::Decompose " << aTensor.ShowDims() << " " << aTensor.ShowType() << " of rank " << target_rank << std::endl;
   }

   if (  target_rank == 0
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-0 target tensor." << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }

   NiceTensor<Nb> result;
   bool preprocessed = false;
   auto ndim = aTensor.NDim();

   // Preprocess
   if (  this->mPreprocessor
      && this->mPreprocessor->Preprocess(aTensor, result)   
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Preprocessing complete." << std::endl;
      }
      preprocessed = true;
      Nb prep_norm2 = C_0;
      {
      //LOGCALL("preprocessed norm2");
      prep_norm2 = result.Norm2();
      }

      // Screen preprocessed tensor
      if (  this->ScreenImpl(aTensor, prep_norm2, aThresh)
         )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Screen tensor of norm: " << std::sqrt(prep_norm2) << std::endl;
         }
         return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
      }
      else if  (  target_rank == 1
               )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Return rank-1 tensor" << std::endl;
         }
         return this->ConstructCanonicalTarget(aTensor);
      }

      {
      //LOGCALL("decomp preprocessed");
      result = this->DecomposeImpl(result, prep_norm2, aThresh);
      }
   }

   // If the tensor has been preprocessed, perform postprocessing and return
   if (  preprocessed
      && this->mPreprocessor->Postprocess(result)
      )
   {
      // Postprocessing is successful
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Postprocessing complete." << std::endl;
      }
   }
   else
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " No preprocessing performed." << std::endl;
      }

      // Clear result (in case postprocessing has failed)
      result.Clear();

      // Construct mode matrices of tensor sums before norm2 calc
      NiceTensor<Nb> reconstruct_sum;
      bool is_sum = aTensor.Type() == BaseTensor<Nb>::typeID::SUM;
      if (  is_sum
         )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Construct mode matrices of TensorSum." << std::endl;
         }
         reconstruct_sum = NiceTensor<Nb>(aTensor.StaticCast<TensorSum<Nb>>().ConstructModeMatrices());
      }

      auto& target_tensor  =  is_sum
                           ?  reconstruct_sum
                           :  aTensor;

      Nb norm2 = C_0;
      {
      //LOGCALL("norm2");
      norm2 = target_tensor.Norm2();
      }
      // Screen tensors
      if (  this->ScreenImpl(target_tensor, norm2, aThresh)
         )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Screen tensor of norm: " << std::sqrt(norm2) << std::endl;
         }
         return NiceCanonicalTensor<Nb>(target_tensor.GetDims(), 0);
      }
      else if  (  target_rank == 1
               )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Return rank-1 tensor" << std::endl;
         }
         return this->ConstructCanonicalTarget(target_tensor);
      }

      {
      //LOGCALL("decomp");
      result = this->DecomposeImpl(target_tensor, norm2, aThresh);
      }
   }

   return result;
}


/**
 * Decompose a NiceTensor.
 * @param aTensor       NiceTensor to decompose.
 * @param aNorm2        Squared norm of aTensor
 * @param aThresh       The requested accuracy
 * @return              Return result of decompostion.
 **/
NiceTensor<Nb> TensorDecompDriverBase::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   //LOGCALL("calls");

   auto target_rank = this->TargetRank(aTensor);

   if (  this->mIoLevel > I_5
      )
   {
      Mout  << " TensorDecompDriverBase::Decompose " << aTensor.ShowDims() << " " << aTensor.ShowType() << " of rank " << target_rank << std::endl;
   }

   // Return rank-0 tensors
   if (  target_rank == 0
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-0 target tensor." << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }

   // Sanity check
   if (  aNorm2 != aNorm2
      || libmda::numeric::float_neg(aNorm2)
      )
   {
      MIDASERROR("TensorDecompDriverBase::Decompose: aNorm2 = "+std::to_string(aNorm2)+" for "+aTensor.ShowType());
   }

   bool preprocessed = false;

   // Screen before preprocessing if Norm2 is already calculated
   if (  this->ScreenImpl(aTensor, aNorm2, aThresh) )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Screen tensor of norm: " << std::sqrt(aNorm2) << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }
   else if  (  target_rank == 1
            )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-1 tensor" << std::endl;
      }
      return this->ConstructCanonicalTarget(aTensor);
   }

   NiceTensor<Nb> result;
   if (  this->mPreprocessor
      && this->mPreprocessor->Preprocess(aTensor, result)   
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Preprocessing complete." << std::endl;
      }
      // Set preprocessed flag
      preprocessed = true;

      // Calculate new norm, if preprocessor changes norm
      Nb prep_norm2 = aNorm2;
      if (  this->mPreprocessor->ModifiesTargetNorm()
         )
      {
         //LOGCALL("preprocessed norm2");
         prep_norm2 = result.Norm2();
      }

      {
      //LOGCALL("decomp preprocessed");
      result = this->DecomposeImpl(result, prep_norm2, aThresh);
      }
   }

   // If the tensor has been preprocessed, perform postprocessing and return
   if (  preprocessed
      && this->mPreprocessor->Postprocess(result)
      )
   {
      // Postprocessing is successful
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Postprocessing complete." << std::endl;
      }
   }
   else
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " No preprocessing performed." << std::endl;
      }
      // Clear result (in case postprocessing has failed)
      result.Clear();
      {
      //LOGCALL("decomp");
      result = this->DecomposeImpl(aTensor, aNorm2, aThresh);
      }
   }

   return result;
}

/**
 * Decompose a NiceTensor.
 * @param aTensor       NiceTensor to decompose.
 * @param aGuess        NiceTensor used as initial guess
 * @param aThresh       The requested accuracy
 * @return              Return result of decompostion.
 **/
NiceTensor<Nb> TensorDecompDriverBase::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   assert(aGuess.Type() == BaseTensor<Nb>::typeID::CANONICAL);

   auto target_rank = this->TargetRank(aTensor);

   if (  this->mIoLevel > I_5
      )
   {
      Mout  << " TensorDecompDriverBase::Decompose " << aTensor.ShowDims() << " " << aTensor.ShowType() << " of rank " << target_rank << std::endl;
   }

   // Return rank-0 tensors
   if (  target_rank == 0
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-0 target tensor." << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }
   bool preprocessed = false;
   NiceTensor<Nb> result;
   NiceTensor<Nb> guess;
   auto ndim = aTensor.NDim();
   if (  this->mPreprocessor
      && this->mPreprocessor->Preprocess(aTensor, result, aGuess, guess)   
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Preprocessing complete." << std::endl;
      }
      // Set successful preprocessing flag
      preprocessed = true;

      auto prep_norm2 = result.Norm2();
      if (  this->ScreenImpl(aTensor, prep_norm2, aThresh) )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Screen tensor of norm: " << std::sqrt(prep_norm2) << std::endl;
         }
         return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
      }
      else if  (  target_rank == 1
               )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Return rank-1 tensor" << std::endl;
         }
         return this->ConstructCanonicalTarget(aTensor);
      }

      result = this->DecomposeImpl(result, prep_norm2, guess, aThresh);
   }

   // If the tensor has been preprocessed, perform postprocessing and return
   if (  preprocessed
      && this->mPreprocessor->Postprocess(result)
      )
   {
      // Postprocessing is successful
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Postprocessing complete." << std::endl;
      }
   }
   else
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " No preprocessing performed." << std::endl;
      }
      result.Clear();

      // Construct mode matrices of tensor sums before norm2 calc
      NiceTensor<Nb> reconstruct_sum;
      bool is_sum = aTensor.Type() == BaseTensor<Nb>::typeID::SUM;
      if (  is_sum
         )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Construct mode matrices of TensorSum." << std::endl;
         }
         reconstruct_sum = NiceTensor<Nb>(aTensor.StaticCast<TensorSum<Nb>>().ConstructModeMatrices());
      }

      auto& target_tensor  =  is_sum
                           ?  reconstruct_sum
                           :  aTensor;

      auto norm2 = target_tensor.Norm2();
      if (  this->ScreenImpl(target_tensor, norm2, aThresh)
         )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Screen tensor of norm: " << std::sqrt(norm2) << std::endl;
         }
         return NiceCanonicalTensor<Nb>(target_tensor.GetDims(), 0);
      }
      else if  (  target_rank == 1
               )
      {
         if (  this->mIoLevel > I_5
            )
         {
            Mout  << " Return rank-1 tensor" << std::endl;
         }
         return this->ConstructCanonicalTarget(target_tensor);
      }

      result = this->DecomposeImpl(target_tensor, norm2, aGuess, aThresh);
   }

   return result;
}


/**
 * Decompose a NiceTensor.
 * @param aTensor       NiceTensor to decompose.
 * @param aNorm2        Norm2 of target tensor
 * @param aGuess        NiceTensor used as initial guess
 * @param aThresh       The requested accuracy
 * @return              Return result of decompostion.
 **/
NiceTensor<Nb> TensorDecompDriverBase::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   auto target_rank = this->TargetRank(aTensor);

   if (  this->mIoLevel > I_5
      )
   {
      Mout  << " TensorDecompDriverBase::Decompose " << aTensor.ShowDims() << " " << aTensor.ShowType() << " of rank " << target_rank << std::endl;
   }

   // Return rank-0 tensors
   if (  target_rank == 0
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-0 target tensor." << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }
   assert(aGuess.Type() == BaseTensor<Nb>::typeID::CANONICAL);

   // Sanity check
   if (  aNorm2 != aNorm2
      || libmda::numeric::float_neg(aNorm2)
      )
   {
      MIDASERROR("TensorDecompDriverBase::Decompose: aNorm2 = "+std::to_string(aNorm2)+" for "+aTensor.ShowType());
   }

   if (  this->ScreenImpl(aTensor, aNorm2, aThresh) )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Screen tensor of norm: " << std::sqrt(aNorm2) << std::endl;
      }
      return NiceCanonicalTensor<Nb>(aTensor.GetDims(), 0);
   }
   else if  (  target_rank == 1
            )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Return rank-1 tensor" << std::endl;
      }
      return this->ConstructCanonicalTarget(aTensor);
   }

   bool preprocessed = false;
   NiceTensor<Nb> result;
   NiceTensor<Nb> guess;
   if (  this->mPreprocessor
      && this->mPreprocessor->Preprocess(aTensor, result, aGuess, guess)   
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Preprocessing complete." << std::endl;
      }
      preprocessed = true;
      auto prep_norm2 = result.Norm2();
      result = this->DecomposeImpl(result, prep_norm2, guess, aThresh);
   }

   if (  preprocessed
      && this->mPreprocessor->Postprocess(result)
      )
   {
      // Postprocessing is successful
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " Postprocessing complete." << std::endl;
      }
   }
   else
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " No preprocessing performed." << std::endl;
      }
      result.Clear();
      result = this->DecomposeImpl(aTensor, aNorm2, aGuess, aThresh);
   }

   return result;
}

/**
 * Get underlying decomposer type (driver+fitter).
 * @return          Returns type as string.
 **/
std::string TensorDecompDriverBase::Type
   (
   ) const
{
   return this->TypeImpl()+std::string(this->mCpFitter->Type());
}

/**
 * Construct mode matrices of target tensor
 *
 * @param aTensor       The target tensor
 * @return              aTensor as CanonicalTensor
 **/
NiceTensor<Nb> TensorDecompDriverBase::ConstructCanonicalTarget
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   //LOGCALL("calls");
   NiceTensor<Nb> result;

   switch(aTensor.Type())
   {
      case BaseTensor<Nb>::typeID::CANONICAL:
      {
         result = aTensor;
         break;
      }
      case BaseTensor<Nb>::typeID::DIRPROD:
      {
         result = NiceTensor<Nb>(aTensor.StaticCast<TensorDirectProduct<Nb>>().ConstructModeMatrices());
         break;
      }
      case BaseTensor<Nb>::typeID::SUM:
      {
         result = NiceTensor<Nb>(aTensor.StaticCast<TensorSum<Nb>>().ConstructModeMatrices());
         break;
      }
      default:
      {
         MIDASERROR("TensorDecompDriverBase: Unable to construct mode matrices of: " + aTensor.ShowType());
      }
   }

   return result;
}

/**
 * Get rank of target tensor.
 *
 * @param aTensor    The target tensor
 * @return Target rank
 **/
In TensorDecompDriverBase::TargetRank
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   In target_rank = -1;
   switch(aTensor.Type())
   {
      case BaseTensor<Nb>::typeID::CANONICAL:
      {
         target_rank = aTensor.StaticCast<CanonicalTensor<Nb>>().GetRank();
         break;
      }
      case BaseTensor<Nb>::typeID::DIRPROD:
      {
         target_rank = aTensor.StaticCast<TensorDirectProduct<Nb>>().Rank();
         break;
      }
      case BaseTensor<Nb>::typeID::SUM:
      {
         target_rank = aTensor.StaticCast<TensorSum<Nb>>().Rank();
         break;
      }
      default:
      {
         break;
      }
   }

   return target_rank;
}


/**
 * Factory method to construct concrete tensor decomp drivers.
 * @param aInfo       Info to construct from.
 * @return            Returns construct TensorDecompDriver.
 **/
std::unique_ptr<TensorDecompDriverBase> TensorDecompDriverBase::Factory
   ( const TensorDecompInfo& aInfo
   )
{
   auto iter = aInfo.find("DRIVER");
   if(iter == aInfo.end()) // check map
   {
      MIDASERROR("No 'DRIVER' entry in TensorDecompInfo.");
   }

   // create singlepoint
   auto concrete = TensorDecompDriverFactory::create(iter->second.get<std::string>(), aInfo);

   // check singlepoint
   if (  !concrete
      )
   {
      MIDASERROR("TensorDecompDriver not constructed.");
   }

   return concrete;
}




/**
 * Preprocess a NiceTensor before decomposition
 *
 * @param aTensor       The target tensor
 * @param aFitTensor    Return the preprocessed tensor
 * @return
 *    True if preprocessing has occured.
 **/
bool ConcreteCpPreprocessorBase::Preprocess
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   )
{
   return this->PreprocessImpl(aTensor, aFitTensor);
}

/**
 * Preprocess a NiceTensor and guess before decomposition
 *
 * @param aTensor       The target tensor
 * @param aFitTensor    Return the preprocessed tensor
 * @param aGuess        The guess (to aTensor)
 * @param aFitGuess     The preprocessed guess
 * @return
 *    True if preprocessing has occured.
 **/
bool ConcreteCpPreprocessorBase::Preprocess
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   ,  const NiceTensor<Nb>& aGuess
   ,  NiceTensor<Nb>& aFitGuess
   )
{
   return this->PreprocessImpl(aTensor, aFitTensor, aGuess, aFitGuess);
}

/**
 * Postprocess a NiceTensor after decomposition
 *
 * @param aFitTensor    The fitted tensor to be modified.
 * @return
 *    True if postprocessing has occured.
 **/
bool ConcreteCpPreprocessorBase::Postprocess
   (  NiceTensor<Nb>& aFitTensor
   )  const
{
   return this->PostprocessImpl(aFitTensor);
}

/**
 * Interface for Type of ConcreteCpPreprocessorBase.
 *
 * @return
 *    Type as string.
 **/
std::string ConcreteCpPreprocessorBase::Type
   (
   )  const
{
   return this->TypeImpl();
}

/**
 * Interface for ModifiesTargetNorm for ConcreteCpPreprocessorBase.
 *
 * @return
 *    True if preprocessor modifies target norm.
 **/
bool ConcreteCpPreprocessorBase::ModifiesTargetNorm
   (
   )  const
{
   return this->ModifiesTargetNormImpl();
}

/**
 * Factory method to construct concrete CP preprocessors
 * @param aInfo       Info to construct from.
 * @return            Returns construct ConcreteCpPreprocessor
 **/
std::unique_ptr<ConcreteCpPreprocessorBase> ConcreteCpPreprocessorBase::Factory
   ( const TensorDecompInfo& aInfo
   )
{
   auto iter = aInfo.find("PREPROCESSOR");
   if (  iter == aInfo.end()
      ) // check map
   {
      MIDASERROR("No 'PREPROCESSOR' entry in TensorDecompInfo.");
   }

   // Return nullptr if no preprocessor is required
   if (  iter->second.get<std::string>() == "NOPREPROCESSOR"   )
   {
      return nullptr;
   }

   //create singlepoint
   auto concrete = ConcreteCpPreprocessorFactory::create(iter->second.get<std::string>(), aInfo);

   if(!concrete) // check singlepoint
   {
      MIDASERROR("ConcreteCpPreprocessor not constructed.");
   }

   return concrete;
}





/**
 * Check that a NiceTensor will be decomposed.
 * @param aTensor                NiceTensor to check.
 * @return                       Return whether the NiceTensor will be decomposed by the ConcreteCpDecomposerBase obj.
 **/
bool ConcreteCpDecomposerBase::CheckDecomposition
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   return this->CheckDecompositionImpl(aTensor);
}

/**
 * Decompose a NiceTensor.
 * @param aTensor       NiceTensor to decompose.
 * @param aTargetNorm2  Norm2 of aTensor
 * @param aFitTensor    NiceTensor used as initial guess
 * @param aThresh      The requested accuracy
 * @return              Return result of decompostion.
 **/
FitReport<Nb> ConcreteCpDecomposerBase::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aTargetNorm2
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   assert(aFitTensor.Type() == BaseTensor<Nb>::typeID::CANONICAL);
   return this->DecomposeImpl(aTensor, aTargetNorm2, aFitTensor, aThresh);
}

/**
 * Get underlying type of ConcreteCpDecomposerBase.
 * @return          Returns type as string.
 **/
std::string ConcreteCpDecomposerBase::Type
   (
   ) const
{
   return this->TypeImpl();
}

/**
 * Factory method to construct concrete tensor decomposer's.
 * @param aInfo       Info to construct from.
 * @return            Returns construct ConcreteCpDecomposerBase.
 **/
std::unique_ptr<ConcreteCpDecomposerBase> ConcreteCpDecomposerBase::Factory
   ( const TensorDecompInfo& aInfo
   )
{
   auto iter = aInfo.find("FITTER");
   if(iter == aInfo.end()) // check map
   {
      MIDASERROR("No 'FITTER' entry in TensorDecompInfo.");
   }

   // Return nullptr if no fitter is required
   if (  iter->second.get<std::string>() == "NOFITTER"   )
   {
      return nullptr;
   }

   //create singlepoint
   auto concrete = ConcreteCpDecomposerFactory::create(iter->second.get<std::string>(), aInfo);

   if (  !concrete
      )  // check singlepoint
   {
      MIDASERROR("ConcreteCpDecomposer not constructed.");
   }

   return concrete;
}

} /* namespace detail */




/**
 * Constructor from a set of TensorDecompInfo's.
 * @param aSet                 Set to construct from.
 **/
TensorDecomposer::TensorDecomposer
   ( const TensorDecompInfo::Set& aSet
   )
{
   for(auto& decompinfo : aSet)
   {
      mDecomposers.emplace_back(detail::TensorDecompDriverBase::Factory(decompinfo));
   }  
}

/**
 * Constructor from single TensorDecompInfo.  Will just wrap in to a set and
 * recursively call set constructor.
 * @param aInfo                TensorDecompInfo to construct from.
 **/
TensorDecomposer::TensorDecomposer
   ( const TensorDecompInfo& aInfo
   )
{
   mDecomposers.emplace_back(detail::TensorDecompDriverBase::Factory(aInfo));
}

/**
 * Check that a NiceTensor will be decomposed by TensorDecomposer.
 * @param    aTensor             NiceTensor to check.
 * @return                       Do decomposition.
 **/
bool TensorDecomposer::CheckDecomposition
   ( const NiceTensor<Nb>& aTensor
   ) const
{
   bool do_decomp = false;
   for(auto& elem : mDecomposers)
   {
      do_decomp = (do_decomp || elem->CheckDecomposition(aTensor));
   }
   return do_decomp;
}


/**
 * Decompose a NiceTensor
 * @param aTensor       NiceTensor to decompose
 * @param aThresh      The requested accuracy
 * @return              The result of the decomposition.
 **/
NiceTensor<Nb> TensorDecomposer::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aThresh
   )  const
{
   // Return scalar
   if (  aTensor.Type() == BaseTensor<Nb>::typeID::SCALAR
      )
   {
      return aTensor;
   }

   for(auto& elem : mDecomposers)
   {
      if (  elem->CheckDecomposition(aTensor)
         )
      {
         return elem->Decompose( aTensor, aThresh );
      }
   }

   MidasWarning("No decomposer would decompose " + std::to_string(aTensor.NDim()) + ".-order " + aTensor.ShowType());

   return aTensor; // if no decomposition we just return a copy of input
}

/**
 * Decompose a NiceTensor
 * @param aTensor       NiceTensor to decompose
 * @param aNorm2        Norm2 of target
 * @param aThresh       The requested accuracy
 * @return              The result of the decomposition.
 **/
NiceTensor<Nb> TensorDecomposer::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   // Check norm
   if (  aNorm2 != aNorm2
      )
   {
      MIDASERROR("TensorDecomposer::Decompose: Argument norm2 is: "+std::to_string(aNorm2)+".");
   }

   // Return scalar
   if (  aTensor.Type() == BaseTensor<Nb>::typeID::SCALAR
      )
   {
      return aTensor;
   }

   for(auto& elem : mDecomposers)
   {
      if (  elem->CheckDecomposition(aTensor)
         )
      {
         return elem->Decompose( aTensor, aNorm2, aThresh );
      }
   }

   MidasWarning("No decomposer would decompose " + std::to_string(aTensor.NDim()) + ".-order " + aTensor.ShowType());

   return aTensor; // if no decomposition we just return a copy of input
}

/**
 * Decompose a NiceTensor
 * @param aTensor       NiceTensor to decompose
 * @param aGuess        NiceTensor used as initial guess
 * @param aThresh      The requested accuracy
 * @return              The result of the decomposition.
 **/
NiceTensor<Nb> TensorDecomposer::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   // Assert same shape
   assert_same_shape(*aTensor.GetTensor(), *aGuess.GetTensor());

   // Return scalar
   if (  aTensor.Type() == BaseTensor<Nb>::typeID::SCALAR
      )
   {
      return aTensor;
   }

   for(auto& elem : mDecomposers)
   {
      if (  elem->CheckDecomposition(aTensor)
         )
      {
         return elem->Decompose(aTensor, aGuess, aThresh);
      }
   }

   MidasWarning("No decomposer would decompose " + std::to_string(aTensor.NDim()) + ".-order " + aTensor.ShowType());

   return aTensor; // if no decomposition we just return a copy of input
}

/**
 * Decompose a NiceTensor
 * @param aTensor       NiceTensor to decompose
 * @param aNorm2        Norm2 of target
 * @param aGuess        NiceTensor used as initial guess
 * @param aThresh       The requested accuracy
 * @return              The result of the decomposition.
 **/
NiceTensor<Nb> TensorDecomposer::Decompose
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   // Assert same shape
   assert_same_shape(*aTensor.GetTensor(), *aGuess.GetTensor());

   // Return scalar
   if (  aTensor.Type() == BaseTensor<Nb>::typeID::SCALAR
      )
   {
      return aTensor;
   }

   // Check norm
   if (  aNorm2 != aNorm2
      )
   {
      MIDASERROR("TensorDecomposer::Decompose: Argument norm2 is: "+std::to_string(aNorm2)+".");
   }

   for(auto& elem : mDecomposers)
   {
      if (  elem->CheckDecomposition(aTensor)
         )
      {
         return elem->Decompose(aTensor, aNorm2, aGuess, aThresh);
      }
   }

   MidasWarning("No decomposer would decompose " + std::to_string(aTensor.NDim()) + ".-order " + aTensor.ShowType());

   return aTensor; // if no decomposition we just return a copy of input
}

} /* namespace tensor */
} /* namespace midas */
