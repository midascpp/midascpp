/**
************************************************************************
* 
* @file    FindBestCpDriver.cc
*
* @date    25-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of FindBestCpDriver methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/FindBestCpDriver.h"

#include <cassert>

#include "util/Io.h"
#include "util/CallStatisticsHandler.h"
#include "tensor/TensorFits.h"

namespace midas
{
namespace tensor
{

//! Register FindBestCpDriver
detail::TensorDecompDriverRegistration<FindBestCpDriver> registerFindBestCpDriver("FINDBESTCP");


/**
 * Check that a given tensor is decomposed by this driver.
 *
 * @param aTensor
 *    The target tensor.
 **/
bool FindBestCpDriver::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool order_ok  =  mDecompToOrder > 0 
                  ?  (In(aTensor.NDim()) >= mDecompToOrder)
                  :  true;

   auto type = aTensor.Type();

   bool type_ok   =  (  type == BaseTensor<Nb>::typeID::SIMPLE
                     || type == BaseTensor<Nb>::typeID::CANONICAL
                     || type == BaseTensor<Nb>::typeID::SUM
                     || type == BaseTensor<Nb>::typeID::DIRPROD
                     );

   return order_ok && type_ok;
}

/**
 * Get decomp threshold
 *
 * @return
 *    The threshold
 **/
Nb FindBestCpDriver::GetThreshold
   (
   )  const
{
   return mFindBestCpResThreshold;
}


/**
 * Return type of decomp driver.
 **/
std::string FindBestCpDriver::TypeImpl
   (
   )  const
{
   return std::string("FindBestCp");
}


/**
 * Fit a tensor to CP format by looping over different ranks.
 *
 * @param aTensor          The target tensor
 * @param aNorm2           Squared norm of the target tensor
 * @param aFitTensor       The fitting tensor
 * @param aStartingRank    First rank to try
 * @param aMaxRank         Maximum rank
 * @param aTargetRank      Rank of target (aTensor)
 * @param aThresh          Requested accuracy of the final fit
 **/
FitReport<Nb> FindBestCpDriver::FitCpTensor
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  NiceTensor<Nb>& aFitTensor
   ,  In aStartingRank
   ,  In aMaxRank
   ,  In aTargetRank
   ,  Nb aThresh
   )  const
{
   In rank = aStartingRank;
   auto norm = std::sqrt(aNorm2);
   auto ndim = aTensor.NDim();
   const auto& dims = aTensor.GetDims();
   FitReport<Nb> fit_report(C_0, norm, 0);

   // Set up rank incr
   In rank_incr = this->mFindBestCpRankIncr;
   if (  ndim < I_3 )
   {
      rank_incr = I_1;
   }
   else if  (  this->mFindBestCpScaleRankIncr )
   {
      rank_incr = (ndim-I_2)*this->mFindBestCpRankIncr;
   }
   else if  (  this->mFindBestCpAdaptiveRankIncr   )
   {
      rank_incr = (ndim-I_2)* *(std::min_element(dims.begin(), dims.end())) * this->mFindBestCpRankIncr;
   }

   // Fitting definitions
   Nb min_error = C_100*norm;
   Nb err_new = norm;
   Nb prev_err = norm;
   In best_fit_rank = I_0;
   In n_error_incr = I_0;
   In n_error_incr_tot = I_0;

   NiceTensor<Nb> best_fit;

   // Output
   if (  this->mIoLevel >= 3  )
   {
      Mout  << "##### Starting FindBestCpDriver #####" << "\n"
            << " Target type  :  " << aTensor.ShowType() << "\n"
            << " Dims         :  " << aTensor.ShowDims() << "\n"
            << " Thresh       :  " << aThresh << "\n"
            << " Norm2        :  " << aNorm2 << "\n"
            << " Norm         :  " << norm << "\n"
            << " Target rank  :  " << aTargetRank << "\n"
            << std::endl;
   }

   // Check norm
   if (  norm != norm
      )
   {
      MIDASERROR("FindBestCpDriver: Norm of " + aTensor.ShowType() + " of dims " + aTensor.ShowDims() + " is equal to: "+std::to_string(norm)+". Calculated from Norm2 = "+std::to_string(aNorm2));
   }
   

   // Loop over ranks
   while (  true  )
   {
      if (  this->mIoLevel >= 5  )
      {
         Mout  << " ### FindBestCpDriver fitting rank: " << rank << " ###" << std::endl;
      }

      // Check if rank = target_rank
      if (  aTargetRank > 0
         && rank == aTargetRank
         )
      {
         if (  this->mIoLevel >=5
            )
         {
            Mout << " rank = target_rank: Construct canonical target and break!" << std::endl;
         }
         aFitTensor = this->ConstructCanonicalTarget(aTensor);
         err_new = C_0;
         break;
      }

      // Increment guess
      if (  rank != aStartingRank
         )
      {
         LOGCALL("update guess");
         this->mGuesser->UpdateGuess(aFitTensor, aTensor, rank, norm, prev_err);
      }

      // Perform fit
      {
      LOGCALL("fitting");
      fit_report = this->mCpFitter->Decompose(aTensor, aNorm2, aFitTensor, aThresh);
      }
      err_new = fit_report.error;

      if (  this->mIoLevel >= 5  )
      {
         Mout << fit_report << std::endl;
         Mout << std::endl;
      }

      // Check for nan. Redo fit with new guess.
      if (  err_new != err_new
         )
      {
         MidasWarning("FindBestCpDriver: Error evaluates to " + std::to_string(err_new) + ". Reset guess!");
         this->mGuesser->MakeStartGuess(aTensor, std::sqrt(aNorm2), aFitTensor, rank);
         continue;
      }

      // Check if the error is larger than with the previous rank fit
      if (  err_new > prev_err
         )
      {
         ++n_error_incr;
         ++n_error_incr_tot;

         if (  n_error_incr > this->mMaxErrorIncr
            && err_new < C_10*this->mFindBestCpResThreshold
            )
         {
            MidasWarning("Break rank loop due to increasing error!");
            aFitTensor = best_fit;
            break;
         }
         else
         {
            MidasWarning("Error increasing with higher tensor rank!");
            Mout  << " New rank = " << rank << "\n"
                  << " Err old  = " << prev_err << "\n"
                  << " Err new  = " << err_new << std::endl;
         }
      }

      // Set saved errors and fit
      prev_err = err_new;
      if (  err_new < min_error
         )
      {
         min_error = err_new;
         best_fit = aFitTensor;
         best_fit_rank = rank;
         n_error_incr = std::max(n_error_incr-I_1, I_0);
      }

      // Check convergence
      if (  err_new < aThresh 
         || rank == aMaxRank
         )
      {
         // Set aFitTensor to best fit
         if (  err_new > min_error  )
         {
            aFitTensor = best_fit;
         }
         break; //break rank loop
      }

      rank += rank_incr;
      if (  rank > aMaxRank   )
      {
         rank = aMaxRank;
      }
   }

   // Output error if FitReport has not been printed
   if (  this->mIoLevel >= 3
      && this->mIoLevel < 5
      )
   {
      Mout  << " Error        :  " << err_new << "\n"
            << std::endl;
   }

   if (  err_new > aThresh   )
   {
      Mout  << "Bad FindBestCp fit of " << ndim << ".-order "+aTensor.ShowType()+" to rank " << rank << ":\n"
            << "\tDims:             " << aTensor.ShowDims() << "\n"
            << "\tThresh:           " << aThresh << "\n"
            << "\tRel. thresh:      " << aThresh / norm << "\n"
            << "\tError:            " << err_new << "\n"
            << "\tNorm:             " << norm << "\n"
            << "\tBest fit error:   " << min_error << "\n"
            << "\tBest fit rank:    " << best_fit_rank << "\n"
            << "\tTarget rank:      " << aTargetRank << "\n"
            << std::endl;;
   }

   return fit_report;
}

/**
 * Check if tensor should be screened (fitted to rank-0)
 *
 * @param aTensor       The target tensor
 * @param aNorm2        Norm2 of target
 * @param aThresh       Decomposition threshold
 * @return
 *    True if tensor should be screened
 **/
bool FindBestCpDriver::ScreenImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   if (  libmda::numeric::float_neg(aNorm2)
      )
   {
      return false;
   }
   else
   {
      Nb thresh   =  aThresh > C_0
                  ?  aThresh
                  :  this->mFindBestCpResThreshold;
   
      // Check norm
      Nb norm = std::sqrt(aNorm2);
      if (  norm < thresh
         )
      {
         return true;
      }
   
      return false;
   }
}


/**
 * Decompose a tensor.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> FindBestCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   LOGCALL("begin");
   // Declarations
   const auto norm2 = aNorm2;
   const auto norm  = std::sqrt(norm2);
   const auto& dims = aTensor.GetDims();
   const auto ndim = aTensor.NDim();
   Nb thresh   =  aThresh > C_0
               ?  aThresh
               :  this->mFindBestCpResThreshold;

   // Decompose low-order tensors
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()
      || (  this->mEffectiveLowOrderSvd
         && std::count_if(dims.begin(), dims.end(), [](unsigned i){ return (i != 1); }) == 2
         )
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("FindBestCpDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Get target rank
   auto target_rank = this->TargetRank(aTensor);

   // Init FitReport and canonical result
   auto fit_report = FitReport<Nb>(C_0, norm, 0);
   NiceTensor<Nb> canonical;
   this->mGuesser->MakeStartGuess(aTensor, norm, canonical, this->mStartingRank);

   // Theoretical max rank and the maximum fitting rank
   In theoretical_max_rank = aTensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
   In max_rank = std::min<In>(mFindBestCpMaxRank, theoretical_max_rank);

   // Check max_rank against target_rank for recompressions
   if (  target_rank > 0   )
   {
      max_rank = std::min<In>(max_rank, target_rank);
   }

   fit_report = this->FitCpTensor(aTensor, norm2, canonical, this->mStartingRank, max_rank, target_rank, thresh);

   if (  this->mPrintFitReport   )
   {
      Mout << " +------------- FindBestCp ---------------+\n" 
           << fit_report 
           << " +-------------------------------------------+\n" << std::endl;
   }

   return canonical;
}


/**
 * Decompose a tensor using an argument guess.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aGuess
 *    The guess tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> FindBestCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   // Check that rank of guess is reasonable
   const auto& dims = aTensor.GetDims();
   const In theoretical_max_rank = aTensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
   auto guess_rank = aGuess.StaticCast<CanonicalTensor<Nb> >().GetRank();
   if (  guess_rank > theoretical_max_rank   )
   {
      MidasWarning("Rank of guess larger than theoretical max. Running standard decomposition!");
      return this->DecomposeImpl(aTensor, aNorm2, aThresh);
   }

//   LOGCALL("begin");
   // Declarations
   const auto norm2 = aNorm2;
   const auto norm  = std::sqrt(norm2);
   const auto ndim = aTensor.NDim();
   Nb thresh   =  aThresh > C_0
               ?  aThresh
               :  this->mFindBestCpResThreshold;

   // Decompose low-order tensors if it has not been done in TensorDecomposer interface
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()
      || (  this->mEffectiveLowOrderSvd
         && std::count_if(dims.begin(), dims.end(), [](unsigned i){ return (i != 1); }) == 2
         )
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("FindBestCpDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Initialize FitReport
   auto fit_report = FitReport<Nb>(C_0, norm, 0);

   // Some declarations
   NiceTensor<Nb> canonical(new CanonicalTensor<Nb>(aGuess.StaticCast<CanonicalTensor<Nb> >()));

   // Get target rank
   auto target_rank = this->TargetRank(aTensor);

   // Declare maximum rank
   In max_rank      = std::min<In>(mFindBestCpMaxRank, theoretical_max_rank);

   // Check max_rank against target_rank for recompressions
   if (  target_rank > 0   )
   {
      max_rank = std::min<In>(max_rank, target_rank);
   }

   fit_report = this->FitCpTensor(aTensor, norm2, canonical, guess_rank, max_rank, target_rank, thresh);

   if (  this->mPrintFitReport   )
   {
      Mout << " +------------- FindBestCp ---------------+\n" 
           << fit_report 
           << " +-------------------------------------------+\n" << std::endl;
   }

   return canonical;
}



/**
 * Constructor from TensorDecompInfo.
 *
 * @param aInfo
 *    The TensorDecompInfo
 **/
FindBestCpDriver::FindBestCpDriver
   (  const TensorDecompInfo& aInfo
   )
   :  TensorDecompDriverBase(aInfo)
   ,  mFindBestCpMaxRank            (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPMAXRANK"].get<In>())
   ,  mFindBestCpRankIncr           (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPRANKINCR"].get<In>())
   ,  mStartingRank                 (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPSTARTINGRANK"].get<In>())
   ,  mFindBestCpScaleRankIncr      (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPSCALERANKINCR"].get<bool>()) 
   ,  mFindBestCpAdaptiveRankIncr   (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPADAPTIVERANKINCR"].get<bool>()) 
   ,  mFindBestCpResThreshold       (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPRESTHRESHOLD"].get<Nb>())
   ,  mMaxErrorIncr                 (const_cast<TensorDecompInfo&>(aInfo)["FINDBESTCPMAXERRORINCR"].get<In>())
   ,  mPrintFitReport               (false)
{
}

} /* namespace tensor */
} /* namespace midas */
