/**
 ************************************************************************
 * 
 * @file                CanonicalIntermediate.h
 *
 * Created:             11.02.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               CanonicalIntermediate: Representation of tensors
 *                      resulting from the contraction of two CanonicalTensor's.
 * 
 * Detailed Description: ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.  The code may only be used and/or
 * copied with the written permission of the author or in accordance with the
 * terms and conditions under which the program was supplied.  The code is
 * provided "as is" without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef CANONICALINTERMEDIATE_H_INCLUDED
#define CANONICALINTERMEDIATE_H_INCLUDED

// std headers
#include<vector>
#include<typeinfo>
#include<sstream>
#include<stdexcept>

#include "BaseTensor.h"
#include "CanonicalTensor.h"
#include "util/read_write_binary.h"

//! Return a freshly allocated copy of array \p input.
//template <class T>
//T* copy_array(const T* input, const size_t length)
//{
//   T* output(new T[length]);
//   memcpy(reinterpret_cast<void*>      (output),
//          reinterpret_cast<const void*>(input),
//          length*sizeof(T));
//   return output;
//}

template <class T>
void copy_matrix(const T* input, T* output, const size_t size)
{
   const T* in_ptr=input;
   T* out_ptr=output;
   while(in_ptr<input+size)
   {
      *(out_ptr++)=*(in_ptr++);
   }
}

//! %Tensor representation resulting from the contraction of two CanonicalTensor's
/*! Suppose a contraction of two CanonicalTensor's like
 * \f[
 *     c_{aj} = a_{ij} b_{ia}
 * \f]
 * with
 * \f[
 *     a_{ij} = \sum_r^R a^{(1)}_{ir} a^{(2)}_{jr}
 * \f]
 * \f[
 *     b_{ia} = \sum_s^S b^{(1)}_{is} b^{(2)}_{as}
 * \f]
 * then \f$c_{aj}\f$ can be written as
 * \f[
 *     c_{aj} = 
 *     \sum_{r,s}^{R,S} \left(\sum_i a^{(1)}_{ir} b^{(1)}_{is}\right) b^{(2)}_{as} a^{(2)}_{jr} =
 *     \sum_{r,s}^{R,S} \sigma_{rs} b^{(2)}_{as} a^{(2)}_{jr}
 * \f]
 * This representation can be directly dumped as a CanonicalTensor of rank \f$RS\f$, but this
 * involves a large increase in the rank and lots of data duplication. CanonicalIntermediate
 */
template <class T>
class CanonicalIntermediate: public BaseTensor<T>
{
   private:
      T** mModeMatrices;
      unsigned int mRankLeft;
      unsigned int mRankRight;
      std::vector<bool> mMatrixIsLeft;
      T* mInnerProductMatrix;

      void ThrowNotImplemented() const
      {
         throw std::runtime_error("You should not be doing this with a CanonicalIntermediate instance.");
      }

   public:
      /// Primary constructor.
      /**
       * Primary constructor.
       *
       * @param aModeMatricesLeft   Matrices from left tensor
       * @param aModeMatricesRight  Matrices from right tensor
       * @param aIndicesLeftIn  
       * @param aIndicesLeftOut
       * @param aIndicesRightIn
       * @param aIndicesRightOut
       * @param aRankLeft
       * @param aRankRight
       * @param aDimsLeft
       * @param aDimsRight
       * @param aInnerProductMatrix
       **/
      CanonicalIntermediate(const T* const * aModeMatricesLeft
                           ,const T* const * aModeMatricesRight
                           ,const std::vector<unsigned int> aIndicesLeftIn
                           ,const std::vector<unsigned int> aIndicesLeftOut
                           ,const std::vector<unsigned int> aIndicesRightIn
                           ,const std::vector<unsigned int> aIndicesRightOut
                           ,const unsigned int aRankLeft
                           ,const unsigned int aRankRight
                           ,const std::vector<unsigned int> aDimsLeft
                           ,const std::vector<unsigned int> aDimsRight
                           ,const T* aInnerProductMatrix
                            )
        :mModeMatrices(new T*[aIndicesLeftIn.size()+aIndicesRightIn.size()])
        ,mRankLeft {aRankLeft}
        ,mRankRight{aRankRight}
        ,mMatrixIsLeft(aIndicesLeftIn.size()+aIndicesRightIn.size(), false)
        ,mInnerProductMatrix(new T[aRankLeft * aRankRight])
      {
         // Copy couplings matrix
         T* ptr_out=mInnerProductMatrix;
         for(const T* ptr_in=aInnerProductMatrix;
             ptr_in<aInnerProductMatrix+aRankLeft * aRankRight;
             ++ptr_in, ++ptr_out)
         {
            *ptr_out = *ptr_in;
         }

         this->mDims=std::vector<unsigned int>(aIndicesLeftIn.size()+aIndicesRightIn.size(), 0);
         auto ileft_out=aIndicesLeftOut.begin();
         for(auto ileft_in: aIndicesLeftIn)
         {
            mMatrixIsLeft[*ileft_out]=true;
            this->mDims[*ileft_out]=aDimsLeft[ileft_in];
            mModeMatrices[*ileft_out]=new T[aDimsLeft[ileft_in]*mRankLeft];
            copy_matrix<T>(aModeMatricesLeft[ileft_in],
                  mModeMatrices[*ileft_out],
                  aDimsLeft[ileft_in]*mRankLeft);
            ileft_out++;
         }
         auto iright_out=aIndicesRightOut.begin();
         for(auto iright_in: aIndicesRightIn)
         {
            this->mDims[*iright_out]=aDimsRight[iright_in];
            mModeMatrices[*iright_out]=new T[aDimsRight[iright_in]*mRankRight];
            copy_matrix(aModeMatricesRight[iright_in],
                  mModeMatrices[*iright_out],
                  aDimsRight[iright_in]*mRankRight);
            iright_out++;
         }
      }

      //> Explicit constructor. Appropriates all pointers.
      CanonicalIntermediate(const std::vector<unsigned int> aDims
                           ,      T**                       aModeMatrices
                           ,const unsigned int              aRankLeft
                           ,const unsigned int              aRankRight
                           ,const std::vector<bool>&        aMatrixIsLeft
                           ,      T*                        aInnerProductMatrix):
         BaseTensor<T>      (aDims)
        ,mModeMatrices      (aModeMatrices      )
        ,mRankLeft          (aRankLeft          )
        ,mRankRight         (aRankRight         )
        ,mMatrixIsLeft      (aMatrixIsLeft      )
        ,mInnerProductMatrix(aInnerProductMatrix)
      {
      }

      
      CanonicalIntermediate(std::istream& input)
      {
         this->ReadDims(input);

         read_binary(mRankLeft,  input);
         read_binary(mRankRight, input);
         
         // std::vector<bool> is a special case of std::vector and the normal
         // vector stuff doesn't work. Yay. We need to make a copy.
         mMatrixIsLeft=std::vector<bool>(this->NDim());
         bool copy[this->NDim()];
         read_binary_array(copy, this->NDim(), input);
         for(int i=0;i<this->NDim();++i)
         {
            mMatrixIsLeft[i]=copy[i];
         }

         mInnerProductMatrix=new T[mRankLeft*mRankRight];
         // Read inner product matrix
         read_binary_array( mInnerProductMatrix, mRankLeft*mRankRight, input );

         // Read mode matrices
         mModeMatrices=new T*[this->NDim()];
         for(unsigned int idim=0;idim<this->NDim();++idim)
         {
            unsigned int rank=mMatrixIsLeft[idim]? mRankLeft : mRankRight;
            size_t blocksize=this->mDims[idim]*rank;
            mModeMatrices[idim]=new T[blocksize];
            read_binary_array( mModeMatrices[idim], blocksize, input );
         }
      }

      typename BaseTensor<T>::typeID Type() const override {return BaseTensor<T>::typeID::CANON_IM;};
      std::string ShowType() const override {return "CanonicalIntermediate";};

      ~CanonicalIntermediate()
      {
         for(unsigned int idim=0;idim<this->NDim();++idim)
         {
            {
               delete[] mModeMatrices[idim];
            }
         }
         delete[] mModeMatrices;
         delete[] mInnerProductMatrix;
      }

      CanonicalIntermediate* Clone() const override
      {
         this->ThrowNotImplemented();
//         return new CanonicalIntermediate(*this);
         return nullptr;
      }

      CanonicalIntermediate* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>& limits) const override
      {
         throw std::runtime_error("Not implemented");
      }

      CanonicalTensor<T> ToCanonical()
      {
         // Allocate output
         CanonicalTensor<T> result(this->mDims, this->mRankLeft*this->mRankRight );
         T** modematrices=result.GetModeMatrices();

         // Copy matrices: clone matrices for lefts, clone rows for rights
         for(unsigned int idim=0;idim<this->NDim();++idim)
         {
            if(this->mMatrixIsLeft[idim])
            {
               T* ptr_out=modematrices[idim];
               for(unsigned int irank_right=0;irank_right<mRankRight;++irank_right)
               {
                  T* ptr_in=mModeMatrices[idim];
                  for(unsigned int irank_left=0;irank_left<mRankLeft;++irank_left)
                  {
                     for(unsigned int n=0;n<this->mDims[idim];++n)
                     {
                        *(ptr_out++)= *(ptr_in++);
                     }
                  }
               }
            }
            else
            {
               T* ptr_out=modematrices[idim];
               T* ptr_in = nullptr;
               T* ptr_in_row=mModeMatrices[idim];
               for(unsigned int irank_right=0;irank_right<mRankRight;++irank_right)
               {
                  for(unsigned int irank_left=0;irank_left<mRankLeft;++irank_left)
                  {
                     ptr_in=ptr_in_row;
                     for(unsigned int n=0;n<this->mDims[idim];++n)
                     {
                        *(ptr_out++)= *(ptr_in++);
                     }
                  }
                  ptr_in_row=ptr_in;
               }
            }
         }

         // Multiply the first matrix with mInnerProductMatrix
         T* ptr_matrix=mInnerProductMatrix;
         T* ptr_out=modematrices[0];
         for(unsigned int irank_right=0;irank_right<mRankRight;++irank_right)
         {
            for(unsigned int irank_left=0;irank_left<mRankLeft;++irank_left)
            {
               for(unsigned int n=0;n<this->mDims[0];++n)
               {
                  *(ptr_out++) *= *ptr_matrix;
               }
               ++ptr_matrix;
            }
         }

         return result;
      }

   private:
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indicespos_inner_left
         ,  const std::vector<unsigned int>& indicespos_outer_left
         ,  const BaseTensor<T>&             right
         ,  const std::vector<unsigned int>& indicespos_inner_right
         ,  const std::vector<unsigned int>& indicespos_outer_right
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indicespos_result_left
         ,  const std::vector<unsigned int>& indicespos_result_right
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override
      {
         MIDASERROR("NOT IMPLEMENTED");
         return nullptr;
      }

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;

      BaseTensor<T>* ContractDown   (const unsigned int idx, const BaseTensor<T>* other) const override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* ContractForward(const unsigned int idx, const BaseTensor<T>* other) const override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* ContractUp     (const unsigned int idx, const BaseTensor<T>* other) const override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      BaseTensor<T>* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* operator*=(const T k) override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* operator/=(const T k)
      {
         this->ThrowNotImplemented();
      }

      CanonicalIntermediate* operator*=(const BaseTensor<T>& other_in) override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* operator/=(const BaseTensor<T>& other_in) override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* operator+=(const BaseTensor<T>& other_in) override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      CanonicalIntermediate* Reorder_impl(const std::vector<unsigned int>&,
                                          const std::vector<unsigned int>&) const override
      {
         this->ThrowNotImplemented();
         return nullptr;
      }

      std::string Prettify() const override
      {
         std::stringstream out;

         out << this->NDim() << "   [ " ;
         if(this->NDim()>0)
         {
            out << this->mDims[0];
            for (unsigned int i=1;i<this->NDim();i++)
            {
               out << ", " << this->mDims[i];
            }
         }
         out << " ]" << std::endl;
         out << "Rank: " << mRankLeft << " x " << mRankRight << std::endl;
         out << "  - Inner products matrix" << std::endl;
         {
            T* copy(new T[mRankLeft*mRankRight]);
            for(size_t i=0;i<mRankLeft*mRankRight;++i)
            {
               copy[i]=mInnerProductMatrix[i];
            }
            out << SimpleTensor<T>(std::vector<unsigned int> {mRankRight, mRankLeft},
                                copy).Prettify() << std::endl;
         }

         out << "  - Mode Matrices" << std::endl;
         for(int idim=0;idim<this->NDim();++idim)
         {
            unsigned int rank=mMatrixIsLeft[idim]? mRankLeft : mRankRight;
            // Copy the mModeMatrix, SimpleTensor will destroy it...
            T* copy(new T[rank * this->mDims[idim]]);
            for(size_t i=0;i<rank * this->mDims[idim];++i)
            {
               copy[i]=mModeMatrices[idim][i];
            }

            out << SimpleTensor<T>(std::vector<unsigned int> {rank, this->mDims[idim]},
                                   copy).Prettify();
            if(idim<this->NDim()-1)
            {
               out << std::endl;
            }
         }
         return out.str();
      }

      std::ostream& write_impl(std::ostream& output) const override
      {
         write_binary(mRankLeft,  output);
         write_binary(mRankRight, output);
         
         const bool& first=mMatrixIsLeft[0];
         const bool* lefts=&first;
         write_binary_array(lefts, this->NDim(), output);

         // Write inner product matrix
         write_binary_array( mInnerProductMatrix, mRankLeft*mRankRight, output );

         // Write mode matrices
         for(unsigned int idim=0;idim<this->NDim();++idim)
         {
            unsigned int rank=mMatrixIsLeft[idim]? mRankLeft : mRankRight;
            size_t blocksize=this->mDims[idim]*rank;
            write_binary_array( mModeMatrices[idim], blocksize, output );
         }
         return output;
      }

      dot_t Dot(const BaseTensor<T>* const) const override;
};

template<class T>
typename DotEvaluator<T>::dot_t CanonicalIntermediate<T>::Dot
   (  const BaseTensor<T>* const other
   )  const 
{
   return midas::math::Conj(other->DotDispatch(this));
}

#endif // CANONICALINTERMEDIATE_H_INCLUDED
