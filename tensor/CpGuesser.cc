/**
************************************************************************
* 
* @file    CpGuesser.cc
*
* @date    01-12-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Pre-compiled templates for CpGuesser class
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

// Include declaration and implementation
#include "tensor/CpGuesser.h"
#include "tensor/CpGuesser_Impl.h"

// Pre-compiled templates
template class CpGuesser<double>;
template class CpGuesser<float>;

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
