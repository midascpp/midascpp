/**
************************************************************************
* 
* @file    EnergyHolderTensor_Impl.h
*
* @date    18-01-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Tensor class for holding modal/orbital energies 
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef ENERGYHOLDERTENSOR_H_INCLUDED
#define ENERGYHOLDERTENSOR_H_INCLUDED

#include "tensor/EnergyHolderTensor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "tensor/EnergyHolderTensor_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* ENERGYHOLDERTENSOR_H_INCLUDED */
