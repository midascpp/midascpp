/**
 ************************************************************************
 * 
 * @file                NiceTensor.h
 *
 * Created:             19.12.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               NiceTensor: Representation-agnostic tensors. Convenient
 *                      wrapper for BaseTensor.
 * 
 * Last modified:       Fri Dec 19 2014
 *
 * Detailed Description: NiceTensor is a layer around BaseTensor to
 *                       implement tensor operations without knowing
 *                       about how those tensors are actually represented
 *                       in memory (full, canonical, etc.). Particular
 *                       focus on arbitrary tensor contractions.
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef NICETENSOR_DECL_H_INCLUDED
#define NICETENSOR_DECL_H_INCLUDED

#include <ostream>

#include "NiceContractor.h"
#include "BaseTensor.h"
#include "Scalar.h"
#include "SimpleTensor.h"
#include "CanonicalTensor.h"
#include "TensorDirectProduct.h"

template <class T>
class NiceTensor;

template <class T>
class BaseTensor;

class ContractionIndices;
class ContractionIndex;

template <class T>
T dot_product(const NiceTensor<T>&, const NiceTensor<T>&);

template<typename T> T sum_prod_elem(const NiceTensor<T>&, const NiceTensor<T>&);

template <class T>
NiceTensor<T> outer_product(const NiceTensor<T>&,
                            const std::vector<unsigned int>&,
                            const NiceTensor<T>&,
                            const std::vector<unsigned int>& );

template <class T>
std::ostream& operator<<(std::ostream&, const NiceTensor<T>&);

//! Representation-agnostic tensors. Convenient self-cleaning wrapper for BaseTensor.
/*! NiceTensor should be used every time the exact representation of the
 *  tensor (full, CP, etc) does not need to be known.
 *
 *  Initialization
 *  --------------
 *  When initializing a NiceTensor from scratch, the precise tensor
 *  representation must be established. To do this, we need to allocate
 *  a fresh BaseTensor*.
 *
 *      NiceTensor<double> b(new SimpleTensor<double>(random_simple_tensor<double>(std::vector<unsigned int>{2,3,2,3})));
 *
 */
template <class T>
class NiceTensor
{
   public:
      using real_t = typename BaseTensor<T>::real_t;

   private:
      //! Tensor being handled.
      BaseTensorPtr<T> mTensor;
      
      bool IsType(typename BaseTensor<T>::typeID) const;

      //! Rank-1 cross approximation
      NiceTensor<T> CrossApproximation
         (  const std::vector<unsigned>&
         ,  const unsigned aMaxSteps = 0
         )  const;

   public:
      //! Default constructor.
      NiceTensor();
      //! Appropriate BaseTensor*, it will be deleted upon destruction.
      NiceTensor(BaseTensor<T>*);
      //! Appropriate BaseTensor*, it will be deleted upon destruction.
      NiceTensor(BaseTensorPtr<T>&&);
      //! Constructs a zero NiceTensor of appropriate BaseTensor type.
      NiceTensor(const std::vector<unsigned int>& arDims, typename BaseTensor<T>::typeID aType);
      //! Constructs a unit NiceTensor of appropriate BaseTensor type, with a 1 at the index.
      NiceTensor(const std::vector<unsigned int>& arDims, const std::vector<unsigned int>& arIndex, typename BaseTensor<T>::typeID aType);

      //! Copy constructor
      NiceTensor(const NiceTensor&);
      //! Move constructor
      NiceTensor(      NiceTensor&&);

      //! Initialize from input stream
      NiceTensor(std::istream&);
      //! Destructor
      ~NiceTensor();

      //! Copy assignment
      NiceTensor& operator=(const NiceTensor&);
      //! Move assignment
      NiceTensor& operator=(      NiceTensor&&);

      //! Create a new tensor by created a new multidimensional slice.
      NiceTensor Slice(const std::vector<std::pair<unsigned int,unsigned int>>& limits) const;

      //! Clear the tensor by setting mTensor to nullptr
      NiceTensor& Clear
         (
         );

      //! Swap tensors
      void SwapTensors
         (  NiceTensor<T>&
         );
/*============== QUERY ==============*/
      //! Get number of dimensions (length of #mTensor->mDims).
      unsigned int NDim()  const;
      //! Get max extent
      unsigned MaxExtent() const;
      //! Get max extent
      unsigned MinExtent() const;
      //! Get extent along dimension
      unsigned Extent(unsigned i) const;
      //! Get total number of elements.
      std::size_t  TotalSize()            const;
      //! Get #mTensor->mDims.
      const std::vector<unsigned int>& GetDims() const;
            //std::vector<unsigned int>& GetDims();

      //! Pretty print dimensions
      std::string ShowDims() const;

      //! Pretty print type (Simple, Canonical, etc.) type of the tensor.
      std::string ShowType() const;
      //! Get underlying type
      typename BaseTensor<T>::typeID Type() const;

      //! Get #mTensor.
      BaseTensor<T>* GetTensor() const;
      //! Get #mTensor.
      BaseTensor<T>* Release();
      //! Checks whether mTensor points to nullptr.
      bool IsNullPtr() const;

      //! Sanity check
      bool SanityCheck() const;

/*============== CONTRACTIONS ==============*/
      //! Contract one dimension away with a vector. See BaseTensor::ContractDown.
      NiceTensor ContractDown   (const unsigned int, const NiceTensor&) const;
      //! Transform a dimension with a matrix. See BaseTensor::ContractForward.
      NiceTensor ContractForward(const unsigned int, const NiceTensor&) const;
      //! Insert a dimension with a vector. See BaseTensor::ContractUp.
      NiceTensor ContractUp     (const unsigned int, const NiceTensor&) const;
      //! Sum over a hyperdiagonal. See BaseTensor::ContractInternal.
      NiceTensor ContractInternal(const std::vector<unsigned int>&) const;

/*============ OTHER OPERATIONS ============*/
      //! Scaling (in place).
      NiceTensor& operator*=(const T&);
      //! Scaling. 
      NiceTensor  operator* (const T&) const;

      //! Add another tensor (in place).
      NiceTensor& operator+=(const NiceTensor&);
      //! Add another tensor.
      NiceTensor  operator+ (const NiceTensor&) const;

      //! Subtract another tensor (in place).
      NiceTensor& operator-=(const NiceTensor&);
      //! Subtract another tensor.
      NiceTensor  operator- (const NiceTensor&) const;

      //! Element-wise multiply with another tensor (in place).
      NiceTensor& operator*=(const NiceTensor&);
      //! Element-wise multiply with another tensor.
      NiceTensor  operator* (const NiceTensor&) const;

      //! Element-wise divide with another tensor (in place).
      NiceTensor& operator/=(const NiceTensor&);

      //! Axpy (Y = aX + Y); add a scalar times a tensor to this tensor, in-place.
      void Axpy(const NiceTensor&, const T&);

      //! Norm of the tensor.
      real_t Norm() const;
      
      //! Norm2 of the tensor, i.e. the sum of squares of all elements.
      real_t Norm2() const;
      
      //! Zero the tensor in-place
      void Zero();

      //! Make the object point to a ZeroTensor.
      void ToZeroTensor();

      //! Scale the tensor in-place
      void Scale(const T&);

      //! Set data to absolute value
      void Abs();

      //! complex conjugate
      void Conjugate
         (
         );
      
      //!
      void ElementwiseScalarAddition(const T&);
      void ElementwiseScalarSubtraction(const T&);

      //! Direct product
      void AdditiveDirectProduct(const T, const std::vector<NiceTensor<T> >&, const std::vector<std::vector<unsigned int> >&);

/*============ PRETTY PRINT ============*/
      //! Print all elements in nice format. Notice this converts to SimpleTensor.
      friend std::ostream& operator<< <>(std::ostream&, const NiceTensor<T>&);
      //! Binary write to stream.
      std::ostream& Write(std::ostream&) const;

      //! Binary write to name
      void WriteToDisc
         (  const std::string&
         ,  midas::mpi::OFileStream::StreamType = midas::mpi::OFileStream::StreamType::MPI_MASTER
         )  const;

      //! Binary read from name
      bool ReadFromDisc
         (  const std::string&
         ,  bool = true
         );

/*============  "CASTS" ============*/
      // Cast underlying tensor into TENSOR type using dynamic cast
      template<class TENSOR>
      const TENSOR& DynamicCast() const { return dynamic_cast<const TENSOR&>(*mTensor); }
      
      // Cast underlying tensor into TENSOR type using dynamic cast
      template<class TENSOR>
      TENSOR& DynamicCast() { return dynamic_cast<TENSOR&>(*mTensor); }
      
      // Cast underlying tensor into TENSOR type using static cast
      template<class TENSOR>
      const TENSOR& StaticCast() const { return static_cast<const TENSOR&>(*mTensor); };
      
      // Cast underlying tensor into TENSOR type using static cast
      template<class TENSOR>
      TENSOR& StaticCast() { return static_cast<TENSOR&>(*mTensor); };

      //! Return a new tensor internally represented as a SimpleTensor.
      NiceTensor ToSimpleTensor() const;

      //! Return a new tensor internally represented as a CanonicalTensor.
      /*! Heavily defaulted, it only takes error as input parameter. Possibly
       *  a more tunable version is needed, but I would suggest very much
       *  sticking to directly acting on
       *  dynamic_cast<SimpleTensor<T>>(GetTensor()).
       */
      NiceTensor ToCanonicalTensor(const T&) const;

      //! Return successive cross approximation to the tensor
      NiceTensor SuccessiveCrossApproximation
         (  const unsigned  
         ,  const unsigned = 0
         )  const;
      
      //! Convert to Scalar and extract value.
      T GetScalar() const;
      
      //! Check whether NiceTensor holds a scalar
      bool IsScalar() const;
      
      //! DumpInto ptr
      void DumpInto(T*) const;

      //! Return a new tensor where the dimensions are reordered.
      /*! This forces the construction of a whole new tensor, it does not make
       *  use of TensorView. */
      NiceTensor Reorder(const std::vector<unsigned int>&) const;
 
/*============  Contractions using NiceContractors ============*/
      NiceContractor<T> operator[] (const ContractionIndices& indices) const;
      NiceContractor<T> operator[] (const ContractionIndex& index)     const;
      NiceTensor(const NiceContractor<T>&);

/*============  MPI STUFF  ============*/
#ifdef VAR_MPI
      void MpiSend(In aRecievingRank) const;
      void MpiRecv(In aSendingRank);
      void MpiBcast(In aRoot);
#endif /* VAR_MPI */
};

//! Multiply with scalar from the left.
template <class T>
NiceTensor<T> operator*(const T&, const NiceTensor<T>&);

//! "Low-level" contraction. Use contract(const NiceContractor<T>&, const NiceContractor<T>&) instead.
template <class T>
NiceTensor<T> contract
   (  const NiceTensor<T>&
   ,  const std::vector<unsigned int>&
   ,  const NiceTensor<T>&
   ,  const std::vector<unsigned int>&
   ,  bool = false
   ,  bool = false
   );

//! Dot product (total contraction).
template <class T>
T dot_product(const NiceTensor<T>&, const NiceTensor<T>&);

//! Assert same shape of tensors
template
   <  typename T
   >
void assert_same_shape
   (  const NiceTensor<T>& first
   ,  const NiceTensor<T>& second
   )
{
   if (  first.GetTensor()
      && second.GetTensor()
      )
   {
      assert_same_shape(first.GetTensor(), second.GetTensor());
   }
   else
   {
      MIDASERROR("Uninitialized tensor: mTensor");
   }
}

template
   <  typename T
   >
inline void Scale
   (  NiceTensor<T>& arThis
   ,  const T& aFactor
   )
{
   arThis.Scale(aFactor);
}

template
   <  typename T
   >
inline void Zero
   (  NiceTensor<T>& arThis
   )
{
   arThis.Zero();
}

template
   <  typename T
   ,  typename U
   >
inline void Axpy
   (  NiceTensor<T>& arThis
   ,  const NiceTensor<T>& aOther
   ,  U aFactor
   )
{
   arThis.Axpy(aOther, T(aFactor));
}

template
   <  typename T
   >
inline void SetShape
   (  NiceTensor<T>& arThis
   ,  const NiceTensor<T>& aShape
   )
{
   arThis = aShape;
   arThis.Zero();
}

template
   <  typename T
   >
inline auto Norm
   (  const NiceTensor<T>& aThis
   )
{
   return aThis.Norm();
}

template
   <  typename T
   >
inline size_t Size
   (  const NiceTensor<T>& aThis
   )
{
   return aThis.TotalSize();
}

template
   <  typename T
   ,  typename U
   >
inline void DataFromPointer
   (  NiceTensor<T>& arThis
   ,  const U* apPtr
   )
{
   constexpr bool complex_tensor = midas::type_traits::IsComplexV<T>;
   constexpr bool complex_ptr = midas::type_traits::IsComplexV<U>;

   if constexpr   (  complex_ptr
                  && !complex_tensor
                  )
   {
      MIDASERROR("Cannot set data in real tensor from complex pointer!");
   }
   else
   {
      if (  arThis.Type() == BaseTensor<T>::typeID::SIMPLE
         )
      {
         auto* data = static_cast<SimpleTensor<T>*>(arThis.GetTensor())->GetData();
         auto* ptr_in = apPtr;

         auto size = arThis.TotalSize();

         for(In i=I_0; i<size; ++i)
         {
            if constexpr   (  complex_tensor
                           && !complex_ptr
                           )
            {
               *(data++) = T(*ptr_in, *(ptr_in+1));
               ptr_in += 2;
            }
            else
            {
               *(data++) = *(ptr_in++);
            }
         }
      }
      else if  (  arThis.Type() == BaseTensor<T>::typeID::SCALAR
               )
      {
         auto& scalar = static_cast<Scalar<T>&>(*arThis.GetTensor());
         auto* ptr_in = apPtr;
         if constexpr   (  complex_tensor
                        && !complex_ptr
                        )
         {
            scalar.SetValue(T(*ptr_in, *(ptr_in+1)));
         }
         else
         {
            scalar.SetValue(*ptr_in);
         }
      }
      else
      {
         MIDASERROR("NiceTensor 'DataFromPointer' not implemented for other types than SimpleTensor and Scalar!");
      }
   }
}

template
   <  typename T
   ,  typename U
   >
inline void DataToPointer
   (  const NiceTensor<T>& aThis
   ,  U* apPtr
   )
{
   constexpr bool complex_tensor = midas::type_traits::IsComplexV<T>;
   constexpr bool complex_ptr = midas::type_traits::IsComplexV<U>;

   if constexpr   (  complex_ptr
                  && !complex_tensor
                  )
   {
      MIDASERROR("Cannot set data in complex pointer from real tensor!");
   }
   else
   {
      if (  aThis.Type() == BaseTensor<T>::typeID::SIMPLE
         )
      {
         const auto* data = static_cast<SimpleTensor<T>*>(aThis.GetTensor())->GetData();
         auto* ptr_out = apPtr;

         auto size = aThis.TotalSize();

         for(In i=I_0; i<size; ++i)
         {
            if constexpr   (  complex_tensor
                           && !complex_ptr
                           )
            {
               *(ptr_out++) = std::real(*data);
               *(ptr_out++) = std::imag(*(data++));
            }
            else
            {
               *(ptr_out++) = *(data++);
            }
         }
      }
      else if  (  aThis.Type() == BaseTensor<T>::typeID::SCALAR
               )
      {
         auto val = static_cast<Scalar<T>*>(aThis.GetTensor())->GetValue();
         auto* ptr_out = apPtr;
         if constexpr   (  complex_tensor
                        && !complex_ptr
                        )
         {
            *(ptr_out++) = std::real(val);
            *(ptr_out++) = std::imag(val);
         }
         else
         {
            *ptr_out = val;
         }
      }
      else
      {
         MIDASERROR("NiceTensor 'DataToPointer' not implemented for other types than SimpleTensor and Scalar!");
      }
   }
}

//! Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   );

//! Mean Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeMeanNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   );

//! Max Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeMaxNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   );

//! Contractions using NiceContractors
template<class T>
NiceTensor<T> make_nice_simple(const std::vector<unsigned>&, T*);

//!
template<class T>
NiceTensor<T> make_nice_random_simple(const std::vector<unsigned>&);

//!
template<class T>
NiceTensor<T> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);

/**
 *
 **/
template
   <  class T
   ,  class... Ts
   >
NiceTensor<T> NiceSimpleTensor(Ts&&... ts)
{
   return NiceTensor<T>(new SimpleTensor<T>(std::forward<Ts>(ts)...));
}

//!
template
   <  class T
   ,  class... Ts
   >
NiceTensor<T> NiceCanonicalTensor(Ts&&... ts)
{
   return NiceTensor<T>(new CanonicalTensor<T>(std::forward<Ts>(ts)...));
}

//!
template
   <  class T
   ,  class... Ts
   >
NiceTensor<T> NiceTensorSum(Ts&&... ts)
{
   return NiceTensor<T>(new TensorSum<T>(std::forward<Ts>(ts)...));
}

//!
template
   <  class T
   ,  class... Ts
   >
NiceTensor<T> NiceTensorDirectProduct(Ts&&... ts)
{
   return NiceTensor<T>(new TensorDirectProduct<T>(std::forward<Ts>(ts)...));
}

//!
template
   <  class T
   ,  class... Ts
   >
NiceTensor<T> NiceScalar(Ts&&... ts)
{
   return NiceTensor<T>(new Scalar<T>(std::forward<Ts>(ts)...));
}

#endif /* NICETENSOR_DECL_H_INCLUDED */
