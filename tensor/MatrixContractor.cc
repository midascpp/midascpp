#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "MatrixContractor.h"
#include "MatrixContractor_Impl.h"
#include <complex>

#define INSTANTIATE_MATRIXCONTRACTOR(T) \
template class MatrixContractor<T>; \
\
template std::ostream& operator<<(std::ostream&, const MatrixContractor<T>&);

INSTANTIATE_MATRIXCONTRACTOR(float)
INSTANTIATE_MATRIXCONTRACTOR(double)
INSTANTIATE_MATRIXCONTRACTOR(std::complex<float>)
INSTANTIATE_MATRIXCONTRACTOR(std::complex<double>)

#undef INSTANTIATE_MATRIXCONTRACTOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
