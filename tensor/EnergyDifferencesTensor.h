#ifndef ENERGYDIFFERENCESTENSOR_H_INCLUDED
#define ENERGYDIFFERENCESTENSOR_H_INCLUDED

#include<vector>

#include"inc_gen/Const.h"
#include"inc_gen/TypeDefs.h"
#include"CanonicalTensor.h"
#include"EnergyDenominatorTensor.h"
//#include"util/MultiIndex.h"
#include "util/CallStatisticsHandler.h"

// Forward decl
std::vector<unsigned int> edt_dims
   (  const unsigned int order
   ,  const unsigned int nvir
   ,  const unsigned int nocc
   );

// Forward decl
std::vector<unsigned int> edt_dims
   (  const std::vector<In>&
   ,  const std::vector<In>&
   );

/**
 *
 **/
template <class T>
class EnergyDifferencesTensor
   : public CanonicalTensor<T>
{
   public:
      ///> Constructor for electronic-structure calculations (used in CC2 for the error vector)
      EnergyDifferencesTensor(const unsigned int order,
                              const std::vector<T>& vir,
                              const std::vector<T>& occ):
         CanonicalTensor<T>(edt_dims(order, static_cast<unsigned int>( vir.size() ),
                                            static_cast<unsigned int>( occ.size() )),
                            order*2)
      {
         // mModeMatrices
         // Even ones are made from occupied energies
         // Odd ones are made from virtual energies
         for(In iorder=0;iorder<order;++iorder)
         {
            // Virtual
            {
               T* ptr=this->mModeMatrices[iorder*2];
               for(In irank=0;irank<2*order;++irank)
               {
                  if(irank==2*iorder)
                  {
                     for(auto e: vir)
                        *(ptr++)= e; // Notice minus
                  }
                  else
                  {
                     for(In i=0;i<vir.size();++i)
                        *(ptr++)= 1.0;
                  }
               }
            }
            // Occupied
            {
               T* ptr=this->mModeMatrices[iorder*2+1];
               for(In irank=0;irank<2*order;++irank)
               {
                  if(irank==2*iorder+1)
                  {
                     for(auto e: occ)
                        *(ptr++)= -e;
                  }
                  else
                  {
                     for(In i=0;i<occ.size();++i)
                        *(ptr++)= 1.0;
                  }
               }
            }
         }
      }

      ///> Constructor for vib calculations. Represents the VCC 0th-order Jacobian as CanonicalTensor.
      EnergyDifferencesTensor
         (  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  const Nb& arNb = C_0
         )
         :  CanonicalTensor<T>
               (  edt_dims
                     (  aModes
                     ,  aNmodals
                     )
               ,  aModes.size()
               )
      {
         LOGCALL("calls");

         Nb val = C_0;
         In mode = I_0;
         for(In imode=I_0; imode<aModes.size(); ++imode)
         {
            T* ptr = this->mModeMatrices[imode];
            for(In irank=I_0; irank<this->mRank; ++irank)
            {
               mode = aModes[imode];
               if(irank == imode)
               {
                  for(In iexci=I_1; iexci<aNmodals[mode]; ++iexci)
                  {
                     // Get eigval
                     aEigVals.DataIo(IO_GET, aOffsets[mode]+iexci, val);

                     // Set element and subtract arNb
                     *(ptr++) = val - arNb;
                  }
               }
               else
               {
                  for(In i=I_1; i<aNmodals[mode]; ++i)
                  {
                     *(ptr++) = static_cast<T>(C_1);
                  }
               }
            }
         }
      }
};


template<class T>
class ExactEnergyDifferencesTensor
   : public SimpleTensor<T>
{
   public:
      ///> Constructor for vib calculations. Represents the VCC 0th-order Jacobian as SimpleTensor.
      ExactEnergyDifferencesTensor
         (  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  Nb aDiagEnergy=C_0
         )
         :  SimpleTensor<T>
               (  edt_dims
                     (  aModes
                     ,  aNmodals
                     )
               )
      {
         LOGCALL("calls");
         auto holder = NiceTensor<T>(new ModalEnergyHolderTensor<T>(this->GetDims(), false, false, aDiagEnergy, aModes, aEigVals, aOffsets));

         auto ptr = std::make_unique<T[]>(holder.TotalSize());

         holder.DumpInto(ptr.get());

         this->SwapData(ptr);
      }
};

#endif /* ENERGYDIFFERENCESTENSOR_H_INCLUDED */
