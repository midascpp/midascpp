#ifndef BASETENSOR_IMPL_H_INCLUDED
#define BASETENSOR_IMPL_H_INCLUDED

#include<algorithm>
#include<stdexcept>
#include<sstream>

#include "Scalar.h"
#include "tensor/TensorView.h"
#include "tensor/CanonicalTensor.h"

#include "util/read_write_binary.h"
#include "util/Io.h"
#include "util/CallStatisticsHandler.h"
#include "util/Math.h"

#include "lapack_interface/GESDD.h"
#include "libmda/numeric/float_eq.h"

#include "mpi/Impi.h"

//! Default constructor (no elements)
template <class T>
BaseTensor<T>::BaseTensor
   (
   )
   : mDims{}
{
}

//! Does nothing, required by pure virtual destructor...
template <class T>
BaseTensor<T>::~BaseTensor
   (
   )
{
}

//! Basic constructor to set up #mDims, to be used by all other derived classes.
template <class T>
BaseTensor<T>::BaseTensor
   ( const std::vector<unsigned int>& aDims
   )
   : mDims(aDims)
{
   if(std::any_of(mDims.begin(), mDims.end(), [](const unsigned int & i)->bool {return i==0;}))
   {
      MIDASERROR("Attempted to create a tensor where at least one dimension is 0!");
   }
}

//! Basic constructor to set up #mDims, to be used by all other derived classes.
template <class T>
BaseTensor<T>::BaseTensor
   ( std::vector<unsigned int>&& aDims
   )
   : mDims(std::move(aDims))
{
   if(std::any_of(mDims.begin(), mDims.end(), [](const unsigned int & i)->bool {return i==0;}))
   {
      MIDASERROR("Attempted to create a tensor where at least one dimension is 0!");
   }
}

//! Number of dimensions (AKA order) of the tensor ( #mDims .size() ).
template <class T>
unsigned int BaseTensor<T>::NDim
   (
   ) const
{
   return mDims.size();
}

//! Total number of elements ( \f$\prod_i N_i \f$ ).
template <class T>
std::size_t BaseTensor<T>::TotalSize
   (
   ) const
{
   std::size_t result = 1;
   for(unsigned int i=0; i < NDim(); ++i)
   {
      result *= mDims[i];
   }
   return result;
}

/// Get the shape (dimensions) of the vector.
template <class T>
const std::vector<unsigned int>& BaseTensor<T>::GetDims() const
{
   return mDims;
}

/// Get the shape (dimensions) of the vector.
template <class T>
std::vector<unsigned int>& BaseTensor<T>::GetDims()
{
   return mDims;
}

/**
 * Show dimensions as string
 *
 * @return
 *    Dimensions as string
 **/
template <class T>
std::string BaseTensor<T>::ShowDims
   (
   )  const
{
   auto dims = this->mDims;
   auto ndim = this->NDim();
   std::string dims_string("[");
   for(size_t i=0; i<ndim; ++i)
   {
      dims_string += std::to_string(dims[i]);
      dims_string += (i == (ndim-1)) ? std::string("]") : std::string(", ");
   }
   if (  !ndim
      )
   {
      dims_string += "]";
   }
   
   return dims_string;
}

//
template<class T>
unsigned BaseTensor<T>::Extent
   ( unsigned i
   ) const
{
   return mDims[i];
}

///
template<class T>
unsigned BaseTensor<T>::MaxExtent
   (
   ) const
{
   unsigned max_extent = 0;
   for(int i = 0; i < this->NDim(); ++i)
   {
      if(max_extent < this->GetDims()[i])
         max_extent = this->GetDims()[i];
   }
   return max_extent;
}

///
template<class T>
unsigned BaseTensor<T>::MinExtent
   (
   ) const
{
   const auto& dims = this->GetDims();
   return *std::min_element(dims.begin(), dims.end());
}

template <class T>
BaseTensor<T>* BaseTensor<T>::operator*(const T factor) const
{
   BaseTensor<T>* result(this->Clone());
   *result*=factor;
   return result;
}

template <class T>
BaseTensor<T>* operator* (const T factor, const BaseTensor<T>& self)
{
   return self * factor;
}


template <class T>
BaseTensor<T>* BaseTensor<T>::operator-=(const BaseTensor<T>& right)
{
   BaseTensor<T>* minus_right=static_cast<T>(-1)*right;
   *this+=*minus_right;
   delete minus_right;
   return this;
}


template <class T>
BaseTensor<T>* linear_combination(const std::vector<T>& coeffs,
                                  const std::vector<BaseTensor<T>*>& tensors)
{
   if(coeffs.size()==tensors.size())
   {
      auto t_ptr=tensors.begin();
      auto c_ptr=coeffs .begin();

      BaseTensor<T>* result= *(c_ptr++) * **(t_ptr++);
      while(t_ptr<tensors.end())
      {
         auto temp=*(c_ptr++) * **(t_ptr++);
         *result += *temp;
         delete temp;
      }
      return result;
   }
   else
   {
      MIDASERROR ( "ERROR: Different number of coefficients and tensors" );
   }
}

//! General tensor contraction.
/*! For end applications, the NiceTensor wrapper class should *always* be used
 *  for contractions. See contract(const NiceContractor&, const NiceContractor&).
 *
 *  This function takes care of parsing the contraction indices and pass them
 *  to the relevant Contract_impl() implementation. See BaseTensor::Contract_impl
 *  for a detailed description of the virtual interface.
 *
 *  Example
 *  -------
 *  \f[
 *     c_{abcd} = a_{icja} b_{jibd}
 *  \f]
 *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 *  contract(a, std::vector<unsigned int>{'i','c','j','a'},
 *           b, std::vector<unsigned int>{'j','i','b','d'});
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *  Notice how the ordering of the output dimensions is determined
 *  by the lexicographic value of the respective indices..
 *  
 *  Implementation details
 *  ----------------------
 *  - The order of the dimensions of the resulting tensor
 *    is determined by the lexicographic value of the outer (i.e. non-contracted)
 *    indices! There is no possibility to have the resulting indices look
 *    like 'aibj'. This is possible using
 *    contract(const NiceTensor&, const NiceTensor&), which you should be using anyway!
 *
 *  @param left_in          A tensor.
 *  @param indices_left_in  Contraction indices for left_in.
 *  @param right_in         Another tensor.
 *  @param indices_right_in Contraction indices for left_in.
 *  @param conjugate_left   Do complex conjugate of left tensor
 *  @param conjugate_right  Do complex conjugate of right tensor
 *
 */
template <class T>
BaseTensor<T>* contract
   (  const BaseTensor<T>& left_in
   ,  const std::vector<unsigned int>& indices_left_in
   ,  const BaseTensor<T>& right_in
   ,  const std::vector<unsigned int>& indices_right_in
   ,  bool conjugate_left
   ,  bool conjugate_right
   )
{
   //------ Reorder indices if we have a TensorView -----------------
   // First for left
   const BaseTensor<T>* left_ptr;
   std::vector<unsigned int> indices_left=indices_left_in;
   if(const TensorView<T>* left_tv_ptr=dynamic_cast<const TensorView<T>*>(&left_in))
   {
      left_tv_ptr->SwapVector(indices_left);
      left_ptr=left_tv_ptr->GetTensor();
   }
   else
   {
      left_ptr=&left_in;
   }

   // Then for right
   const BaseTensor<T>* right_ptr;
   std::vector<unsigned int> indices_right=indices_right_in;
   if(const TensorView<T>* right_tv_ptr=dynamic_cast<const TensorView<T>*>(&right_in))
   {
      right_tv_ptr->SwapVector(indices_right);
      right_ptr=right_tv_ptr->GetTensor();
   }
   else
   {
      right_ptr=&right_in;
   }

   // ------------ Declarations --------------
   // Variables called index_XXX refer to elements of indices_right and
   // indices_left, it is an actual index or letter
   // variables called indexpos_XXX are indices themselves, i.e. positions
   // of the elements
   // example:
   // index_left_a = indices_left[ indexpos_left_a ];
   std::vector<unsigned int> indices_inner;
   std::vector<unsigned int> indices_outer_left;
   std::vector<unsigned int> indices_outer_right;
   std::vector<bool> index_right_was_matched(indices_right_in.size(), false);
   std::vector<unsigned int> indicespos_inner_left;
   std::vector<unsigned int> indicespos_outer_left;
   std::vector<unsigned int> indicespos_inner_right;
   std::vector<unsigned int> indicespos_outer_right;

   // ------ Check that we passed the correct number of indices ------
   if((indices_left.size()!=left_ptr->NDim())||(indices_right.size()!=right_ptr->NDim()))
   {
      MIDASERROR("In contract: the number of contraction indices must match the number of dimensions of the tensor");
   }

   // --------- Find matching indices in left and right ------------
   // Iterate over all left indices
   for(int indexpos_left=indices_left.size()-1;indexpos_left>=0;--indexpos_left)
   {
      bool match=false;
      // Iterate over all right indices
      for(int indexpos_right=indices_right.size()-1;indexpos_right>=0;--indexpos_right)
      {
         // Are the indices the same (i.e. both are "i") and must be summed over?
         if(indices_left[indexpos_left] == indices_right[indexpos_right] )
         {
            // Error-checking: cannot contract along selected indices
            if(left_ptr->mDims[indexpos_left] != right_ptr->mDims[indexpos_right])
            {
               std::stringstream pretty;
               pretty << "A(dims: [ ";
               for(auto i: left_ptr->mDims)
                  pretty << i << ", ";
               pretty << "]) [ ";
               for(auto i: indices_left)
                  pretty << (char) i << ", ";
               pretty << " ] x B(dims: [";
               for(auto i: right_ptr->mDims)
                  pretty << i << ", ";
               pretty << "]) [ ";
               for(auto i: indices_right)
                  pretty << (char) i << ", ";
               pretty << " ]";

               MIDASERROR(
                  "Attempting to contract along two dimensions with different sizes "+pretty.str());
            }
            else
            {
               indices_inner.push_back(indices_left[indexpos_left]);
               indicespos_inner_left.push_back(indexpos_left);
               index_right_was_matched[indexpos_right]=true;
               match=true;
               break;
            }
         }
      }
      if(!match)
      {
         indices_outer_left.push_back(indices_left[indexpos_left]);
         indicespos_outer_left.push_back(indexpos_left);
      }
   }
   for(int indexpos_right=indices_right.size()-1;indexpos_right>=0;--indexpos_right)
   {
      if(!index_right_was_matched[indexpos_right])
      {
         indices_outer_right.push_back(indices_right[indexpos_right]);
         indicespos_outer_right.push_back(indexpos_right);
      }
   }

   // Now we have indices_inner and indices_outer_left in the same order
   // they apper in indices_left, and indices_outer_right in the same order
   // they appear in indices_right
   // indicespos_inner_right follows the order of indicespos_inner_left
   for(auto indexpos_left: indicespos_inner_left)
   {
      for(unsigned int indexpos_right=0;indexpos_right<indices_right.size();++indexpos_right)
      {
         if( indices_right[indexpos_right] == indices_left[indexpos_left] )
         {
            indicespos_inner_right.push_back(indexpos_right);
         }
      }
   }

   //// ---------- Sort indices according to their values -------------
   //auto sort_indices=[](       std::vector<unsigned int>& indices,
   //                      const std::vector<unsigned int>& values)
   //{
   //   return std::sort(indices.begin(), indices.end(),
   //         [&values](unsigned int i1, unsigned int i2)
   //         {
   //            return values[i1]>values[i2];
   //         });
   //};


   // ----------- Put all outer indices together, and sort ---------------
   std::vector<unsigned int> indices_result=indices_outer_left;
   indices_result.insert(indices_result.end(), indices_outer_right.begin(),
                                               indices_outer_right.end()  );
   std::sort(indices_result.begin(), indices_result.end());

   // OK, now figure out the position of the iterator indices for the result ...
   std::vector<unsigned int> indicespos_result_left;
   std::vector<unsigned int> indicespos_result_right;
   // ... and the dimensions of the result
   std::vector<unsigned int> dims_result;

   // Figure out in which order we have to navigate the output tensor
   for(auto i:indicespos_outer_right)
   {
      unsigned int pos=0;
      for(auto j: indices_result)
      {
         if(j==indices_right[i])
         {
            indicespos_result_right.push_back(pos);
         }
         ++pos;
      }
   }
   for(auto i:indicespos_outer_left)
   {
      unsigned int pos=0;
      for(auto j: indices_result)
      {
         if(j==indices_left[i])
         {
            indicespos_result_left.push_back(pos);
         }
         ++pos;
      }
   }

   // ------------ Figure out dimensions of the result ---------------
   for(auto index_result: indices_result)
   {
      bool match=false;
      for(unsigned int indexpos_left=0;indexpos_left<indices_left.size();++indexpos_left)
      {
         if(index_result==indices_left[indexpos_left])
         {
            dims_result.push_back(left_ptr->mDims[indexpos_left]);
            match=true;
            break;
         }
      }
      if(match)
      {
         continue;
      }
      for(unsigned int indexpos_right=0;indexpos_right<indices_right.size();++indexpos_right)
      {
         if(index_result==indices_right[indexpos_right])
         {
            dims_result.push_back(right_ptr->mDims[indexpos_right]);
         }
      }
   }

   // ------------ AAAAAAAAAAAND WE CALL THE ACTUAL CONTRACTOR!!! ----------
   return left_ptr->Contract_impl(indicespos_inner_left,
                                  indicespos_outer_left,
                                  *right_ptr,
                                  indicespos_inner_right,
                                  indicespos_outer_right,
                                  dims_result,
                                  indicespos_result_left,
                                  indicespos_result_right,
                                  conjugate_left,
                                  conjugate_right
                                  );
}

template <class T>
BaseTensor<T>* BaseTensor<T>::ContractInternal(const std::vector<unsigned int>& indices_in) const
{
   const BaseTensor<T>* this_ptr;
   std::vector<unsigned int> indices=indices_in;
   if(const TensorView<T>* this_tv_ptr=dynamic_cast<const TensorView<T>*>(this))
   {
      this_tv_ptr->SwapVector(indices);
      this_ptr=this_tv_ptr->GetTensor();
   }
   else
   {
      this_ptr=this;
   }

   std::vector<std::pair<unsigned int,unsigned int>> inner_indices_pairs;
   std::vector<unsigned int> outer_indices;

   // First pass: Find pair of repeated indices, put into inner_indices,
   for(int i1=0;i1<indices.size();++i1)
   {
      for(int i2=i1+1;i2<indices.size();++i2)
      {
         if(indices[i1]==indices[i2])
         {
            inner_indices_pairs.push_back(std::pair<unsigned int,unsigned int>(i1,i2));
            break;
         }
      }
      
   }

   // Second pass: put remaining indices into outer_indices
   for(int i=0;i<indices.size();++i)
   {
      bool found=false;
      for(auto inner_indices_pair: inner_indices_pairs)
      {
         if(i==inner_indices_pair.first || i==inner_indices_pair.second )
         {
            found=true;
         }
      }
      if(found)
      {
         continue;
      }
      else
      {
         outer_indices.push_back(i);
      }
   }

   // Sort remaining indices according to their values
   auto sort_indices=[](       std::vector<unsigned int>& indices,
                         const std::vector<unsigned int>& values)
   {
      std::sort(indices.begin(), indices.end(),
            [&values](unsigned int i1, unsigned int i2)
            {
               return values[i1]<values[i2];
            });
   };
   sort_indices(outer_indices, indices);

   // OK, now we have figured out:
   // - if there are indices to contract (found)
   // - which indices are those (inner_indices)
   // - the remaining indices (outer_indices)

   if(inner_indices_pairs.size()==0) // No indices to contract, just reorder the tensor
   {
      return this_ptr->Reorder(outer_indices);
   }
   else // Found a pair of indices! We contract those
   {
      // First check the contraction makes sense
      for(auto inner_indices_pair: inner_indices_pairs)
      {
         if(mDims[inner_indices_pair.first]!=mDims[inner_indices_pair.second])
         {
            MIDASERROR("Trying to internally contract two non-conforming dimensions.");
         }
      }

      return ContractInternal_impl(inner_indices_pairs, outer_indices);
   }
}

/// For a dimension \f$j\f$, return \f$\prod_{i=0}^{j-1}N_i\f$. Used by contractions.
template <class T>
size_t BaseTensor<T>::NBefore(const unsigned int idx) const
{
   size_t result=1;
   for(unsigned int i=0;i<idx;i++)
   {
      result*=this->mDims[i];
   }
   return result;
}

/// For a dimension \f$j\f$, return \f$\prod_{i=j+1}^{D}N_i\f$. Used by contractions.
template <class T>
size_t BaseTensor<T>::NAfter (const unsigned int idx) const
{
   size_t result=1;
   for(unsigned int i=idx+1;i<this->NDim();i++)
   {
      result*=this->mDims[i];
   }
   return result;
}



/*!
 * Usage example:
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 * BaseTensor* t_prime(
 *    new BaseTensor(
 *       t.Reorder(std::vector<unsigned int>{'a', 'd', 'b', 'c'})
 *    )
 * );
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * This corresponds to the assignment
 *
 * \f[
 *     t'_{abcd}=t_{adbc}
 * \f]
 *
 * The order of the output indices results from reordering \p newindices
 * alphabetically. The example above yields the same results as
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 * BaseTensor* t_prime(
 *    new BaseTensor(
 *       t.Reorder(std::vector<unsigned int>{0, 3, 1, 2})
 *    )
 * );
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * This method only processes the indices and figures out the dimensions
 * of the resulting tensor. The actual reordering is carried out by the
 * particular implementation of Reorder_impl() of the relevant derived
 * class.
 *
 * Note that NiceTensor provides a more practical interface for reordering:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 * NiceTensor t_prime( t[X::a, X::d, X::b, X::c] );
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * See NiceTensor::operator[] for more information.
 *
 * @param newindices New positions of the given indices. They do not
 *                   need to be contiguous.
 */
template <class T>
BaseTensor<T>* BaseTensor<T>::Reorder
   ( const std::vector<unsigned int>& newindices
   ) const
{
   // Sort indices according to their values
   auto sort_indices=[](       std::vector<unsigned int>& indices,
                         const std::vector<unsigned int>& values)
   {
      std::sort(indices.begin(), indices.end(),
            [&values](unsigned int i1, unsigned int i2)
            {
               return values[i1]<values[i2];
            });
   };

   std::vector<unsigned int> oldorder;
   std::vector<unsigned int> neworder;
   for(int i = 0; i < this->NDim(); ++i)
   {
      oldorder.push_back(i);
   }
   neworder = oldorder;
   sort_indices(neworder, newindices);

   // Find the dimensions of the reordered tensor.
   std::vector<unsigned int> olddims(this->GetDims());
   std::vector<unsigned int> newdims(this->NDim());
   unsigned int i_old = 0;
   for(auto i_new : neworder)
   {
      newdims[i_new] = olddims[i_old++];
   }

   // Call actual implementation.
   return Reorder_impl(neworder, newdims);
}

/*! Usage example:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 * BaseTensor* slice(
 *    new BaseTensor(
 *       tensor.Slice(std::vector<std::pair<unsigned int,unsigned int>>{ {0,4}, {3,5}, {2,7} })
 *    )
 * );
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Using Kolda's notation, this corresponds to
 * \f[
 *     \mathbf{t}'=\mathbf{t}_{0:3,3:4,2:6}
 * \f]
 *
 * The actual work is done by Slice_impl(). This subroutine does nothing, other than reordering
 * the limits if the BaseTensor being sliced is a TensorView instance.
 */
template <class T>
BaseTensor<T>* BaseTensor<T>::Slice
   ( const std::vector<std::pair<unsigned int,unsigned int>>& limits_in
   ) const
{
   if(limits_in.size()!=this->mDims.size())
   {
     MIDASERROR("Wrong number of dimensions to slice");
   }

   // Reorder limits if *this is a TensorView instance.
   std::vector<std::pair<unsigned int,unsigned int>> limits=limits_in;
   //const BaseTensor<T>* this_ptr;
   if(const TensorView<T>* this_tv_ptr=dynamic_cast<const TensorView<T>*>(this))
   {
      this_tv_ptr->SwapVector(limits);
      //this_ptr=this_tv_ptr->GetTensor();
   }
   //else
   //{
   //   this_ptr=this;
   //}

   // Call the actual Slice implementation.
   return this->Slice_impl(limits);
}

/// Return the norm, i.e. the square root of the sum of the squares of its elements.
/*! Norm is just a wrapper for `sqrt(dot_product(*this,*this))`.
 */
template <class T>
typename BaseTensor<T>::real_t BaseTensor<T>::Norm
   (
   )  const
{
   return std::sqrt(Norm2());
}

/// Return the norm, i.e. the square root of the sum of the squares of its elements.
/*! Norm2 is just a wrapper for `dot_product(*this,*this)`.
 */
template <class T>
typename BaseTensor<T>::real_t BaseTensor<T>::Norm2
   (
   )  const
{
/* This check is not good enough. A CanonicalTensor fit to a low numerical
 * threshold with 0 norm can have a negative dot product with itself, due to
 * numerical errors from finite precision arithmetic. 
 */
#define TOLERANCE 1.e-8
   auto dp = this->Dot(this);

   real_t dp_real = std::real(dp);
   real_t dp_imag = std::imag(dp);
   if (  dp_real < 0.
      )
   {
      if (  dp_real/TotalSize() > -TOLERANCE
         )
      {
         dp_real = -dp_real;
      }
      else
      {
         MIDASERROR("SANITY CHECK FAILED: dot_product is buggy");
      }
   }
   else if  (  !libmda::numeric::float_numeq_zero(dp_imag, dp_real)
            )
   {
      Mout  << " BaseTensor::Norm2() = " << dp << std::endl;
      MIDASERROR("BaseTensor: Complex Norm2 detected!");
   }
   return dp_real;
}

/**
 * Safe Norm2 function
 *
 * @return Norm2 of tensor
 **/
template <class T>
typename BaseTensor<T>::real_t BaseTensor<T>::SafeNorm2
   (
   )  const
{
   auto size = this->TotalSize();

   auto allocator_tp = this->GetAllocatorTp();
   auto ptr = allocator_tp.AllocateUniqueArray(size);
   this->DumpInto(ptr.get());

   auto safe_result = static_cast<T>(0.0);
   for(size_t idx = 0; idx < size; ++idx)
   {
      safe_result += midas::math::Conj(ptr[idx])*ptr[idx];
   }

   real_t real_safe_result = std::real(safe_result);

   // Check sanity
   if (  !midas::util::IsSane(safe_result)
      || (  libmda::numeric::float_neg(real_safe_result)
         && !libmda::numeric::float_numeq_zero(real_safe_result, static_cast<real_t>(1.), 1000)
         )
      || !libmda::numeric::float_numeq_zero(std::imag(safe_result), std::real(safe_result))
      )
   {
      Mout  << " BaseTensor::SafeNorm2 returns " << safe_result << std::endl;
      MIDASERROR("BaseTensor::SafeNorm2 sanity check failed!");
   }

   return real_safe_result;
}

/**
 * Conjugate in place
 */
template
   <  class T
   >
void BaseTensor<T>::Conjugate
   (
   )
{
   // Only do something if we work with complex numbers
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      this->ConjugateImpl();
   }
}

// *********** FACILITIES TO COMPARE TENSOR SHAPES ******************
/// Returns true if the two arguments have the same shape, false otherwise.
template <class T>
bool have_same_shape
   ( const BaseTensor<T>& left
   , const BaseTensor<T>& right
   )
{
   //std::vector<unsigned int> ldims= left.GetDims();
   //std::vector<unsigned int> rdims=right.GetDims();
   auto& ldims =  left.GetDims();
   auto& rdims = right.GetDims();
   if(ldims.size() != rdims.size())
   {
      return false;
   }
   for (unsigned int i=0;i<ldims.size();i++)
   {
      if(ldims[i] != rdims[i])
      {
         return false;
      }
   }
   return true;
}

/// Abort execution unless have_same_shape returns true.
template <class T>
void assert_same_shape
   ( const BaseTensor<T>& left
   , const BaseTensor<T>& right
   )
{
   if (  !have_same_shape(left, right)
      )
   {
      Mout  << " left:  " << left.ShowDims() << "\n"
            << " right: " << right.ShowDims() << "\n"
            << std::flush;
      MIDASERROR("ERROR: Tensors have different shapes");
   }
}

/// Abort execution unless have_same_shape returns true.
template <class T>
void assert_same_shape
   ( const BaseTensor<T>* const left
   , const BaseTensor<T>* const right
   )
{
   if(!have_same_shape(*left, *right))
   {
      MIDASERROR("ERROR: Tensors have different shapes");
   }
}

template <class T>
std::ostream& operator<<(std::ostream& os, const BaseTensor<T>& self)
{
   os << self.Prettify();
   return os;
}

template <class T>
std::ostream& BaseTensor<T>::Write(std::ostream& output) const
{
   unsigned int tmp=NDim();
   write_binary(tmp, output);
   for(auto& dim: mDims)
      write_binary(dim, output);
   return this->write_impl(output);
}

template <class T>
std::istream& BaseTensor<T>::ReadDims
   ( std::istream& input
   )
{
   // Wipe out old dims
   this->mDims=std::vector<unsigned int>();

   // Read ndim
   unsigned int ndim;
   read_binary(ndim, input);

   // Read dims
   for(int idim=0;idim<ndim;++idim)
   {
      unsigned int tmp;
      read_binary(tmp, input);
      this->mDims.push_back(tmp);
   }
   return input;
}

/*! DotDispatch
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const BaseTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const SimpleTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const CanonicalTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const ZeroTensor<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const Scalar<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const TensorDirectProduct<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

template<class T>
typename DotEvaluator<T>::dot_t BaseTensor<T>::DotDispatch
   ( const TensorSum<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}


/*! If no virtual implementation is available, show
 */
template <class T>
std::string BaseTensor<T>::Prettify() const
{
   return std::string("<< Undefined format for "+ShowType()+" >>");
}

/*!
 */
template <class T>
void BaseTensor<T>::DumpInto(T*) const
{
   MIDASERROR("DumpInto() not implemented for: " + this->ShowType() + ".");
}

/**
 * Dump a tensor with only one dimension > 1 into a CanonicalTensor
 *
 * @return  New rank-1 tensor
 **/
template <class T>
BaseTensorPtr<T> BaseTensor<T>::DumpVectorToCanonical
   (
   )  const
{
   auto ndim = this->NDim();
   auto dims = this->mDims;

   auto tot_size = this->TotalSize();
   bool is_scalar = (tot_size == 1);

   assert(  ndim == 1
         || this->MaxExtent() == tot_size
         );

   // Get allocator
   auto allocator_tp = this->GetAllocatorTp();
   auto allocator_tpp = this->GetAllocatorTpp();

   auto** data = allocator_tpp.Allocate(this->NDim());

   // If the tensor is a scalar, distribute the norm
   if (  is_scalar   )
   {
      auto scalar_ptr = allocator_tp.AllocateUniqueArray(1);
      this->DumpInto(scalar_ptr.get());
      auto scalar = scalar_ptr[0];

      auto phase = scalar / std::abs(scalar);
      auto value = std::pow( std::abs(scalar), static_cast<T>(1.)/static_cast<T>(ndim) );

      for(unsigned idim=0; idim<ndim; ++idim)
      {
         data[idim] = allocator_tp.Allocate(1);

         data[idim][0] = value;
      }
      data[0][0] *= phase;
   }
   else
   {
      // Allocate pointers and dump into the dimension of extent > 1
      for(unsigned idim=0; idim<ndim; ++idim)
      {
         data[idim] = allocator_tp.Allocate(dims[idim]);

         if (  dims[idim] > 1 )
         {   
            this->DumpInto(data[idim]);
         }
         else
         {
            data[idim][0] = static_cast<T>(1.); // Perhaps the norm should be distributed...
         }
      }
   }

   BaseTensorPtr<T> result_ptr( new CanonicalTensor<T>(dims, 1, data) );

   return result_ptr;
}


/**
 * Reduce the rank of a matrix by dumping into SimpleTensor
 * and performing an SVD.
 *
 * @param aThresh
 *    The SVD threshold used to determine the rank, if aRank<0.
 * @param aRank
 *    Optional SVD rank. If negative, aThresh is used.
 * @return
 *    The reduced tensor as BaseTensorPtr
 **/
template <class T>
BaseTensorPtr<T> BaseTensor<T>::SvdRankReduction
   (  real_t aThresh
   ,  In aRank
   )  const
{
   // Check that we are dealing with a matrix
   auto dims = this->GetDims();
   auto ndims = this->NDim();
   assert(std::count_if(dims.begin(), dims.end(), [](unsigned i) { return (i != 1); }) == 2);

   // Get indices of extents != 1
   auto red_dims = this->GetDims();
   red_dims.clear();
   red_dims.reserve(2);
   for(unsigned id=0; id<ndims; ++id)
   {
      if (  dims[id] != 1
         )
      {
         red_dims.emplace_back(id);
      }
   }

   // Dump data into ptr
   auto allocator_tp = this->GetAllocatorTp();
   auto data_ptr = allocator_tp.AllocateUniqueArray(this->TotalSize());
   int m = dims[red_dims[0]];
   int n = dims[red_dims[1]];

   if (  ndims == 2
      && this->Type() == BaseTensor<T>::typeID::CANONICAL
      )
   {
      auto* cp_ptr = static_cast<const CanonicalTensor<T>* const>(this);

      // use gemm
      char transa = 'N';
      char transb = 'T';
      int k = cp_ptr->GetRank();
      T alpha = static_cast<T>(1.);
      T* a = cp_ptr->GetModeMatrices()[0];
      int lda = std::max(1, m);
      T* b = cp_ptr->GetModeMatrices()[1];
      int ldb = std::max(1, n);
      T beta = static_cast<T>(0.);
      int ldc = std::max(1, m);

      midas::lapack_interface::gemm (  &transa, &transb
                                    ,  &m, &n, &k
                                    ,  &alpha, a, &lda
                                    ,  b, &ldb, &beta
                                    ,  data_ptr.get(), &ldc
                                    );
   }
   else
   {
      // Make row-major pointer
      this->DumpInto( data_ptr.get() );

      // Make pointer to column-major matrix
      auto colmaj_ptr = allocator_tp.AllocateUniqueArray(this->TotalSize());

      T* out_ptr = colmaj_ptr.get();
      T* in_ptr_ref = data_ptr.get();
      for(int i=0; i<n; ++i)
      {
         T* in_ptr = in_ptr_ref + i;
         for(int j=0; j<m; ++j)
         {
            *(out_ptr++) = *in_ptr;
            in_ptr += n;
         }
      }

      // Swap data into data_ptr
      std::swap(colmaj_ptr, data_ptr);
   }

   // Perform SVD (GESDD reads column-major matrix)
   auto svd = GESDD( data_ptr.get(), m, n );

   if (  svd.info != 0
      )
   {
      MidasWarning("SvdRankReduction on " + this->ShowType() + " of dims " + this->ShowDims() + " and norm " + std::to_string(this->Norm()) + ": SVD info = " + std::to_string(svd.info));
   }

   // Truncate to accuracy
   auto rank = aRank < 0 ? RankAcc( svd, aThresh ) : aRank;
   
   BaseTensorPtr<T> result_ptr( new CanonicalTensor<T>(dims, svd, rank) );

   return result_ptr;
}




#ifdef VAR_MPI
/**
 *
 **/
template<class T>
void BaseTensor<T>::MpiSend
   (  In aRecievingRank
   )  const
{
   MIDASERROR("MpiSend() not implemented for: " + this->ShowType() + ".");
}

/**
 *
 **/
template<class T>
void BaseTensor<T>::MpiRecv
   (  In aSendingRank
   )
{
   MIDASERROR("MpiRecv() not implemented for: " + this->ShowType() + ".");
}

/**
 *
 **/
template
   <  class T
   >
void BaseTensor<T>::MpiBcast
   (  In aRoot
   )
{
   MIDASERROR("MpiBcast() not implemented for: " + this->ShowType() + ".");
}

/**
 *
 **/
template<class T>
void BaseTensor<T>::MpiSendDimensions
   (  In aRecievingRank
   )  const
{
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   
   // send order
   unsigned order = mDims.size();
   midas::mpi::detail::WRAP_Send(&order, 1, MPI_UNSIGNED, aRecievingRank, 0, MPI_COMM_WORLD);
   
   // send extents
   void* ptr_dim = const_cast<void*>(static_cast<const void*>(mDims.data()));
   midas::mpi::detail::WRAP_Send(ptr_dim, mDims.size(), MPI_UNSIGNED, aRecievingRank, 0, MPI_COMM_WORLD);
}

/**
 *
 **/
template<class T>
void BaseTensor<T>::MpiRecvDimensions
   (  In aSendingRank
   )
{
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   MPI_Status status;
   
   // receive order
   unsigned order;
   midas::mpi::detail::WRAP_Recv(&order, 1, MPI_UNSIGNED, aSendingRank, 0, MPI_COMM_WORLD, &status);
   
   // receive extents
   mDims.resize(order);
   void* ptr_dim = const_cast<void*>(static_cast<const void*>(mDims.data()));
   midas::mpi::detail::WRAP_Recv(ptr_dim, mDims.size(), MPI_UNSIGNED, aSendingRank, 0, MPI_COMM_WORLD, &status);
}

/**
 *
 **/
template
   <  class T
   >
void BaseTensor<T>::MpiBcastDimensions
   (  In aRoot
   )
{
   // Bcast order
   unsigned order = this->NDim();
   midas::mpi::detail::WRAP_Bcast(&order, 1, MPI_UNSIGNED, aRoot, MPI_COMM_WORLD);

   // Resize mDims (does nothing for aRoot)
   this->mDims.resize(order);

   // Bcast extents
   void* ptr_dim = const_cast<void*>(static_cast<const void*>(mDims.data()));
   midas::mpi::detail::WRAP_Bcast(ptr_dim, order, MPI_UNSIGNED, aRoot, MPI_COMM_WORLD);
}

/**
 *
 **/
template<class T>
std::unique_ptr<BaseTensor<T> > BaseTensor<T>::ConstructFromType
   ( int type
   )
{
   switch(static_cast<typeID>(type))
   {
      case typeID::SIMPLE:
      {
         return std::unique_ptr<BaseTensor<T> >{ new SimpleTensor<T>() };
      }
      case typeID::SCALAR:
      {
         return std::unique_ptr<BaseTensor<T> >{ new Scalar<T>() };
      }
      case typeID::CANONICAL:
      {
         return std::unique_ptr<BaseTensor<T> >{ new CanonicalTensor<T>() };
      }
      default:
      {
         MIDASERROR("Type not known");
         return std::unique_ptr<BaseTensor<T> >{ nullptr };
      }
   }
}
#endif /* VAR_MPI */

//! (left-right).Norm()
template
   <  class T
   >
typename BaseTensor<T>::real_t diff_norm
   (  const BaseTensor<T>& left
   ,  const BaseTensor<T>& right
   )
{
   using real_t = typename BaseTensor<T>::real_t;

   BaseTensor<T>* diff = left.Clone();
   *(diff) -= right;
   real_t result = diff->Norm();
   delete diff;
   return result;
}

//! 
template
   <  class T
   >
typename BaseTensor<T>::real_t diff_norm_new
   (  const BaseTensor<T>& left
   ,  const BaseTensor<T>& right
   )
{
   return diff_norm_new(&left, left.Norm2(), &right, right.Norm2());
}
   
//! (left-right).Norm(). Right tensor is defined as the "target"-tensor.
template
   <  class T
   >
typename BaseTensor<T>::real_t diff_norm_new
   (  const BaseTensor<T>* const left
   ,  const typename BaseTensor<T>::real_t left_norm2
   ,  const BaseTensor<T>* const right
   ,  const typename BaseTensor<T>::real_t right_norm2
   ,  T dotprod 
   ,  const size_t tolerance
   )
{
   return std::sqrt(diff_norm2_new(left, left_norm2, right, right_norm2, dotprod, tolerance));
}

//! (left-right).Norm2(). Right tensor is defined as the "target"-tensor.
template
   <  class T
   >
typename BaseTensor<T>::real_t diff_norm2_new
   (  const BaseTensor<T>* const left
   ,  const typename BaseTensor<T>::real_t left_norm2
   ,  const BaseTensor<T>* const right
   ,  const typename BaseTensor<T>::real_t right_norm2
   ,  T dotprod 
   ,  const size_t tolerance
   )
{
   using real_t = typename BaseTensor<T>::real_t;
//   LOGCALL("calls");

   if (  !std::abs(dotprod)
      )
   {
//      LOGCALL("dot");
      dotprod = left->Dot(right);
   }
   
   // calculate diff norm
   real_t result = left_norm2 + right_norm2 - static_cast<real_t>(2.0)*std::real(dotprod);
   
   // check if result is sane
   if(libmda::numeric::float_neg(result)) 
   {
      if(libmda::numeric::float_numeq_zero(result, right_norm2, tolerance))
      {
         result = -result;
      }
      else
      {
//         LOGCALL("Safe");
         MidasWarning("Safe diff norm");
         
         result = safe_diff_norm2(left, right);
      }
   }

   return result;
}

/**
 *
 **/
template
   <  class T
   >
typename BaseTensor<T>::real_t safe_diff_norm2
   (  const BaseTensor<T>* const left
   ,  const BaseTensor<T>* const right
   )
{
   using real_t = typename BaseTensor<T>::real_t;
   auto alloc = left->GetAllocatorTp();

   auto size = left->TotalSize();
   auto left_ptr  = alloc.AllocateUniqueArray(size);
   auto right_ptr = alloc.AllocateUniqueArray(size);
   left->DumpInto(left_ptr.get());
   right->DumpInto(right_ptr.get());

   real_t safe_result(0.0);
   for(int idx = 0; idx < size; ++idx)
   {
      auto diff_elem = left_ptr[idx] - right_ptr[idx];
      safe_result += std::real(midas::math::Conj(diff_elem) * diff_elem);
   }

   return safe_result;
}

#endif /* BASETENSOR_IMPL_H_INCLUDED */
