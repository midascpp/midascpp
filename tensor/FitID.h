/**
************************************************************************
* 
* @file    FitID.h
*
* @date    19-02-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Functions for recompressing CP tensors using interpolative decomposition.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/


#ifndef FITID_H_INCLUDED
#define FITID_H_INCLUDED

#include <random>

#include "tensor/FitReport.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/GammaMatrix.h"

#include "util/RandomNumberGenerator.h"

#include "mmv/InterpolativeDecomposition.h"

#include "util/CallStatisticsHandler.h"

/**
 * Struct for holding input values for CP-ID recompression.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
struct CpIdInput
{
   //! Different distributions
   enum class distrID : int
   {
      NORMAL = 0
   };

   //! Max rank
   In mMaxRank = 10;

   //! # extra random rank-1 tensors for randomized algorithm
   In mAddRank = 5;

   //! Condition number for truncation of QR
   T mRcond = static_cast<T>(1.e-6);

   //! Use absolute value of largest neglected singular value for truncation
   bool mAbsConv = false;

   //! Distribution for generating random tensors and matrices
   distrID mDistribution = distrID::NORMAL;

   //! Perform SVD on Y matrix to check singular values
   bool mDoSvd = false;

   //! Use randomized algorithm for matrix ID
   bool mRandomizeMatrixId = false;

   //! IoLevel
   In mIoLevel = I_1;
   
   /**
    * Default constructor.
    **/
   CpIdInput()
   {
   }

   /**
    * Constructor from TensorDecompInfo.
    **/
   CpIdInput
      (  const TensorDecompInfo& aInfo
      )
      :  mMaxRank                   (const_cast<TensorDecompInfo&>(aInfo)["CPIDMAXRANK"].template get<In>())
      ,  mAddRank                   (const_cast<TensorDecompInfo&>(aInfo)["CPIDADDRANK"].template get<In>())
      ,  mRcond                     (const_cast<TensorDecompInfo&>(aInfo)["CPIDRCOND"].template get<Nb>())
      ,  mAbsConv                   (const_cast<TensorDecompInfo&>(aInfo)["CPIDABSCONV"].template get<bool>())
      ,  mDistribution              (const_cast<TensorDecompInfo&>(aInfo)["CPIDDISTRIBUTION"].template get<distrID>())
      ,  mDoSvd                     (const_cast<TensorDecompInfo&>(aInfo)["CPIDDOSVD"].template get<bool>())
      ,  mRandomizeMatrixId         (const_cast<TensorDecompInfo&>(aInfo)["CPIDRANDOMIZEMATRIXID"].template get<bool>())
      ,  mIoLevel                   (const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].template get<In>())
   {
   }

#define SETTER(NAME, TYPE) \
   CpIdInput& Set##NAME(TYPE a##NAME) \
   { \
      m##NAME = a##NAME; \
      return *this; \
   }
   
   SETTER(MaxRank, In);
   SETTER(AddRank, In);
   SETTER(Rcond, T);
   SETTER(AbsConv, bool);
   SETTER(Distribution, distrID);
   SETTER(DoSvd, bool);
   SETTER(RandomizeMatrixId, bool);
   SETTER(IoLevel, In);

#undef SETTER
};


/**
 * Function for recompressing a CP tensor using interpolative decomposition.
 *
 * @param aTarget          The target tensor.
 * @param aFitTensor       The fitting tensor. Stores the result. Does not need to be initialized.
 * @param aInput           CpIdInput struct.
 *
 * @return
 *    FitReport
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPID
   (  const NiceTensor<T>& aTarget
   ,  NiceTensor<T>& aFitTensor
   ,  const CpIdInput<T>& aInput
   )
{
   LOGCALL("calls");
   // Only works for CANONICAL at the moment
   assert(aTarget.Type() == BaseTensor<T>::typeID::CANONICAL);
   
   // Set up some sizes
   auto k = aInput.mMaxRank;
   auto l = k + aInput.mAddRank;
   auto distr = aInput.mDistribution;
   auto do_svd = aInput.mDoSvd;
   const auto& dims = aTarget.GetDims();
   auto ndim = aTarget.NDim();
   auto rcond = aInput.mRcond;
   bool absconv = aInput.mAbsConv;
   auto rand = aInput.mRandomizeMatrixId;
   auto io = aInput.mIoLevel;

   // Create random CP tensor
   auto nice_rand_tens = NiceCanonicalTensor<T>(dims, l);
   auto rand_tens = static_cast<CanonicalTensor<T>* const>(nice_rand_tens.GetTensor());
   switch   (  distr
            )
   {
      case CpIdInput<T>::distrID::NORMAL:
      {
         LOGCALL("create random");
         // Create normal distribution with zero mean and unit variance
         std::normal_distribution norm_dist(0., 1.);

         // Set mode-matrix elements
         T** mm = rand_tens->GetModeMatrices();
         for(In idim=0; idim<ndim; ++idim)
         {
            auto len = l*dims[idim];
            for(In i=0; i<len; ++i)
            {
               mm[idim][i] = norm_dist(midas::util::detail::get_mersenne());
            }
         }

         break;
      }
      default:
      {
         MIDASERROR("No match for given distribution type...");
      }
   }

   // Calculate Y matrix
   unsigned target_rank = 0;
   unsigned y_size = 0;
   T y_norm2 = C_0;
   std::unique_ptr<T[]> y_mat = nullptr;
   {
      LOGCALL("construct y matrix");
      // Init gamma intermeds for constructing Y matrix
      GeneralGammaIntermediates<T> gamma  (  rand_tens
                                          ,  static_cast<const CanonicalTensor<T>* const>(aTarget.GetTensor())
                                          );

      // Get Y matrix
      target_rank = gamma.RankRight();
      if (  target_rank < k
         )
      {
         MIDASERROR("FitCPID: Rank of target is < output rank!");
      }
      if (  target_rank < l
         )
      {
         MidasWarning("FitCPID: l > target_rank");
      }
      y_size = gamma.Rank2();
      y_mat = std::make_unique<T[]>(y_size);
      gamma.FullGammaMatrix(y_mat); // Calculate full gamma and return norm2
      
      for(size_t i=0; i<y_size; ++i)
      {
         y_norm2 += y_mat[i]*y_mat[i];
      }
   }

   // DEBUG: Check possible accuracy by performing SVD
   if (  do_svd
      )
   {
      LOGCALL("debug svd");
      // Make a copy of Y
      auto y_copy = std::make_unique<T[]>(y_size);
      for(In i=I_0; i<y_size; ++i)
      {
         y_copy[i] = y_mat[i];
      }

      auto svd = GESDD(y_copy.get(), l, target_rank, 'N');

      auto s_0 = svd.s[0];
      auto s_k = svd.s[k-1];
      auto s_kp1 = ( k >= l || k >= target_rank ) ? static_cast<T>(0.) : svd.s[k];

      // SVD rank
      auto svd_rank = Rank(svd, rcond);

      // Error estimate
      auto c1 = std::sqrt( C_4*k*(target_rank-k) + C_1); 
      auto err = s_kp1 * std::sqrt(static_cast<T>(target_rank)) * c1;

      Mout  << " SVD check in CP-ID decomposition:\n"
            << "    Tensor dims:       " << aTarget.ShowDims() << "\n"
            << "    Y norm2:           " << y_norm2 << "\n"
            << "    Rcond:             " << rcond << "\n"
            << "    SVD error bound:   " << err << "\n"
            << "    SVD rank:          " << svd_rank << "\n"
            << "    SVD smax:          " << s_0 << "\n"
            << "    SVD smin:          " << s_k << "\n"
            << "    SVD srp1:          " << s_kp1 << "\n"
            << std::flush;
   }

   // Perform matrix ID on Y matrix
   bool save_ac = false;
   midas::ID_struct<T> mat_id;
   {
   LOGCALL("matrix id");
   mat_id = midas::InterpolativeDecomposition(y_mat.get(), y_norm2, l, target_rank, rcond, k, rand, save_ac, absconv, io);
   }

   // Set size of aFitTensor
   aFitTensor = NiceCanonicalTensor<T>(aTarget.GetDims(), mat_id.mRank);

   // Store result in aFitTensor
   // Take the rank-1 terms corresponding to the leading columns in the matrix ID and scale them by 
   // \alpha_m = \sum_{l=1}^{r_target} X_{m l}, where X is the coefficient matrix of the ID.
   {
   LOGCALL("construct result");
   const auto& cp_target = *static_cast<CanonicalTensor<T>*>(aTarget.GetTensor());
   for(In irank=I_0; irank<mat_id.mRank; ++irank)
   {
      T coef = static_cast<T>(0.);

      // Calculate scaling coefficient
      for(In ir=I_0; ir<target_rank; ++ir)
      {
         coef += mat_id.XElem(irank, ir);
      }

      // Set rank-1 term
      auto& cp_fit = *static_cast<CanonicalTensor<T>*>(aFitTensor.GetTensor());
      cp_fit.SetRankOneTerm(irank, mat_id.mP[irank], cp_target, coef);
   }
   }

   // Return fit report
   auto tensor_err_bound = mat_id.mErrorBound*std::sqrt(target_rank);
   return FitReport<T>(tensor_err_bound, C_0, 0);
}


#endif /* FITID_H_INCLUDED */
