#ifndef PRINT_UNIQUE_PTR_H_INCLUDED
#define PRINT_UNIQUE_PTR_H_INCLUDED

#include <memory>
#include <iostream>

template<class T>
void print_unique_ptr
   ( const std::unique_ptr<T[]>& ptr
   , unsigned size
   )
{
   for(unsigned i = 0; i < size; ++i) 
   {
      std::cout << ptr[i] << std::endl;
   }
}

#endif /* PRINT_UNIQUE_PTR_H_INCLUDED */
