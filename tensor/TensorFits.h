#ifndef TENSORFITS_H_INCLUDED
#define TENSORFITS_H_INCLUDED

#include "tensor/NiceTensor.h"
#include "tensor/FitReport.h"
#include "tensor/FitALS.h"
#include "tensor/FitNCG.h"
#include "tensor/FitASD.h"
#include "tensor/CanonicalTensor.h"

/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPALS
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const unsigned int maxiter
   ,  const T maxerr
   ,  const T rcond = 1e-12
   )
{
   return dynamic_cast<CanonicalTensor<T>* >(result.GetTensor())->FitALS(*target.GetTensor(), target_norm2, maxiter, maxerr, rcond);
}

/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPALS
   (  const NiceTensor<T>& target_tensor
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const CpAlsInput<T>& input
   )
{
   return FitALS(target_tensor.GetTensor(), target_norm2, dynamic_cast<CanonicalTensor<T>* >(result.GetTensor()), input);
}


/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPPALS
   (  const NiceTensor<T>& target_tensor
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const CpAlsInput<T>& input
   )
{
   return FitPALS(target_tensor.GetTensor(), target_norm2, dynamic_cast<CanonicalTensor<T>* >(result.GetTensor()), input);
}

/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPSteepestDescent
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const unsigned int maxiter
   ,  const T maxerr
   )
{
   return dynamic_cast<CanonicalTensor<T>* >(result.GetTensor())->FitSteepestDescent(*target.GetTensor(), maxiter, maxerr);
}

/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPNCG
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const unsigned int maxiter
   ,  const T maxerr
   )
{
   return dynamic_cast<CanonicalTensor<T>* >(result.GetTensor())->FitNCG(*target.GetTensor(), maxiter, maxerr);
}


/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPNCG
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const CpNCGInput<T>& input
   )
{
   return FitNCG(target.GetTensor(), target_norm2, dynamic_cast<CanonicalTensor<T>* >(result.GetTensor()), input);
}

/*!
 *
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPPASD
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const CpAsdInput<T>& input
   )
{
   return FitPASD(target.GetTensor(), target_norm2, dynamic_cast<CanonicalTensor<T>*>(result.GetTensor()), input);
}

/**
 *
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitCPASD
   (  const NiceTensor<T>& target
   ,  const T target_norm2
   ,  NiceTensor<T>& result
   ,  const CpAsdInput<T>& input
   )
{
   return FitASD(target.GetTensor(), target_norm2, dynamic_cast<CanonicalTensor<T>*>(result.GetTensor()), input);
}

#endif /* TENSORFITS_H_INCLUDED */
