#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "NiceTensor.h"
#include "NiceTensor_Impl.h"

template class NiceTensor<float >;
template class NiceTensor<double>;
template class NiceTensor<std::complex<float>>;
template class NiceTensor<std::complex<double>>;

//! Multiply with scalar from the left.
template NiceTensor<float > operator*(const float& , const NiceTensor<float >&);
template NiceTensor<double> operator*(const double&, const NiceTensor<double>&);
template NiceTensor<std::complex<float> > operator*(const std::complex<float>& , const NiceTensor<std::complex<float> >&);
template NiceTensor<std::complex<double>> operator*(const std::complex<double>&, const NiceTensor<std::complex<double>>&);

//! "Low-level" contraction. Use contract(const NiceContractor<T>&, const NiceContractor<T>&) instead.
template NiceTensor<float> contract(const NiceTensor<float>&, const std::vector<unsigned int>&,
                                    const NiceTensor<float>&, const std::vector<unsigned int>&, bool, bool);
template NiceTensor<double> contract(const NiceTensor<double>&, const std::vector<unsigned int>&,
                                    const NiceTensor<double>&, const std::vector<unsigned int>&, bool, bool);
template NiceTensor<std::complex<float>> contract(const NiceTensor<std::complex<float>>&, const std::vector<unsigned int>&,
                                    const NiceTensor<std::complex<float>>&, const std::vector<unsigned int>&, bool, bool);
template NiceTensor<std::complex<double>> contract(const NiceTensor<std::complex<double>>&, const std::vector<unsigned int>&,
                                    const NiceTensor<std::complex<double>>&, const std::vector<unsigned int>&, bool, bool);

//! Dot product (total contraction).
template float  dot_product(const NiceTensor<float >&, const NiceTensor<float >&);
template double dot_product(const NiceTensor<double>&, const NiceTensor<double>&);
template std::complex<float>  dot_product(const NiceTensor<std::complex<float >>&, const NiceTensor<std::complex<float >>&);
template std::complex<double> dot_product(const NiceTensor<std::complex<double>>&, const NiceTensor<std::complex<double>>&);

// Non-conj dot product
template float  sum_prod_elem(const NiceTensor<float >&, const NiceTensor<float >&);
template double sum_prod_elem(const NiceTensor<double>&, const NiceTensor<double>&);
template std::complex<float>  sum_prod_elem(const NiceTensor<std::complex<float >>&, const NiceTensor<std::complex<float >>&);
template std::complex<double> sum_prod_elem(const NiceTensor<std::complex<double>>&, const NiceTensor<std::complex<double>>&);

////! Dot product (total contraction).
//template float  safe_dot_product(const NiceTensor<float >&, const NiceTensor<float >&);
//template double safe_dot_product(const NiceTensor<double>&, const NiceTensor<double>&);

//! 
template NiceTensor<float> make_nice_simple(const std::vector<unsigned>&, float* );
template NiceTensor<double> make_nice_simple(const std::vector<unsigned>&, double*);
template NiceTensor<std::complex<float >> make_nice_simple(const std::vector<unsigned>&, std::complex<float>* );
template NiceTensor<std::complex<double>> make_nice_simple(const std::vector<unsigned>&, std::complex<double>*);

//!
template NiceTensor<float > make_nice_random_simple(const std::vector<unsigned>&);
template NiceTensor<double> make_nice_random_simple(const std::vector<unsigned>&);
template NiceTensor<std::complex<float >> make_nice_random_simple(const std::vector<unsigned>&);
template NiceTensor<std::complex<double>> make_nice_random_simple(const std::vector<unsigned>&);

//!
template NiceTensor<float> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);
template NiceTensor<double> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);
template NiceTensor<std::complex<float >> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);
template NiceTensor<std::complex<double>> random_canonical_tensor(const std::vector<unsigned int>&, const unsigned int);

//!
template std::ostream& operator<<(std::ostream&, const NiceTensor<float >&);
template std::ostream& operator<<(std::ostream&, const NiceTensor<double>&);
template std::ostream& operator<<(std::ostream&, const NiceTensor<std::complex<float >>&);
template std::ostream& operator<<(std::ostream&, const NiceTensor<std::complex<double>>&);

//! ODE error norms
#define INSTANTIATE_NICETENSOR_ODENORMS(T, U)   \
template typename NiceTensor<T>::real_t OdeNorm2(const NiceTensor<T>&, U, U, const NiceTensor<T>&, const NiceTensor<T>&); \
template typename NiceTensor<T>::real_t OdeMeanNorm2(const NiceTensor<T>&, U, U, const NiceTensor<T>&, const NiceTensor<T>&); \
template typename NiceTensor<T>::real_t OdeMaxNorm2(const NiceTensor<T>&, U, U, const NiceTensor<T>&, const NiceTensor<T>&);

INSTANTIATE_NICETENSOR_ODENORMS(float, float);
INSTANTIATE_NICETENSOR_ODENORMS(double, double);
INSTANTIATE_NICETENSOR_ODENORMS(std::complex<float>, float);
INSTANTIATE_NICETENSOR_ODENORMS(std::complex<double>, double);

#undef INSTANTIATE_NICETENSOR_ODENORMS

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
