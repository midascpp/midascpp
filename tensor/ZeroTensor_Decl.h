#ifndef ZEROTENSOR_DECL_H_INCLUDED
#define ZEROTENSOR_DECL_H_INCLUDED

#include "tensor/BaseTensor.h"
#include "util/Io.h"

// Forward declarations required by friends
template <class T>
class SimpleTensor;

template <class T>
class CanonicalTensor;


template<class T>
class ZeroTensor
   :  public BaseTensor<T>
{
   private:

      ///> private, default copy constructor. Copies should be made through Clone().
      ZeroTensor(const ZeroTensor&) = default;

      ///> private, default move constructor
      ZeroTensor(ZeroTensor&&) = default;

      ///> private, deleted copy assignment (default if needed)
      ZeroTensor& operator=(const ZeroTensor&) = delete;

      ///> private, deleted move assignment (default if needed)
      ZeroTensor& operator=(ZeroTensor&&) = delete;

      //! Assign (can be done with other ZeroTensor)
      bool Assign
         (  const BaseTensor<T> * const aOther
         )  override;

      ///>
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indicespos_inner_left
         ,  const std::vector<unsigned int>& indicespos_outer_left
         ,  const BaseTensor<T>&             right
         ,  const std::vector<unsigned int>& indicespos_inner_right
         ,  const std::vector<unsigned int>& indicespos_outer_right
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indicespos_result_left
         ,  const std::vector<unsigned int>& indicespos_result_right
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override;

      //! Called by ContractInternal().
      BaseTensor<T>* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //! Called by Reorder(), who parses its input and calculates \p neworder and \p newdims.
      /*! @param neworder New position assigned to a given dimension, e.g. {1,2,0}
       *                  means that the first dimension becomes the second one,
       *                  the second dimension becomes the third one, and the third
       *                  dimensions becomes the first one.
       *  @param newdims  Dimensions of the output tensor.
       *
       */
      BaseTensor<T>* Reorder_impl(const std::vector<unsigned int>& neworder,
            const std::vector<unsigned int>& newdims) const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //! Called by Slice().
      BaseTensor<T>* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>&) const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //!
      void ConjugateImpl
         (
         )  override
      {
         return;
      }

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;

      ///> constructor from dimensions.
      ZeroTensor(const std::vector<unsigned>&);

      ///> default destructor.
      ~ZeroTensor() = default;

      ///> Clone.
      ZeroTensor* Clone() const override;

      ///> Type
      typename BaseTensor<T>::typeID Type() const override;

      ///> Type as string.
      std::string ShowType() const override;

      //!
      bool SanityCheck() const override
      {
         return true;
      }

      //! Dump tensor into pointer
      void DumpInto
         (  T* apPtr
         )  const override
      {
         // DEBUG
         if (  this->TotalSize() > 1
            )
         {
            MidasWarning("ZeroTensor::DumpInto called for tensor of dims: " + this->ShowDims());
         }

         for(In i=I_0; i<this->TotalSize(); ++i)
         {
            apPtr[i] = T(0.0);
         }
      }

      ///> Friends.
      friend class SimpleTensor<T>;
      friend class CanonicalTensor<T>;

      /*============== CONTRACTIONS ==============*/
      ///> Contract one dimension away with a vector. See BaseTensor::ContractDown.
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const override;
      ///> Transform a dimension with a matrix. See BaseTensor::ContractForward.
      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override;
      ///> Insert a dimension with a vector. See BaseTensor::ContractUp.
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override;

      /*============== UTILITY ===================*/ 
      ///> Zero the ZeroTensor, i.e. do nothing.
      void Zero() override;

      ///> Scale the ZeroTensor, i.e. do nothing.
      void Scale(const T&) override;

      //! Absolute value
      void Abs() override;

      void Axpy(const BaseTensor<T>&, const T&) override;

      /*============== OPERATORS =================*/ 
      ZeroTensor* operator*=(const T) override { MIDASERROR("NOT Implemented"); return nullptr; }
      ZeroTensor* operator+=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }
      ZeroTensor* operator*=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }
      ZeroTensor* operator/=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }

      /*============== IO ========================*/
      std::ostream& write_impl(std::ostream& output) const override { MIDASERROR("Not Implemented."); return output; }

      /*============== DOT  ========================*/
      dot_t Dot(const BaseTensor<T>* const) const override;
};

#endif /* ZEROTENSOR_DECL_H_INCLUDED */
