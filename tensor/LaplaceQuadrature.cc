/**
************************************************************************
* 
* @file                LaplaceQuadrature.cc
*
* Created:             14-02-2016
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Implements functions for LaplaceQuadrature
* 
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include<string>
#include<set>
#include<fstream>

#include"LaplaceQuadrature.h"

namespace midas
{
namespace tensor
{
namespace detail
{
/*
 * @function generate_file_name()   Generates the name of the relevant Laplace Quadrature file
 * @param    aNumPoints             The number of grid points
 * @param    aR                     The interval length
 */
const std::string generate_file_name(const In aNumPoints, const Nb aR)
{
   // Create the name of the datafile
   std::string s_numpoints = (aNumPoints < I_10) ? "0"+std::to_string(aNumPoints) : std::to_string(aNumPoints);

   // Obtain aR in xEy format, e.g. 3E2 for aR closest to 3.e2
   Nb x = aR;
   In power = I_0;
   while(std::round(x)>=C_10)
   {
      x/=I_10;
      ++power;
   }

   std::string s_interval = std::to_string(static_cast<In>(std::round(x)))+"E"+std::to_string(power);

   if(s_interval == "1E0")
   {
      MidasWarning("LaplaceQuadrature: Interval too short for datafile to exist. Trying smallest possible interval, but accuracy may be very poor!");
      s_interval = "2E0";
   }

   std::string s_filename = "1_xk" + s_numpoints + "_" + s_interval;
   
   return s_filename;
}

} /* namespace detail */
} /* namespace tensor */
} /* namespace midas */


// Pre-compiled templates
#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "tensor/LaplaceQuadrature_Impl.h"


#define INSTANTIATE_LAPLACE(T) \
template class LaplaceQuadratureBase<T>; \
template class RealLaplaceQuadrature<T>; \
template class ComplexLaplaceQuadrature<T>; \
template class LaplaceQuadrature<T>; \


INSTANTIATE_LAPLACE(float);
INSTANTIATE_LAPLACE(double);

#undef INSTANTIATE_LAPLACE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
