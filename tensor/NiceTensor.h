#ifndef NICETENSOR_H_INCLUDED
#define NICETENSOR_H_INCLUDED

// Include declaration
#include "tensor/ForwardDecl.h"

#include "NiceTensor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "NiceTensor_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* NICETENSOR_H_INCLUDED */
