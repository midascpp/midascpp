#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "ContractionMatrix.h"
#include "ContractionMatrix_Impl.h"

// define
#define INSTANTIATE_CONTRACTIONMATRIX(T) \
template class ContractionMatrix<T>; \
\
template std::ostream& operator<<(std::ostream&, const ContractionMatrix<T>&);

// concrete instantiations
INSTANTIATE_CONTRACTIONMATRIX(float)
INSTANTIATE_CONTRACTIONMATRIX(double)
INSTANTIATE_CONTRACTIONMATRIX(std::complex<float>)
INSTANTIATE_CONTRACTIONMATRIX(std::complex<double>)

#undef INSTANTIATE_CONTRACTIONMATRIX

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
