#include<stdexcept>

#include"util/read_write_binary.h"

template <class E>
void
assert_same_size(const std::vector<E>& v1, const std::vector<E>& v2)
{
   if(v1.size() != v2.size() )
   {
      throw std::runtime_error("Passed two non-conforming vectors");
   }
}

template <class T>
std::vector<NiceTensor<T>> operator+(const std::vector<NiceTensor<T>>& left,
                                     const std::vector<NiceTensor<T>>& right)
{
   assert_same_size(left, right);
   std::vector<NiceTensor<T>> result;

   for(int i=0;i<left.size();++i)
   {
      result.push_back(left[i]+right[i]);
   }
   return result;
}

template <class T>
std::vector<NiceTensor<T>> operator*(const std::vector<NiceTensor<T>>& left,
                                     const std::vector<NiceTensor<T>>& right)
{
   assert_same_size(left, right);
   std::vector<NiceTensor<T>> result;

   for(int i=0;i<left.size();++i)
   {
      result.push_back(left[i]*right[i]);
   }
   return result;
}

template <class T>
std::vector<NiceTensor<T>> operator*(const T k,
                                     const std::vector<NiceTensor<T>>& self)
{
   std::vector<NiceTensor<T>> result;
   for(int i=0;i<self.size();++i)
   {
      result.push_back(k * self[i]);
   }
   return result;
}

template <class T>
std::vector<NiceTensor<T>> operator*(const std::vector<NiceTensor<T>>& self,
                                     const T k)
{
   return k*self;
}

template <class T>
T dot_product(const std::vector<NiceTensor<T>>& left,
              const std::vector<NiceTensor<T>>& right)
{
   assert_same_size(left, right);
   T result=0.;

   for(int i=0;i<left.size();++i)
   {
      result+=dot_product(left[i], right[i]);
   }
   return result;
}

template <class T>
std::ostream& write_NiceTensor_vector(const std::vector<NiceTensor<T>>& vector,
                                      std::ostream& output)
{
   write_binary(vector.size(), output);
   for(const auto& tensor: vector)
      tensor.Write(output);
   return output;
}

template <class T>
std::vector<NiceTensor<T>> read_NiceTensor_vector(std::istream& input)
{
   std::vector<NiceTensor<T>> result;

   typename std::vector<NiceTensor<T>>::size_type vector_size;  
   read_binary(vector_size, input);

   for(int i=0;i<vector_size;++i)
      result.push_back(NiceTensor<T>(input));
   
   return result;
}

template <template<class,class> class C, class T, class Alloc>
std::vector<NiceTensor<T>> linear_combination(
      const std::vector<T>& coeffs,
      const C<std::vector<NiceTensor<T>>,Alloc>& solutions
      )
{
   std::vector<NiceTensor<T>> result;

   // Initialize result by adding the first iteration
   for(auto& t: solutions.front())
   {
      result.push_back(coeffs.front() * t);
   }

   // Add the rest of the iterations
   auto solution=solutions.begin();
   ++solution;
   int i=1;
   for(;solution!=solutions.end();++solution)
   {
      auto tensor_in=solution->begin();
      for(auto& tensor_out: result)
      {
         tensor_out += coeffs[i] * (*tensor_in);
         ++tensor_in;
      }
      ++i;
   }
   return result;
}
