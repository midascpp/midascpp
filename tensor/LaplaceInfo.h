#ifndef LAPLACEINFO_H_INCLUDED
#define LAPLACEINFO_H_INCLUDED

#include <map>
#include <set>
#include "libmda/util/any_type.h"
#include "inc_gen/Const.h"

namespace midas
{
namespace tensor
{

/**
 * Class to hold somewhat processed Laplace input.
 * Used to construct object of type LaplaceQuadrature.
 **/
class LaplaceInfo
   : public std::map<std::string, libmda::util::any_type>
{
   public:
      ///> define what a set of TensorDecompInfo's means
      using Set = std::set<LaplaceInfo>;

      //! Default constructor
      LaplaceInfo() = default;

      //! Constructor from type. Sets all necessary defaults.
      LaplaceInfo
         (  const std::string& type
         ,  Nb delta = C_I_10_1
         );
};

//! Output
std::ostream& operator<<
   (  std::ostream& os
   ,  const LaplaceInfo& info
   );

} /* namespace tensor */
} /* namespace midas */

using midas::tensor::LaplaceInfo;

#endif /* LAPLACEINFO_H_INCLUDED */
