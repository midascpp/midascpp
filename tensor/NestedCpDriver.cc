/**
************************************************************************
* 
* @file    NestedCpDriver.cc
*
* @date    28-03-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of NestedCpDriver methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/NestedCpDriver.h"

#include <cassert>

#include "util/Io.h"
#include "util/CallStatisticsHandler.h"
#include "tensor/TensorFits.h"
#include "tensor/TensorSum.h"

namespace midas
{
namespace tensor
{

//! Register NestedCpDriver
detail::TensorDecompDriverRegistration<NestedCpDriver> registerNestedCpDriver("NESTEDCP");


/**
 * Check that a given tensor is decomposed by this driver.
 *
 * @param aTensor
 *    The target tensor.
 **/
bool NestedCpDriver::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool order_ok  =  mDecompToOrder > 0 
                  ?  (In(aTensor.NDim()) >= mDecompToOrder)
                  :  true;

   auto type = aTensor.Type();

   bool type_ok   =  (  type == BaseTensor<Nb>::typeID::SIMPLE
                     || type == BaseTensor<Nb>::typeID::CANONICAL
                     || type == BaseTensor<Nb>::typeID::SUM
                     || type == BaseTensor<Nb>::typeID::DIRPROD
                     );

   return order_ok && type_ok;
}

/**
 * Get decomp threshold
 *
 * @return
 *    The threshold
 **/
Nb NestedCpDriver::GetThreshold
   (
   )  const
{
   return mNestedCpResThreshold;
}

/**
 * Return type of decomp driver.
 **/
std::string NestedCpDriver::TypeImpl
   (
   )  const
{
   return std::string("NestedCp");
}


/**
 * Check if tensor should be screened (fitted to rank-0)
 *
 * @param aTensor       The target tensor
 * @param aNorm2        Norm2 of target
 * @param aThresh       Decomposition threshold
 * @return
 *    True if tensor should be screened
 **/
bool NestedCpDriver::ScreenImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   if (  libmda::numeric::float_neg(aNorm2)
      )
   {
      return false;
   }
   else
   {
      Nb thresh   =  aThresh > C_0
                  ?  aThresh
                  :  this->mNestedCpResThreshold;
   
      // Check norm
      Nb norm = std::sqrt(aNorm2);
      if (  norm < thresh
         )
      {
         return true;
      }
   
      return false;
   }
}


/**
 * Decompose a tensor.
 *
 * @param aTensor       The target tensor
 * @param aNorm2        Norm2 of target tensor
 * @param aThresh       The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> NestedCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   //LOGCALL("begin");

   // Declarations
   const auto norm2 = aNorm2;
   const auto norm  = std::sqrt(norm2);
   const auto& dims = aTensor.GetDims();
   const auto ndim = aTensor.NDim();
   Nb thresh   =  aThresh > C_0
               ?  aThresh
               :  this->mNestedCpResThreshold;

   // Decompose low-order tensors
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()
      || (  this->mEffectiveLowOrderSvd
         && std::count_if(dims.begin(), dims.end(), [](unsigned i){ return (i != 1); }) == 2
         )
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("NestedCpDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Get target rank
   auto target_rank = this->TargetRank(aTensor);

   // Theoretical max rank and the maximum fitting rank
   In theoretical_max_rank = aTensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
   In max_rank = std::min<In>(this->mNestedCpMaxRank, theoretical_max_rank);

   // Check max_rank against target_rank for recompressions
   if (  target_rank > 0   )
   {
      max_rank = std::min<In>(max_rank, target_rank);
   }

   // Rank incr
   In rank_incr = this->mNestedCpRankIncr; 
   if (  ndim < I_3 )
   {
      rank_incr = I_1;
   }
   else if  (  this->mNestedCpScaleRankIncr )
   {
      rank_incr = (ndim-I_2)*this->mNestedCpRankIncr;
   }
   else if  (  this->mNestedCpAdaptiveRankIncr   )
   {
      rank_incr = (ndim-I_2)* *(std::min_element(dims.begin(), dims.end())) * this->mNestedCpRankIncr;
   }
   
   // Init fit report
   auto fit_report = FitReport<Nb>(C_0, norm, 0);

   // Init result, update, and residual
   NiceTensor<Nb> result( new CanonicalTensor<Nb>(dims, 0) );
   NiceTensor<Nb> update;
   NiceTensor<Nb> residual( new TensorSum<Nb>(dims) );
   residual += aTensor;

   auto res_norm2 = norm2;
   auto res_norm  = norm;

   // Begin rank loop
   unsigned rank = 0;
   unsigned iter = 0;
   while (  true  )
   {
      //LOGCALL("rank loop");

      // Set rank incr and accumulated rank
      rank_incr   =  rank + rank_incr > max_rank
                  ?  max_rank - rank
                  :  rank_incr;
      rank += rank_incr;

      // Check if rank = target_rank
      if (  target_rank > 0
         && rank == target_rank
         )
      {
         result = this->ConstructCanonicalTarget(aTensor);
         res_norm = C_0;
         res_norm2 = C_0;
         break;
      }

      // Output information
      if (  this->mIoLevel >= 5  )
      {
         Mout  << "### NestedCpDriver iteration " << ++iter << " ###\n"
               << "  # Adding rank-" << rank_incr << " tensor.\n"
               << "  # Accumulated rank = " << rank << "\n"
               << std::flush;
      }

      // Make start guess for update
      {
      //LOGCALL("make start guess");
      this->mGuesser->MakeStartGuess(residual, res_norm, update, rank_incr);
      }

      // Fit update
      {
      //LOGCALL("fit update");
      fit_report = this->mCpFitter->Decompose(residual, res_norm2, update, thresh);
      }
      if (  this->mBalance
         )
      {
         update.StaticCast<CanonicalTensor<Nb>>().BalanceModeVectors();
      }

      if (  fit_report.error > res_norm
         )
      {
         MidasWarning("NestedCpDriver: Update error greater than residual norm!");
      }

      // Print fit report
      if (  this->mIoLevel >=7   )
      {
         Mout << fit_report << std::endl;
      }

      // Update result
      {
      //LOGCALL("update result");
      result += update;
      }

      // Update residual and norms
      {
      //LOGCALL("update residual");
      residual -= update;
      }

      // Re-calculate residual norm
      if (  this->mSafeResidualNorm )
      {
         //LOGCALL("update residual norms");

         res_norm2 = residual.Norm2();
         res_norm  = std::sqrt(res_norm2);
      }
      else
      {
         res_norm = fit_report.error;
         res_norm2 = res_norm*res_norm;
      }

      if (  this->mIoLevel >= 5  )
      {
         Mout  << "  # Residual norm = " << res_norm << "\n"
               << std::endl;
      }

      // Check convergence and break loop
      if (  res_norm < thresh
         || rank == max_rank
         )
      {
         break;
      }
   }

   // Fit the whole tensor if not converged within an order of magnitude
   if (  this->mAllowRefit
      && res_norm > thresh*C_10
      )
   {
      MidasWarning("NestedCpDriver: Refit full tensor!");
      Mout << " Error before refit = " << res_norm << std::endl;
      fit_report = this->mCpFitter->Decompose(aTensor, aNorm2, result, thresh);
      res_norm = fit_report.error;
      Mout << " Error after refit  = " << res_norm << std::endl;
   }

   // Print fit report
   if (  this->mPrintFitReport   )
   {
      Mout << " +------------- Final fit of NestedCpDriver ---------------+\n" 
           << fit_report 
           << " +-------------------------------------------+\n" << std::endl;
   }

   // Print error if fit is not converged
   if (  res_norm > thresh   )
   {
      Mout  << "Bad NestedCp fit of " << ndim << ".-order "+aTensor.ShowType()+" to rank " << rank << ":\n"
            << "\tDims:          " << aTensor.ShowDims() << "\n"
            << "\tThresh:        " << thresh << "\n"
            << "\tRel. thresh:   " << thresh / norm << "\n"
            << "\tError:         " << res_norm << "\n"
            << "\tNorm:          " << norm << "\n"
            << "\tTarget rank:   " << target_rank << "\n"
            << std::endl;;
   }

   return result;
}


/**
 * Decompose a tensor using an argument guess.
 *
 * NB:   The use of an argument guess is awkward in the NestedCp algorithm.
 *       It is therefore ignored in the current implementation.
 *
 * @param aTensor       The target tensor
 * @param aNorm2        Norm2 of target tensor
 * @param aGuess        The guess tensor
 * @param aThresh       The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> NestedCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   MidasWarning("NestedCpDriver: Argument guess is ignored in decomposition!");

   // Simply perform standard decomposition
   return this->DecomposeImpl(aTensor, aNorm2, aThresh);
}



/**
 * Constructor from TensorDecompInfo.
 *
 * @param aInfo
 *    The TensorDecompInfo
 **/
NestedCpDriver::NestedCpDriver
   (  const TensorDecompInfo& aInfo
   )
   :  TensorDecompDriverBase(aInfo)
   ,  mNestedCpMaxRank              (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPMAXRANK"].get<In>())
   ,  mNestedCpRankIncr             (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPRANKINCR"].get<In>())
   ,  mNestedCpScaleRankIncr        (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPSCALERANKINCR"].get<bool>())
   ,  mNestedCpAdaptiveRankIncr     (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPADAPTIVERANKINCR"].get<bool>())
   ,  mNestedCpResThreshold         (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPRESTHRESHOLD"].get<Nb>())
   ,  mSafeResidualNorm             (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPSAFERESIDUALNORM"].get<bool>())
   ,  mBalance                      (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPBALANCEMODEVECTORS"].get<bool>())
   ,  mAllowRefit                   (const_cast<TensorDecompInfo&>(aInfo)["NESTEDCPALLOWREFIT"].get<bool>())
   ,  mPrintFitReport               (false)
{
}

} /* namespace tensor */
} /* namespace midas */
