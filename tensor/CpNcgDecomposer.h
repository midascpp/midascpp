/**
************************************************************************
* 
* @file    CpNcgDecomposer.h
*
* @date    18-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Tensor decomposer for fitting a tensor to a fixed rank using 
*     non-linear conjugate gradient methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CPNCGDECOMPOSER_H_INCLUDED
#define CPNCGDECOMPOSER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"

namespace midas
{
namespace tensor
{

class CpNcgDecomposer
   :  public detail::ConcreteCpDecomposerBase
{
   private:
      //! Threshold for CPNCG algorithm (gradient norm)
      Nb mCpNcgThreshold;

      //! Relative threshold for CPNCG algorithm
      Nb mCpNcgRelativeThreshold;

      //! Relative threshold for checkiter in FitNCG
      Nb mCpNcgRelativeCheckThresh;

      //! Reference to TensorDecompInfo
      const TensorDecompInfo& mInfo; 


      //! Implementation of CheckDecomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;
      
      //! Implementation of Decompose with argument guess.
      FitReport<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Implementation of Type
      std::string TypeImpl
         (
         )  const override;

   public:
      //! c-tor from TensorDecompInfo
      CpNcgDecomposer
         (  const TensorDecompInfo&
         );
};
   
} /* namespace tensor */
} /* namespace midas */


#endif /* CPNCGDECOMPOSER_H_INCLUDED */
