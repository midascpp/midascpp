#ifndef MATRIX_CONTRACTOR_IMPL_H_INCLUDED
#define MATRIX_CONTRACTOR_IMPL_H_INCLUDED

#include <set>

///*!
// *
// */
//template<class T>
//MatrixContractor<T>::Sum::Sum
//   ( unsigned aIndex
//   , unsigned aExtent
//   , const matrix_vector& aMatrices
//   ) 
//   : mIndex(aIndex)
//   , mExtent(aExtent)
//{
//   //for(int i = 0; i < aMatrices.size(); ++i)
//   //{
//   //   if(aMatrices[i].Indices().first == aIndex)
//   //   {
//   //      mMatrices.emplace_back({i,0});
//   //   }
//   //   if(aMatrices[i].Indices().second == aIndex)
//   //   {
//   //      mMatrices.emplace_back({i,1});
//   //   }
//   //}
//}
//
///*!
// *
// */
//template<class T>
//size_t MatrixContractor<T>::Sum::Nmatrices
//   (
//   ) const
//{
//   return mMatrices.size();
//}
//
///*!
// * Define operator< for Sum. 
// *                       Checks on number of matrices in sum, and on extent of sum.
// */
//template<class T>
//bool MatrixContractor<T>::Sum::operator<
//   ( const Sum& aOther
//   ) const
//{
//   return this->Nmatrices() > aOther.Nmatrices() ? true : this->mExtent < aOther.mExtent;
//}
//
///**
// * Find overlapping contraction sets.
// * @return                         Returns a vector of contraction pair sets.
// *                                 Each set is decoupled and can be contracted separately.
// **/
//template<class T>
//typename MatrixContractor<T>::contraction_pair_set MatrixContractor<T>::FindOverlappingSets
//   (
//   ) const
//{
//   MIDASERROR("NOT IMPLEMENTED.");
//   contraction_pair_set cs;
//   for(int i = 0; i < mMatrices.size(); ++i)
//   {
//   }
//
//   return cs;
//}

/**
 * Construct a matrix contractor.
 * @param aReserve              Preallocate aReserve elements for later effiency when emplacing back.
 **/
template<class T>
MatrixContractor<T>::MatrixContractor
   ( int aReserve
   )
   : mMatrices()
{
   mMatrices.reserve(aReserve);
}

/**
 * Add a contraction matrix to matrix contractor.
 * @param aMatrix                   ContractionMatrix to add.
 **/
template<class T>
void MatrixContractor<T>::AddContractionMatrix
   ( ContractionMatrix<T>&& aMatrix
   )
{
   //mMatrices.emplace_back(matrix_tuple(std::move(aMatrix), false, false));
   mMatrices.emplace_back(std::move(aMatrix));
}

/**
 * 
 **/
template<class T>
T MatrixContractor<T>::Contract
   (
   ) const
{
   //std::cout << (*this) << std::endl;
   // find unique indices
   std::set<std::pair<unsigned, unsigned> > unique;
   for(int i = 0; i < mMatrices.size(); ++i)
   {
      unique.emplace(mMatrices[i].Indices().first , mMatrices[i].Extents().first );
      unique.emplace(mMatrices[i].Indices().second, mMatrices[i].Extents().second);
   }
   
   unsigned size = 1;
   for(auto& p : unique)
   {
      size *= p.second;
   }
   
   auto result = static_cast<T>(0.0);
   std::vector<unsigned> indices(unique.size(), 0);
   // loop over unique indices
   int elem = 0;
   while(elem < size)
   {
      //std::cout << "indices: " << indices << std::endl;
      auto product = static_cast<T>(1.0);

      // calculate product
      for(int i = 0; i < mMatrices.size(); ++i)
      {
         product *= mMatrices[i].GetElem(indices[mMatrices[i].Indices().first], indices[mMatrices[i].Indices().second]);
      }
      result += product;

      // increment index
      auto iter = unique.end();
      for(int i = indices.size() - 1; i >= 0; --i)
      {
         --iter;
         if(++indices[i] == iter->second)
            indices[i] = 0;
         else
            break;
      }
      ++elem;
   }
   return result;
}

/**
 * Overload for output operator.
 * @param os              Output stream object.
 * @param contractor      Contractor to output.
 * @return                Return output stream so operator<< can be chained.
 **/
template<class T>
std::ostream& operator<<(std::ostream& os, const MatrixContractor<T>& contractor)
{
   os << " Matrix contractor     size: " << contractor.mMatrices.size() << std::endl;
   for(int i = 0; i < contractor.mMatrices.size(); ++i)
   {
      os << " Matrix " << i << "\n";
      //os << std::get<0>(contractor.mMatrices[i]);
      os << contractor.mMatrices[i];
   }
   return os;
}

#endif /* MATRIX_CONTRACTOR_IMPL_H_INCLUDED */
