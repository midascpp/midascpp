/**
************************************************************************
* 
* @file    CpGuesser_Decl.h
*
* @date    01-12-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Declarations for CpGuesser class that creates and updates guesses
*     for CP-decomposition algorithms.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef CPGUESSER_DECL_H_INCLUDED
#define CPGUESSER_DECL_H_INCLUDED

#include "tensor/NiceTensor.h"
#include "tensor/BaseTensor.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/TensorDecompInfo.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include <string>

// Forward declaration that disables CpGuesser for complex numbers
template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class CpGuesser;

/**
 *
 **/
template
   <  class T
   >
class CpGuesser<T>
{
   public:
      //! Enum for guess types
      enum class guessType : int
      {
         SVD = 0
      ,  SCA
      ,  RANDOM
      ,  CONSTVAL
      };

      //! Enum for update types
      enum class updateType : int
      {
         REPLACE = 0
      ,  RANDOM
      ,  CONSTVAL
      ,  SCA
      ,  FITDIFF
      ,  STEPWISEFIT
      };


   private:
      //! The algorithm for creating a start guess to a given rank
      guessType mStartGuesser;

      //! The algorithm for updating the guess to a higher rank
      updateType mGuessUpdater;

      //! Scale guesses and increments with the norm of the target tensor
      bool mScaleGuessNorm;

      //! Scale random guess incr by overlap with residual
      bool mResidualOverlapScaling;

      //! Use residual norm for scaling of guess updates
      bool mUseResidualNorm;

      //! Relative threshold for CP-ALS fitting of residual in FitResidual
      T mFitDiffRelativeThreshold;

      //! Use default (hard-coded) residual fitter
      bool mDefaultResidualFitter;

      //! IoLevel
      In mIoLevel;

      //! Reference to TensorDecompInfo
      const TensorDecompInfo& mInfo;

      /** @name Private guesser methods **/
      //!@{

      //! Create SVD-based guess
      NiceTensor<T> SvdGuess
         (  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //! Create random guess
      NiceTensor<T> RandomGuess
         (  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //! Create constant-value guess
      NiceTensor<T> ConstValueGuess
         (  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //! Create SCA guess
      NiceTensor<T> ScaGuess
         (  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //!@}
      
      /** @name Private guess-update methods **/
      //!@{

      //! Create additional rank-1 tensors by fitting the residual to rank-r
      NiceTensor<T> FitResidual
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const T
         ,  const T
         ,  const In
         )  const;

      //! Create additional rank-1 tensors by fitting the residual in steps of 1
      NiceTensor<T> StepwiseResidualFit
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const T
         ,  const T
         ,  const In
         )  const;
      
      //! Add random rank-1 tensors to guess
      NiceTensor<T> RandomUpdate
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //! Add constant-valued rank-1 tensors to guess
      NiceTensor<T> ConstValueUpdate
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //! Add SCA-fit of residual
      NiceTensor<T> ScaUpdate
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const T
         ,  const In
         )  const;

      //!@}
      
      /** @name Private helper methods **/
      //!@{

      //! Construct the residual (target - fit)
      NiceTensor<T> ConstructResidual
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;

      //! Fit residual tensor
      FitReport<T> FitResidualTensor
         (  const NiceTensor<T>&
         ,  T
         ,  NiceTensor<T>&
         )  const;

      //!@}

   public:
      //! Constructor from TensorDecompInfo
      CpGuesser
         (  const TensorDecompInfo&
         );

      //! Create a starting guess
      void MakeStartGuess
         (  const NiceTensor<T>&
         ,  const T
         ,  NiceTensor<T>&
         ,  const In
         )  const;


      //! Update the guess to a higher rank
      void UpdateGuess
         (  NiceTensor<T>&
         ,  const NiceTensor<T>&
         ,  const In
         ,  const T
         ,  const T
         )  const;

      //! Improve guess by fitting each mode matrix to specialized residual
      void ImproveGuess
         (  NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;
};

#endif /* CPGUESSER_DECL_H_INCLUDED */
