#ifndef FITREPORT_H_INCLUDED
#define FITREPORT_H_INCLUDED

#include <type_traits>
#include "util/type_traits/Complex.h"
#include "tensor/ForwardDecl.h"

/**
 * @class FitReport  
 **/
template
   <  class T
   >
struct FitReport<T>
{
   T error;                               ///< Asolute error.
   T norm;                                ///< Target norm
   unsigned int numiter;                  ///< Number of iterations.
   T error_change = static_cast<T>(0.);   ///< Absolute error change.
   T gradient_norm = static_cast<T>(0.);  ///< Gradient norm
   bool converged = false;                ///< Is micro iterations converged.

   /*!
    * c-tor (will probably be removed at some point)
    */
   FitReport
      (  const T& a_error
      ,  const T& a_norm
      ,  const unsigned int& a_numiter
      )
      :  error(a_error)
      ,  norm(a_norm)
      ,  numiter(a_numiter)
   {
   }
   
   /*!
    * c-tor
    */
   FitReport
      (  const T& a_error
      ,  const T& a_norm
      ,  const unsigned int& a_numiter
      ,  const T& a_error_change
      ,  const T& a_gradient_norm
      ,  const bool a_converged
      )
      :  error(a_error)
      ,  norm(a_norm)
      ,  numiter(a_numiter)
      ,  error_change(a_error_change)
      ,  gradient_norm(a_gradient_norm)
      ,  converged(a_converged)
   {
   }

   /*!
    * dis-allow copy c-tor
    */
   FitReport(const FitReport&) = delete;

   /*!
    * allow move c-tor with default implementation
    */
   FitReport(FitReport&&) = default;
   
   /*!
    * dis-allow copy assignment
    */
   FitReport& operator=(const FitReport&) = delete;

   /*!
    * allow move assignment with default implementetation
    */
   FitReport& operator=(FitReport&&) = default;
   
   /*!
    * overload output operator for nice output
    */
   friend std::ostream& operator<<(std::ostream& os, const FitReport<T>& fit_report)
   {
      os << " FitReport: abs error     = " << fit_report.error << "\n"
         << "            rel error     = " << fit_report.error / fit_report.norm << "\n"
         << "            error_change  = " << fit_report.error_change << "\n"
         << "            gradient_norm = " << fit_report.gradient_norm << "\n"
         << "            numiter       = " << fit_report.numiter << "\n"
         << "            converged     = " << std::boolalpha << fit_report.converged << std::noboolalpha;
      return os;
   }
};

#endif /* FITREPORT_H_INCLUDED */
