#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "ZeroTensor.h"
#include "ZeroTensor_Impl.h"

// define
#define INSTANTIATE_ZEROTENSOR(T) \
template class ZeroTensor<T>;

// concrete instantiations
INSTANTIATE_ZEROTENSOR(float)
INSTANTIATE_ZEROTENSOR(double)
INSTANTIATE_ZEROTENSOR(std::complex<float>)
INSTANTIATE_ZEROTENSOR(std::complex<double>)

#undef INSTANTIATE_ZEROTENSOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
