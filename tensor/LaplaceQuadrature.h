#ifndef LAPLACEQUADRATURE_INCLUDED
#define LAPLACEQUADRATURE_INCLUDED

// Include declaration
#include "tensor/LaplaceQuadrature_Decl.h"

//#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "tensor/LaplaceQuadrature_Impl.h"
//#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* LAPLACEQUADRATURE_INCLUDED */
