#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "EnergyHolderTensor.h"
#include "EnergyHolderTensor_Impl.h"

// define 
#define INSTANTIATE_ENERGYHOLDER(T, U) \
template class EnergyHolderTensorBase<T, U>; \
template EnergyHolderTensorBase<T,U>::EnergyHolderTensorBase(const std::vector<unsigned>&, bool, bool, T, const std::vector<In>&, const DataCont&, const std::vector<In>&);

// concrete instantiations
INSTANTIATE_ENERGYHOLDER(float, ModalEnergyHolder)
INSTANTIATE_ENERGYHOLDER(double, ModalEnergyHolder)
//INSTANTIATE_ENERGYHOLDER(std::complex<float>, ModalEnergyHolder)
//INSTANTIATE_ENERGYHOLDER(std::complex<double>, ModalEnergyHolder)

#undef INSTANTIATE_ENERGYHOLDER

#endif /* DISABLE_PRECOMPILED_TEMPLATES */

