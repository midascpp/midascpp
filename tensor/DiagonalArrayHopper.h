#ifndef DIAGONALARRAYHOPPER_H_INCLUDED
#define DIAGONALARRAYHOPPER_H_INCLUDED

#include<vector>
#include<iostream>
#include<cstddef>
#include<string>
#include<sstream>
#include<utility>

#include"ArrayHopper.h"

inline std::vector<ptrdiff_t> get_diagonal_jumps(
      const UIVec& dims,
      const std::vector<std::pair<unsigned int, unsigned int>> jump_indices_pairs)
{
   std::vector<ptrdiff_t> jumps;
   int njumps=jump_indices_pairs.size();
   for(int m=0;m<njumps;++m)
   {
      jumps.push_back( dimsproduct(dims, jump_indices_pairs[m].first) +
                       dimsproduct(dims, jump_indices_pairs[m].second) );
      for(int n=0;n<m;++n)
      {
         *(jumps.end()-1) += (1-static_cast<ptrdiff_t>(dims[jump_indices_pairs[n].first]) ) * 
                              dimsproduct(dims, jump_indices_pairs[n].first) +
                             (1-static_cast<ptrdiff_t>(dims[jump_indices_pairs[n].second]) ) * 
                              dimsproduct(dims, jump_indices_pairs[n].second);
      }
   }
   return jumps;
}

std::vector<unsigned int> get_seconds(std::vector<std::pair<unsigned int, unsigned int>> pairs);

template <class T>
class DiagonalArrayHopper: public ArrayHopper<T>
{
   public:
      DiagonalArrayHopper(T* const     aArray,
                  const UIVec& aDims,
                  const std::vector<std::pair<unsigned int, unsigned int>>& aJumpIndicesPairs):
         ArrayHopper<T>(aArray, aDims, aDims, get_seconds(aJumpIndicesPairs))
      {
         this->mJumps=get_diagonal_jumps(aDims, aJumpIndicesPairs);
      }
};
#endif /* DIAGONALARRAYHOPPER_H_INCLUDED */
