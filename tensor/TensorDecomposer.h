#ifndef TENSORDECOMPOSER_DECL_H_INCLUDED
#define TENSORDECOMPOSER_DECL_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "util/AbstractFactory.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/NiceTensor.h"
#include "tensor/CpGuesser.h"

namespace midas
{
namespace tensor
{
namespace detail
{

//! Forward declarations
class ConcreteCpDecomposerBase;
class TensorDecompDriverBase;
class ConcreteCpPreprocessorBase;

//! Define ConcreteCpDecomposerFactory
using ConcreteCpDecomposerFactory = AbstractFactory<ConcreteCpDecomposerBase*(const TensorDecompInfo&), std::string>;

//! Define TensorDecompDriverFactory
using TensorDecompDriverFactory = AbstractFactory<TensorDecompDriverBase*(const TensorDecompInfo&), std::string>;

//! Define ConcreteCpPreprocessorFactory
using ConcreteCpPreprocessorFactory = AbstractFactory<ConcreteCpPreprocessorBase*(const TensorDecompInfo&), std::string>;

//! Define registration classes
template<class A>
using ConcreteCpDecomposerRegistration = AbstractFactoryRegistration<ConcreteCpDecomposerBase*(const TensorDecompInfo&), A, std::string>;

template<class A>
using TensorDecompDriverRegistration = AbstractFactoryRegistration<TensorDecompDriverBase*(const TensorDecompInfo&), A, std::string>;

template<class A>
using ConcreteCpPreprocessorRegistration = AbstractFactoryRegistration<ConcreteCpPreprocessorBase*(const TensorDecompInfo&), A, std::string>;


/**
 * Abstract base class for the set of concrete tensor decomposer classes.
 **/
class ConcreteCpDecomposerBase
{
   private:
      //! Overloadable implementation function for Decompose with argument guess
      virtual FitReport<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const
         =  0;

      //! Overloadable implementation function for CheckDecomposition
      virtual bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const = 0;

      //! Overloadable implementation function for Type
      virtual std::string TypeImpl() const = 0;

   public:
      virtual ~ConcreteCpDecomposerBase() = default;
      
      //! Interface for Decompose with argument guess and threshold. Will call DecomposeImpl
      FitReport<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const;

      //! Interface for Type. Will call TypeImpl
      std::string Type() const;

      //! Interface to CheckDecomposition
      bool CheckDecomposition
         (  const NiceTensor<Nb>&
         )  const;

      //! Factory
      static std::unique_ptr<ConcreteCpDecomposerBase> Factory(const TensorDecompInfo&);
};


/**
 * Abstract base class for tensor decomp preprocessing
 **/
class ConcreteCpPreprocessorBase
{
   private:
      //! Preprocess the tensor
      virtual bool PreprocessImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         )  = 0;

      //! Preprocess tensor and guess
      virtual bool PreprocessImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         )  = 0;

      //! Postprocess the tensor after decomposition
      virtual bool PostprocessImpl
         (  NiceTensor<Nb>&
         )  const = 0;

      //! Type
      virtual std::string TypeImpl
         (
         )  const = 0;

      //! Modifies target norm
      virtual bool ModifiesTargetNormImpl
         (
         )  const = 0;

   public:
      //! Virtual destructor
      virtual ~ConcreteCpPreprocessorBase() = default;

      //! Interface to Preprocess
      bool Preprocess
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         );

      //! Interface to preprocess tensor and guess
      bool Preprocess
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         );

      //! Interface to Postprocess
      bool Postprocess
         (  NiceTensor<Nb>&
         )  const;

      //! Interface to Type
      std::string Type
         (
         )  const;

      //! Interface to ModifiesTargetNorm
      bool ModifiesTargetNorm
         (
         )  const;

      //! Factory
      static std::unique_ptr<ConcreteCpPreprocessorBase> Factory(const TensorDecompInfo&);
};


/**
 * Abstract base class for tensor decomp drivers
 **/
class TensorDecompDriverBase
{
   private:
      //! Preprocess the tensor before decomposition
      std::unique_ptr<ConcreteCpPreprocessorBase> mPreprocessor;

      //! Decompose types
      bool mDecomposeCanonical = true;
      bool mDecomposeDirProd   = true;
      bool mDecomposeSum       = true;


      //! Overloadable implementation for CheckDecomposition
      virtual bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const = 0;

      //! Overloadable implementation for Type
      virtual std::string TypeImpl
         (
         )  const = 0;

      //! Overloadable implementation for Decompose
      virtual NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const = 0;

      //! Overloadable implementation for Decompose with argument guess
      virtual NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const = 0;

      //! Overloadable implementation of screening
      virtual bool ScreenImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const = 0;

      //! Get accuracy threshold
      virtual Nb GetThreshold
         (
         )  const = 0;

   protected:
      //! Concrete decomposer that fits a guess to the taget tensor
      std::unique_ptr<ConcreteCpDecomposerBase> mCpFitter;

      //! Guesser to generate and update guess for CP decomposition
      std::unique_ptr<CpGuesser<Nb>> mGuesser;

      //! Reference to TensorDecompInfo
      const TensorDecompInfo& mInfo; 

      //! Decompose tensors down to this order
      In mDecompToOrder;

      //! IoLevel
      In mIoLevel;

      /** @name Low-order decomposition **/
      //!@{

      //! Relative threshold for SVD fitting of matrices.
      Nb mMatrixSvdRelThresh;

      //! Use full-rank SVD
      bool mFullRankSvd;

      //! Check accuracy of SVD
      bool mCheckSvd;

      //! Use SVD on high-order tensors with some dims=1
      bool mEffectiveLowOrderSvd;

      //!@}

      //! Overloadable implementation of low-order tensor decomposition (default implementation available)
      virtual bool DecomposeLowOrderTensorsImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const;

      //! Construct mode matrices of target
      NiceTensor<Nb> ConstructCanonicalTarget
         (  const NiceTensor<Nb>&
         )  const;


   public:
      //! c-tor from TensorDecompInfo
      TensorDecompDriverBase
         (  const TensorDecompInfo&
         );

      //! Virtual destructor
      virtual ~TensorDecompDriverBase() = default;

      //! Public interface to CheckDecomposition
      bool CheckDecomposition
         (  const NiceTensor<Nb>&
         )  const;

      //! Public interface to Type
      std::string Type
         (
         )  const;

      //! Public interface to Decompose
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         )  const;

      //! Public interface to Decompose with norm2
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const;

      //! Public interface to Decompose with guess
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const;

      //! Public interface to Decompose with guess and norm2
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const;

      //! Get rank of target tensor (return -1 for SimpleTensor)
      In TargetRank
         (  const NiceTensor<Nb>&
         )  const;

      //! Factory
      static std::unique_ptr<TensorDecompDriverBase> Factory(const TensorDecompInfo&);
};

} /* namespace detail */



/**
 * Interface to decompose different kinds of tensor types, to other various
 * kinds of tensor types.
 **/
class TensorDecomposer
{
   private:
      ///> Vector of actual decomposition driver classes
      std::vector<std::unique_ptr<detail::TensorDecompDriverBase> > mDecomposers;

   public:
      ///> Construct from a set of TensorDecompInfo's
      TensorDecomposer(const TensorDecompInfo::Set&);
      
      ///> Construct from a single TensorDecompInfo
      TensorDecomposer(const TensorDecompInfo&);

      ///> Dis-allow copy c-tor
      TensorDecomposer(const TensorDecomposer&) = delete;
      
      ///> Dis-allow move c-tor
      TensorDecomposer(TensorDecomposer&&) = delete;
      
      ///> Dis-allow copy assignment
      TensorDecomposer& operator=(const TensorDecomposer&) = delete;
      
      ///> Dis-allow move assignment
      TensorDecomposer& operator=(TensorDecomposer&&) = delete;

      ///> Check if a decompostion will be done on NiceTensor
      bool CheckDecomposition(const NiceTensor<Nb>&) const;

      //! Decompose a NiceTensor using argument threshold
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb arThresh=-C_1
         )  const;

      //! Decompose a NiceTensor using norm2 and argument threshold
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const;

      ///> Decompose a NiceTensor using argument guess and threshold
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  const NiceTensor<Nb>&
         ,  Nb arThresh=-C_1
         )  const;

      ///> Decompose a NiceTensor using argument guess, norm2, and threshold
      NiceTensor<Nb> Decompose
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const;
};

} /* namespace tensor */
} /* namespace midas */

#endif /* TENSORDECOMPOSER_DECL_H_INCLUDED */
