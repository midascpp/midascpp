#ifndef TENSOR_SUM_IMPL_H_INCLUDED
#define TENSOR_SUM_IMPL_H_INCLUDED

#include <cassert>

#include "util/CallStatisticsHandler.h"
#include "util/Math.h"
#include "util/SanityCheck.h"
#include "libmda/numeric/float_eq.h"

#include "tensor/CanonicalTensor.h"
#include "tensor/TensorDirectProduct.h"

/**
 *
 **/
template<class T>
TensorSum<T>::TensorSum
   ( const std::vector<unsigned>& aDims
   )
   : BaseTensor<T>(aDims)
{
}

/**
 *
 **/
template<class T>
typename BaseTensor<T>::typeID TensorSum<T>::Type
   (
   ) const
{
   return BaseTensor<T>::typeID::SUM;
}

/**
 *
 **/
template<class T>
std::string TensorSum<T>::ShowType
   (
   ) const
{
   return {"TensorSum"};
}

/**
 *
 **/
template<class T>
BaseTensor<T>* TensorSum<T>::Clone
   (
   ) const
{
   auto result = new TensorSum<T>( this->GetDims() );

   auto size = this->mTensors.size();
   result->mTensors.reserve(size);
   result->mCoefs.reserve(size);

   for(size_t i=0; i<size; ++i)
   {
      result->mTensors.emplace_back( this->mTensors[i]->Clone() );
      result->mCoefs.emplace_back( this->mCoefs[i] );
   }

   return std::move(result);
}

/**
 *
 **/
template
   <  class T
   >
void TensorSum<T>::ConjugateImpl
   (
   )
{
   for(auto& coef : mCoefs)
   {
      coef = midas::math::Conj(coef);
   }
   for(auto& tens : mTensors)
   {
      tens->Conjugate();
   }
}

/**
 *
 **/
template<class T>
void TensorSum<T>::DumpInto
   ( T* ptr
   ) const
{
   auto size = this->TotalSize();
   // zero incoming ptr
   for(int j = 0 ; j < size; ++j)
   {
      ptr[j] = static_cast<T>(0.0);
   }

   // add each term in sum
   auto alloc_tp = this->GetAllocatorTp();
   auto internal_ptr = alloc_tp.AllocateUniqueArray(size);
   for(int i = 0; i < mTensors.size(); ++i)
   {
      mTensors[i]->DumpInto(internal_ptr.get());
      for(int j = 0 ; j < size; ++j)
      {
         ptr[j] += mCoefs[i]*internal_ptr[j];
      }
   }
}

/**
 * Construct mode matrices and return BaseTensorPtr 
 * to new CanonicalTensor.
 *
 * @return     BaseTensorPtr to new tensor.
 **/
template <class T>
BaseTensorPtr<T> TensorSum<T>::ConstructModeMatrices
   (
   )  const
{
   // Dump vectors (no need to construct sum of vectors)
   auto  ndim    = this->NDim();
   assert(ndim > 0);
   auto rank = this->Rank();
   auto& extents = this->GetDims();

   if (  rank == 0
      )
   {
      return std::unique_ptr<BaseTensor<T>>(new CanonicalTensor<T>(extents, rank));
   }
   else if  (  ndim == 1
            || this->TotalSize() == this->MaxExtent()
            )
   {
      return this->DumpVectorToCanonical();
   }
   else
   {
      // Declarations
      auto& coefs   = this->GetCoefs();
      auto& tensors = this->GetTensors();
      auto  ntensor = tensors.size();

      // Assert all canonical
      if (  !this->AllCanonical()
         )
      {
         for(int itensor = 0; itensor<ntensor; ++itensor)
         {
            Mout  << tensors[itensor]->ShowType() << std::endl;
         }
         MIDASERROR("All tensors should be canonical in TensorSum::ConstructModeMatrices!");
      }

      // Allocate result
      auto result = std::unique_ptr<BaseTensor<T>>(new CanonicalTensor<T>(extents, rank));

      auto new_modematrices = static_cast<CanonicalTensor<T>*>(result.get())->GetModeMatrices();

      // Loop through result
      auto cumulative_rank = 0;
      for(int itensor = 0; itensor < ntensor; ++itensor)
      {
         const CanonicalTensor<T>& canonical_sum = *(static_cast<const CanonicalTensor<T>*>(tensors[itensor].get()));
         T phase = coefs[itensor] / std::abs(coefs[itensor]);
         T coef = std::pow(std::abs(coefs[itensor]), 1.0/ndim);
         auto canonical_rank = canonical_sum.GetRank();
         auto mode_matrices = canonical_sum.GetModeMatrices();
         for(int idim = 0; idim < ndim; ++idim)
         {
            auto extent = extents[idim];
            auto new_modematrix = new_modematrices[idim] + cumulative_rank * extent;
            auto modematrix = mode_matrices[idim];
            auto scaling = idim == 0 ? phase*coef : coef;
            for(int irank = 0; irank < canonical_rank; ++irank)
            {
               for(int iextent = 0; iextent < extent; ++iextent)
               {
                  *(new_modematrix++) = scaling * (*(modematrix++));
               }
            }
         }
         cumulative_rank += canonical_rank;
      }

      return result;
   }
}


/**
 *
 **/
template<class T>
unsigned TensorSum<T>::Rank
   (
   ) const
{
   if (  this->SimpleAndCanonical()
      )
   {
      MIDASERROR("Cannot calculate rank of TensorSum with both simple and canonical tensors!!!");
   }

   auto rank = static_cast<unsigned>(0);
   for(int i = 0; i < mTensors.size(); ++i)
   {
      if(mTensors[i]->Type() == BaseTensor<T>::typeID::CANONICAL)
      {
         rank += static_cast<CanonicalTensor<T>* >(mTensors[i].get())->GetRank();
      }
      else if(mTensors[i]->Type() == BaseTensor<T>::typeID::DIRPROD)
      {
         rank += static_cast<TensorDirectProduct<T>* >(mTensors[i].get())->Rank();
      }
      else if  (  mTensors[i]->Type() == BaseTensor<T>::typeID::SCALAR
               )
      {
         MIDASERROR("TensorSum contains Scalar... This should not happen!");
      }
   }
   return rank;
}

/**
 *
 **/
template<class T>
typename BaseTensor<T>::real_t TensorSum<T>::Norm2
   (
   ) const
{
   //LOGCALL("calls");
   real_t norm2(0.0);
   const auto size = this->mTensors.size();
   const int tot = size*(size+1)/2;

//   T dot = static_cast<T>(0.);
//   int i = 0;
//   int j = 0;
//
//   // Niels: Probably more efficient to use MKL threading in DGEMMs
//   #pragma omp parallel for schedule(dynamic) private(dot, i, j) reduction(+:norm2)
//   for(int k=0; k<tot; ++k)
//   {
//      i = k / (size+1);
//      j = k % (size+1);
//      if (  j > i )
//      {
//         i = size - i - 1;
//         j = size - j;
//      }
//      dot = mCoefs[i]*mCoefs[j]*mTensors[i]->Dot(mTensors[j].get());
//      norm2 += (i==j) ? dot : static_cast<T>(2.)*dot;
//   }

   // Niels: This is the old, non-collapsed loop
   for(int i = 0 ; i < size; ++i)
   {
      for(int j = i + 1; j < size; ++j)
      {
         //LOGCALL("terms");
         norm2 += static_cast<real_t>(2.0)*std::real(midas::math::Conj(mCoefs[i])*mCoefs[j]*mTensors[i]->Dot(mTensors[j].get())); // off-diagonal = \sum_{i<j} (<i|j> + <j|i>) = 2 \sum_{i<j} Re<i|j>
      }
      norm2 += std::real(midas::math::Conj(mCoefs[i])*mCoefs[i]*mTensors[i]->Norm2()); // diagonal
   }

   // Check sanity
   if (  !midas::util::IsSane(norm2)
      )
   {
      std::ostringstream os;
      os << "TensorSum::Norm2 returns " << norm2;
      MIDASERROR(os.str());
   }
   if (  libmda::numeric::float_neg(norm2)
      )
   {
      if(libmda::numeric::float_numeq_zero(norm2, static_cast<real_t>(1.), 1000))
      {
         norm2 = -norm2;
      }
      else
      {
         MidasWarning("Safe Norm2");
         norm2 = this->SafeNorm2();
      }
   }

   return norm2;
}

/**
 * Value of element used in cross approximation.
 *
 * @param aIndices   The index vector
 * @return
 *    Value of element
 **/
template <class T>
T TensorSum<T>::Element
   (  const std::vector<unsigned>& aIndices
   )  const
{
   assert(aIndices.size() == this->NDim());

   auto terms = this->mTensors.size();

   T result = static_cast<T>(0.);

//   #pragma omp parallel for schedule(dynamic) reduction(+:result)
   for(size_t i=0; i<terms; ++i)
   {
      result += this->mCoefs[i]*this->mTensors[i]->Element(aIndices);
   }

   return result;
}

/**
 *
 **/
template<class T>
bool TensorSum<T>::AllCanonical
   (
   ) const
{
   bool allcanonical = true; // assume all are canonical
   for(int i = 0; i < mTensors.size(); ++i)
   {
      //Mout << " TYPE " << i << " : " << mTensors[i]->ShowType() << std::endl;
      if(mTensors[i]->Type() != BaseTensor<T>::typeID::CANONICAL)
      {
         allcanonical = false;
         break; // break for
      }
   }
   return allcanonical;
}

/**
 *
 **/
template
   <  class T
   >
bool TensorSum<T>::SimpleAndCanonical
   (
   )  const
{
   bool contains_simple = false;
   bool contains_cp = false;

   for(int i = 0; i < mTensors.size(); ++i)
   {
      if (  mTensors[i]->Type() == BaseTensor<T>::typeID::CANONICAL
         || (  mTensors[i]->Type() == BaseTensor<T>::typeID::DIRPROD
            && static_cast<const TensorDirectProduct<T>* const>(mTensors[i].get())->AllCanonical()
            )
         )
      {
         contains_cp = true;
      }
      else if  (  mTensors[i]->Type() == BaseTensor<T>::typeID::SIMPLE
               )
      {
         contains_simple = true;
      }
      else
      {
         MidasWarning("TensorSum also contains " + mTensors[i]->ShowType());
      }

      if (  contains_cp
         && contains_simple
         )
      {
         return true;
      }
   }

   return false;
}

/**
 *
 **/
template<class T>
bool TensorSum<T>::SanityCheck() const
{
   bool sane = true;
   for(size_t i = 0; i<this->mTensors.size(); ++i)
   {
      sane = sane & this->mTensors[i]->SanityCheck();
   }

   return sane;
}

/**
 *
 **/
template<class T>
BaseTensor<T>* TensorSum<T>::operator+=
   ( const BaseTensor<T>& other
   )
{
   assert_same_shape(this, &other);
   
   mCoefs.emplace_back(static_cast<T>(1.0));
   mTensors.emplace_back(other.Clone());
   
   return this;
}

/**
 *
 **/
template <class T>
void TensorSum<T>::Axpy
   (  const BaseTensor<T>& other
   ,  const T& coef
   )
{
   assert_same_shape(this, &other);

   mCoefs.emplace_back(coef);
   mTensors.emplace_back(other.Clone());
}

/**
 *
 **/
template<class T>
BaseTensor<T>* TensorSum<T>::operator-=
   ( const BaseTensor<T>& other
   )
{
   assert_same_shape(this, &other);
   
   mCoefs.emplace_back(static_cast<T>(-1.0));
   mTensors.emplace_back(other.Clone());
   
   return this;
}

/**
 *
 **/
template <class T>
void TensorSum<T>::Scale
   (  const T& aScaling
   )
{
   for(auto& coef : this->mCoefs)
   {
      coef *= aScaling;
   }
}


/**
 *
 **/
template<class T>
typename TensorSum<T>::dot_t TensorSum<T>::Dot
   ( const BaseTensor<T>* const other
   ) const
{
   return midas::math::Conj(other->DotDispatch(this));
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t TensorSum<T>::DotDispatch
   ( const SimpleTensor<T>* const other
   ) const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this));   // arguments swapped
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t TensorSum<T>::DotDispatch
   ( const CanonicalTensor<T>* const other
   ) const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this));   // arguments swapped
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t TensorSum<T>::DotDispatch
   ( const TensorDirectProduct<T>* const other
   ) const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this));   // arguments swapped
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t TensorSum<T>::DotDispatch
   ( const TensorSum<T>* const other
   ) const
{
   return DotEvaluator<T>().Dot(this, other);
}

#endif /* TENSOR_SUM_IMPL_H_INCLUDED */
