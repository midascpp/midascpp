#ifndef GAMMAMATRIX_H_INCLUDED
#define GAMMAMATRIX_H_INCLUDED

#include <memory>
#include <cassert>
#include "lapack_interface/math_wrappers.h"

//====================================================
// Forward declarations to disable complex numbers
template
   <  typename T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class GeneralGammaIntermediates;

template
   <  typename T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class PartialGammaIntermediates;

template
   <  typename T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class GammaIntermediates;
//====================================================

/**
 * In-place multiply each element of \p orig with each element of \p contrib.
 **/
template 
   <  class T
   ,  class SmartPtr1
   ,  class SmartPtr2
   >
void element_wise_multiply
   ( const SmartPtr1& orig
   , const SmartPtr2& contrib
   , size_t num_elements
   )
{
   T* orig_ptr = orig.get();
   T* contrib_ptr = contrib.get();
   for(size_t i = 0; i < num_elements; ++i)
   {
      *(orig_ptr++) *= *(contrib_ptr++);
   }
}

/**
 *
 **/
template
   <  class T
   >
void transposematrix_times_matrix
   (  int m
   ,  int k
   ,  int n
   ,  T* left
   ,  T* right
   ,  T* result
   ,  bool conjugate_left = false
   )
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;
   char tc = is_complex && conjugate_left ? 'C' : 'T';
   char nc = 'N';
   T alpha = static_cast<T>(1.0);
   T beta  = static_cast<T>(0.0);
   midas::lapack_interface::gemm( &tc, &nc, &m, &n, &k, &alpha
                                , left , &k, right, &k
                                , &beta, result, &m
                                );
}

/**
 * @class GeneralGammaIntermediates
 **/
template
   <  typename T
   >
class GeneralGammaIntermediates<T>
{
   private:
      unsigned mNdim;
      unsigned mRankLeft;
      unsigned mRankRight;
      unsigned mRank2;
      std::unique_ptr<T[]> mIntermediates;
      
      /**
       *
       **/
      void Initialize(const CanonicalTensor<T>* const canonical_left, const CanonicalTensor<T>* const canonical_right)
      {
         T** mode_matrices_left  = canonical_left->GetModeMatrices();
         T** mode_matrices_right = canonical_right->GetModeMatrices();
         T* intermeds = mIntermediates.get();
         for(int idim = 0; idim < mNdim; ++idim)
         {
            transposematrix_times_matrix(mRankLeft, canonical_left->Extent(idim), mRankRight, mode_matrices_left[idim], mode_matrices_right[idim], intermeds, true);
            intermeds += mRank2;
         }
      }

   public:
      /**
       *
       **/
      GeneralGammaIntermediates
         (  const CanonicalTensor<T>* const canonical_left
         ,  const CanonicalTensor<T>* const canonical_right
         )
         :  mNdim(canonical_left->NDim())
         ,  mRankLeft(canonical_left->GetRank())
         ,  mRankRight(canonical_right->GetRank())
         ,  mRank2(mRankLeft*mRankRight)
         ,  mIntermediates(new T[mNdim*mRank2])
      {
         this->Initialize(canonical_left, canonical_right);
      }

      /**
       *
       **/
      template<class SmartPtr>
      void GammaMatrix
         (  unsigned idim
         ,  SmartPtr& gamma
         )  const
      {
         for(unsigned i = 0; i < mRank2; ++i)
         {
            gamma[i] = static_cast<T>(1.0);
         }
         
         T* intermeds = mIntermediates.get();
         for(int jdim = 0; jdim < mNdim; ++jdim)
         {
            if(jdim == idim) // if we hit idim, we just skip it by incrementing intermed pointer to next set of intermediates.
            {
               intermeds += mRank2;
            }
            else
            {
               T* gamma_ptr = gamma.get();
               for(unsigned irank2 = 0; irank2 < mRank2; ++irank2)
               {
                  *(gamma_ptr++) *= *(intermeds++);
               }
            }
         }
      }

      /**
       * Transform mode matrix with (inverse) GammaMatrix diagonal.
       * Used for preconditioning in CP-ASD algorithm.
       *
       * @param idim          Current dimension
       * @param rank          Rank of input mode matrix
       * @param extent        Extent of input mode matrix
       * @param mat_ptr       Pointer to mode matrix
       * @param inverse       Transform with inverse diagonal
       * @return              Overlap between original and transformed tensor
       **/
      T DiagonalTransformation
         (  unsigned idim
         ,  unsigned rank
         ,  unsigned extent
         ,  T* mat_ptr
         ,  bool inverse
         )  const
      {
         assert(rank == this->mRankLeft && rank == this->mRankRight);
         auto ndim = this->mNdim;
         assert(idim < ndim);

         T overlap = static_cast<T>(0.);

         T elem = static_cast<T>(1.);

         // Loop over rank index of aTensor
         for(unsigned irank=0; irank<rank; ++irank)
         {
            // Calculate diagonal gamma element
            elem = static_cast<T>(1.);
            for(unsigned jdim=0; jdim<ndim; ++jdim)
            {
               if (  jdim != idim   )
               {
                  // First skip to correct mode (set of intermeds), then to irank'th diagonal element.
                  elem *= this->mIntermediates[jdim*this->mRank2 + irank*(rank+1)];
               }
            }
            // Divide mode vector by diagonal element
            for(unsigned i=0; i<extent; ++i)
            {
               if (  inverse  )
               {
                  overlap += (*mat_ptr) * (*mat_ptr) / elem;
                  *(mat_ptr++) /= elem;
               }
               else
               {
                  overlap += (*mat_ptr) * (*mat_ptr) * elem;
                  *(mat_ptr++) *= elem;
               }
            }
         }

         return overlap;
      }

      /**
       * Transform CanonicalTensor with (inverse) GammaMatrix diagonal.
       * Used for preconditioning in the CP-NCG algorithm.
       *
       * @param tens          Pointer to CanonicalTensor
       * @param inverse       Transform with inverse diagonal
       * @return              Overlap between original and transformed tensor
       **/
      T DiagonalTransformation
         (  const CanonicalTensor<T>* const tens
         ,  bool inverse
         )  const
      {
         auto ndim = tens->NDim();
         T overlap = static_cast<T>(0.);
         auto rank = tens->GetRank();

         for(size_t i=0; i<ndim; ++i)
         {
            auto extent = tens->Extent(i);
            T* mode_matrix = tens->GetModeMatrices()[i];
            overlap += this->DiagonalTransformation(i, rank, extent, mode_matrix, inverse);
         }

         return overlap;
      }

      /**
       * Norm2 of mode vector.
       *
       * @param idim    The mode
       * @param irank   The rank
       *
       * @return     Norm2 of mode vector
       **/
      T ModeVectorNorm2
         (  unsigned idim
         ,  unsigned irank
         )  const
      {
         assert(this->mRankLeft == this->mRankRight);
         auto rank = this->mRankLeft;
         assert(irank <= rank);
         assert(idim <= this->mNdim);

         return this->mIntermediates[idim*this->mRank2 + irank*(rank+1)];
      }


      /**
       * Compute sum of rank-1 tensor norm2s (denoted as c).
       * This is used for computing the condition number \f$ \kappa = \sqrt{c} / ||\boldsymbol{\mathcal{F}}|| \f$ :
       * [Algorithms in high dimensions](https://amath.colorado.edu/faculty/beylkin/papers/BEY-MOH-2005.pdf)
       *
       * @return     The c coefficient
       **/
      T ConditionCoefficient
         (
         )  const
      {
         assert(this->mRankLeft == this->mRankRight);
         auto rank = this->mRankLeft;

         T result = static_cast<T>(0.);
         auto ndim = this->mNdim;
         T tmp = static_cast<T>(1.);

         for(unsigned irank=0; irank<rank; ++irank)
         {
            tmp = static_cast<T>(1.);
            for(unsigned idim=0; idim<ndim; ++idim)
            {
               tmp *= this->mIntermediates[idim*this->mRank2 + irank*(rank+1)];
            }
            result += tmp;
         }

         return result;
      }

      
      /**
       *
       **/
      template<class SmartPtr>
      void FullGammaMatrix
         (  SmartPtr& gamma
         )  const
      {
         for(unsigned i = 0; i < mRank2; ++i)
         {
            gamma[i] = static_cast<T>(1.0);
         }
         
         T* intermeds = mIntermediates.get();
         for(int jdim = 0; jdim < mNdim; ++jdim)
         {
            T* gamma_ptr = gamma.get();
            for(unsigned irank2 = 0; irank2 < mRank2; ++irank2)
            {
               *(gamma_ptr++) *= *(intermeds++);
            }
         }
      }

      /**
       *
       **/
      template <class SmartPtr>
      T FullGammaMatrixNorm2
         (  SmartPtr& gamma
         )  const
      {
         this->FullGammaMatrix(gamma);

         auto gamma_ptr = gamma.get();
//         std::sort(gamma_ptr, gamma_ptr+this->mRank2);

         T result = static_cast<T>(0.);

         for(int k=0; k<this->mRank2; ++k)
         {
            result += *(gamma_ptr++);
         }

         return result;
      }
      

      /**
       *
       **/
      void Update(unsigned idim, const CanonicalTensor<T>* const canonical_left, const CanonicalTensor<T>* const canonical_right)
      {
         T* mode_matrix_left  = canonical_left->GetModeMatrices()[idim];
         T* mode_matrix_right = canonical_right->GetModeMatrices()[idim];
         T* intermeds = mIntermediates.get(); 
         intermeds += idim*mRank2;
         transposematrix_times_matrix(mRankLeft, canonical_left->Extent(idim), mRankRight, mode_matrix_left, mode_matrix_right, intermeds, true);
      }


      /**
       *
       **/
      void ReCalculate
         (  const CanonicalTensor<T>* const canonical_left
         ,  const CanonicalTensor<T>* const canonical_right
         )
      {
         this->Initialize(canonical_left, canonical_right);
      }

      /**
       *
       **/
      unsigned Rank2
         (
         ) const
      {
         return mRank2;
      }
      
      /**
       *
       **/
      unsigned RankLeft
         (
         ) const
      {
         return mRankLeft;
      }
      
      /**
       *
       **/
      unsigned RankRight
         (
         ) const
      {
         return mRankRight;
      }
};

/**
 * @class PartialGammaIntermediates
 **/
template
   <  typename T
   >
class PartialGammaIntermediates<T>
{
   private:
      unsigned mNdim;
      unsigned mRankLeft;
      unsigned mRankRight;
      unsigned mRank2;
      std::vector<unsigned> mIndices;
      std::unique_ptr<T[]> mIntermediates;
      
      /**
       *
       **/
      void Initialize(const CanonicalTensor<T>* const canonical_left, const CanonicalTensor<T>* const canonical_right)
      {
         T** mode_matrices_left  = canonical_left->GetModeMatrices();
         T** mode_matrices_right = canonical_right->GetModeMatrices();
         T* intermeds = mIntermediates.get();
         for(int idim = 0; idim < mNdim; ++idim)
         {
            transposematrix_times_matrix(mRankLeft, canonical_right->Extent(idim), mRankRight, mode_matrices_left[mIndices[idim]], mode_matrices_right[idim], intermeds, true);
            intermeds += mRank2;
         }
      }

   public:
      /**
       * @param canonical_left
       * @param canonical_right
       * @param indices
       *    Gives right-to-left index conversion, meaning that if 
       *    `indices[0] := 2`, then index-0 of canonical_right corresponds to
       *    index-2 of canonical_left. Easy :D.
       **/
      PartialGammaIntermediates
         ( const CanonicalTensor<T>* const canonical_left
         , const CanonicalTensor<T>* const canonical_right
         , const std::vector<unsigned>& indices
         )
         : mNdim(canonical_right->NDim()) // Ndim is decided by left, as it must be the smallest
         , mRankLeft(canonical_left->GetRank())
         , mRankRight(canonical_right->GetRank())
         , mRank2(mRankLeft*mRankRight)
         , mIndices(indices)
         , mIntermediates(new T[mNdim*mRank2])
      {
         this->Initialize(canonical_left, canonical_right);
      }

      /**
       *
       **/
      template<class SmartPtr>
      void GammaMatrix
         (  unsigned idim
         ,  SmartPtr& gamma
         )
      {
         for(unsigned i = 0; i < mRank2; ++i)
         {
            gamma[i] = static_cast<T>(1.0);
         }
         
         T* intermeds = mIntermediates.get();
         for(int jdim = 0; jdim < mNdim; ++jdim)
         {
            if(mIndices[jdim] == idim) // if we hit idim, we just skip it by incrementing intermed pointer to next set of intermediates.
            {
               intermeds += mRank2;
            }
            else
            {
               T* gamma_ptr = gamma.get();
               for(unsigned irank2 = 0; irank2 < mRank2; ++irank2)
               {
                  *(gamma_ptr++) *= *(intermeds++);
               }
            }
         }
      }
      
      /**
       *
       **/
      template<class SmartPtr>
      void FullGammaMatrix
         (  SmartPtr& gamma
         )
      {
         for(unsigned i = 0; i < mRank2; ++i)
         {
            gamma[i] = static_cast<T>(1.0);
         }
         
         T* intermeds = mIntermediates.get();
         for(int jdim = 0; jdim < mNdim; ++jdim)
         {
            T* gamma_ptr = gamma.get();
            for(unsigned irank2 = 0; irank2 < mRank2; ++irank2)
            {
               *(gamma_ptr++) *= *(intermeds++);
            }
         }
      }
      

      /**
       *
       **/
      void Update
         ( unsigned idim
         , const CanonicalTensor<T>* const canonical_left
         , const CanonicalTensor<T>* const canonical_right
         )
      {
         unsigned jdim = -1;
         for(unsigned i = 0; i < mIndices.size(); ++i)
         {
            if(mIndices[i] == idim)
            {
               jdim = i;
            }
         }
         //MidasAssert(jdim != unsigned(-1), "Didnt find index :C");
         //MidasAssert(canonical_left->Extent(idim) == canonical_right->Extent(jdim), "indices not the same");
         T* mode_matrix_left  = canonical_left->GetModeMatrices()[idim];
         T* mode_matrix_right = canonical_right->GetModeMatrices()[jdim];
         T* intermeds = mIntermediates.get();

         intermeds += jdim*mRank2;
         transposematrix_times_matrix(mRankLeft, canonical_right->Extent(jdim), mRankRight, mode_matrix_left, mode_matrix_right, intermeds, true);
      }

      /**
       *
       **/
      unsigned Rank2
         (
         ) const
      {
         return mRank2;
      }
      
      /**
       *
       **/
      unsigned RankLeft
         (
         ) const
      {
         return mRankLeft;
      }
      
      /**
       *
       **/
      unsigned RankRight
         (
         ) const
      {
         return mRankRight;
      }


};

/**
 * @class GammaIntermediates
 **/
template
   <  typename T
   >
class GammaIntermediates<T>
{
   private:
      unsigned mNdim;
      unsigned mRank;
      unsigned mRank2;
      std::unique_ptr<T[]> mIntermediates;

   public:
      /**
       *
       **/
      GammaIntermediates(unsigned ndim, unsigned rank)
         : mNdim(ndim)
         , mRank(rank)
         , mRank2(rank*rank)
         , mIntermediates(new T[mNdim*mRank2])
      {
      }

      /**
       *
       **/
      template<class SmartPtr>
      void GammaMatrix
         (  unsigned idim
         ,  SmartPtr& gamma
         )
      {
         for(unsigned i = 0; i < mRank2; ++i)
         {
            gamma[i] = static_cast<T>(1.0);
         }
         
         T* intermeds = mIntermediates.get();
         for(int jdim = 0; jdim < mNdim; ++jdim)
         {
            if(jdim == idim) // if we hit idim, we just skip it by incrementing intermed pointer to next set of intermediates.
            {
               intermeds += mRank2;
            }
            else
            {
               T* gamma_ptr = gamma.get();
               for(unsigned irank2 = 0; irank2 < mRank2; ++irank2)
               {
                  *(gamma_ptr++) *= *(intermeds++);
               }
            }
         }
      }
      
      /**
       *
       **/
      void Initialize(const CanonicalTensor<T>* const canonical)
      {
         T** mode_matrices = canonical->GetModeMatrices();
         T* intermeds = mIntermediates.get();
         for(int idim = 0; idim < mNdim; ++idim)
         {
            transposematrix_times_matrix(mRank, canonical->Extent(idim), mRank, mode_matrices[idim], mode_matrices[idim], intermeds, true);
            intermeds += mRank2;
         }
      }

      /**
       *
       **/
      void Update(unsigned idim, const CanonicalTensor<T>* const canonical)
      {
         T* mode_matrix = canonical->GetModeMatrices()[idim];
         T* intermeds = mIntermediates.get(); 
         intermeds += idim*mRank2;
         transposematrix_times_matrix(mRank, canonical->Extent(idim), mRank, mode_matrix, mode_matrix, intermeds, true);
      }
};

/**
 * Compute the \f$ \Gamma matrix \f$ of dimension \f$ r_{left} \cdot r_{right} \f$.
 *
 * @param left    Pointer to the lhs canonical tensor.
 * @param right   Pointer to the rhs canonical tensor.
 * @param idim    The dimension to be left out.
 * @param gamma   On output the calculated gamma matrix.
 **/
template 
   <  class T
   ,  class SmartPtr
   >
void GammaMatrix
   (  const CanonicalTensor<T>* left
   ,  const CanonicalTensor<T>* right
   ,  const unsigned int idim
   ,  SmartPtr& gamma
   )
{
   if constexpr   (  midas::type_traits::IsComplexV<T>
                  )
   {
      MIDASERROR("CP decomposition is not adapted to complex numbers yet!");
   }

   assert_same_shape(left, right);

   unsigned totalsize = left->GetRank() * right->GetRank();

   for(unsigned i = 0; i < totalsize; ++i)
   {
      gamma[i] = 1.0;
   }

   auto temp = left->GetAllocatorTp().AllocateUniqueArray(totalsize);
   
   // calcualte the gamma matrix
   unsigned ndim = left->NDim();
   const auto& left_dims = left->GetDims();
   auto left_mode_matrices = left->GetModeMatrices();
   auto right_mode_matrices = right->GetModeMatrices();
   for(int jdim = 0; jdim < ndim; ++jdim)
   {
      if(jdim != idim)
      {
         transposematrix_times_matrix<T>(  left->GetRank()
                                        ,  left_dims[jdim]
                                        ,  right->GetRank()
                                        ,  left_mode_matrices[jdim]
                                        ,  right_mode_matrices[jdim]
                                        ,  temp.get()
                                        ,  true
                                        );
         element_wise_multiply<T>( gamma, temp, totalsize );
      }
   }
}

/**
 *
 **/
template
   <  class SmartPtr
   >
auto GammaNorm
   ( const SmartPtr& gamma
   , int rank
   )
{
   using element_type = typename SmartPtr::element_type;
   static_assert(std::is_floating_point<element_type>::value, "Only work for floating point types.");

   element_type gamma_norm = 0.0;
   int rank2 = rank*rank;
   for(int i = 0; i < rank2; ++i)
   {
      gamma_norm += gamma[i]*gamma[i];
   }
   gamma_norm = std::sqrt(gamma_norm);
   return gamma_norm;
}

#endif /* GAMMAMATRIX_H_INCLUDED */
