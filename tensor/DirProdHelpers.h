#ifndef DIRPRODHELPERS_H_INCLUDED
#define DIRPRODHELPERS_H_INCLUDED

#include "NiceTensor.h"
#include "inc_gen/TypeDefs.h"

//! Check that a vector of NiceTensor%s contains only SimpleTensor%s.
template
   <  typename T
   >
bool AllSimpleTensors
   (  const std::vector<NiceTensor<T> >& aInTensor
   )
{
   bool all_simple = true;
   for(int i = 0; i < aInTensor.size(); ++i)
   {
      all_simple = all_simple && (aInTensor[i].Type() == BaseTensor<T>::typeID::SIMPLE);
   }
   return all_simple;
}

//! Check that a vector of NiceTensor%s contains only CanonicalTensor%s.
template
   <  typename T
   >
bool AllCanonicalTensors
   (  const std::vector<NiceTensor<T> >& aInTensor
   )
{
   bool all_canonical = true;
   for(int i = 0; i < aInTensor.size(); ++i)
   {
      all_canonical = all_canonical && (aInTensor[i].Type() == BaseTensor<T>::typeID::CANONICAL);
   }
   return all_canonical;
}

//! Checks if a vector of NiceTensor%s contains a ZeroTensor
template
   <  typename T
   >
bool ContainsZeroTensor
   (  const std::vector<NiceTensor<T> >& aInTensor
   )
{
   for(const auto& tens : aInTensor)
   {
      if (  tens.Type() == BaseTensor<T>::typeID::ZERO
         )
      {
         return true;
      }
   }

   return false;
}

#endif /* DIRPRODHELPERS_H_INCLUDED */
