#ifndef DOTEVALUATOR_H_INCLUDED
#define DOTEVALUATOR_H_INCLUDED

// declaration
#include "DotEvaluator_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// implementation
#include "DotEvaluator_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* DOTEVALUATOR_H_INCLUDED */
