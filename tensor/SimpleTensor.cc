#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "SimpleTensor.h"
#include "SimpleTensor_Impl.h"

// define 
#define INSTANTIATE_SIMPLETENSOR(T) \
template class SimpleTensor<T>; \
\
template BaseTensorPtr<T>  random_simple_tensor(const std::vector<unsigned int>&); \
\
template SimpleTensor<T> identity_tensor(unsigned int dims); \
\
template SimpleTensor<T> diagonal_tensor(const std::vector<T>& diag);

// concrete instantiations
INSTANTIATE_SIMPLETENSOR(float)
INSTANTIATE_SIMPLETENSOR(double)
INSTANTIATE_SIMPLETENSOR(std::complex<float>)
INSTANTIATE_SIMPLETENSOR(std::complex<double>)

#undef INSTANTIATE_SIMPLETENSOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
