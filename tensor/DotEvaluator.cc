#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "BaseTensor.h"
#include "DotEvaluator.h"
#include "DotEvaluator_Impl.h"

// define
#define INSTANTIATE_DOTEVALUATOR(T) \
template class DotEvaluator<T>; \
template typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot<ModalEnergyHolder>(const CanonicalTensor<T>* const, const EnergyHolderTensorBase<T, ModalEnergyHolder>* const) const;

// concrete instantiations
INSTANTIATE_DOTEVALUATOR(float)
INSTANTIATE_DOTEVALUATOR(double)
INSTANTIATE_DOTEVALUATOR(std::complex<float>)
INSTANTIATE_DOTEVALUATOR(std::complex<double>)

#undef INSTANTIATE_DOTEVALUATOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
