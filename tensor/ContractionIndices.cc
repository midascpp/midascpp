#include<algorithm>

#include"ContractionIndices.h"

ContractionIndex::ContractionIndex(const char aChar):
    mChar(aChar)
{
}

char ContractionIndex::GetChar() const
{
   return mChar;
}

/// Equal-to operator.
bool ContractionIndex::operator ==(const ContractionIndex& other)
{
   return this->mChar==other.mChar;
}

ContractionIndices::ContractionIndices(const std::vector<ContractionIndex>& orig):
    std::vector<ContractionIndex>(orig)
{
}

ContractionIndices::ContractionIndices():
   ContractionIndices(std::vector<ContractionIndex>{})
{
}

ContractionIndices ContractionIndices::operator ,(ContractionIndex i)
{
    this->push_back(i);
    return *this;
}

std::vector<unsigned int> ContractionIndices::ToUIntVector() const
{
   std::vector<unsigned int> result;
   for(auto i: *this)
      result.push_back(i.GetChar());
   return result;
}

ContractionIndices operator ,(ContractionIndex i1, ContractionIndex i2)
{
   ContractionIndices result(std::vector<ContractionIndex>{i1, i2});
   return result;
}

ContractionIndices set_difference(const ContractionIndices& indices1, const ContractionIndices& indices2 )
{
   ContractionIndices result;
   for(auto i1: indices1)
   {
      bool found=false;
      for(auto i2: indices2)
      {
         if(i1==i2)
         {
            found=true;
            break;
         }
      }
      if(!found)
         result.push_back(i1);
   }
   return result;
}

// Returns all entries which only appear in either of the two vectors
ContractionIndices sorted_symmetric_difference(const ContractionIndices& indices1, const ContractionIndices& indices2 )
{
   ContractionIndices result   (set_difference(indices1, indices2));
   ContractionIndices b_minus_a(set_difference(indices2, indices1));
   result.insert(result.end(), b_minus_a.begin(), b_minus_a.end());
//   ContractionIndices remaining_indices;
//    Add all indices in i1 not present in i2
//   for(auto i1: indices1)
//   {
//      bool found=false;
//      for(auto i2: indices2)
//      {
//         if(i1==i2)
//         {
//            found=true;
//            break;
//         }
//      }
//      if(!found)
//         remaining_indices.push_back(i1);
//   }

//    Add all indices in i2 not present in i1
//   for(auto i2: indices2)
//   {
//      bool found=false;
//      for(auto i1: indices1)
//      {
//         if(i2==i1)
//         {
//            found=true;
//            break;
//         }
//      }
//      if(!found)
//         remaining_indices.push_back(i2);
//   }
   // Sort the result before returning
   std::sort(result.begin(), result.end(),
             [](const ContractionIndex& i1, const ContractionIndex& i2){return i1.GetChar()<i2.GetChar();});
   return result;
}
