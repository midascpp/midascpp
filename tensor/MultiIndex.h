#ifndef MULTIINDEX_H_INCLUDED
#define MULTIINDEX_H_INCLUDED

#include<vector>
#include<stdexcept>

#include "util/Io.h"

using multiindex_t       = std::vector<unsigned int>;
using indexswap_t        = std::pair<unsigned int, unsigned int>;
using indexpermutation_t = std::vector<indexswap_t>;

inline int compare(const multiindex_t& m1, const multiindex_t& m2)
{
   if(m1.size()!=m2.size())
   {
      //throw std::runtime_error(
      //   "Something went wrong, attempted to compare multiindex_t's of different sizes");
      MIDASERROR("Something went wrong, attempted to compare multiindex_t's of different sizes");
   }
   auto i1=m1.begin();
   auto i2=m2.begin();
   while(i1<m1.end())
   {
      if(*i1!=*i2)
      {
         if(*i1<*i2)
         {
            return -1;
         }
         else
         {
            return 1;
         }
      }
      ++i1;
      ++i2;
   }
   return 0;
}

inline bool operator< (const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) < 0;
}

inline bool operator<=(const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) <=0;
}

inline bool operator> (const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) > 0;
}

inline bool operator>=(const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) >=0;
}

inline bool operator==(const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) == 0;
}

inline bool operator!=(const multiindex_t& m1, const multiindex_t& m2)
{
   return compare(m1, m2) != 0;
}

inline multiindex_t& increase(multiindex_t& m, const unsigned int max)
{
   for(auto i=m.end()-1; i>=m.begin(); --i) // will this work ?? :O
   {
      if(*i<max-1)
      {
         ++(*i);
         break; // break for
      }
      else
      {
         *i=0;
      }
   }
   return m;
}

inline multiindex_t& permute(multiindex_t& m, const indexpermutation_t& p)
{
   for(auto swap: p)
   {
      std::swap(m[swap.first], m[swap.second]);
   }
   return m;
}

#endif /* MULTIINDEX_H_INCLUDED */
