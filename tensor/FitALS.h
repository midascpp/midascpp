#ifndef FITALS_H_INCLUDED
#define FITALS_H_INCLUDED

#include <numeric>

#include "tensor/FitReport.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/GammaMatrix.h"
#include "tensor/Squeezer.h"
#include "lapack_interface/math_wrappers.h"

#include "util/CallStatisticsHandler.h"

/*!
 * Input handler for FitALS function. Collects all settings for the CPALS algorithm.
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
struct CpAlsInput
{
   // 
   unsigned int mMaxiter = 10;
   unsigned int mCheckIter = -1; // Set to approx 10 for a VCC calc.
   T mCheckThresh = 1.e-4;
   T mMaxerr = 1e-6;
   T mRcond = 1e-12;
   bool mUseFitNormChange = false;
   bool mDistanceConvCheck = true;

   //! Use GELSD instead of GELSS for solving least-squares system
   In mCpAlsGelsd = I_20;

   //! Solve blockwise Newton equations in each iteration to obtain mode-matrix update
   bool mCpAlsNewton = false;

   //! Scale least-squares system by inverse target norm2
   bool mScaleLsSystem = false;

   //! Scale fit tensor to target norm before iterating
   bool mScaleFitTensor = false;

   //! Use line search in CP-ALS (scale update after solving the least-squares system)
   bool mCpAlsLineSearch = false;

   //! Tikhonov
   T mTikhonovQ = 0.1;
   T mTikhonovAlpha = static_cast<Nb>(0.0);
   bool mRALS = false;

   // Norm regularization
   T mRelativeLambda2 = C_0;

   //! Degeneracy check. Quit if largest mode-matrix element > mDegeneracyCheck*target_norm.
   bool mConditionCheck = false;

   //! Calculate error before iterations begin
   bool mInitialErrorCalculation = false;

   //! Balance mode vectors of result tensor
   bool mBalanceResult = false;

   //! IO level
   In mIoLevel = I_1;
   
   /**
    * Default constructor. Many defaults are probably the same as in TensorDecompInfo for FINDBESTCPALS.
    **/
   CpAlsInput()
   {
   }

   /**
    * Constructor from TensorDecompInfo.
    **/
   CpAlsInput(const TensorDecompInfo& aInfo)
      :  mMaxiter                (const_cast<TensorDecompInfo&>(aInfo)["CPALSMAXITER"].get<In>())
      ,  mCheckIter              (const_cast<TensorDecompInfo&>(aInfo)["CHECKITER"].get<In>())
      ,  mMaxerr                 (const_cast<TensorDecompInfo&>(aInfo)["CPALSTHRESHOLD"].get<Nb>())
      ,  mRcond                  (const_cast<TensorDecompInfo&>(aInfo)["CPALSRCOND"].get<Nb>())
      ,  mUseFitNormChange       (const_cast<TensorDecompInfo&>(aInfo)["CPALSUSEFITNORMCHANGE"].get<bool>())
      ,  mDistanceConvCheck      (const_cast<TensorDecompInfo&>(aInfo)["CPALSDISTANCECONVCHECK"].get<bool>())
      ,  mCpAlsGelsd             (const_cast<TensorDecompInfo&>(aInfo)["CPALSGELSD"].get<In>())
      ,  mCpAlsNewton            (const_cast<TensorDecompInfo&>(aInfo)["CPALSNEWTON"].get<bool>())
      ,  mScaleLsSystem          (const_cast<TensorDecompInfo&>(aInfo)["CPALSSCALELSSYSTEM"].get<bool>())
      ,  mScaleFitTensor         (const_cast<TensorDecompInfo&>(aInfo)["CPALSSCALEFITTENSOR"].get<bool>())
      ,  mCpAlsLineSearch        (const_cast<TensorDecompInfo&>(aInfo)["CPALSLINESEARCH"].get<bool>())
      ,  mTikhonovQ              (const_cast<TensorDecompInfo&>(aInfo)["CPALSTIKHONOVQ"].get<Nb>())
      ,  mTikhonovAlpha          (const_cast<TensorDecompInfo&>(aInfo)["CPALSTIKHONOVALPHA"].get<Nb>())
      ,  mRALS                   (const_cast<TensorDecompInfo&>(aInfo)["CPALSRALS"].get<bool>())
      ,  mRelativeLambda2        (const_cast<TensorDecompInfo&>(aInfo)["CPALSRELATIVELAMBDA2"].get<Nb>())
      ,  mConditionCheck         (const_cast<TensorDecompInfo&>(aInfo)["CPALSCONDITIONCHECK"].get<bool>())
      ,  mInitialErrorCalculation(const_cast<TensorDecompInfo&>(aInfo)["CPALSINITIALERRORCALCULATION"].get<bool>())
      ,  mBalanceResult          (const_cast<TensorDecompInfo&>(aInfo)["CPALSBALANCERESULT"].get<bool>())
      ,  mIoLevel                (const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].get<In>())
   {
   }

#define SETTER(NAME, TYPE) \
   CpAlsInput& Set##NAME(TYPE a##NAME) \
   { \
      m##NAME = a##NAME; \
      return *this; \
   }
   
   SETTER(Maxiter, unsigned)
   SETTER(CheckIter, unsigned)
   SETTER(CheckThresh, T)
   SETTER(CpAlsGelsd, bool)
   SETTER(CpAlsNewton, bool)
   SETTER(ScaleLsSystem, bool)
   SETTER(ScaleFitTensor, bool)
   SETTER(CpAlsLineSearch, bool)
   SETTER(DistanceConvCheck, bool)
   SETTER(Maxerr, T)
   SETTER(Rcond, T)
   SETTER(TikhonovAlpha, T)
   SETTER(RelativeLambda2, T)
   SETTER(BalanceResult, bool)
   SETTER(IoLevel, In)

#undef SETTER
};

/**
 * Find optimal lwork. This is done by a so-called workspace query, by passing in lwork := -1.
 * 
 * @param m      The number of rows of matrix A.
 * @param n      The number of cols of matrix A.
 * @param nrhs   The number of right hand sides.
 * @param gelsd  Use GELSD instead of GELSS
 * @return       Return the optimal size of lwork.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
std::pair<int,int> FindOptimalLWork
   (  int m
   ,  int n
   ,  int nrhs
   ,  bool gelsd
   )
{
   int lwork_query = -1;
   int lda = std::max(1,m);
   int ldb = std::max(1,std::max(m,n));
   T a;
   T b;
   T s;
   T rcond;
   int rank;
   T work; // work array, will only use 1-element
   int info;
   int iwork;

   if (  gelsd )
   {
      midas::lapack_interface::gelsd( &n, &m, &nrhs
                                    , &a, &lda
                                    , &b, &ldb
                                    , &s, &rcond
                                    , &rank, &work, &lwork_query
                                    , &iwork
                                    , &info
                                    );
   }
   else
   {
      // find optimal l-work (done by passing in lwork := -1)
      midas::lapack_interface::gelss( &n, &m, &nrhs
                                    , &a, &lda
                                    , &b, &ldb
                                    , &s, &rcond
                                    , &rank, &work, &lwork_query
                                    , &info
                                    );
      iwork = 0;
   }

   // check info for errors
   if(info != 0)
   {
      std::string routine = gelsd ? "GELSD" : "GELSS";
      MidasWarning(routine + " INFO = " + std::to_string(info));
   }

   int lwork_optimal = static_cast<int>(work);
   int iwork_optimal = static_cast<int>(std::max(1,iwork));

   return std::make_pair(lwork_optimal, iwork_optimal);
}


/**
 * Add a Tikhonov regularization term to \f$ \Gamma matrix \f$.
 *
 * @param gamma    The \f$ \Gamma \f$ matrix to update.
 * @param rank     Row and column size of the \f$ \Gamma \f$ matrix.
 * @param alpha    The regularization factor.
 **/
template
   <  class T
   ,  class SmartPtr
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
void TikhonovRegularization
   (  SmartPtr& gamma
   ,  int rank
   ,  T alpha
   )
{
   // add the thikhonov term
   auto gamma_ptr = gamma.get();
   for(int irank = 0; irank < rank; ++irank)
   {
      (*gamma_ptr) += alpha;
      gamma_ptr += rank + 1;
   }
}

/**
 * Regularize the gamma matrix with the second condition described in
 * [Espig & Hackbusch: A regularized Newton method for the efficient approximation...](https://link.springer.com/article/10.1007%2Fs00211-012-0465-9)
 *
 * @param gamma            The gamma matrix
 * @param rank             Rank
 * @param lambda           Scaling factor for norm restriction (lambda2)
 **/
template
   <  class T
   ,  class SmartPtr
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
void NormRegularization
   (  SmartPtr& gamma
   ,  int rank
   ,  T lambda
   )
{
   auto gamma_ptr = gamma.get();

   // Regularize gamma matrix
   for(int irank = 0; irank < rank; ++irank)
   {
      (*gamma_ptr) *= (static_cast<T>(1.)+lambda);

      gamma_ptr += rank + 1;
   }
}

/**
 * Regularize rhs matrix.
 * 
 * @param rhs                     The rhs matrix. On ouput the regularized matrix
 * @param rank                    The rank, i.e. the number of rows of rhs
 * @param idim                    The dimension number, giving the number of cols
 * @param fitting_tensor          The canonical fitting tensor
 * @param alpha                   The regularization parameter
 **/
template
   <  class T
   ,  class SmartPtr
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
void RhsRegularization
   (  SmartPtr& rhs
   ,  int rank
   ,  int idim
   ,  const CanonicalTensor<T>* const fitting_tensor
   ,  T alpha
   )
{
   auto extent = fitting_tensor->Extent(idim);
   auto mode_matrix = fitting_tensor->GetModeMatrices()[idim];
   
   auto rhs_ptr = rhs.get();
   for(int iextent = 0; iextent < extent; ++iextent)
   {
      auto mode_matrix_ptr = mode_matrix + iextent;
      for(int irank = 0; irank < rank; ++irank)
      {
         *(rhs_ptr++) += alpha * *(mode_matrix_ptr);
         mode_matrix_ptr += extent;
      }
   }
}
   
/**
 * Fit a target tensor to CP format using an ALS algorithm.
 *
 * @param target_tensor
 * @param target_norm2
 * @param fitting_tensor
 * @param input
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitALS
   (  const BaseTensor<T>* const target_tensor
   ,  const T target_norm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpAlsInput<T>& input
   )
{
   //#####################################
   //# initilalization
   //#####################################
   //LOGCALL("calls");
   assert_same_shape(fitting_tensor, target_tensor);

   auto norm = std::sqrt(target_norm2);
   if(fitting_tensor->GetRank() == 0)
   {
      return FitReport<T>{norm, norm, 0};
   }

   auto inv_norm2 = target_norm2 < static_cast<T>(0.) ? static_cast<T>(1.) : static_cast<T>(1.) / target_norm2;

   // Setup some sizes
   const int ndim = fitting_tensor->NDim();
   const int max_extent = fitting_tensor->MaxExtent();
   int rank = fitting_tensor->GetRank();
   int rank2 = rank*rank;

   // Setup loop order of modes (relevant for intermediates in TensorDirectProduct)
   std::vector<unsigned> mode_indices;
   if (  target_tensor->Type() == BaseTensor<T>::typeID::DIRPROD
      )
   {
      mode_indices.reserve(ndim);
      const auto& connect = static_cast<const TensorDirectProduct<T>* const>(target_tensor)->GetReverseConnection();
      for(const auto& tens_dims : connect)
      {
         for(const auto& dim : tens_dims)
         {
            mode_indices.emplace_back(dim);
         }
      }
   }
   else
   {
      mode_indices.resize(ndim);
      std::iota(mode_indices.begin(), mode_indices.end(), 0);
   }
   
   // initialize iteration variables
   unsigned int iter;
   unsigned int maxiter = input.mMaxiter;
   unsigned int checkiter = input.mCheckIter;
   T checkthresh = input.mCheckThresh;
   T maxerr = input.mMaxerr;
   T rcond  = input.mRcond;
   bool usefitnormchange = input.mUseFitNormChange;
   bool balance_result = input.mBalanceResult;
   bool cpalsgelsd = (rank >= input.mCpAlsGelsd) && (input.mCpAlsGelsd > 0);
   bool scalefittensor = input.mScaleFitTensor;
   bool newton = input.mCpAlsNewton;
   bool scale_ls_system = input.mScaleLsSystem && libmda::numeric::float_pos(target_norm2);
   bool line_search = input.mCpAlsLineSearch;
   bool dist_conv_check = input.mDistanceConvCheck;
   bool check_condition = input.mConditionCheck;
   T err_old = norm;
   T err_new    = static_cast<T>(0.0); 
   T err_change = static_cast<T>(0.0);
   T dist_new = static_cast<T>(0.);
   T dist_rel_change = static_cast<T>(0.);
   T dist_old = std::sqrt(static_cast<T>(1.) + err_old*err_old);
   T step_size = static_cast<T>(1.);
   bool converged = false;
   In io = input.mIoLevel;

   // setup some sizes
   auto workspace = FindOptimalLWork<T>(rank, rank, max_extent, cpalsgelsd);
   int lwork = workspace.first;
   int iwork = workspace.second;
   std::unique_ptr<int[]> iworkspace = nullptr;

   // setup some working space
   auto rhs = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank*max_extent);
   auto work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(lwork); // allocate working array with optimal lwork
   auto s = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank);
   auto gamma = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank2);
   if (  cpalsgelsd  )
   {
      iworkspace = std::unique_ptr<int[]>(new int[iwork]);
   }

   // tikhonov
   bool tikhonov = bool(input.mTikhonovAlpha);
   bool rals = input.mRALS;
   T q = input.mTikhonovQ;
   T alpha = input.mTikhonovAlpha;

   // espig norm regularization
   T num_eps = std::numeric_limits<T>::epsilon();
   bool norm_reg = bool(input.mRelativeLambda2);
   T rel_lambda2 = input.mRelativeLambda2;

   // setup intermediate machines
   GeneralGammaIntermediates<T> gamma_intermeds(fitting_tensor, fitting_tensor);
   Squeezer<T> squeezer(fitting_tensor, target_tensor);

   // Calculate fitting norm
   T fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma);
   T fitting_norm_old = std::sqrt(fitting_norm2);
   T fitting_norm_new = static_cast<T>(0.0);
   T fitting_norm_change = static_cast<T>(0.0);

   if (  scalefittensor
      && libmda::numeric::float_pos(target_norm2)
      )
   {
      auto coef = std::sqrt(target_norm2 / fitting_norm2);
      fitting_tensor->Scale(coef);
      fitting_norm2 = target_norm2;
   }

   // Calculate first error (allows for err_change and dist_rel_change convergence check after 1 iteration)
   bool initial_error_calc = input.mInitialErrorCalculation;
   if (  initial_error_calc
      || target_norm2 < C_0
      )
   {
      err_old = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
      dist_old = std::sqrt(static_cast<T>(1.)+err_old*err_old);
   }
   
   //#####################################
   //# start iteration loop
   //#####################################
   for(iter = 0; iter < maxiter; ++iter)
   {
      if (  io >= I_10
         )
      {
         Mout << "|===== FitALS iteration " << iter << " =====|" << std::endl;
      }

      if (  line_search
         && newton
         )
      {
         // Niels: Bro suggests iter^(1/3) for 3rd-order tensors.
         // However, he only performs the line search after looping through all
         // dimensions. This costs extra memory, though.
         step_size = std::pow(iter+1, 1./ndim);
      }

//      LOGCALL("loop");
//      for(int idim = 0; idim < ndim; ++idim)
      for(const auto& idim : mode_indices)
      {
//         LOGCALL("dim loop");

         int extent = fitting_tensor->Extent(idim);

         // Squeezer
         {
            //LOGCALL("squeeze");
            squeezer.SqueezeModeMatrix(idim, rhs, true);
         }
         // Gamma matrix
         {
            //LOGCALL("gamma");
            gamma_intermeds.GammaMatrix(idim, gamma);
         }

         // Scale both sides of least-squares system by 1/||target||^2
         if (  scale_ls_system
            )
         {
            for(size_t ielem=0; ielem<rank2; ++ielem)
            {
               gamma[ielem] *= inv_norm2;
            }
            for(size_t jelem=0; jelem<rank*extent; ++jelem)
            {
               rhs[jelem] *= inv_norm2;
            }
         }

         // Tikhonov regularization
         if (  tikhonov
            ) 
         {
            // Scale alpha by gamma norm
            T gamma_norm = GammaNorm(gamma, rank);
            T alpha_i = gamma_norm * alpha;

            if (  scale_ls_system   )
            {
               alpha_i *= inv_norm2;
            }
            TikhonovRegularization(gamma, rank, alpha_i);
            if (  rals  )
            {
               RhsRegularization(rhs, rank, idim, fitting_tensor, alpha_i);
            }
            
            // setup alpha for next iteration
            alpha *= q;
         }

         // Norm regularization (Espig)
         if (  norm_reg
            )
         {
            T scaling = rel_lambda2 * std::pow(num_eps, 2) / (2. * maxerr);

            NormRegularization(gamma, rank, scaling);
         }


         // If Newton, set rhs to gradient block
         if (  newton   )
         {
//            LOGCALL("gradient block");
            fitting_tensor->Gradient(idim, gamma, rhs, -C_1, true);
         }

         // Solve linear system
         int nrhs = extent;
         int out_rank;
         int info;
         
         if (  cpalsgelsd  )
         {
            //LOGCALL("DGELSD");
            midas::lapack_interface::gelsd( &rank, &rank, &nrhs
                                          , gamma.get() , &rank
                                          , rhs.get()   , &rank
                                          , s.get()     , const_cast<T*>(&rcond)
                                          , &out_rank, work.get(), &lwork
                                          , iworkspace.get()
                                          , &info
                                          );
         }
         else
         {
            //LOGCALL("DGELSS");
            midas::lapack_interface::gelss( &rank, &rank, &nrhs
                                          , gamma.get() , &rank
                                          , rhs.get()   , &rank
                                          , s.get()     , const_cast<T*>(&rcond)
                                          , &out_rank, work.get(), &lwork
                                          , &info
                                          );
         }

         if(info != 0)
         {
            std::string routine = cpalsgelsd ? "GELSD" : "GELSS";

            MidasWarning(routine+" INFO = "+std::to_string(info) );

            if (  std::abs(info) == 4
               )
            {
               auto gamma_norm = GammaNorm(gamma, rank);
               Mout  << " Gamma norm = " << gamma_norm << "\n"
                     << " Rank       = " << rank << std::endl;
            }
         }

         if (  newton   )
         {
//            LOGCALL("update mode matrix");
            fitting_tensor->ModeMatrixAxpy(idim, rhs.get(), step_size, true);
         }
         else  // Put the transpose of Q into the new A
         {
//            LOGCALL("put new transposed");
            fitting_tensor->PutNewTransposeModeMatrix(rhs.get(), idim);
         }

         // Update intermediates
         {
            //LOGCALL("GAMMA UPDATE");
            gamma_intermeds.Update(idim, fitting_tensor, fitting_tensor);
         }
         {
            //LOGCALL("SQUEEZER UPDATE");
            squeezer.Update(idim);
         }
      }

      // Calculate norm2 of fit
      {
//         LOGCALL("fitting norm2");
         fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma);
      }
      
      // calculate errors
      if (  usefitnormchange  )
      {
         fitting_norm_new = std::sqrt(fitting_norm2);
         fitting_norm_change = std::fabs(fitting_norm_new - fitting_norm_old);
         fitting_norm_old = fitting_norm_new;
      }
      else
      {
//         LOGCALL("calculate error");

         err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
         err_change = std::fabs(err_old - err_new);

         dist_new = std::sqrt(static_cast<T>(1.) + err_new*err_new);
         dist_rel_change = std::fabs(dist_old - dist_new) / dist_new;

         // test for NaN
         if (  err_new != err_new
            || dist_new != dist_new
            )
         {
            auto cond_coef = gamma_intermeds.ConditionCoefficient();
            auto condition_number = std::sqrt(cond_coef / fitting_norm2);
            MidasWarning(" WARNING bad CP-ALS fit of " + target_tensor->ShowType() + ".");
            Mout  << "    Condition number:        " << condition_number << "\n"
                  << "    Should be smaller than:  " << maxerr / ( num_eps * std::sqrt(fitting_norm2) ) << std::endl;

            break;
         }
         err_old = err_new;
         dist_old = dist_new;
      }

      if (  io >= I_10
         )
      {
         Mout  << "   err_new          : " << err_new << "\n"
               << "   err_change       : " << err_change << "\n"
               << "   dist_new         : " << dist_new << "\n"
               << "   dist_rel_change  : " << dist_rel_change << "\n"
               << std::endl;
      }

      // Check convergence
      if (  (  iter > 0
            || initial_error_calc
            )
         && (  err_change < maxerr
            || (  dist_conv_check
               && dist_rel_change < maxerr
               )
            )
         && fitting_norm_change < maxerr
         )
      {
         converged = true;
         break;
      }

      // Break the iterations at iteration aInfo.mCheckIter
      // if the error is too much larger than the threshold.
      if (  iter == checkiter   
         && err_new > checkthresh
         )
      {
         break;
      }

      // Check conditioning
      if (  check_condition
         )
      {
         auto cond_coef = gamma_intermeds.ConditionCoefficient();

         if (  libmda::numeric::float_lt<T>(std::sqrt(cond_coef), maxerr / num_eps)  )
         {
            auto condition_number = std::sqrt(cond_coef / fitting_norm2);
            MidasWarning("FitALS: Ill-conditioning detected!");
            Mout  << " Condition number = " << condition_number << "\n"
                  << " Maxerr           = " << maxerr << std::endl;
         }
      }
   }
   
   // calculate error if using fit-norm change
   if (  usefitnormchange  )
   {
      err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
   }

   // Balance result tensor
   if (  balance_result
      )
   {
      fitting_tensor->BalanceModeVectors();
   }

   return FitReport<T>(err_new, norm, iter, err_change, 0., converged);
}

/**
 * Fit a target tensor to CP format using an ALS algorithm.
 *
 * @param target_tensor
 * @param target_norm2
 * @param fitting_tensor
 * @param maxiter
 * @param maxerr
 * @param rcond
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitALS
   ( const BaseTensor<T>* const target_tensor
   , const T target_norm2
   , CanonicalTensor<T>* const fitting_tensor
   , const unsigned int maxiter
   , const T maxerr
   , const T rcond
   )
{
   return FitALS(target_tensor, target_norm2, fitting_tensor, CpAlsInput<T>().SetMaxiter(maxiter)
                                                                             .SetMaxerr(maxerr)
                                                                             .SetRcond(rcond)
                );
}


/**
 * Fit a tensor to CP format using a pivotised ALS method.
 *
 * @param target_tensor       The target tensor.
 * @param target_norm2        Squared norm of target tensor.
 * @param fitting_tensor      The fitting tensor (starting guess).
 * @param input               Input parameters for CP-ALS algorithm.
 * @return
 *    FitReport of the final fit.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitPALS
   (  const BaseTensor<T>* const target_tensor
   ,  const T target_norm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpAlsInput<T>& input
   )
{
   //#####################################
   //# initilalization
   //#####################################
//   LOGCALL("calls");
   assert_same_shape(fitting_tensor, target_tensor);

   auto norm = std::sqrt(target_norm2);
   if(fitting_tensor->GetRank() == 0)
   {
      return FitReport<T>{norm, norm, 0};
   }

   // setup setup some sizes
   const int ndim = fitting_tensor->NDim();
   const int max_extent = fitting_tensor->MaxExtent();
   int rank = fitting_tensor->GetRank();
   int rank2 = rank*rank;

   // Initialize iteration variables
   unsigned iter=0;
   unsigned int checkiter = input.mCheckIter;
   T checkthresh = input.mCheckThresh;
   unsigned maxiter = input.mMaxiter;
   bool cpalsgelsd = (rank >= input.mCpAlsGelsd) && (input.mCpAlsGelsd > 0);
   bool balance_result = input.mBalanceResult;
   bool newton = input.mCpAlsNewton;
   bool line_search = input.mCpAlsLineSearch;
   bool dist_conv_check = input.mDistanceConvCheck;
   T maxerr = input.mMaxerr;
   T rcond  = input.mRcond;
   T grad_norm = static_cast<T>(1.);
   T err_old = norm;
   T err_new = static_cast<T>(0.);
   T err_change = static_cast<T>(0.);
   T dist_new = static_cast<T>(0.);
   T dist_rel_change = static_cast<T>(0.);
   T dist_old = std::sqrt(static_cast<T>(1.) + err_old*err_old);
   T step_size = static_cast<T>(1.);
   std::pair<unsigned, T> pivot = { 0, static_cast<T>(0.) };
   bool converged = false;
   In io = input.mIoLevel;
   bool check_condition = input.mConditionCheck;

   auto workspace = FindOptimalLWork<T>(rank, rank, max_extent, cpalsgelsd);
   int lwork = workspace.first;
   int iwork = workspace.second;
   std::unique_ptr<int[]> iworkspace = nullptr;

   // tikhonov
   bool tikhonov = bool(input.mTikhonovAlpha);
   bool rals = input.mRALS;
   T q = input.mTikhonovQ;
   T alpha = input.mTikhonovAlpha;

   // espig norm regularization
   T num_eps = std::numeric_limits<T>::epsilon();
   bool norm_reg = bool(input.mRelativeLambda2);
   T rel_lambda2 = input.mRelativeLambda2;

   // setup some working space
   auto rhs = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank*max_extent);
   auto work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(lwork); // allocate working array with optimal lwork
   auto s = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank);
   auto gamma = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(rank2);
   if (  cpalsgelsd  )
   {
      iworkspace = std::unique_ptr<int[]>(new int[iwork]);
   }

   // Initialize intermediate machines
   GeneralGammaIntermediates<T> gamma_intermeds(fitting_tensor, fitting_tensor);
   Squeezer<T> squeezer(fitting_tensor, target_tensor);

   // Init fitting norm2
   auto fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma);

   // Calculate first error (allows for err_change and dist_rel_change convergence check after 1 iteration)
   bool initial_error_calc = input.mInitialErrorCalculation;
   if (  initial_error_calc
      )
   {
      err_old = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
      dist_old = std::sqrt(static_cast<T>(1.)+err_old*err_old);
   }

   // Calculate first gradient
   CanonicalTensor<T> gradient(fitting_tensor->GetDims(), rank);
   fitting_tensor->Gradient(gradient, gamma_intermeds, squeezer, gamma, work);

   grad_norm = gradient.ElementwiseNorm();

   if (  grad_norm < maxerr   )
   {
      return FitReport<T>{diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot()), norm, iter};
   }

   //#####################################
   //# start iteration loop
   //#####################################
   for(iter = 0; iter < maxiter; ++iter)
   {
//      LOGCALL("iteration");

      if (  io >= I_10
         )
      {
         Mout << "|===== FitPALS iteration " << iter << " =====|" << std::endl;
      }

      if (  line_search
         && newton
         )
      {
         step_size = std::pow(iter+1, 1./ndim);
      }

      // Find max. gradient block
      pivot =  gradient.MaxModeMatrixElement();
//      pivot =  gradient.MaxModeMatrixNorm2();

      const auto& max_dim = pivot.first;

      // Construct RHS
      if (  newton   )
      {
//         LOGCALL("set rhs from gradient");
         T* grad_ptr_ref = gradient.GetModeMatrices()[max_dim];
         T* rhs_ptr = rhs.get();

         auto extent = fitting_tensor->Extent(max_dim);
         for(unsigned i=0; i<extent; ++i)
         {
            T* grad_ptr = grad_ptr_ref + i;
            for(unsigned irank=0; irank<rank; ++irank)
            {
               *(rhs_ptr++) = static_cast<T>(-1.) * *(grad_ptr);
               grad_ptr += extent;
            }
         }
      }
      else
      {
//         LOGCALL("squeeze");
         squeezer.SqueezeModeMatrix(max_dim, rhs, true);
      }

      // Gamma matrix
      {
//         LOGCALL("gamma");
         gamma_intermeds.GammaMatrix(max_dim, gamma);
      }

      // Tikhonov regularization
      if (  tikhonov 
         && !newton
         ) 
      {
         T gamma_norm = GammaNorm(gamma, rank);
         T alpha_i = gamma_norm * alpha;
         TikhonovRegularization(gamma, rank, alpha_i);
         if (  rals  )
         {
            RhsRegularization(rhs, rank, max_dim, fitting_tensor, alpha_i);
         }
         alpha *= q; // setup alpha for next iteration
      }

      // Norm regularization (Espig)
      if (  norm_reg
         && !newton
         )
      {
         T scaling = rel_lambda2 * std::pow(num_eps, 2) / (2. * maxerr);

         NormRegularization(gamma, rank, scaling);
      }

      // Solve linear system
      int nrhs = fitting_tensor->Extent(max_dim);
      int out_rank;
      int info;
      if (  cpalsgelsd  )
      {
//         LOGCALL("DGELSD");
         midas::lapack_interface::gelsd( &rank, &rank, &nrhs
                                       , gamma.get() , &rank
                                       , rhs.get()   , &rank
                                       , s.get()     , const_cast<T*>(&rcond)
                                       , &out_rank, work.get(), &lwork
                                       , iworkspace.get()
                                       , &info
                                       );
      }
      else
      {
//         LOGCALL("DGELSS");
         midas::lapack_interface::gelss( &rank, &rank, &nrhs
                                       , gamma.get() , &rank
                                       , rhs.get()   , &rank
                                       , s.get()     , const_cast<T*>(&rcond)
                                       , &out_rank, work.get(), &lwork
                                       , &info
                                       );
      }

      if(info != 0)
      {
         std::string routine = cpalsgelsd ? "GELSD" : "GELSS";
         MidasWarning(routine + " INFO = " + std::to_string(info));
      }


      // If Newton, we subtract the result from the mode matrix
      if (  newton   )
      {
//         LOGCALL("update mode matrix");
         fitting_tensor->ModeMatrixAxpy(max_dim, rhs.get(), step_size, true);
      }
      // Else put the transpose of Q into the new A
      else
      {
//         LOGCALL("put new transposed");
         fitting_tensor->PutNewTransposeModeMatrix(rhs.get(), max_dim);
      }

      // Update intermediates
      {
//         LOGCALL("GAMMA UPDATE");
         gamma_intermeds.Update(max_dim, fitting_tensor, fitting_tensor);
      }
      {
//         LOGCALL("SQUEEZER UPDATE");
         squeezer.Update(max_dim);
      }

      // Update gradient
      {
//      LOGCALL("new gradient");
      fitting_tensor->Gradient(gradient, gamma_intermeds, squeezer, gamma, work);
      }

      // Calculate gradient norm
      grad_norm = gradient.ElementwiseNorm();

      // Update fitting_norm2
      {
//      LOGCALL("fitting norm2");
      fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma);
      }

      // Calculate error
      {
//         LOGCALL("calculate error");
         err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
         err_change = std::fabs(err_old - err_new);

         dist_new = std::sqrt(static_cast<T>(1.) + err_new*err_new);
         dist_rel_change = std::fabs(dist_old - dist_new) / dist_new;

         // test for NaN
         if (  err_new != err_new
            || dist_new != dist_new
            )
         {
            MidasWarning(" WARNING bad CP-ALS fit. ");
         }
         err_old = err_new;
         dist_old = dist_new;
      }

      if (  io >= I_10
         )
      {
         Mout << "   update mode    :  " << max_dim << "\n"
              << "   pivot value    :  " << pivot.second << "\n"
              << "   err_new        :  " << err_new << "\n"
              << "   err_change     :  " << err_change << "\n"
              << "   dist_new       :  " << dist_new << "\n"
              << "   dist_rel_change:  " << dist_rel_change << "\n"
              << std::endl;
      }

      // Check convergence
      if (  (  iter > 0
            || initial_error_calc   // If the err_0 has been calculated, we can check err_change
            )
         && grad_norm < maxerr      // Gradient must be converged
         && (  err_change < maxerr
            || (  dist_conv_check   // If dist_conv_check is allowed, it is checked together with err_change
               && dist_rel_change < maxerr
               )
            )
         )
      {
         converged = true;
         break;
      }

      // Break the iterations at iteration aInfo.mCheckIter
      // if the error is too much larger than the threshold.
      if (  iter == checkiter   
         && err_new > checkthresh
         )
      {
         break;
      }

      // Check conditioning
      if (  check_condition
         )
      {
         auto cond_coef = gamma_intermeds.ConditionCoefficient();

         if (  libmda::numeric::float_lt<T>(std::sqrt(cond_coef), maxerr / num_eps)  )
         {
            auto condition_number = std::sqrt(cond_coef / fitting_norm2);
            MidasWarning("FitPALS: Ill-conditioning detected!");
            Mout  << " Condition number = " << condition_number << "\n"
                  << " Maxerr           = " << maxerr << std::endl;
         }
      }
   }

   // Balance result tensor
   if (  balance_result
      )
   {
      fitting_tensor->BalanceModeVectors();
   }

   return FitReport<T>(err_new, norm, iter, err_change, grad_norm, converged);
}

#endif /* FITALS_H_INCLUDED */
