#ifndef TENSORVIEW_IMPL_H_INCLUDED
#define TENSORVIEW_IMPL_H_INCLUDED

template<class T>
typename DotEvaluator<T>::dot_t TensorView<T>::Dot
   ( const BaseTensor<T>* const other
   ) const
{
   return other->DotDispatch(this);
}

#endif /* TENSORVIEW_IMPL_H_INCLUDED */
