/**
************************************************************************
* 
* @file    CpGuesser.h
*
* @date    01-12-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Include file for CpGuesser class
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/
#ifndef CPGUESSER_H_INCLUDED
#define CPGUESSER_H_INCLUDED

// Include declaration
#include "tensor/CpGuesser_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "tensor/CpGuesser_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* CPGUESSER_H_INCLUDED */
