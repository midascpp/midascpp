#ifndef FORWARDDECL_H_INCLUDED
#define FORWARDDECL_H_INCLUDED

#include <memory>
#include <functional>
#include <vector>
#include "util/type_traits/Complex.h"

/*! Forward declare all tensor classes that are supposed to inherit from BaseTensor
 *
 */
// BaseTensor
template<class T>
class BaseTensor;
// SimpleTensor
template<class T>
class SimpleTensor;
// CanonicalTensor
template
   <  class T
   >
class CanonicalTensor;
// Scalar
template<class T>
class Scalar;
// ZeroTensor
template<class T>
class ZeroTensor;
// TensorGraph
template<class T>
class TensorGraph;
// TensorView
template<class T>
class TensorView;
// CanonicalIntermediate
template<class T>
class CanonicalIntermediate;
// TensorDirectPRoduct
template<class T>
class TensorDirectProduct;
// TensorDirectPRoduct
template<class T>
class TensorSum;
// EnergyHolderTensor
template
   <  class T
   ,  template<class> class IMPL
   >
class EnergyHolderTensorBase;
// EnergyDenominatorTensor
template<class T>
class EnergyDenominatorTensor;
// ExactEnergyDenominatorTensor
template<class T>
class ExactEnergyDenominatorTensor;
// EnergyDifferencesTensor
template<class T>
class EnergyDifferencesTensor;
// ExactEnergyDifferencesTensor
template<class T>
class ExactEnergyDifferencesTensor;

// Disable FitReport for complex numbers
template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
struct FitReport;


// Definitions
// define BaseTensorPtr as a unique ptr
template<class T>
using BaseTensorPtr = std::unique_ptr<BaseTensor<T> >;
// define BaseTensorPtr as a unique ptr
template<class T>
using ConstBaseTensorPtr = std::unique_ptr<const BaseTensor<T> >;
// define BaseTensorPtr as a unique ptr
template<class T>
using BaseTensorRawPtr = BaseTensor<T>*;
// define BaseTensorPtr as a unique ptr
template<class T>
using ConstBaseTensorRawPtr = const BaseTensor<T>*;

// define ModalEnergyHolderTensor
template<class T>
class ModalEnergyHolder;
template<class T>
using ModalEnergyHolderTensor = EnergyHolderTensorBase<T, ModalEnergyHolder>;

//template<class T>
//using OrbitalEnergyHolderTensor = EnergyHolderTensorBase<T, OrbitalEnergyHolder>;



// Functions
// Forward decl
template
   <  class T
   >
BaseTensorPtr<T> random_canonical_tensor_explicit
   (  const std::vector<unsigned int>&
   ,  const unsigned int
   );

template
   <  class T
   >
BaseTensorPtr<T> cross_approximation
   (  const BaseTensor<T>* const
   ,  const std::vector<unsigned>&
   ,  const unsigned
   );

template
   <  class T
   >
BaseTensorPtr<T> const_value_cptensor
   (  const std::vector<unsigned int>& arDims
   ,  const T aValue
   ,  const unsigned int aRank
   );

template <class T>
BaseTensorPtr<T> random_simple_tensor
   (  const std::vector<unsigned int>& dims
   );

template
   <  class T
   ,  class U = T
   ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
   >
CanonicalTensor<T> compress_to_canonical
   (  const BaseTensor<T>& target
   ,  typename BaseTensor<T>::real_t maxerror
   ,  std::function<CanonicalTensor<T> (const unsigned int)> guesser=nullptr
   ,  FitReport<U>(CanonicalTensor<T>::*fitter)(const BaseTensor<T>&, const unsigned int, const typename BaseTensor<T>::real_t, midas::type_traits::DisableIfComplexT<U>*)=&CanonicalTensor<T>::template FitALS<U>
   );



#endif /* FORWARDDECL_H_INCLUDED */
