#ifndef BASETENSORALLOCATOR_H_INCLUDED
#define BASETENSORALLOCATOR_H_INCLUDED

#include "util/Memalloc.h"

namespace midas
{
namespace tensor
{

/**
 * Class to manage memory allocation for Tensor hierarchy.
 **/
template
   <  class T
#ifdef MEMPOOL_SIZE
   ,  class Alloc = memalloc::mempool_allocator<T>
#else
   ,  class Alloc = memalloc::allocator<T>
#endif /* MEMPOOL_SIZE */
   >
class BaseTensorAllocator
   :  private memalloc::allocatable<T , Alloc>
{
   private:
      using Allocatable  = memalloc::allocatable<T , Alloc>;

   public:
      using UniqueArray = typename Allocatable::unique_array;
      using RawPointer = typename Allocatable::pointer;

      //! Allocate raw pointer
      auto Allocate(std::size_t aN, typename Allocatable::const_pointer aHint = nullptr) const
      {
         return const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).allocate(aN, aHint);
      }
      
      //! Deallocate raw pointer
      void Deallocate(typename Allocatable::pointer aPtr, std::size_t aN) const
      {
         const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).deallocate(aPtr, aN);
      }
      
      //! Allocate unique pointer of type T.
      auto AllocateUniquePointer(typename Allocatable::const_pointer aHint = nullptr) const
      {
         return const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).allocate_unique_pointer(aHint);
      }

      //! Allocate unique array of type T with size n.
      auto AllocateUniqueArray(std::size_t aN, typename Allocatable::const_pointer aHint = nullptr) const
      {
         return const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).allocate_unique_array(aN, aHint);
      }
      
      //! Allocate and construct unique pointer of type T.
      template<class... Args>
      auto AllocateAndConstructUniquePointer(typename Allocatable::const_pointer aHint = nullptr, Args&&... aArgs) const
      {
         return const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).allocate_and_construct_unique_pointer(aHint, std::forward<Args>(aArgs)...);
      }
      
      //! Allocate and construct unique array of type T and size n.
      template<class... Args>
      auto AllocateAndConstructUniqueArray(std::size_t aN, typename Allocatable::const_pointer aHint = nullptr, Args&&... aArgs) const
      {
         return const_cast<Allocatable&>(static_cast<const Allocatable&>(*this)).allocate_and_construct_unique_pointer(aN, aHint, std::forward<Args>(aArgs)...);
      }

};

} /* namespace tensor */
} /* namespace midas */

#endif  /* BASETENSORALLOCATOR_H_INCLUDED */
