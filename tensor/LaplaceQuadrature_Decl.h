#ifndef LAPLACEQUADRATURE_DECL_H_INCLUDED
#define LAPLACEQUADRATURE_DECL_H_INCLUDED

#include "EnergyHistogram.h"
#include "lapack_interface/math_wrappers.h"
#include "util/MidasSystemCaller.h"
#include "util/DefaultParameterProxy.h"
#include "tensor/LaplaceInfo.h"
#include "solver/downhill_simplex.h"

#include "input/LaplaceInput.h"

namespace midas
{
namespace tensor
{
namespace detail
{
///> Generate the name of the correct data file from number of points and interval length
const std::string generate_file_name(const In, const Nb);
} /* namespace detail */
} /* namespace tensor */
} /* namespace midas */


// Forward decl
template
   <  class T
   >
class LaplaceQuadrature;

template
   <  class T
   >
std::ostream& operator<<
   (  std::ostream&
   ,  const LaplaceQuadrature<T>&
   );

template
   <  class T
   >
class LaplaceErrorFunction;


/**
 *
 **/
template
   <  class T
   >
class LaplaceQuadratureBase
{
   friend LaplaceErrorFunction<T>;

   private:
      //! Minimal X for which denominator was refitted
      T mXmin = static_cast<T>(0.);

      //! Minimal y for which denominator was refitted
      T mXmax = static_cast<T>(0.);

      //! A distribution of eigenvalues 
      EnergyHistogram<T> mEigen;

      //! The interval has been moved from the negative to the positive axis
      bool mNegativeInterval = false;

   protected:
      //! Number of grid points
      unsigned mNumPoints = 4;

      //! Quadrature grid points
      std::vector<T> mPoints;

      //! Quadrature weights
      std::vector<T> mWeights;

      //! Least-squares error
      T mLsError = static_cast<T>(-1.);

      //! Norm2 of function on interval (used in error calculation)
      T mNorm2 = static_cast<T>(0.);

   private:
      //! Get least-squares error for given points and weights.
      virtual T LeastSquaresError
         (  const std::vector<T>& points
         ,  const std::vector<T>& weights
         ,  T thresh = static_cast<T>(1.e-20)
         )  = 0;

      //! Read best possible quadrature from library
      virtual bool ReadBestDataFile
         (  bool = false
         )  = 0;

      //! Create default (stupid) guess
      virtual bool DefaultGuess
         (
         )  = 0;

      //! Fit weights to points
      virtual LaplaceQuadratureBase* RefitWeights
         (  const std::vector<T>& points
         ,  bool& singular = DefaultParameterProxy<bool>(false).GetRef()
         )  = 0;

      //! Optimize quadrature
      LaplaceQuadratureBase* Optimize
         (
         );


   protected:
      //! Construct points and weights as determined by LaplaceInfo after setting Xmin and Xmax.
      void ConstructLaplaceQuadrature
         (  const midas::tensor::LaplaceInfo& info
         );

      //! Calculate error for current points and weights.
      T LeastSquaresError
         (  T thresh = static_cast<T>(1.e-20)
         );

      //! Create starting guess for optimization
      LaplaceQuadratureBase* MakeGuess
         (  bool readbest = true
         );

      //! Refit weights for current points
      LaplaceQuadratureBase* RefitWeights
         (
         );

   public:
      //! Types
      enum class quadID : int
      {
         REALLAP = 0
      ,  COMPLEXLAP
      };

      //! Constructor
      LaplaceQuadratureBase
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  T xmin
         ,  T xmax
         ,  EnergyHistogram<T>&& eigen
         );

      //! Virtual destructor
      virtual ~LaplaceQuadratureBase() {};

      //! Type info
      virtual quadID Type() const = 0;

      //! Type string
      virtual std::string ShowType() const = 0;

      //! Make mode-matrix for EnergyDenominatorTensor for electronic-structure calculations
      virtual void MakeMatrix
         (  const std::vector<T>& eigen
         ,  T shift_part
         ,  T* result
         ,  bool real = true
         )  const = 0;
   
      //! Make mode-matrix for EnergyDenominatorTensor for vibrational calculations
      virtual void MakeMatrix
         (  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  In arModeNumber
         ,  unsigned aExtent
         ,  T arDiagEnerPart
         ,  T* result
         ,  bool real = true
         )  const = 0;

      //! Print quadrature info
      virtual void PrintInfo
         (  std::ostream& os
         )  const;

      //! Print (overload)
      void PrintInfo
         (
         )  const;

      //! Make plot
      virtual void MakePlot
         (  const std::string& aName
         ,  T aDelta
         )  const = 0;

      //! Getters
      const std::vector<T>& Points() const   { return this->mPoints; }
      const std::vector<T>& Weights() const  { return this->mWeights; }
      unsigned NumPoints() const { return this->mNumPoints; }
      const EnergyHistogram<T>& EigDistrib() const { return this->mEigen; }
      T Xmin() const { return this->mXmin; }
      T Xmax() const { return this->mXmax; }
      T Norm2() const { return this->mNorm2; }
      bool NegativeInterval() const { return this->mNegativeInterval; }


      //! Factory
      static std::unique_ptr<LaplaceQuadratureBase<T>> Factory
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  const std::pair<T,T>& interval
         ,  EnergyHistogram<T>&& eigen
         );
};


/**
 *
 **/
template
   <  class T
   >
class RealLaplaceQuadrature
   :  public LaplaceQuadratureBase<T>
{
   private:
      /* ============================== STATIC MEMBERS ======================================================= */
      //! Set of available data files for minmax approximation
      static std::set<std::string> mDataFileSet;

      //! Max intervals for given number of points
      static std::map<In, Nb> mMaxIntervals;

      //! Initialize data file set
      static void InitDataFileSet();

      //! Initialize mMaxIntervals
      static void InitMaxIntervals();

      //! Check that a data file exists
      static bool DataFileExists
         (  const std::string&
         );
      /* ===================================================================================================== */

      /* ============================== OPTIMIZATION ========================================================= */
      //! Read in exponents and weights from file.
      bool ReadBestDataFile
         (  bool aAllowMorePoints = false
         )  override;

      //! Make default guess
      bool DefaultGuess
         (
         )  override;

      //! Read hard-coded points from Almlöf paper
      void ReadHardCodedData
         (  unsigned
         );

      //! Get the weights, which are optimal for a given set of exponents/points
      LaplaceQuadratureBase<T>* RefitWeights
         (  const std::vector<T>& points
         ,  bool& singular = DefaultParameterProxy<bool>(false).GetRef()
         )  override;

      //! Calculate the Laplace error function for a given set of points
      T LeastSquaresError
         (  const std::vector<T>& points
         ,  const std::vector<T>& weights
         ,  T thrshld = static_cast<T>(1.e-20)
         )  override;

      //! Perform numerical integration of Laplace error function
      T IntegrateLaplaceErrorFunction
         (  const std::vector<T>& points
         ,  const std::vector<T>& weights
         ,  T thrshld
         ,  T xmin
         ,  T xmax
         )  const;

      /* ===================================================================================================== */

   public:
      /* ============================== CONSTRUCTORS, ASSIGNMENT, ETC. ======================================= */
      //! Constructor
      RealLaplaceQuadrature
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  T xmin
         ,  T xmax
         ,  EnergyHistogram<T>&& eigen
         );
      /* ===================================================================================================== */

      //! Type
      typename LaplaceQuadratureBase<T>::quadID Type() const override;
      std::string ShowType() const override;
      /* ===================================================================================================== */
     
      //! Make mode-matrix for EnergyDenominatorTensor for electronic-structure calculations
      void MakeMatrix
         (  const std::vector<T>& eigen
         ,  T shift_part
         ,  T* result
         ,  bool real = true
         )  const override;
   
      //! Make mode-matrix for EnergyDenominatorTensor for vibrational calculations
      void MakeMatrix
         (  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  In arModeNumber
         ,  unsigned aExtent
         ,  T arDiagEnerPart
         ,  T* result
         ,  bool real = true
         )  const override;

      //! Make data for plot
      void MakePlot
         (  const std::string& aName
         ,  T aDelta = static_cast<T>(1.e-5)
         )  const override;
};




/**
 * Laplace quadrature for approximating the real and imaginary parts of the function \f$ \frac{1}{x-(\omega + i \gamma)} = i \int_0^{\infty} e^{-\gamma s} e^{-i(x-\omega)s} ds \f$ .
 *
 * Re:   \f$ \frac{x-\omega}{(x-\omega)^2 + \gamma^2} \approx \sum_{q=1}^Q w_q e^{-\gamma t_q} \sin{t_q (x-\omega)} \f$
 * Im:   \f$ \frac{\gamma}{(x-\omega)^2 + \gamma^2} \approx \sum_{q=1}^Q w_q e^{-\gamma t_q} \cos{t_q (x-\omega)} \f$
 **/
template
   <  class T
   >
class ComplexLaplaceQuadrature
   :  public LaplaceQuadratureBase<T>
{
   private:
      //! Complex eigenvalue shift
      T mGamma = static_cast<T>(1.e-5);

      //! Length of interval (max abs value of xmin/xmax)
      T mL = static_cast<T>(0.);

      //! Vector of integrals for fitting weights and calculating error
      GeneralMidasVector<T> mB;

      //! Matrix of integrals for fitting weights and calculating error
      GeneralMidasMatrix<T> mA;

      //! Overload of ReadBestDataFile
      bool ReadBestDataFile
         (  bool = true
         )  override;

      //! Make default guess
      bool DefaultGuess
         (
         )  override;

      //! Get the weights, which are optimal for a given set of exponents/points
      LaplaceQuadratureBase<T>* RefitWeights
         (  const std::vector<T>& points
         ,  bool& singular = DefaultParameterProxy<bool>(false).GetRef()
         )  override;

      //! Calculate the Laplace error function for a given set of points
      T LeastSquaresError
         (  const std::vector<T>& points
         ,  const std::vector<T>& weights
         ,  T thrshld = static_cast<T>(1.e-20)
         )  override;

      //! Calculate A matrix
      LaplaceQuadratureBase<T>* CalculateA
         (  const std::vector<T>& points
         );

      //! Calculate B vector
      LaplaceQuadratureBase<T>* CalculateB
         (  const std::vector<T>& points
         );


   public:
      //! Overload constructors
      ComplexLaplaceQuadrature
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  T xmin
         ,  T xmax
         ,  EnergyHistogram<T>&& eigen
         );

      //! Type
      typename LaplaceQuadratureBase<T>::quadID Type() const override;
      std::string ShowType() const override;

      //! Make mode-matrix for EnergyDenominatorTensor for electronic-structure calculations
      void MakeMatrix
         (  const std::vector<T>& eigen
         ,  T shift_part
         ,  T* result
         ,  bool real = true
         )  const override;
   
      //! Make mode-matrix for EnergyDenominatorTensor for vibrational calculations
      void MakeMatrix
         (  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  In arModeNumber
         ,  unsigned aExtent
         ,  T arDiagEnerPart
         ,  T* result
         ,  bool real = true
         )  const override;

      //! Print
      void PrintInfo
         (  std::ostream& os
         )  const override;

      //! Make data for plot
      void MakePlot
         (  const std::string& aName
         ,  T aDelta = static_cast<T>(1.e-5)
         )  const override;

      //! Get gamma
      T Gamma() const { return this->mGamma; }
};





/**
 * Wrapper class for different laplace quadratures
 **/
template
   <  class T
   >
class LaplaceQuadrature
{
   private:
      //! Pointer to concrete laplace quadrature
      std::unique_ptr<LaplaceQuadratureBase<T>> mLapQuad = nullptr;

      //! Determine interval from orbital energies and shift
      std::pair<T,T> CalculateInterval
         (  unsigned order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  T shift = static_cast<T>(0.)
         )  const;

      //! Determine interval from modal energies and shift
      std::pair<T,T> CalculateInterval
         (  const std::vector<In>& modes
         ,  const std::vector<In>& nmodals
         ,  const DataCont& eigvals
         ,  const std::vector<In>& offsets
         ,  T shift = static_cast<T>(0.)
         )  const;

      //! Determine interval from orbital energies and shift
      bool CheckQuadratureInput
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  const std::pair<T,T>& interval
         ,  unsigned order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  T shift = static_cast<T>(0.)
         )  const;

      //! Determine interval from modal energies and shift
      bool CheckQuadratureInput
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  const std::pair<T,T>& interval
         ,  const std::vector<In>& modes
         ,  const std::vector<In>& nmodals
         ,  const DataCont& eigvals
         ,  const std::vector<In>& offsets
         ,  T shift = static_cast<T>(0.)
         )  const;

   public:
      //! Default constructor (makes nullptr)
      LaplaceQuadrature() = default;

      //! Constructor that determines interval and calls factory
      template
         <  class... Us
         >
      LaplaceQuadrature
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  Us&&... energies
         );

      //! Constructor from info and interval
      LaplaceQuadrature
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  T xmin
         ,  T xmax
         );

      //! Make mode-matrix for EnergyDenominatorTensor for electronic-structure calculations
      template
         <  class... Us
         >
      void MakeMatrix
         (  Us&&...
         )  const;

      //! Check if the quadrature has been initialized
      bool IsNullPtr
         (
         )  const;

      //! Create data for plot
      void MakePlot
         (  const std::string&
         ,  T aDelta = static_cast<T>(1.e-5)
         )  const;
   
      //! Output
      friend std::ostream& operator<< <>
         (  std::ostream&
         ,  const LaplaceQuadrature<T>&
         );

      //! Getters
      unsigned NumPoints() const;
      const std::vector<T>& Points() const;
      const std::vector<T>& Weights() const;
      bool NegativeInterval() const;
};




/**
 * Wrapper for downhill_simplex algorithm
 **/
template
   <  class T
   >
class LaplaceErrorFunction
{
   private:
      LaplaceQuadratureBase<T>* mLapQuad = nullptr;

   public:
      using val_t = T;

      LaplaceErrorFunction(LaplaceQuadratureBase<T>* lap_quad) : mLapQuad(lap_quad) {};

      val_t operator()
         (  const std::vector<T>& point
         ,  T thresh
         ,  bool& fail
         )
      {
         mLapQuad->RefitWeights(point, fail);
         return mLapQuad->LeastSquaresError(point, mLapQuad->Weights(), thresh);
      }
};

#endif /* LAPLACEQUADRATURE_DECL_H_INCLUDED */
