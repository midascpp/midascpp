#ifndef DOTEVALUATOR_HELPERS_H_INCLUDED
#define DOTEVALUATOR_HELPERS_H_INCLUDED

#include <map>
#include "lapack_interface/math_wrappers.h"
#include "util/type_traits/Complex.h"

/*!
 *
 */
template<class T>
void contract_index_modematrices
   ( T* left_matrix
   , unsigned left_rank
   , T* right_matrix
   , unsigned right_rank
   , unsigned idx_size
   , T* result
   )
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;

   char nt = is_complex ? 'C' : 'T';
   char nn = 'N';
   int m = static_cast<int>(left_rank);
   int n = static_cast<int>(right_rank);
   int k = static_cast<int>(idx_size);
   auto one  = static_cast<T>(1.0);
   auto zero = static_cast<T>(0.0);

   midas::lapack_interface::gemm( &nt, &nn
                                , &m, &n, &k
                                , &one
                                , left_matrix, &k
                                , right_matrix, &k
                                , &zero
                                , result, &m);
}

/*!
 *
 */
class IndexCreator
{
   private:
      int mIndex = 0;
      std::map<int, int> mIndexMapLeft;
      std::map<int, int> mIndexMapRight;
   public:
      IndexCreator() = default;

      int GetLeftIndex(int aLeft)
      {
         const auto& p = mIndexMapLeft.insert({aLeft, mIndex});
         if(p.second)
            ++mIndex;
         return p.first->second;
      }
      
      int GetRightIndex(int aLeft)
      {
         const auto& p = mIndexMapRight.insert({aLeft, mIndex});
         if(p.second)
            ++mIndex;
         return p.first->second;
      }
};

#endif /* DOTEVALUATOR_HELPERS_H_INCLUDED */
