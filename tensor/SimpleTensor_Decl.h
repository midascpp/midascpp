/**
 ************************************************************************
 * 
 * @file                SimpleTensor.h
 *
 * Created:             02.10.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               SimpleTensor: Full representation of tensors.
 * 
 * Last modified: Wed Oct 22, 2014  18:44
 * 
 * Detailed Description: The ContractDown, ContractForward and ContractUp
 *                       member functions are to supersede
 *                       Integrals::Contract
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

// std headers
#include<iostream>
#include<iomanip>
#include<vector>
#include<cmath>
#include<memory>

#include"BaseTensor.h"
#include"ArrayHopper.h"

template <class T>
SimpleTensor<T> binary_operator(const SimpleTensor<T>&, const SimpleTensor<T>&,
                                T(const T&, const T&));

template <class T>
BaseTensorPtr<T> random_simple_tensor(const std::vector<unsigned int>&);

/************ CLASS SIMPLETENSOR ***************/
/// Simple representation of full tensors.
template
   <  class T
   >
class SimpleTensor
   :  public BaseTensor<T>
{
   friend DotEvaluator<T>;

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;
      using data_t = std::unique_ptr<T[]>;
      using rawptr_t = T*;

   private:
      data_t mData;  ///< Pointer to the data.

      //! Allocate data
      void Allocate
         (
         );

   public:
      using real_t = typename BaseTensor<T>::real_t;

      /// See BaseTensor<T>::Type().
      typename BaseTensor<T>::typeID Type() const override {return BaseTensor<T>::typeID::SIMPLE;};
      std::string ShowType() const override {return "SimpleTensor";};

   private:
      /// See BaseTensor<T>::Contract_impl() for a description of the interface.
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const BaseTensor<T>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  bool = false
         ,  bool = false
         )  const override;

      /// See BaseTensor<T>::ContractInternal_impl().
      BaseTensor<T>* ContractInternal_impl
         (  const std::vector<std::pair<unsigned int, unsigned int>>&
         ,  const std::vector<unsigned int>&
         )  const override;

      /// See BaseTensor<T>::Reorder_impl().
      SimpleTensor* Reorder_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         )  const override;

      /// See BaseTensor<T>::Slice_impl().
      SimpleTensor* Slice_impl
         (  const std::vector<std::pair<unsigned int,unsigned int>>&
         )  const override;
   public:
      friend class CanonicalTensor<T>;

//*============== CONSTRUCTORS, DESTRUCTOR, ASSIGNMENTS ==============*/

      SimpleTensor();
      SimpleTensor(std::vector<unsigned int>&&, const T aValue = static_cast<T>(0.0));
      SimpleTensor(const std::vector<unsigned int>&, const T aValue = static_cast<T>(0.0));
      SimpleTensor(const std::vector<unsigned int>&, rawptr_t);
      SimpleTensor(const std::vector<unsigned int>&, const T*, const size_t);
      SimpleTensor(const CanonicalTensor<T>&);
      SimpleTensor(std::istream&);

      // Unit SimpleTensor constructor (1 at arIndex, 0 otherwise).
      SimpleTensor(const std::vector<unsigned int>& arDims, const std::vector<unsigned int>& arIndex);

      SimpleTensor(const SimpleTensor&);
      SimpleTensor(SimpleTensor&&);

      virtual ~SimpleTensor() = default;

      SimpleTensor* Clone() const override;

      bool Assign
         (  BaseTensor<T> const* const
         )  override;

      SimpleTensor& operator=(const SimpleTensor&);
      SimpleTensor& operator=(SimpleTensor&&);

      void ConjugateImpl() override;


/*========== MEMBER ACCESS ==========*/
      rawptr_t       GetData   ();
      const rawptr_t GetData   () const;
      rawptr_t       ReleaseData();
      ArrayHopper<T> Hopper1D  (const unsigned int idim, T* start);
      ArrayHopper<T> Hopper1D  (const unsigned int idim);

      void SwapData
         (  data_t&
         );

      //! Get value of element
      T Element
         (  const std::vector<unsigned int>&
         )  const override;

/*============== IO ==============*/
      virtual std::string Prettify() const override;
      std::ostream& write_impl(std::ostream& output) const override;
     
/*============== CONTRACTIONS ==============*/
   public:
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const override;
      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override;
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override;

      BaseTensor<T>* CollapseDimensions(const std::vector<unsigned int>& dims) const;

/*============== OTHER OPERATIONS ==============*/
      SimpleTensor* operator*=(const T) override;

      SimpleTensor* operator*=(const BaseTensor<T>&) override;
      SimpleTensor* operator/=(const BaseTensor<T>&) override;

      SimpleTensor* operator+=(const BaseTensor<T>&) override;

      bool SanityCheck() const override;

      void Axpy(const BaseTensor<T>&, const T&) override;
      T Sum() const;
      void Zero() override;
      void Scale(const T&) override;
      void Abs() override;
      void ElementwiseScalarAddition(const T&) override;
      void ElementwiseScalarSubtraction(const T&) override;

      CanonicalTensor<T> CanonicalSVDGuess(const unsigned int) const;

      void DumpInto(T*) const override;

      friend BaseTensorPtr<T> random_simple_tensor<>(const std::vector<unsigned int>&);

   /*============== DECOMPOSITIONS ==============*/
      CanonicalTensor<T> DecomposeALS(int, const int, const T);
      
   /*============== DOT  ==============*/
      // interface
      dot_t Dot(const BaseTensor<T>* const) const override;

      // dispatches
      dot_t DotDispatch(const SimpleTensor<T>* const) const override;

      //! Norm2
      real_t Norm2
         (
         )  const override;
   
   /*============== MPI  ==============*/
#ifdef VAR_MPI
      void MpiSend(In aRecievingRank) const override;
      void MpiRecv(In aSendingRank) override;
      void MpiBcast(In aRoot) override;
#endif /* VAR_MPI */
};

template <class T>
BaseTensorPtr<T> random_simple_tensor(const std::vector<unsigned int>&);

template <class T>
SimpleTensor<T> identity_tensor(unsigned int dims);

template <class T>
SimpleTensor<T> diagonal_tensor(const std::vector<T>& diag);
