/**
************************************************************************
* 
* @file    CpIdDriver.cc
*
* @date    27-02-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of CP-ID driver.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/CpIdDriver.h"
#include "tensor/FitID.h"

#include "util/CallStatisticsHandler.h"

namespace midas::tensor
{

//! Register CpIdDriver
detail::TensorDecompDriverRegistration<CpIdDriver> registerCpIdDriver("CPID");

/**
 * Check decomposition
 *
 * @param aTensor
 *    The tensor to decompose
 *
 * @return
 *    Do decomposition. True or false.
 **/
bool CpIdDriver::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool order_ok  =  mDecompToOrder > 0 
                  ?  (In(aTensor.NDim()) >= mDecompToOrder)
                  :  true;

   auto type = aTensor.Type();

   bool type_ok   =  (  type == BaseTensor<Nb>::typeID::CANONICAL
                     || type == BaseTensor<Nb>::typeID::SUM
                     || type == BaseTensor<Nb>::typeID::DIRPROD // Only test impl!
                     );

   return order_ok && type_ok;
}

/**
 * Get decomp threshold
 *
 * @return
 *    The threshold
 **/
Nb CpIdDriver::GetThreshold
   (
   )  const
{
   return mCpIdResThreshold;
}

/** 
 * Type
 *
 * @return
 *    Name of type
 **/
std::string CpIdDriver::TypeImpl
   (
   )  const
{
   return std::string("CpId");
}

/**
 * Decompose a tensor.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> CpIdDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   LOGCALL("begin");

   // Declarations
   const auto norm2 = aNorm2;
   const auto norm  = std::sqrt(norm2);
   const auto& dims = aTensor.GetDims();
   const auto ndim = aTensor.NDim();

   // Get target rank
   auto target_rank = this->TargetRank(aTensor);

   auto thresh    =  aThresh > C_0
                  ?  aThresh
                  :  this->mCpIdResThreshold;

   // Decompose low-order tensors
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()
      || (  this->mEffectiveLowOrderSvd
         && std::count_if(dims.begin(), dims.end(), [](unsigned i){ return (i != 1); }) == 2
         )
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("CpIdDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Output
   if (  this->mIoLevel >= 3  )
   {
      Mout  << "##### Starting CpIdDriver #####" << "\n"
            << " Target type  :  " << aTensor.ShowType() << "\n"
            << " Dims         :  " << aTensor.ShowDims() << "\n"
            << " Thresh       :  " << thresh << "\n"
            << " Norm2        :  " << aNorm2 << "\n"
            << " Norm         :  " << norm << "\n"
            << " Target rank  :  " << target_rank << "\n"
            << std::endl;
   }

   // Theoretical max rank
   In theoretical_max_rank = aTensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
   In rank_bound = std::min<In>(theoretical_max_rank, target_rank);

   // Construct mode matrices of sum or dirprod
   NiceTensor<Nb> constructed_target;
   if (  aTensor.Type() == BaseTensor<Nb>::typeID::SUM
      )
   {
      LOGCALL("cp from sum");
      constructed_target = NiceTensor<Nb>(static_cast<TensorSum<Nb>*>(aTensor.GetTensor())->ConstructModeMatrices());
   }
   else if  (  aTensor.Type() == BaseTensor<Nb>::typeID::DIRPROD
            )
   {
      LOGCALL("cp from dirprod");
      constructed_target = NiceTensor<Nb>(static_cast<TensorDirectProduct<Nb>*>(aTensor.GetTensor())->ConstructModeMatrices());
   }

   // Set the target tensor to either the argument tensor or the constructed CP tensor
   const auto& target_tensor  =  constructed_target.IsNullPtr()
                              ?  aTensor
                              :  constructed_target;

   // Make input and do CP-ID. Increment maxrank if no good fit is obtained.
   NiceTensor<Nb> fit_tensor;
   CpIdInput<Nb> cpid_input(this->mInfo);
   auto maxrank = std::min<In>(cpid_input.mMaxRank, rank_bound);
   Nb error = norm;
   In rank = I_0;

   In iter = I_0;
   In k = maxrank;
   while (  true
         )
   {
      LOGCALL("maxrank loop");
      cpid_input.mMaxRank = std::min<In>(k, rank_bound);
      cpid_input.mAddRank = std::min<In>(cpid_input.mAddRank, target_rank - cpid_input.mMaxRank);

      if (  this->mIoLevel >= I_3
         )
      {
         Mout  << " === Trying maxrank: " << cpid_input.mMaxRank << " ===\n"
               << "     AddRank:       " << cpid_input.mAddRank << "\n"
               << std::flush;
      }

      {
      LOGCALL("fitid");
      FitCPID(target_tensor, fit_tensor, cpid_input);
      }

      rank = fit_tensor.StaticCast<CanonicalTensor<Nb>>().GetRank();

      // Return target if we are fitting to target_rank anyway
      if (  target_rank == rank
         )
      {
         return target_tensor;
      }

      // If we have a fitter, do fitting on the CP-ID result and get error
      if (  this->mCpFitter
         )
      {
         LOGCALL("fitter");
         auto fit_report = this->mCpFitter->Decompose(target_tensor, aNorm2, fit_tensor, thresh);

         error = fit_report.error;
      }
      // Otherwise, calculate error
      else
      {
         LOGCALL("err calc");
         auto fit_norm2 = fit_tensor.Norm2();
         error = diff_norm_new(fit_tensor.GetTensor(), fit_norm2, target_tensor.GetTensor(), norm2);
      }

      if (  this->mIoLevel >= I_3
         )
      {
         Mout  << "     Output rank:   " << rank << "\n"
               << "     Error:         " << error << "\n\n"
               << std::flush;
      }

      // Check convergence
      if (  rank < cpid_input.mMaxRank  // if effective rank is smaller than max
         || libmda::numeric::float_lt(error, this->mCpIdResThreshold)   // if error is small enough
         || iter >= this->mCpIdMaxRankIncr   // if we have reached max iters
         || k >= rank_bound   // if k is larger than the rank bound
         )
      {
         break;
      }
      else
      {
         // Increment iter and k
         ++iter;
         k += maxrank;
      }
   }


   // Check fit
   if (  libmda::numeric::float_gt(error, this->mCpIdResThreshold)
      )
   {
      Mout  << "Bad CpId fit of " << ndim << ".-order "+aTensor.ShowType()+" to rank " << rank << ":\n"
            << "\tDims:          " << aTensor.ShowDims() << "\n"
            << "\tThresh:        " << thresh << "\n"
            << "\tRel. thresh:   " << thresh / norm << "\n"
            << "\tError:         " << error << "\n"
            << "\tNorm:          " << norm << "\n"
            << "\tTarget rank:   " << target_rank << "\n"
            << std::endl;;
   }

   return fit_tensor;
}

/**
 * Decompose a tensor using an argument guess.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aGuess
 *    The guess tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> CpIdDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   MidasWarning("CP-ID driver does not use the argument guess tensor!");

   return this->DecomposeImpl(aTensor, aNorm2, aThresh);
}

/**
 * Check if tensor should be screened (fitted to rank-0)
 *
 * @param aTensor       The target tensor
 * @param aNorm2        Norm2 of target
 * @param aThresh       Decomposition threshold
 * @return
 *    True if tensor should be screened
 **/
bool CpIdDriver::ScreenImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   if (  libmda::numeric::float_neg(aNorm2)
      )
   {
      return false;
   }
   else
   {
      Nb thresh   =  aThresh > C_0
                  ?  aThresh
                  :  this->mCpIdResThreshold;
   
      // Check norm
      Nb norm = std::sqrt(aNorm2);
      if (  norm < thresh
         )
      {
         return true;
      }
   
      return false;
   }
}

/**
 * c-tor from TensorDecompInfo
 *
 * @param aInfo
 *    TensorDecompInfo
 **/
CpIdDriver::CpIdDriver
   (  const TensorDecompInfo& aInfo
   )
   :  TensorDecompDriverBase(aInfo)
   ,  mCpIdResThreshold
         (  const_cast<TensorDecompInfo&>(aInfo)["CPIDRESTHRESHOLD"].template get<Nb>()
         )
   ,  mCpIdMaxRankIncr
         (  const_cast<TensorDecompInfo&>(aInfo)["CPIDMAXRANKINCR"].template get<In>()
         )
{
}

} /* namespace midas::tensor */
