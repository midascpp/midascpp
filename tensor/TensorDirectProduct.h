#ifndef TENSORDIRECTPRODUCT_H_INCLUDED
#define TENSORDIRECTPRODUCT_H_INCLUDED

// Include declarations
#include "TensorDirectProduct_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include definitions
#include "TensorDirectProduct_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* TENSORDIRECTPRODUCT_H_INCLUDED */
