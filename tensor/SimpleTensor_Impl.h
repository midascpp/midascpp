#include <typeinfo>
#include <algorithm>
#include <utility>
#include <stack>

#include "CanonicalTensor.h"
#include "Scalar.h"
#include "ZeroTensor.h"
#include "DiagonalArrayHopper.h"
#include "lapack_interface/math_wrappers.h"

#include "util/read_write_binary.h"
#include "util/Math.h"
#include "util/SanityCheck.h"
#include "util/AbsVal.h"
#include "util/RandomNumberGenerator.h"
#include "util/type_traits/Complex.h"
#include "libmda/util/stacktrace.h"

#include "mpi/Impi.h"

/**
 * Default constructor (empty dimensions, i.e. a scalar with one element)
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (
   )
   :  BaseTensor<T>
         (
         )
   ,  mData
         (  nullptr
         )
{
   this->Allocate();
}

/** 
 * Allocate new constructor, set to aValue (default=0).
 *
 * @param aDims   Dimensions of the tensor.
 * @param aValue  The value with which initialize.
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const std::vector<unsigned int>& aDims
   ,  const T aValue
   )
   :  BaseTensor<T>
         (  aDims
         )
{
   this->Allocate();
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] = aValue; //defaults to 0
   }
}

/** 
 * Allocate new constructor, set to aValue (default=0).
 *
 * @param aDims   Dimensions of the tensor.
 * @param aValue  The value with which initialize.
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  std::vector<unsigned int>&& aDims
   ,  const T aValue
   )
   :  BaseTensor<T>
         (  std::move(aDims)
         )
{
   this->Allocate();
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] = aValue; //defaults to 0
   }
}

/** 
 * Use pre-allocated chunk of memory for tensor.
 *
 * @warning USE CAREFULLY.
 * @param aDims Dimensions of the tensor.
 * @param data  Data (this tensor takes ownership)
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const std::vector<unsigned int>& aDims
   ,  rawptr_t data
   )
   :  BaseTensor<T>
         (  aDims
         )
   ,  mData
         (  data
         )
{
}

/**
 *  Construct a matrix (second order tensor), by copying from pre-existing tensor.
 *  Supports extracting a contiguous slice of the original matrix.
 *  Example: given a 5x7 matrix A, we want to extract the chunk A(2:4, 3:5)
 *
 *                    %......
 *                  A ..@oo..    @ represents data, the first element we want to include.
 *      aDims[0]=3  | ..ooo..    If the pointer to the original tensor is ptr (represented
 *                  V ..ooo..    by %) then in this case data=ptr+9
 *                    .......
 *         aDims[1]=3   <->      The rest of the tensor elements are represented by "."
 *           stride=7 <----->
 *
 *  @param aDims   Dimensions of the slice (i.e. the new tensor).
 *  @param data    Location of the first element of the pre-existing tensor which
 *                 we want to include into the new tensor.
 *  @param stride  Total length of the original tensor                        */
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const std::vector<unsigned int>& aDims
   ,  const T* data
   ,  const size_t stride
   )
   :  BaseTensor<T>
         (  aDims
         )
{
   if(aDims.size()!=2)
   {
      throw std::runtime_error("ERROR: This constructor is only valid for second-order tensors (matrices)");
   }
   size_t sz = this->TotalSize();
   this->Allocate();
   size_t i_out=0;
   size_t i_in=0;
   while(i_out < sz)
   {
      for(size_t j = 0; j < this->mDims[0]; ++j)
      {
         mData[i_out++] = data[i_in++];
      }
      i_in += stride - this->mDims[0];
   }
}

/**
 * Read from istream
 *
 * @param input   Input stream
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  std::istream& input
   )
   :  BaseTensor<T>
         (
         )
{
   this->ReadDims(input);

   auto size = this->TotalSize();
   this->Allocate();
   read_binary_array( mData.get(), size, input );
}

/**
 * Unit SimpleTensor constructor (1 at arIndex, 0 otherwise).
 * 
 * @param arDims
 * @param arIndex
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const std::vector<unsigned int>& arDims
   ,  const std::vector<unsigned int>& arIndex
   )
   :  SimpleTensor<T>
         (  arDims
         ,  static_cast<T>(C_0)
         )
{
   // Dimensions have been set and all values initialized to zero.
   // Now assert that index matches dimensions.
   // Also deduce the position in mData, based on the index.
   MidasAssert(arDims.size() == arIndex.size(), "Unequal numbers of dimensions and indices.");
   std::size_t pos = I_0;
   std::size_t sub_size = this->TotalSize();
   for(In i = I_0; i < arDims.size(); ++i)
   {
      // Assertion.
      MidasAssert((I_0 <= arIndex[i]) && (arIndex[i] < arDims[i]), "Index doesn't match dimensions.");

      // And contribution from index to position.
      sub_size /= arDims[i];
      pos      += arIndex[i]*sub_size;
   }
   
   // With the position deduced, set the value of the element.
   mData[pos] = C_1;
}

/**
 * Create full tensor from decomposed tensor
 *
 * @param other      The CP tensor to dump to SimpleTensor format
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const CanonicalTensor<T>& other
   )
   :  SimpleTensor<T>
         (  other.GetDims()
         )
{
   other.DumpInto(this->GetData());
}


/**
 * Copy constructor
 *
 * @param other
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  const SimpleTensor<T>& other
   )
   :  SimpleTensor<T>
         (  other.mDims
         )
{
   auto* ptr_source = other.mData.get();
   auto* ptr_target =       mData.get();
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      *(ptr_target++)=*(ptr_source++);
   }
}
/**
 * Move constructor
 *
 * @param other
 **/
template
   <  class T
   >
SimpleTensor<T>::SimpleTensor
   (  SimpleTensor<T>&& other
   )
   :  BaseTensor<T>
         (  std::move(other.mDims)
         )
   ,  mData
         (  std::move(other.mData)
         )
{
}

/**
 * Allocate data
 **/
template
   <  class T
   >
void SimpleTensor<T>::Allocate
   (
   )
{
   auto size = this->TotalSize();
   mData = std::make_unique<T[]>(size);
//   auto alloc_tp = this->GetAllocatorTp();
//   mData = alloc_tp.AllocateUniqueArray(size);
}


/**
 * clone
 *
 * @return
 *    Copy of this
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::Clone
   (
   )  const
{
   return new SimpleTensor<T>(*this);
}

/**
 * Assign data from other tensor if size matches
 *
 * @param aOther     The tensor to copy data from
 * @return
 *    true if size matches and the data has been assigned
 **/
template
   <  class T
   >
bool SimpleTensor<T>::Assign
   (  BaseTensor<T> const * const aOther
   )
{
   if (  aOther->Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      MIDASERROR("Tensor types do not match!");
   }

   // check that other is simple
   auto ptr = static_cast<SimpleTensor<T> const * const>(aOther);
   if(!ptr)
   {
      return false;
   }
   
   // check sizes match
   auto this_size = this->TotalSize();
   auto other_size = ptr->TotalSize();
   if(this_size != other_size)
   {
      return false;
   }
   
   // if size is the same we copy dimensions and elements
   this->GetDims() = ptr->GetDims();
   for(int i = 0; i < this_size; ++i)
   {
      this->mData[i] = ptr->mData[i];
   }

   return true;
}

/**
 * Copy assignment
 *
 * @param other
 * @return
 *    Reference to this
 **/
template
   <  class T
   >
SimpleTensor<T>& SimpleTensor<T>::operator=
   (  const SimpleTensor<T>& other
   )
{
   if(this != &other) // check for self assignment
   {
      // if dimensions differ we need to update mDims and possibly reallocate memory
      if(this->GetDims() != other.GetDims()) // check dims 
      {
         // do base copy to copy dimensions
         BaseTensor<T>::operator=(static_cast<const BaseTensor<T>&>(other));
         
         // then reallocate new data pointer with new size if the number of elements is different
         if (  this->TotalSize() != other.TotalSize()
            )
         {
            this->Allocate();
         }
      }
      
      // copy all elements
      auto size = this->TotalSize();
      for(decltype(size) i = 0; i < size; ++i)
      {
         mData[i] = other.mData[i];
      }
   }
   return *this;
}

/**
 * Move assignment
 *
 * @param other
 * @return
 *    Reference to this
 **/
template
   <  class T
   >
SimpleTensor<T>& SimpleTensor<T>::operator=
   (  SimpleTensor<T>&& other
   )
{
   BaseTensor<T>::operator=(static_cast<BaseTensor<T>&&>(other)); // do base move
   std::swap(this->mData, other.mData); 
   return *this;
}

/**
 * Access tensor data.
 *
 * @return
 *    Raw pointer to data
 **/
template
   <  class T
   >
typename SimpleTensor<T>::rawptr_t SimpleTensor<T>::GetData
   (
   )
{
   return mData.get();
}

/**
 * Access tensor data.
 *
 * @return
 *    Const pointer to data
 **/
template
   <  class T
   >
const typename SimpleTensor<T>::rawptr_t SimpleTensor<T>::GetData
   (
   )  const
{
   return mData.get();
}

/**
 * Release data
 *
 * @warning Use carefully!
 *
 * @return
 *    Pointer to data
 **/
template
   <  typename T
   >
typename SimpleTensor<T>::rawptr_t SimpleTensor<T>::ReleaseData
   (
   )
{
   return mData.release();
}

/**
 * Swap data
 *
 * @warning          Use carefully. Be sure the dimensions match.
 * @param other      Other data
 **/
template
   <  class T
   >
void SimpleTensor<T>::SwapData
   (  data_t& other
   )
{
   this->mData.swap(other);
}

/**
 * Return ArrayHopper to hop over 1 dimension
 *
 * @param dim     Dimension to hop over
 * @param start   Pointer to first element
 * @return
 *    ArrayHopper
 **/
template
   <  class T
   >
ArrayHopper<T> SimpleTensor<T>::Hopper1D
   (  const unsigned int dim
   ,  T* start
   )
{
   return ArrayHopper<T>(start, this->mDims, this->mDims, std::vector<unsigned int>{dim});
}

/**
 * Return ArrayHopper to hop over 1 dimension
 *
 * @param dim     Dimension to hop over
 * @return
 *    ArrayHopper
 **/
template
   <  class T
   >
ArrayHopper<T> SimpleTensor<T>::Hopper1D
   (  const unsigned int dim
   )
{
   return this->Hopper1D(dim, this->mData.get());
}

/**
 * Get value of element
 *
 * @param aIndices      Indices of element
 * @return
 *    Value of element
 **/
template
   <  class T
   >
T SimpleTensor<T>::Element
   (  const std::vector<unsigned int>& aIndices
   )  const
{
   assert( aIndices.size() == this->mDims.size() );

   if (  !mData
      )
   {
      MIDASERROR("Trying to get SimpleTensor element from uninitialized data!");
   }

   if (  aIndices.empty()
      )
   {
      return this->mData[0];
   }
   else
   {
      auto ndim = this->NDim();
      size_t index=aIndices[0];
      for(size_t i=1; i<ndim; ++i)
      {
         index *= this->mDims[i];
         index += aIndices[i];
      }

      return this->mData[index];
   }
}

/**
 * Output dimensions and elements.
 *
 * @return
 *    String for output
 **/
template
   <  class T
   >
std::string SimpleTensor<T>::Prettify
   (
   )  const
{
   std::ostringstream output;

   // Print header (dims)
   unsigned int ndim=this->NDim();
   unsigned int idx=0;
   std::vector<unsigned int> curr(ndim);
   int idim;
   output << this->NDim() << "   [ " ;
   if(this->NDim()>0)
   {
      output << this->mDims[0];
      for (unsigned int i=1;i<this->NDim();i++)
      {
         output << ", " << this->mDims[i];
      }
   }
   output << " ]" << std::endl;

   // Print data
   output << std::fixed << std::setprecision(3);
   bool go_on=true;
   while (  go_on
         )
   {
      go_on=false;
      output << std::setw(22) << std::setprecision(7) << std::scientific << this->mData[idx];
      output << " ("; 
      if(!curr.empty())
      {
         output << std::setw(2) << curr[0];
         for (auto i=curr.begin()+1;i<curr.end();i++)
         {
            output << "," << std::setw(2) << *i;
         }
      }
      output << ")";
      if (  gDebug
         )
      {
         output << ", idx: " << idx;
      }
      for(idim=ndim-1;idim>=0;idim--)
      {
         if(curr[idim]<this->mDims[idim]-1)
         {
            curr[idim]++;
            idx++;
            for( int jdim=idim+1;jdim<ndim;jdim++)
            {
               curr[jdim]=0;
            }
            go_on=true;
            break;
         }
      }
      for(unsigned int i=idim;i<ndim-1;i++)
      {
         output << std::endl;
      }
   }
   return output.str();
}


/**
 * Write data to output stream
 *
 * @param output     reference to ostream
 * @return
 *    ostream
 **/
template
   <  class T
   >
std::ostream& SimpleTensor<T>::write_impl
   (  std::ostream& output
   )  const
{
   return output.write( reinterpret_cast<char*>(mData.get()), this->TotalSize()*sizeof(T) );
}

/**
 * Add tensor to tensor
 *
 * @param other      Tensor to add
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::operator+=
   (  const BaseTensor<T>& other
   )
{
   assert_same_shape(*this, other);
   switch   (  other.Type()
            )
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         const SimpleTensor<T>& other_cast=static_cast<const SimpleTensor<T>&>(other);
         
         auto size = this->TotalSize();
         for(decltype(size) i = 0; i < size; ++i)
         {
            mData[i]+=other_cast.mData[i];
         }
         return this;
         break;
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         return this;
         break;
      }
      case BaseTensor<T>::typeID::VIEW:
      {
         SimpleTensor<T>* handle(static_cast<SimpleTensor<T>*>(other.Clone()));
         *this+=*handle;
         delete handle;
         return this;
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      case BaseTensor<T>::typeID::DIRPROD:
      case BaseTensor<T>::typeID::SUM:
      {
         auto alloc_tp = this->GetAllocatorTp();
         auto size = this->TotalSize();
         auto ptr = alloc_tp.AllocateUniqueArray(size);
         other.DumpInto(ptr.get());

         for(decltype(size) i=0; i<size; ++i)
         {
            this->mData[i] += ptr[i];
         }

         return this;
         break;
      }
      default:
      {
         MIDASERROR("Addition not supported for the input classes: '" + this->ShowType() + "' and '" + other.ShowType() + "'.");
         return this;
      }
   }
}


/**
 * Axpy. In-place add arA times arX.
 *
 * @param arX     Other tensor
 * @param arA     Scalar
 **/
template
   <  class T
   >
void SimpleTensor<T>::Axpy
   (  const BaseTensor<T>& arX
   ,  const T& arA
   )
{
   // First assert that the two tensors have identical shapes.
   assert_same_shape(*this, arX);

   // Then switch according to type of the other tensor.
   // Cases, other is
   //  - SimpleTensor; element-wise y += a*x.
   //  - CanonicalTensor; element-wise y += a*x (converting to SimpleTensor format).
   //  - ZeroTensor; do nothing.
   switch   (  arX.Type()
            )
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         // Get and dyn.cast x-tensor and size.
         const SimpleTensor<T>& x_cast = static_cast<const SimpleTensor<T>&>(arX);
         auto size = this->TotalSize();

         // In SimpleTensor we can simply loop through the elements one by one.
         for(decltype(size) i=I_0; i<size; ++i)
         {
            GetData()[i] += arA * x_cast.GetData()[i];
         }

         // That's it, so now just break out of switch-case.
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // Const-references for the data needed.
         const CanonicalTensor<T>& x_cast        = static_cast<const CanonicalTensor<T>&>(arX);
         const std::size_t y_size                = this->TotalSize();
         const std::vector<unsigned int>& x_dims = x_cast.GetDims();
         const unsigned int x_rank               = x_cast.GetRank();
         T** const x_matrices                    = x_cast.GetModeMatrices();

         // Loop through indices of y by simple incrementation.
         // For each y index, deduct the x index in mode format.
         std::vector<unsigned int> x_index(x_dims.size());
         for(std::size_t y_index = I_0; y_index < y_size; ++y_index)
         {
            // Deduct some indices, through "division-with-remainder".
            // E.g.
            //    y_index = 17
            //    x_dims  = (2 3 4)
            // tmp_y_index = 21
            // tmp_y_size  = 24
            // mode = 0
            //    tmp_y_size    = 12
            //    x_index[mode] = 1  (integer division)
            //    tmp_y_index   = 9
            // mode = 1
            //    tmp_y_size    = 4
            //    x_index[mode] = 2  (integer division)
            //    tmp_y_index   = 1
            // mode = 0
            //    tmp_y_size    = 1
            //    x_index[mode] = 1  (integer division)
            //    tmp_y_index   = 0
            // x_index = (1 2 1)
            std::size_t tmp_y_index = y_index;
            std::size_t tmp_y_size  = y_size;

            for(std::size_t mode = I_0; mode < x_index.size(); ++mode)
            {
               tmp_y_size /= x_dims[mode];
               x_index[mode] = tmp_y_index/tmp_y_size;
               tmp_y_index -= x_index[mode]*tmp_y_size;
            }

            // Add. y += arA*(sum_{r=0}^{rank-1} x^{mode=0}_{r i_0} *...* x^{mode=M-1}_{r i_{M-1}})
            T sum = C_0;
            for(std::size_t rank = I_0; rank < x_rank; ++rank)
            {
               T product = C_1;
               for(std::size_t mode = I_0; mode < x_dims.size(); ++mode)
               {
                  product *= x_matrices[mode][x_dims[mode]*rank + x_index[mode]];
               }
               sum += product;
            }

            // Multiply with the Axpy scalar.
            sum *= arA;

            // Then add to element in y tensor.
            this->GetData()[y_index] += sum;
         }
         break;
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         // Adding a zero-tensor, i.e. do nothing...
         break;
      }
      default:
      {
         MIDASERROR("In SimpleTensor<T>::Axpy: Not impl. for other tensor being of type '"+arX.ShowType()+"'.");
         break;
      }
   }
}

/**
 * Reorder tensor data (in new tensor)
 *
 * @param neworder
 * @param newdims
 *
 * @return
 *    Pointer to result
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::Reorder_impl
   (  const std::vector<unsigned int>& neworder
   ,  const std::vector<unsigned int>& newdims
   )  const
{
   std::vector<unsigned int> oldorder;
   for(int i=this->NDim()-1;i>=0;--i)
   {
      oldorder.push_back(i);
   }

   std::vector<unsigned int> neworder_r(neworder);
   std::reverse(neworder_r.begin(), neworder_r.end());

   SimpleTensor<T>* result(new SimpleTensor<T>(newdims));
   auto hopper_in  =ArrayHopper<T>(this  ->mData.get(), this  ->mDims, this  ->mDims, oldorder);
   auto hopper_out =ArrayHopper<T>(result->mData.get(), result->mDims, result->mDims, neworder_r);

   auto ptr_in =hopper_in .begin();
   auto ptr_out=hopper_out.begin();
   while(ptr_in<hopper_in.end())
   {
      *(ptr_out)=*(ptr_in);
      ++ptr_in;
      ++ptr_out;
   }
   return result;
}

/**
 * Slice tensor
 *
 * @param limits
 * @return
 *    Slice of tensor
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::Slice_impl
   (  const std::vector<std::pair<unsigned int,unsigned int>>& limits
   )  const
{
    // Initialize empty tensor
    std::vector<unsigned int> newdims;
    for(auto lim: limits)
    {
        newdims.push_back(lim.second-lim.first);
    }
    SimpleTensor<T>* result(new SimpleTensor<T>(newdims));

    // Position of the first element
    size_t skip=limits[0].first;
    for(int i=1;i<this->NDim();++i)
    {
        skip*=this->mDims[i];
        skip+=limits[i].first;
    }
    T* initpos = mData.get() + skip;

    // Order of iteration, as usual, from last to first
    std::vector<unsigned int> order;
    for(int i=this->NDim()-1;i>=0;--i)
    {
       order.push_back(i);
    }

    auto hopper_out=ArrayHopper<T>(result->GetData(), newdims, newdims, order);
    auto hopper_in =ArrayHopper<T>(initpos, this->mDims, newdims, order);
    auto p_out=hopper_out.begin();
    auto p_in =hopper_in .begin();
    char dummy;
    while(p_in < hopper_in.end())
    {
        *p_out = *p_in;
        ++p_out;
        ++p_in;
    }

    return result;
}


/**
 *
 **/
template
   <  class T
   >
void SimpleTensor<T>::ConjugateImpl
   (
   )
{
   auto size = this->TotalSize();
   for(int i=0; i<size; ++i)
   {
      this->mData[i] = midas::math::Conj(this->mData[i]);
   }
}

/**
 * Down contract.
 * Contract the tensor along the `idx`-th dimension with vector `vect`.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{jk}=A_{ijk}h_{i}\f$,
 *   - With `idx=1`, return a second-order tensor with elements
 *     \f$B_{ik}=A_{ijk}h_{j}\f$,
 *   - With `idx=2`, return a second-order tensor with elements
 *     \f$B_{ij}=A_{ijk}h_{k}\f$.                                       */
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::ContractDown
   (  const unsigned int idx
   ,  const BaseTensor<T>* vect_in
   )  const
{
   if (  vect_in->Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      MIDASERROR("SimpleTensor::ContractDown: Vector must be a SimpleTensor!");
   }

   // Ensure vect_in is a SimpleTensor instance
   const SimpleTensor<T>& vect = static_cast<const SimpleTensor<T>&>(*vect_in);
   /* Compute output dimensions and initialize output tensor
    * new_dims = [ old_dims[:idx-1],  old_dims[idx+1:] ] */
   std::vector<unsigned int> newdims(this->NDim() - 1);
   unsigned counter = 0;
   for(unsigned i = 0; i < this->NDim(); ++i)
   {
      if(i != idx)
      {
         newdims[counter] = this->mDims[i];
         ++counter;
      }
   }
   SimpleTensor<T>* result = new SimpleTensor<T>(std::move(newdims));

   /* Carry out contraction */
   T *tensin=                   mData.get();
   T *tensout_row_start=result->mData.get();

   auto before = BaseTensor<T>::NBefore(idx);
   auto after = BaseTensor<T>::NAfter(idx);

   for(unsigned int i_bef = 0; i_bef < before; ++i_bef)
   {
      T *op=vect.mData.get();
      T *tensout = nullptr;
      for(unsigned int i=0;i<this->mDims[idx];i++)
      {
         tensout = tensout_row_start;
         for(unsigned int i_aft = 0; i_aft < after; ++i_aft)
         {
            *(tensout++) += *(tensin++) * *op;
         }
         ++op;
      }
      tensout_row_start = tensout;
   }
   
   // if scalar we return as scalar
   if (  result->NDim() == 0
      )
   {
      BaseTensor<T>* actual_result(new Scalar<T>(result->mData[0]));
      delete result;
      return actual_result;
   }
   else
   {
      return result;
   }
}

/** 
 * Forward contract.
 * Contract the tensor along the `idx`-th dimension with the second index of a matrix `matrix`.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{i'jk}=A_{ijk}h_{i'i}\f$,
 *   - With `idx=1`, return a second-order tensor with elements
 *     \f$B_{ij'k}=A_{ijk}h_{j'j}\f$,
 *   - With `idx=2`, return a second-order tensor with elements
 *     \f$B_{ijk'}=A_{ijk}h_{k'k}\f$.                                       
 **/
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::ContractForward
   (  const unsigned int idx
   ,  const BaseTensor<T>* matrix_in
   )  const
{
   if (  matrix_in->Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      MIDASERROR("SimpleTensor::ContractForward: Matrix must be a SimpleTensor!");
   }

   // Ensure vect_in is a SimpleTensor instance
   const SimpleTensor<T>& matrix = static_cast<const SimpleTensor<T>&>(*matrix_in);
   /* Compute output dimensions and initialize output tensor
    * new_dims = [ old_dims[:idx-1], matrix.dims[1], old_dims[idx+1:] ] */
   std::vector<unsigned int> newdims(this->NDim());
   for(unsigned i = 0; i < newdims.size(); ++i)
   {
      if(i == idx)
      {
         newdims[i]= matrix.mDims[0];
      }
      else
      {
         newdims[i] = this->mDims[i];
      }
   }
   SimpleTensor<T>* result = new SimpleTensor<T>(std::move(newdims)); // initialized to 0

   /* Carry out contraction */
   T* tens_out = result->mData.get();
   T* tens_in  = this  ->mData.get();

   auto before = BaseTensor<T>::NBefore(idx);
   auto after  = BaseTensor<T>::NAfter (idx);
   auto out_after = result->NAfter(idx);
   auto a_after = after * this->mDims[idx];
   
   T* tens_out_outer = tens_out;
   T* tens_in_outer = tens_in;
   for(unsigned int i_bef = 0; i_bef < before; i_bef++)
   {
      T* op_row_start = matrix.mData.get();
      for(unsigned int j = 0; j < result->mDims[idx]; j++) // integrals rows
      {
         T* op = op_row_start;
         T* tens_in_inner  = tens_in_outer;
         for(unsigned int i = 0; i < this->mDims[idx]; i++) // integrals cols
         {
            T* tens_out_inner = tens_out_outer;
            //std::cout << " starting loop " << std::endl; 
            for(unsigned int i_aft = 0; i_aft < after; i_aft++)
            {
               //std::cout << " tens_out_inner " << tens_out_inner << "      tens_in_inner = " << *tens_in_inner << "   op = " << *op << std::endl;
               *(tens_out_inner++) += *(tens_in_inner++) * *op;
            }
            ++op;
         }
         op_row_start += matrix.mDims[1];
         tens_out_outer += out_after;
      }
      tens_in_outer += a_after;
   }
   return result;
}

/**
 * Up "contract".
 * Outer product of tensor with `vect`, along the `idx`-th dimension.
 *
 * Example: given a third-order tensor with elements \f$A_{ijk}\f$,
 *   - With `idx=0`, return a second-order tensor with elements
 *     \f$B_{pijk}=A_{ijk}h_{p}\f$,
 *   - With `idx=1`, return a second-order tensor with elements
 *     \f$B_{ipjk}=A_{ijk}h_{p}\f$,
 *   - With `idx=2`, return a second-order tensor with elements
 *     \f$B_{ijpk}=A_{ijk}h_{p}\f$,
 *   - With `idx=3`, return a second-order tensor with elements
 *     \f$B_{ijkp}=A_{ijk}h_{p}\f$.                                         */
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::ContractUp
   (  const unsigned int idx
   ,  const BaseTensor<T>* vect_in
   )  const
{
   if (  vect_in->Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      MIDASERROR("SimpleTensor::ContractUp: Vector must be a SimpleTensor!");
   }

   // Ensure vect_in is a SimpleTensor instance
   const SimpleTensor<T>& vect = static_cast< const SimpleTensor<T>& >(*vect_in);
   /* Compute output dimensions and initialize output tensor
    * new_dims = [ old_dims[:idx] vector.dims[0], old_dims[idx:] ] */
   std::vector<unsigned int> newdims(this->NDim() + 1);
   unsigned counter = 0;
   for(unsigned int i = 0; i < this->NDim(); ++i)
   {
      if(i == idx)
      {
         //newdims.push_back(vect.mDims[0]);
         newdims[counter] = vect.mDims[0];
         ++counter;
      }
      //newdims.push_back(this->mDims[i]);
      newdims[counter] = this->mDims[i];
      ++counter;
   }
   if(idx == this->NDim())
   {
      //newdims.push_back(vect.mDims[0]);
      newdims[counter] = vect.mDims[0];
      ++counter;
   }
   SimpleTensor<T>* result = new SimpleTensor<T>(std::move(newdims));

   /* Carry out contraction */
   T *tensin_row_start = mData.get();
   T *tensin           = nullptr;
   T *tensout          = result->mData.get();

   auto n_bef = BaseTensor<T>::NBefore(idx);
   auto n_result_aft = result->NAfter(idx);
   for(unsigned int i_bef = 0; i_bef < n_bef; ++i_bef)
   {
      T *op = vect.mData.get();
      for(unsigned int i = 0; i < vect.mDims[0]; ++i)
      {
         tensin = tensin_row_start;
         for(unsigned int i_aft = 0; i_aft < n_result_aft; ++i_aft)
         {
            *(tensout++) += *(tensin++) * *op;
         }
         ++op;
      }
      tensin_row_start = tensin;
   }
   return result;
}


/**
 * Perform internal contraction
 *
 * @param inner_indices_pairs
 * @param outer_indices
 *
 * @return
 *    Pointer to result
 **/
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::ContractInternal_impl
   (  const std::vector<std::pair<unsigned int, unsigned int>>& inner_indices_pairs
   ,  const std::vector<unsigned int>& outer_indices
   )  const
{
   std::vector<unsigned int> dims_result;
   for(auto i: outer_indices)
   {
      dims_result.push_back(this->mDims[i]);
   }
   std::vector<unsigned int> result_indices;
   for(int i=outer_indices.size()-1;i>=0;--i)
   {
      result_indices.push_back(i);
   }

   SimpleTensor<T>* result(new SimpleTensor<T>(dims_result));

   std::vector<unsigned int> outer_indices_reverse=outer_indices;
   std::reverse(outer_indices_reverse.begin(), outer_indices_reverse.end());

   ArrayHopper<T> outer_hopper( this->mData.get(), this->mDims, this->mDims, outer_indices_reverse );
   ArrayHopper<T> result_hopper( result->mData.get(), result->mDims, result->mDims, result_indices);

   auto result_iter=result_hopper.begin();
   auto outer_iter=outer_hopper.begin();

   while(outer_iter<outer_hopper.end())
   {
      for(auto inner_iter: DiagonalArrayHopper<T>(
               &*outer_iter,
               this->mDims,
               inner_indices_pairs ))
      {
         *result_iter+=inner_iter;
      }
      ++result_iter;
      ++outer_iter;
   }

   if(dims_result.size()==0)
   {
      Scalar<T>* actual_result=new Scalar<T>(*result->mData.get());
      delete result;
      return actual_result;
   }
   else
   {
      return result;
   }
}

/**
 *  Performance considerations
 *  --------------------------
 *  Tensors elements are iterated over according to the following rules:
 *  - The most external loop iterates over the outer indices of `*this`, in
 *    the order they are stored in memory (that is, rightmost indices first).
 *  - The middle loop iterates over the outer indices of `other`, in
 *    the order they are stored in memory (that is, rightmost indices first).
 *  - The innermost loop iterates over the inner indices of `*this`, in
 *    the order they are stored in memory (that is, rightmost indices first).
 *    The inner indices of `other` are iterated to match 
 *    order for `*this`, that is last indices are iterated first.
 *  - The elements of the resulting tensor are iterated according to the
 *    external and middle loop.
 *
 *  The parameters passed to Contract_impl have been obtained by
 *  contract() to facilitate this particular looping.
 *
 *  Example: consider the contraction
 *  \f[
 *     c_{abcd} = a_{aijb} b_{jdic}
 *  \f]
 *
 *  - Outermost loop: over all outer indices of left (\f$a\f$).
 *  \f$a_{\mathbf{a}ij\mathbf{b}}\f$: \f$a_{0ij0} \rightarrow
 *                                       a_{0ij1} \rightarrow
 *                                       a_{0ij2} \rightarrow
 *                                       \dots    \rightarrow
 *                                       a_{1ij0} \rightarrow
 *                                       a_{1ij1} \rightarrow
 *                                       a_{1ij2} \rightarrow
 *                                                         \dots    \f$
 *  - Middle loop: over all outer indices of right (\f$b\f$).
 *  \f$b_{j\mathbf{d}i\mathbf{c}}\f$: \f$b_{j0i0} \rightarrow
 *                                       b_{j0i1} \rightarrow
 *                                       b_{j0i2} \rightarrow
 *                                       \dots    \rightarrow
 *                                       b_{j1i0} \rightarrow
 *                                       b_{j1i1} \rightarrow
 *                                       b_{j1i2} \rightarrow
 *                                       \dots    \f$
 *  - Innermost loop: over all inner indices of left (\f$a\f$).
 *  \f$a_{a\mathbf{ij}b}\f$: \f$a_{a00b} \rightarrow
 *                              a_{a01b} \rightarrow
 *                              a_{a02b} \rightarrow
 *                              \dots    \rightarrow
 *                              a_{a10b} \rightarrow
 *                              a_{a11b} \rightarrow
 *                              a_{a12b} \rightarrow
 *                                                         \dots    \f$
 *
 *  The iterations are done by ArrayHopper.
 *
 *  Let's have a look an example where the dimension denoted by \f$j\f$ has a
 *  length of 3 and all other dimensions have lengths of 2 (that is, the
 *  \f$\mathbf{a}\f$ tensor is \f$2\times2\times3\times2\f$ and the
 *  \f$\mathbf{b}\f$ tensor is \f$3\times2\times2\times2\f$)
 *  Here is a step-by-step breakdown:
 *
 *  - \f$c_{0000}\f$ += \f$a_{0000} b_{0000}\f$
 *  - \f$c_{0000}\f$ += \f$a_{0010} b_{1000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0000}\f$ += \f$a_{0020} b_{2000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0000}\f$ += \f$a_{0100} b_{0010}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0000}\f$ += \f$a_{0110} b_{1010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0000}\f$ += \f$a_{0120} b_{2010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0010}\f$ += \f$a_{0000} b_{0001}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{0010}\f$ += \f$a_{0010} b_{1001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0010}\f$ += \f$a_{0020} b_{2001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0010}\f$ += \f$a_{0100} b_{0011}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0010}\f$ += \f$a_{0110} b_{1011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0010}\f$ += \f$a_{0120} b_{2011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0001}\f$ += \f$a_{0000} b_{0100}\f$ Increase \f$d\f$ (Middle)
 *  - \f$c_{0001}\f$ += \f$a_{0010} b_{1100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0001}\f$ += \f$a_{0020} b_{2100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0001}\f$ += \f$a_{0100} b_{0110}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0001}\f$ += \f$a_{0110} b_{1110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0001}\f$ += \f$a_{0120} b_{2110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0011}\f$ += \f$a_{0000} b_{0101}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{0011}\f$ += \f$a_{0010} b_{1101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0011}\f$ += \f$a_{0020} b_{2101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0011}\f$ += \f$a_{0100} b_{0111}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0011}\f$ += \f$a_{0110} b_{1111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0011}\f$ += \f$a_{0120} b_{2111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0100}\f$ += \f$a_{0001} b_{0000}\f$ Increase \f$b\f$ (Outer)
 *  - \f$c_{0100}\f$ += \f$a_{0011} b_{1000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0100}\f$ += \f$a_{0021} b_{2000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0100}\f$ += \f$a_{0101} b_{0010}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0100}\f$ += \f$a_{0111} b_{1010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0100}\f$ += \f$a_{0121} b_{2010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0110}\f$ += \f$a_{0001} b_{0001}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{0110}\f$ += \f$a_{0011} b_{1001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0110}\f$ += \f$a_{0021} b_{2001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0110}\f$ += \f$a_{0101} b_{0011}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0110}\f$ += \f$a_{0111} b_{1011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0110}\f$ += \f$a_{0121} b_{2011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0101}\f$ += \f$a_{0001} b_{0100}\f$ Increase \f$d\f$ (Middle)
 *  - \f$c_{0101}\f$ += \f$a_{0011} b_{1100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0101}\f$ += \f$a_{0021} b_{2100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0101}\f$ += \f$a_{0101} b_{0110}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0101}\f$ += \f$a_{0111} b_{1110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0101}\f$ += \f$a_{0121} b_{2110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0111}\f$ += \f$a_{0001} b_{0101}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{0111}\f$ += \f$a_{0011} b_{1101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0111}\f$ += \f$a_{0021} b_{2101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0111}\f$ += \f$a_{0101} b_{0111}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{0111}\f$ += \f$a_{0111} b_{1111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{0111}\f$ += \f$a_{0121} b_{2111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1000}\f$ += \f$a_{1000} b_{0000}\f$ Increase \f$a\f$ (Outer)
 *  - \f$c_{1000}\f$ += \f$a_{1010} b_{1000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1000}\f$ += \f$a_{1020} b_{2000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1000}\f$ += \f$a_{1100} b_{0010}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1000}\f$ += \f$a_{1110} b_{1010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1000}\f$ += \f$a_{1120} b_{2010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1010}\f$ += \f$a_{1000} b_{0001}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{1010}\f$ += \f$a_{1010} b_{1001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1010}\f$ += \f$a_{1020} b_{2001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1010}\f$ += \f$a_{1100} b_{0011}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1010}\f$ += \f$a_{1110} b_{1011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1010}\f$ += \f$a_{1120} b_{2011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1001}\f$ += \f$a_{1000} b_{0100}\f$ Increase \f$d\f$ (Middle)
 *  - \f$c_{1001}\f$ += \f$a_{1010} b_{1100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1001}\f$ += \f$a_{1020} b_{2100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1001}\f$ += \f$a_{1100} b_{0110}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1001}\f$ += \f$a_{1110} b_{1110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1001}\f$ += \f$a_{1120} b_{2110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1011}\f$ += \f$a_{1000} b_{0101}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{1011}\f$ += \f$a_{1010} b_{1101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1011}\f$ += \f$a_{1020} b_{2101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1011}\f$ += \f$a_{1100} b_{0111}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1011}\f$ += \f$a_{1110} b_{1111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1011}\f$ += \f$a_{1120} b_{2111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1100}\f$ += \f$a_{1001} b_{0000}\f$ Increase \f$b\f$ (Outer)
 *  - \f$c_{1100}\f$ += \f$a_{1011} b_{1000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1100}\f$ += \f$a_{1021} b_{2000}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1100}\f$ += \f$a_{1101} b_{0010}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1100}\f$ += \f$a_{1111} b_{1010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1100}\f$ += \f$a_{1121} b_{2010}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1110}\f$ += \f$a_{1001} b_{0001}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{1110}\f$ += \f$a_{1011} b_{1001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1110}\f$ += \f$a_{1021} b_{2001}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1110}\f$ += \f$a_{1101} b_{0011}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1110}\f$ += \f$a_{1111} b_{1011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1110}\f$ += \f$a_{1121} b_{2011}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1101}\f$ += \f$a_{1001} b_{0100}\f$ Increase \f$d\f$ (Middle)
 *  - \f$c_{1101}\f$ += \f$a_{1011} b_{1100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1101}\f$ += \f$a_{1021} b_{2100}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1101}\f$ += \f$a_{1101} b_{0110}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1101}\f$ += \f$a_{1111} b_{1110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1101}\f$ += \f$a_{1121} b_{2110}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1111}\f$ += \f$a_{1001} b_{0101}\f$ Increase \f$c\f$ (Middle)
 *  - \f$c_{1111}\f$ += \f$a_{1011} b_{1101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1111}\f$ += \f$a_{1021} b_{2101}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1111}\f$ += \f$a_{1101} b_{0111}\f$ Increase \f$i\f$ (Inner)
 *  - \f$c_{1111}\f$ += \f$a_{1111} b_{1111}\f$ Increase \f$j\f$ (Inner)
 *  - \f$c_{1111}\f$ += \f$a_{1121} b_{2111}\f$ Increase \f$j\f$ (Inner)
 *
 */
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::Contract_impl
   (  const std::vector<unsigned int>& indices_inner_this
   ,  const std::vector<unsigned int>& indices_outer_this
   ,  const BaseTensor<T>&             other_in
   ,  const std::vector<unsigned int>& indices_inner_other
   ,  const std::vector<unsigned int>& indices_outer_other
   ,  const std::vector<unsigned int>& dims_result
   ,  const std::vector<unsigned int>& indices_left_result
   ,  const std::vector<unsigned int>& indices_right_result
   ,  bool conjugate_left
   ,  bool conjugate_right
   )  const
{
   switch(other_in.Type())
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         const SimpleTensor<T>& other=static_cast<const SimpleTensor<T>&>(other_in);
         // In this example we contract h_abc = f_iakjb g_jkci
         SimpleTensor<T>* result=new SimpleTensor<T>(dims_result);

         auto result_left_hopper =ArrayHopper<T>(result->mData.get(), result->mDims, result->mDims, indices_left_result);
         auto result_left=result_left_hopper.begin();

         /***** ITERATION OVER OUTER (NON-CONTRACTION) INDICES OF THIS *****/
         ArrayHopper<T> outer_this_hopper( this->mData.get(), this->mDims, this->mDims,  indices_outer_this );
         for(auto outer_this=outer_this_hopper.begin(); outer_this<outer_this_hopper.end(); ++outer_this)
         {
            ArrayHopper<T> outer_other_hopper( other.mData.get(), other.mDims, other.mDims,  indices_outer_other );
            auto result_right_hopper=ArrayHopper<T>(result_left, result->mDims, result->mDims, indices_right_result);
            auto result_right=result_right_hopper.begin();
            /***** ITERATION OVER OUTER (NON-CONTRACTION) INDICES OF OTHER *****/
            ArrayHopper<T> outer_other( other.mData.get(), other.mDims, other.mDims,  indices_outer_other );
            for(auto outer_other=outer_other_hopper.begin(); outer_other<outer_other_hopper.end(); ++outer_other)
            {
               /***** SUMMATION OVER CONTRACTION INDICES *****/
               // Set to 0
               (*result_right)=static_cast<T>( 0 );
               // These two iterators over the contraction indices advance exactly in parallel
               auto inner_this_hopper =ArrayHopper<T>(outer_this,  this->mDims, this->mDims, indices_inner_this);
               auto inner_other_hopper=ArrayHopper<T>(outer_other, other.mDims, other.mDims, indices_inner_other);
               auto inner_this  = inner_this_hopper.begin();
               auto inner_other = inner_other_hopper.begin();

               while( inner_this < inner_this_hopper.end() || inner_other < inner_other_hopper.end() )
               {
                  if constexpr   (  midas::type_traits::IsComplexV<T>
                                 )
                  {
                     auto val_this = conjugate_left ? midas::math::Conj(*inner_this) : *inner_this;
                     auto val_other = conjugate_right ? midas::math::Conj(*inner_other) : *inner_other;

                     (*result_right) += val_this * val_other;
                  }
                  else
                  {
                     (*result_right) += (*inner_this) * (*inner_other);
                  }
                  ++inner_other;
                  ++inner_this;
               }
               ++result_right;
            }
            ++result_left;
         }
            
         if(dims_result.size()==0)
         {
            Scalar<T>* actual_result=new Scalar<T>(*result->mData.get());
            delete result;
            return actual_result;
         }
         else
         {
            return result;
         }
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // We call the CanonicalTensor implementation of Contract_impl with
         // swapped arguments
         const CanonicalTensor<T>& other=static_cast<const CanonicalTensor<T>&>(other_in);
         return other.Contract_impl(indices_inner_other,
                                    indices_outer_other,
                                    *this,
                                    indices_inner_this,
                                    indices_outer_this,
                                    dims_result,
                                    indices_right_result,
                                    indices_left_result,
                                    conjugate_right,     // swap
                                    conjugate_left);     // swap
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         // We call the ZeroTensor implementation of Contract_impl with
         // swapped arguments
         const ZeroTensor<T>& other=static_cast<const ZeroTensor<T>&>(other_in);
         return other.Contract_impl(indices_inner_other,
                                    indices_outer_other,
                                    *this,
                                    indices_inner_this,
                                    indices_outer_this,
                                    dims_result,
                                    indices_right_result,
                                    indices_left_result,
                                    conjugate_right,     // swap
                                    conjugate_left);     // swap
      }
      default:
      {
         MIDASERROR("In SimpleTensor<T>::Contract_impl: Not impl. for other tensor being of type '"+other_in.ShowType()+"'.");
      }
   }
   return nullptr;
}

/**
 * Multiply (in-place) all elements with scalar
 *
 * @param k       Scalar
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::operator*=
   (  const T k
   )
{
   T *ptr=mData.get();
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      *(ptr++)*=k;
   }
   return this;
}

/**
 * Multiply (in-place) elementwise with other tensor.
 *
 * @param other      The other tensor
 * @return
 *    This
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::operator*=
   (  const BaseTensor<T>& other
   )
{
   assert_same_shape(*this, other);
   switch   (  other.Type()
            )
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         const SimpleTensor<T>& other_cast=static_cast<const SimpleTensor<T>&>(other);
         
         auto size = this->TotalSize();
         for(decltype(size) i = 0; i < size; ++i)
         {
            mData[i]*=other_cast.mData[i];
         }
         return this;
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         *this *= SimpleTensor<T>(static_cast<const CanonicalTensor<T>&>(other));
         return this;
         break;
      }
      default:
      {
         MIDASERROR("Elementwise multiplication not supported for the input classes");
         return this;
      }
   }
}

/**
 * Divide (in-place) elementwise with other tensor.
 *
 * @param other      The other tensor
 * @return
 *    This
 **/
template
   <  class T
   >
SimpleTensor<T>* SimpleTensor<T>::operator/=
   (  const BaseTensor<T>& other
   )
{
   assert_same_shape(*this, other);
   switch   (  other.Type()
            )
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         const SimpleTensor<T>& other_cast=static_cast<const SimpleTensor<T>&>(other);
         
         auto size = this->TotalSize();
         for(decltype(size) i = 0; i < size; ++i)
         {
            mData[i]/=other_cast.mData[i];
         }
         return this;
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         *this /= SimpleTensor<T>(static_cast<const CanonicalTensor<T>&>(other));
         return this;
         break;
      }
      default:
      {
         MIDASERROR("Elementwise division not supported for the input classes");
         return this;
      }
   }
}

/**
 * Return a new tensor by summing over a list of dimensions
 *
 * @param dims
 * @return
 *    pointer to result
 **/
template
   <  class T
   >
BaseTensor<T>* SimpleTensor<T>::CollapseDimensions
   (  const std::vector<unsigned int>& dims
   )  const
{
   std::vector<unsigned int> surviving_dims;
   for(unsigned int dim=this->NDim();dim!=0;--dim)
   {
      unsigned int actual_dim=dim-1;
      if(std::none_of(dims.begin(), dims.end(),
                      [actual_dim](const unsigned int& i){return i==actual_dim;}))
      {
         surviving_dims.push_back(actual_dim);
      }
   }

   std::vector<unsigned int> result_dims{};
   for(auto dim=surviving_dims.rbegin();dim!=surviving_dims.rend();++dim)
   {
      result_dims.push_back(this->mDims[*dim]);
   }

   SimpleTensor<T>* result(new SimpleTensor<T>(result_dims));

   auto out_ptr=result->mData.get();
   ArrayHopper<T> outer_hopper(this->mData.get(), this->mDims, this->mDims, surviving_dims);
   for (auto outer=outer_hopper.begin(); outer!=outer_hopper.end(); ++outer)
   {
      ArrayHopper<T> inner_hopper(outer, this->mDims, this->mDims, dims);
      for (auto elem=inner_hopper.begin(); elem!=inner_hopper.end(); ++elem)
      {
         *out_ptr += *elem;
      }
      ++out_ptr;
   }

   if(result->NDim()==0)
   {
      BaseTensor<T>* actual_result(new Scalar<T>(result->mData[0]));
      delete result;
      return actual_result;
   }
   else
   {
      return result;
   }
}

/**
 * Return the sum of all the elements.
 *
 * @return
 *    Sum of elements
 **/
template
   <  class T
   >
T SimpleTensor<T>::Sum
   (
   )  const
{
   T result=0.;
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      result+=mData[i];
   }
   return result;
}

/**
 * Zero all elements
 **/
template
   <  class T
   >
void SimpleTensor<T>::Zero
   (
   )
{
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] = static_cast<T>(0.0);
   }
}

/**
 * Scale all elements
 *
 * @param aScalar
 **/
template
   <  class T
   >
void SimpleTensor<T>::Scale
   (  const T& aScalar
   )
{
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] *= aScalar;
   }
}

/**
 * Set all elements to their absolute values
 **/
template 
   <  class T
   >
void SimpleTensor<T>::Abs
   (
   )
{
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i<size; ++i)
   {
      mData[i] = std::abs(mData[i]);
   }
}

/**
 * Add scalar to all elements
 *
 * @param aScalar
 **/
template
   <  class T
   >
void SimpleTensor<T>::ElementwiseScalarAddition
   (  const T& aScalar
   )
{
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] += aScalar;
   }
}

/**
 * Subtract scalar from all elements
 *
 * @param aScalar
 **/
template <class T>
void SimpleTensor<T>::ElementwiseScalarSubtraction
   (  const T& aScalar
   )
{
   auto size = this->TotalSize();
   for(decltype(size) i = 0; i < size; ++i)
   {
      mData[i] -= aScalar;
   }
}


/**
 * Create stating guess for CP decomposition by performing truncated SVDs on matrix unfoldings.
 *
 * @param rank
 * @return
 *    CP tensor
 **/
template
   <  class T
   >
CanonicalTensor<T> SimpleTensor<T>::CanonicalSVDGuess
   (  const unsigned int rank
   )  const
{
   CanonicalTensor<T> result(this->mDims, rank);
   for(unsigned int idim=0;idim<this->NDim();++idim)
   {
      std::vector<unsigned int> new_order;
      unsigned int newpos=0;
      for(unsigned int jdim=0;jdim<this->NDim();++jdim)
      {
         if(jdim!=idim)
         {
            new_order.push_back(newpos++);
         }
         else
         {
            new_order.push_back(this->NDim()-1);
         }
      }

      SimpleTensor<T>* reorder = static_cast<SimpleTensor<T>*>(this->Reorder(new_order));
      
      //std::cout <<  " old_order : " << this->mDims << std::endl;
      //std::cout <<  " this    : " << (*this) << std::endl;
      //std::cout <<  " new_order : " << new_order << std::endl;
      //std::cout <<  " REORDER : " << (*reorder) << std::endl;

      char jobu ='O';
      char jobvt='N';
      int m=this->mDims[idim];
      int n=BaseTensor<T>::NBefore(idim)* BaseTensor<T>::NAfter(idim);
      auto s = std::make_unique<real_t[]>(this->mDims[idim]);
      std::unique_ptr<T[]> u = nullptr;
      std::unique_ptr<T[]> vt = nullptr;
      int lwork=10*this->mDims[idim];
      auto alloc = this->GetAllocatorTp();
      auto work = alloc.AllocateUniqueArray(lwork);
      int info;
      midas::lapack_interface::gesvd(&jobu, &jobvt,
                                     &m, &n, reorder->mData.get(), &m,
                                     s.get(), u.get(), &m, vt.get(), &m,
                                     work.get(), &lwork, &info);

      // Copy first this->mRank rows
      T* out=result.mModeMatrices[idim];
      T* in =reorder->mData.get();
      for(size_t i=0;i<result.mDims[idim]*result.mRank;++i)
      {
         *(out++)=*(in++);
      }
      delete reorder;
   }
   return result;
}

/**
 * Copy contents into (previously allocated) memory location.
 *
 * @param target
 **/
template
   <  class T
   >
void SimpleTensor<T>::DumpInto
   (  T* target
   )  const
{
   const T* ptr_to_mData = mData.get();
   auto size = this->TotalSize();
   for (decltype(size) i = 0; i < size; ++i)
   {
      *(target++) = *(ptr_to_mData++);
   }
}

/**
 * Sanity check
 *
 * @return true if tensor is sane
 **/
template
   <  class T
   >
bool SimpleTensor<T>::SanityCheck
   (
   )  const
{
   auto size = this->TotalSize();

   for(size_t i=0; i<size; ++i)
   {
      if (  !midas::util::IsSane(this->mData[i])
         )
      {
         return false;
      }
   }

   return true;
}

/**
 * Generate tensor with random elements.
 *
 * @param dims
 *
 * @return
 *    Pointer to random tensor
 **/
template
   <  class T
   >
BaseTensorPtr<T> random_simple_tensor
   (  const std::vector<unsigned int>& dims
   )
{
   BaseTensorPtr<T> result_ptr(new SimpleTensor<T>(dims));
   SimpleTensor<T>& result = *static_cast<SimpleTensor<T>*>(result_ptr.get());
   auto size = result.TotalSize();
   for (decltype(size) i = 0; i < size; ++i)
   {
      if constexpr   (  !midas::type_traits::IsComplexV<T>
                     )
      {
         result.GetData()[i] = midas::util::RandomSignedFloat<T>();
      }
      else
      {
         using real_t = midas::type_traits::RealTypeT<T>;
         result.GetData()[i] = T(midas::util::RandomSignedFloat<real_t>(), midas::util::RandomSignedFloat<real_t>());
      }
   }
   return result_ptr;
}

/**
 * Generate identity matrix in SimpleTensor format
 *
 * @param dim
 * @return result
 **/
template
   <  class T
   >
SimpleTensor<T> identity_tensor(unsigned int dim)
{
   SimpleTensor<T> result(std::vector<unsigned int>{dim, dim});
   {
      int i0=0;
      for(auto& it0: result.Hopper1D(0))
      {
         int i1=0;
         for(auto& it1: result.Hopper1D(1, &it0))
         {
            it1 = (i0==i1) ? T(1) : T(0);
            ++i1;
         }
         ++i0;
      }
   }
   return result;
}

/**
 * Generate diagonal 2-way tensor from std::vector
 *
 * @param diag    Vector of diagonal elements
 * @return result
 **/
template
   <  class T
   >
SimpleTensor<T> diagonal_tensor
   (  const std::vector<T>& diag
   )
{
   SimpleTensor<T> result(std::vector<unsigned int>{static_cast<unsigned int>(diag.size()), 
                                                    static_cast<unsigned int>(diag.size())});
   int i0=0;
   int count=0;
   for(auto& it0: result.Hopper1D(0))
   {
      int i1=0;
      for(auto& it1: result.Hopper1D(1, &it0))
      {
         if(i0==i1){it1=diag[count];}
         else{it1=0;}
         ++i1;
      }
      ++i0;
      ++count;
   }

   return result;
}

/**
 * Norm2
 *
 * @return
 *    Squared Frobenius norm
 **/
template
   <  typename T
   >
typename SimpleTensor<T>::real_t SimpleTensor<T>::Norm2
   (
   )  const
{
   auto size = this->TotalSize();
   auto* data = this->GetData();

   real_t result(0.);

   for(In i=I_0; i<size; ++i)
   {
      result += midas::util::AbsVal2(*(data++));
   }

   return result;
}

/** 
 * Dot interface
 *
 * @param other
 * @return dot product
 */
template
   <  class T
   >
typename DotEvaluator<T>::dot_t SimpleTensor<T>::Dot
   (  const BaseTensor<T>* const other
   )  const
{
   return midas::math::Conj(other->DotDispatch(this));  
}

/** 
 * @param other
 * @return dot product
 */
template
   <  class T
   >
typename DotEvaluator<T>::dot_t SimpleTensor<T>::DotDispatch
   (  const SimpleTensor<T>* const other
   )  const
{
   return DotEvaluator<T>().Dot(this, other);
}

#ifdef VAR_MPI
/**
 * Overload of MpiSend for SimpleTensor
 *
 * @param aRecievingRank  The rank to send the SimpleTensor to.
 **/
template
   <  class T
   >
void SimpleTensor<T>::MpiSend
   (  In aRecievingRank
   )  const
{
   // send basetensor stuff
   BaseTensor<T>::MpiSendDimensions(aRecievingRank);
   
   // send simpletensor stuff
   void* ptr_data = static_cast<void*>(mData.get());
   int nelem = this->TotalSize();
   
   midas::mpi::detail::WRAP_Send(ptr_data, nelem, midas::mpi::DataTypeTrait<T>::Get(), aRecievingRank, 0, MPI_COMM_WORLD);
}

/**
 * Overload of MpiRecv for SimpleTensor
 *
 * @param aSendingRank  The rank to receive the SimpleTensor from.
 **/
template
   <  class T
   >
void SimpleTensor<T>::MpiRecv
   (  In aSendingRank
   )
{
   // recv basetensor stuff
   BaseTensor<T>::MpiRecvDimensions(aSendingRank);
   
   // recv simpletensor stuff
   MPI_Status status;
   int nelem = this->TotalSize();
   
   this->Allocate();
   void* ptr_data = static_cast<void*>(mData.get());
   midas::mpi::detail::WRAP_Recv(ptr_data, nelem, midas::mpi::DataTypeTrait<T>::Get(), aSendingRank, 0, MPI_COMM_WORLD, &status);
}

/**
 * Bcast SimpleTensor
 *
 * @param aRoot      Root rank to send from
 **/
template
   <  class T
   >
void SimpleTensor<T>::MpiBcast
   (  In aRoot
   )
{
   if constexpr   (  MPI_DEBUG
                  )
   {
      std::ostringstream oss;
      oss   << " SimpleTensor Bcast. Dimensions before: " << this->GetDims();
      midas::mpi::WriteToLog(oss.str());
   }
   int nelem_old = this->TotalSize();

   // Bcast dimensions
   BaseTensor<T>::MpiBcastDimensions(aRoot);
   int nelem = this->TotalSize();

   if constexpr   (  MPI_DEBUG
                  )
   {
      std::ostringstream oss;
      oss   << " SimpleTensor Bcast. Dimensions after: " << this->GetDims();
      midas::mpi::WriteToLog(oss.str());
   }

   // Re-allocate data if we have a different number of elements or mData is null (note that TotalSize returns 1 if mDims is empty!)
   if (  nelem_old != nelem
      || !mData
      )
   {
      if (  midas::mpi::GlobalRank() == aRoot
         )
      {
         MIDASERROR("MPI reallocation in root! This should not happen!");
      }
      this->Allocate();
   }

   if (  !mData
      )
   {
      MIDASERROR("Trying to MPI_Bcast nullptr data in SimpleTensor. nelem = " + std::to_string(nelem));
   }

   // Bcast data
   void* ptr_data = static_cast<void*>(mData.get());
   midas::mpi::detail::WRAP_Bcast(ptr_data, nelem, midas::mpi::DataTypeTrait<T>::Get(), aRoot, MPI_COMM_WORLD);
}

#endif /* VAR_MPI */
