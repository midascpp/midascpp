#ifndef DOTEVALUATOR_IMPL_H_INCLUDED
#define DOTEVALUATOR_IMPL_H_INCLUDED

#include <vector>

#include "util/Error.h"
#include "tensor/DotEvaluator_Helpers.h"
#include "tensor/ContractionMatrix.h"
#include "tensor/MatrixContractor.h"
#include "util/CallStatisticsHandler.h"
#include "lapack_interface/math_wrappers.h"

#include "tensor/TensorSum.h"
#include "tensor/TensorDirectProduct.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/SimpleTensor.h"
#include "tensor/Scalar.h"
#include "tensor/ZeroTensor.h"
#include "tensor/EnergyHolderTensor.h"
#include "tensor/NiceTensor.h"

#include "util/type_traits/Complex.h"
#include "util/Math.h"

/*! 
 *
 */
template
   <  class T
   >
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const BaseTensor<T>* const left
   ,  const BaseTensor<T>* const right
   )  const
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;

   assert_same_shape(left, right);
   dot_t result;

   std::vector<unsigned int> indices(left->NDim());
   for(int i = 0; i < left->NDim(); ++i)
   {
      indices[i] = i;
   }

   Scalar<T>* scalar = nullptr;

   scalar = dynamic_cast<Scalar<T>*>(contract(*left, indices, *right, indices, true, false)); // conjugate left tensor

   result = scalar->GetValue();

   delete scalar;

   return result;
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const BaseTensor<T>* const left
   ,  const SimpleTensor<T>* const right
   )  const
{
   return this->Dot(left, static_cast<const BaseTensor<T>* const>(right));
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const BaseTensor<T>* const left
   ,  const CanonicalTensor<T>* const right
   )  const
{
   return this->Dot(left, static_cast<const BaseTensor<T>* const>(right));
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const BaseTensor<T>* const left
   ,  const Scalar<T>* const right
   )  const
{
   assert_same_shape(left, right);
   auto left_scalar = dynamic_cast<const Scalar<T>* const>(left);
   return midas::math::Conj(left_scalar->GetValue())*right->GetValue();
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   ( const BaseTensor<T>* const left
   , const ZeroTensor<T>* const right
   ) const
{
   assert_same_shape(left, right);
   return static_cast<dot_t>(0.0);
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   ( const BaseTensor<T>* const left
   , const TensorSum<T>* const right
   ) const
{
   //LOGCALL("BASE SUM DOT");
   assert_same_shape(left, right);
   auto dot = static_cast<dot_t>(0.0);

   // Probably more efficient to use MKL threading in DGEMMs
//   #pragma omp parallel for schedule(dynamic) reduction(+:dot)
   for(int i = 0; i < right->mTensors.size(); ++i)
   {
      dot += right->mCoefs[i]*left->Dot(right->mTensors[i].get());
   }
   return dot;
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const SimpleTensor<T>* const left
   ,  const SimpleTensor<T>* const right
   )  const
{
   assert_same_shape(left, right);
   
   auto result = static_cast<dot_t>(0.0);
   auto size = left->TotalSize();
   for(unsigned i = 0; i < size; ++i)
   {
      result += midas::math::Conj(left->mData[i]) * right->mData[i];
   }
   return result;
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const SimpleTensor<T>* const left
   ,  const CanonicalTensor<T>* const right
   )  const
{
   return this->Dot(static_cast<const BaseTensor<T>* const>(left), static_cast<const BaseTensor<T>* const>(right));
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const SimpleTensor<T>* const left
   ,  const Scalar<T>* const right
   )  const
{
   //assert_same_shape(left, right);
   //auto left_scalar = dynamic_cast<const Scalar<T>* const>(left);
   //return left_scalar->GetValue()*right->GetValue();
   MIDASERROR("not implemented");

   return static_cast<dot_t>(0.);
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const SimpleTensor<T>* const left
   ,  const ZeroTensor<T>* const right
   )  const
{
   assert_same_shape(left, right);
   return static_cast<dot_t>(0.0);
}

/*! 
 * Dot between CanonicaTensor and CanonicalTensor:
 *   \f[
 *       \mathbf{A}_{ij \dots k} = \sum_{r } \mathbf{a}^1_{ir } \mathbf{a}^2_{jr } \dots \mathbf{a}^d_{kr }
 *       \mathbf{B}_{ij \dots k} = \sum_{r'} \mathbf{b}^1_{ir'} \mathbf{b}^2_{jr'} \dots \mathbf{b}^d_{kr'}
 *       
 *       \left\langle \mathbf{A} \middle| \mathbf{B} \right\rangle = \sum_{ij \dots k} \left( \sum_{r } \mathbf{a}^1_{ir} \mathbf{a}^2_{jr} \dots \mathbf{a}^d_{kr}\right) \left( \sum_{r'} \mathbf{b}^1_{ir} \mathbf{b}^2_{jr} \dots \mathbf{b}^d_{kr} \right)
 *                                                                 = \sum_{r } \sum_{r'} \prod_{n} \sum_l \mathbf{a}^n_{lr} \mathbf{b}^n_{lr'}
 *   \f]
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const CanonicalTensor<T>* const left
   ,  const CanonicalTensor<T>* const right
   )  const
{
   //LOGCALL("CANONICAL CANONICAL DOT");
   assert_same_shape(left, right);

   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;

   auto result = static_cast<dot_t>(0.0);
   const auto left_modematrices = left->GetModeMatrices();
   const auto right_modematrices = right->GetModeMatrices();
   const auto left_rank  = left->GetRank();
   const auto right_rank = right->GetRank();
   const auto size = left_rank * right_rank;
   const auto ndim = left->NDim();
   
   // initialize result array to 1, so we can multiply into it
   std::unique_ptr<T[]> result_array(new T[size]);
   auto result_ptr = result_array.get();
   for(int i = 0; i < size; ++i)
   {
      *(result_ptr++) = static_cast<T>(1.0);
   }
   std::unique_ptr<T[]> intermed_array(new T[size]);
   
   // calculate result_array
   for(int idim = 0; idim < ndim; ++idim)
   {
      int m = left_rank;
      int n = right_rank;
      int k = left->Extent(idim);
      int lda = std::max(1, k);
      int ldb = std::max(1, k);
      int ldc = std::max(1, m);
      char tc = is_complex ? 'C' : 'T';      // Complex conjugation
      char nc = 'N';
      T alpha = static_cast<T>(1.0);
      T beta  = static_cast<T>(0.0);
      midas::lapack_interface::gemm( &tc, &nc, &m, &n, &k, &alpha
                                   , left_modematrices [idim], &lda
                                   , right_modematrices[idim], &ldb
                                   , &beta, intermed_array.get(), &ldc
                                   );
      
      result_ptr = result_array.get();
      auto intermed_ptr = intermed_array.get();
      for(int i = 0; i < size; ++i)
      {
         *(result_ptr++) *= *(intermed_ptr++);
      }
   }
   
   // collect result
   result_ptr = result_array.get();
   
   for(int i = 0; i < size; ++i)
   {
      result += *(result_ptr++);
   }


   // Check sanity
   if (  !midas::util::IsSane(result)
      )
   {
      std::ostringstream os;
      os << "CANONICAL CANONICAL DOT returns " << result;
      MIDASERROR(os.str());
   }
   
   return result;
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const CanonicalTensor<T>* const left
   ,  const Scalar<T>* const right
   )  const
{
   //assert_same_shape(left, right);
   //auto left_scalar = dynamic_cast<const Scalar<T>* const>(left);
   //return left_scalar->GetValue()*right->GetValue();
   MIDASERROR("not implemented");
   return static_cast<dot_t>(0.);
}

/*! 
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const CanonicalTensor<T>* const left
   ,  const ZeroTensor<T>* const right
   )  const
{
   assert_same_shape(left, right);
   return static_cast<dot_t>(0.0);
}

/*! 
 * Dot between CanonicalTensor and TensorDirectProduct:
 *    
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const CanonicalTensor<T>* const left
   ,  const TensorDirectProduct<T>* const right
   )  const
{
   //LOGCALL("CANONICAL DIRPROD DOT");
   assert_same_shape(left, right);
   
   auto result = static_cast<dot_t>(0.0);
   auto left_modematrices = left->GetModeMatrices();
   auto reverse_indices = right->GetReverseConnection();

   // loop over tensors in direct product
   auto& left_dims = left->GetDims();
   auto left_rank = left->GetRank();
   for(unsigned lrank = 0; lrank < left_rank; ++lrank)
   {
      auto dirprod_result = static_cast<dot_t>(1.0);
      auto ntensors = right->mTensors.size();
      for(unsigned itens = 0; itens < ntensors; ++itens)
      {
         // for now only implemented for direct product of canonical tensors (at some point make partial dots for all other types)
         if(right->mTensors[itens]->Type() != BaseTensor<T>::typeID::CANONICAL) 
         {
            MIDASERROR("Only implemented for canonical");
         }
         auto canonical_ptr = static_cast<const CanonicalTensor<T>* const>(right->mTensors[itens].get());
         auto right_modematrices = canonical_ptr->GetModeMatrices();
         auto& canonical_dims = canonical_ptr->GetDims();
         auto canonical_rank = canonical_ptr->GetRank();
         auto canonical_ndim = canonical_ptr->NDim();
      
         // do partial dot for current tensor
         auto intermed_result = static_cast<dot_t>(0.0);
         for(unsigned rrank = 0; rrank < canonical_rank; ++rrank)
         {
            auto product_result = static_cast<dot_t>(1.0);
            for(unsigned dim = 0; dim < canonical_ndim; ++dim)
            {
               auto dim_result = static_cast<dot_t>(0.0);
               auto left_modematrix  = left_modematrices[reverse_indices[itens][dim]] + left_dims[reverse_indices[itens][dim]]*lrank;
               auto right_modematrix = right_modematrices[dim] + canonical_dims[dim]*rrank;

               for(unsigned extent = 0; extent < canonical_dims[dim]; ++extent)
               {
                  dim_result += midas::math::Conj(*left_modematrix) * (*right_modematrix);     // Complex conjugation
                  ++left_modematrix;
                  ++right_modematrix;
               }
               product_result *= dim_result;
            }
            intermed_result += product_result;
         }
         dirprod_result *= intermed_result;
      }
      // collect result
      result += dirprod_result;
   }
   
   return result*right->mCoef;
}

/**
 * CanonicalTensor, EnergyHolderTensorBase dot
 **/
template
   <  class T
   >
template
   <  template<class> class IMPL
   >
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const CanonicalTensor<T>* const left
   ,  const EnergyHolderTensorBase<T, IMPL>* const right
   )  const
{
   assert_same_shape(left, right);

   auto result = static_cast<dot_t>(0.);

   auto totsize = left->TotalSize();
   auto ndim = left->NDim();
   auto dims = left->GetDims();
   auto rank = left->GetRank();
   auto** mode_matrices = left->GetModeMatrices();

   std::vector<unsigned> indices(ndim, 0);
   
   // Loop over elements
   for(unsigned ielem = 0; ielem < totsize; ++ielem)
   {
      // Calculate element of CanonicalTensor
      auto left_elem = static_cast<T>(0.);
      for(unsigned irank = 0; irank < rank; ++irank)
      {
         auto prod_elem = static_cast<T>(1.0);
         for(unsigned idim = 0; idim < ndim; ++idim)
         {
            prod_elem *= mode_matrices[idim][indices[idim] + irank*dims[idim]];
         }
         left_elem += prod_elem;
      }

      // Add dot to result
      result += midas::math::Conj(left_elem) * right->Element(indices);  // complex conj
      
      // Increment indices
      for (int i = indices.size() - 1; i >= 0; --i)
      {
         if (++indices[i] == dims[i])
         {
            indices[i] = 0;
         }
         else
         {
            break;
         }
      }
   }

   return result;
}

/*! 
 * Dot between TensorDirectProduct and TensorDirectProduct:
 *    NB: this is made in a VERY in-efficient way!
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   (  const TensorDirectProduct<T>* const left
   ,  const TensorDirectProduct<T>* const right
   )  const
{
   constexpr bool is_complex = midas::type_traits::IsComplexV<T>;

//   LOGCALL("DIRPROD DIRPROD DOT");
   if(left->AllCanonical() && right->AllCanonical())
   {
      // if either left or right has rank 0, we shortcircuit and return 0.0
      if((left->Rank() == 0) || (right->Rank() == 0))
      {
         return static_cast<dot_t>(0.0);
      }

      auto left_connection  = left->GetConnection();
      auto right_connection = right->GetConnection();
      if( left_connection.size() != right_connection.size())
      {
         MIDASERROR("Connections are not same size.");
      }
      
      // loop over all indices and contract each index to a RxR' matrix for later contraction
      MatrixContractor<T> matrix_contractor(left_connection.size());
      IndexCreator index_creator;
      for(int idx = 0; idx < left_connection.size(); ++idx)
      {
         auto left_ptr  = static_cast<CanonicalTensor<T>* >(left ->GetTensors()[left_connection [idx].first].get());
         auto right_ptr = static_cast<CanonicalTensor<T>* >(right->GetTensors()[right_connection[idx].first].get());
         auto left_matrix  = left_ptr ->GetModeMatrices()[left_connection [idx].second];
         auto right_matrix = right_ptr->GetModeMatrices()[right_connection[idx].second];
         auto left_rank = left_ptr->GetRank();
         auto right_rank = right_ptr->GetRank();
         auto result_size = left_rank * right_rank;
         
         auto matrix = ContractionMatrix<T>( { index_creator.GetLeftIndex (left_connection[idx].first )
                                             , index_creator.GetRightIndex(right_connection[idx].first)
                                             }
                                           , { left_rank, right_rank } );
         
         // assert dimensions are correct
         if(left_ptr->GetDims()[left_connection[idx].second] != right_ptr->GetDims()[right_connection[idx].second])
         {
            MIDASERROR("left and right extents does not fit :C" );
         }
         
         // do the contraction and add matrx to the matrix contractor
         contract_index_modematrices( left_matrix, left_rank
                                    , right_matrix, right_rank
                                    , left_ptr->GetDims()[left_connection[idx].second]
                                    , matrix.Get() );                                     // Complex conjugation is done here!

         matrix_contractor.AddContractionMatrix(std::move(matrix));
      }
      
      // finally calculate the result and return
      auto result = matrix_contractor.Contract();
      return midas::math::Conj(left->Coef())*right->Coef()*result;
   }
   else
   {
      MidasWarning("Not all tensors in TensorDirectProduct are CanonicalTensors.");
      auto size_left = left->TotalSize();
      auto size_right = right->TotalSize();
      assert(size_left == size_right);

      auto left_ptr  = std::unique_ptr<T[]>(new T[size_left]);
      auto right_ptr = std::unique_ptr<T[]>(new T[size_right]);
         
      left->DumpInto(left_ptr.get());
      right->DumpInto(right_ptr.get());
      
      auto result = static_cast<T>(0.0);
      for(int i = 0; i < size_left; ++i)
      {
         result += midas::math::Conj(left_ptr[i]) * right_ptr[i];   // complex conj
      }
      return result;
   }
}

/*! 
 * Dot between TensorSum and TensorSum:
 *    
 */
template<class T>
typename DotEvaluator<T>::dot_t DotEvaluator<T>::Dot
   ( const TensorSum<T>* const left
   , const TensorSum<T>* const right
   ) const
{
   //LOGCALL("SUM SUM DOT");
   auto result = static_cast<dot_t>(0.0);
   auto rsize = right->mTensors.size();
   auto lsize = left->mTensors.size();

   // Probably more efficient to use MKL threading in DGEMMs
//   #pragma omp parallel for collapse(2) schedule(dynamic) reduction(+:result)
   for(int i = 0 ; i < lsize; ++i)
   {
      for(int j = 0; j < rsize; ++j)
      { 
         result += midas::math::Conj(left->mCoefs[i])*right->mCoefs[j]*left->mTensors[i]->Dot(right->mTensors[j].get());
      }
   }

   return result;
}

#endif /* DOTEVALUATOR_IMPL_H_INCLUDED */
