#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "BaseTensor.h"
#include "BaseTensor_Decl.h"
#include "BaseTensor_Impl.h"

// define
#define INSTANTIATE_BASETENSOR(T) \
template class BaseTensor<T>; \
\
template void assert_same_shape<T >(const BaseTensor<T >&, const BaseTensor<T >&); \
\
template void assert_same_shape(const BaseTensor<T>* const, const BaseTensor<T>* const);\
\
template typename BaseTensor<T>::real_t  diff_norm(const BaseTensor<T >&, const BaseTensor<T >&); \
\
template typename BaseTensor<T>::real_t  diff_norm_new(const BaseTensor<T >&, const BaseTensor<T >&); \
\
template typename BaseTensor<T>::real_t  diff_norm_new(const BaseTensor<T >*, const typename BaseTensor<T>::real_t, const BaseTensor<T >*, const typename BaseTensor<T>::real_t, T, const size_t); \
\
template typename BaseTensor<T>::real_t  diff_norm2_new(const BaseTensor<T >*, const typename BaseTensor<T>::real_t, const BaseTensor<T >*, const typename BaseTensor<T>::real_t, T, const size_t); \
\
template typename BaseTensor<T>::real_t  safe_diff_norm2(const BaseTensor<T >*, const BaseTensor<T >*); \
\
template bool have_same_shape(const BaseTensor<T>&, const BaseTensor<T>&); \
\
template BaseTensor<T >* operator*(T , const BaseTensor<T >&); \
\
template BaseTensor<T>* contract(const BaseTensor<T>&, \
                        const std::vector<unsigned int>&, \
                        const BaseTensor<T>&, \
                        const std::vector<unsigned int>&, bool, bool ); \
\
template std::ostream& operator<<(std::ostream&, const BaseTensor<T>&);


// concrete instantiations
INSTANTIATE_BASETENSOR(float)
INSTANTIATE_BASETENSOR(double)
INSTANTIATE_BASETENSOR(std::complex<float>)
INSTANTIATE_BASETENSOR(std::complex<double>)

#undef INSTANTIATE_BASETENSOR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
