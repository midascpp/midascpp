#ifndef CONTRACTION_MATRIX_DECL_H_INCLUDED
#define CONTRACTION_MATRIX_DECL_H_INCLUDED

#include <memory>

///> forward declaration of friends
template<class T>
class ContractionMatrix;
template<class T>
std::ostream& operator<<(std::ostream&, const ContractionMatrix<T>&);

/**
 * Class to hold a matrix resulting from a contraction of
 * two mode matrices of CanonicalTensor%s, where the dimension index
 * has been contracted away, and the two rank indices are left.
 * Used by DotEvaluator for evaluating dots between two
 * TensorDirectProduct%s of all CanonicalTensor%s.
 **/
template<class T>
class ContractionMatrix
{
   private:
      std::pair<unsigned, unsigned> mIndices; ///< Contraction indices
      std::pair<unsigned, unsigned> mExtents; ///< Extents of each dimension
      unsigned                      mSize;    ///< Total size of matrix
      std::unique_ptr<T[]>          mData;    ///< The actual data
   public:
      ///> constructor
      ContractionMatrix( std::pair<unsigned, unsigned>
                       , std::pair<unsigned, unsigned> );
      
      ///> Access indices
      const std::pair<unsigned, unsigned>& Indices() const;
      
      ///> Access extents
      const std::pair<unsigned, unsigned>& Extents() const;

      ///> Access Size
      unsigned Size() const;
      
      ///> Access raw pointer.
      T* Get() const;
      
      ///> Access element (not an efficient API!!!)
      T GetElem(unsigned, unsigned) const;

      ///> Output operator
      friend std::ostream& operator<< <T>(std::ostream&, const ContractionMatrix<T>&);
};

#endif /* CONTRACTION_MATRIX_DECL_H_INCLUDED */
