#ifndef SIMPLETENSOR_H_INCLUDED
#define SIMPLETENSOR_H_INCLUDED

// Include declaration
#include "SimpleTensor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// Include implementation
#include "SimpleTensor_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* SIMPLETENSOR_H_INCLUDED */
