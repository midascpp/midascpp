/**
************************************************************************
* 
* @file    CpOptimization.h
*
* @date    27-03-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Helper functions for non-linear optimization of CP tensors.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CPOPTIMIZATION_H_INCLUDED
#define CPOPTIMIZATION_H_INCLUDED

#include "tensor/FitNCG.h"
#include "tensor/FitReport.h"
#include "libmda/numeric/optim/wolfe_line_search.h"
#include "libmda/numeric/optim/hager_zhang_line_search.h"
#include "libmda/numeric/optim/exact_3pg_line_search.h"
#include "util/CallStatisticsHandler.h"
#include "libmda/numeric/float_eq.h"

template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
struct CpNCGInput;

namespace midas
{
namespace tensor
{
namespace detail
{

/**
 * Pre-allocated workspace for CP optimization. Contains intermediate 
 * machines and work arrays for gradient calculation.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
struct CpOptWorkspace
{
   //! Pre-allocated gradient workspace
   std::unique_ptr<CanonicalTensor<T>> mGradWork = nullptr;

   //! Pre-allocated trial-vector workspace
   std::unique_ptr<CanonicalTensor<T>> mTrialWork = nullptr;

   //! Squeezer intermediates (NB: Tracks mTrialWork!)
   std::unique_ptr<Squeezer<T>> mSqueezer = nullptr;

   //! Gamma intermediates
   std::unique_ptr<GeneralGammaIntermediates<T>> mGammaIntermeds = nullptr;

   //! Pre-allocated gamma-matrix workspace
   std::unique_ptr<T[]> mGammaWork = nullptr;
   
   //! Pre-allocated squeezer-matrix workspace
   std::unique_ptr<T[]> mSqueezerWork = nullptr;


   //! Constructor from target tensor and fitting tensor
   CpOptWorkspace
      (  const BaseTensor<T>* const target_tensor
      ,  const CanonicalTensor<T>* const fitting_tensor
      ,  T gradient_scaling = static_cast<T>(1.)
      )
      :  mGradWork(new CanonicalTensor<T>(fitting_tensor->GetDims(), fitting_tensor->GetRank()))
      ,  mTrialWork(fitting_tensor->Clone())
      ,  mSqueezer(new Squeezer<T>(mTrialWork.get(), target_tensor))
      ,  mGammaIntermeds(new GeneralGammaIntermediates<T>(mTrialWork.get(), mTrialWork.get()))
      ,  mGammaWork(new T[fitting_tensor->GetRank()*fitting_tensor->GetRank()])
      ,  mSqueezerWork(new T[fitting_tensor->GetRank()*fitting_tensor->MaxExtent()])
   {
   }

   //! Dis-allow copy construction
   CpOptWorkspace(const CpOptWorkspace&) = delete;

   //! Dis-allow copy assignment
   CpOptWorkspace& operator=(const CpOptWorkspace&) = delete;


   //! Calculate gradient and store in mGradWork
   void UpdateIntermediatesAndGradient
      (
      )
   {
      // Update intermediates
      this->mSqueezer->ReCalculate();
      this->mGammaIntermeds->ReCalculate(this->mTrialWork.get(), this->mTrialWork.get());

      // Update mGradWork
      this->mTrialWork->Gradient
         (  *this->mGradWork
         ,  *this->mGammaIntermeds
         ,  *this->mSqueezer
         ,  this->mGammaWork
         ,  this->mSqueezerWork
         );
   }
};

/**
 * Calculate beta value for CP-NCG algorithm
 *
 * @param g_new      New gradient
 * @param g_old      Old gradient
 * @param d          Previous search direction
 * @param input      CpNCGInput of decomp definitions
 * @param work       Workspace to store intermediates
 *
 * @return
 *    Beta value
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
T CpNcgBeta
   (  const CanonicalTensor<T>* const g_new
   ,  const CanonicalTensor<T>* const g_old
   ,  const CanonicalTensor<T>* const d
   ,  const CpNCGInput<T>& input
   ,  CpOptWorkspace<T>& work
   )
{
   bool precon = input.mDiagonalPreconditioner;

   T beta = static_cast<T>(0.);

   switch(input.mNcgType)
   {
      case CpNCGInput<T>::ncgType::HS:
      {
         // Use work.mGradWork to store y
         auto& y = work.mGradWork;
         *y = *g_new;
         y->ElementwiseAxpy(*g_old, static_cast<T>(-1.));

         // Calculate denominator
         auto denom = d->ElementwiseDot(*y);

         // Precondition y
         if (  precon   )
         {
            work.mGammaIntermeds->DiagonalTransformation(y.get(), true);
         }

         // Calculate beta
         beta = g_new->ElementwiseDot(*y) / denom;
         break;
      }
      case CpNCGInput<T>::ncgType::HZ:
      {
         // Use work.mGradWork to store y
         auto& y = work.mGradWork;
         *y = *g_new;
         y->ElementwiseAxpy(*g_old, static_cast<T>(-1.));

         // Calculate intermediates including y
         T inv_denom = static_cast<T>(1.) / d->ElementwiseDot(*y);

         // Precondition y and calculate overlap with non-preconditioned tensor
         T y_norm2 = precon ? work.mGammaIntermeds->DiagonalTransformation(y.get(), true) : y->ElementwiseNorm2();

         // Store new intermediate in y
         T scaling = -input.mHagerZhangTheta*y_norm2 * inv_denom;
         y->ElementwiseAxpy(*d, scaling);

         // Calculate beta
         beta = inv_denom * y->ElementwiseDot(*g_new);

         // Lower bound
         T eta = static_cast<T>(1.e-2);
         
         T dnorm = static_cast<T>(0.);
         T gnorm = static_cast<T>(0.);

         // Calculate preconditioned norms of d and g_old
         if (  precon   )
         {
            // Store intermediate in y
            *y = *d;
            dnorm = work.mGammaIntermeds->DiagonalTransformation(y.get(), false);   // Transform with inverse preconditioner

            // Store intermediate in y
            *y = *g_old;
            gnorm = work.mGammaIntermeds->DiagonalTransformation(y.get(), true);
         }
         else
         {
            dnorm = d->ElementwiseNorm();
            gnorm = g_old->ElementwiseNorm();
         }

         T eta_k = static_cast<T>(-1.) / (dnorm*std::min(eta, gnorm));

         beta = std::max(beta, eta_k);

         break;
      }
      case CpNCGInput<T>::ncgType::FR:
      {
         // Calculate beta
         if (  precon   )
         {
            // Use work.mGradWork to store preconditioned gradients
            auto& pgrad = work.mGradWork;

            // Calculate beta numerator
            *pgrad = *g_new;
            beta = work.mGammaIntermeds->DiagonalTransformation(pgrad.get(), true);

            // Calculate beta denominator
            *pgrad = *g_old;
            beta /= work.mGammaIntermeds->DiagonalTransformation(pgrad.get(), true);
         }
         else
         {
            beta = g_new->ElementwiseNorm2() / g_old->ElementwiseNorm2();
         }
         break;
      }
      case CpNCGInput<T>::ncgType::PR:
      {
         // Use work.mGradWork to store y
         auto& y = work.mGradWork;
         *y = *g_new;
         y->ElementwiseAxpy(*g_old, static_cast<T>(-1.));

         // Precondition y
         if (  precon   )
         {
            work.mGammaIntermeds->DiagonalTransformation(y.get(), true);
         }
         
         // Calculate beta numerator
         beta = g_new->ElementwiseDot(*y);

         // Divide by denominator
         if (  precon   )
         {
            // Store preconditioned result in work.mGradWork (aka y)
            *y = *g_old;
            beta /= work.mGammaIntermeds->DiagonalTransformation(y.get(), true);
         }
         else
         {
            beta /= g_old->ElementwiseNorm2();
         }

         // Check if we use PRP+
         if (  input.mPrpPlus )
         {
            beta = std::max(beta, static_cast<T>(0.));
         }

         break;
      }
      case CpNCGInput<T>::ncgType::DY:
      {
         // Use work.mGradWork to store y
         auto& y = work.mGradWork;
         *y = *g_new;
         y->ElementwiseAxpy(*g_old, static_cast<T>(-1.));

         auto denom = d->ElementwiseDot(*y);

         // Calculate numerator
         if (  precon   )
         {
            *y = *g_new;
            beta = work.mGammaIntermeds->DiagonalTransformation(y.get(), true);
         }
         else
         {
            beta = g_new->ElementwiseNorm2();
         }

         // Calculate beta
         beta /= denom;

         break;
      }
      case CpNCGInput<T>::ncgType::FP:
      {
         // Not implemented
      }
      default:
      {
         MIDASERROR("CpNcgBeta: ncgType not implemented!");
         break;
      }
   }

   return beta;
}


/**
 * Calculate line-search parameter (step length) for CP-NCG
 *
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
T CpOptLineSearch
   (  const BaseTensor<T>* const target_tensor
   ,  T target_norm2
   ,  const CanonicalTensor<T>* const fitting_tensor
   ,  const CanonicalTensor<T>* const update_tensor
   ,  T alpha_old
   ,  T slope_init
   ,  const CpNCGInput<T>& input
   ,  CpOptWorkspace<T>& workspace
   )
{
//   LOGCALL("call line search");

   // Construct phi function
   struct phi_func
   {
      // Step and value type
      using step_t [[maybe_unused]] = T;
      using value_t = T;

      // Pointers to tensors
      const BaseTensor<T>* const mTarget;
      T mTargetNorm2;
      const CanonicalTensor<T>* const mFit;
      const CanonicalTensor<T>* const mUpdate;

      // Reference to workspace
      CpOptWorkspace<T>& mWork;

      // Constructor
      phi_func
         (  const BaseTensor<T>* const target
         ,  T target_norm2
         ,  const CanonicalTensor<T>* const fit
         ,  const CanonicalTensor<T>* const update
         ,  CpOptWorkspace<T>& work
         )
         :  mTarget(target)
         ,  mTargetNorm2(target_norm2)
         ,  mFit(fit)
         ,  mUpdate(update)
         ,  mWork(work)
      {
      }

      // Calculate phi function
      value_t operator()
         (  T a
         )  const
      {
//         LOGCALL("phi evaluation");
         // Copy to pre-allocated workspace
         *this->mWork.mTrialWork = *this->mFit;
         this->mWork.mTrialWork->ElementwiseAxpy(*this->mUpdate, a);

         //T err_norm2 = diff_norm2_new(&copy, copy.Norm2(), this->mTarget, this->mTargetNorm2);
         T err_norm2 = safe_diff_norm2(this->mWork.mTrialWork.get(), this->mTarget);

         return static_cast<T>(0.5)*err_norm2;
      }

      // Calculate first derivative
      value_t first_derivative
         (  T a
         )  const
      {
//         LOGCALL("phi derivative");
         // Copy to pre-allocated workspace
         *this->mWork.mTrialWork = *this->mFit;
         this->mWork.mTrialWork->ElementwiseAxpy(*this->mUpdate, a);

         // Calculate gradient and store in pre-allocated workspace
         this->mWork.UpdateIntermediatesAndGradient();

         return this->mWork.mGradWork->ElementwiseDot(*this->mUpdate);
      }

   }  phi(target_tensor, target_norm2, fitting_tensor, update_tensor, workspace);

   T alpha = static_cast<T>(1.);

   // Perform the line search
   switch(  input.mLineSearch )
   {
      case CpNCGInput<T>::lsType::WOLFE:
      {
         T alpha_resolution = input.mLineSearchAlphaResolution;
         T alpha_max = input.mLineSearchAlphaMax;
         T c_1 = input.mWolfeConditionsC1;
         T c_2 = input.mWolfeConditionsC2;
         T alpha_init = static_cast<T>(0.);
         alpha = libmda::numeric::optim::wolfe_line_search(phi, alpha_init, alpha_max, alpha_resolution, c_1, c_2);
         break;
      }
      case CpNCGInput<T>::lsType::EXACT:
      {
         T alpha_max_trial = static_cast<T>(2.)*alpha_old + static_cast<T>(1.e-16); // in case alpha_old = 0
         T c = input.m3pgC;
         T d = input.m3pgD;
         T acc = input.mMaxerr * input.m3pgRelativeThreshold;
         size_t maxiter = input.m3pgMaxiter;
         alpha = libmda::numeric::optim::exact_3pg_line_search(phi, alpha_max_trial, slope_init, c, d, acc, maxiter);
         break;
      }
      case CpNCGInput<T>::lsType::HZ:
      {
         T alpha_max_trial = static_cast<T>(2.)*alpha_old;
         alpha = libmda::numeric::optim::hager_zhang_line_search(phi, 0., alpha_max_trial, true, slope_init);
         break;
      }
      default:
      {
         MIDASERROR("CpNcgLineSearch: Unknown line-search type!");
         break;
      }
   }

   return alpha;
}

} /* namespace detail */
} /* namespace tensor */
} /* namespace midas */

#endif /* CPOPTIMIZATION_H_INCLUDED */
