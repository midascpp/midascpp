/**
************************************************************************
* 
* @file    CpNcgDecomposer.cc
*
* @date    18-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of CpNcgDecomposer methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/CpNcgDecomposer.h"
#include "tensor/TensorFits.h"
#include "tensor/FitNCG.h"

#include <cassert>

namespace midas
{
namespace tensor
{

//! register CpNcgDecomposer with ConcreteCpDecomposerFactory
detail::ConcreteCpDecomposerRegistration<CpNcgDecomposer> registerCpNcgDecomposer("NCG");



/** 
 * Check if tensor is decomposed by this decomposer.
 * @param aTensor                    Tensor to check.
 * @return                           Returns whether tensor will be decomposed.
 **/
bool CpNcgDecomposer::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool type_ok  = ( (aTensor.Type() == BaseTensor<Nb>::typeID::SIMPLE) 
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::CANONICAL)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::SUM)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::DIRPROD)
                   );
   return type_ok;
}

/**
 * Decompose NiceTensor using CPNCG algorithm.
 * @param aTensor        Tensor to decompose.
 * @param aNorm2         Squared norm of the target tensor
 * @param aFitTensor     Fitting tensor and starting guess
 * @param aThresh        The requested accurcy
 * @return               FitReport of the decomposition
 **/
FitReport<Nb> CpNcgDecomposer::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   // Set threshold if the driver does not require a specific accuracy
   Nb thresh = aThresh > C_0 ? aThresh : this->mCpNcgThreshold;
   Nb check_thresh = thresh * this->mCpNcgRelativeCheckThresh;
   if (  this->mCpNcgRelativeThreshold > C_0 )
   {
      thresh *= this->mCpNcgRelativeThreshold;
   }

   CpNCGInput<Nb> input(mInfo);
   input.SetMaxerr(thresh);
   input.SetCheckThresh(check_thresh);

   auto fit_report = FitCPNCG( aTensor, aNorm2, aFitTensor, input );

   return fit_report;
}

/**
 * Get type of decomposer as a std::string.
 * @return          String with type.
 **/
std::string CpNcgDecomposer::TypeImpl
   (
   )  const
{
   return std::string("NcgDecomposer");
}

/**
 * Constructor from TensorDecompInfo.
 * @param aInfo                TensorDecompInfo to construct from.
 **/
CpNcgDecomposer::CpNcgDecomposer
   ( const TensorDecompInfo& aInfo
   )
   :  mCpNcgThreshold            (const_cast<TensorDecompInfo&>(aInfo)["CPNCGTHRESHOLD"].get<Nb>())
   ,  mCpNcgRelativeThreshold    (const_cast<TensorDecompInfo&>(aInfo)["CPNCGRELATIVETHRESHOLD"].get<Nb>())
   ,  mCpNcgRelativeCheckThresh  (const_cast<TensorDecompInfo&>(aInfo)["CHECKTHRESH"].get<Nb>())
   ,  mInfo                      (aInfo)
{
}

} /* namespace tensor */
} /* namespace midas */
