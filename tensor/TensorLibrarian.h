/**
 ************************************************************************
 * 
 * @file                TensorLibrarian.h
 *
 * Created:             30.04.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               TensorLibrarian: Dictionary (map) for NiceTensor's with support for synonyms.
 * 
 * Detailed Description: Data structure to store a collection of tensors
 *                       representing different blocks of larger tensors.
 *                       Support for block-symmetry in the form of key synonyms.
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */
#ifndef TENSORLIBRARIAN_H_INCLUDED
#define TENSORLIBRARIAN_H_INCLUDED

#include<vector>
#include<iostream>
#include<map>
#include<functional>

#include "NiceTensor.h"
#include "MultiIndex.h"
#include "TensorView.h"

template <class T>
class TensorLibrarian_InitIterator;

template <class T>
class TensorLibrarian_InitContainer;

/** \brief Dictionary (map) for NiceTensor's. Support for synonyms.
*
* `TensorLibrarian` is a class designed to conveniently store tensors which are
* organized in symmetry-related blocks. For example consider the two-electron MO
* integrals \f$g_{pqrs}\f$. Each index can be either occupied (o) or virtual
* (v).  The symmetry relations are the index permutations \f$(1\leftrightarrow
* 2)\f$, \f$(3\leftrightarrow 4)\f$ and \f$(1\leftrightarrow 3, 2\leftrightarrow
* 4)\f$, which yields the following equivalencies:
*
* \f[ g_{pqrs} =g_{qprs} =g_{pqsr} =g_{qpsr}= g_{rspq} =g_{rsqp} =g_{srpq}
* =g_{srqp} \f]
*
* and the following unique blocks:
*
* \f{eqnarray}{ g_{oooo}\\ g_{vooo}\equiv g_{ovoo}\equiv g_{oovo}\equiv
* g_{ooov}\\ g_{vvoo}\equiv g_{oovv}\\ g_{vovo}\equiv g_{ovvo}\equiv
* g_{voov}\equiv g_{ovov}\\ g_{vvvo}\equiv g_{vvov}\equiv g_{vovv}\equiv
* g_{ovvv}\\ g_{vvvv} \f}
*
* `TensorLibrarian` allows us to store only one of \f$g_{vovo}\f$,
* \f$g_{ovvo}\f$, \f$g_{voov}\f$ and \f$g_{ovov}\f$, but being able to interact
* with it in the way we requested. For example, let's suppose the block
* \f$g_{ovvo}\f$ is requested, but the block \f$g_{vovo}\f$ is the one that is
* stored, when we deal with element \f$g_{iabj}\f$ in reality we are dealing
* with \f$g_{iabj}\f$ (but we won't notice anything).
*
* Example
* -------
* The following piece of code showcases how to construct the MO integral
* blocks from the AO integrals.
*
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* #include"tensor/TensorLibrarian.h"
* #include"tensor/MultiIndex.h"
*
* unsigned int nocc=...;        // Number of occupied
* unsigned int nvir=...;        // Number of virtual
* unsigned int nbas=nocc+nvir;  // Number of basis functions
*
* // AO integrals
* NiceTensor<double> ao_two_electron(std::vector<unsigned int>{nbas,nbas,nbas,nbas}, ...);
*
* // MO coefficients, separated into virtual and occupied
* std::map<unsigned int, NiceTensor<double>> mo;
* mo['o']=NiceTensor<double>(std::vector<unsigned int>{nbas,nocc}, ...);
* mo['v']=NiceTensor<double>(std::vector<unsigned int>{nbas,nvir}, ...);
*
* //--------------- DECLARATION ---------------
* TensorLibrarian<double> g(
*             4,                      // Number of labels
*             multiindex_t{'v','o'},  // Labels, in order: 'v' comes before 'o'
*             std::vector<indexpermutation_t>{
*                    indexpermutation_t{indexswap_t{0,1}}, // pqrs <-> qprs
*                    indexpermutation_t{indexswap_t{2,3}}, // pqrs <-> pqsr
*                    indexpermutation_t{indexswap_t{0,2},  // pqrs <-> rspq
*                                       indexswap_t{1,3}}}
* );
* //--------------- INITIALIZATION ------------
* for(const auto& initpair: g.InitializationLoop())
* {
*    // initpair.first is the key, a multiindex_t vector, e.g. {'v','v','v','o'}
*    // initpair.second is a pointer (NiceTensor<double>*) to the value
*    initpair.second = new NiceTensor<double>(
*        initpair.second = new NiceTensor<Nb>(
*              // The block is constructed by contracting with the correct
*              // matrices. For example, the vvvo block is obtained as
*              // g_{abci}=\sum_{pqrs} C_{pa} C_{qb} C_{rc} C_{si} g^{AO}_{pqrs}
*              ao_two_electron.ContractForward(0, 
*                 mo.at(initpair.first[0])).ContractForward(1, 
*                    mo.at(initpair.first[1])).ContractForward(2, 
*                       mo.at(initpair.first[2])).ContractForward(3, 
*                          mo.at(initpair.first[3])));
*                 );
* }
* //--------------- USAGE ------------
* // g_{aibj} + t_{cidj} g{acbd}  -> g_{pqrs} + t_{cqds} g{pcrd} (see ContractionIndices.h)
* NiceTensor<double> something=
*      g[multiindex_t{'v','o','v','o'}] +
*      contract(t[2][X::c,X::q,X::d,X::s], g[multiindex_t{'v','v','v','v'}][X::p,X::c,X::r,X::d]);
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
template <class T> class TensorLibrarian {
   private:
      //! Container for the tensors.
      std::map<std::vector<unsigned int>, NiceTensor<T>*> mTensors;
      //! Number of letters in a key. For \f$g\f$: 4.
      unsigned int mKeyLength;
      //! All the possible values that a letter in a key can take. For \f$g\f$: {'v','o'}
      /*! IMPORTANT: The order determines their lexicographic value. */
      std::vector<unsigned int>       mAllowedKeyValues;
      //! Synonym rules. For \f$g\f$: { { (0,1) }, { (2,3) }, { (0,2),(1,3) } }
      std::vector<indexpermutation_t> mAllowedPermutations;
      //! Lexicographic values of the keys. For \f$g\f$: { 'v':0, 'o':1 }
      /*! Determined by mAllowedKeyValues */
      std::map<unsigned int, unsigned int> mKeysIds;
      //! Predicates that must return false for a key to be accepted.
      /*! Used to exclude given keys that would otherwise be allowed. In our
       * example, if we wanted to exclude the \f$vvvv\f$ block, we pass a vector
       * containing only one lambda function:
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       * std::vector<std::function<bool(const std::vector<unsigned int>&)>>
       * {
       *    [](const std::vector<unsigned int>& key)
       *    {
       *       return std::all_of(key.begin(), key.end(), [](unsigned int x){return x=='v';});
       *       // NOTE: Requires overloading
       *       // operator==(const std::vector<unsigned int>&, const std::vector<unsigned int>&)
       *    }
       * }
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       * A explicit blacklist can also be considered:
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       * // Blacklists vvvv and ooov blocks
       * 
       * std::vector<std::function<bool(const std::vector<unsigned int>&)>>
       * {
       *    [](const std::vector<unsigned int>& key)
       *    {
       *       std::vector<std::vector<unsigned int>> blacklist{ {'v','v','v','v'},
       *                                                         {'v','v','o','o'} };
       *       return std::any_of(blacklist.begin(),
       *                          blacklist.end(),
       *                          [key](const std::vector<unsigned int> bad_key){return bad_key==key;});
       *       // NOTE: Requires overloading
       *       // operator==(const std::vector<unsigned int>&, const std::vector<unsigned int>&)
       *    }
       * }
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       *  
       * The predicates are evaluated when the TensorLibrarian is being
       * initialized using InitializationLoop(), within
       * TensorLibrarian_InitIterator::operator*.
       *
       * IMPORTANT: The predicates are evaluated on the lowest-lexicographic
       * permutation. So in the blacklist example, including ``{'o','o','v','v'}``
       * in the blacklist (instead of ``{'v','v','o','o'}``) will miss that block.
       * */
      std::vector<std::function<bool(const std::vector<unsigned int>&)>> mBlackListPredicates;

      friend TensorLibrarian_InitIterator<T>;

      //! Convert key into Ids: e.g. {1,1,0,1} -> {'o','o','v','o'}
      multiindex_t IdsToKeys(const multiindex_t& ids) const
      {
         multiindex_t keys;
         for(auto id:ids)
            keys.push_back(mAllowedKeyValues.at(id));
         return keys;
      }

      //! Convert Ids into key: e.g. {'o','o','v','o'} -> {1,1,0,1}
      multiindex_t KeysToIds(const multiindex_t& keys) const
      {
         multiindex_t ids;
         for(auto key:keys)
            ids.push_back(mKeysIds.at(key));
         return ids;
      }

      //! Check if a key is blacklisted
      bool IsKeyBlacklisted(const std::vector<unsigned int>& key)
      {
         for(auto pred: mBlackListPredicates)
         {
            if(pred(key))
            {
               std::cout << " ######## BLACKLISTED: (";
               for(auto c: key)
                  std::cout << char(c);
               std::cout << ")" << std::endl;
               return true;
            }
         }
         return false;
      }

   public:
      //! Default constructor
      TensorLibrarian()
      {
      };

      //! Normal constructor: key length, key values, symmetry rules
      TensorLibrarian(const unsigned int                     aKeyLength
                     ,const std::vector<unsigned int>&       aAllowedKeyValues
                     ,const std::vector<indexpermutation_t>& aAllowedPermutations
                     ,const std::vector<std::function<bool(const std::vector<unsigned int>&)>>& aBlackListPredicates={}):
         mKeyLength          { aKeyLength }
        ,mAllowedKeyValues   { aAllowedKeyValues }
        ,mAllowedPermutations{ aAllowedPermutations }
        ,mBlackListPredicates{ aBlackListPredicates }
      {
         unsigned int i=0;
         for(auto key: mAllowedKeyValues)
            mKeysIds[key]=i++;
      }

      //! Copy constructor
      TensorLibrarian(const TensorLibrarian& other):
         mKeyLength          (other.mKeyLength)
        ,mAllowedKeyValues   (other.mAllowedKeyValues)
        ,mAllowedPermutations(other.mAllowedPermutations)
        ,mKeysIds            (other.mKeysIds)
        ,mBlackListPredicates(other.mBlackListPredicates)
      {
         for(auto i: other.mTensors)
            mTensors[i.first]=new NiceTensor<T>(*i.second);
      };

      //! Move constructor
      TensorLibrarian(TensorLibrarian&& other):
         mKeyLength          (std::move(other.mKeyLength))
        ,mAllowedKeyValues   (std::move(other.mAllowedKeyValues))
        ,mAllowedPermutations(std::move(other.mAllowedPermutations))
        ,mKeysIds            (std::move(other.mKeysIds))
        ,mBlackListPredicates(std::move(other.mBlackListPredicates))
      {
         for(auto i= other.mTensors.begin();i!=other.mTensors.end();++i)
         {
            mTensors[i->first]=nullptr;
            std::swap( mTensors[i->first], i->second );
         }
      };

      //! Copy assignment
      TensorLibrarian& operator=(const TensorLibrarian& other)
      {
         mKeyLength          =other.mKeyLength;
         mAllowedKeyValues   =other.mAllowedKeyValues;
         mAllowedPermutations=other.mAllowedPermutations;
         mKeysIds            =other.mKeysIds;
         mBlackListPredicates=other.mBlackListPredicates;
         for(auto& i: other.mTensors)
            mTensors[i.first]=new NiceTensor<T>( *i.second );
         return *this;
      };

      //! Move assignment
      TensorLibrarian& operator=(TensorLibrarian&& other)
      {
         std::swap(mKeyLength          ,other.mKeyLength);
         std::swap(mAllowedKeyValues   ,other.mAllowedKeyValues);
         std::swap(mAllowedPermutations,other.mAllowedPermutations);
         std::swap(mKeysIds            ,other.mKeysIds);
         std::swap(mTensors            ,other.mTensors);
         std::swap(mBlackListPredicates,other.mBlackListPredicates);

         return *this;
      };

      //! Destructor
      ~TensorLibrarian()
      {
         for(auto& pair: mTensors)
            if(pair.second)
               delete pair.second;
      }

      //! Read-only element access.
      NiceTensor<T> operator[](const multiindex_t& keys) const
      {
         // The first step is permuting the passed keys until we obtain the
         // lowest lexicographic order.
         multiindex_t ids=KeysToIds(keys);
         // The permutations that were necessary to recreate the key are the
         // same we will use to initialize the TensorView.
         std::vector<indexpermutation_t> permutations_needed;
         for(auto permutation: mAllowedPermutations)
         {
            multiindex_t copy=ids;
            // We permute until the lexicographic value of the key is minimized
            if(permute(copy, permutation) < ids)
            {
               ids=copy;
               permutations_needed.push_back(permutation);
            }
         }

         // We return the relevant block wrapped in a TensorView
         return NiceTensor<T>( new TensorView<T>(*( mTensors.at(ids)->GetTensor() ), permutations_needed) );
      }

      //! Read-only element access, more convenient char[]-based.
      NiceTensor<T> operator[](const char* keys) const
      {
         return (*this)[multiindex_t(keys, keys+strlen(keys))];
      }

      //! Loop through all valid keys to initialize tensors.
      /*! Usage:
       *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       *  TensorLibrarian<...> foo(...);
       *  for(const auto& initpair: foo.InitializationLoop())
       *  {
       *     // initpair.first is the key
       *     // initpair.second is a pointer to the tensor to be created
       *     initpair.second = new NiceTensor<...> ( ...some function that depends on initpair.first... );
       *  }
       *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       */
      TensorLibrarian_InitContainer<T> InitializationLoop()
      {
         return TensorLibrarian_InitContainer<T>(*this);
      }
};

//! Wrapper for iteration over keys and pointers to values in initialization of TensorLibrarian (see TensorLibrarian::InitializationLoop)
template <class T>
class TensorLibrarian_InitContainer
{
   private:
      TensorLibrarian<T>& mRef;

   public:
      TensorLibrarian_InitContainer(TensorLibrarian<T>& aRef):
         mRef(aRef)
      {
      }
         
      //! Dummy. The actual worload was performed by TensorLibrarian_InitIterator::TensorLibrarian_InitIterator()
      TensorLibrarian_InitIterator<T> begin()
      {
         return TensorLibrarian_InitIterator<T>(mRef);
      }

      //! Dummy. The exit condition for the loop is done by overloading TensorLibrarian_InitIterator::operator !=
      TensorLibrarian_InitIterator<T> end()
      {
         return begin();
      }
};

template <class T>
class TensorLibrarian_InitIterator
{
   private:
      //! Object we are initializing/iterating over
      TensorLibrarian<T>& mRef;
      //! Exit condition
      bool                mDone;
      //! Number of possible letters in a key = Maximum lexicographical value of a letter
      unsigned int        mMaxval;
      //! Loop variables.
      /*! Starts at {0,0,...,0} and is increaed by TensorLibrarian_InitIterator::operator ++ in lexicographic order.
       */
      multiindex_t        mKeyIds;
   
   public:
      //! Default constructor
      TensorLibrarian_InitIterator(TensorLibrarian<T>& aRef)
         : mRef   (aRef)
         , mDone  (false)
         , mMaxval(aRef.mAllowedKeyValues.size())
         , mKeyIds (aRef.mKeyLength, 0)
      {
      }

      //! Advances mKeyIds to the next allowed value, creates an entry in TensorLibrarian::mTensors and returns a pointer to that entry
      TensorLibrarian_InitIterator& operator++()
      {
         // In this loop, we increase and check all possible permutations
         bool new_key_still_not_found=true;
         while(new_key_still_not_found)
         {
            // We are done when all the keys have become maximum
            mDone=std::all_of( mKeyIds.begin(), mKeyIds.end(), [this](unsigned int x){return x==mMaxval-1;} );
            // Note that despite returning *this, once mDone is true != will evaluate to false
            // and this last value won't be used
            if(mDone)
               return *this;
            // Obtain a new key
            increase(mKeyIds, mMaxval);
            // Assume that the new key is truly a new key
            new_key_still_not_found=false;
            // Test that this key is truly a new key and we have not already done
            // a allowed permutation. We check that the new key is the smallest
            // (lexicographically) of all its allowed permutations
            multiindex_t copy=mKeyIds;
            for(auto permutation: mRef.mAllowedPermutations)
            {
               multiindex_t recopy=copy;
               if(permute(recopy, permutation) < copy)
               {
                  new_key_still_not_found=true;
                  break;
               }
               copy=recopy;
            }
         }
         return *this;
      }

      //! Dummy termination check. The check is performed by operator++
      bool operator!=(const TensorLibrarian_InitIterator& ignore)
      {
         return !mDone;
      }

      //! Construct the initialization pair (key, pointer to value)
      std::pair<multiindex_t, NiceTensor<T>*&> operator*()
      {
         // Check if key is blacklisted, increase otherwise
         while( mRef.IsKeyBlacklisted(mRef.IdsToKeys(mKeyIds)) )
            ++ (*this);
         multiindex_t key=mRef.IdsToKeys(mKeyIds);
         return std::pair<multiindex_t, NiceTensor<T>*&>{key, mRef.mTensors[mKeyIds] };
      }
};

#endif /* TENSORLIBRARIAN_H_INCLUDED */
