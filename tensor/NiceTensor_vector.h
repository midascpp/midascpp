/**
 ************************************************************************
 * 
 * @file                NiceTensor_vector.h
 *
 * Created:             08.05.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               Operations with std::vector<NiceTensor>.
 * 
 * Last modified:       8th May 2015
 *
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef NICETENSOR_VECTOR_H_INCLUDED
#define NICETENSOR_VECTOR_H_INCLUDED

#include<vector>

#include "NiceTensor.h"

template <class T>
std::vector<NiceTensor<T>> operator+(const std::vector<NiceTensor<T>>&,
                                     const std::vector<NiceTensor<T>>&);

template <class T>
std::vector<NiceTensor<T>> operator*(const std::vector<NiceTensor<T>>&,
                                     const std::vector<NiceTensor<T>>&);

template <class T>
std::vector<NiceTensor<T>> operator*(const T,
                                     const std::vector<NiceTensor<T>>&);

template <class T>
std::vector<NiceTensor<T>> operator*(const std::vector<NiceTensor<T>>&,
                                     const T);

template <class T>
T dot_product(const std::vector<NiceTensor<T>>&,
              const std::vector<NiceTensor<T>>&);

template <class T>
std::ostream& write_NiceTensor_vector(const std::vector<NiceTensor<T>>&, std::ostream&);

template <class T>
std::vector<NiceTensor<T>> read_NiceTensor_vector(std::istream&);

template <template<class,class> class C, class T, class Alloc>
std::vector<NiceTensor<T>> linear_combination(
      const std::vector<T>& coeffs,
      const C<std::vector<NiceTensor<T>>,Alloc>& solutions
      );

#include"NiceTensor_vector_Impl.h"
#endif /* NICETENSOR_VECTOR_H_INCLUDED */
