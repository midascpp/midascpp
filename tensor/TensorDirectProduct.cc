#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "TensorDirectProduct.h"
#include "TensorDirectProduct_Impl.h"

// define
#define INSTANTIATE_TENSORDIRECTPRODUCT(T) \
template class TensorDirectProduct<T>;

// concrete instantiations
INSTANTIATE_TENSORDIRECTPRODUCT(float)
INSTANTIATE_TENSORDIRECTPRODUCT(double)
INSTANTIATE_TENSORDIRECTPRODUCT(std::complex<float>)
INSTANTIATE_TENSORDIRECTPRODUCT(std::complex<double>)

#undef INSTANTIATE_TENSORDIRECTPRODUCT

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
