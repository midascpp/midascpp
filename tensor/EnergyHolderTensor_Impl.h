/**
************************************************************************
* 
* @file    EnergyHolderTensor_Impl.h
*
* @date    18-01-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Tensor class for holding modal/orbital energies 
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef ENERGYHOLDERTENSOR_IMPL_H_INCLUDED
#define ENERGYHOLDERTENSOR_IMPL_H_INCLUDED

#include "tensor/SimpleTensor.h"
#include "tensor/Scalar.h"

#include "util/Math.h"

/**
 *
 **/
template
   <  class T
   >
ModalEnergyHolder<T>::ModalEnergyHolder
   (  const std::vector<In>& aModes
   ,  const DataCont& aEigVals
   ,  const std::vector<In>& aModalOffsets
   )
   :  mModes(aModes)
   ,  mEigVals(aEigVals)
   ,  mModalOffsets(aModalOffsets)
{
}

/**
 *
 **/
template
   <  class T
   >
T ModalEnergyHolder<T>::GetEnergy
   (  const std::vector<unsigned>& aIndices
   )  const
{
   assert(aIndices.size() == mModes.size());

   T result = static_cast<T>(0.);

   auto size = aIndices.size();
   Nb val = C_0;
   for(size_t i=0; i<size; ++i)
   {
      const auto& mode = this->mModes[i];
      mEigVals.DataIo(IO_GET, mModalOffsets[mode]+aIndices[i]+1, val);

      result += static_cast<T>(val);
   }

   return result;
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
template
   <  class... Us
   >
EnergyHolderTensorBase<T, IMPL>::EnergyHolderTensorBase
   (  const std::vector<unsigned>& aDims
   ,  bool aInverse
   ,  bool aSquare
   ,  T aShift
   ,  Us&&... aArgs
   )
   :  BaseTensor<T>(aDims)
   ,  IMPL<T>(std::forward<Us>(aArgs)...)
   ,  mInverse(aInverse)
   ,  mSquare(aSquare)
   ,  mShift(aShift)
{
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
typename BaseTensor<T>::typeID EnergyHolderTensorBase<T, IMPL>::Type() const
{
   return BaseTensor<T>::typeID::ENERGYHOLDER;
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
std::string EnergyHolderTensorBase<T, IMPL>::ShowType() const
{
   return std::string("EnergyHolderTensor");
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
T EnergyHolderTensorBase<T, IMPL>::Element
   (  const std::vector<unsigned>& aIndices
   )  const
{
   // Get energy
   T result = IMPL<T>::GetEnergy(aIndices) - this->mShift;

   // Invert
   if (  this->mInverse
      )
   {
      result = static_cast<T>(1.) / result;
   }

   // Square
   if (  this->mSquare
      )
   {
      result *= result;
   }

   return result;
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
EnergyHolderTensorBase<T, IMPL>* EnergyHolderTensorBase<T, IMPL>::Clone
   (
   )  const
{
   return new EnergyHolderTensorBase(*this);
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
typename BaseTensor<T>::real_t EnergyHolderTensorBase<T, IMPL>::Norm2
   (
   )  const
{
   T result = static_cast<T>(0.);
   std::vector<unsigned> idx(this->NDim(), 0);  // vector of zeros

   auto size = this->TotalSize();

   for(unsigned i=0; i<size; ++i)
   {
      result += std::pow(this->Element(idx), 2);

      // Increment idx
      for (int d = idx.size() - 1; d >= 0; --d)
      {
         if (++idx[d] == this->Extent(d))
         {
            idx[d] = 0;
         }
         else
         {
            break;
         }
      }
   }

   return result;
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
void EnergyHolderTensorBase<T, IMPL>::DumpInto
   (  T* ptr
   )  const
{
   std::vector<unsigned> idx(this->NDim(), 0);  // vector of zeros

   auto size = this->TotalSize();

   for(unsigned i=0; i<size; ++i)
   {
      ptr[i] = this->Element(idx);

      // Increment idx
      for (int d = idx.size() - 1; d >= 0; --d)
      {
         if (++idx[d] == this->Extent(d))
         {
            idx[d] = 0;
         }
         else
         {
            break;
         }
      }
   }
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
typename DotEvaluator<T>::dot_t EnergyHolderTensorBase<T, IMPL>::DotDispatch
   (  const CanonicalTensor<T>* const other
   )  const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this)); // args swapped
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
BaseTensor<T>* EnergyHolderTensorBase<T, IMPL>::ContractDown
   (  const unsigned int idx
   ,  const BaseTensor<T>* vect_in
   )  const
{
   try
   {
      // Ensure vect_in is a SimpleTensor instance
      const SimpleTensor<T>& vect = dynamic_cast<const SimpleTensor<T>&>(*vect_in);

      /* Compute output dimensions and initialize output tensor
       * new_dims = [ old_dims[:idx-1],  old_dims[idx+1:] ] */
      std::vector<unsigned int> newdims(this->NDim() - 1);
      unsigned counter = 0;
      for(unsigned i = 0; i < this->NDim(); ++i)
      {
         if(i != idx)
         {
            newdims[counter] = this->mDims[i];
            ++counter;
         }
      }
      SimpleTensor<T> *result = new SimpleTensor<T>(std::move(newdims));

      /* Carry out contraction */
      std::vector<unsigned> energy_indices(this->NDim(), 0); // vector of zeros
      T* tensout_row_start=result->GetData();

      auto before = BaseTensor<T>::NBefore(idx);
      auto after = BaseTensor<T>::NAfter(idx);

      for(int i_bef = 0; i_bef < before; ++i_bef)
      {
         const T* op = vect.GetData();
         T *tensout = nullptr;
         for(int i=0; i<this->mDims[idx]; ++i)
         {
            tensout = tensout_row_start;
            for(int i_aft = 0; i_aft < after; ++i_aft)
            {
               *(tensout++) += this->Element(energy_indices) * *op;
               
               // Increment energy index (quite inefficient)
               for (int d = energy_indices.size() - 1; d >= 0; --d)
               {
                  if (++energy_indices[d] == this->Extent(d))
                  {
                     energy_indices[d] = 0;
                  }
                  else
                  {
                     break;
                  }
               }
            }
            ++op;
         }
         tensout_row_start = tensout;
      }
      
      // if scalar we return as scalar
      if(result->NDim() == 0)
      {
         BaseTensor<T>* actual_result(new Scalar<T>(result->GetData()[0]));
         delete result;
         return actual_result;
      }
      else
      {
         return result;
      }
   }
   catch(const std::bad_cast&)
   {
      throw std::runtime_error("ERROR: In EnergyHolderTensorBase<T, IMPL>::ContractDown:  vect_in must be a SimpleTensor instance.");
   }
   catch(...)
   {
      throw std::runtime_error("CAUGHT SOMETHING IN EnergyHolderTensorBase<T, IMPL>::ContractDown()");
   }
};


#endif /* ENERGYHOLDERTENSOR_IMPL_H_INCLUDED */
