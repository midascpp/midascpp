#ifndef FITNCG_H_INCLUDED
#define FITNCG_H_INCLUDED

#include "tensor/CanonicalTensor_SolverWrapper.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/FitReport.h"
#include "tensor/CpOptimization.h"
#include "util/CallStatisticsHandler.h"
#include "libmda/numeric/float_eq.h"
   
/*!
 * Input handler for FitNCG function. Collects all settings for the CPGRAD algorithm.
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>*
   >
struct CpNCGInput
{
   //! Identifiers for different NCG methods
   enum class ncgType : int
   {
      HS = 0   ///< Hestenes-Stiefel
   ,  FR       ///< Fletcher-Reeves
   ,  PR       ///< Polak-Ribiere
   ,  FP       ///< FR-PR
   ,  DY       ///< Dai-Yuan
   ,  HZ       ///< Hager-Zhang
   };

   //! Identifiers for line-search types
   enum class lsType : int
   {
      WOLFE = 0   ///< Wolfe line search (Nocedal and Wright: Numerical Optimization, 2nd ed. Chapter 3.5)
   ,  EXACT       ///< Exact 3-PG line search
   ,  HZ          ///< Hager-Zhang
   };

   //! Maximum number of iterations
   unsigned mMaxiter = 10;

   // Iteration where we check the error against mMaxerr
   unsigned int mCheckIter = -1;

   // Break at iteration=mCheckIter if err > mCheckThresh, i.e. if we are too far from convergence.
   T mCheckThresh = 1.e-4;

   //! Maximum gradient norm
   T mMaxerr = 1e-6;

   //! Use diagonal preconditioner (only for specialised solver)
   bool mDiagonalPreconditioner = false;

   //! Balance vectors after each update
   bool mBalanceModeVectors = false;

   //! Interpolate step length
   bool mInterpolateStepLength = false;

   //! Use safe_diff_norm2 for final error calculation
   bool mSafeError = false;

   //! Type of NCG method (beta value)
   ncgType mNcgType = ncgType::HS;

   //! Value of theta for Hager-Zhang CG method (balance between conjugacy and descent)
   T mHagerZhangTheta = 2.;

   //! Use PRP+ method for mNcgType = ncgType::PR
   bool mPrpPlus = true;

   //! Type of line search
   lsType mLineSearch = lsType::EXACT;

   //! Max. step length
   T mLineSearchAlphaMax = 10.;

   //! Line-search resolution
   T mLineSearchAlphaResolution = 0.05;

   //! C1 parameter for Wolfe conditions
   T mWolfeConditionsC1 = 1.e-4;

   //! C2 parameter for Wolfe conditions
   T mWolfeConditionsC2 = 0.1;

   //! C parameter for 3-PG line search
   T m3pgC = 0.6;

   //! D parameter for 3-PG line search
   T m3pgD = 0.6;

   //! Max. iterations for 3-PG line search
   size_t m3pgMaxiter = 100;

   //! Relative threshold for exact line search
   T m3pgRelativeThreshold = 1.e-3;

   //! Converge on max. element of gradient (only for specialised solver)
   bool mMaxElementConvCheck = false;

   //! Allow restart of the NCG algorithm
   T mRestartThreshold = 1.e-1;

   //! Use the general solver framework
   bool mUseGeneralSolver = false;

   //! IoLevel
   In mIoLevel = I_1;
   
   /**
    * Default constructor. Many defaults are probably the same as in TensorDecompInfo for FINDBESTCPALS.
    **/
   CpNCGInput()
   {
   }

   /**
    * Constructor from TensorDecompInfo.
    **/
   CpNCGInput(const TensorDecompInfo& aInfo)
      :  mMaxiter                   (const_cast<TensorDecompInfo&>(aInfo)["CPNCGMAXITER"].get<In>())
      ,  mCheckIter                 (const_cast<TensorDecompInfo&>(aInfo)["CHECKITER"].get<In>())
      ,  mMaxerr                    (const_cast<TensorDecompInfo&>(aInfo)["CPNCGTHRESHOLD"].get<T>())
      ,  mDiagonalPreconditioner    (const_cast<TensorDecompInfo&>(aInfo)["CPNCGDIAGONALPRECONDITIONER"].get<bool>())
      ,  mBalanceModeVectors        (const_cast<TensorDecompInfo&>(aInfo)["CPNCGBALANCEMODEVECTORS"].get<bool>())
      ,  mNcgType                   (const_cast<TensorDecompInfo&>(aInfo)["CPNCGTYPE"].get<ncgType>())
      ,  mHagerZhangTheta           (const_cast<TensorDecompInfo&>(aInfo)["HAGERZHANGTHETA"].get<Nb>())
      ,  mLineSearch                (const_cast<TensorDecompInfo&>(aInfo)["CPNCGLINESEARCH"].get<lsType>())
      ,  mLineSearchAlphaMax        (const_cast<TensorDecompInfo&>(aInfo)["CPNCGALPHAMAX"].get<T>())
      ,  mLineSearchAlphaResolution (const_cast<TensorDecompInfo&>(aInfo)["CPNCGALPHARESOLUTION"].get<T>())
      ,  mWolfeConditionsC1         (const_cast<TensorDecompInfo&>(aInfo)["CPNCGWOLFEC1"].get<T>())
      ,  mWolfeConditionsC2         (const_cast<TensorDecompInfo&>(aInfo)["CPNCGWOLFEC2"].get<T>())
      ,  m3pgRelativeThreshold      (const_cast<TensorDecompInfo&>(aInfo)["CPNCG3PGRELTHRESHOLD"].get<T>())
      ,  mMaxElementConvCheck       (const_cast<TensorDecompInfo&>(aInfo)["CPNCGMAXELEMENTCONVCHECK"].get<bool>())
      ,  mRestartThreshold          (const_cast<TensorDecompInfo&>(aInfo)["CPNCGRESTART"].get<Nb>())
      ,  mUseGeneralSolver          (const_cast<TensorDecompInfo&>(aInfo)["CPNCGGENERALSOLVER"].get<bool>())
      ,  mIoLevel                   (const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].get<In>())
   {
   }

#define SETTER(NAME, TYPE) \
   CpNCGInput& Set##NAME(TYPE a##NAME) \
   { \
      m##NAME = a##NAME; \
      return *this; \
   }
   
   SETTER(Maxiter, unsigned)
   SETTER(Maxerr, T)
   SETTER(CheckThresh, Nb)
   SETTER(DiagonalPreconditioner, bool)
   SETTER(BalanceModeVectors, bool)
   SETTER(InterpolateStepLength, bool)
   SETTER(NcgType, ncgType)
   SETTER(HagerZhangTheta, T)
   SETTER(LineSearch, lsType)
   SETTER(LineSearchAlphaMax, T)
   SETTER(LineSearchAlphaResolution, T)
   SETTER(WolfeConditionsC1, T)
   SETTER(WolfeConditionsC2, T)
   SETTER(3pgRelativeThreshold, T)
   SETTER(MaxElementConvCheck, bool)
   SETTER(RestartThreshold, T)
   SETTER(UseGeneralSolver, bool)
   SETTER(IoLevel, In)

#undef SETTER
};

namespace midas
{
namespace tensor
{

/**
 * Fit a tensor to CP format using the general solver framework.
 *
 * @param target_tensor  The target tensor.
 * @param aTargetNorm2
 * @param fitting_tensor The fitting tensor.
 * @param input          Input for the algorithm.
 *
 * @return               Returns a fitting report.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> GeneralSolverCpNcg
   (  const BaseTensor<T>* const target_tensor
   ,  const T aTargetNorm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpNCGInput<T>& input
   )
{
   switch(  input.mNcgType  )
   {
      case CpNCGInput<T>::ncgType::HS:
      {
         *fitting_tensor = hestenes_stiefel
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      case CpNCGInput<T>::ncgType::FR:
      {
         *fitting_tensor = fletcher_reeves
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      case CpNCGInput<T>::ncgType::PR:
      {
         *fitting_tensor = polak_ribiere
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      case CpNCGInput<T>::ncgType::FP:
      {
         *fitting_tensor = fr_pr
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      case CpNCGInput<T>::ncgType::DY:
      {
         *fitting_tensor = dai_yuang
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      case CpNCGInput<T>::ncgType::HZ:
      {
         *fitting_tensor = hager_zhang
                              (  CTEvaluator<BaseTensor<T>, T>(*target_tensor, aTargetNorm2)
                              ,  CanonicalTensor_SolverWrapper<T>(*fitting_tensor)
                              ,  input.mMaxerr
                              ,  false
                              ).mTensor;
         break;
      }
      default:
      {
         MIDASERROR("GeneralSolverCpNcg: No match for ncgType.");
      }
   }

   // We only have information on the final error.
   return FitReport<T>{diff_norm_new(fitting_tensor, fitting_tensor->Norm2(), target_tensor, aTargetNorm2), std::sqrt(aTargetNorm2), 0};
}


/**
 * Fit a tensor to CP format using specialised implementation.
 *
 * @param target_tensor  The target tensor.
 * @param aTargetNorm2
 * @param fitting_tensor The fitting tensor.
 * @param input          Input for the algorithm.
 *
 * @return               Returns a fitting report.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> SpecialisedCpNcg
   (  const BaseTensor<T>* const target_tensor
   ,  const T aTargetNorm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpNCGInput<T>& input
   )
{
   T target_norm2 = aTargetNorm2 < C_0 ? static_cast<T>(0.) : aTargetNorm2;

   // Setup variables
   const auto& dims = fitting_tensor->GetDims();
   unsigned ndim = fitting_tensor->NDim();
   unsigned rank = fitting_tensor->GetRank();
   unsigned rank2 = rank*rank;
   unsigned maxiter = input.mMaxiter;
   unsigned checkiter = input.mCheckIter;
   T checkthresh = input.mCheckThresh;
   T target_norm = std::sqrt(target_norm2);
   bool balance = input.mBalanceModeVectors;
   bool interpolate_alpha = input.mInterpolateStepLength;
   bool precon = input.mDiagonalPreconditioner;
   T maxerr = input.mMaxerr;
   T alpha = static_cast<T>(1.);
   T alpha_old = static_cast<T>(1.);
   T alpha_trial = static_cast<T>(1.);
   T dg_dot_new = static_cast<T>(-1.);
   T dg_dot_old = static_cast<T>(-1.);
   T beta = static_cast<T>(0.);
   bool converged = false;
   bool grad_conv_check = false;
   bool restart = false;
   T restart_thresh = input.mRestartThreshold;
   bool allow_restart = restart_thresh > C_0;
   In io = input.mIoLevel;

   // Swap after line search if we a sure the line search evaluates the final trial vector and gradient
   bool swap_after_line_search   =  input.mLineSearch == CpNCGInput<T>::lsType::EXACT;

   // Setup some working space
   detail::CpOptWorkspace<T> workspace(target_tensor, fitting_tensor);
   {
   //LOGCALL("first intermeds and gradient");
   workspace.UpdateIntermediatesAndGradient();
   }

   // Initialize search directions and gradients
   auto g_new = std::unique_ptr<CanonicalTensor<T>>(workspace.mGradWork->Clone());
   auto g_old = std::unique_ptr<CanonicalTensor<T>>(g_new->Clone());
   auto d_new = std::unique_ptr<CanonicalTensor<T>>(g_new->Clone());
   d_new->ScaleModeMatrices(static_cast<T>(-1.));

   // Precondition initial direction
   if (  precon   )
   {
      workspace.mGammaIntermeds->DiagonalTransformation(d_new.get(), true);
   }

   // Initialize gradient norms and errors
   T grad_norm2 = g_new->ElementwiseNorm2();
   T grad_norm = std::sqrt(grad_norm2);
   T grad_max_elem = g_new->MaxModeMatrixElement().second;

   T err_new = aTargetNorm2 < C_0 ? static_cast<T>(0.) : target_norm;
   T err_old = aTargetNorm2 < C_0 ? static_cast<T>(0.) : target_norm;
   T err_change = static_cast<T>(0.);

   // Check gradient convergence
   grad_conv_check   =  input.mMaxElementConvCheck
                     ?  grad_max_elem < maxerr
                     :  grad_norm < maxerr;

   // If gradient is converged, check error
   if (  grad_conv_check
      )
   {
      // Calculate fitting_tensor->Norm2() from gamma intermeds
      auto fitting_norm2 = workspace.mGammaIntermeds->FullGammaMatrixNorm2(workspace.mGammaWork);

      // Calculate diff norm from squeezer dot
      err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, workspace.mSqueezer->Dot());
      
      if (  (  err_new < maxerr
            && aTargetNorm2 > C_0
            )
         || libmda::numeric::float_numeq_zero(grad_norm, 1.)
         )
      {
         converged = true;
         return FitReport<T>  {  err_new                 // Diff norm
                              ,  target_norm             // Target norm
                              ,  0                       // Number of iterations
                              ,  0.                      // Error change
                              ,  grad_norm               // Gradient norm
                              ,  converged               // Converged
                              };
      }
   }

   // Begin iteration loop
   unsigned iter = 0;
   for(iter=0; iter<maxiter; ++iter)
   {
      if (  io >= I_10
         )
      {
         Mout << "|===== FitNCG iteration " << iter << " =====|" << std::endl;
      }

      /****** Perform line search ******/
      {
      //LOGCALL("line search");
      dg_dot_new = d_new->ElementwiseDot(*g_new);
      alpha_trial = alpha_old;

      // Interpolate trial alpha
      if (  interpolate_alpha
         )
      {
         T scalar = dg_dot_old / dg_dot_new;
         alpha_trial *= scalar;
      }

      // Perform line search
      alpha = detail::CpOptLineSearch(target_tensor, target_norm2, fitting_tensor, d_new.get(), alpha_trial, dg_dot_new, input, workspace);

      alpha_old = alpha;
      dg_dot_old = dg_dot_new;
      }

      if (  io >= I_10
         )
      {
         Mout << "   alpha       : " << alpha << std::endl;
      }

      /****** Update fit and gradient ******/
      // If workspace already holds the new gradient and fitting_tensor, we simply swap
      if (  swap_after_line_search
         )
      {
         *fitting_tensor = *workspace.mTrialWork;

         {
         //LOGCALL("swap gradients");
         std::swap(g_old, g_new);
         std::swap(g_new, workspace.mGradWork);
         }
      }
      // Else, we need to update fit and calculate gradient
      else
      {
         {
         //LOGCALL("update fit");
         fitting_tensor->ElementwiseAxpy(*d_new, alpha);
         *workspace.mTrialWork = *fitting_tensor;  // workspace.mTrialWork must be = fitting_tensor for gradient calculation
         }

         {
         //LOGCALL("new gradient");
         std::swap(g_old, g_new);
         workspace.UpdateIntermediatesAndGradient();
         std::swap(g_new, workspace.mGradWork);
         }
      }

      // Calculate norms
      grad_norm2 = g_new->ElementwiseNorm2();
      grad_norm = std::sqrt(grad_norm2);
      grad_max_elem = g_new->MaxModeMatrixElement().second;

      // Calculate error
      if (  input.mSafeError  )
      {
         //LOGCALL("calculate safe error");

         // Calculate safe diff norm
         err_new = std::sqrt(safe_diff_norm2(fitting_tensor, target_tensor));
      }
      else
      {
         //LOGCALL("calculate error");

         // Calculate fitting_tensor->Norm2() from gamma intermeds
         auto fitting_norm2 = workspace.mGammaIntermeds->FullGammaMatrixNorm2(workspace.mGammaWork);

         // Calculate diff norm from squeezer dot
         err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, workspace.mSqueezer->Dot());
      }

      err_change = std::fabs(err_new - err_old);
      err_old = err_new;

      // Test for nan
      if (  err_new != err_new  )
      {
         MidasWarning("Bad CP-NCG fit. Error evaluates to " + std::to_string(err_new));
      }
      if (  grad_norm != grad_norm
         )
      {
         MidasWarning("CP-NCG gradient norm evaluates to " + std::to_string(grad_norm));
         break;
      }

      if (  io >= I_10
         )
      {
         Mout << "   |g_new|     : " << grad_norm << std::endl;
         Mout << "   err_new     : " << err_new << std::endl;
         Mout << "   err_change  : " << err_change << std::endl;
      }

      /****** Balance fitting_tensor ******/
      if (  balance
         )
      {
         fitting_tensor->BalanceModeVectors();
      }


      /****** Check convergence ******/
      grad_conv_check   =  input.mMaxElementConvCheck
                        ?  grad_max_elem < maxerr
                        :  grad_norm < maxerr;

      if (  iter > 0
         && grad_conv_check
         && err_change < maxerr
         )
      {
         converged = true;
         break;
      }

      // Break the iterations at iteration aInfo.mCheckIter
      // if the error is too much larger than the threshold.
      if (  iter == checkiter   
         && err_new > checkthresh
         )
      {
         break;
      }

      /****** Setup new search direction if not converged ******/
      if (  !converged  )
      {
         /****** Check restart ******/
         // Restart if cos to angle between g_old and g_new is larger than 0.1 (gradients are far from orthogonal)
         restart  =  allow_restart
                  && libmda::numeric::float_geq(std::fabs(g_new->ElementwiseDot(*g_old))/grad_norm2, restart_thresh);

         /****** Calculate beta ******/
         // NB: this overwrites workspace.mGradWork
         if (  !restart )
         {
            //LOGCALL("beta");
            beta = detail::CpNcgBeta(g_new.get(), g_old.get(), d_new.get(), input, workspace);
         }
         else
         {
            beta = static_cast<T>(0.);
         }

         if (  io >= I_10
            )
         {
            Mout << "   restart     : " << std::boolalpha << restart << std::noboolalpha << std::endl;
            Mout << "   beta        : " << beta << std::endl;
         }
            

         /****** Update search direction ******/
         // NB: To save memory: Scale first, then subtract g_new
         {
         //LOGCALL("construct new direction");
         d_new->ScaleModeMatrices(beta);

         if (  precon   )
         {
            // We need to save an intermediate if we precondition. Use workspace.mGradWork.
            auto& pgrad = workspace.mGradWork;
            *pgrad = *g_new;
            workspace.mGammaIntermeds->DiagonalTransformation(pgrad.get(), true);

            d_new->ElementwiseAxpy(*pgrad, static_cast<T>(-1.));
         }
         else
         {
            d_new->ElementwiseAxpy(*g_new, static_cast<T>(-1.));
         }
         }
      }
   }


   return FitReport<T>  {  err_new                 // Diff norm
                        ,  target_norm             // Target norm
                        ,  iter                    // Number of iterations
                        ,  err_change              // Error change
                        ,  grad_norm               // Gradient norm
                        ,  converged               // Converged
                        };
}

} /* namespace tensor */
} /* namespace midas */


/**
 * Fit a CanonicalTensor to a target tensor using a non-linear conjugated-gradient algorithm.
 * 
 * @param target_tensor  The target tensor.
 * @param aTargetNorm2
 * @param fitting_tensor The fitting tensor.
 * @param input          Input for the algorithm.
 *
 * @return               Returns a fitting report.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitNCG
   (  const BaseTensor<T>* const target_tensor
   ,  const T aTargetNorm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpNCGInput<T>& input
   )
{
   //LOGCALL("calls");
   assert_same_shape(target_tensor, fitting_tensor);
   if(fitting_tensor->GetRank() == 0)
   {
      auto norm2 = aTargetNorm2 < static_cast<T>(0.) ? static_cast<T>(0.) : aTargetNorm2;
      return FitReport<T>{std::sqrt(norm2), std::sqrt(norm2), 0};
   }

   bool general_solver = input.mUseGeneralSolver;

   if (  general_solver )
   {
      return midas::tensor::GeneralSolverCpNcg(target_tensor, aTargetNorm2, fitting_tensor, input);
   }
   else
   {
      return midas::tensor::SpecialisedCpNcg(target_tensor, aTargetNorm2, fitting_tensor, input);
   }
}


#endif /* FITNCG_H_INCLUDED */
