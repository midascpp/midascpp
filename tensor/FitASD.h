/**
************************************************************************
* 
* @file    FitASD.h
*
* @date    11-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Functions for fitting CP tensors using the (Pivotised) Alternating 
*     Steepest Descent method.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef FITASD_H_INCLUDED
#define FITASD_H_INCLUDED

#include "tensor/FitReport.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/GammaMatrix.h"
#include "tensor/Squeezer.h"
#include "tensor/CanonicalTensor_Fits.h"
#include "lapack_interface/math_wrappers.h"

#include "util/CallStatisticsHandler.h"

/*!
 * Input handler for FitASD and FitPASD functions. Collects all settings for the CP-(P)ASD algorithm.
 */
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
struct CpAsdInput
{
   //! Maximum iterations
   unsigned int mMaxiter = 10;

   //! Iteration where we check the error against mMaxerr
   unsigned int mCheckIter = -1;

   //! Break at iteration=mCheckIter if err > mCheckThresh, i.e. if we are too far from convergence.
   T mCheckThresh = 1.e-4;

   //! Maximum error (gradient norm)
   T mMaxerr = 1e-6;

   //! Use norm of mode matrix instead of max. element to determine pivot.
   bool mModeMatrixNormPivot = false;

   //! Use diagonal preconditioner (inverse gamma-matrix diagonal)
   bool mDiagonalPreconditioner = false;

   //! Balance mode vectors after each update
   bool mBalanceModeVectors = false;

   //! IoLevel
   In mIoLevel = I_1;

   /**
    * Default constructor.
    **/
   CpAsdInput()
   {
   }

   /**
    * Constructor from TensorDecompInfo.
    **/
   CpAsdInput(const TensorDecompInfo& aInfo)
      :  mMaxiter                (const_cast<TensorDecompInfo&>(aInfo)["CPASDMAXITER"].get<In>())
      ,  mCheckIter              (const_cast<TensorDecompInfo&>(aInfo)["CHECKITER"].get<In>())
      ,  mMaxerr                 (const_cast<TensorDecompInfo&>(aInfo)["CPASDTHRESHOLD"].get<Nb>())
      ,  mModeMatrixNormPivot    (const_cast<TensorDecompInfo&>(aInfo)["CPPASDMODEMATRIXNORMPIVOT"].get<bool>())
      ,  mDiagonalPreconditioner (const_cast<TensorDecompInfo&>(aInfo)["CPASDDIAGONALPRECONDITIONER"].get<bool>())
      ,  mBalanceModeVectors     (const_cast<TensorDecompInfo&>(aInfo)["CPPASDBALANCEMODEVECTORS"].get<bool>())
      ,  mIoLevel                (const_cast<TensorDecompInfo&>(aInfo)["IOLEVEL"].get<In>())
   {
   }

#define SETTER(NAME, TYPE) \
   CpAsdInput& Set##NAME(TYPE a##NAME) \
   { \
      m##NAME = a##NAME; \
      return *this; \
   }
   
   SETTER(CheckThresh, Nb)
   SETTER(Maxiter, unsigned)
   SETTER(Maxerr, T)
   SETTER(ModeMatrixNormPivot, bool)
   SETTER(DiagonalPreconditioner, bool)
   SETTER(BalanceModeVectors, bool)
   SETTER(IoLevel, In)

#undef SETTER
};

/**
 * Calculate denominator for ASD step size.
 *
 * @param rank          Rank of fitting tensor
 * @param extent        Extent of the current dimension
 * @param gradient_ptr  Pointer to gradient block of current dimension
 * @param gamma         GammaMatrix
 * @param work          Working space
 * @return
 *    Overlap between gradient and transformed gradient <grad|gamma|grad>.
 **/
template
   <  class T
   ,  class SmartPtr
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
T AsdStepSizeDenominator
   (  unsigned rank
   ,  unsigned extent
   ,  T* gradient_ptr
   ,  const SmartPtr& gamma
   ,  SmartPtr& work
   )
{
   T* gradient_ref = gradient_ptr;
   T* gamma_ptr = gamma.get();
   T* gamma_ref = gamma_ptr;
   T* work_ptr = work.get();
   T* work_ref = work_ptr;

   char nc = 'N';
   T alpha = static_cast<T>(1.);
   T beta = static_cast<T>(0.);
   int m = extent;
   int n = rank;
   int k = rank;
   midas::lapack_interface::gemm
      (  &nc
      ,  &nc
      ,  &m
      ,  &n
      ,  &k
      ,  &alpha
      ,  gradient_ptr
      ,  &m
      ,  gamma_ptr
      ,  &n
      ,  &beta
      ,  work_ptr
      ,  &m
      );

   // Reset pointers
   gradient_ptr = gradient_ref;
   work_ptr = work_ref;

   // Calculate dot product
   T result = static_cast<T>(0.);
   auto size = rank*extent;

   for(size_t i=0; i<size; ++i)
   {
      result += *(gradient_ptr++) * *(work_ptr++);
   }

   return result;
}


/**
 * Fit a target tensor to CP format using the PASD algorithm.
 *
 * @param target_tensor       The target tensor.
 * @param target_norm2        Squared norm of the target tensor.
 * @param fitting_tensor      Fitting tensor (guess).
 * @param input               CpAsdInput struct.
 *
 * @return
 *    FitReport of the obtained fit.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitPASD
   (  const BaseTensor<T>* const target_tensor
   ,  const T target_norm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpAsdInput<T>& input
   )
{
   //#####################################
   //# initilalization
   //#####################################
   //LOGCALL("calls");
   assert_same_shape(fitting_tensor, target_tensor);

   T target_norm = std::sqrt(target_norm2);
   if(fitting_tensor->GetRank() == 0)
   {
      return FitReport<T>{target_norm, target_norm, 0};
   }

   // Initialize iteration variables
   unsigned iter=0;
   unsigned maxiter = input.mMaxiter;
   unsigned checkiter = input.mCheckIter;
   T checkthresh = input.mCheckThresh;
   T maxerr = input.mMaxerr;
   T grad_norm = static_cast<T>(1.);
   std::pair<unsigned, T> pivot = { 0, static_cast<T>(0.) };
   bool norm2_based_pivot = input.mModeMatrixNormPivot;
   T step_size = static_cast<T>(0.);
   T transformed_overlap = static_cast<T>(1.);
   const auto& dims = fitting_tensor->GetDims();
   unsigned rank = fitting_tensor->GetRank();
   unsigned rank2 = rank*rank;
   unsigned max_extent = fitting_tensor->MaxExtent();
   bool precon = input.mDiagonalPreconditioner;
   bool balance = input.mBalanceModeVectors;
   bool converged = false;
   In io = input.mIoLevel;

   // Initialize intermediate machines
   GeneralGammaIntermediates<T> gamma_intermeds(fitting_tensor, fitting_tensor);
   Squeezer<T> squeezer(fitting_tensor, target_tensor);

   // Allocate work memory for step-size calculation
   int work_size = rank * max_extent; // Must be able to hold largest mode matrix of gradient
   auto work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(work_size);
   int gamma_work_size = rank*rank; // Must be able to hold gamma matrix
   auto gamma_work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(gamma_work_size);

   // Calculate first gradient
   CanonicalTensor<T> gradient(dims, rank);
   fitting_tensor->Gradient(gradient, gamma_intermeds, squeezer, gamma_work, work);

   grad_norm = gradient.ElementwiseNorm();

   T err_new = target_norm;
   T err_old = target_norm;
   T err_change = static_cast<T>(1.);
   T dist_new = static_cast<T>(0.);
   T dist_old = std::sqrt(static_cast<T>(1.)+err_old*err_old);
   T dist_rel_change = static_cast<T>(0.);

   // If the gradient norm is small enough compared to target_norm2, then quit!
   if (  grad_norm < maxerr*target_norm2
      && libmda::numeric::float_pos(target_norm2)
      )
   {
      if (  io >= I_10  )
      {
         Mout << "CP-PASD: First gradient norm = " << grad_norm << ". Quit!" << std::endl;
      }
      return FitReport<T>(diff_norm_new(fitting_tensor, fitting_tensor->Norm2(), target_tensor, target_norm2, squeezer.Dot()), target_norm, iter);
   }


   //#####################################
   //# start iteration loop
   //#####################################
   for(iter = 0; iter < maxiter; ++iter)
   {
      //LOGCALL("iteration");

      // Find max. gradient block
      pivot =  norm2_based_pivot 
            ?  gradient.MaxModeMatrixNorm2()
            :  gradient.MaxModeMatrixElement();

      const auto& max_dim = pivot.first;
      const auto& pivot_value = pivot.second;
      const auto& extent = fitting_tensor->Extent(max_dim);

      if (  io >= I_10
         )
      {
         Mout  << "|===== FitPASD iteration " << iter << " =====|\n"
               << " Update mode:       " << max_dim << "\n"
               << " Extent:            " << extent << "\n"
               << " Pivot value:       " << pivot_value << "\n"
               << std::flush;
      }
      

      // Calculate step size
      {
      //LOGCALL("calculate step size");
      if (  precon   )
      {
         // Transform gradient mode matrix and return overlap (which is the numerator of the step size)
         step_size = gamma_intermeds.DiagonalTransformation(max_dim, rank, extent, gradient.GetModeMatrices()[max_dim], true);
      }
      else
      {
         step_size   =  norm2_based_pivot
                     ?  pivot_value
                     :  gradient.ModeMatrixDot(max_dim, &gradient);
      }

      // Get gamma matrix
      gamma_intermeds.GammaMatrix(max_dim, gamma_work);

      // Transform gradient block
      transformed_overlap = AsdStepSizeDenominator(rank, extent, gradient.GetModeMatrices()[max_dim], gamma_work, work);

      step_size /= transformed_overlap;
      }

      if (  io >= I_10
         )
      {
         Mout  << " Step size:      " << step_size << std::endl;
      }

      // Update the mode matrix
      {
      //LOGCALL("update mode matrix");
      fitting_tensor->ModeMatrixAxpy(max_dim, gradient, -step_size);
      }

      // Update intermediates (or balance tensor and recalculate intermediates)
      if (  balance  )
      {
         {
         //LOGCALL("balance tensor");
         fitting_tensor->BalanceModeVectors();
         }
         {
         //LOGCALL("recalculate intermediates");
         gamma_intermeds.ReCalculate(fitting_tensor, fitting_tensor);
         squeezer.ReCalculate();
         }
      }
      else
      {
         //LOGCALL("update intermediates");
         gamma_intermeds.Update(max_dim, fitting_tensor, fitting_tensor);
         squeezer.Update(max_dim);
      }
      
      // Update gradient
      //prev_grad_norm = grad_norm;
      {
      //LOGCALL("new gradient");
      fitting_tensor->Gradient(gradient, gamma_intermeds, squeezer, gamma_work, work);
      }
      grad_norm = gradient.ElementwiseNorm();

      // Calculate error
      {
         auto fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma_work);
         err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
         err_change = std::fabs(err_new - err_old);

         dist_new = std::sqrt(static_cast<T>(1.) + err_new*err_new);
         dist_rel_change = std::fabs(dist_old - dist_new) / dist_new;
         
         // test for NaN
         if (  err_new != err_new
            || dist_new != dist_new
            )
         {
            MidasWarning(" WARNING bad CP-PASD fit. ");
         }

         err_old = err_new;
         dist_old = dist_new;
      }

      if (  io >= I_10
         )
      {
         Mout  << " err_new:           " << err_new << "\n"
               << " err_change:        " << err_change << "\n"
               << " grad_norm:         " << grad_norm << "\n"
               << " dist_new:          " << dist_new << "\n"
               << " dist_rel_change:   " << dist_rel_change << "\n"
               << std::flush;
      }


      // Check convergence
      if (  iter > 0
         && grad_norm < maxerr
         && (  err_change < maxerr
            || dist_rel_change < maxerr
            )
         )
      {
         converged = true;
         break;
      }

      // Break the iterations at iteration aInfo.mCheckIter
      // if the error is too much larger than the threshold.
      if (  iter == checkiter   
         && err_new > checkthresh
         )
      {
         break;
      }
   }

   return FitReport<T>
               {  err_new
               ,  target_norm
               ,  iter
               ,  err_change
               ,  grad_norm
               ,  converged
               };
}



/**
 * Fit at tensor to CP format using Alternating Steepest Descent.
 *
 * @param target_tensor       The target tensor
 * @param target_norm2        Squared norm of target tensor
 * @param fitting_tensor      Fitting tensor (guess)
 * @param input               CpAsdInput struct
 * @return
 *    FitReport of the obtained fit.
 **/
template
   <  class T
   ,  midas::type_traits::DisableIfComplexT<T>* = nullptr
   >
FitReport<T> FitASD
   (  const BaseTensor<T>* const target_tensor
   ,  const T target_norm2
   ,  CanonicalTensor<T>* const fitting_tensor
   ,  const CpAsdInput<T>& input
   )
{
   //#####################################
   //# initilalization
   //#####################################
   //LOGCALL("calls");
   assert_same_shape(fitting_tensor, target_tensor);

   auto ndim = fitting_tensor->NDim();
   auto norm = std::sqrt(target_norm2);
   if(fitting_tensor->GetRank() == 0)
   {
      return FitReport<T>{norm, norm, 0};
   }
   
   // initialize iteration variables
   unsigned iter;
   unsigned maxiter = input.mMaxiter;
   unsigned checkiter = input.mCheckIter;
   T checkthresh = input.mCheckThresh;
   T maxerr = input.mMaxerr;
   bool converged = false;
   T err_old = norm;
   T err_new = static_cast<T>(0.);
   T err_change = static_cast<T>(0.);
   T step_size = static_cast<T>(1.);
   T transformed_overlap = static_cast<T>(1.);
   unsigned rank = fitting_tensor->GetRank();
   unsigned rank2 = rank*rank;
   unsigned max_extent = fitting_tensor->MaxExtent();
   bool precon = input.mDiagonalPreconditioner;

   // Initialize intermediate machines
   GeneralGammaIntermediates<T> gamma_intermeds(fitting_tensor, fitting_tensor);
   Squeezer<T> squeezer(fitting_tensor, target_tensor);

   // Allocate work memory for step-size calculation
   int work_size = rank * max_extent; // Must be able to hold largest mode matrix of gradient
   auto work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(work_size);
   auto gradient_work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(work_size);
   int gamma_work_size = rank*rank; // Must be able to hold gamma matrix
   auto gamma_work = fitting_tensor->GetAllocatorTp().AllocateUniqueArray(gamma_work_size);

   //#####################################
   //# begin iteration
   //#####################################
   for(iter = 0; iter<maxiter; ++iter)
   {
      //LOGCALL("loop");

      /* Begin dim loop */
      for(unsigned idim = 0; idim < ndim; ++idim)
      {
         const auto& extent = fitting_tensor->Extent(idim);

         //LOGCALL("dim loop");
         // Squeezer (save result in gradient_work)
         {
            //LOGCALL("squeeze");
            squeezer.SqueezeModeMatrix(idim, gradient_work, true);
         }
         // Gamma matrix (save result in gamma work)
         {
            //LOGCALL("gamma");
            gamma_intermeds.GammaMatrix(idim, gamma_work);
         }
         // Gradient block (save result in gradient_work)
         {
            //LOGCALL("gradient block");
            fitting_tensor->Gradient(idim, gamma_work, gradient_work);
         }

         // Calculate step size
         {
            //LOGCALL("calculate step size");
            if (  precon   )
            {
               // Precondition the gradient and return overlap (which is the numerator of the step size)
               step_size = gamma_intermeds.DiagonalTransformation(idim, rank, extent, gradient_work.get(), true);
            }
            else
            {
               // Simply calculate overlap
               step_size = static_cast<T>(0.);
               auto size = extent*rank;
               for(unsigned i=0; i<size; ++i)
               {
                  step_size += gradient_work[i]*gradient_work[i];
               }
            }

            // Transform gradient block
            transformed_overlap = AsdStepSizeDenominator(rank, extent, gradient_work.get(), gamma_work, work);

            step_size /= transformed_overlap;
         }

         // Update mode matrix
         {
            //LOGCALL("mode-matrix update");
            fitting_tensor->ModeMatrixAxpy(idim, gradient_work.get(), -step_size, false);
         }

         // Update intermediates
         {
            //LOGCALL("gamma update");
            gamma_intermeds.Update(idim, fitting_tensor, fitting_tensor);
         }
         {
            //LOGCALL("squeezer update");
            squeezer.Update(idim);
         }
      }
      /* End dim loop */


      // Calculate error
      {
         //LOGCALL("calculate error");
         auto fitting_norm2 = gamma_intermeds.FullGammaMatrixNorm2(gamma_work);
         err_new = diff_norm_new(fitting_tensor, fitting_norm2, target_tensor, target_norm2, squeezer.Dot());
         err_change = std::fabs(err_old - err_new);
         
         if((err_new != err_new)) // test for NaN
         {
            MidasWarning(" WARNING bad CP-ASD fit. ");
         }
         err_old = err_new;
      }

      // Check convergence
      if (  iter > 0
         && err_change < maxerr
         )
      {
         converged = true;
         break;
      }

      // Break the iterations at iteration aInfo.mCheckIter
      // if the error is too much larger than the threshold.
      if (  iter == checkiter   
         && err_new > checkthresh
         )
      {
         break;
      }
   }

   return FitReport<T>(err_new, norm, iter, err_change, static_cast<T>(0.), converged);
}

#endif /* FITASD_H_INCLUDED */
