#ifndef TENSORSUM_DECL_H_INCLUDED
#define TENSORSUM_DECL_H_INCLUDED

#include <string>

#include "BaseTensor.h"

/**
 * Class to handle a sum of tensors.
 **/
template
   <  class T
   >
class TensorSum
   :  public BaseTensor<T>
{
   public:
      friend DotEvaluator<T>;

      using dot_t = typename DotEvaluator<T>::dot_t;

      using real_t = typename BaseTensor<T>::real_t;

   private:
      std::vector<T> mCoefs;
      std::vector<std::unique_ptr<BaseTensor<T> > > mTensors;
      
   private:
      

      
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>& indicespos_inner_left
         ,  const std::vector<unsigned int>& indicespos_outer_left
         ,  const BaseTensor<T>&             right
         ,  const std::vector<unsigned int>& indicespos_inner_right
         ,  const std::vector<unsigned int>& indicespos_outer_right
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indicespos_result_left
         ,  const std::vector<unsigned int>& indicespos_result_right
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override
      {
         MIDASERROR("NOT IMPLEMENTED");
         return nullptr;
      }

      BaseTensor<T>* ContractInternal_impl
         (  const std::vector<std::pair<unsigned int, unsigned int>>&
         ,  const std::vector<unsigned int>&
         )  const override
      {
         MIDASERROR("NOT IMPLEMENTED");
         return nullptr;
      }

      BaseTensor<T>* Reorder_impl
         (  const std::vector<unsigned int>& neworder
         ,  const std::vector<unsigned int>& newdims
         )  const override
      {
         MIDASERROR("NOT IMPLEMENTED");
         return nullptr;
      }

      //! Called by Slice().
      BaseTensor<T>* Slice_impl
         (  const std::vector<std::pair<unsigned int,unsigned int>>&
         )  const override
      {
        MIDASERROR("NOT IMPLEMENTED");
         return nullptr;
      }

      //!
      void ConjugateImpl() override;
      
   public:
      /******************************** C-TORS ********************************/
      TensorSum(const std::vector<unsigned>&);
      
      //TensorSum(const std::vector<unsigned>&, std::vector<std::unique_ptr<BaseTensor<T> > >&&, std::vector<T>&&);

      /******************************** BASIC  ********************************/
      //! Type
      typename BaseTensor<T>::typeID Type() const override;

      //! Type as string
      std::string ShowType() const override;

      //! Clone
      BaseTensor<T>* Clone() const override;
      
      //! Dump tensor into pointer
      void DumpInto(T*) const override;

      //! Construct mode matrices
      BaseTensorPtr<T> ConstructModeMatrices() const;

      /******************************** QUERY  ********************************/
      //! Rank
      unsigned Rank() const;

      //! Norm2
      real_t Norm2() const override;

      //! Scale
      void Scale(const T&) override;

      //! Element
      T Element
         (  const std::vector<unsigned>&
         )  const override;
      
      //! Are all tensors canonical
      bool AllCanonical() const; 

      //! Does the sum contain both simple and canonical
      bool SimpleAndCanonical() const;

      //! Sanity check
      bool SanityCheck() const override;
      
      const std::vector<std::unique_ptr<BaseTensor<T> > >& GetTensors() const { return mTensors; }
      const std::vector<T>& GetCoefs() const { return mCoefs; }
      
      /******************************** CONTRACTIONS ********************************/
      //! Contract one dimension away with a vector. See BaseTensor::ContractDown.
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const override { MIDASERROR("NOT IMPLEMENTED"); return nullptr; }
      //! Transform a dimension with a matrix. See BaseTensor::ContractForward.
      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override { MIDASERROR("NOT IMPLEMENTED"); return nullptr; }
      //! Insert a dimension with a vector. See BaseTensor::ContractUp.
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override { MIDASERROR("NOT IMPLEMENTED"); return nullptr; }
      
      /******************************** OPERATORS ********************************/
      //! Scaling (in place).
      BaseTensor<T>* operator*= (const T) override { MIDASERROR("NOT IMPLELEMTED"); return nullptr; }
      //! Add another tensor (in place).
      BaseTensor<T>* operator+=(const BaseTensor<T>&) override; 
      //! Add another tensor (in place).
      BaseTensor<T>* operator-=(const BaseTensor<T>&) override; 

      //! Element-wise multiplication (in-place).
      BaseTensor<T>* operator*=(const BaseTensor<T>&) override { MIDASERROR("NOT IMPLEMENTED"); return nullptr; }
      //! Element-wise division (in-place)
      BaseTensor<T>* operator/=(const BaseTensor<T>&) override { MIDASERROR("NOT IMPLEMENTED"); return nullptr; }

      //! Axpy
      void Axpy(const BaseTensor<T>&, const T&) override;
      
      /******************************** IO          ********************************/
      std::ostream& write_impl(std::ostream& output) const override { MIDASERROR("NOT IMPLEMENTED"); return output; }
       
      /******************************** DOT PRODUCT ********************************/
      dot_t Dot(const BaseTensor<T>* const) const override;
      // dispatches
      dot_t DotDispatch(const SimpleTensor<T>* const) const override;
      dot_t DotDispatch(const CanonicalTensor<T>* const) const override;
      dot_t DotDispatch(const TensorDirectProduct<T>* const) const override;
      dot_t DotDispatch(const TensorSum<T>* const) const override;
};

#endif /* TENSORSUM_DECL_H_INCLUDED */
