#ifndef MATRIX_CONTRACTOR_DECL_H_INCLUDED
#define MATRIX_CONTRACTOR_DECL_H_INCLUDED

#include <iostream>
#include <vector>
#include <tuple>
#include "tensor/ContractionMatrix.h"

// forward declaration for friends
template<class T>
class MatrixContractor;
template<class T>
std::ostream& operator<<(std::ostream&, const MatrixContractor<T>&);

/**
 * Class for fully contracting all indices of a set of ContractionMatrix%s.
 * Used by DotEvaluator to efficiently evaluate dots between two 
 * TensorDirectProduct%s of all CanonicalTensor%s.
 **/
template<class T>
class MatrixContractor
{
   //using index_set = std::set<unsigned>;
   //using contraction_pair = std::pair<index_set, index_set>;
   //using contraction_pair_set = std::vector<contraction_pair>;
   
   //using matrix_tuple = std::tuple<ContractionMatrix<T>, bool, bool>;
   //using matrix_vector = std::vector<matrix_tuple>;
   
   using matrix_vector = std::vector<ContractionMatrix<T> >;

   //class Sum 
   //{
   //   private:
   //      std::vector<unsigned> mIndeces;
   //      std::vector<unsigned> mExtents;
   //      std::vector<std::pair<int,int> > mMatrices;
   //   public:
   //      Sum(char, unsigned, const matrix_vector&);
   //      
   //      size_t Nmatrices() const;

   //      bool operator<(const Sum&) const;
   //};

   private:
      matrix_vector mMatrices; ///< Matrices to be contracted.
      
      //contraction_pair_set FindOverlappingSets() const; ///< find overlapping sets
   public:
      ///> Constructor
      MatrixContractor(int = 0);
      
      ///> add a matrix to the contracion
      void AddContractionMatrix(ContractionMatrix<T>&&);

      ///> do the contraction
      T Contract() const;

      ///> overload for operator<<
      friend std::ostream& operator<< <T>(std::ostream&, const MatrixContractor<T>&);
};

#endif /* MATRIX_CONTRACTOR_DECL_H_INCLUDED */
