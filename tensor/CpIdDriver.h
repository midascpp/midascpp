/**
************************************************************************
* 
* @file    CpIdDriver.h
*
* @date    27-02-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Recompression of CP tensors using CP-ID algorithm which 
*     can be followed by a fitting (CP-ALS, CP-PASD, CP-NCG, etc.).
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CPIDDRIVER_H_INCLUDED
#define CPIDDRIVER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"

namespace midas::tensor
{

/**
 * Decomposer class using ID algorithm
 **/
class CpIdDriver
   :  public detail::TensorDecompDriverBase
{
   private:
      //! Threshold for decomposition
      Nb mCpIdResThreshold;

      //! Max number of rank increments
      In mCpIdMaxRankIncr;

      //! Check decomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Decompose
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Decompose with argument guess
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Check if tensor should be fitted to rank-0
      bool ScreenImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Get threshold
      Nb GetThreshold
         (
         )  const override;

   public:
      //! c-tor from TensorDecompInfo
      CpIdDriver
         (  const TensorDecompInfo&
         );
};


} /* namespace midas::tensor */

#endif /* CPIDRIVER_H_INCLUDED */
