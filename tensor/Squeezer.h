#ifndef SQUEEZER_H_INCLUDED
#define SQUEEZER_H_INCLUDED

#include <memory>

#include "tensor/GammaMatrix.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/TensorSum.h"
#include "tensor/TensorDirectProduct.h"
#include "tensor/EnergyHolderTensor.h"
#include "tensor/SimpleTensor.h"

//========================================================
// Forward declarations that disable complex numbers
template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class Squeezer;

template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class BaseSqueezer;

template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class CanonicalSqueezer;

template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class DirectProductSqueezer;

template
   <  class T
   ,  typename = midas::type_traits::DisableIfComplexT<T>
   >
class SumSqueezer;
//========================================================

template
   <  class T
   >
void SqueezeNewModeMatrix
   (  const CanonicalTensor<T>& fit_tensor
   ,  const BaseTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* ptr
   ,  bool transpose=false
   );

template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const SimpleTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose=false
   );

template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const CanonicalTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose=false
   );

template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const TensorDirectProduct<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose=false
   );

template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const TensorSum<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose=false
   );

template
   <  class T
   ,  template<class> class IMPL
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const EnergyHolderTensorBase<T, IMPL>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose=false
   );

/**
 * Base class for squeezers.
 **/
template
   <  class T
   >
class BaseSqueezer<T>
{
   protected:
      const CanonicalTensor<T>* const mCanonical;
      const BaseTensor<T>* const mTarget;

   public:
      /**
       *
       **/
      BaseSqueezer
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
         :  mCanonical(canonical_tensor)
         ,  mTarget(target_tensor)
      {
      }

      /**
       *
       **/
      virtual ~BaseSqueezer()
      {
      }
      
      /**
       *
       **/
      virtual void SqueezeModeMatrix
         (  unsigned idim
         ,  T* rhs
         ,  bool transposed
         )
      {
         SqueezeNewModeMatrix(*mCanonical, *mTarget, idim, rhs, transposed);
      }

      /**
       *
       **/
      virtual void Update
         (  unsigned idim
         )
      {
      }

      /**
       *
       **/
      virtual void ReCalculate
         (
         )
      {
      }

      virtual T Dot
         (
         )
      {
         return mCanonical->Dot(mTarget);
      }
      
      /**
       *
       **/
      static std::unique_ptr<BaseSqueezer> Factory
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
      {
         switch(target_tensor->Type())
         {
            case BaseTensor<T>::typeID::CANONICAL:
               return std::unique_ptr<BaseSqueezer<T> >(new CanonicalSqueezer<T>(canonical_tensor, target_tensor));
            case BaseTensor<T>::typeID::DIRPROD:
               return std::unique_ptr<BaseSqueezer<T> >(new DirectProductSqueezer<T>(canonical_tensor, target_tensor));
            case BaseTensor<T>::typeID::SUM:
               return std::unique_ptr<BaseSqueezer<T> >(new SumSqueezer<T>(canonical_tensor, target_tensor));
            default:
               return std::unique_ptr<BaseSqueezer<T> >(new BaseSqueezer<T>(canonical_tensor, target_tensor));
         }
      }
};

/**
 * Squeeze a target tensor of Canonical format.
 **/
template
   <  class T
   >
class CanonicalSqueezer<T>
   :  public BaseSqueezer<T>
   ,  private GeneralGammaIntermediates<T>
{
   private:
      std::unique_ptr<T[]> mWorkGamma;
      const CanonicalTensor<T>* const mTarget;

   public:
      CanonicalSqueezer
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
         :  BaseSqueezer<T>(canonical_tensor, target_tensor)
         ,  GeneralGammaIntermediates<T>(canonical_tensor, static_cast<const CanonicalTensor<T>* const>(target_tensor))
         ,  mWorkGamma(new T[GeneralGammaIntermediates<T>::Rank2()])
         ,  mTarget(static_cast<const CanonicalTensor<T>* const>(target_tensor))
      {
      }
      
      /**
       *
       **/
      virtual void SqueezeModeMatrix
         (  unsigned idim
         ,  T* rhs
         ,  bool transposed
         )  override
      {
         GeneralGammaIntermediates<T>::GammaMatrix(idim, mWorkGamma);
         if(transposed)
         {
            char nc='N';
            char nt='T';
            T alpha = static_cast<T>(1.0);
            T beta  = static_cast<T>(0.0);
            int m = GeneralGammaIntermediates<T>::RankLeft();
            int n = BaseSqueezer<T>::mCanonical->Extent(idim);
            int k = GeneralGammaIntermediates<T>::RankRight();
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                         , mWorkGamma.get(), &lda
                                         , mTarget->GetModeMatrices()[idim], &ldb
                                         , &beta, rhs, &ldc 
                                         );
         }
         else
         {
            char nc='N';
            char nt='T';
            T alpha = static_cast<T>(1.0);
            T beta  = static_cast<T>(0.0);
            int m = BaseSqueezer<T>::mCanonical->Extent(idim);
            int n = GeneralGammaIntermediates<T>::RankLeft();
            int k = GeneralGammaIntermediates<T>::RankRight();
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                         , mWorkGamma.get(), &lda
                                         , mTarget->GetModeMatrices()[idim], &ldb
                                         , &beta, rhs, &ldc 
                                         );
         }
      }

      /**
       *
       **/
      virtual void Update
         (  unsigned idim
         )  override
      {
         GeneralGammaIntermediates<T>::Update(idim, BaseSqueezer<T>::mCanonical, mTarget);
      }

      /**
       *
       **/
      virtual void ReCalculate
         (
         )  override
      {
         GeneralGammaIntermediates<T>::ReCalculate(BaseSqueezer<T>::mCanonical, mTarget);
      }


      /**
       *
       **/
      virtual T Dot
         (
         )  override
      {
         return GeneralGammaIntermediates<T>::FullGammaMatrixNorm2(mWorkGamma);
      }
};

/**
 * @class DirectProductSqueezer
 **/
template
   <  class T
   >
class DirectProductSqueezer<T>
   :  public BaseSqueezer<T>
{
   private:
      const TensorDirectProduct<T>* const mTarget;
      unsigned mCanonicalRank;
      unsigned mLargestTargetRank;
      unsigned mNtensor;
      std::unique_ptr<T[]> mIntermedLevel2;
      std::vector<PartialGammaIntermediates<T> > mGammaIntermeds;
      std::unique_ptr<T[]> mWorkGamma;
      std::unique_ptr<T[]> mWorkLevel2;

      void UpdateLevel2
         (  unsigned itens
         ,  T* intermed_ptr
         ,  unsigned g_rank
         )
      {
         mGammaIntermeds[itens].FullGammaMatrix(mWorkGamma);
         auto gamma_ptr = mWorkGamma.get();
         auto intermed_ptr_incr = intermed_ptr;
         for(int rtilde = 0; rtilde < mCanonicalRank; ++rtilde)
         {
            *(intermed_ptr_incr++) = static_cast<T>(0.0);
         }
         for(int rmark = 0; rmark < g_rank; ++rmark)
         {
            intermed_ptr_incr = intermed_ptr;
            for(int rtilde = 0; rtilde < mCanonicalRank; ++rtilde)
            {  
               *(intermed_ptr_incr++) += *(gamma_ptr++);
            }
         }
      }

      void CalculateRankIntermeds
         (  unsigned itens
         ,  std::unique_ptr<T[]>& rank_intermeds
         )
      {
         auto intermed_ptr = mIntermedLevel2.get();
         auto rank_intermeds_incr = rank_intermeds.get();
         for(int i = 0; i < mCanonicalRank; ++i)
         {
            *(rank_intermeds_incr++) = mTarget->Coef();
         }
         for(int iitens = 0; iitens < mNtensor; ++iitens)
         {
            rank_intermeds_incr = rank_intermeds.get();
            if(iitens == itens)
            {
               intermed_ptr += mCanonicalRank;
            }
            else
            {
               for(int irank = 0; irank < mCanonicalRank; ++irank)
               {
                  *(rank_intermeds_incr++) *= *(intermed_ptr++);
               }
            }
         }
      }

      int FindTensor
         (  unsigned idim
         )
      {
         auto reverse_connection = mTarget->GetReverseConnection();
         auto itens = -1;
         bool already_found = false;
         for(int i = 0; i < reverse_connection.size(); ++i)
         {
            for(int j = 0; j < reverse_connection[i].size(); ++j)
            {
               if(reverse_connection[i][j] == idim)
               {
                  itens = i;
                  already_found = true;
                  break;
               }
            }
            if(already_found) break;
         }
         if(itens == -1) MIDASERROR("Tensor not found...");
         return itens;
      }


   public:
      /**
       *
       **/
      DirectProductSqueezer
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
         :  BaseSqueezer<T>(canonical_tensor, target_tensor)
         ,  mTarget(static_cast<const TensorDirectProduct<T>* const>(target_tensor))
         ,  mCanonicalRank(canonical_tensor->GetRank())
         ,  mLargestTargetRank(0)
         ,  mNtensor(static_cast<const TensorDirectProduct<T>* const>(target_tensor)->GetTensors().size())
         ,  mIntermedLevel2(new T[mCanonicalRank * mNtensor])
         ,  mGammaIntermeds()
         ,  mWorkGamma(nullptr)
         ,  mWorkLevel2(new T[mCanonicalRank])
      {
         
         mGammaIntermeds.reserve(mNtensor);
         auto ptr = static_cast<const TensorDirectProduct<T>* const>(target_tensor);
         auto& tensors = ptr->GetTensors();
         auto& reverse_connection = ptr->GetReverseConnection();
         
         // check that we have all canonical
         for(int itens = 0; itens < mNtensor; ++itens)
         {
            if (  tensors[itens]->Type() != BaseTensor<T>::typeID::CANONICAL
               )
            {
               MIDASERROR("Not all canoniocal :CC");
            }
            auto rank = static_cast<const CanonicalTensor<T>* const>(tensors[itens].get())->GetRank();
            if(mLargestTargetRank < rank)
               mLargestTargetRank = rank; 
         }
         mWorkGamma = std::unique_ptr<T[]>(new T[mCanonicalRank*mLargestTargetRank]);

         // Calculate intermediates
         // do level 1 intermediates
         for(int itens = 0 ; itens < mNtensor; ++itens)
         {
            mGammaIntermeds.emplace_back(BaseSqueezer<T>::mCanonical, static_cast<const CanonicalTensor<T>* const>(tensors[itens].get()), reverse_connection[itens]);
         }

         // do level 2 intermediates from level 2 intermediates
         auto intermed_ptr = mIntermedLevel2.get();
         for(int itens = 0; itens <mNtensor; ++itens)
         {
            UpdateLevel2(itens, intermed_ptr, static_cast<const CanonicalTensor<T>* const>(tensors[itens].get())->GetRank());
            intermed_ptr += mCanonicalRank;
         }
      }
      
      /**
       *
       **/
      virtual void SqueezeModeMatrix
         (  unsigned idim
         ,  T* rhs
         ,  bool transposed
         )  override
      {
         auto itens = FindTensor(idim);
         auto tensor_ptr = static_cast<const CanonicalTensor<T>* const>(mTarget->GetTensors()[itens].get());
         auto tensor_idim = mTarget->GetConnection()[idim].second;
         //MidasAssert(itens == mTarget->GetConnection()[idim].first, "something is wrong !!");
         auto extent = BaseSqueezer<T>::mCanonical->Extent(idim);
         mGammaIntermeds[itens].GammaMatrix(idim, mWorkGamma);

         if (  transposed  )
         {
            // calculate Q^T
            char nc='N';
            char nt='T';
            T alpha = static_cast<T>(1.0);
            T beta  = static_cast<T>(0.0);
            int m = mGammaIntermeds[itens].RankLeft();
            int n = extent;
            int k = mGammaIntermeds[itens].RankRight();
            int lda = std::max(1, m);
            int ldb = std::max(1, n);
            int ldc = std::max(1, m);
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                         , mWorkGamma.get(), &lda
                                         , tensor_ptr->GetModeMatrices()[tensor_idim], &ldb
                                         , &beta, rhs, &ldc 
                                         );

            // multiply by level 2 intermeds
            CalculateRankIntermeds(itens, mWorkLevel2);
            
            auto rhs_ptr = rhs;
            for(int i = 0; i < extent; ++i)
            {
               auto intermed_ptr_incr = mWorkLevel2.get();
               for(int rtilde = 0; rtilde < mCanonicalRank; ++rtilde)
               {
                  *(rhs_ptr++) *= *(intermed_ptr_incr++);
               }
            }
         }
         else
         {
            MIDASERROR("NOT IMPLEMENTED!");
         //   // calculate Q
         //   char nc='N';
         //   char nt='T';
         //   T alpha = static_cast<T>(1.0);
         //   T beta  = static_cast<T>(0.0);
         //   int m = extent;
         //   int n = mGammaIntermeds[itens].RankLeft();
         //   int k = mGammaIntermeds[itens].RankRight();
         //   int lda = std::max(1, m);
         //   int ldb = std::max(1, n);
         //   int ldc = std::max(1, m);
         //   midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
         //                                , tensor_ptr->GetModeMatrices()[tensor_idim], &lda
         //                                , this->mWorkGamma.get(), &ldb
         //                                , &beta, rhs, &ldc 
         //                                );

         //   // multiply by level 2 intermeds
         //   CalculateRankIntermeds(itens, mWorkLevel2);

         //   auto rhs_ptr = rhs;
         //   auto intermed_ptr = this->mWorkLevel2.get();
         //   for(int rtilde=0; rtilde < this->mCanonicalRank; ++rtilde)
         //   {
         //      for(int i=0; i<extent; ++i)
         //      {
         //         *(rhs_ptr++) *= (*intermed_ptr);
         //      }
         //      ++intermed_ptr;
         //   }
         }
      }

      /**
       *
       **/
      virtual void Update
         (  unsigned idim
         )  override
      {
         auto itens = FindTensor(idim);
         auto intermed_ptr = mIntermedLevel2.get();
         auto& tensors = mTarget->GetTensors();
         // update level 1
         mGammaIntermeds[itens].Update(idim, BaseSqueezer<T>::mCanonical, static_cast<const CanonicalTensor<T>* const>(tensors[itens].get()));
         // update level 2
         intermed_ptr += itens*mCanonicalRank;
         UpdateLevel2(itens, intermed_ptr, static_cast<const CanonicalTensor<T>* const>(tensors[itens].get())->GetRank());
      }

      /**
       *
       **/
      virtual void ReCalculate
         (
         )  override
      {
         auto ndim = this->mTarget->NDim();

         for(unsigned idim=0; idim<ndim; ++idim)
         {
            this->Update(idim);
         }
      }

      /**
       *
       **/
      virtual T Dot
         (
         )  override
      {
         auto work_ptr = mWorkLevel2.get();
         for(int irank = 0; irank < mCanonicalRank; ++irank)
         {
            *(work_ptr++) = static_cast<T>(1.0);
         }
         
         auto intermed_ptr = mIntermedLevel2.get();
         for(int itens = 0; itens < mNtensor; ++itens)
         {
            work_ptr = mWorkLevel2.get();
            for(int irank = 0; irank < mCanonicalRank; ++irank)
            {
               *(work_ptr++) *= *(intermed_ptr++);
            }
         }

         work_ptr = mWorkLevel2.get();

         auto dot = static_cast<T>(0.0);
         for(int irank = 0; irank < mCanonicalRank; ++irank)
         {
            dot += *(work_ptr++);
         }

         return dot * mTarget->Coef();
      }
};

/**
 *
 **/
template
   <  class T
   >
class SumSqueezer<T>
   :  public BaseSqueezer<T>
{
   private:
      const TensorSum<T>* const mTarget;
      unsigned mRank;
      unsigned mNtens;
      std::unique_ptr<T[]> mWorkRhs;
      std::vector<std::unique_ptr<BaseSqueezer<T> > > mSqueezers;
   public:
      /**
       *
       **/
      SumSqueezer
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
         :  BaseSqueezer<T>(canonical_tensor, target_tensor)
         ,  mTarget(static_cast<const TensorSum<T>* const>(target_tensor))
         ,  mRank(canonical_tensor->GetRank())
         ,  mNtens(mTarget->GetTensors().size())
         ,  mWorkRhs(new T[mRank*target_tensor->MaxExtent()])
      {
         auto& tensors = mTarget->GetTensors();
         mSqueezers.reserve(mNtens);
         for(int itens = 0; itens < mNtens; ++itens)
         {
            mSqueezers.emplace_back(BaseSqueezer<T>::Factory(canonical_tensor, tensors[itens].get()));
         }
      }

      /**
       *
       **/
      virtual void SqueezeModeMatrix
         (  unsigned idim
         ,  T* rhs
         ,  bool transposed
         )  override
      {
         // get stuff
         auto& tensors = mTarget->GetTensors();
         auto& coefs   = mTarget->GetCoefs();
         
         // init to zero, so we can sum up
         auto extent = mTarget->Extent(idim);
         auto size = mRank*extent;
         for(int i = 0; i < size; ++i)
         {
            rhs[i] = static_cast<T>(0.0);
         }

         for(int itens = 0; itens < mNtens; ++itens)
         {
            if( (tensors[itens]->Type() == BaseTensor<T>::typeID::DIRPROD && static_cast<TensorDirectProduct<T>*>(tensors[itens].get())->Rank() == 0)
             || (tensors[itens]->Type() == BaseTensor<T>::typeID::CANONICAL && static_cast<CanonicalTensor<T>*>(tensors[itens].get())->GetRank() == 0)
              )
            {
               continue; // continue for loop over tensors
            }
            mSqueezers[itens]->SqueezeModeMatrix(idim, mWorkRhs.get(), transposed);
            for(int j = 0; j < size; ++j)
            {
               rhs[j] += coefs[itens]*mWorkRhs[j];
            }
         }
      }
      
      /**
       *
       **/
      virtual void Update
         (  unsigned idim
         )  override
      {
         for(int itens = 0; itens < mNtens; ++itens)
         {
            mSqueezers[itens]->Update(idim);
         }
      }

      /**
       *
       **/
      virtual void ReCalculate
         (
         )  override
      {
         for(int itens=0; itens < mNtens; ++itens)
         {
            mSqueezers[itens]->ReCalculate();
         }
      }

      /**
       *
       **/
      virtual T Dot
         (
         )  override
      {
         auto& coefs   = mTarget->GetCoefs();
         auto dot = static_cast<T>(0.0);

         for(int itens = 0; itens < mNtens; ++itens)
         {
            dot += coefs[itens]*mSqueezers[itens]->Dot();
         }
         
         return dot;
      }
};

/**
 * Squeezer interface class.
 * Holds concrete squeezers and forwards all calls to them.
 **/
template
   <  class T
   >
class Squeezer<T>
{
   private:
      std::unique_ptr<BaseSqueezer<T> > mConcreteSqueezer;

   public:
      /**
       *
       **/
      Squeezer
         (  const CanonicalTensor<T>* const canonical_tensor
         ,  const BaseTensor<T>* const target_tensor
         )
         :  mConcreteSqueezer(BaseSqueezer<T>::Factory(canonical_tensor, target_tensor))
      {
      }
      
      /**
       *
       **/
      template<class SmartPtr>
      void SqueezeModeMatrix
         (  unsigned idim
         ,  SmartPtr& rhs
         ,  bool transposed
         )
      {
         mConcreteSqueezer->SqueezeModeMatrix(idim, rhs.get(), transposed);
      }


      /**
       *
       **/
      void Update
         (  unsigned idim
         )
      {
         mConcreteSqueezer->Update(idim);
      }

      /**
       *
       **/
      void ReCalculate
         (
         )
      {
         mConcreteSqueezer->ReCalculate();
      }

      /**
       *
       **/
      T Dot
         (
         )  const
      {
         return mConcreteSqueezer->Dot();
      }
};


// Functions called by BaseSqueezer
template
   <  class T
   >
void SqueezeNewModeMatrix
   (  const CanonicalTensor<T>& fit_tensor
   ,  const BaseTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* ptr
   ,  bool transpose
   )
{
   assert_same_shape(fit_tensor, target_tensor);

   switch(target_tensor.Type())
   {
   /*------------- Squeeze with SimpleTensor target --------------*/
      case BaseTensor<T>::typeID::SIMPLE:
         Squeeze(fit_tensor, dynamic_cast<const SimpleTensor<T>&>(target_tensor), idx, ptr, transpose);
         break;
   /*----------- Squeeze with CanonicalTensor target -------------*/
      case BaseTensor<T>::typeID::CANONICAL:
         Squeeze(fit_tensor, dynamic_cast<const CanonicalTensor<T>&>(target_tensor), idx, ptr, transpose);
         break;
   /*------- Squeeze with TensorDirectProduct target -------------*/
      case BaseTensor<T>::typeID::DIRPROD:
         Squeeze(fit_tensor, dynamic_cast<const TensorDirectProduct<T>&>(target_tensor), idx, ptr, transpose);
         break;
   /*------- Squeeze with TensorDirectProduct target -------------*/
      case BaseTensor<T>::typeID::SUM:
         Squeeze(fit_tensor, dynamic_cast<const TensorSum<T>&>(target_tensor), idx, ptr, transpose);
         break;
   /*------- Squeeze with EnergyHolderTensorBase target -------------*/
      case BaseTensor<T>::typeID::ENERGYHOLDER:
      {
         if (  auto* cast = dynamic_cast<const EnergyHolderTensorBase<T, ModalEnergyHolder>*>(&target_tensor)
            )
         {
            Squeeze(fit_tensor, *cast, idx, ptr, transpose);
         }
         else
         {
            MIDASERROR("Squeeze mode matrix for EnergyHolderTensorBase: dynamic cast failed!");
         }
         break;
      }
   /*------- Default -------------*/
      default:
         MIDASERROR("Squeeze operation for type of target tensor is missing!");
   }
}

/**
 *
 **/
template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const SimpleTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   )
{
   assert_same_shape(fit_tensor, target_tensor);
   auto rank = fit_tensor.GetRank();
   auto dims = fit_tensor.GetDims();
   auto ndim = fit_tensor.NDim();
   auto** mode_matrices = fit_tensor.GetModeMatrices();

   int step;
   int stride;

   if(transpose)
   {
      step = rank;
      stride = 1;
   }
   else
   {
      step = 1;
      stride = dims[idx];
   }

   for(int irank = 0; irank < rank; ++irank)
   {
      const BaseTensor<T> *prev;
      const BaseTensor<T> *next = &target_tensor;
      for(int jdim = ndim-1; jdim >= 0; --jdim)
      {
         if(jdim!=idx)
         {
            prev=next;
            T* copy(new T[dims[jdim]]);
            T* ptr_in=mode_matrices[jdim]+irank*dims[jdim];
            T* ptr_out=copy;
            for(unsigned int i=0;i<dims[jdim];++i)
            {
               *(ptr_out++)=*(ptr_in++);
            }

            BaseTensorPtr<T> simple_ptr(new SimpleTensor<T>(std::vector<unsigned int> {dims[jdim]}, copy));
            next = prev->ContractDown(jdim, simple_ptr.get());
            if(prev!=&target_tensor) delete prev;
         }
      }

      //Copy into the result, transpose or not
      const T *in_ptr = dynamic_cast<const SimpleTensor<T>*>(next)->GetData();
      T *out_ptr = result + irank*stride;

      for(int i = 0; i < dims[idx]; i++)
      {
         *out_ptr = *(in_ptr++);
         out_ptr += step;
      }
      if(next != &target_tensor) delete next; 
   }
}

/**
 *
 **/
template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const CanonicalTensor<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   )
{
   assert_same_shape(fit_tensor, target_tensor);
   auto fit_rank = fit_tensor.GetRank();
   auto target_rank = target_tensor.GetRank();
   auto dims = fit_tensor.GetDims();
   auto ndim = fit_tensor.NDim();
   auto** fit_mode_matrices = fit_tensor.GetModeMatrices();
   auto** target_mode_matrices = target_tensor.GetModeMatrices();

   if(transpose)
   {
      auto mixed_gamma = fit_tensor.GetAllocatorTp().AllocateUniqueArray(fit_rank * target_rank);
      GammaMatrix(&fit_tensor, &target_tensor, idx, mixed_gamma);
      char nc='N';
      char nt='T';
      T one =static_cast<T>(1.0);
      T zero=static_cast<T>(0.0);
      int m=fit_rank;
      int n=dims[idx];
      int k=target_rank;
      midas::lapack_interface::gemm(&nc, &nt, &m, &n, &k,
                                    &one,
                                    mixed_gamma.get(),         &m,
                                    target_mode_matrices[idx], &n,
                                    &zero,
                                    result,              &m );
   }
   else
   {
      auto mixed_gamma = fit_tensor.GetAllocatorTp().AllocateUniqueArray(target_rank * fit_rank);
      GammaMatrix(&target_tensor, &fit_tensor, idx, mixed_gamma);
      char nc='N';
      T one =static_cast<T>(1.0);
      T zero=static_cast<T>(0.0);
      int m=dims[idx];
      int n=fit_rank;
      int k=target_rank;
      midas::lapack_interface::gemm(&nc, &nc, &m, &n, &k,
                                    &one,
                                    target_mode_matrices[idx], &m,
                                    mixed_gamma.get(),         &k,
                                    &zero,
                                    result,              &m );
   }
}

/**
 *
 **/
template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const TensorDirectProduct<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   )
{
   auto rank = fit_tensor.GetRank();
   auto dims = fit_tensor.GetDims();

//   LOGCALL("dirprod");
   // get stuff
   auto& tensors = target_tensor.GetTensors();
   auto& reverse_connection = target_tensor.GetReverseConnection();
   
   // calculate intermediates
   std::vector<std::vector<T> > intermeds(tensors.size());
   for(int i = 0; i < intermeds.size(); ++i) // loop over tensors
   {
      auto& reverse_indices = reverse_connection[i];
      intermeds[i] = fit_tensor.PartialContract(tensors[i].get(), reverse_indices);
   }

   auto itens = -1;
   bool already_found = false;
   for(int i = 0; i < reverse_connection.size(); ++i)
   {
      for(int j = 0; j < reverse_connection[i].size(); ++j)
      {
         if(reverse_connection[i][j] == idx)
         {
            itens = i;
            already_found = true;
            break;
         }
      }
      if(already_found) break;
   }
   if(itens == -1) MIDASERROR("Tensor not found...");

   auto result_partial = fit_tensor.PartialSqueeze(tensors[itens].get(), reverse_connection[itens], idx, transpose);
   for(int i = 0; i < rank*dims[idx]; ++i)
   {
      result[i] = result_partial[i];
   }
   
   std::vector<T> rank_intermeds(rank);
   for(int irank = 0; irank < rank_intermeds.size(); ++irank)
   {
      rank_intermeds[irank] = static_cast<T>(1.0);
      for(int iitens = 0; iitens < tensors.size(); ++iitens)
      {
         if(iitens != itens)
         {
            rank_intermeds[irank] *= intermeds[iitens][irank];
         }
      }
   }

   if(transpose)
   {
      auto result_ptr = result;
      for(int idim = 0; idim < dims[idx]; ++idim)
      {
         for(int irank = 0; irank < rank; ++irank)
         {
            *(result_ptr++) *= rank_intermeds[irank]*target_tensor.Coef();
         }
      }

   }
   else
   {
      auto result_ptr = result;
      for(int irank = 0; irank < rank; ++irank)
      {
         for(int idim = 0; idim < dims[idx]; ++idim)
         {
            *(result_ptr++) *= rank_intermeds[irank]*target_tensor.Coef();
         }
      }
   }
}

/**
 *
 **/
template
   <  class T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const TensorSum<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   )
{
//   LOGCALL("sum");
   // get stuff
   auto& tensors = target_tensor.GetTensors();
   auto& coefs   = target_tensor.GetCoefs();
   auto rank = fit_tensor.GetRank();
   auto dims = fit_tensor.GetDims();
   
   auto size = rank*dims[idx];
   for(int i = 0; i < size; ++i)
   {
      result[i] = static_cast<T>(0.0);
   }

   auto intermed_result = fit_tensor.GetAllocatorTp().AllocateUniqueArray(size);
   for(int i = 0; i < tensors.size(); ++i)
   {
      if( (tensors[i]->Type() == BaseTensor<T>::typeID::DIRPROD && static_cast<TensorDirectProduct<T>*>(tensors[i].get())->Rank() == 0)
       || (tensors[i]->Type() == BaseTensor<T>::typeID::CANONICAL && static_cast<CanonicalTensor<T>*>(tensors[i].get())->GetRank() == 0)
        )
      {
         continue; // continue for loop over tensors
      }
      SqueezeNewModeMatrix(fit_tensor, *(tensors[i].get()), idx, intermed_result.get(), transpose);
      for(int j = 0; j < size; ++j)
      {
         result[j] += coefs[i]*intermed_result[j];
      }
   }
}

/**
 *
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const EnergyHolderTensorBase<T, IMPL>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   )
{
   assert_same_shape(fit_tensor, target_tensor);
   auto rank = fit_tensor.GetRank();
   auto dims = fit_tensor.GetDims();
   auto ndim = fit_tensor.NDim();
   auto** mode_matrices = fit_tensor.GetModeMatrices();

   int step;
   int stride;

   if(transpose)
   {
      step = rank;
      stride = 1;
   }
   else
   {
      step = 1;
      stride = dims[idx];
   }

   // pre-allocate copy
   auto copy = std::make_unique<T[]>(fit_tensor.MaxExtent());

   // Loop over ranks
   for(int irank = 0; irank < rank; ++irank)
   {
      // Set up tensor pointers to intermediate results.
      const BaseTensor<T>* prev;
      const BaseTensor<T>* next = &target_tensor;

      // Loop over dimensions
      for(int jdim = ndim-1; jdim >= 0; --jdim)
      {
         // Perform down contraction for all jdim!=idx
         if(jdim!=idx)
         {
            prev=next;
            T* ptr_in=mode_matrices[jdim]+irank*dims[jdim];
            T* ptr_out=copy.get();
            for(unsigned int i=0;i<dims[jdim];++i)
            {
               *(ptr_out++)=*(ptr_in++);
            }

            // Move data owned by copy to simple_ptr for use in down-contraction interface
            BaseTensorPtr<T> simple_ptr(new SimpleTensor<T>(std::vector<unsigned int>{dims[jdim]}, copy.release())); // simple_ptr manages copy

            // Perform down contraction
            next = prev->ContractDown(jdim, simple_ptr.get());

            // Move data back to copy
            static_cast<SimpleTensor<T>*>(simple_ptr.get())->SwapData(copy);

            if(prev!=&target_tensor) delete prev;
         }
      }

      //Copy into the result, transpose or not
      const T* in_ptr = dynamic_cast<const SimpleTensor<T>*>(next)->GetData();
      T* out_ptr = result + irank*stride;

      for(int i = 0; i < dims[idx]; ++i)
      {
         *out_ptr = *(in_ptr++);
         out_ptr += step;
      }
      if(next != &target_tensor) delete next; 
   }
}

#endif /* SQUEEZER_H_INCLUDED */
