/**
************************************************************************
* 
* @file                EnergyHistogram.h
*
* Created:             22-11-2017
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Wraps the eigen_distrib.h functions in a class.
* 
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ENERGYHISTOGRAM_H_INCLUDED
#define ENERGYHISTOGRAM_H_INCLUDED

#include<algorithm>
#include<utility>
#include<vector>
#include<cmath>
#include<list>

#include "mmv/DataCont.h"
#include "inc_gen/TypeDefs.h"

template <class T>
using bin_t=std::pair<T, int>;

template <class T>
using distrib_t=std::vector<bin_t<T>>;

// Creates a histogram with a box width of delta. Empty boxes are
// not included.
template
   <  class T
   >
distrib_t<T> histogram
   (  const std::vector<T>& values
   ,  T delta
   )
{
   T curr=values[0];

   distrib_t<T> result{bin_t<T>(curr+0.5*delta, 0)};
   for(auto val: values)
   {
      if(val<curr+delta)
      {
         (result.end()-1) -> second ++;
      }
      else
      {
         curr+=floor((val-curr+0.5*delta)/delta) * delta;
         result.push_back( bin_t<T>( curr+0.5*delta, 1) );
      }
   }
   return result;
}

template
   <  class T
   >
distrib_t<T> histogram
   (  const std::vector<T>& values
   ,  unsigned int nbox
   )
{
   T min=*( values.begin() );
   T max=*( values.end() );

   return histogram(values, (max-min)/nbox );
}

// Creates a histogram by adding up two histograms
template
   <  class T
   >
distrib_t<T> sum_histogram
   ( const distrib_t<T>& d1
   , const distrib_t<T>& d2
   , const T delta
   )
{
   T start= (d1.begin())->first + (d2.begin())->first;
   T end  = (d1.end()-1)->first + (d2.end()-1)->first;

   distrib_t<T> result;

   for(auto p1: d1)
   {
      for(auto p2: d2)
      {
         bool inserted=false;
         bin_t<T> newpair(p1.first+p2.first, p1.second*p2.second);
         for(auto& already_inserted: result)
         {
            if(newpair.first > already_inserted.first-0.5*delta &&
               newpair.first < already_inserted.first+0.5*delta)
            {
               already_inserted.second+=newpair.second;
               inserted=true;
               break;
            }
         }
         if(!inserted)
         {
            result.push_back(newpair);
         }
      }
   }
   std::sort(result.begin(), result.end(), [](const bin_t<T>& b1, const bin_t<T>& b2){return b1.first<b2.first;});
   return result;
}

/**
 *
 **/
template
   <  class T
   >
class EnergyHistogram
   :  public distrib_t<T>
{
   private:

   public:
      //! Constructor for orbital-energy histogram
      EnergyHistogram
         (  const std::pair<T,T>& interval
         ,  const T delta
         ,  unsigned int order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  T shift=static_cast<T>(0.)
         )
      {
         std::vector<T> denoms;
         for(auto o: occ)
            for(auto v: vir)
               denoms.push_back(v-o);
         std::sort(denoms.begin(), denoms.end());

         std::vector<T> shifted_denoms = denoms;
         for(auto& d : shifted_denoms)
            d -= shift;

         auto h = histogram<T>(denoms, delta);
         auto h2 = histogram<T>(shifted_denoms, delta);
         for(int i=1;i<order;++i)
         {
            h2 = sum_histogram<T>(h2,h,delta);
         }

         std::move(h2.begin(), h2.end(), std::back_inserter(*this));
      }

      //! Constructor for modal-energy histogram
      EnergyHistogram
         (  const std::pair<T,T>& interval
         ,  T delta
         ,  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  T shift=static_cast<T>(0.)
         )
      {
         // Definitions
         auto order = aModes.size();
      
         // Initialize
         distrib_t<T> h_result;
         distrib_t<T> h_tmp;
         std::vector<T> denoms;
      
         // Loop over modes
         unsigned iorder = 0;
         Nb val = C_0;
         for(const auto& mode : aModes)
         {
            // Number of modals (including occ)
            const auto& nmodals = aNmodals[mode];
      
            // Create vector of modal energies
            denoms.clear();
            denoms.reserve(nmodals-1);
            for(size_t imodal=1; imodal<nmodals; ++imodal)
            {
               aEigVals.DataIo(IO_GET, aOffsets[mode]+imodal, val);
               denoms.emplace_back(val);
      
               // Subtract shift from first histogram only
               if (  iorder == 0
                  )
               {
                  denoms.back() -= shift;
               }
            }
      
            // In first iteration, simply set h_result
            if (  iorder == 0
               )
            {
               h_result = histogram<T>(denoms, delta);
            }
            else
            {
               // Create temporary histogram
               h_tmp = histogram<T>(denoms, delta);
      
               // Convolute it with result
               h_result = sum_histogram<T>(h_result, h_tmp, delta);
            }
      
            ++iorder;
         }

         std::move(h_result.begin(), h_result.end(), std::back_inserter(*this));
      }

      //! Constructor for no given energies. Just put one in each box.
      EnergyHistogram
         (  const std::pair<T,T>& interval
         ,  T delta
         )
      {
         auto xmin = interval.first;
         auto xmax = interval.second;
         unsigned nboxes = std::round((xmax - xmin)/delta);
         this->resize(nboxes);

         for(size_t i=0; i<nboxes; ++i)
         {
            T val = xmin + (static_cast<T>(i)+static_cast<T>(0.5))*delta;
            (*this)[i] = std::make_pair(val, 1);
         }
      }
};

#endif /* ENERGYHISTOGRAM_H_INCLUDED */
