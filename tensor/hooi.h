
/**
 *************************************************************************
 * 
 * @file                hooi.h
 *
 * Created:             14.07.2016
 *
 * Author:              Gunnar Schmitz (Gunnar.Schmitz@chem.au.dk)
 *
 * \brief               hooi: Conversion to Tucker Type tensor
 * 
 * 
 * Detailed Description: "Higher Order Orthogonal Iterations" are used
 *                       to convert a tensor to the Tucker Format.
 *                       Furthermore a higher order SVD (HOSVD) can
 *                       be performed as side product  
 *
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 *************************************************************************
 **/

#include<cstring>
#include<cstdlib>
#include<stack>
#include<numeric>

#include "tensor/NiceTensor.h"
#include "tensor/SimpleTensor.h"

#include "lapack_interface/math_wrappers.h"
#include "lapack_interface/GESVD.h"

#include "tensor/TensorDecomposer.h"

namespace X=contraction_indices;


template <class T>
struct tucker_struct
{

   unsigned int ndim;
   std::vector<unsigned int> dimcore;
   std::vector<unsigned int> dims;

   T* CoreTensor;
   T** SideMatrices;

};


/**
 * Performs Higher Order Orthogonal Iteration HOOI or Higher Order SVD HOSVD.
 *
 * @param  target  Tensor in CP format, which rank should be reduced.
 * @param  thrsvd  Threshold for the truncated SVDs.
 * @param  maxiter Maximal number of iterations in the ALS. 
 * @param  dohosvd If dohosvd stop directly after the HOSVD guess.
 * @return Tensor in Tucker format.
 * */
template <class T>
tucker_struct<T> hooi
   (  const NiceTensor<T>& target
   ,  const T thrsvd
   ,  const unsigned int maxiter
   ,  const bool dohosvd = true 
   )
{

   // constants
   const bool locdbg   = false;
   const bool dbg_svd  = false;  
   const bool doprtmat = false;  
   const bool check_error = true;

   // locals
   std::vector<unsigned int> dims=target.GetDims();
   const unsigned int ndim(target.NDim());
   In ranks[ndim];

   unsigned int nlrankmax = 0;
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      unsigned int ndimranks = I_1;
      for (unsigned int jmode = 0; jmode < ndim; ++jmode)
      {
         if (jmode != imode) ndimranks = ndimranks * dims[jmode];
      }
      nlrankmax = std::max(nlrankmax,std::min(ndimranks,dims[imode]));
   }

   // memory for the projection matrices U and singular values
   T** umat = new T*[ndim];
   T** sval = new T*[ndim];
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      umat[imode] = new T[dims[imode]*nlrankmax];
      sval[imode] = new T[std::min(dims[imode],nlrankmax)];
      ranks[imode] = dims[imode];
   }

   // main loop
   for (unsigned int iter = 0; iter<maxiter+1; ++iter)
   {

      const bool init = iter == 0;      // in first iteration we do a HOSVD

      if (iter > 0 && dohosvd) break;   // Stop if we only shoud do a HOSVD

      In newranks[ndim];

      for (unsigned int kmode = 0; kmode < ndim; ++kmode)
      {

         // set dimensions for this kmode 
         std::vector<unsigned int> dimsin;    // dimensions in the input tensor
         std::vector<unsigned int> dimsout;   // dimensions in the output matrix
         std::vector<unsigned int> noffin;    // offsets for the input tensor  
         std::vector<unsigned int> noffout;   // offsets for the output matrix  

         std::vector<unsigned int> ranksloc;

         dimsout.push_back(dims[kmode]);
         for (unsigned int jmode = 0; jmode < ndim; ++jmode)
         {
            if (jmode == kmode)
            {
               dimsin.push_back(dims[kmode]);
            }
            else
            {
               dimsin.push_back(  ranks[jmode]);
               dimsout.push_back( ranks[jmode]);

               ranksloc.push_back(ranks[jmode]);
            }

         }
         std::reverse(dimsout.begin(), dimsout.end());  // transpose it (Fortran vs. C++ issue)
      

         // create vector with offsets
         unsigned int nlastin  = dimsin[ndim-1];
         unsigned int nlastout = dimsout[ndim-1];

         noffin.push_back(nlastin);
         noffout.push_back(nlastout);
         for (int idx = ndim-2; idx > 0; --idx)
         {
            unsigned int ncurin = dimsin[idx]*nlastin;
            noffin.push_back(ncurin);
            nlastin = ncurin;

            unsigned int ncurout = dimsout[idx]*nlastout;
            noffout.push_back(ncurout);
            nlastout = ncurout;
         }   
         std::reverse(noffin.begin(), noffin.end());
         std::reverse(noffout.begin(),noffout.end());
         noffin.push_back(1);  // only as helper
         noffout.push_back(1); // only as helper

         if (locdbg) 
         {
            Mout << "Dimensions: " << dimsin << std::endl;
            Mout << "Ranks: " << ranksloc << std::endl;
         }
 
         // a) Construct partially projected tensor 
         NiceTensor<Nb> intermed = target;
         if (!init) // in the first iteration we only do a HOSVD 
         {
            for (unsigned int jmode = 0; jmode < ndim; ++jmode)
            {
               if (jmode != kmode) 
               {
                  T* tmp = new T[dims[jmode]*ranks[jmode]];
                  memcpy(reinterpret_cast<void*>(tmp),
                         reinterpret_cast<void*>(umat[jmode]),dims[jmode]*ranks[jmode]*sizeof(T));

                  NiceTensor<T> proj( new SimpleTensor<T>(std::vector<unsigned int>{static_cast<unsigned int>(ranks[jmode]),
                                                                                    static_cast<unsigned int>(dims[jmode])}, tmp)); 
 
                  intermed = intermed.ContractForward(jmode,proj);
               }
            }
         }

         // b) Reshape into a matrix

         // b.1 get second dimension
         In ndimranks = I_1;
         for (unsigned int jmode = 0; jmode < ndim; ++jmode) 
         {
            if (jmode != kmode) ndimranks = ndimranks * ranks[jmode];
         }

         // b.2 copy data of the tensor
         const unsigned int  nsize = dims[kmode]*ndimranks;

         T* tmp(new T[nsize]);
         memcpy(reinterpret_cast<void*>(tmp),
                reinterpret_cast<void*>(dynamic_cast<SimpleTensor<T>*>(intermed.GetTensor())->GetData()),nsize*sizeof(T));

         // b.3 do the matrix unfolding
         if (locdbg) Mout << "Dims for unfolding " <<  dims[kmode] << " " <<  ndimranks << std::endl;
         T* mat = new T[dims[kmode]*ndimranks];
         unsigned int iadrsrc = 0;
         unsigned int iadrdes = 0;

         /*
            Realization of arbitrary many nested loops via the basic counting principle to
            ensure an reshaping for any kind of tensor. This might effect a bit the
            performance, but other parts are much more rate limiting...
         */
         int i,j;

         std::vector<unsigned int> index(ndim);
         std::vector<unsigned int> len(dimsout.rbegin(), dimsout.rend());
         // init
         for (unsigned int idx = 0; idx < ndim; ++idx)
         {
            index[idx] = 0;
         }
         while(true)
         {
            for(i = 0 ; i < ndim; ++i)
            {

               //----------------------------------------------------------+
               // copy data
               //----------------------------------------------------------+

               // construct index vector
               std::stack<unsigned int> stidx;
               std::vector<unsigned int> ivecin;
               std::vector<unsigned int> ivecout;

               for (int ix = ndim-1; ix >= 0; --ix)
               {
                  if (ix > 0) stidx.push(index[ix]);
                  ivecout.push_back(index[ix]);
               }  

               for (unsigned int i = 0; i<ndim; i++)
               {
                  if (i == kmode) 
                  {
                     ivecin.push_back(index[0]);
                  }
                  else
                  {
                     unsigned int elem = stidx.top();
                     stidx.pop();
                     ivecin.push_back(elem);
                  }
               }
   
               // calculate address as dot product of the index and offset vector
               iadrsrc = std::inner_product(begin(noffin), end(noffin), begin(ivecin), 0);

               // also calculate the address in the output array
               iadrdes = std::inner_product(begin(noffout),end(noffout), begin(ivecout), 0);

               mat[iadrdes] = tmp[iadrsrc]; 
            }
            
            // increment indices
            for(j = ndim-1 ; j>=0 ; j--)
            {
               if(++index[j] < len[j])
               {
                  break;
               }
               else
               {
                  index[j]=0;
               }
            }
            if(j<0)
            {
               break;
            }
         }  
         delete[] tmp;  

         if (dbg_svd) 
         {
            T* prtMat = new T[dims[kmode]*ndimranks];
            memcpy(reinterpret_cast<void*>(prtMat),
                   reinterpret_cast<void*>(mat),dims[kmode]*ndimranks*sizeof(T));

            NiceTensor<T> prtTensor ( new SimpleTensor<Nb> (std::vector<unsigned int>{static_cast<unsigned int>(ndimranks),
                                                                                      static_cast<unsigned int>(dims[kmode])},prtMat));
            Mout << "Matrix for SVD:" << std::endl;
            Mout << prtTensor << std::endl;
         }

         // c) Caclulate truncated SVD of the matricization
         SVD_struct<T> svdk = GESVD(mat, static_cast<int>(dims[kmode]), static_cast<int>(ndimranks), 'A');

         if (dbg_svd) 
         {
            Mout << "Mode: " << kmode << std::endl;
            Mout << "Dims: " << svdk.Min() << " " << dims[kmode] << " " << ndimranks << std::endl;
            Mout << "Singular values: " << std::endl;
            for (unsigned int irank = 0; irank<svdk.Min(); ++irank)
            {
               Mout << irank << " " << svdk.s[irank] << std::endl;
            }
         }

         // get rank/truncate SVD
         In svd_currank = 0;
         for (unsigned int irank = 0; irank < svdk.Min(); ++irank)
         {
            if (svdk.s[irank] > thrsvd)
            { 
               svd_currank += 1;
            }
            sval[kmode][irank] = svdk.s[irank];
         }

         // d) Update the subspace for kmode
         size_t counter = 0;
         for (unsigned int irank = 0; irank < svd_currank; ++irank)
         {
            for (unsigned int idim = 0; idim < dims[kmode]; ++idim)
            {
               umat[kmode][irank*dims[kmode] + idim] = svdk.u[counter++];
            }
         }
         newranks[kmode] = svd_currank;
         if (!init) ranks[kmode] = svd_currank;

         // clean up a bit
         delete[] mat;

      }

      // update ranks if we not already did it (init/HOSVD)
      for (unsigned int kmode = 0; kmode < ndim; ++kmode)
      {
         ranks[kmode] = newranks[kmode];
      }
   }

   // Do the final projection with the converged Side matrices 
   NiceTensor<T> coretensor = target;
   for (unsigned int jmode = 0; jmode < ndim; ++jmode)
   {
      T* tmp = new T[dims[jmode]*ranks[jmode]];
      memcpy(reinterpret_cast<void*>(tmp),
             reinterpret_cast<void*>(umat[jmode]),dims[jmode]*ranks[jmode]*sizeof(T));

      NiceTensor<T> proj( new SimpleTensor<T>(std::vector<unsigned int>{static_cast<unsigned int>(ranks[jmode]),
                                                                        static_cast<unsigned int>(dims[jmode])}, tmp)); 
 
      coretensor = coretensor.ContractForward(jmode,proj);
   }

   if (check_error)
   {
      // Recreate tensor
      NiceTensor<T> newtensor = coretensor;
      for (unsigned int jmode = 0; jmode < ndim; ++jmode)
      {
         T* tmp = new T[dims[jmode]*ranks[jmode]];
         memcpy(reinterpret_cast<void*>(tmp),
                reinterpret_cast<void*>(umat[jmode]),dims[jmode]*ranks[jmode]*sizeof(T));

         NiceTensor<T> proj( new SimpleTensor<T>(std::vector<unsigned int>{static_cast<unsigned int>(ranks[jmode]),
                                                                           static_cast<unsigned int>(dims[jmode])}, tmp));

         newtensor = newtensor.ContractForward(jmode,proj[X::q,X::p]);
      }
      NiceTensor<Nb> diff = target - newtensor;
      Mout << "|| X - T || " << diff.Norm() <<  std::endl;
   }


   // prepare return object
   tucker_struct<T> result;
   result.ndim = ndim;
   result.dimcore = coretensor.GetDims();
   result.dims = dims;

   const size_t totalsize(coretensor.TotalSize());
   result.CoreTensor = new T[totalsize];

   result.SideMatrices = new T*[ndim];      
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      result.SideMatrices[imode] = new T[dims[imode]*ranks[imode]];
      memcpy(reinterpret_cast<void*>(result.SideMatrices[imode]),
             reinterpret_cast<void*>(umat[imode]),dims[imode]*ranks[imode]*sizeof(T));
      
   }

   // clean up
   for (unsigned int imode = 0; imode < ndim; ++imode)
   {
      delete[] umat[imode];
      delete[] sval[imode];
   }
   delete[] umat;
   delete[] sval;

   // finally return
   return result; 
}
