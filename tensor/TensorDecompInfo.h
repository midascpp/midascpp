#ifndef TENSORDECOMPINFO_H_INCLUDED
#define TENSORDECOMPINFO_H_INCLUDED

#include <map>
#include <set>
#include "libmda/util/any_type.h"
#include "inc_gen/Const.h"

namespace midas
{
namespace tensor
{

/**
 * Class to hold somewhat processed tensor decomposition input.
 * Used to construct object of type TensorDecomposer.
 **/
class TensorDecompInfo
   : public std::map<std::string, libmda::util::any_type>
{
   public:
      ///> define what a set of TensorDecompInfo's means
      using Set = std::set<TensorDecompInfo>;

      //! Default c-tor
      TensorDecompInfo() = default;

      //! Constructor from preset and threshold
      TensorDecompInfo
         (  const std::string& aPreset
         ,  Nb aThreshold
         );
};

/**
 * Overload of output operator for TensorDecompInfo.
 * @param os             Output stream.
 * @param info           TensorDecompInfo to output.
 * @return               Returns output stream so operator<<'s can be chained.
 **/
inline std::ostream& operator<<(std::ostream& os, const TensorDecompInfo& info)
{
   os << " TENSORDECOMPINFO: \n";
   for(auto& elem : info)
   {
      os << " KEYWORD = " << elem.first << ", VALUE = " << elem.second << "\n";
   }
   return os;
}

//! Get the smallest or largest decomp threshold
Nb GetDecompThreshold
   (  const TensorDecompInfo::Set& aInfo
   ,  bool aSmallest = false
   );

//! Update decomp thresholds
void UpdateDecompThresholds
   (  TensorDecompInfo::Set& aInfo
   ,  Nb aNewThresh
   ,  Nb aPreprocThresh = -C_1
   );

} /* namespace tensor */
} /* namespace midas */

using midas::tensor::TensorDecompInfo;

#endif /* TENSORDECOMPINFO_H_INCLUDED */
