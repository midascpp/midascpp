/**
 ************************************************************************
 * 
 * @file                TensorGraph.h
 *
 * Created:             17.08.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               TensorGraph: Tensor representation for lazy
 *                                   tensor contractions.
 * 
 * Detailed Description: Class to represent the result of tensor
 *                       contractions with minimal effort.
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef TENSORGRAPH_H_INCLUDED
#define TENSORGRAPH_H_INCLUDED

#include<vector>
#include<memory>
#include<utility>

#include"BaseTensor.h"
#include"SimpleTensor.h"

#include"TGMatrix.h"

template <class T>
class TensorGraph;

//! Vertex of a TensorGraph.
template <class T>
class TGVertex
{
   private:
      const unsigned int                              mRank;
      const std::vector<std::shared_ptr<TGMatrix<T>>> mModeMatrices;
      const std::vector<unsigned int>                 mIndexPositions;
   public:
      TGVertex(const unsigned int aRank);
      TGVertex(const unsigned int aRank
              ,const std::vector<std::shared_ptr<TGMatrix<T>>>& aModeMatrices
              ,const std::vector<unsigned int>&                 aIndexPositions);

      // --------------- Getters --------------------
            unsigned int                               GetRank()           const;
      const std::vector<std::shared_ptr<TGMatrix<T>>>& GetModeMatrices()   const;
      const std::vector<unsigned int>&                 GetIndexPositions() const;

      std::string Prettify() const;

   friend class TensorGraph<T>;

};

template <class T>
std::vector<unsigned int> collect_vertices_dims(const std::vector<TGVertex<T>>& vertices);
template <class T>
std::vector<TGVertex<T>> filter_and_swap_indices(
      const std::vector<TGVertex<T>>&  vertices
     ,const std::vector<unsigned int>& old_indices
     ,const std::vector<unsigned int>& new_indices);


//! Edge of a TensorGraph.
template <class T>
class TGEdge
{
   private:
      const unsigned int           mFrom;
      const unsigned int           mTo;
      std::shared_ptr<TGMatrix<T>> mMatrix; // size: mFrom.mRank x mTo.mRank

   public:
      TGEdge(const unsigned int                 mFrom
            ,const unsigned int                 mTo
            ,const std::shared_ptr<TGMatrix<T>> mMatrix);

      // --------------- Getters --------------------
            unsigned int                  GetFrom()   const;
            unsigned int                  GetTo()     const;
      const std::shared_ptr<TGMatrix<T>>& GetMatrix() const;

      std::string Prettify() const;


   friend class TensorGraph<T>;
};

template <class T>
std::vector<TGEdge<T>> shift_vertex_refs(
      const std::vector<TGEdge<T>>&  edges
     ,const unsigned int shift);

//! Graph representation of tensors.
/*! \f[
 *     t_{\mathbf{i}}=\sum_{\mathrm{Rank~indices}}
 *     \left(\prod_{e\in\mathrm{edges}} Z^e_{\alpha_{e_1}\alpha_{e_2}}\right)
 *     \left(\prod_{v\in\mathrm{vertices}} \prod_{i\in v}t^{v,i}_{\alpha_v u_i}\right)
 *  \f]
 */
template <class T>
class TensorGraph
   : public BaseTensor<T>
{
   private:
      const std::vector<TGVertex<T>> mVertices;
      const std::vector<TGEdge<T>>   mEdges;

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;
      typename BaseTensor<T>::typeID Type() const {return BaseTensor<T>::typeID::GRAPH;};
      std::string ShowType() const {return "TensorGraph";};

//*==============  Implementations of tensor operations ==============*/
   private:
      BaseTensor<T>* Contract_impl(const std::vector<unsigned int>&,
                                   const std::vector<unsigned int>&,
                                   const BaseTensor<T>&,
                                   const std::vector<unsigned int>&,
                                   const std::vector<unsigned int>&,
                                   const std::vector<unsigned int>&,
                                   const std::vector<unsigned int>&,
                                   const std::vector<unsigned int>&) const;

      TensorGraph* Reorder_impl(const std::vector<unsigned int>&,
                                const std::vector<unsigned int>&) const;

      BaseTensor<T>* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const;

      TensorGraph* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>&) const;

   public:
//*============== CONSTRUCTORS, DESTRUCTOR, ASSIGNMENTS ==============*/
//      TensorGraph();
      TensorGraph(const std::vector<TGVertex<T>> aVertices
                 ,const std::vector<TGEdge<T>>   aEdges   );
//      TensorGraph(std::istream&);

      
      TensorGraph* Clone() const;
/*============== CONTRACTIONS ==============*/
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const;
      TensorGraph*   ContractForward(const unsigned int, const BaseTensor<T>*) const;
      TensorGraph*   ContractUp     (const unsigned int, const BaseTensor<T>*) const;

      TensorGraph* operator*=(const T);
      BaseTensor<T>* operator+=(const BaseTensor<T>&);

      TensorGraph* operator*=(const BaseTensor<T>&);

      TensorGraph* operator/=(const BaseTensor<T>&);

      std::string Prettify() const;

      std::ostream& write_impl(std::ostream& output) const;

      dot_t Dot(const BaseTensor<T>* const) const;
/*============== TensorGraph specific  ==============*/
      BaseTensor<T>* ToFullTensor() const;
      std::vector<std::pair<unsigned int,unsigned int>> LocateIndices() const;
};

#include"TensorGraph_Impl.h"

#endif /* TENSORGRAPH_H_INCLUDED */
