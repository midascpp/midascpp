#ifndef CONTRACTION_MATRIX_H_INCLUDED
#define CONTRACTION_MATRIX_H_INCLUDED


// declaration
#include "tensor/ContractionMatrix_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// implementation
#include "tensor/ContractionMatrix_Impl.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

#endif /* CONTRACTION_MATRIX_H_INCLUDED */
