/**
************************************************************************
* 
* @file                LaplaceQuadrature_Impl.h
*
* Created:             03-11-2017
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Implements functions for LaplaceQuadrature
* 
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LAPLACEQUADRATURE_IMPL_H_INCLUDED
#define LAPLACEQUADRATURE_IMPL_H_INCLUDED

#include <array>

#include "input/ProgramSettings.h"
#include "tensor/LaplaceQuadrature.h"
#include "util/conversions/FromString.h"
#include "util/Math.h"

#ifdef ENABLE_GSL
#include <gsl/gsl_integration.h>
#endif /* ENABLE_GSL */

using namespace midas;

/**
 * Constructor from number of points and interval. Used for generating new points fitted to a specific interval.
 *
 * @param lapinfo       LaplaceInfo for generation of points and weights
 * @param xmin          Minimum point of interval
 * @param xmax          Maximum point of interval
 * @param eigen         The distribution of energies
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>::LaplaceQuadratureBase
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  T xmin
   ,  T xmax
   ,  EnergyHistogram<T>&& eigen
   )
   :  mXmin(xmin)
   ,  mXmax(xmax)
   ,  mEigen(eigen)
   ,  mNegativeInterval(false)
   ,  mLsError(static_cast<T>(-1.))
{
   assert(mXmin < mXmax);

   // If the interval is on the negative axis, move it to the positive, and apply symmetry operations on weights afterwards.
   if (  mXmin < static_cast<T>(0.)
      && mXmax < static_cast<T>(0.)
      )
   {
//      MidasWarning("LaplaceQuadrature for negative interval will be optimized for interval on positive axis. Make sure this is handled properly in the further steps!");
//      Mout  << " Input interval:    [" << mXmin << ", " << mXmax << "]" << std::endl;

      mNegativeInterval = true;
      mXmin *= -static_cast<T>(1.);
      mXmax *= -static_cast<T>(1.);
      std::swap(mXmin, mXmax);

//      Mout  << " New interval:      [" << mXmin << ", " << mXmax << "]" << std::endl;
   }
};

/**
 * Construct points and weights after setting Xmin and Xmax.
 *
 * @param info    LaplaceInfo
 **/
template
   <  class T
   >
void LaplaceQuadratureBase<T>::ConstructLaplaceQuadrature
   (  const midas::tensor::LaplaceInfo& info
   )
{
   T ls_err = static_cast<T>(0.);
   T denom_range = this->mXmax / this->mXmin;

   const auto& type = info.at("TYPE").template get<std::string>();
   const auto& io = info.at("IOLEVEL").template get<In>();

   bool uselib = info.at("USELIBGUESS").template get<bool>();

   // If we want to find best laplace
   if (  type == "BESTLAP"
      )
   {
      auto lapconv = info.at("LAPCONV").template get<Nb>();
      auto maxpoints = info.at("MAXPOINTS").template get<In>();
      auto minpoints = info.at("MINPOINTS").template get<In>();
      bool converged = false;
      bool print_flow = io >= I_10;

      // Optimize exponents with a simplex algorithm
      for(unsigned nlap = minpoints; nlap <= maxpoints; ++nlap)
      {
         // Set number of points
         this->mNumPoints = nlap;

         // 1.) Set start points and weights
         this->MakeGuess(uselib);   

         // 2.) Optimize points and refit weights
         this->Optimize();

         // 3.) Check value of Laplace error function
         ls_err = this->LeastSquaresError();

         // Print flow of optimization
         if (  print_flow
            )
         {
            Mout  << " ### Result of Laplace optimization with " << nlap << " points ###" << "\n"
                  << "  Points:              " << this->mPoints << "\n"
                  << "  Weights:             " << this->mWeights << "\n"
                  << "  Laplace error:       " << ls_err << "\n"
                  << std::flush;
         }

         if (  ls_err < lapconv
            )
         {
            converged = true;
            break;
         }
      }

      // Throw error if not converged
      if (  !converged
         )
      {
         Mout << "ls_err " << ls_err << " lapconv " << lapconv << std::endl;
         MIDASERROR("Laplace quadrature is not accurate enough!");
      }

      // Print info
      if (  io >= I_5
         )
      {
         this->PrintInfo();
      }
   }
   // If we want to optimize a fixed number of points
   else if  (  type == "FIXLAP"
            )
   {
      // Set number of points
      this->mNumPoints = info.at("NUMPOINTS").template get<In>();

      // 1.) Set start points and weights
      this->MakeGuess(uselib);   

      // 2.) Optimize points and refit weights
      this->Optimize();

      // Calculate error and print info (requires numerical integration)
      if (  io >= I_10
         )
      {
         ls_err = this->LeastSquaresError();
         this->PrintInfo();
      }
   }
   // If we want to simply use the starting-guess points
   else if  (  type == "NOOPT"
            )
   {
      this->mNumPoints = info.at("NUMPOINTS").template get<In>();
      auto allow_more_points = info.at("NOOPTALLOWMOREPOINTS").template get<bool>();

      this->ReadBestDataFile(allow_more_points);

      // Calculate error and print info (requires numerical integration)
      if (  io >= I_10
         )
      {
         ls_err = this->LeastSquaresError();
         this->PrintInfo();
      }
   }
   else
   {
      MIDASERROR("LaplaceQuadrature: No match for Laplace TYPE: "+type);
   }

   // Check points
   for(const auto& point : this->mPoints)
   {
      if (  libmda::numeric::float_neg(point)
         )
      {
         Mout  << " LaplaceQuadrature: point = " << point << std::endl;
         MidasWarning("Negative-valued point in LaplaceQuadrature!");
      }
   }
}


/**
* Driver routine to optimize the Laplace points with a simplex algorithm
*
* @return
*  Pointer to this
**/
template
   <  class T
   >
LaplaceQuadratureBase<T>* LaplaceQuadratureBase<T>::Optimize
   (
   )
{
   Mout  << " ============== Optimize LaplaceQuadrature ================ " << std::endl;
   const bool locdbg = false;

   // Volume factor for initial simplex
//   T vol = static_cast<T>(1.e0);
   T vol = static_cast<T>(1.e-3);

   // sanity check 
   if (  mXmin == static_cast<T>(0.)
      && mXmax == mXmin
      )
   {
      MIDASERROR("Optimization of interval for interval [0,0]. This should not happen!");
   }

   std::vector<T> errors(this->mNumPoints + 1);    // holding the function values of the Laplace error function
   bool singular = false;                    // logical to indicate a problem in the simplex optimization
   bool converged = false;                   // logical to indicate if simplex optimization converged
   int iter = 0;                             // how many iterations were used in the optimization?
   int mxsmplx = 500;                        // maximal number of iterations in simplex algorithm

   T ftol  = 1.0e-4;                         // some hard coded thresholds for the error function
   T ftol2 = 1.0e-20;                        // not completely tested yet...

   // Construct error function
   LaplaceErrorFunction<T> err_func(this);

   std::vector< std::vector<T>> point(this->mNumPoints + 1, std::vector<T>(this->mNumPoints) );

   if (locdbg) 
   {
      Mout << "start points" << std::endl;
      Mout << this->mPoints << std::endl;
   }

   singular = true;
   while (singular)
   {
      singular = false;

      bool iterate = true;
      while (iterate)
      {
         iterate = false;

         // generate the other start vectors for simplex algorithm:
         // This is a crucial step. The initial simplex should not be too small
         point[0] = this->mPoints;
         solver::detail::initialize_simplex(point, vol);

         // calculate vector y -> input for simplexopt
         Mout  << " Calculate errors of initial simplex:" << std::endl;
         for (int k = 0; k < this->mNumPoints + 1; ++k)
         {
            Mout  << "    point[" << k << "]: " << point[k] << std::endl;
            errors[k] = err_func(point[k], ftol2, singular);
         }

         // vary vectors with a simplex algorithm
         solver::downhill_simplex(err_func, point, errors, ftol, ftol2, mxsmplx, iter, singular, converged);
//         simplexopt(point, errors, mNumPoints, xparam, 2, ftol, ftol2, mxsmplx, iter, singular, converged, this->mEigen);

         iterate = (!converged) && (!singular) && iter < mxsmplx;
      }
   }

   if (locdbg)
   {
      Mout << "optimized points" << std::endl;
      Mout << point[0] << std::endl;
   }

   if (locdbg) Mout << "Number of iterations: " << iter << std::endl;

   // update points
   this->mPoints = point[0];

   // and don't forget to refit weights for new points
   this->RefitWeights();

   return this;
}

/**
 * Calculate least-squares error for current points and weights.
 *
 * @param thresh     Threshold for numerical integration of error function (if not calculated analytically).
 *
 * @return
 *    Value of error function.
 **/
template
   <  class T
   >
T LaplaceQuadratureBase<T>::LeastSquaresError
   (  T thresh
   )
{
   return this->LeastSquaresError(this->mPoints, this->mWeights, thresh);
}


/**
 * Make guess of Laplace points and weights before optimization.
 *
 * @param readbest      Read data from library instead of using default (stupid) guess.
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* LaplaceQuadratureBase<T>::MakeGuess
   (  bool readbest
   )
{
   if (  readbest
      && this->ReadBestDataFile(false)
      )
   {
      // We have a guess from file
   }
   else if  (  this->DefaultGuess()
            )
   {
      // We have made a default guess
   }
   else
   {
      MIDASERROR("Unable to make guess for LaplaceQuadrature optimization!");
   }

   return this;
}

/**
 * Refit the weights for a given set of points by solving linear equations.
 *
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* LaplaceQuadratureBase<T>::RefitWeights
   (
   )
{
   bool singular = false;
   this->RefitWeights(this->mPoints, singular);

   if (  singular
      ) 
   {
      MIDASERROR("Weights could not be determined (Matrix singular)!");
   }

   return this;
}

/**
 * Print info
 *
 * @param os      Output stream
 **/
template
   <  class T
   >
void LaplaceQuadratureBase<T>::PrintInfo
   (  std::ostream& os
   )  const
{
   os << " " << this->ShowType() << ":\n"
      << "    NumPoints:      " << this->mNumPoints << "\n"
      << "    Xmin:           " << this->mXmin << "\n"
      << "    Xmax:           " << this->mXmax << "\n"
      << "    Points:         " << this->mPoints << "\n"
      << "    Weights:        " << this->mWeights << "\n"
      << "    Error:          " << this->mLsError << "\n"
      << "    Relerr:         " << this->mLsError / std::sqrt(this->mNorm2) << "\n"
      << "    Norm2:          " << this->mNorm2 << "\n"
      << std::flush;
}

/**
 * Print info
 **/
template
   <  class T
   >
void LaplaceQuadratureBase<T>::PrintInfo
   (
   )  const
{
   this->PrintInfo(Mout);
}


/**
 * Factory for LaplaceQuadratureBase
 *
 * @param lapinfo    LaplaceInfo for determination of points and weights
 * @param interval   Interval where the quadrature should be optimized
 * @param eigen      Histogram of energies
 *
 * @return
 *    Pointer to (optimized) quadrature.
 **/
template
   <  class T
   >
std::unique_ptr<LaplaceQuadratureBase<T>> LaplaceQuadratureBase<T>::Factory
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  const std::pair<T,T>& interval
   ,  EnergyHistogram<T>&& eigen
   )
{
   bool comp = lapinfo.at("COMPLEXQUADRATURE").template get<bool>();
   bool incl_zero = ((interval.first < static_cast<T>(0.)) != (interval.second < static_cast<T>(0.)));

   if (  comp
      || incl_zero
      )
   {
      return std::unique_ptr<LaplaceQuadratureBase<T>>(new ComplexLaplaceQuadrature<T>(lapinfo, interval.first, interval.second, std::forward<EnergyHistogram<T>>(eigen)));
   }
   else
   {
      return std::unique_ptr<LaplaceQuadratureBase<T>>(new RealLaplaceQuadrature<T>(lapinfo, interval.first, interval.second, std::forward<EnergyHistogram<T>>(eigen)));
   }
}


/**
 * Constructor
 *
 **/
template
   <  class T
   >
RealLaplaceQuadrature<T>::RealLaplaceQuadrature
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  T xmin
   ,  T xmax
   ,  EnergyHistogram<T>&& eigen
   )
   :  LaplaceQuadratureBase<T>
         (  lapinfo
         ,  xmin
         ,  xmax
         ,  std::forward<EnergyHistogram<T>>(eigen)
         )
{
   this->mNorm2 = T(1.)/this->Xmin() - T(1.)/this->Xmax();

   this->ConstructLaplaceQuadrature(lapinfo);
}

/**
 * Type overload for RealLaplaceQuadrature
 **/
template
   <  class T
   >
typename LaplaceQuadratureBase<T>::quadID RealLaplaceQuadrature<T>::Type
   (
   )  const
{
   return LaplaceQuadratureBase<T>::quadID::REALLAP;
}

/**
 * ShowType overload for RealLaplaceQuadrature
 **/
template
   <  class T
   >
std::string RealLaplaceQuadrature<T>::ShowType
   (
   )  const
{
   return "RealLaplaceQuadrature";
}

/**
 * Calculate the Laplace error function something similar to the root mean square deviation of the fit
 *
 * @param points     Laplace points
 * @param weights    Laplace weights
 * @param thrshld    Threshold for numerical integration of error function
 *
 * @return
 *    Value of the error function
 **/
template
   <  class T
   >
T RealLaplaceQuadrature<T>::LeastSquaresError
   (  const std::vector<T>& points
   ,  const std::vector<T>& weights
   ,  T thrshld
   )
{
//   MidasWarning("Laplace weights are not refitted before error calculation. Is that fine?");

   bool numint = false;

   T xmin = this->Xmin();
   T xmax = this->Xmax();

   T result = static_cast<T>(0.);

   if (  numint
      )
   {
      result = this->IntegrateLaplaceErrorFunction(points, weights, thrshld, xmin, xmax);
   }
   else
   {
      /*
       * Calculate integral J = \int_xmin^xmax ( 1/x - \sum_q w_q exp(-\alpha_q x) )^2 dx
       * This is equal to:
       * J = \int_xmin^xmax 1/x dx + \sum_pq w_p w_q \int_xmin^xmax exp(-(\alpha_p + \alpha_q)*x) dx - 2*\sum_q w_q \int_xmin^xmax exp(-\alpha_q x)/x dx
       *   = 1/(2*xmin) - 1/(2*xmax) + \sum_pq w_p*w_q/(\alpha_p+\alpha_q) (exp(-(\alpha_p+\alpha_q)*xmin) - exp(-(\alpha_p+\alpha_q)*xmax)) - \sum_q w_q (Ei(-\alpha_q xmax) - Ei(-\alpha_q xmin))
       *   = A + B + C
       *
       * The first two terms are easy to integrate, and the last term is the exponential integral.
       */
      result = static_cast<T>(0.);

      // Term A
      result += static_cast<T>(0.5) / xmin;
      result -= static_cast<T>(0.5) / xmax;

      auto size = weights.size();
      for(size_t q = 0; q<size; ++q)
      {
         // Term C
         result -= weights[q] * (midas::util::ExpInt(-points[q]*xmax) - midas::util::ExpInt(-points[q]*xmin));

         // Term B, diagonal
         auto two_alphq = static_cast<T>(2.)*points[q];
         result += static_cast<T>(0.5)*(std::pow(weights[q], 2) / two_alphq) * (std::exp(-two_alphq*xmin) - std::exp(-two_alphq*xmax));

         // Term B, off-diagonal
         for(size_t p = q+1; p<size; ++p)
         {
            auto sumpoints = points[p] + points[q];
            result += (weights[p]*weights[q] / sumpoints) * (std::exp(-sumpoints*xmin) - std::exp(-sumpoints*xmax));
         }
      }
   }

   if (  libmda::numeric::float_neg(result)
      )
   {
      Mout  << " Points:   " << this->mPoints << "\n"
            << " Weights:  " << this->mWeights << "\n"
            << std::flush;
      MIDASERROR("Negative result in RealLaplaceQuadrature::LeastSquaresError!!!");
   }

   // Set error
   this->mLsError = result;

   return result;
}

/**
 * Calculate the Laplace error function via numerical Romberg integration.
 * NB: Hard-coded for real Laplace (interval does not include zero)
 *
 * @param points     Laplace points
 * @param weights    Laplace weights
 * @param thrshld    Threshold for numerical integration of error function
 * @param xmin       Min of interval (smallest element)
 * @param xmax       Max of interval (largest element)
 *
 * @return
 *    Value of the error function
 **/
template
   <  class T
   >
T RealLaplaceQuadrature<T>::IntegrateLaplaceErrorFunction
   (  const std::vector<T>& points
   ,  const std::vector<T>& weights
   ,  T thrshld
   ,  T xmin
   ,  T xmax
   )  const
{
   //----------------------------------------------------------------+
   // F = int_xmin^xmax [1/x - sum_i w_i exp(-x*t_i)]^2 dx
   //
   // do (numerical) integration via Romberg method
   //----------------------------------------------------------------+
  
   int numpoints = points.size();
   auto func = [](T x) { return static_cast<T>(1.) / x; };
   int maxorder = 50;
   int maxorder1 = maxorder + 1;

   std::unique_ptr<T[]> sumerr(new T[maxorder1]);

   T xrom[maxorder1+1][maxorder1+1];

   sumerr[0] = 0.0;
   T approx  = 0.0;

   {
      approx = 0.0;
      for (int k = 0; k < numpoints; k++)
      {
         approx += weights[k] * exp( - points[k] * xmin);
      }
      T errr = func(xmin) - approx;
      sumerr[0] += errr * errr;
   }

   {
      approx = 0.0;
      for (int k = 0; k < numpoints; k++)
      {
         approx += weights[k] * exp( - points[k] * xmax);
      }
      T errr = func(xmax) - approx;
      sumerr[0] += errr*errr;
   }

   xrom[1][1] = sumerr[0] * (xmax - xmin) / 2.0;

   int ninterv = 2;
   for (int norder = 1; norder <= maxorder; norder++)
   {
      T xstep = (xmax - xmin) / ninterv;

      sumerr[norder] = 0.0;
      for (int istep = 1; istep <= ninterv - 1; istep += 2)
      {
         T xval  = xmin + istep * xstep;

         approx = 0.0;
         for (int k = 0; k < numpoints; k++)
         {
            approx += weights[k] * exp( - points[k] * xval);
         }
         //T errr = 1.0 / xval - approx;
         T errr = func(xval) - approx;
         sumerr[norder] += 2.0 * errr*errr;
      }

      T xinte = 0.0;
      for (int k = 0; k <= norder; k++)
      {
         xinte += sumerr[k];
      }

      xrom[norder + 1][1] = xinte * (xmax - xmin) / (2.0 * ninterv);

      for (int k = 2; k <= norder + 1; k++)
      {
         int j = 2 + norder - k;
         xrom[j][k] = ( std::pow(4.0, k - 1) * xrom[j+1][k-1] - xrom[j][k-1] )  /  ( std::pow(4.0, k - 1) - 1.0);
      }

      T errest = abs( xrom[1][norder+1] - xrom[1][norder] ); 

      if ( errest < thrshld && norder > 10)
      {
         return xrom[1][norder+1];
      }
      else if (norder == maxorder) 
      {
         Mout << "erest " << errest << std::endl;
         MIDASERROR("Bad numerical integration for Laplace error function!");
      }
     
      ninterv = ninterv * 2;
   }

   MIDASERROR("How did I get here?");
   return static_cast<T>(0.);
}


/**
 * Read hard-coded points and weights from Almlöf paper
 *
 * @param aNumPoints    Number of points to read
 **/
template
   <  class T
   >
void RealLaplaceQuadrature<T>::ReadHardCodedData
   (  unsigned aNumPoints
   )
{
   // Set up points and weights
   switch(  aNumPoints  )
   {
      case(1):
         this->mPoints =std::vector<T>{0.2543066};
         this->mWeights=std::vector<T>{0.7443778};
         break;                      
      case(2):                       
         this->mPoints =std::vector<T>{0.1057374,0.8263985};
         this->mWeights=std::vector<T>{0.2998529,1.3750404};
         break;                      
      case(3):                       
         this->mPoints =std::vector<T>{0.0664618,0.4380785,1.6101925};
         this->mWeights=std::vector<T>{0.1793919,0.6329602,1.9996848};
         break;                      
      case(4):                       
         this->mPoints =std::vector<T>{0.0135967,0.1646374,0.6569702,2.0357815};
         this->mWeights=std::vector<T>{0.0527171,0.2757647,0.7858024,2.2786679};
         break;                     
      case(5):                      
         this->mPoints =std::vector<T>{0.0064785,0.0669154,0.3089156,0.9478808,2.5786393};
         this->mWeights=std::vector<T>{0.0177806,0.1278500,0.3878375,0.9741098,2.6125732};
         break;                     
      case(6):                      
         this->mPoints =std::vector<T>{0.0060050,0.0553016,0.2346500,0.6587781,1.5760606,3.6502575};
         this->mWeights=std::vector<T>{0.0164421,0.0995694,0.2759458,0.6106806,1.3178664,3.1857008};
         break;                     
      case(7):                      
         this->mPoints =std::vector<T>{0.0035243,0.0251939,0.1193583,0.3653936,0.8850526,1.9454617,4.2401468};
         this->mWeights=std::vector<T>{0.0095904,0.0437062,0.1566528,0.3542937,0.7264528,1.4928836,3.4634348};
         break;                     
      case(8):                      
         this->mPoints =std::vector<T>{0.0034250,0.0238627,0.1023430,0.2947359,0.6736046,1.3821837,2.7133497,5.4051090};
         this->mWeights=std::vector<T>{0.0093035,0.0390022,0.1267465,0.2692945,0.5112993,0.9512673,1.8163835,3.9522146};
         break;
      default:
         MIDASERROR("Invalid number of Laplace quadrature points");
         break;
   }
}


/**
 * Construct default guess for quadrature.
 *
 * @return
 *    True if no error occured
 **/
template
   <  class T
   >
bool RealLaplaceQuadrature<T>::DefaultGuess
   (
   )
{
   unsigned hc_points = std::min(this->NumPoints(), static_cast<unsigned int>(8));

   this->ReadHardCodedData(hc_points);

   if (  hc_points < this->NumPoints()
      )
   {
      // Add new start vectors just by multiplying last point by 10
      // (extremely stupid guess, but for the moment we don't have more and hopefully this code is never invoked)
      for(unsigned ilap = hc_points - 1; ilap < this->NumPoints(); ++ilap)
      {
         Nb newpoint  = this->mPoints[ilap] * 10.0;
         this->mPoints.push_back(newpoint);
         this->mWeights.push_back(1.0);
      }
   }

   return true;
}

/**
 * Read in exponents and weights from file.
 *
 * @param aAllowMorePoints
 *    Allow the algorithm to increment the number of points if an appropriate file is not found.
 *    Note that the best [1:R] approximation for a fixed number of points is also the best [1:inf) approx.
 *
 * @return
 *    True if a good file was found
 **/
template
   <  class T
   >
bool RealLaplaceQuadrature<T>::ReadBestDataFile
   (  bool aAllowMorePoints
   )
{
   // Initialize set of filenames if not done already
   if (  RealLaplaceQuadrature<T>::mDataFileSet.empty()
      ) 
   {
      RealLaplaceQuadrature::InitDataFileSet();
   }
   if (  RealLaplaceQuadrature<T>::mMaxIntervals.empty()
      )
   {
      RealLaplaceQuadrature<T>::InitMaxIntervals();
   }

   bool file_found = false;
   const int max_num_points = aAllowMorePoints ? 21 : this->NumPoints();      // 21 is the highest number of points for which we have all data
   unsigned num_points = this->NumPoints();
   std::string s_filename;

   Nb r = this->Xmax() / this->Xmin();
   if (  std::round(r) < C_2
      )
   {
      MidasWarning("No Laplace data exists for such small interval! We try with the smallest possible.");
      Mout << " Xmax / Xmin = " << r << std::endl;

      // Set r = 2 to be able to find a file
      r = C_2;
   }

   while (  !file_found )
   {
      const Nb max_interval = RealLaplaceQuadrature<T>::mMaxIntervals.at(num_points);
      s_filename = midas::tensor::detail::generate_file_name(num_points, r);

      // If file exists, but is not available: Unpack the .tar.gz file in INSTALL_DATADIR
      if (  !InquireFile(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + std::string("/")+s_filename)
         && RealLaplaceQuadrature<T>::DataFileExists(s_filename)
         )
      {
         std::string tar_file_path = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/1_x_data.tar.gz";
         
         if (  InquireFile(tar_file_path)
            )
         {
            system_command_t command = {"tar", "-xzvf", tar_file_path, "-C", input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR")};
            int status = MIDASSYSTEM(command);
            if(status != 0)
            {
               MIDASERROR  (  "Could not unpack '1_x_data.tar.gz'.\n" 
                              "Do you have write permissions for '" + input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "'?\n"  
                              "Using command: '" + command_to_string(command) + "'."
                           );
            }
         }
         else
         {
            MIDASERROR("Data for LaplaceQuadrature not found in MIDAS_INSTALL_DATADIR. This should not happen!");
         }
      }
      // If file does not exist: Increment number of points or use best at fixed number of points
      else if  (  !RealLaplaceQuadrature<T>::DataFileExists(s_filename)
               )
      {
         // If num_points has reached the maximum value for which we have data
         if (  num_points >= max_num_points
            )
         {
            if (  r > max_interval
               )
            {
               MidasWarning("LaplaceQuadrature: No existing data file for requested interval! Using maximum interval instead, which is optimal for [1;inf)");
               Mout << " Num points  = " << num_points << "\n"
                    << " Xmax / Xmin = " << r << std::endl;
               s_filename = midas::tensor::detail::generate_file_name(num_points, RealLaplaceQuadrature<T>::mMaxIntervals.at(num_points));
               file_found = true;
            }
            // If the initial number of points is too high: Go back to 4 where all small intervals are represented
            else
            {
               MidasWarning("LaplaceQuadrature: Decreasing number of points to 4 because of small interval!");
               num_points = 4;
            }
         }
         else
         {
            ++num_points;
         }
      }
      // We have a match.
      else
      {
         file_found = true;
      }
   }
   

   // Set mNumPoints after finding the file
   this->mNumPoints = num_points;
   this->mWeights.reserve(num_points);
   this->mPoints.reserve(num_points);

   // Read in point and weights from file and divide by mXmin
   std::ifstream datafile(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + std::string("/")+ s_filename, std::ifstream::in);
   datafile.exceptions( std::ifstream::failbit | std::ifstream::badbit );

   std::string s_tmp;
   for(int i=0; i<this->mNumPoints; ++i)
   {
      std::getline(datafile, s_tmp, '{');
      datafile.ignore(256,'\n');
      // Don't throw warning if all decimals cannot be read into Nb
      this->mWeights.emplace_back(midas::util::FromString<Nb>(s_tmp,false)/this->Xmin());
   }
   for(int i=0; i<this->mNumPoints; ++i)
   {
      std::getline(datafile, s_tmp, '{');
      datafile.ignore(256,'\n');
      // Don't throw warning if all decimals cannot be read into Nb
      this->mPoints.emplace_back(midas::util::FromString<Nb>(s_tmp,false)/this->Xmin());
   }

   datafile.close();

   return true;

   // Gunnar's implementation of basically the same thing. Do we need this?
//      // use pretabulated data to get points
//
//      // Initialize set of filenames if not done already
//      if(LaplaceQuadrature::mDataFileSet.empty()) LaplaceQuadrature::InitDataFileSet();
//
//      T flapold = 100000;
//      bool found = false;
//
//      for (const auto& sfile: mDataFileSet)
//      {
//
//         // Example of filename 1_xk50_4E8
//         int num_points = std::stoi(sfile.substr(4,2));
//         int ibase = std::stoi(sfile.substr(7,1));  
//         int iexpn = std::stoi(sfile.substr(9,1)); 
//
//         T rlim = static_cast<T>(ibase) * std::pow(10, iexpn);
//
//         if (rlim > denom_range && num_points == nlap)
//         {
//
//            std::vector<Nb> savepoints = mPoints;
//            std::vector<Nb> saveweights = mWeights;
//
//            // Set mNumPoints 
//            this->mNumPoints = num_points;
//            this->mWeights.resize(num_points);
//            this->mPoints.resize(num_points);
//           
//            this->mPoints.clear();
//            this->mWeights.clear();
//
//            // Read in point and weights from file and divide by aMinElement
//            std::ifstream datafile(INSTALL_DATADIR + sfile, std::ifstream::in);
//            datafile.exceptions( std::ifstream::failbit | std::ifstream::badbit );
//
//            std::string s_tmp;
//            for(int i=0; i<this->mNumPoints; ++i)
//            {
//               std::getline(datafile, s_tmp, '{');
//               datafile.ignore(256,'\n');
//               Nb dweight = midas::util::FromString<Nb>(s_tmp,false)/aMinElement;
//               // Don't throw warning if all decimals cannot be read into Nb
//               this->mWeights.push_back(dweight);
//            }
//            for(int i=0; i<this->mNumPoints; ++i)
//            {
//               std::getline(datafile, s_tmp, '{');
//               datafile.ignore(256,'\n');
//               Nb dpoint = midas::util::FromString<Nb>(s_tmp,false)/aMinElement;
//               // Don't throw warning if all decimals cannot be read into Nb
//               this->mPoints.push_back(dpoint);
//            }
//
//            datafile.close();
//
//            found = true;
//
//            T flap = GetLaplaceError();  
//
//            if (flap < flapold) 
//            {
//               flapold = flap;
//            }     
//            else
//            {
//               mPoints = savepoints;
//               mWeights = saveweights;
//            }
//         } 
//      }
//
//      if (!found)
//      {
//         Mout << "Did not find a good pretabulated start guess for Laplace quadrature. I will make my own (stupid) guess" << std::endl;
//         MakeGuess(nlap, denom_range, aMinElement, false);
//      }  
}


/**
 * Set optimal weights for input points.
 *
 * @param points           The current points.
 * @param singular         True on exit if the matrix is singular.
 *
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* RealLaplaceQuadrature<T>::RefitWeights
   (  const std::vector<T>& points
   ,  bool& singular
   )
{
   const bool locdbg = false;

   const auto& eigen_distrib = this->EigDistrib();

   if (  eigen_distrib.empty()
      )
   {
      MIDASERROR("We need an EnergyHistogram in order to refit weights!");
   }

   int numpoints = points.size();

   std::vector<T> weights(numpoints);

   singular = false;

   // Find weights by solving Aw=b
   // Compute matrix A
   std::unique_ptr<T[]> amatrix(new T[numpoints * numpoints]);
   {
      T* p = amatrix.get();
      for(auto tq: points)
      {
         for(auto tp: points)
         {
            *p=0.0;
            for(auto u: eigen_distrib)
            {
               *p+=u.second*exp(-u.first*(tp+tq));
            }
            ++p;
         }
      }
   }
   // Compute rhs vector b (put it in weights)
   std::unique_ptr<T[]> w(new T[numpoints]);
   {
      T* p=w.get();
      for(auto tq: points)
      {
         *p=0.0;
         for(auto u: eigen_distrib)
         {
            *p+=u.second * exp(-tq*u.first)/u.first;
         }
         ++p;
      }
   }
   
   // save the A matrix and the b vector before solving the linear system
   std::unique_ptr<T[]> amat(new T[numpoints * numpoints]);
   std::unique_ptr<T[]> bvec(new T[numpoints]);

   memcpy(amat.get(),amatrix.get(),numpoints * numpoints * sizeof(T));
   memcpy(bvec.get(),w.get(), numpoints * sizeof(T));

   // Aaaand solve linear system 
   int m = numpoints;
   int nrhs = 1;
   int info = 0;
   char nc = 'N';
   std::unique_ptr<int[]> ipiv(new int[numpoints * numpoints]);

   // Get LU decompostion and save it on amtrix
   midas::lapack_interface::getrf(&m, &m, amatrix.get(), &m, ipiv.get(), &info);

   // some sanity checks
   if (info < 0) Mout << "illegal value in dgetrf" << std::endl;
   if (info > 0) Mout << "singular matrix in dgetrf" << std::endl;
   //if (info != 0) MIDASERROR("Problem to determine weights for Laplace quadrature");

   // Use LU decomposition to solve linear system
   midas::lapack_interface::getrs(&nc, &m, &nrhs, amatrix.get(), &m, ipiv.get(), w.get(), &m, &info);
   //if (info != 0) MIDASERROR("Problem to determine weights for Laplace quadrature");

   if (info != 0) 
   {
      singular = true;
   }

   {
      T* p=w.get();
      for(auto& it: weights)
      {
         it=*(p++);
      }
   }

   // calculate residual r = b - A*w
   std::unique_ptr<T[]> res(new T[numpoints]);
   {
      T* pr = res.get();
      T* pa = amat.get();
      T* pb = bvec.get();
      for (auto tq: points)
      {
         *(pr) = *(pb);

         T* pw = w.get();
         for (auto tp: points)
         {
            *(pr) = *(pr) - (*(pa++) * *(pw++));
         }  
         ++pr;
         ++pb;     
      }

      {
         T* pres = res.get();
         T rnrm = 0.0;
         for (auto tq: points)
         {
            T dval = *(pres++);
            rnrm += dval * dval;
         }
         rnrm = std::sqrt(rnrm);
         if (locdbg)
         { 
            Mout << "residual before adding the correction:" << std::endl;
            Mout << "||R||" << rnrm << std::endl;
         }
      }
   }

   //-----------------------------------------------------------------------
   // calculate correction for weights by solving B dw = r
   //-----------------------------------------------------------------------

   // Use LU decomposition to solve linear system
   midas::lapack_interface::getrs(&nc,&m,&nrhs,amatrix.get(),&m,ipiv.get(),res.get(),&m,&info);

   if (info != 0)
   {
      singular = true;
   }

   // update weights
   {
      T* pw  = w.get();
      T* pdw = res.get();
      for(auto& it: weights)
      {
         it = *(pw++) + *(pdw++);
      }
   }

   // recalculate residual
   std::unique_ptr<T[]> dres(new T[numpoints]);
   {
      T* pr = dres.get();
      T* pa = amat.get();
      T* pb = bvec.get();
      for (auto tq: points)
      {
         *(pr) = *(pb);

         T* pw = w.get();
         T* pdw = res.get();
         for (auto tp: points)
         {
            *(pr) = *(pr) - (*(pa++) * (*(pw++) + *(pdw++)) );
         }  
         ++pr;
         ++pb;     
      }

      {
         T* pres = dres.get();
         T rnrm = 0.0;
         for (auto tq: points)
         {
            T dval = *(pres++);
            rnrm += dval * dval;
         }
         rnrm = std::sqrt(rnrm);
         if (locdbg)
         {
            Mout << "residual after adding the correction:" << std::endl;
            Mout << "||R||" << rnrm << std::endl;
         }
      }
   }

   this->mWeights = weights;

   return this;
}


/**
 * Make mode-matrix for EnergyDenominatorTensor for electronic-structure calculations
 *
 * @param eigen      Vector of orbital energies
 * @param shift_part Part of the energy shift distributed onto this matrix
 * @param result     Pointer to the result
 * @param real       Calculate real part (always true here).
 **/
template
   <  class T
   >
void RealLaplaceQuadrature<T>::MakeMatrix
   (  const std::vector<T>& eigen
   ,  T shift_part
   ,  T* result
   ,  bool real
   )  const
{
   if (  !real
      )
   {
      MIDASERROR("Asking for imaginary part of RealLaplaceQuadrature!");
   }

   T* ptr=result;
   T x = static_cast<T>(0.);
   for(const auto& point : this->mPoints)
   {
      for(const auto& e: eigen)
      {
         // If we have changed sign of the interval, x must change sign
         x  =  this->NegativeInterval()
            ?  shift_part - e
            :  e - shift_part;
         *(ptr++)= exp(-point * x );
      }
   }
}

/**
 * Make mode-matrix for EnergyDenominatorTensor for vibrational calculations
 *
 * @param aEigVals         VSCF eigenvalues
 * @param aOffsets         Offsets of eigenvalues for different modes (where to start for mode i)
 * @param aModeNumber      The mode in question
 * @param aExtent          The dimension of the matrix
 * @param aDiagEnerPart    The part of the shift that will be subtracted in this mode matrix
 * @param result           Pointer to result
 * @param real             Calculate real part (always true here).
 **/
template
   <  class T
   >
void RealLaplaceQuadrature<T>::MakeMatrix
   (  const DataCont& aEigVals
   ,  const std::vector<In>& aOffsets
   ,  In aModeNumber
   ,  unsigned aExtent
   ,  T aDiagEnerPart
   ,  T* result
   ,  bool real
   )  const
{
   if (  !real
      )
   {
      MIDASERROR("Asking for imaginary part of RealLaplaceQuadrature!");
   }

   T* ptr = result;
   Nb val = C_0;
   T x = static_cast<T>(0.);
   for(const auto& point : this->mPoints)
   {
      for(In imodal=I_0; imodal<aExtent; ++imodal)
      {
         aEigVals.DataIo(IO_GET, aOffsets[aModeNumber]+imodal+1, val);
         // If we have changed sign of the interval, x must change sign
         x  =  this->NegativeInterval()
            ?  aDiagEnerPart - static_cast<T>(val)
            :  static_cast<T>(val) - aDiagEnerPart;
         *(ptr++) = exp(-point * x);
      }
   }
}

/**
 * Make plot data
 *
 * @param aName      Name of file
 * @param aDelta     Point spacing
 **/
template
   <  class T
   >
void RealLaplaceQuadrature<T>::MakePlot
   (  const std::string& aName
   ,  T aDelta
   )  const
{
   std::ofstream output( aName );

   output.precision(16);

   size_t nb_width = output.precision() + 9;

   output   << "# " 
            << std::setw(nb_width) << "x" 
            << std::setw(nb_width) << "approx" 
            << std::setw(nb_width) << "exact" 
            << std::endl;

   T min = this->Xmin();
   T max = this->Xmax();

   for(T x=min; x <= max; x+=aDelta)
   {
      T exact = T(1.) / x;

      T approx = T(0.);
      for(size_t q=0; q<this->NumPoints(); ++q)
      {
         approx += this->Weights()[q]*std::exp(-this->Points()[q]*x);
      }

      output   << "  " 
               << std::setw(nb_width) << x 
               << std::setw(nb_width) << approx 
               << std::setw(nb_width) << exact 
               << std::endl;
   }

   output.close();
}


/**
 * Constructor for ComplexLaplaceQuadrature
 **/
template
   <  class T
   >
ComplexLaplaceQuadrature<T>::ComplexLaplaceQuadrature
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  T xmin
   ,  T xmax
   ,  EnergyHistogram<T>&& eigen
   )
   :  LaplaceQuadratureBase<T>
         (  lapinfo
         ,  xmin
         ,  xmax
         ,  std::forward<EnergyHistogram<T>>(eigen)
         )
   ,  mGamma(lapinfo.at("COMPLEXSHIFT").template get<Nb>())
   ,  mL(std::max(std::abs(xmin), std::abs(xmax)))
{
//   this->mNorm2 = static_cast<T>(0.5) * ( (std::atan2(mL, mGamma) / mGamma) - (mL / (mL*mL + mGamma*mGamma)) );
   this->mNorm2 = std::atan2(mL, mGamma) / mGamma;

   this->ConstructLaplaceQuadrature(lapinfo);
}

///**
// * Copy constructor for ComplexLaplaceQuadrature
// **/
//template
//   <  class T
//   >
//ComplexLaplaceQuadrature<T>::ComplexLaplaceQuadrature
//   (  const ComplexLaplaceQuadrature<T>& other
//   )
//   :  LaplaceQuadratureBase<T>
//         (  other
//         )
//   ,  mGamma(other.mGamma)
//{
//}
//
///**
// * Move constructor for ComplexLaplaceQuadrature
// **/
//template
//   <  class T
//   >
//ComplexLaplaceQuadrature<T>::ComplexLaplaceQuadrature
//   (  ComplexLaplaceQuadrature<T>&& other
//   )
//   :  LaplaceQuadratureBase<T>
//         (  std::forward<ComplexLaplaceQuadrature<T>>(other)
//         )
//   ,  mGamma(std::move(other.mGamma))
//{
//}
//
///**
// * Move assignment for ComplexLaplaceQuadrature
// **/
//template
//   <  class T
//   >
//ComplexLaplaceQuadrature<T>& ComplexLaplaceQuadrature<T>::operator=
//   (  ComplexLaplaceQuadrature<T>&& other
//   )
//{
//   LaplaceQuadratureBase<T>::operator=(std::forward<ComplexLaplaceQuadrature<T>>(other));
//   this->mGamma = std::move(other.mGamma);
//
//   return *this;
//}

/**
 * Read data from library
 *
 * @param aAllowMorePoints    Allow the algorithm to increment the number of points
 *
 * @return
 *    True if file was found.
 **/
template
   <  class T
   >
bool ComplexLaplaceQuadrature<T>::ReadBestDataFile
   (  bool aAllowMorePoints
   )
{
   MidasWarning("No data available for ComplexLaplaceQuadrature. Use default guess!");
   return false;
}

/**
 * Make default guess
 *
 * @return
 *    True if no error occured
 **/
template
   <  class T
   >
bool ComplexLaplaceQuadrature<T>::DefaultGuess
   (
   )
{
   this->mPoints.resize(this->NumPoints());
   this->mWeights.resize(this->NumPoints());

   this->mPoints[this->NumPoints()-1] = static_cast<T>(1.) / this->mGamma; 

   // Use gamma: p[i] = 10^{-i} / gamma
   size_t count = 0;
   for(size_t i=0; i<this->NumPoints(); ++i)
   {
      this->mPoints[i] = std::pow(static_cast<T>(10.), -static_cast<T>(i)) / this->mGamma;
      ++count;

      if (  this->mPoints[i] < static_cast<T>(10.)
         )
      {
         break;
      }
   }

   // Use a Fourier sine series for the rest
   for(size_t i=0; i<this->NumPoints()-count; ++i)
   {
      this->mPoints[count+i] = (i+1)*C_PI / this->mL;
   }

   // Sort the input
   std::sort(this->mPoints.begin(), this->mPoints.end());

   Mout  << " Points after default guess:\n" << this->mPoints << std::endl;

   bool fail = false;
   this->RefitWeights(this->mPoints, fail);

   Mout  << " Weights after default guess:\n" << this->mWeights << std::endl;

   return !fail;
}

/**
 * Calculate A matrix for ComplexLaplaceQuadrature weight optimization.
 *
 * @param aPoints    Points to calculate matrix for
 *
 * @return
 *    Pointer to this.
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* ComplexLaplaceQuadrature<T>::CalculateA
   (  const std::vector<T>& aPoints
   )
{
   auto npoints = aPoints.size();
   if (  this->mA.Nrows() != npoints 
      || this->mA.Ncols() != npoints 
      )
   {
      this->mA.SetNewSize(npoints, false, true);
   }

   for(size_t i=0; i<npoints; ++i)
   {
      // Set diagonal terms
      const auto& pi = aPoints[i];
      this->mA[i][i] = std::exp(-mGamma*T(2.)*pi) * this->mL;
//      this->mA[i][i] = static_cast<T>(0.5)*std::exp(-mGamma*two_p) * ( mL - std::sin(static_cast<T>(2.)*pi*mL)/(static_cast<T>(2.)*pi) );

      // Set off-diagonal terms (symmetric matrix)
      for(size_t j=i+1; j<npoints; ++j)
      {
         const auto& pj = aPoints[j];
         auto sum = pi + pj;
         auto diff = pi - pj;

         auto elem = std::exp(-mGamma*sum)*(std::sin(mL*diff)/(diff));
//         auto elem = static_cast<T>(0.5)*std::exp(-mGamma*sum)*(std::sin(mL*diff)/(diff) - std::sin(mL*sum)/(sum));

         this->mA[i][j] = elem;
         this->mA[j][i] = elem;
      }
   }

//   Mout  << " Laplace A matrix:\n" << this->mA << std::endl;

   return this;
}

/**
 * Calculate B vector
 *
 * @param aPoints    Points to calculate B for.
 *
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* ComplexLaplaceQuadrature<T>::CalculateB
   (  const std::vector<T>& aPoints
   )
{
   auto npoints = aPoints.size();
   if (  this->mB.Size() != npoints 
      )
   {
      this->mB.SetNewSize(npoints);
   }

   /***********************************************************/
   /* Use GSL for performing adaptive integration             */
   /***********************************************************/
#ifdef ENABLE_GSL
   // Lambda function for GSL qags
   // params is a std::array containing the Laplace point and the gamma parameter.
   auto f = [](Nb x, void* params)
   {
      auto p = *static_cast<std::array<Nb, 2>*>(params);

      auto result = (x*std::sin(p[0]*x) + p[1]*std::cos(p[0]*x)) / (x*x + p[1]*p[1]);
//      auto result = (x*std::sin(p[0]*x)) / (x*x + p[1]*p[1]);

//      std::cout   << " f(" << x << ") = " << result << std::endl;

      return result;
   };

   gsl_function F;
   F.function = f;

   // Lambda function for GSL qawo
   auto f_wo_sin = [](Nb x, void* params)
   {
      auto p = *static_cast<std::array<Nb, 2>*>(params);

      auto result = x/(x*x + p[1]*p[1]);

//      std::cout   << " f_wo(" << x << ") = " << result << std::endl;

      return result;
   };
   auto f_wo_cos = [](Nb x, void* params)
   {
      auto p = *static_cast<std::array<Nb, 2>*>(params);

      auto result = p[1]/(x*x + p[1]*p[1]);

//      std::cout   << " f_wo(" << x << ") = " << result << std::endl;

      return result;
   };

   gsl_function F_wo_sin;
   F_wo_sin.function = f_wo_sin;

   gsl_function F_wo_cos;
   F_wo_cos.function = f_wo_cos;

   // Init result and error (must be double in gsl)
   double result = 0.;
   double error = 0.;

   // Frequency threshold for using qawo instead of qags
   T qawo_thresh = T(1.e2);

   // Size of workspace
   size_t work_size = 10000;

   // Thresholds for integration
   double abs = 1.e-20;
   double rel = 1.e-9;

   // Init params
   std::array<Nb, 2> params = {{ static_cast<Nb>(aPoints[0]), static_cast<Nb>(this->mGamma) }};

   // Init workspace and qawo table (if needed)
   auto* work = gsl_integration_workspace_alloc(work_size);
   gsl_integration_qawo_table* tab = nullptr;

   if (  std::any_of(aPoints.begin(), aPoints.end(), [&qawo_thresh](T p){ return libmda::numeric::float_gt(p, qawo_thresh); })
      )
   {
      tab = gsl_integration_qawo_table_alloc(params[0], this->mL, GSL_INTEG_SINE, work_size);
   }

   // Calculate integrals
   for(size_t i=0; i<npoints; ++i)
   {
      // Set params
      params[0] = aPoints[i];
   
      // Use qags for normal case
      if (  params[0] < qawo_thresh
         )
      {
         F.params = &params;
         auto status = gsl_integration_qags
            (  &F
            ,  0.
            ,  this->mL
            ,  abs
            ,  rel
            ,  work_size
            ,  work
            ,  &result
            ,  &error
            );

         if (  status != 0
            )
         {
            MIDASERROR("gsl_integration_qags failed with code: " + std::to_string(status));
         }
      }
      // Use qawo for high oscillations
      else
      {
         double tmp_result = 0.;

         // Integrate sine part first
         {
            gsl_integration_qawo_table_set(tab, params[0], this->mL, GSL_INTEG_SINE);

            F_wo_sin.params = &params;

            auto status = gsl_integration_qawo
               (  &F_wo_sin
               ,  0.
               ,  abs
               ,  rel
               ,  work_size
               ,  work
               ,  tab
               ,  &tmp_result
               ,  &error
               );

            if (  status != 0
               )
            {
               MIDASERROR("gsl_integration_qawo sine failed with code: " + std::to_string(status));
            }

            result += tmp_result;
         }
         // Integrate cosine part second
         {
            tmp_result = 0.;

            gsl_integration_qawo_table_set(tab, params[0], this->mL, GSL_INTEG_COSINE);

            F_wo_cos.params = &params;

            auto status = gsl_integration_qawo
               (  &F_wo_cos
               ,  0.
               ,  abs
               ,  rel
               ,  work_size
               ,  work
               ,  tab
               ,  &tmp_result
               ,  &error
               );

            if (  status != 0
               )
            {
               MIDASERROR("gsl_integration_qawo cosine failed with code: " + std::to_string(status));
            }

            result += tmp_result;
         }
      }

      // Set element
      this->mB[i] = result * std::exp(-mGamma*aPoints[i]);
   }

   // Free table
   if (  tab
      )
   {
      gsl_integration_qawo_table_free(tab);
   }

   // Free workspace
   gsl_integration_workspace_free(work);
#else
   MIDASERROR("Only implemented with GSL! We need an interface to e.g. boost quadrature to make this work.");
#endif /* ENABLE_GSL */

//   Mout  << " Laplace B vector:\n" << this->mB << std::endl;

   return this;
}

/**
 * Refit weights
 *
 * @param points     Points to fit weights to
 * @param singular   True if matrix is singular
 *
 * @return
 *    Pointer to this
 **/
template
   <  class T
   >
LaplaceQuadratureBase<T>* ComplexLaplaceQuadrature<T>::RefitWeights
   (  const std::vector<T>& points
   ,  bool& singular
   )
{
   // Calculate matrix elements
   this->CalculateA(points);
   this->CalculateB(points);

//   T rcond = static_cast<T>(1.e-12);
//   auto lin_sol = GELSS(this->mA, this->mB, rcond);
   auto lin_sol = GESV(this->mA, this->mB);

   singular = (lin_sol.info != 0);

   if (  singular
      )
   {
      MIDASERROR("ComplexLaplaceQuadrature::RefitWeights: Linear solution failed with info: " + std::to_string(lin_sol.info));
   }

   for(size_t i=0; i<this->NumPoints(); ++i)
   {
      this->mWeights[i] = std::move(lin_sol.solution[i]);
   }

   Mout  << " Fitted weights:\n" << this->mWeights << std::endl;

   return this;
}


/**
 * Calculate error defined as: \f$ err = \sqrt{2J} \f$, where \f$ J = \frac{1}{2} \int_0^L dx \left| \frac{1}{x-i\gamma} - i\sum_q w_q e^{-\gamma \alpha_q} e^{-i \alpha_q x} \right|^2 \f$ .
 *
 * @param points
 * @param weights
 * @param thrshld    Threshold for numerical integration
 *
 * @return
 *    Least-squares error
 **/
template
   <  class T
   >
T ComplexLaplaceQuadrature<T>::LeastSquaresError
   (  const std::vector<T>& points
   ,  const std::vector<T>& weights
   ,  T thrshld
   )
{
   // We assume, that the weights have just been fitted such that the A and B elements are fine...

//   MidasWarning("Re-calculating A and b for least-squares error. This is not necessary!");
//   this->CalculateA(points);
//   this->CalculateB(points);

   Mout  << " Calculate Laplace error:\n"
         << "    Npts:  " << this->NumPoints() << "\n"
         << "    L:     " << this->mL << "\n"
         << "    N:     " << this->mNorm2 << "\n"
         << std::flush;
   auto npoints = points.size();
   assert(points.size() == weights.size());

   std::vector<T> err_vec;
   size_t terms = 1 + 2*npoints + size_t(T(npoints-1)/2.);
   err_vec.reserve(terms);

   // Add norm term
   err_vec.emplace_back(this->mNorm2);

   for(size_t i=0; i<npoints; ++i)
   {
      const auto& wi = weights[i];

      // Subtract term from b vector
      err_vec.emplace_back(-T(2.)*wi*this->mB[i]);

      // Add diagonal A-matrix terms
      err_vec.emplace_back(wi*wi*this->mA[i][i]);

      // Add off-diagonal A-matrix terms (symmetric)
      for(size_t j=i+1; j<npoints; ++j)
      {
         const auto& wj = weights[j];

         err_vec.emplace_back(T(2.)*wi*wj*this->mA[i][j]);
      }
   }

   // Sort error vector
   std::sort(err_vec.begin(), err_vec.end(), [](T a, T b) { return std::abs(a) < std::abs(b); });

   // Get result using Neumaier summation algorithm
   T err2 = err_vec[0];
   volatile T c = T(0.);
   for(size_t i=1; i<err_vec.size(); ++i)
   {
      T ei = err_vec[i];
      T t = err2 + ei;
      if (  libmda::numeric::float_gt(std::abs(err2), std::abs(ei))
         )
      {
         c += (err2 - t) + ei;
      }
      else
      {
         c += (ei - t) + err2;
      }
      err2 = t;
   }
   err2 += c;
//   T err2 = std::accumulate(err_vec.begin(), err_vec.end(), T(0.));

   if (  libmda::numeric::float_neg(err2)
      )
   {
      Mout  << " !!!!!!!! NB: Negative error calculated !!!!!!!!" << std::endl;
      Mout  << " Sorted error terms before summation:" << std::endl;
      for(const auto& e : err_vec)
      {
         Mout  << e << std::endl;
      }
      Mout  << "    err2: " << err2 << "\n"
            << "    err2/norm2: " << err2 / this->mNorm2 << "\n"
            << std::flush;

      Mout  << " Points:   " << points << "\n"
            << " Weights:  " << weights << "\n"
            << " A matrix:\n" << this->mA << "\n"
            << " B vector:\n" << this->mB << "\n"
            << std::flush;
      MIDASERROR("Negative result in ComplexLaplaceQuadrature::LeastSquaresError!");
   }

   // Calculate error
   T err = std::sqrt(err2);

   Mout  << "    Abserr: " << err << "\n"
         << "    Relerr: " << err / std::sqrt(this->mNorm2) << "\n"
         << std::flush;

   // Set error
   this->mLsError = err;

   return err;
}

/**
 * Type overload for ComplexLaplaceQuadrature
 **/
template
   <  class T
   >
typename LaplaceQuadratureBase<T>::quadID ComplexLaplaceQuadrature<T>::Type
   (
   )  const
{
   return LaplaceQuadratureBase<T>::quadID::COMPLEXLAP;
}

/**
 * ShowType overload for ComplexLaplaceQuadrature
 **/
template
   <  class T
   >
std::string ComplexLaplaceQuadrature<T>::ShowType
   (
   )  const
{
   return "ComplexLaplaceQuadrature";
}

/**
 * MakeMatrix overload
 *
 * @param eigen
 * @param shift_part
 * @param result
 * @param real
 **/
template
   <  class T
   >
void ComplexLaplaceQuadrature<T>::MakeMatrix
   (  const std::vector<T>& eigen
   ,  T shift_part
   ,  T* result
   ,  bool real
   )  const
{
   MIDASERROR("Not impl!");
}

/**
 * MakeMatrix overload
 *
 * @param aEigVals
 * @param aOffsets
 * @param aModeNumber
 * @param aExtent
 * @param aDiagEnerPart
 * @param result
 * @param real
 **/
template
   <  class T
   >
void ComplexLaplaceQuadrature<T>::MakeMatrix
   (  const DataCont& aEigVals
   ,  const std::vector<In>& aOffsets
   ,  In aModeNumber
   ,  unsigned aExtent
   ,  T aDiagEnerPart
   ,  T* result
   ,  bool real
   )  const
{
   MIDASERROR("Not impl!");
}

/**
 * Print info
 **/
template
   <  class T
   >
void ComplexLaplaceQuadrature<T>::PrintInfo
   (  std::ostream& os
   )  const
{
   LaplaceQuadratureBase<T>::PrintInfo(os);
   os << "    Gamma:          " << this->mGamma << std::endl;
}

/**
 * Make plot data
 *
 * @param aName      Name of file
 * @param aDelta     Point spacing
 **/
template
   <  class T
   >
void ComplexLaplaceQuadrature<T>::MakePlot
   (  const std::string& aName
   ,  T aDelta
   )  const
{
   std::ofstream output( aName );

   output.precision(16);

   size_t nb_width = output.precision() + 9;

   output   << "# " 
            << std::setw(nb_width) << "x"
            << std::setw(nb_width) << "approx_re" 
            << std::setw(nb_width) << "exact_re" 
            << std::setw(nb_width) << "approx_im" 
            << std::setw(nb_width) << "exact_im" 
            << std::endl;

   T min = this->Xmin();
   T max = this->Xmax();

   for(T x=min; x <= max; x+=aDelta)
   {
      T exact_re = x / (x*x + this->mGamma*this->mGamma);
      T exact_im = this->mGamma / (x*x + this->mGamma*this->mGamma);

      T approx_re = T(0.);
      T approx_im = T(0.);
      for(size_t q=0; q<this->NumPoints(); ++q)
      {
         approx_re += this->Weights()[q] * std::exp(-this->Points()[q]*this->mGamma) * std::sin(this->Points()[q]*x);
         approx_im += this->Weights()[q] * std::exp(-this->Points()[q]*this->mGamma) * std::cos(this->Points()[q]*x);
      }

      output   << "  " 
               << std::setw(nb_width) << x 
               << std::setw(nb_width) << approx_re
               << std::setw(nb_width) << exact_re
               << std::setw(nb_width) << approx_im
               << std::setw(nb_width) << exact_im
               << std::endl;
   }

   output.close();
}


/**
 * Constructor for LaplaceQuadrature
 *
 * @param lapinfo    LaplaceInfo
 * @param energies   Arguments for determining EnergyHistogram
 **/
template
   <  class T
   >
template
   <  class... Us
   >
LaplaceQuadrature<T>::LaplaceQuadrature
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  Us&&... energies
   )
{
   // Calculate interval
   auto interval = this->CalculateInterval(std::forward<Us>(energies)...);

   auto delta = lapinfo.at("HISTOGRAMDELTA").template get<Nb>();

   // Construct quadrature
   if (  this->CheckQuadratureInput(lapinfo, interval, std::forward<Us>(energies)...)
      )
   {
      this->mLapQuad = LaplaceQuadratureBase<T>::Factory(lapinfo, interval, EnergyHistogram<T>(interval, delta, std::forward<Us>(energies)...));
   }
   else
   {
      this->mLapQuad = nullptr;
   }
}

/**
 * Constructor for LaplaceQuadrature
 *
 * @param lapinfo    LaplaceInfo
 * @param xmin
 * @param xmax
 **/
template
   <  class T
   >
LaplaceQuadrature<T>::LaplaceQuadrature
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  T xmin
   ,  T xmax
   )
{
   // Calculate interval
   auto interval = std::make_pair(xmin, xmax);

   auto delta = lapinfo.at("HISTOGRAMDELTA").template get<Nb>();

   // Construct quadrature
   if (  xmin < xmax
      )
   {
      this->mLapQuad = LaplaceQuadratureBase<T>::Factory(lapinfo, interval, EnergyHistogram<T>(interval, delta));
   }
   else
   {
      this->mLapQuad = nullptr;
      Mout  << " LaplaceQuadrature not constructed for x in [" << interval.first << ", " << interval.second << "]." << std::endl;
   }
}

/**
 * Check if the LaplaceQuadrature should be constructed.
 *
 * @param lapinfo    LaplaceInfo
 * @param interval   Interval of energies
 * @param modes      Modes in MC
 * @param nmodals    Number of modals
 * @param eigvals    Eigenvalues from e.g. VSCF
 * @param offsets    Modal offsets in VSCF (eigvals for mode i starts at the j'th position...)
 * @param shift      Real part of shift
 *
 * @return
 *    True if the quadrature should be constructed.
 **/
template
   <  class T
   >
bool LaplaceQuadrature<T>::CheckQuadratureInput
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  const std::pair<T,T>& interval
   ,  const std::vector<In>& modes
   ,  const std::vector<In>& nmodals
   ,  const DataCont& eigvals
   ,  const std::vector<In>& offsets
   ,  T shift
   )  const
{
   // If the order of the tensor will be smaller than 2, we do not need quadrature.
   // If the interval has zero width, we cannot make a quadrature.
   // If we always decompose complex energy-denominator tensors directly, we need no quadrature.
   if (  modes.size() < 2
      || interval.first == interval.second
      || (  lapinfo.at("DECOMPOSECOMPLEX").template get<bool>()
         && (  lapinfo.at("COMPLEXQUADRATURE").template get<bool>()
            || interval.first*interval.second < static_cast<T>(0.)
            )
         )
      )
   {
      if (  gDebug
         && modes.size() > 1
         )
      {
         Mout  << " LaplaceQuadrature not constructed for x in [" << interval.first << ", " << interval.second << "] and ndim = " << modes.size() << std::endl;
      }
      return false;
   }
   else
   {
      return true;
   }
}

/**
 * Check if the LaplaceQuadrature should be constructed.
 *
 * @param lapinfo    LaplaceInfo
 * @param interval   Interval of energies
 * @param order      Half the order of the requested energy-denominator tensor (number of virtual indices)
 * @param vir        Virtual-orbital energies
 * @param occ        Occupied-orbital energies
 * @param shift      Real part of energy shift
 *
 * @return
 *    True if the quadrature should be constructed.
 **/
template
   <  class T
   >
bool LaplaceQuadrature<T>::CheckQuadratureInput
   (  const midas::tensor::LaplaceInfo& lapinfo
   ,  const std::pair<T,T>& interval
   ,  unsigned order
   ,  const std::vector<T>& vir
   ,  const std::vector<T>& occ
   ,  T shift
   )  const
{
   // If the order of the tensor will be smaller than 2, we do not need quadrature.
   // If the interval has zero width, we cannot make a quadrature.
   // If we always decompose complex energy-denominator tensors directly, we need no quadrature.
   if (  order < 1
      || interval.first == interval.second
      || (  lapinfo.at("DECOMPOSECOMPLEX").template get<bool>()
         && (  lapinfo.at("COMPLEXQUADRATURE").template get<bool>()
            || interval.first*interval.second < static_cast<T>(0.)
            )
         )
      )
   {
      if (  gDebug
         && order > 0
         )
      {
         Mout  << " LaplaceQuadrature not constructed for x in [" << interval.first << ", " << interval.second << "] and order = " << order << std::endl;
      }
      return false;
   }
   else
   {
      return true;
   }
}

/**
 * Calculate energy interval for electronic-structure calculations.
 *
 * @param order      The number of virtual indices of the tensor.
 * @param vir        Virtual-orbital energies.
 * @param occ        Occupied-orbital energies.
 * @param shift      Real part of energy shift.
 *
 * @return
 *    Pair of {xmin, xmax}.
 **/
template
   <  class T
   >
std::pair<T,T> LaplaceQuadrature<T>::CalculateInterval
   (  unsigned order
   ,  const std::vector<T>& vir
   ,  const std::vector<T>& occ
   ,  T shift
   )  const
{
   T vir_min = *std::min_element(vir.begin(), vir.end());
   T vir_max = *std::max_element(vir.begin(), vir.end());

   T occ_min = *std::min_element(occ.begin(), occ.end());
   T occ_max = *std::max_element(occ.begin(), occ.end());

   T denom_min = order * ( vir_min - occ_max ) - shift;
   T denom_max = order * ( vir_max - occ_min ) - shift;

   return std::make_pair(denom_min, denom_max);
}

/**
 * Calculate energy interval for vibrational-structure calculations.
 *
 * @param modes      Modes in MC
 * @param nmodals    Number of modals for each mode
 * @param eigvals    Eigenvalues from VSCF calc.
 * @param offsets    Offsets in eigvals (eigvals for mode i begin at j'th position)
 * @param shift      Real part of energy shift.
 *
 * @return
 *    Pair of {xmin, xmax}.
 **/
template
   <  class T
   >
std::pair<T,T> LaplaceQuadrature<T>::CalculateInterval
   (  const std::vector<In>& modes
   ,  const std::vector<In>& nmodals
   ,  const DataCont& eigvals
   ,  const std::vector<In>& offsets
   ,  T shift
   )  const
{
   T xmax = -shift;
   T xmin = -shift;

   Nb val = C_0;

   for(const auto& mode : modes)
   {
      eigvals.DataIo(IO_GET, offsets[mode]+nmodals[mode], val);
      xmax += val;
      eigvals.DataIo(IO_GET, offsets[mode]+I_1, val);
      xmin += val;
   }

   return std::make_pair(xmin, xmax);
}

/**
 * MakeMatrix interface
 *
 * @param args    Arguments to MakeMatrix for concrete LaplaceQuadrature
 **/
template
   <  class T
   >
template
   <  class... Us
   >
void LaplaceQuadrature<T>::MakeMatrix
   (  Us&&... args
   )  const
{
   if (  mLapQuad
      )
   {
      this->mLapQuad->MakeMatrix(std::forward<Us>(args)...);
   }
   else
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }
}

/**
 * Check if the LaplaceQuadrature contains a quadrature
 *
 * @return
 *    True if mLapQuad is nullptr
 **/
template
   <  class T
   >
bool LaplaceQuadrature<T>::IsNullPtr
   (
   )  const
{
   return mLapQuad ? false : true;
}

/**
 * Make data for plot
 *
 * @param aName      Name of file
 * @param aDelta     Point spacing
 **/
template
   <  class T
   >
void LaplaceQuadrature<T>::MakePlot
   (  const std::string& aName
   ,  T aDelta
   )  const
{
   if (  mLapQuad
      )
   {
      mLapQuad->MakePlot(aName, aDelta);
   }
   else
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }
}

/**
 * Get number of points
 *
 * @return npoints
 **/
template
   <  class T
   >
unsigned LaplaceQuadrature<T>::NumPoints
   (
   )  const
{
   if (  !mLapQuad
      )
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }
   return mLapQuad->NumPoints();
}

/**
 * Get points
 *
 * @return Points
 **/
template
   <  class T
   >
const std::vector<T>& LaplaceQuadrature<T>::Points
   (
   )  const
{
   if (  !mLapQuad
      )
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }

   return mLapQuad->Points();
}

/**
 * Get weights
 *
 * @return Weights
 **/
template
   <  class T
   >
const std::vector<T>& LaplaceQuadrature<T>::Weights
   (
   )  const
{
   if (  !mLapQuad
      )
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }

   return mLapQuad->Weights();
}

/**
 * Get mNegativeInterval
 *
 * @return
 *    mNegativeInterval
 **/
template
   <  class T
   >
bool LaplaceQuadrature<T>::NegativeInterval
   (
   )  const
{
   if (  !mLapQuad
      )
   {
      MIDASERROR("Uninitialized quadrature (mLapQuad)!");
   }

   return mLapQuad->NegativeInterval();
}

/**
 * Output
 *
 * @param os
 * @param lq
 * @return ostream
 **/
template<class T>
std::ostream& operator<<
   (  std::ostream& os
   ,  const LaplaceQuadrature<T>& lq
   )
{
   if (  lq.IsNullPtr()
      )
   {
      MIDASERROR("Uninitialized quadrature (lq)!");
   }
   else
   {
      // We cannot get the error, so we print -1
      lq.mLapQuad->PrintInfo(os);
   }

   return os;
}



/** 
 * Declare static members
 **/
template <class T>
std::set<std::string> RealLaplaceQuadrature<T>::mDataFileSet;

template<class T>
std::map<In, Nb> RealLaplaceQuadrature<T>::mMaxIntervals;

/**
 * Initialize mMaxIntervals
 **/
template
   <  class T
   >
void RealLaplaceQuadrature<T>::InitMaxIntervals()
{
   RealLaplaceQuadrature<T>::mMaxIntervals.clear();

   // Insert max intervals
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(1)] = static_cast<Nb>(1.e1);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(2)] = static_cast<Nb>(5.e1);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(3)] = static_cast<Nb>(2.e2);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(4)] = static_cast<Nb>(5.e2);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(5)] = static_cast<Nb>(2.e3);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(6)] = static_cast<Nb>(3.e3);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(7)] = static_cast<Nb>(7.e3);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(8)] = static_cast<Nb>(2.e4);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(9)] = static_cast<Nb>(3.e4);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(10)] = static_cast<Nb>(1.e5);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(11)] = static_cast<Nb>(2.e5);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(12)] = static_cast<Nb>(3.e5);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(13)] = static_cast<Nb>(4.e5);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(14)] = static_cast<Nb>(7.e5);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(15)] = static_cast<Nb>(2.e6);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(16)] = static_cast<Nb>(3.e6);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(17)] = static_cast<Nb>(4.e6);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(18)] = static_cast<Nb>(7.e6);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(19)] = static_cast<Nb>(1.e7);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(20)] = static_cast<Nb>(2.e7);
   RealLaplaceQuadrature<T>::mMaxIntervals[static_cast<In>(21)] = static_cast<Nb>(3.e7);
}

/*
 * @function LaplaceQuadrature::InitDataFileSet()  Initialize static set of existing data files
 */
template
   <  class T
   >
void RealLaplaceQuadrature<T>::InitDataFileSet()
{
   RealLaplaceQuadrature<T>::mDataFileSet.clear();

   // Insert all available files
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk01_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk02_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk03_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk04_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk05_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk06_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_2E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_3E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_4E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_5E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_6E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_7E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_8E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_9E0");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk07_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk08_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk09_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk10_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk11_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_3E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk12_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_3E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_4E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk13_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_3E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_4E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_7E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk14_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk15_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk16_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_3E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_4E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk17_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_4E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_4E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_5E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_7E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk18_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_1E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_4E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_5E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_7E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk19_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_1E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_2E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_4E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_5E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_6E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_7E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_7E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_8E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk20_9E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_1E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_2E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_3E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_3E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_3E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_3E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_3E7");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_4E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_4E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_4E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_4E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_5E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_5E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_5E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_5E5");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_5E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_6E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_6E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_7E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_7E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_7E4");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_7E6");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_8E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_8E3");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_9E1");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_9E2");
   RealLaplaceQuadrature<T>::mDataFileSet.insert("1_xk21_9E3");
}

/*
 * @function LaplaceQuadrature<T>::DataFileExists()   Check that a file is in mDataFileSet
 * @param    arName                                   The name of the file
 */
template
   <  class T
   >
bool RealLaplaceQuadrature<T>::DataFileExists
   (  const std::string& arName
   )
{
   return (RealLaplaceQuadrature<T>::mDataFileSet.find(arName) != RealLaplaceQuadrature<T>::mDataFileSet.end());
}

#endif /* LAPLACEQUADRATURE_IMPL_H_INCLUDED */
