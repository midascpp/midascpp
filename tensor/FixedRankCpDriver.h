/**
************************************************************************
* 
* @file    FixedRankCpDriver.h
*
* @date    23-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Driver for fitting a tensor to CP format with a fixed rank.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef FIXEDRANKCPDRIVER_H_INCLUDED
#define FIXEDRANKCPDRIVER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"

namespace midas
{
namespace tensor
{

/**
 * Driver class for fixed-rank CP decomposition.
 **/
class FixedRankCpDriver
   :  public detail::TensorDecompDriverBase
{
   private:
      //! Rank of fitted tensor
      In mCpRank;

      //! Print FitReport after each decomposition
      bool mPrintFitReport;


      //! Check decomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Decompose
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Decompose with argument guess
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Check if a tensor should be screened
      bool ScreenImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Decompose low-order tensors
      bool DecomposeLowOrderTensorsImpl
         (  const NiceTensor<Nb>&
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Get threshold
      Nb GetThreshold
         (
         )  const override;

   public:
      //! Constructor from TensorDecompInfo
      FixedRankCpDriver
         (  const TensorDecompInfo&
         );
};

} /* namespace tensor */
} /* namespace midas */


#endif /* FIXEDRANKCPDRIVER_H_INCLUDED */
