/**
************************************************************************
* 
* @file    FixedRankCpDriver.cc
*
* @date    23-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of FixedRankCpDriver methods.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/FixedRankCpDriver.h"

namespace midas
{
namespace tensor
{

//! Register FixedRankCpDriver
detail::TensorDecompDriverRegistration<FixedRankCpDriver> registerFixedRankCpDriver("FIXEDRANKCP");

/**
 * Get decomp threshold
 *
 * @return
 *    The threshold
 **/
Nb FixedRankCpDriver::GetThreshold
   (
   )  const
{
   return -C_1;
}

/**
 * Check that a given tensor is decomposed by this driver.
 *
 * @param aTensor
 *    The target tensor.
 **/
bool FixedRankCpDriver::CheckDecompositionImpl
   (  const NiceTensor<Nb>& aTensor
   )  const
{
   bool order_ok  =  mDecompToOrder > 0 
                  ?  (In(aTensor.NDim()) >= mDecompToOrder)
                  :  true;

   auto type = aTensor.Type();

   bool type_ok   =  (  type == BaseTensor<Nb>::typeID::SIMPLE
                     || type == BaseTensor<Nb>::typeID::CANONICAL
                     || type == BaseTensor<Nb>::typeID::SUM
                     || type == BaseTensor<Nb>::typeID::DIRPROD
                     );

   return order_ok && type_ok;
}

/**
 * Return type of decomp driver.
 **/
std::string FixedRankCpDriver::TypeImpl
   (
   )  const
{
   return std::string("FixedRankCp");
}

/**
 * Check if the tensor should be screened.
 *
 * @param aTensor    The target tensor
 * @param aNorm2     Norm2 of target
 * @param aThresh    Decomposition threshold
 * @return           True if tensor should be screened
 **/
bool FixedRankCpDriver::ScreenImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   if (  libmda::numeric::float_neg(aNorm2)
      )
   {
      return false;
   }
   else
   {
      // Check norm
      Nb norm = std::sqrt(aNorm2);
      if (  norm < aThresh
         )
      {
         return true;
      }
   
      return false;
   }
}

/**
 * Fit vectors.
 *
 * @param aTensor       Target tensor
 * @param aFitTensor    Fitting tensor
 * @param aThresh       Decomposition threshold
 * @return              True if tensor has been decomposed
 **/
bool FixedRankCpDriver::DecomposeLowOrderTensorsImpl
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   auto ndim = aTensor.NDim();

   // Dump vectors to rank-1 CanonicalTensor
   if (  ndim == 1
      || aTensor.MaxExtent() == aTensor.TotalSize()
      )
   {
      aFitTensor = NiceTensor<Nb>( aTensor.GetTensor()->DumpVectorToCanonical() );
      return true;
   }
   else if  (  ndim == 2
            )
   {
      // Perform SVD and truncate at mCpRank (aThresh makes no difference here).
      aFitTensor = NiceTensor<Nb>( aTensor.GetTensor()->SvdRankReduction(aThresh, this->mCpRank) );
      return true;
   }
   else
   {
      return false;
   }
}

/**
 * Decompose a tensor.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> FixedRankCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  Nb aThresh
   )  const
{
   // Norms
   auto norm2 = aNorm2;
   auto norm = std::sqrt(norm2);

   const auto& dims = aTensor.GetDims();
   auto ndim = aTensor.NDim();

   auto thresh = aThresh < C_0 ? C_I_10_8 : aThresh;

   // Dump vectors to rank-1 CanonicalTensor.
   // Fit matrices using SVD.
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()   // If all dimensions except one is 1.
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("FixedRankCpDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Generate guess
   NiceTensor<Nb> fit_tensor;
   this->mGuesser->MakeStartGuess(aTensor, norm, fit_tensor, this->mCpRank);

   // Return fitted tensor
   auto fit_report = this->mCpFitter->Decompose(aTensor, norm2, fit_tensor, thresh);

   if (  this->mPrintFitReport   )
   {
      Mout << " +------------- FindBestCpAls ---------------+\n" 
           << fit_report 
           << " +-------------------------------------------+\n" << std::endl;
   }

   return fit_tensor;
}


/**
 * Decompose a tensor using an argument guess.
 *
 * @param aTensor
 *    The target tensor
 * @param aNorm2
 *    Norm2 of target tensor
 * @param aGuess
 *    The guess tensor
 * @param aThresh
 *    The decomposition threshold
 * @return
 *    The decomposed tensor
 **/
NiceTensor<Nb> FixedRankCpDriver::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aNorm2
   ,  const NiceTensor<Nb>& aGuess
   ,  Nb aThresh
   )  const
{
   // Calculate norms
   auto norm2 = aNorm2;
   auto norm = std::sqrt(norm2);

   const auto& dims = aTensor.GetDims();
   auto ndim = aTensor.NDim();

   auto thresh = aThresh < C_0 ? C_I_10_8 : aThresh;

   // Dump vectors to rank-1 CanonicalTensor.
   // Fit matrices using SVD.
   if (  ndim < 3
      || aTensor.MaxExtent() == aTensor.TotalSize()   // If all dimensions except one is 1.
      )
   {
      NiceTensor<Nb> result;
      if (  TensorDecompDriverBase::DecomposeLowOrderTensorsImpl(aTensor, result, thresh)
         )
      {
         return result;
      }
      else
      {
         MIDASERROR("FixedRankCpDriver: Error in decomposition of low-order tensors!");
      }
   }

   // Return fitted tensor
   NiceTensor<Nb> fit_tensor = aGuess;
   auto fit_report = this->mCpFitter->Decompose(aTensor, norm2, fit_tensor, thresh);

   if (  this->mPrintFitReport   )
   {
      Mout << " +------------- FindBestCpAls ---------------+\n" 
           << fit_report 
           << " +-------------------------------------------+\n" << std::endl;
   }

   return fit_tensor;
}



/**
 * Constructor from TensorDecompInfo.
 *
 * @param aInfo
 *    The TensorDecompInfo
 **/
FixedRankCpDriver::FixedRankCpDriver
   (  const TensorDecompInfo& aInfo
   )
   :  TensorDecompDriverBase(aInfo)
   ,  mCpRank           (const_cast<TensorDecompInfo&>(aInfo)["CPRANK"].get<In>())
   ,  mPrintFitReport   (false)
{
}

} /* namespace tensor */
} /* namespace midas */
