#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "CanonicalTensor.h"
#include "CanonicalTensor_Impl.h"
#include "CanonicalTensor_Fits.h"

// define
#define INSTANTIATE_CANONICALTENSOR(T) \
template class CanonicalTensor<T>; \
\
template BaseTensorPtr<T> random_canonical_tensor_explicit(const std::vector<unsigned int>&, const unsigned int); \
\
template BaseTensorPtr<T> const_value_cptensor(const std::vector<unsigned int>&, const T, const unsigned int); \
template BaseTensorPtr<T> cross_approximation \
   (  const BaseTensor<T>* const apTensor \
   ,  const std::vector<unsigned>& arPivot \
   ,  const unsigned aMaxSteps \
   );

#define INSTANTIATE_CANONICALTENSOR_2(T, U) \
template void CanonicalTensor<T>::ScaleModeMatrix \
         (  unsigned \
         ,  U \
         ); \
\
template void CanonicalTensor<T>::ScaleModeVector \
         (  unsigned \
         ,  unsigned \
         ,  U \
         ); \

#define INSTANTIATE_CANONICALTENSOR_DISABLE(T) \
template CanonicalTensor<T> compress_to_canonical \
   (  const BaseTensor<T>& target \
   ,  typename BaseTensor<T>::real_t maxerror \
   ,  std::function<CanonicalTensor<T> (const unsigned int)> guesser \
   ,  FitReport<T>(CanonicalTensor<T>::*fitter)(const BaseTensor<T>&, const unsigned int, const typename BaseTensor<T>::real_t, midas::type_traits::DisableIfComplexT<T>*) \
   ); \
\
template typename BaseTensor<T>::UniqueArray CanonicalTensor<T>::PartialSqueeze \
         (  const BaseTensor<T>* const \
         ,  const std::vector<unsigned>& \
         ,  const int \
         ,  bool transposed \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template typename BaseTensor<T>::UniqueArray CanonicalTensor<T>::PartialGammaMatrix \
         (  const CanonicalTensor<T>* const \
         ,  const std::vector<unsigned>& \
         ,  const unsigned int \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template CanonicalTensor<T> CanonicalTensor<T>::DirectIncrease \
         (  const CanonicalTensor<T>& \
         ,  const T \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template CanonicalTensor<T> CanonicalTensor<T>::Gradient \
         (  const BaseTensor<T>& \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template void CanonicalTensor<T>::Gradient \
         (  CanonicalTensor<T>& \
         ,  GeneralGammaIntermediates<T>& \
         ,  Squeezer<T>& \
         ,  std::unique_ptr<T[]>& \
         ,  std::unique_ptr<T[]>& \
         ,  T \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template void CanonicalTensor<T>::Gradient \
         (  unsigned idim \
         ,  std::unique_ptr<T[]>& \
         ,  std::unique_ptr<T[]>& \
         ,  T \
         ,  bool \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         )  const; \
\
template FitReport<T> CanonicalTensor<T>::FitALS \
         (  const BaseTensor<T>& \
         ,  const unsigned int \
         ,  const real_t \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         ); \
\
template FitReport<T> CanonicalTensor<T>::FitALS \
         (  const BaseTensor<T>& \
         ,  const real_t \
         ,  const unsigned int \
         ,  const real_t \
         ,  const real_t \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         ); \
\
template FitReport<T> CanonicalTensor<T>::FitSteepestDescent \
         (  const BaseTensor<T>& \
         ,  const unsigned int \
         ,  const real_t \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         ); \
\
template FitReport<T> CanonicalTensor<T>::FitNCG \
         (  const BaseTensor<T>& \
         ,  const unsigned int \
         ,  const real_t \
         ,  midas::type_traits::DisableIfComplexT<T>* \
         ); \
\







// concrete instantiations
INSTANTIATE_CANONICALTENSOR(float);
INSTANTIATE_CANONICALTENSOR(double);
INSTANTIATE_CANONICALTENSOR(std::complex<float>);
INSTANTIATE_CANONICALTENSOR(std::complex<double>);

INSTANTIATE_CANONICALTENSOR_2(float, float);
INSTANTIATE_CANONICALTENSOR_2(double, double);
INSTANTIATE_CANONICALTENSOR_2(std::complex<float>, float);
INSTANTIATE_CANONICALTENSOR_2(std::complex<double>, double);
INSTANTIATE_CANONICALTENSOR_2(std::complex<float>, std::complex<float>);
INSTANTIATE_CANONICALTENSOR_2(std::complex<double>, std::complex<double>);

INSTANTIATE_CANONICALTENSOR_DISABLE(float);
INSTANTIATE_CANONICALTENSOR_DISABLE(double);



#undef INSTANTIATE_CANONICALTENSOR
#undef INSTANTIATE_CANONICALTENSOR_2
#undef INSTANTIATE_CANONICALTENSOR_DISABLE

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
