/**
 ************************************************************************
 * 
 * @file                CanonicalTensor.h
 *
 * Created:             02.10.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               CanonicalTensor: Canonical representation of tensors.
 * 
 * Last modified: Wed Oct 22, 2014  18:44
 * 
 * Detailed Description: The ContractDown, ContractForward and ContractUp
 *                       member functions are to supersede
 *                       Integrals::Contract
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef CANONICALTENSOR_H_INCLUDED
#define CANONICALTENSOR_H_INCLUDED

// std headers
#include <iostream>
#include <vector>
#include <typeinfo>
#include <memory>
#include <string>
#include <functional>

#include "BaseTensor.h"
#include "FitReport.h"
#include "GammaMatrix.h"
#include "Squeezer.h"

#include "lapack_interface/SVD_struct.h"
#include "util/type_traits/Complex.h"

// Forward declarations
template
   <  class T
   ,  class SmartPtr
   >
void GammaMatrix
   (  const CanonicalTensor<T>*
   ,  const CanonicalTensor<T>*
   ,  const unsigned int
   ,  SmartPtr&
   );

template
   <  typename T
   >
void Squeeze
   (  const CanonicalTensor<T>& fit_tensor
   ,  const TensorDirectProduct<T>& target_tensor
   ,  const unsigned int idx
   ,  T* result
   ,  bool transpose
   );


/// Canonical tensor representation.
/*! \f[
 *      a_{ijk} = \sum_r^R a^{1}_{ri} a^{2}_{rj} a^{3}_{rk}
 *  \f]
 */
template
   <  class T
   >
class CanonicalTensor
   :  public BaseTensor<T>
{
   protected:
      using Base = BaseTensor<T>;
      using real_t = typename Base::real_t;

      unsigned int mRank; //< Rank \f$R\f$.
      T** mModeMatrices;  //< Matrices \f$a^{1}_{ri}\f$, \f$a^{2}_{rj}\f$, \f$a^{3}_{rk}\f$...

   private:
      void Allocate();
      void Deallocate();

   protected:
      //! Call Deallocate and Allocate to set new rank from derived classes
      void SetNewRank
         (  unsigned
         );

   public:
      friend DotEvaluator<T>;
      using dot_t = typename DotEvaluator<T>::dot_t;

      /// See BaseTensor<T>::Type().
      typename BaseTensor<T>::typeID Type() const override {return BaseTensor<T>::typeID::CANONICAL;};
      std::string ShowType() const override {return "CanonicalTensor";};
      
      void DumpInto(T*) const override; 
   private:
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const BaseTensor<T>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  bool = false
         ,  bool = false
         )  const override;

      CanonicalTensor* Reorder_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         )  const override;

      BaseTensor<T>* ContractInternal_impl
         (  const std::vector<std::pair<unsigned int, unsigned int>>&
         ,  const std::vector<unsigned int>&
         )  const override;

      CanonicalTensor* Slice_impl
         (  const std::vector<std::pair<unsigned int,unsigned int>>&
         )  const override;
      
      std::vector<T> PartialContract(const BaseTensor<T>* const, const std::vector<unsigned>&) const;

      template
         <  typename U = T
         >
      typename Base::UniqueArray PartialSqueeze
         (  const BaseTensor<T>* const
         ,  const std::vector<unsigned>&
         ,  const int
         ,  bool transposed
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;

      template
         <  typename U = T
         >
      typename Base::UniqueArray PartialGammaMatrix
         (  const CanonicalTensor<T>* const
         ,  const std::vector<unsigned>&
         ,  const unsigned int
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;

      T compute_condition_number() const;
      std::vector<std::vector<T> > compute_norm_matrix() const;
      
      void ConjugateImpl() override;


   public:
      friend class SimpleTensor<T>;

//*============== CONSTRUCTORS, DESTRUCTOR, ASSIGNMENTS ==============*/
      CanonicalTensor();
      CanonicalTensor(const std::vector<unsigned int>&, const unsigned int=0);
      CanonicalTensor(const std::vector<unsigned int>&, const unsigned int, T**);
      CanonicalTensor(const std::vector<unsigned int>&, const SVD_struct<T>&, unsigned);
      CanonicalTensor(std::istream&);

      // Unit CanonicalTensor constructor (rank 1, outer product of unit vectors according to index).
      CanonicalTensor(const std::vector<unsigned int>& arDims, const std::vector<unsigned int>& arIndex);

      CanonicalTensor(const CanonicalTensor<T>&);
      CanonicalTensor(CanonicalTensor<T>&&);

      virtual ~CanonicalTensor();

      CanonicalTensor<T>* Clone() const override;

      CanonicalTensor& operator=(const CanonicalTensor<T>&);
      CanonicalTensor& operator=(      CanonicalTensor<T>&&);
/*========== MEMBER ACCESS ==========*/
      T** GetModeMatrices() const;

/*============== IO ==============*/
      virtual std::string Prettify() const override;
      std::ostream& write_impl(std::ostream& output) const override;

/*============== QUERY ==============*/
      unsigned int GetRank() const;
      real_t Norm2() const override;

      unsigned NElem() const;

      //! Value of element
      T Element
         (  const std::vector<unsigned>&
         )  const override;

      //! Check if one or more mode-matrix elements are nan
      bool CheckModeMatrices
         (
         )  const;

      //! Sanity check
      bool SanityCheck() const override;


/*============== MANIPULATE ==============*/
      void Scale(const T&) override;
      void Zero() override;

      //! Distribute norms between mode vectors: ||f^n_r|| = ||f^m_r||
      void BalanceModeVectors();

      //! Norm2 of mode vector (used in BalanceModeVectors)
      real_t ModeVectorNorm2
         (  unsigned
         ,  unsigned
         )  const;

      //! Scale mode matrix
      template
         <  typename U
         >
      void ScaleModeMatrix
         (  unsigned
         ,  U
         );

      //! Scale norm of mode vector (used in BalanceModeVectors)
      template
         <  typename U
         >
      void ScaleModeVector
         (  unsigned
         ,  unsigned
         ,  U
         );

/*============== ELEMENT-WISE BINARY ARITHMETIC OPERATORS ==============*/
   public:
      CanonicalTensor* operator*=(const T) override;

      CanonicalTensor* PartialHadamardProduct
         (  unsigned
         ,  const CanonicalTensor<T>&
         );

      BaseTensor<T>* operator+=(const BaseTensor<T>&) override;

      CanonicalTensor<T>* operator*=(const BaseTensor<T>&) override;
      CanonicalTensor<T>* operator/=(const BaseTensor<T>&) override;

      void Axpy(const BaseTensor<T>&, const T&) override;

/*==========EXOTIC OPERATIONS (needed for fits, etc) ======= */
   private:
      typename Base::UniqueArray calculate_inner_product_matrix
         (  const unsigned int left_rank
         ,  const std::vector<unsigned int>& left_dims
         ,  T** left_matrices
         ,  const std::vector<unsigned int>& left_indices
         ,  const unsigned int               right_rank
         ,  const std::vector<unsigned int>& right_dims
         ,  T** right_matrices
         ,  const std::vector<unsigned int>& right_indices
         ,  bool conjugate_left
         )  const;

      T RankOneOverlap(const unsigned int) const;

      //! Optimize pivot (largest element)
      real_t OptimizePivot
         (  std::vector<unsigned>&
         ,  const unsigned aMaxSteps=0
         )  const override;

   public:
/*============== DECOMPOSITIONS AND RECOMPRESSIONS ==============*/
      //! Return N first terms in rank expansion
      CanonicalTensor<T> Truncate
         (  const unsigned int
         )  const;

      //! Return rank-1 tensor having largest overlap with the full tensor
      CanonicalTensor<T> BestRankOneTensor
         (
         )  const;

      //!
      template
         <  typename U = T
         >
      CanonicalTensor<T> DirectIncrease
         (  const CanonicalTensor<T>&
         ,  const T
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;


      /*** @name The following lines are defined in CanonicalTensor_Fits.h ***/
      //!@{

      //! Gradient of CanonicalTensor
      template
         <  typename U = T
         >
      CanonicalTensor Gradient
         (  const BaseTensor<T>&
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;

      //! Gradient taking pre-calculated intermediates and pre-allocated workspace
      template
         <  typename SmartPtr
         ,  typename U = T
         >
      void Gradient
         (  CanonicalTensor<T>&
         ,  GeneralGammaIntermediates<U>&
         ,  Squeezer<U>&
         ,  SmartPtr&
         ,  SmartPtr&
         ,  T=static_cast<T>(1.)
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;

      //! Gradient block taking pre-calculated gamma and squeezed mode matrix (stores result in squeezed)
      template
         <  typename SmartPtr
         ,  typename U = T
         >
      void Gradient
         (  unsigned idim
         ,  SmartPtr&
         ,  SmartPtr&
         ,  T=static_cast<T>(1.)
         ,  bool=false
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         )  const;

      //! Fit using CP-ALS
      template
         <  typename U = T
         >
      FitReport<U> FitALS
         (  const BaseTensor<T>&
         ,  const unsigned int
         ,  const real_t
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Fit using CP-ALS
      template
         <  typename U = T
         >
      FitReport<U> FitALS
         (  const BaseTensor<T>&
         ,  const real_t
         ,  const unsigned int
         ,  const real_t
         ,  const real_t = 1e-12
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Fit using steepest descent
      template
         <  typename U = T
         >
      FitReport<U> FitSteepestDescent
         (  const BaseTensor<T>&
         ,  const unsigned int
         ,  const real_t
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );

      //! Fit using CP-NCG
      template
         <  typename U = T
         >
      FitReport<U> FitNCG
         (  const BaseTensor<T>&
         ,  const unsigned int
         ,  const real_t
         ,  midas::type_traits::DisableIfComplexT<U>* = nullptr
         );
      //!@}

      void PutNewTransposeModeMatrix(T*, const unsigned int);
      void PutNewTransposeModeMatrix(T*, T*, const unsigned int);
      void PutNewModeMatrix(T*, const unsigned int);
      void PutNewModeMatrix(T*, T*, const unsigned int);
      
      /** @name Methods used when dealing with gradients as CanonicalTensor%s **/
      //!@{

      //! Dot product between mode matrices
      T ModeMatrixDot
         (  const unsigned
         ,  const CanonicalTensor<T>* const
         )  const;

      //! Axpy on one mode matrix
      void ModeMatrixAxpy
         (  const unsigned
         ,  const CanonicalTensor<T>&
         ,  const T
         );

      //! Axpy on one mode matrix taking pointer instead of tensor
      void ModeMatrixAxpy
         (  unsigned
         ,  T*
         ,  T
         ,  bool = false
         );

      std::pair<unsigned, real_t> MaxModeMatrixElement
         (
         )  const;

      std::pair<unsigned, real_t> MaxModeMatrixNorm2
         (
         )  const;

      T ElementwiseDot
         (  const CanonicalTensor<T>&
         )  const;

      T ElementwiseNorm2
         (
         )  const;

      T ElementwiseNorm
         (
         )  const;

      void ElementwiseAxpy
         (  const CanonicalTensor<T>&
         ,  const T
         );

      void ScaleModeMatrices
         (  const T
         );

      //!@}
      
      //! Change a specific rank-1 tensor in the CP tensor
      void SetRankOneTerm
         (  unsigned aRankOut
         ,  unsigned aRankIn
         ,  const CanonicalTensor<T>& aTensIn
         ,  T aCoef
         );

      //! Get vector of norms of each of the terms
      std::vector<real_t> GetRankOneNorms
         (
         )  const;
      

/*============== CONTRACTIONS ==============*/
   public:
      BaseTensor<T>* ContractDown   (const unsigned int, const BaseTensor<T>*) const override;
      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override;
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override;

      friend BaseTensorPtr<T> random_canonical_tensor_explicit<>(const std::vector<unsigned int>&, const unsigned int);
      friend BaseTensorPtr<T> const_value_cptensor<>(const std::vector<unsigned int>&, const T, const unsigned int);
      friend void Squeeze<>(const CanonicalTensor<T>&, const TensorDirectProduct<T>&, const unsigned int, T*, bool);

      /*============== DOT ==============*/
      // interface
      dot_t Dot(const BaseTensor<T>* const) const override;

      // dispatches
      dot_t DotDispatch(const SimpleTensor<T>* const) const override;
      dot_t DotDispatch(const CanonicalTensor<T>* const) const override;
      dot_t DotDispatch(const ZeroTensor<T>* const) const override;
      dot_t DotDispatch(const TensorDirectProduct<T>* const) const override;

      /*============== MPI  ==============*/
#ifdef VAR_MPI
      void MpiSend(In aRecievingRank) const override;
      void MpiRecv(In aSendingRank) override;
      void MpiBcast(In aRoot) override;
#endif /* VAR_MPI */
};

#endif /* CANONICALTENSOR_H_INCLUDED */
