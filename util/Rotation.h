#ifndef MIDAS_UTIL_ROTATION_HPP_INCLUDED
#define MIDAS_UTIL_ROTATION_HPP_INCLUDED

#include "mmv/MidasMatrix.h"

namespace midas
{
namespace util
{

enum Unit : int {Radians, Degrees};

MidasMatrix CreateRotationMatrixX(double aAngle, Unit aUnit = Unit::Radians);
MidasMatrix CreateRotationMatrixY(double aAngle, Unit aUnit = Unit::Radians);
MidasMatrix CreateRotationMatrixZ(double aAngle, Unit aUnit = Unit::Radians);

}/* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_ROTATION_HPP_INCLUDED */
