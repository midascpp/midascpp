/**
 *******************************************************************************
 * 
 * @file    InterfaceOpenMP.h
 * @date    01-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Provides an interface for OpenMP that also works when not compiling with
 *    OpenMP.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef UTIL_OPENMPINTERFACE_H_INCLUDED
#define UTIL_OPENMPINTERFACE_H_INCLUDED

#if defined(_OPENMP)
// The Gnu compiler is known to define the _OPENMP variable when compiling with
// OpenMP. We are not sure about other compilers. This could be improved, e.g.
// by compiling with -DVAR_OMP or similar (like -DVAR_MPI for MPI builds), and
// then swithcing on 'VAR_OMP'.
#include <omp.h>
#else
// If not an OpenMP build, provide any necessary interface here, to allow Midas
// code to be agnostic about whether or not it's an OpenMP build.
typedef int omp_int_t;
inline omp_int_t omp_get_thread_num()  {return 0;}
inline omp_int_t omp_get_max_threads() {return 1;}
#endif

#endif/*UTIL_OPENMPINTERFACE_H_INCLUDED*/
