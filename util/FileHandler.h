/**
************************************************************************
* 
* @file                FileHandler.h
*
* Created:             10-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Classes for handling writing to .mbar, .mop and .plot files
* 
* Last modified: Thu Oct 10, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FILEHANDLER_H
#define FILEHANDLER_H

// std headers
#include<fstream>
#include<string>
#include<vector>
#include<stdarg.h>
#include<map>

// midas headers
#include"inc_gen/TypeDefs.h"
#include"mmv/MidasVector.h"
#include"util/Io.h"

namespace FileHandles
{
   /*
   * The following struck and function allows us to set the fstream which we wish to direkt our output to...
   */
   struct To_One{
      In mSpecOfStream;
   };

   inline To_One
   to_one(In aIn)
   {
      To_One theone;
      theone.mSpecOfStream = aIn;
      return theone;
   }

   class FileHandler
   {
      private:
         ofstream* mListOfFiles; //< The actual fstreams
         In mNoOfFiles;          //< The number of fstreams
         In mSpecOfStream;       //< The filestream we are currently outputting to if only one is selected
         bool mToAll;            //< Are we outputting to all?
         bool mToOne;            //< Are we outputting to a specific fstream

      public:
         void SetmSpecOfStream(In aIn){mSpecOfStream = aIn;} //< Helper function which is called through our stream interface
         void SetToAll(bool aBool){mToAll = aBool;} //< For setting to all when needed
         void SetToOne(bool aBool){mToOne = aBool;} //< For setting to one when needed
         //< Note that all of the above functions should only be called through the << operators, since we want to keep things simple
         

         FileHandler(); //< constructor
         ~FileHandler(); //< destructor
         void Width(In); //< This function sets the width of the fstream, see the std lib on fstream...
         void OpenFiles(string,In,string); //< Opens the needed files
         void OpenFile(string); //< Opens a single file
         void Close(); //< close all files
         FileHandler& operator<<(MidasVector); //< Function for putting a MVector into the file(s), check implementation in .cc
         ofstream& GetOfstreamNo(In aIn) {return mListOfFiles[aIn];} //< Get a specific stream, should not be needed ever
         
         FileHandler& operator<<(ostream& (*aFunc)(ostream&)) 
         {
            if(mToAll)
               for(In i = I_0; i < mNoOfFiles; i++) 
                  mListOfFiles[i] << aFunc; 
            else if(mToOne)
               mListOfFiles[mSpecOfStream] << aFunc;
            else
               for(In i = I_0; i < mNoOfFiles; i++) 
                  mListOfFiles[i] << aFunc;
            return *this;
         } //< Operator for passing std::endl and such into the fstreams
         FileHandler& operator<<(FileHandler& (*aFunc)(FileHandler&)) {return aFunc(*this);} 
         //< Operator for passing the functions defined below into the stream
         FileHandler& operator<<(To_One ToOne){this->SetToAll(false); this->SetToOne(true);
                                               this->SetmSpecOfStream(ToOne.mSpecOfStream); return *this;}
         //< Operator for passing the special to_one function into the stream
         template <typename T> FileHandler& operator<<(T aType)
         {
            if(mToAll)
               for(In i = I_0; i < mNoOfFiles; i++)
                  mListOfFiles[i] << aType;
            else if(mToOne)
               mListOfFiles[mSpecOfStream] << aType;
            else
               for(In i = I_0; i < mNoOfFiles; i++)
                  mListOfFiles[i] << aType;
            return *this;
         } //< Output for any type which doesn't fit into the ones defined above
         template <typename T> FileHandler& operator<<(vector<T> aVecType)
         {
            if(mToAll)
               for(In i = I_0; i < mNoOfFiles; i++)
                  for(In j = I_0; j < aVecType.size(); j++)
                     mListOfFiles[i] << aVecType[j] << "  ";
            else if(mToOne)
               for(In j = I_0; j < aVecType.size(); j++)
                  mListOfFiles[mSpecOfStream] << aVecType[j] << "  ";
            else
               if(aVecType.size() != mNoOfFiles)
                  MIDASERROR("Mismatch in size in output to dist in FileHandler.h");
               else
                  for(In i = I_0; i < aVecType.size(); i++)
                     mListOfFiles[i] << aVecType[i] << "  ";
            return *this;
         } //< Output for vectors of any type...
   };

   static FileHandler& to_all(FileHandler& a)
   {
      a.SetToAll(true);
      a.SetToOne(false);
      return a;
   }

   static FileHandler& to_dist(FileHandler& a)
   {
      a.SetToAll(false);
      a.SetToOne(false);
      return a;
   }

   struct FileNr{
      In mToPass;
   };

   inline FileNr
   set_file(In aIn)
   {
      FileNr theone;
      theone.mToPass = aIn;
      return theone;
   }
}
#endif //define FILEHANDLER_H
