#include "Path.h"

namespace midas
{
namespace path
{
namespace detail
{

// Settings for unix
const struct
{
   char delimeter = '/';
   char extension = '.';
} settings;

} /* namespace detail */

/**
 * Check if path has directory end (on Unix '/').
 * 
 * @param aPath   The path to check.
 *
 * @return    Returns true if aPath has directory delimeter as last character, otherwise false.
 **/
bool HasDirEnd(const std::string& aPath)
{
   if(aPath.empty())
   {
      return false;
   }
   else
   {
      return aPath.back() == '/';
   }
}

/**
 * Check if path has directory beginning (on Unix '/').
 * 
 * @param aPath   The path to check.
 *
 * @return    Returns true if aPath has directory delimeter as first character, otherwise false.
 **/
bool HasDirBegin(const std::string& aPath)
{
   if(aPath.empty())
   {
      return false;
   }
   else
   {
      return aPath.front() == '/';
   }
}

/**
 * Remove directory delimeter at end of path if present.
 *
 * @param aPath   The path to check.
 *
 * @return   Returns path with no directory delimeter at the end.
 **/
std::string RemoveDirEnd(const std::string& aPath)
{
   if(HasDirEnd(aPath))
   {
      return aPath.substr(0, aPath.size()-1);
   }
   else
   {
      return aPath;
   }
}

/**
 * Return file name 
 *
 * @param aPath   Path to get file name for.
 *
 * @return Returns file name.
 **/
std::string FileName(const std::string& aPath)
{
   auto last = aPath.find_last_of(detail::settings.delimeter);
   if (  last == std::string::npos)
   {
      return aPath;
   }
   else
   {
      return aPath.substr(last + 1);
   }
}

/**
 * Return base name
 *
 * @param aPath   Path to get base name for.
 *
 * @return Returns base name.
 **/
std::string BaseName(const std::string& aPath)
{
   auto filename = FileName(aPath);
   auto last = filename.find_last_of(detail::settings.extension);
   if (  last == std::string::npos)
   {
      return filename;
   }
   else
   {
      return filename.substr(0, last);
   }
}

/**
 * Return directory name
 *
 * @param aPath   Path to get directory name for.
 *
 * @return Returns directory name.
 **/
std::string DirName(const std::string& aPath)
{
   auto last = aPath.find_last_of(detail::settings.delimeter);

   if (  last == std::string::npos)
   {
      return "";
   }
   else
   {
      return aPath.substr(0, last);
   }
}

/**
 * Check if path is absolute.
 *
 * @param aPath   Path to check.
 *
 * @return Returns true if path is absolute, otherwise false.
 **/
bool IsAbsPath(const std::string& aPath)
{
   if (  !aPath.empty() )
   {
      if (  aPath[0] == detail::settings.delimeter)
      {
         return true;
      }
   }
   return false;
}

/**
 * Check if path is relative.
 *
 * @param aPath   Path to check.
 *
 * @return Returns true if path is relative, otherwise false.
 **/
bool IsRelPath(const std::string& aPath)
{
   if (  !aPath.empty() )
   {
      if (  aPath[0] != detail::settings.delimeter)
      {
         return true;
      }
   }
   return false;
}

/**
 * Join two paths.
 *
 * @param aPath1   First  path.
 * @param aPath2   Second path.
 *
 * @return    Returns the joined path.
 **/
std::string Join(const std::string& aPath1, const std::string& aPath2)
{
   if(HasDirEnd(aPath1) && HasDirBegin(aPath2))
   {  
      return RemoveDirEnd(aPath1) + aPath2;
   }
   else if(HasDirEnd(aPath1) || HasDirBegin(aPath2))
   {
      return aPath1 + aPath2;
   }
   else
   {
      return aPath1 + detail::settings.delimeter + aPath2;
   }
}

} /* namespace path */
} /* namespace midas */
