#ifndef MIDAS_MEMALLOC_H_INCLUDED
#define MIDAS_MEMALLOC_H_INCLUDED

#include "util/memalloc/allocator_traits.hpp"
#include "util/memalloc/allocator.hpp"
#include "util/memalloc/mempool_allocator.hpp"
#include "util/memalloc/allocatable.hpp"

namespace midas
{
namespace memalloc
{

//
using namespace ::memalloc;

} /* namespace memalloc */
} /* namespace midas */

#endif /* MIDAS_MEMALLOC_H_INCLUDED */
