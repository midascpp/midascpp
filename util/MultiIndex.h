/**
************************************************************************
* 
* @file                util/MultiIndex.h
*
* Created:             11-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for containing/manipulating multi-indices
* 
* Last modified: man mar 21, 2005  11:40
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MULTIINDEX_H
#define MULTIINDEX_H

// std headers
#include<vector>
#include<string>

// midas headers
#include"inc_gen/TypeDefs.h"
#include"input/VccCalcDef.h"
#include"input/ModeCombi.h"

// using declarations
using std::vector;
using std::string;

/**
* Class MultiIndex: 
* */
class MultiIndex 
{
   protected:
      vector<In> mLow;                                          ///< Contains the lower limit for indices.
      vector<In> mHig;                                          ///< Contains the higher limit for indices. 
      vector<In> mNum;                                          ///< The number vector = # in [Low;Incr] 
   public:
      MultiIndex();
      MultiIndex(const vector<In>& arV1, const vector<In>& arV2,
            const string& aString ="LOWHIG",bool aExiOnly=false);
      MultiIndex(const ModeCombi& arM,
            const VccCalcDef* const apVccCalcDef)
      {ReInit(arM,apVccCalcDef);}                       // Constructors 
      void ReInit(const ModeCombi& arM,
            const VccCalcDef* const apVccCalcDef);
      void ReInit(const vector<In>& arV1, const vector<In>& arV2,
            const string& aString ="LOWHIG",bool aExiOnly=false);
                                                                ///< constructor, string = LOWHIG,LOWINC,HIGINC
      In VecSize() const {return mLow.size();}                  ///< Number of dimensions.
      In Size() const;                                          ///< Number of dimensions.
      void InForIvec(vector<In>& arIvec, In& aI) const;         ///< I for a i-vec.
      void IvecForIn(vector<In>& arIvec, In& aI) const;         ///< i-vec for a In
};

#endif
