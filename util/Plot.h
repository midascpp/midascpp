/**
************************************************************************
* 
* @file                Plot.h
*
* Created:             4-10-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Spectrum class definition and funtion
*                      declarations.
* 
* ???? Last modified: Mon Jan 18, 2010  02:02PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PLOT_H
#define PLOT_H

// std headers
#include <string>

// midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector_Decl.h"

// using declarations
using std::string;

//! Class for storing the text and position of a label.
class Label
{
   public:
       Nb x;         ///< x coord.
       Nb y;         ///< y coord.
       string lbl;   ///< Text.
};

//! Class for creating plots of data.
/** The class contins all points needed for the plot.
    On creation, the number of x-values is specified.
    It is then possible to add different shapes, e.g. gaussians
    and labels to the spectrum. */
class Plot
{
   protected:
       //MidasVector mXvals;                  ///< x values.
       //MidasVector mYvals;                  ///< y values.
       GeneralMidasVector<Nb> mXvals;         ///< x values.
       GeneralMidasVector<Nb> mYvals;         ///< y values.
       string mXlabel;
       string mYlabel;
       string mTitle;
       vector<Label> mLabels;               ///< Labels to be placed on plot.

       Nb mPlotRangeXl;                     ///< Limits for axes in plot.
       Nb mPlotRangeXr;
       Nb mPlotRangeYb;
       Nb mPlotRangeYt;

       bool mLogScaleX;
       bool mLogScaleY;

       string mLineStyle;                   ///< Line style that can be recognized by gnuplot.

       vector<string> mAnaFuncs;
       ///< Strings describing analytical functions to be plotted by gnuplot.
       
       vector<string> mAnaFuncTitles;       ///< Titles of analytical function.

       void SortDataPoints();
       ///< Sort data points according to ascending x. Necessary for e.g. locate() to work.
       
       In Locate(const Nb aX); ///< Get an index in mXVals correspoindig to aX.
       void GenerateDataFile(const string& aFilename) const;

       bool mSmooth;         ///< Do smoothing of data in plot 
       bool mKey;            ///< Show key in plot
       string mSmoothMethod; ///< Smoothing method 
   public:
       Plot(const In aSize);                ///< Size is no. of points on x axis.
       
       void SetRange(const Nb aXmin, const Nb aXmax);
       ///< Distribute numbers from aXmin to aXmax evenly across x axis. Clears y data.
       
       In SetXYvals(const MidasVector& aX, const MidasVector& aY);
       ///< Setting new x,y data. Returns 0 on succes.
       
       void SetXlabel(string aLabel) {mXlabel = aLabel;}
       void SetYlabel(string aLabel) {mYlabel = aLabel;}
       void SetTitle(string aTitle)  {mTitle = aTitle;}
      
       void SetPlotRange(Nb axl, Nb axr, Nb ayb, Nb ayt);
      
       void SetPlotRangeRel(Nb axl, Nb axr, Nb ayb, Nb ayt);
       ///< Set plot range relative to min,max values of x,y data.
       ///< xl=x,left  xr=x,right  yb=y,bottom,  yt=y,top.
       ///< E.g. x axis will run from xmin-axl*|xmin| to xmax+axr*|xmax|.

       Nb GetMinX();
       Nb GetMaxX();
       Nb GetMinY();
       Nb GetMaxY();

       void UnsetKey() {mKey=false;}
       void SetKey()   {mKey=true; }

       void SetLogScaleX(bool b) {mLogScaleX = b;}
       void SetLogScaleY(bool b) {mLogScaleY = b;}

       void ScaleX(Nb aScale);
       void ScaleY(Nb aScale);

       void SetLineStyle(const string& s) {mLineStyle = s;}
       ///< Line style is to be recognized by gnuplot.

       void AddGaussian(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel);
       ///< Add a Gaussian. aWidth is FWHM.
       
       void AddLorentzian(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel);
       ///< Add a Lorentzian. aWidth is FWHM.
       
       void AddBar(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel);
       ///< Add a bar of width aWidth centered at aXpos. aWidth = 0 gives narrowest possible bar.

       void AddLabel(const Nb aXpos, const Nb aYpos, const string& aLabel);  ///< Add a label.     
     
       void AddAnaNormDist(const Nb aMu, const Nb aSigma, const string& aTitle = "auto");
       ///< Add an analytical normal distribution to plot

       void SetSmooth(const string& aSmoothMethod) {mSmoothMethod=aSmoothMethod;mSmooth=true;} 
         ///< Set smmothing and method. Allows are gnuplot allowed =csplines, bezier, sbezier,acsplines, unique
    
       void MakeGnuPlot(const string& aFilename);   ///< Make gnuplot command file and data file.
       
       void MakeGnuPlot(const string& aFilename, Plot** aAddPlots, const In anAddPlots);
       ///< Same as MakeGnuPlot(), but create plot data, labels and analytical functions
       ///< from additional plots pointed to by aAddPlots.
       
       void MakePsFile(const string& aFilename);    ///< Plot spectrum to Postscript file.
       void MakePsFile(const string& aFilename, Plot** aAddPlots, const In anAddPlots);
};

//
// Utility functions.
//
void MakeMultiPlotPs(const string& aFilename, const string& aXlabel, const string& aYlabel,
                     string* aTitles,
                     MidasVector* aXvals, MidasVector* aYvals,
                     In anSets, bool aXlog = false, bool aYlog = false,
                     Nb aScaleX = C_1, Nb aScaleY = C_1);

void MakeMultiPlotPsAutoX(const string& aFilename, const string& aXlabel, const string& aYlabel,
                          string* aTitles,
                          In aFirstX, MidasVector* aYvals,
                          In aNsets, bool aXlog, bool aYlog,
                          Nb aScaleX, Nb aScaleY);

#include "mmv/MidasVector.h"

#endif // PLOT_H

