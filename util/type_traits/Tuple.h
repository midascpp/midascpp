/**
************************************************************************
* 
* @file    Tuple.h
*
* @date    17-12-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Check if class is tuple or pair
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TYPE_TRAITS_TUPLE_H_INCLUDED
#define TYPE_TRAITS_TUPLE_H_INCLUDED

#include <type_traits>
#include <tuple>
#include <array>

namespace midas::type_traits
{
namespace detail
{
template<typename T> struct IsTuple : public std::false_type {};
template<typename T, typename U> struct IsTuple<std::pair<T,U>> : public std::true_type {};
template<typename... Ts> struct IsTuple<std::tuple<Ts...>> : public std::true_type {};
template<typename T, std::size_t N> struct IsTuple<std::array<T,N>> : public std::true_type {};
}

//! Interface
template<typename T> inline constexpr bool IsTupleV = detail::IsTuple<T>::value;

}

#endif /* TYPE_TRAITS_TUPLE_H_INCLUDED */
