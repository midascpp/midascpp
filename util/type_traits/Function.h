/**
 *******************************************************************************
 * 
 * @file    Function.h
 * @date    11-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Static type trait tools for functions
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TYPE_TRAITS_FUNCTION_H_INCLUDED
#define MIDAS_TYPE_TRAITS_FUNCTION_H_INCLUDED

#include "Function_Impl.h"

namespace midas::type_traits
{
/**
 * Get return type of function ptr, lambda, functor, member function, etc.
 **/
template<typename T>
using ReturnType = typename detail::FunctionTraits<T>::return_type;

/**
 * Get number of arguments to function ptr, lambda, functor, member function, etc.
 **/
template<typename T>
inline constexpr std::size_t ArgumentCount = detail::FunctionTraits<T>::argc;

/**
 * Get argument type of function ptr, lambda, functor, member function, etc.
 **/
template<typename T, std::size_t N>
using ArgumentType = typename detail::FunctionTraits<T>::template argument<N>::type;
} /* namespace midas::type_traits */

#endif /* MIDAS_TYPE_TRAITS_FUNCTION_H_INCLUDED */
