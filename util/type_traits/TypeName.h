/**
 *******************************************************************************
 * 
 * @file    TypeName.h
 * @date    18-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TYPE_TRAITS_TYPENAME_H_INCLUDED
#define MIDAS_TYPE_TRAITS_TYPENAME_H_INCLUDED

#include <typeinfo>
#include <complex>
#include <string>

#include "inc_gen/TypeDefs.h"

// Declarations.
template<class> class GeneralMidasVector;
template<class> class GeneralMidasMatrix;
template<class> class GeneralDataCont;

namespace midas::type_traits
{
   namespace detail
   {
      /************************************************************************//**
       * @name Generic typename framework
       ***************************************************************************/
      //!@{

      /************************************************************************//**
       * Generic template to be used for two or more arguments; will call itself
       * recursively, until there's only one template parameter left in pack
       * expansion.
       ***************************************************************************/
      template<class T, class... Us>
      struct TypeName
      {
         std::string operator()() const
         {
            return TypeName<T>()()+","+TypeName<Us...>()();
         }
      };

      /************************************************************************//**
       * Specialization for one template parameter, which should return an actual
       * pretty-formatted type name.
       *
       * Generally we return the type_info name, which might be mangled and might
       * be implementation dependent.
       * Specialize for desired types below.
       *
       * @note
       *    Provide overloads for references, pointers, const, etc. as necessary.
       *    E.g.
       *    ~~~cpp
       *    template<class T> struct TypeName<T&> { ... };
       *    ~~~
       ***************************************************************************/
      template<class T>
      struct TypeName<T>
      {
         std::string operator()() const
         {
            return typeid(T).name();
         }
      };

      /************************************************************************//**
       * Struct for class templates (taking any number of arguments). Also
       * specialize this as desired.
       *
       * @note
       *    Don't know how to make typeid or similar work for class templates, so
       *    it'll just be some generic string.
       ***************************************************************************/
      template<template<class...> class T>
      struct TmplName
      {
         std::string operator()()
         {
            return "unknown_tmpl";
         }
      };

      /************************************************************************//**
       * Specialization for template classes, producing something like
       * "A<T0,T1,...>", i.e. wrapping the comma-separated Ts in angle-brackets.
       ***************************************************************************/
      template<template<class...> class A, class... Ts>
      struct TypeName<A<Ts...>>
      {
         std::string operator()() const
         {
            return TmplName<A>{}()+"<"+TypeName<Ts...>{}()+">";
         }
      };

      /************************************************************************//**
       * Specialization for template classes with just _one_ template
       * parameter; this enables e.g. std::vector<T> to just print
       * "std::vector<T>" without the default allocator, which oftenmost just
       * looks messy.
       *
       * @note
       *    It's doesn't seem possible to do the same for _two_ template
       *    parameters (useful for printing e.g. std::map<K,M> without its
       *    default comparison operator and allocator), due to ambiguous
       *    overload resolutions. So one has to make a dedicated overload if in
       *    need of that, e.g.
       *    ~~~cpp
       *    template<class K, class M> struct TypeName<std::map<K,M>> { ... };
       *    ~~~
       ***************************************************************************/
      template<template<class...> class A, class T>
      struct TypeName<A<T>>
      {
         std::string operator()() const
         {
            return TmplName<A>{}()+"<"+TypeName<T>{}()+">";
         }
      };

      //!@}

      /************************************************************************//**
       * @name Full specializations
       *
       * @note
       *    At the moment only implementing In, Uin and Nb (with those
       *    corresponding strings) instead of all the POD types (short, int,
       *    ..., unsigned, ..., float, double, ...). These will still have a
       *    fallback option in the typeid(T).name() variant.
       *    You can modify this if necessary. -MBH, Apr 2019.
       ***************************************************************************/
      //!@{

      //@{
      //! Common MidasCpp numeric types.
      template<> struct TypeName<In> {std::string operator()() const {return "In";}};
      template<> struct TypeName<Uin> {std::string operator()() const {return "Uin";}};
      template<> struct TypeName<Nb> {std::string operator()() const {return "Nb";}};
      //@}

      //@{
      //! Standard containers.
      template<> struct TmplName<std::complex> {std::string operator()() const {return "std::complex";}};
      template<> struct TmplName<std::vector> {std::string operator()() const {return "std::vector";}};
      //@}

      //@{
      //! Common MidasCpp containers.
      template<> struct TmplName<GeneralMidasVector> {std::string operator()() const {return "GeneralMidasVector";}};
      template<> struct TmplName<GeneralMidasMatrix> {std::string operator()() const {return "GeneralMidasMatrix";}};
      template<> struct TmplName<GeneralDataCont> {std::string operator()() const {return "GeneralDataCont";}};
      //@}

      //!@}

   } /* namespace detail */

   //@{
   //! Pretty-printed type name of T, whether T is a type or a class template.
   template<class T>
   std::string TypeName()
   {
      return detail::TypeName<T>{}();
   }

   template<template<class...> class T>
   std::string TypeName()
   {
      return detail::TmplName<T>{}();
   }
   //@}

   //! Pretty-printed type name for the type T of the passed variable.
   template<class T>
   std::string TypeName(const T&)
   {
      return TypeName<T>();
   }

} /* namespace midas::type_traits */

#endif /* MIDAS_TYPE_TRAITS_TYPENAME_H_INCLUDED */
