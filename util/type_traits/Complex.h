/**
 *******************************************************************************
 * 
 * @file    Complex.h
 * @date    24-04-2018
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Static type trait tools for complex numbers.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TYPE_TRAITS_COMPLEX_H_INCLUDED
#define MIDAS_TYPE_TRAITS_COMPLEX_H_INCLUDED

#include <type_traits>
#include <complex>

#include "util/type_traits/Complex_Impl.h"

//! Additional type traits support not (yet) provided by the C++ standard.
namespace midas::type_traits
{
   //! True if T has type std::complex<U>, else false.
   template<class T>
   inline constexpr bool IsComplexV = detail::IsComplex<T>::value;

   //! The underlying real type; T for both T and std::complex<T>.
   template<class T>
   using RealTypeT = typename detail::RealType<T>::type;

   //! Utilities
   template<class T>
   using DisableIfComplexT = typename std::enable_if_t<!IsComplexV<T>>;

   template<class T>
   using EnableIfComplexT = typename std::enable_if_t<IsComplexV<T>>;

} /* namespace midas::type_traits */

#endif /* MIDAS_TYPE_TRAITS_COMPLEX_H_INCLUDED */
