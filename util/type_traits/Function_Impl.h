/**
 *******************************************************************************
 * 
 * @file    Function_Impl.h
 * @date    11-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Static type trait tools for functions
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TYPE_TRAITS_FUNCTION_IMPL_H_INCLUDED
#define MIDAS_TYPE_TRAITS_FUNCTION_IMPL_H_INCLUDED

#include <type_traits>

namespace midas::type_traits::detail
{

/**
 * Declare function traits
 **/
template<typename F> struct FunctionTraits;

/**
 * Implement for functions
 **/
template
   <  typename R
   ,  typename... Args
   >
struct FunctionTraits<R(Args...)>
{
   using return_type = R;

   static constexpr std::size_t argc = sizeof...(Args);

   template
      <  std::size_t N
      >
   struct argument
   {
      static_assert(N < argc, "Argument number out of range!");

      using type = typename std::tuple_element<N, std::tuple<Args...>>::type;
   };
};

/**
 * Specialize for function pointers
 **/
template<typename R, typename... Args> struct FunctionTraits<R(*)(Args...)> : public FunctionTraits<R(Args...)> {};

/**
 * Specialize for member functions
 **/
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...)> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) &> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const &> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile &> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile &> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) &&> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const &&> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile &&> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile &&> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) & noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const & noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile & noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile & noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) && noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const && noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) volatile && noexcept> : public FunctionTraits<R(Args...)> {};
template<typename C, typename R, typename... Args> struct FunctionTraits<R(C::*)(Args...) const volatile && noexcept> : public FunctionTraits<R(Args...)> {};

/**
 * Default implementation for functors and lambda functions
 **/
template<typename F> struct FunctionTraits : public FunctionTraits<decltype(&F::operator())> {};
template<typename F> struct FunctionTraits<F&> : public FunctionTraits<F> {};
template<typename F> struct FunctionTraits<F&&> : public FunctionTraits<F> {};

} /* namespace midas::type_traits::detail */

#endif /* MIDAS_TYPE_TRAITS_FUNCTION_IMPL_H_INCLUDED */
