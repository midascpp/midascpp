/**
************************************************************************
* 
* @file                FileHandlerWithStreams.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for handling the output of .mop files
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <fstream>
#include <string>
#include <vector>
#include <stdarg.h>
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "util/FileHandler.h"
#include "util/FileHandlerWithStreams.h"

// using declarations
using namespace FileHandles;
using namespace FileHandlerStreams;

FileHandlerWithStreams& FileHandlerWithStreams::operator<<(ostream& (*aFunc)(ostream&))
{
   if(mToHead)
   {
      mConstantsForOutput << aFunc;
   }
   else
   {
      mStreams[mGoingTo][mSpecOfStream] << aFunc;
   }
   return *this;
}

FileHandlerWithStreams::~FileHandlerWithStreams()
{
   mFiles << FileHandles::to_all << mConstantsForOutput.str();
   for(In i = I_0; i < mNrOfStreams; ++i)
   {
      mFiles << FileHandles::to_one(i) << "FitBasis:" << endl << mFitBasisForOutput[i].str()
                                       << "Functions:" << endl << mFunctionsForOutput[i].str()
                                       << "Splines:" << endl << mSplinesForOutput[i].str()
                                       << "Operators:" <<  endl << mOperatorsForOutput[i].str();
   }
   if(mNrOfStreams > I_0)
   {
      delete[] mFunctionsForOutput;
      delete[] mOperatorsForOutput;
      delete[] mFitBasisForOutput;
      delete[] mSplinesForOutput;
      delete[] mStreams;
   }
}
