#include "util/RandomNumberGenerator.h"

#include "util/Error.h"
#include "util/Io.h"
#include "util/MidasStream.h"

namespace midas
{
namespace util
{
namespace detail
{

// mersenne twister
std::mt19937 mersenne_twister;
bool         mersenne_initialized = false;

// Getter for mersenne twister
std::mt19937& get_mersenne()
{
   if (!mersenne_initialized)
   {
      MIDASERROR("Mersienne twister not initialized before use.");
   }
   return mersenne_twister;
}

} /* namespace detail */

/**
 * Seed random number generator.
 *
 * @param seed_vec   Seed for initialization.
 **/
void SeedRng(std::vector<unsigned int> seed_vec)
{
   if (!detail::mersenne_initialized)
   {
      if (seed_vec.size() == 0)
      {
         // Seed by random device
         std::random_device r;
         seed_vec = std::vector<unsigned int>({r(), r(), r(), r(), r(), r(), r(), r()});
      }
       
      Mout << " RNG seed sequence : " << seed_vec << "\n" << std::endl;

      std::stringstream sstr;
      sstr << seed_vec;
      AddErrorInfo("RNG SEED", sstr.str());

      std::seed_seq seed(seed_vec.begin(), seed_vec.end());
      detail::mersenne_twister.seed(seed);

      detail::mersenne_initialized = true;
   }
}

/**
 * Re-seed random number generator.
 *
 * @param seed_vec   Seed for initialization.
 **/
void ReSeedRng(std::vector<unsigned int> seed_vec)
{
   MidasWarning("Re-seedning random number generator. Are you sure you want to do this?");

   if (seed_vec.size() == 0) MIDASERROR("Seed vector must be non-empty for re-seeding to make sense!");
      
   std::seed_seq seed(seed_vec.begin(), seed_vec.end());
   detail::mersenne_twister.seed(seed);

   detail::mersenne_initialized = true;
}


} /* namespace util */
} /* namespace midas */
