/**
 *******************************************************************************
 * 
 * @file    OrderedCombination.h
 * @date    27-03-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    OrderedCombination class and related stuff.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef ORDEREDCOMBINATION_H_INCLUDED
#define ORDEREDCOMBINATION_H_INCLUDED

#include <vector>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/***************************************************************************//**
 * @brief
 *    For looping through a range of combinations of elements.
 * 
 * This class is designed to loop through ordered combinations from a set of
 * elements (integers). A combination is in this context an ordered subset of
 * the elements.
 * Example: If the set of elements is {0,1,2}, then the possible combinations
 * are {}, {0}, {1}, {2}, {0,1}, {0,2}, {1,2}, {0,1,2}.
 *
 * Upon construction you can supply a vector of integers to be used as the set;
 * if supplying {-1,2,5}, e.g., the combinations will accordingly be {},
 * {-1},..., {-1,2},..., {-1,2,5}.
 *
 * You can also supply a set of _fixed_ integers, that will be included in all
 * combinations. Said in another way, if constructing from the sets {0,1} as
 * fixed integers and {2,3,4} as non-fixed integers, the object will generate
 * the combinations {0,1}, {0,1,2}, {0,1,3}, ..., {0,1,2,3}, ..., {0,1,2,3,4},
 * i.e. the combinations of {0,1,2,3,4} that contain {0,1}.
 *
 * You can specify upon construction at what _level_ you want to begin and end,
 * i.e. begin with the first combination containing 'aBeg' elements and end at
 * (_not_ including) the first combination containing 'aEnd' elements.
 *
 * The class is designed with ModeCombi%s and ModeCombiOpRange%s in mind, and
 * for doing loops in a manner such as this:
 *
 * ~~~{.cpp}
 * for(OrderedCombination oc(0, 3, 5); !oc.AtEnd(); ++oc)
 * {
 *    // do stuff with the OrderedCombinations
 *    // {}, {0}, ..., {4}, {0,1}, ..., {3,4}
 *    // (Starting with 0 elements, ending at 3 elements.)
 *    oc.GetCombination();
 * }
 * ~~~
 *
 * The class shall always make sure that the provided fixed and nonfixed
 * elements are sorted so that the elements of the individual combinations are
 * likewise sorted in ascending order.
 *
 * Internally the object has a _base_ combination of unsigned ordered integers
 * {0,...} which is then mapped to an _actual_ combination according to the
 * fixed/nonfixed elements, when calling GetCombination(). E.g. if the internal
 * _base_ is {1,2}, the fixed elements are {11,13}, and the nonfixed elements
 * are {2,3,5,7}, the base is mapped to {3,5,11,13}.
 *
 * Incrementing the object shall be in agreement with the less-than operator
 * for ModeCombi%s (operator<(const ModeCombi&,const ModeCombi&)) in the sense
 * that incrementing shall result in an OrderedCombination that is _only just_
 * larger than the previous state according to that less-than operator.
 ******************************************************************************/
class OrderedCombination
{
   public:
      /*********************************************************************//**
       * @name Default constructors, destructor
       *
       * Assignments unnecessary so deleted for now. Implement if you must.
       * Copy constructors copies range details and current internal state.
       ************************************************************************/
      //!@{
      OrderedCombination() = delete;
      OrderedCombination& operator=(const OrderedCombination&) = delete;
      OrderedCombination& operator=(OrderedCombination&&) = delete;
      OrderedCombination(const OrderedCombination&);
      OrderedCombination(OrderedCombination&&);
      ~OrderedCombination();
      //!@}

      /*********************************************************************//**
       * @name Constructors
       *
       * In all cases, set up the object to initially be at the first
       * combination with `aBeg` nonfixed elements and to be AtEnd() when
       * reaching the first combination with `aEnd` nonfixed elements.
       *
       * Either supply a total number of elements (including any fixed ones);
       * the constructor will then just generate integers in ascending order
       * starting from 0. Or supply nonfixed elements yourself, as well as
       * fixed elements.
       *
       * Both fixed and nonfixed elements will be sorted, duplicates will be
       * removed, and any fixed elements will be removed from the nonfixed
       * ones.
       *
       * @note
       *    In all cases, the number of (nonfixed) elements must be greater
       *    than or equal to `aEnd - 1`. Also `aEnd` must not be smaller than
       *    `aBeg`.  If any of these criteria are not satisfied, it causes a
       *    hard error.
       *
       * @note
       *    The constructors take objects (not references) as arguments,
       *    however, these are `std::move`d for efficiency, although it relies
       *    on the compiler to optimize it somewhat (through copy-elision,
       *    etc.). In this way it's a unified framework for passing both
       *    lvalues and rvalues as arguments.
       ************************************************************************/
      //!@{
      OrderedCombination(Uin aBeg, Uin aEnd, Uin aNumElemsTotal);
      OrderedCombination(Uin aBeg, Uin aEnd, std::vector<In> aNonFixed);
      OrderedCombination(Uin aBeg, Uin aEnd, Uin aNumElemsTotal, std::vector<In> aFixed);
      OrderedCombination(Uin aBeg, Uin aEnd, std::vector<In> aNonFixed, std::vector<In> aFixed);
      //!@}

      /*********************************************************************//**
       * @name Internal setup
       *
       * Information about object according to how it was constructed.
       ************************************************************************/
      //!@{
      Uin NumElementsNonFixed() const;
      Uin NumElementsTotal() const;
      Uin SizeBegin() const;
      Uin SizeEnd() const;
      //!@}

      //! Return the current combination of the object.
      std::vector<In> GetCombination() const;

      //! Whether object has reached end, i.e. first-beyond-last combination.
      bool AtEnd() const;

      //! Increment the object to the next ordered combination.
      OrderedCombination& operator++();

   private:
      //! A simple {0,1,2,...} combination used for all the internal logic.
      std::vector<Uin> mBase;

      //! Fixed elements, if any, that shall be part of all combinations.
      const std::vector<In> mFixed;

      //! The elements that are varied when incrementing the object.
      const std::vector<In> mNonFixed;

      //! Number of (nonfixed) elements in the first combination in the range.
      const Uin mSizeBegin;

      //! Number of (nonfixed) elements in first-beyond-last combination.
      const Uin mSizeEnd;


      //! Checks that object has been set up consistently.
      void CheckValidity() const;

      //! Make first simple combination of size aBeg, reserving space for aEnd.
      std::vector<Uin> MakeBaseVector(Uin aBeg, Uin aEnd) const;

      //! Make vec. of nonfixed elems. according to total and fixed elements.
      std::vector<In> MakeNonFixedElems(Uin aNumElemsTotal, const std::vector<In>& arFixed) const;

      //! Make a sorted vector with all duplicate elements removed from it.
      std::vector<In> SortRemoveDuplicates(std::vector<In>) const;

      //! Remove elements in the second argument from the first argument.
      std::vector<In> InplaceDifferenceSortedVectors(std::vector<In>, const std::vector<In>&) const;

      //@{
      //! For mBase iterator, get the min/max allowed value for that position.
      Uin MinValueForPosition(std::vector<Uin>::const_reverse_iterator) const;
      Uin MaxValueForPosition(std::vector<Uin>::const_reverse_iterator) const;
      //@}

      //! Return sorted vector with nonfixed part of current combination.
      std::vector<In> GetNonFixed() const;
};

#endif/*ORDEREDCOMBINATION_H_INCLUDED*/
