/**
************************************************************************
* 
* @file                Plot2D.cc
*
* Created:             4-10-2002
*
* Author:              D. Toffoli
*
* Short Description:   Implementing Plot2D class methods.
* 
*        Last modified: Mon Nov 06, 2006  02:42PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <sstream>
#ifdef VAR_MPI
#include <mpi.h>
#endif

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "util/MidasSystemCaller.h"
#include "util/Io.h"
#include "input/Input.h"
#include "Plot2D.h"

#include "mpi/Interface.h"

#include "libmda/numeric/float_eq.h"

//TODO: testing of multiplot routines

/**
*Constructor. Data files should cointain a triplet for each record. Output is in grid fashion
**/
Plot2D::Plot2D(const In aSize)
{
   mXvals.SetNewSize(aSize, false);
   mYvals.SetNewSize(aSize, false);
   mZvals.SetNewSize(aSize, false);

   mPlotRangeXl = C_0;
   mPlotRangeXu = C_0;
   mPlotRangeYl = C_0;
   mPlotRangeYu = C_0;
   mPlotRangeZl = C_0;
   mPlotRangeZu = C_0;


   mLogScaleX = false;
   mLogScaleY = false;
   mLogScaleZ = false;

   mLineStyle = "lines";
   mSmooth    = false;
   mSmoothMethod = "NOSMOOTHING";
}

/**
* Set the range for the x and y axes; zero out array containing Z values
**/
void Plot2D::SetRange(const Nb aXmin, const Nb aXmax, const Nb aYmin, const Nb aYmax)
{
   Nb diffx = aXmax-aXmin;
   In dimx = mXvals.Size();
   Nb diffy = aYmax-aYmin;
   In dimy = mYvals.Size();

   for (In i=0; i<dimx; i++)
      mXvals[i] = aXmin + i*( diffx/(dimx-I_1) );

   for (In i=0; i<dimy; i++)
      mYvals[i] = aYmin + i*( diffy/(dimy-I_1) );

   mZvals.Zero();
}
/**
* Fed the data grid to plot
**/
In Plot2D::SetXYZvals(const MidasVector& aX, const MidasVector& aY, const MidasVector& aZ)
{
   if (aX.Size() != mXvals.Size())
      return -1;
   if (aY.Size() != mYvals.Size())
      return -2;
   if (aZ.Size() != mZvals.Size())
      return -3;


   for (In i=0; i<mXvals.Size(); i++)
   {
      mXvals[i] = aX[i];
      mYvals[i] = aY[i];
      mZvals[i] = aZ[i];
   }
   //SortDataPoints();
   return 0;
}
/**
* set the ranges for plotting
**/
void Plot2D::SetPlotRange(Nb axl, Nb axu, Nb ayl, Nb ayu, Nb azl, Nb azu)
{
   mPlotRangeXl = axl;
   mPlotRangeXu = axu;
   mPlotRangeYl = ayl;
   mPlotRangeYu = ayu;
   mPlotRangeZl = azl;
   mPlotRangeZu = azu;
}
/**
* set relative ranges for plotting
**/
void Plot2D::SetPlotRangeRel(Nb axl, Nb axu, Nb ayl, Nb ayu, Nb azl, Nb azu)
{
   Nb xmin = mXvals.FindMinValue();
   mPlotRangeXl = xmin - axl*fabs(xmin);
   Nb xmax = mXvals.FindMaxValue();
   mPlotRangeXu = xmax + axu*fabs(xmax);
   Nb ymin = mYvals.FindMinValue();
   mPlotRangeYl = ymin - ayl*fabs(ymin);
   Nb ymax = mYvals.FindMaxValue();
   mPlotRangeYu = ymax + ayu*fabs(ymax);
   Nb zmin = mZvals.FindMinValue();
   mPlotRangeZl = zmin - azl*fabs(zmin);
   Nb zmax = mZvals.FindMaxValue();
   mPlotRangeZu = zmax + azu*fabs(zmax);
}
/**
* add a label
**/
void Plot2D::AddLabel(const Nb aXpos, const Nb aYpos, const Nb aZpos, const string& aLabel)
{
   Label2D lbl;
   lbl.x = aXpos;
   lbl.y = aYpos;
   lbl.z = aZpos;
   lbl.lbl = aLabel;
   mLabels.push_back(lbl);
}

/**
* generate data file to load
**/
void Plot2D::GenerateDataFile(const string& aFilename) const
{
   string datafile = aFilename + ".data";
   
   ofstream odata(datafile.c_str(),ios_base::out);
   midas::stream::ScopedPrecision(22, odata);

   //for a clear 2d plot, find the different block of data:
   In buf_rec_init=0;
   In buf_rec_fin=0;
   for (In i=0; i<mXvals.Size(); i++)
   {
      buf_rec_fin++;
      if (buf_rec_fin>=mXvals.Size())
      {
        buf_rec_fin--;
        for (In i_rec=buf_rec_init; i_rec<=buf_rec_fin; i_rec++)
        {
           odata.setf(ios::scientific);
           odata.setf(ios::uppercase);
           odata << mXvals[i_rec] << "\t" << mYvals[i_rec] <<  "\t" << mZvals[i_rec] << endl;
        }
      }
      if (mXvals[buf_rec_fin]!=mXvals[i])
      {
         for (In i_rec=buf_rec_init; i_rec<buf_rec_fin; i_rec++)
         {
            odata.setf(ios::scientific);
            odata.setf(ios::uppercase);
            odata << mXvals[i_rec] << "\t" << mYvals[i_rec] <<  "\t" << mZvals[i_rec] << endl;
         }
         odata << endl;
         buf_rec_init=buf_rec_fin;
      }
      //buf_rec++;
      //if (buf_rec>mXvals.Size()) break;
   }
   odata.close();
}
/**
* Create gnuplot command file and gnuplot data file.
* Filename should be specified without extension, as these are added by the function.
*/
void Plot2D::MakeGnu2DPlot(const string& aFilename)
{
   string cmdfile = aFilename + ".plot";
   
   GenerateDataFile(aFilename);

   // If range has not been set, set default.
   if ((mPlotRangeXl == mPlotRangeXu) &&
       (mPlotRangeYl == mPlotRangeYu) &&
       (mPlotRangeZl == mPlotRangeZu))
      SetPlotRangeRel(C_0, C_0, C_0, C_0, C_0, C_0);

   // Start generating command file.
   ofstream ocmd(cmdfile.c_str());
   ocmd << "set term postscript enhanced" << endl
        << "set output \"" << aFilename << ".ps\""  << endl
        << "set xlabel \"" << mXlabel << "\"" << endl
        << "set ylabel \"" << mYlabel << "\"" << endl
        << "set zlabel \"" << mZlabel << "\"" << endl;

   ocmd << "set xrange [" << mPlotRangeXl << ":" << mPlotRangeXu << "]" << endl
        << "set yrange [" << mPlotRangeYl << ":" << mPlotRangeYu << "]" << endl
        << "set zrange [" << mPlotRangeZl << ":" << mPlotRangeZu << "]" << endl;
   
   // Scaling of axes.
   if (mLogScaleX)
      ocmd << "set logscale x" << endl;
   if (mLogScaleY)
      ocmd << "set logscale y" << endl;
   if (mLogScaleZ)
      ocmd << "set logscale z" << endl;

   
   // Add labels.
   for (vector<Label2D>::const_iterator it = mLabels.begin(); it != mLabels.end(); it++)
      ocmd << "set label \"" << it->lbl << "\" at " << it->x
           << "," << it->y << "," << it->z << " center" << endl;

   // Plot data and analytical functions.
   ocmd << "set style data " << mLineStyle << endl;

   ocmd << "splot \"" << aFilename << ".data\""
        << " title \"" << mTitle <<"\""; 
   //if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod; //smoothing may not work for 3D graphs

   //add analytic finctions
   if(mAnaFuncs.size() != 0)
      for (In i=0; i<mAnaFuncs.size(); i++)
            ocmd << ",\\" << endl
                 << mAnaFuncs[i] << " t \"" << mAnaFuncTitles[i];
}

/**
* Same as before. but make several plots at a time
* these can be added in the same if the names are the same except by the extension .int, .grid
* Note that anAddPlots should contain the value for only separate files
**/
void Plot2D::MakeGnu2DPlot(const string& aFilename,
                       Plot2D** aAddPlots, const In anAddPlots)
{
   string cmdfile = aFilename + ".plot";
   
   GenerateDataFile(aFilename);
   for (In i=0; i<anAddPlots; i++)
      aAddPlots[i]->GenerateDataFile(aFilename + "_" + std::to_string(i+1));

   // If range has not been set, set default.
   if ((mPlotRangeXl == mPlotRangeXu) &&
       (mPlotRangeYl == mPlotRangeYu))
      SetPlotRangeRel(C_0, C_0, C_0, C_0, C_0, C_0);

   // Start generating command file.
   ofstream ocmd(cmdfile.c_str());
   ocmd << "set term postscript enhanced" << endl
        << "set output \"" << aFilename << ".ps\""  << endl
        << "set xlabel \"" << mXlabel << "\"" << endl
        << "set ylabel \"" << mYlabel << "\"" << endl
        << "set zlabel \"" << mZlabel << "\"" << endl;

   ocmd << "set xrange [" << mPlotRangeXl << ":" << mPlotRangeXu << "]" << endl
        << "set yrange [" << mPlotRangeYl << ":" << mPlotRangeYu << "]" << endl
        << "set zrange [" << mPlotRangeZl << ":" << mPlotRangeZu << "]" << endl;
   
   // Scaling of axes.
   if (mLogScaleX)
      ocmd << "set logscale x" << endl;
   if (mLogScaleY)
      ocmd << "set logscale y" << endl;
   if (mLogScaleZ)
      ocmd << "set logscale z" << endl;
   
   // Add labels.
   for (vector<Label2D>::const_iterator it = mLabels.begin(); 
          it!=mLabels.end(); it++)
      ocmd << "set label \"" << it->lbl << "\" at " << it->x
           << "," << it->y << "," << it->z << " center" << endl;
           
   //now add labels for other plots in the same frame
   for (In i=0; i<anAddPlots; i++)
      for (vector<Label2D>::const_iterator it = aAddPlots[i]->mLabels.begin();
           it != mLabels.end(); it++)
         ocmd << "set label \"" << it->lbl << "\" at " << it->x
              << "," << it->y << "," << it->z << " center" << endl;
       
   // Plot data and analytical functions.
   ocmd << "splot \"" << aFilename << ".data\""
        << " with " << mLineStyle
        << " title \"" << mTitle<<"\"";
   //if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod;

   //plot the others
   for (In i=0; i<anAddPlots; i++)
   {
      ocmd << ",\\" << endl
           << "splot \"" << aFilename << "_" << std::to_string(i) << ".data\""
           << " with " << mLineStyle
           << " title \"" << aAddPlots[i]->mTitle<<"\"";
      //if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod;
   }

   for (In i=0; i<mAnaFuncs.size(); i++)
         ocmd << ",\\" << endl
              << mAnaFuncs[i] << " t \"" << mAnaFuncTitles[i];

   for (In i=0; i<anAddPlots; i++)
      for (In j=0; j<aAddPlots[i]->mAnaFuncs.size(); j++)
            ocmd << ",\\" << endl
                 << aAddPlots[i]->mAnaFuncs[j]
                 << " t \"" << aAddPlots[i]->mAnaFuncTitles[j];
}

void Plot2D::MakePsFile(const string& aFilename)
{
   if(midas::mpi::get_Interface().GetMpiGlobalRank() == 0) 
   {
      MakeGnu2DPlot(aFilename);
      
      {
         system_command_t cd_cmd = {"cd",gAnalysisDir};
         MIDASSYSTEM(cd_cmd);
      }
      {
         system_command_t plot_cmd = {"gnuplot",aFilename+".plot"};
         MIDASSYSTEM(plot_cmd);
      }
      {
         system_command_t cd_cmd = {"cd",".."};
         MIDASSYSTEM(cd_cmd);
      }
   }

//#ifdef VAR_MPI
//   MPI_Barrier(MPI_COMM_WORLD);
//#endif
}

void Plot2D::MakePsFile
   (  const string& aFilename
   ,  Plot2D** aAddPlots
   ,  const In anAddPlots
   )
{
   if(midas::mpi::get_Interface().GetMpiGlobalRank() == 0) 
   {
      MakeGnu2DPlot(aFilename, aAddPlots, anAddPlots);
      {
         system_command_t plot_cmd = {"gnuplot",aFilename+".plot"};
         MIDASSYSTEM(plot_cmd);
      }
   }

//#ifdef VAR_MPI
//   MPI_Barrier(MPI_COMM_WORLD);
//#endif

}

/***********************************
 *
 * Class implementation is over.
 * Next are some utility functions.
 *
 ***********************************/

/** Utility function for plotting a set of data in the same figure. */
void MakeMulti2DPlotPs(const string& aFilename, const string& aXlabel, const string& aYlabel, const string& aZlabel,
                     string* aTitles, MidasVector* aXvals, MidasVector* aYvals, MidasVector* aZvals,
                     In anSets, bool aXlog, bool aYlog, bool aZlog,
                     Nb aScaleX, Nb aScaleY, Nb aScaleZ)
{
   Plot2D mainplot(aXvals[0].Size());

   // Just in case someone tries to plot negative values on a logarithmic scale.
   if (aXlog) for(int i=0; i<aXvals[0].Size(); ++i) if(libmda::numeric::float_neg(aXvals[0][i])) MIDASERROR("Cannot plot negative numbers on log-scale.");
   if (aYlog) for(int i=0; i<aYvals[0].Size(); ++i) if(libmda::numeric::float_neg(aYvals[0][i])) MIDASERROR("Cannot plot negative numbers on log-scale.");
   if (aZlog) for(int i=0; i<aZvals[0].Size(); ++i) if(libmda::numeric::float_neg(aZvals[0][i])) MIDASERROR("Cannot plot negative numbers on log-scale.");
    
   mainplot.SetXYZvals(aXvals[0], aYvals[0], aZvals[0]);
   mainplot.SetXlabel(aXlabel);
   mainplot.SetYlabel(aYlabel);
   mainplot.SetZlabel(aZlabel);
   mainplot.SetTitle(aTitles[0]);
   mainplot.SetLogScaleX(aXlog);
   mainplot.SetLogScaleY(aYlog);
   mainplot.SetLogScaleZ(aZlog);
   mainplot.ScaleX(aScaleX);
   mainplot.ScaleY(aScaleY);
   mainplot.ScaleZ(aScaleZ);

   // For determining plotting range.
   Nb xmin = aXvals[0].FindMinValue();
   Nb xmax = aXvals[0].FindMaxValue();
   Nb ymin = aYvals[0].FindMinValue();
   Nb ymax = aYvals[0].FindMaxValue();
   Nb zmin = aZvals[0].FindMinValue();
   Nb zmax = aZvals[0].FindMaxValue();


   Plot2D** addplots = new Plot2D*[anSets-1];
   for (In i=1; i<anSets; i++)
   {
      if (aXlog) for(int j=0; j<aXvals[i].Size(); ++j) if(libmda::numeric::float_neg(aXvals[i][j])) MIDASERROR("Cannot plot negative numbers on log-scale.");
      if (aYlog) for(int j=0; j<aYvals[i].Size(); ++j) if(libmda::numeric::float_neg(aYvals[i][j])) MIDASERROR("Cannot plot negative numbers on log-scale.");
      if (aZlog) for(int j=0; j<aZvals[i].Size(); ++j) if(libmda::numeric::float_neg(aZvals[i][j])) MIDASERROR("Cannot plot negative numbers on log-scale.");
         
      addplots[i-1] = new Plot2D(aXvals[i].Size());
      addplots[i-1]->SetXYZvals(aXvals[i], aYvals[i], aZvals[i]);
      addplots[i-1]->ScaleX(aScaleX);
      addplots[i-1]->ScaleY(aScaleY);
      addplots[i-1]->ScaleZ(aScaleZ);

      if (aXvals[i].FindMinValue() < xmin) xmin=aXvals[i].FindMinValue();
      if (aXvals[i].FindMaxValue() > xmax) xmax=aXvals[i].FindMaxValue();
      if (aYvals[i].FindMinValue() < ymin) ymin=aYvals[i].FindMinValue();
      if (aYvals[i].FindMaxValue() > ymax) ymax=aYvals[i].FindMaxValue();
      if (aZvals[i].FindMinValue() < zmin) zmin=aZvals[i].FindMinValue();
      if (aZvals[i].FindMaxValue() > zmax) zmax=aZvals[i].FindMaxValue();
   }
   //ymin = ymin - fabs(ymin)/C_10;
   //ymax = ymax + fabs(ymax)/C_10;
   zmin = zmin - fabs(zmin)/C_10;
   zmax = zmax + fabs(zmax)/C_10;
   if (fabs(zmax-zmin)<C_NB_EPSILON*C_10)
      zmax = zmin + C_NB_EPSILON*C_10_2;
   
   mainplot.SetPlotRange(aScaleX*xmin, aScaleX*xmax, aScaleY*ymin, aScaleY*ymax, aScaleZ*zmin, aScaleZ*zmax);
   mainplot.MakePsFile(aFilename, addplots, anSets-1);

   for (In i=1; i<anSets; i++)
      delete addplots[i-1];
   delete [] addplots;
}

/** 
* Utility function for plotting a set of data in the same figure.
* xValues are automatically generated as aFirstX(Y), aFirstX(Y)+1, aFirstX(Y)+2,...
**/
void MakeMulti2DPlotPsAutoX(const string& aFilename,
                          const string& aXlabel, const string& aYlabel, const string& aZlabel,
                          string* aTitles,
                          In aFirstX, In aFirstY, MidasVector* aZvals,
                          In aNsets, bool aXlog, bool aYlog, bool aZlog,
                          Nb aScaleX, Nb aScaleY, Nb aScaleZ)
{
// Simply generate the x and y values and call the overloaded function which takes
// the x values as an argument.
   MidasVector* xvals = new MidasVector[aNsets];
   for (In i=0; i<aNsets; i++)
   {
      xvals[i].SetNewSize(aZvals[i].Size());
      for (In n=0; n<aZvals[i].Size(); n++)
         xvals[i][n] = aFirstX+n;
   }

   MidasVector* yvals = new MidasVector[aNsets];
   for (In i=0; i<aNsets; i++)
   {
      yvals[i].SetNewSize(aZvals[i].Size());
      for (In n=0; n<aZvals[i].Size(); n++)
         yvals[i][n] = aFirstY+n;
   }


   MakeMulti2DPlotPs(aFilename, aXlabel, aYlabel, aZlabel, aTitles, xvals, yvals, aZvals, aNsets,
                   aXlog, aYlog, aZlog, aScaleX, aScaleY, aScaleZ);

   delete[] xvals;
   delete[] yvals;
}
