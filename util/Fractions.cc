/**
************************************************************************
* 
* @file                Fractions.cc
*
* Created:             18-03-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Tools for displaying fractions.
* 
* Last modified: Fri May 01, 2009  12:57PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <sstream>
#include <cmath>

// midas headers
#include "Fractions.h"

void FracFloat(Nb aF, In& aNom, In& aDen)
{
   if (C_0 == aF)
   {
      aNom = I_0;
      aDen = I_1;
      return;
   }

   In sgn = aF<C_0? -I_1: I_1;
   aF = fabs(aF);

   Nb inv = C_1/aF;
   Nb a   = inv;
   Nb tot = C_1;
   while(true)
   {
      Nb dec = a-In(a);
      if (dec < pow(C_10, -C_7))
         break;
      a = C_1/dec;
      tot *= a;
   }
   aNom = sgn*In(tot);
   aDen = In(tot*inv);
}

void FracFloat(Nb aF, std::string& aStr)
{
   In nom,den;
   FracFloat(aF, nom, den);
   std::ostringstream ss;
   if (nom < 0)
      ss << "-";
   else
      ss << "+";
   ss << abs(nom);
   if (den != I_1)
      ss << "/" << den;
   aStr = ss.str();
}

void FracFloatLatex(Nb aF, std::string& aStr, bool aSign)
{
   In nom,den;
   FracFloat(aF, nom, den);
   std::ostringstream os;
   if (I_1 != den)
   {
      if (aSign && nom >= I_0)
         os << "+";
      if (nom < I_0)
         os << "-";
      os << "\\frac{" << abs(nom) << "}{" << den << "}";
   }
   else
      if (-I_1 == nom)
         os << "-";
      else if (I_1 != nom)
      {
         if (aSign && nom>I_0)
            os << "+";
         os << nom;
      }
      else if (aSign)
         os << "+";
   aStr = os.str();
}
