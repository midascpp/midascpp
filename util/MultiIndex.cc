/**
************************************************************************
* 
* @file                MultiIndex.cc
*
* Created:             11-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement MultiIndex class members 
* 
* Last modified: man mar 21, 2005  11:41
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string> 
#include <vector> 

// midas headers
#include "inc_gen/TypeDefs.h" 
#include "input/Input.h" 
#include "util/Io.h" 
#include "util/MultiIndex.h" 
#include "input/ModeCombi.h" 
#include "input/VccCalcDef.h" 

// using declarations
using std::string;
using std::vector;

/**
* Initialize the MultiIndex
* */
MultiIndex::MultiIndex()
{
}
MultiIndex::MultiIndex(const vector<In>& arV1, 
      const vector<In>& arV2,const string& aString,bool aExciOnly)
{
   ReInit(arV1,arV2,aString,aExciOnly);
}
/**
* Re-Initialize the MultiIndex, from ModeCombi
* */
void MultiIndex::ReInit(const ModeCombi& arM,
      const VccCalcDef* const apVccCalcDef)
{
   In ndim = arM.Size();
   vector<In> occ(ndim);
   vector<In> occmax(ndim);
   for (In i=0;i<ndim;i++) 
   {
      occ[i] = I_0;
      In i_op_mode =  arM.Mode(i);
      In nmod      = apVccCalcDef->Nmodals(i_op_mode);
      occmax[i]    = nmod-I_1;
   }
   string lowhig = "LOWHIG";
   bool exci_only=true; 
   ReInit(occ,occmax,lowhig,exci_only);
}
void MultiIndex::ReInit(const vector<In>& arV1, 
      const vector<In>& arV2,const string& aString,bool aExciOnly)
{
   mLow.clear();
   mHig.clear();
   mNum.clear();
   if (arV1.size() != arV2.size()) MIDASERROR(" Size mismatch of input vectors in MultiIndex.");

   // If aExciOnly then add one to the lowest index! dirty but convenient solution for many uses.
   if (aExciOnly)
   {
      if (aString == "LOWHIG")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mLow.push_back(arV1[i]+1);
            mHig.push_back(arV2[i]);
            mNum.push_back(arV2[i]-mLow[i]+1);
         }
      }
      else if (aString == "LOWINC")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mLow.push_back(arV1[i]+1);
            mNum.push_back(arV2[i]);
            mHig.push_back(arV2[i]+mLow[i]-1);
         }
      }
      else if (aString == "HIGINC")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mHig.push_back(arV1[i]);
            mNum.push_back(arV2[i]);
            mLow.push_back(arV1[i]-arV2[i]+1);
         }
      }
      else
      {
         MIDASERROR("string not recognized");
      }
   }
   else
   {
      if (aString == "LOWHIG")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mLow.push_back(arV1[i]);
            mHig.push_back(arV2[i]);
            mNum.push_back(arV2[i]-arV1[i]+1);
         }
      }
      else if (aString == "LOWINC")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mLow.push_back(arV1[i]);
            mNum.push_back(arV2[i]);
            mHig.push_back(arV2[i]+arV1[i]-1);
         }
      }
      else if (aString == "HIGINC")
      {
         for (In i=0;i<arV1.size();i++)
         {
            mHig.push_back(arV1[i]);
            mNum.push_back(arV2[i]);
            mLow.push_back(arV1[i]-arV2[i]+1);
         }
      }
      else
      {
         MIDASERROR("string not recognized");
      }
   }
}
/**
* Calculated one index and multi index address.
* */
void MultiIndex::InForIvec(vector<In>& arIvec,In& aI) const
{
   In n=VecSize();
   if (n>0)
   {
      aI =arIvec[0]-mLow[0];
      for (In idx=1;idx<n;idx++) aI = aI*mNum[idx] + (arIvec[idx]-mLow[idx]);
   }
   else 
   {
      aI = 0;
   }
}
/**
* Calculated one index and multi index address.
* NOT TESTED FOR LOWER != 0
* */
void MultiIndex::IvecForIn(vector<In>& arIvec,In& aI) const
{
   In n=VecSize();
   if (n>0)
   {
      In i_tmp = aI; 
      for (In idx=n-1;idx>=0;idx--) 
      {
         //Mout << " idx " << idx << " aI " << aI << " i_tmp " << i_tmp << " arIvec " << arIvec[idx] << 
         //" high" << mNum[idx] << " low " << mLow[idx] << endl;
         arIvec[idx] = i_tmp%mNum[idx] + mLow[idx];
         i_tmp /= mNum[idx];
         //Mout << " idx " << idx << " aI " << aI << " i_tmp " << i_tmp << " arIvec " << arIvec[idx] << endl;
      }
   }
   else 
   {
      arIvec.clear();
   }
}
/**
 * * Calculated one index and multi index address.
 * * NOT TESTED FOR LOWER != 0
 * * */
In MultiIndex::Size() const
{
   In n=VecSize();
   if (n==0) return 1;
   In i=mNum[0];
   for (In idx=1;idx<n;idx++) i *= mNum[idx];
   return i;
}
