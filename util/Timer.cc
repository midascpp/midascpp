/**
************************************************************************
* 
* @file                Timer.cc
*
* Created:             24-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement timing class and utilities
* 
* Last modified: Thu Dec 9, 2021  11:17AM Rasmus
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <chrono>

// Link to standard headers:
#include "inc_gen/math_link.h" 
#include "inc_gen/time_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "util/MidasStream.h"

void OutputDate(std::ostream& aOut, const string& s)
{
   time_t now = time(NULL);
   aOut << s << ctime(&now) << endl;
}

Timer::Timer()
{
   mClockWorks = true;
   mClock0 = clock();
   mClockLast = mClock0;
   if (mClock0 == clock_t(-1)) mClockWorks = false;

   mTime0  = std::chrono::high_resolution_clock::now();
   mTimeLast = mTime0; 
}
/**
* Output Cpu time used relative to last call (or relative to first call
* if aRelative0 = true).
* */
void Timer::CpuOut(ostream& aOut, const string& aString,bool aRelative0)
{
   clock_t clock_now = clock();
   if (clock_now == clock_t(-1)) mClockWorks = false;
   clock_t clock_rel;
   if (!aRelative0) 
   {
      clock_rel = mClockLast;
      mClockLast = clock_now;
   }
   if (aRelative0)  clock_rel = mClock0;
   Nb out = Nb(clock_now-clock_rel)/CLOCKS_PER_SEC;
   midas::stream::ScopedPrecision(8, aOut);

   aOut << left << setw(45) << aString << setw(I_8) << right << scientific << out 
      << left << " seconds " << endl;
   if (!mClockWorks) aOut << 
      " NB! CPU timer does not work or is overflown " << endl;
}

/**
* Output Wall time relative to last call (or relative to first call
* if aRelative0 = true).
* */
void Timer::WallOut(ostream& aOut, const string& aString,bool aRelative0)
{
   auto time_now = std::chrono::high_resolution_clock::now();
   std::chrono::time_point<std::chrono::high_resolution_clock> time_rel;
   if (!aRelative0) 
   {
      time_rel = mTimeLast;
      mTimeLast = time_now;
   }
   if (aRelative0)  time_rel = mTime0;
   Nb out = std::chrono::duration<Nb> (time_now - time_rel).count();
   midas::stream::ScopedPrecision(8, aOut);
   aOut << left << setw(45) << aString << setw(I_8) << right << scientific << out 
      << left << " seconds " << endl;
}

/**
* Give Cpu time used relative to last call (or relative to first call
* if aRelative0 = true).
* */
Nb Timer::CpuTime(bool aRelative0)
{
   clock_t clock_now = clock();
   if (clock_now == clock_t(-1)) mClockWorks = false;
   clock_t clock_rel;
   if (!aRelative0) 
   {
      clock_rel = mClockLast;
      mClockLast = clock_now;
   }
   if (aRelative0)  clock_rel = mClock0;
   Nb out = Nb(clock_now-clock_rel)/CLOCKS_PER_SEC;
   if (!mClockWorks) 
      Mout << " NB! CPU timer does not work or is overflown " << endl;
   return out;
}

/**
* Give Wall time relative to last call (or relative to first call
* if aRelative0 = true).
* */
Nb Timer::WallTime(bool aRelative0)
{
   auto time_now = std::chrono::high_resolution_clock::now();
   std::chrono::time_point<std::chrono::high_resolution_clock> time_rel;
   if (!aRelative0) 
   {
      time_rel  = mTimeLast;
      mTimeLast = time_now;
   }
   if (aRelative0)  time_rel = mTime0;
   Nb out = std::chrono::duration<Nb> (time_now - time_rel).count();
   return out;
}

/**
* Set timer
**/
void Timer::SetTimer()
{
   mClockLast = clock();
   if (mClockLast == clock_t(-1)) mClockWorks = false;
   
   mTimeLast = std::chrono::high_resolution_clock::now();
}

/**
* Reset the timer
**/
void Timer::Reset()
{
   mClockWorks = true;
   mClock0 = clock();
   mClockLast = mClock0;
   if (mClock0 == clock_t(-1)) mClockWorks = false;

   mTime0  = std::chrono::high_resolution_clock::now();
   mTimeLast = mTime0; 
}
