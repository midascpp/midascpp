#include "util/Error.h"

/**
 * Inserts functions/constants into the container by using the shunting yard algorithm.
 * Note that the function container uses the functions already in when creating functions
 *
 * @param aName      The name of the function.
 * @param aFunction  The function as a string.
 * @param aVars      ?
 **/
template<class T>
void FunctionContainer<T>::Insert
   (  const std::string& aName
   ,  const std::string& aFunction
   ,  const std::vector<std::string>& aVars
   )
{
   if(!aFunction.length())
   {
      return;
   }
   if(mFunctionMap.find(aName) != mFunctionMap.end())
   {
      // error state
      MIDASERROR("ERROR STATE");
   }
   ShuntingYard<T> for_work(aVars);
   In arit_count;
   mFunctionMap.emplace(std::make_pair(aName,for_work.GenerateExpression(aFunction, *this, arit_count)));

   //Run shunting yard and save the result in the map
   //Mout << "Map is now : " << endl;
   //for(auto it = mFunctionMap.begin(); it != mFunctionMap.end(); ++it)
   //{
   //   Mout << it->first << " " << it->second << endl;
   //}
}

/**
 *
 **/
template<class T>
FunctionContainer<T>::FunctionContainer
   (  const FunctionContainer<T>& aFC
   )
   :  mFunctionMap()
   ,  mConstants(aFC.mConstants)
   ,  mFunctions(aFC.mFunctions)
{
   // manually copy map, as we need to make a clone of each function
   for(auto it = aFC.mFunctionMap.begin(); it != aFC.mFunctionMap.end(); ++it)
   {
      mFunctionMap.emplace(std::make_pair(it->first, it->second->Copy()));
   }
}

/**
 * Finds a function/constant in the map and returns a copy of the function for use in the Shunting Yard algorithm
 **/
template<class T>
std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FunctionContainer<T>::FindFunction
   (  const std::string& aName
   )  const
{
   auto it = mFunctionMap.find(aName);
   if(mFunctionMap.end() == it)
   {
      Mout << "Map contains : \n";
      for(auto it = mFunctionMap.begin(); it != mFunctionMap.end(); ++it)
         Mout << it->first << " : \n";
      Mout.flush();
      MIDASERROR("Could not find function \""+aName+"\" in map");
   }
   return it->second->Copy();
}

/**
 * Inserts a function of the form f(a,b,c) where f is the name and a,b,c are variables...
 * The variables and parenthesis are stripped of the input string aName by FindVarsAndName
 **/
template<class T>
void FunctionContainer<T>::InsertFunction
   (  const std::string& aName
   ,  const std::string& aFunction
   )
{
   string temp_name(aName);
   std::vector<std::string> vars;
   FindVarsAndName(temp_name, vars);
   mFunctions.emplace_back(make_pair(temp_name,vars.size()));
   Mout << "Inserting function : " << temp_name << " with vars : " << vars << " and definition : " << aFunction << endl;
   Insert(temp_name, aFunction, vars);
}

/**
 * Inserts a constat with a simple name into the container
 **/
template<class T>
void FunctionContainer<T>::InsertConstant
   (  const std::string& aName
   ,  T aConst
   )
{
   if(isdigit(aName[I_0]))
   {
      //ERROR STATE
      MIDASERROR("ERROR STATE");
   }
   mConstants.emplace_back(aName);
   if(mFunctionMap.find(aName) != mFunctionMap.end())
   {
      //error state
      MIDASERROR("ERROR STATE");
   }
   mFunctionMap.emplace(std::make_pair(aName,std::make_unique<PrimitiveExpressions::Literal<T>>(aConst)));
}

/**
 *  Tries to find a constant which is currently at the start of the string and remove the constant from the string
 **/
template<class T>
std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FunctionContainer<T>::FindConstant
   (  std::string& aStr
   ,  std::string& aConstName
   )  const
{
   In curr_size = I_0;
   In match = -I_1;
   for(In i = I_0; i < mConstants.size(); ++i)
   {
      if(mConstants[i].size() > curr_size)
      {
         size_t poss = aStr.find(mConstants[i]);
         if(I_0 == poss)
         {
            match = i;
            curr_size = mConstants[i].size();
         }
      }
   }
   if(-I_1 == match)
      return nullptr;
   aConstName = aStr.substr(I_0, curr_size);
   aStr.erase(I_0, curr_size);
   return FindFunction(mConstants[match]);
}

/**
 *  Finds a function token in the first part of the string and removes it returning a copy of the function and aNrOfArgs
 **/
template<class T>
std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FunctionContainer<T>::FindFunction
   (  std::string& aStr
   ,  In& aNrOfArgs
   ,  std::string& aFuncName
   )  const
{
   In curr_size = I_0;
   In match = -I_1;
   for(In i = I_0; i < mFunctions.size(); ++i)
   {
      if(mFunctions[i].first.size() > curr_size)
      {
         size_t poss = aStr.find(mFunctions[i].first);
         if(I_0 == poss)
         {
            match = i;
            curr_size = mFunctions[i].first.size();
         }
      }
   }
   if(-I_1 == match)
      return nullptr;
   aFuncName = aStr.substr(I_0,curr_size);
   aStr.erase(I_0, curr_size);
   aNrOfArgs = mFunctions[match].second;
   return FindFunction(mFunctions[match].first);
}

/**
 * Clears the function container, needed for the Adga reread of the operator
 **/
template<class T>
void FunctionContainer<T>::Clear
   (
   )
{
   mFunctionMap.clear();
   mConstants.clear();
   mFunctions.clear();
}

/**
 * Finds the function name and the variables of the function...
 **/
template<class T>
void FunctionContainer<T>::FindVarsAndName
   (  std::string& aFuncDef
   ,  std::vector<std::string>& aVars
   )
{
   std::string test(aFuncDef);
   if(test[test.length() - 1] != ')')
   {
      //ERROR STATE
      MIDASERROR("ERROR STATE! Trying to parse string : '" + test + "'.");
   }
   test.resize(test.length()-1); //remove last character
   size_t right_par = aFuncDef.find("(");
   if(string::npos == right_par)
   {
      //ERROR STATE
      MIDASERROR("ERROR STATE! Trying to parse string : '" + test + "'.");
   }
   aFuncDef = test.substr(0, right_par);
   test.erase(0,right_par+1); //Remove the name and the '('
   if(string::npos != test.find_first_of("()"))
   {
      //ERROR STATE
      MIDASERROR("ERROR STATE! Trying to parse string : '" + test + "'.");
   }
   while(test.length())
   {
      //Mout << "test : " << test << endl;
      size_t comma = test.find(",");
      if(std::isdigit(test[0]))
      {
         //ERROR STATE
         MIDASERROR("ERROR STATE! Trying to parse string : '" + test + "'.");
      }
      aVars.emplace_back(test.substr(0, comma)); //Save var name
      test.erase(0, comma + 1); //Remove the varname + ","
      if(string::npos == comma)
      {
         test.erase(0, comma);
      }
   }
   //if(!test.length())
   //{
   //   //ERROR STATE
   //   MIDASERROR("ERROR STATE");
   //}
   //Mout << "Function name : " << name << std::endl;
   //for(size_t i = 0; i < variables.size(); ++i)
      //Mout << "Variable " << i << " : " << variables[i] << std::endl;
}

/**
 *
 **/
template<class T>
void FunctionContainer<T>::WriteCppCode
   (  std::ostream& arCodeStream
   )  const
{
   for(const auto& func : mFunctionMap)
   {
      arCodeStream << func.second->GenerateCppCode() << ";\n" << std::flush;
   }
}
