/**
************************************************************************
*
* @file                GenericFunctions.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Definitions of the primitive functions for the function wrappers
*
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ONEMODEFUNCTIONGEN_H_INCLUDED
#define ONEMODEFUNCTIONGEN_H_INCLUDED

// std headers
#include <math.h>
#include <iostream>
#include <string>
#include <memory>

#include "libmda/util/to_string.h"

// midas headers
#include "util/Io.h"
#include "mathlib/Taylor/taylor.hpp"

namespace PrimitiveExpressions{
   template<class T>
   class AbstractExpr{
      public:
         virtual bool Compare(const std::unique_ptr<AbstractExpr<T>>&) const = 0;
         virtual std::unique_ptr<AbstractExpr<T>> Copy() const = 0;
         virtual T eval(const vector<T>&) const = 0;
         virtual ~AbstractExpr() {}
         virtual std::string GenerateCppCode() const = 0;
   };

   template<class T>
   class TerminalExpr : public AbstractExpr<T>{
      public:
         virtual ~TerminalExpr() {}
   };

   template<class T>
   class NonTerminalExpr : public AbstractExpr<T>{
      public:
        virtual ~NonTerminalExpr() {}
   };

   template<class T>
   class Literal : public TerminalExpr<T>{
      public:
         Literal(T v) : _val(v) {}
         T eval(const vector<T>&) const {return _val;}
         std::unique_ptr<AbstractExpr<T>> Copy() const {return std::make_unique<Literal<T>>(_val);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Literal<T>* ptr = dynamic_cast<const Literal<T>* >(aPtr.get());
            if(ptr)
               return ptr->_val == _val;
            return false;
         }
         std::string GenerateCppCode() const
         {
            return libmda::util::to_string_with_precision(_val);
         }
         //Note above that we need this to return a new with the right val..
         ~Literal() {}
      private:
         const T _val;
   };

   template<class T,int Nvar,int Ndeg>
   class Literal<taylor<T, Nvar, Ndeg> > : public TerminalExpr<taylor<T, Nvar, Ndeg> >{
      public:
         Literal(taylor<T, Nvar, Ndeg> v) : _val(v) {}
         taylor<T, Nvar, Ndeg> eval(const vector<taylor<T,Nvar,Ndeg> >& aVars) const {return _val;}
         std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg> > > Copy() const
            {return std::make_unique<Literal<taylor<T,Nvar,Ndeg> > >(_val);}
         bool Compare(const std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg> >>& aPtr) const
         {
            const Literal<taylor<T, Nvar, Ndeg> >* ptr = dynamic_cast<const Literal<taylor<T, Nvar, Ndeg> >* >(aPtr.get());
            if(ptr)
            {
               return ptr->_val[0] == _val[0];
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            MIDASERROR("");
            return "";
         }
         ~Literal() {}
      private:
         const taylor<T, Nvar, Ndeg> _val;
   };

   template<class T>
   class LiteralInt : public TerminalExpr<T>{
      public:
         LiteralInt(In v) : _val(v) {}
         T eval(const vector<T>&) const {return _val;}
         std::unique_ptr<AbstractExpr<T>> Copy() const {return std::make_unique<LiteralInt<T>>(_val);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const LiteralInt<T>* ptr = dynamic_cast<const LiteralInt<T>* >(aPtr.get());
            if(ptr)
               return ptr->_val == _val;
            return false;
         }
         std::string GenerateCppCode() const
         {
            return std::to_string(_val);
         }
         //Note above that we need this to return a new with the right val..
         ~LiteralInt() {}
         const In _val;
   };

   template<class T, int Nvar, int Ndeg>
   class LiteralInt<taylor<T, Nvar, Ndeg> > : public TerminalExpr<taylor<T, Nvar, Ndeg> >{
      public:
         LiteralInt(In v) : _val(v) {}
         taylor<T, Nvar, Ndeg> eval(const vector<taylor<T, Nvar, Ndeg>>&) const {return taylor<T, Nvar, Ndeg>(_val);}
         std::unique_ptr<AbstractExpr<taylor<T, Nvar, Ndeg>>> Copy() const
            {return std::make_unique<LiteralInt<taylor<T, Nvar, Ndeg>>>(_val);}
         bool Compare(const std::unique_ptr<AbstractExpr<taylor<T, Nvar, Ndeg>>>& aPtr) const
         {
            const LiteralInt<taylor<T, Nvar, Ndeg>>* ptr = dynamic_cast<const LiteralInt<taylor<T, Nvar, Ndeg>>* >(aPtr.get());
            if(ptr)
               return ptr->_val == _val;
            return false;
         }
         std::string GenerateCppCode() const
         {
            return std::to_string(_val);
         }
         //Note above that we need this to return a new with the right val..
         ~LiteralInt() {}
         const In _val;
   };

   template<class T>
   class Variable : public TerminalExpr<T>{
      public:
         Variable(In aposs) : _poss(aposs) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const {return std::make_unique<Variable<T>>(_poss);}
         T eval(const vector<T>& aVars) const {return aVars[_poss];}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Variable<T>* ptr = dynamic_cast<const Variable<T>*>(aPtr.get());
            if(ptr)
               return _poss == ptr->_poss;
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "variable_" + std::to_string(_poss);
         }
         ~Variable() {}
      private:
         In _poss;
   };

   template<class T>
   class UnaryExpr : public NonTerminalExpr<T>{
      protected:
         UnaryExpr(std::unique_ptr<AbstractExpr<T>> e) : _expr(std::move(e)) {}
         virtual ~UnaryExpr() {}
         std::unique_ptr<AbstractExpr<T>> _expr;
   };

   template<class T>
   class Function : public UnaryExpr<T>
   {
      public:
         Function(std::unique_ptr<AbstractExpr<T>> aFunc, std::vector<std::unique_ptr<AbstractExpr<T>>>& aArgs, int aNArgs) :
            UnaryExpr<T>(std::move(aFunc))
         {
            for(In i = 0; i < aNArgs; ++i)
            {
               mArgs.emplace_back(std::move(aArgs.back()));
               aArgs.pop_back();
            }
         }
         T eval(const vector<T>& aVars) const
         {
            vector<T> real_args;
            for(size_t i = 0; i < mArgs.size(); ++i)
            {
               if(nullptr == mArgs[i]) MIDASERROR("Something is undefined in function");
               real_args.emplace_back(mArgs[i]->eval(aVars));
            }
            return UnaryExpr<T>::_expr->eval(real_args);
         }
         std::unique_ptr<AbstractExpr<T>> Copy() const
         {
            vector<std::unique_ptr<AbstractExpr<T>>> new_args;
            for(size_t i = 0; i < mArgs.size(); ++i)
            {
               if(nullptr == mArgs[i]) MIDASERROR("Something is undefined in function");
               new_args.emplace_back(mArgs[i]->Copy());
            }
            return std::make_unique<Function<T>>(UnaryExpr<T>::_expr->Copy(), new_args, new_args.size());
         }

         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Function<T>* ptr = dynamic_cast<const Function<T>* >(aPtr.get());
            if(ptr)
            {
               if(ptr->mArgs.size() != mArgs.size())
                  return false;
               if(!UnaryExpr<T>::_expr->Compare(ptr->_expr))
                  return false;
               for(size_t i = 0; i < mArgs.size(); ++i)
               {
                  if(!mArgs[i]->Compare(ptr->mArgs[i]))
                     return false;
               }
               return true;
            }
            return false;
         }

         std::string GenerateCppCode() const
         {
            //MIDASERROR("");
            return "";
         }
      private:
         vector<std::unique_ptr<AbstractExpr<T>>> mArgs;
   };

   template<class T>
   class Uminus : public UnaryExpr<T>{
      public:
         Uminus(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Uminus<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return -1*UnaryExpr<T>::_expr->eval(aVars);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Uminus<T>* ptr = dynamic_cast<const Uminus<T>* >(aPtr.get());
            if(ptr)
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "-(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Cos : public UnaryExpr<T>{
      public:
         Cos(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Cos<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return cos(UnaryExpr<T>::_expr->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Cos<T>* ptr = dynamic_cast<const Cos<T>* >(aPtr.get());
            if(ptr)
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::cos(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Sin : public UnaryExpr<T>{
      public:
         Sin(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Sin<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return sin(UnaryExpr<T>::_expr->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Sin<T>* ptr = dynamic_cast<const Sin<T>* >(aPtr.get());
            if(ptr)
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::sin(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Exp
      : public UnaryExpr<T>
   {
      public:
         Exp(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Exp<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return exp(UnaryExpr<T>::_expr->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Exp<T>* ptr = dynamic_cast<const Exp<T>* >(aPtr.get());
            if (ptr)
            {
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::exp(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Tan : public UnaryExpr<T>{
      public:
         Tan(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Tan<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return tan(UnaryExpr<T>::_expr->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Tan<T>* ptr = dynamic_cast<const Tan<T>* >(aPtr.get());
            if(ptr)
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::tan(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T,int Nvar,int Ndeg>
   class Tan<taylor<T, Nvar, Ndeg> > : public UnaryExpr<taylor<T, Nvar, Ndeg> >{
      public:
         Tan(std::unique_ptr<AbstractExpr<taylor<T, Nvar, Ndeg> > > e) : UnaryExpr<taylor<T, Nvar, Ndeg> >(std::move(e))
                  {MIDASERROR("We cannot use Tan for automatic differentiation atm.");}
         std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg> >> Copy() const
            {return std::make_unique<Tan<taylor<T,Nvar,Ndeg> > >(UnaryExpr<taylor<T,Nvar,Ndeg> >::_expr->Copy());}
         taylor<T,Nvar,Ndeg> eval(const vector<taylor<T, Nvar, Ndeg> >&) const {MIDASERROR("");return taylor<T,Nvar,Ndeg>(C_0);}
         bool Compare(const std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg> >>& aPtr) const
         {
            const Tan<taylor<T,Nvar,Ndeg> >* ptr = dynamic_cast<const Tan<taylor<T,Nvar,Ndeg> >* >(aPtr.get());
            if(ptr)
               return UnaryExpr<taylor<T, Nvar, Ndeg> >::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            MIDASERROR("");
            return "";
         }
   };

   template<class T>
   class Sqrt : public UnaryExpr<T>{
      public:
         Sqrt(std::unique_ptr<AbstractExpr<T>> e) : UnaryExpr<T>(std::move(e)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Sqrt<T>>(UnaryExpr<T>::_expr->Copy());}
         T eval(const vector<T>& aVars) const {return sqrt(UnaryExpr<T>::_expr->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Sqrt<T>* ptr = dynamic_cast<const Sqrt<T>* >(aPtr.get());
            if(ptr)
               return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::sqrt(" + this->_expr->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class BinaryExpr : public NonTerminalExpr<T>{
      protected:
         BinaryExpr(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) : _expr1(std::move(e1)), _expr2(std::move(e2)) {}
         virtual ~BinaryExpr () {}
         std::unique_ptr<AbstractExpr<T>> _expr1;
         std::unique_ptr<AbstractExpr<T>> _expr2;
   };

   template<class T>
   class Sum : public BinaryExpr<T>{
      public:
         Sum(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) :
            BinaryExpr<T>(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Sum<T>>(BinaryExpr<T>::_expr1->Copy(),BinaryExpr<T>::_expr2->Copy());}
         T eval(const vector<T>& aVars) const {return BinaryExpr<T>::_expr1->eval(aVars) + BinaryExpr<T>::_expr2->eval(aVars);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Sum<T>* ptr = dynamic_cast<const Sum<T>* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<T>::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<T>::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "(" + this->_expr1->GenerateCppCode() + " + " + this->_expr2->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Product : public BinaryExpr<T>{
      public:
         Product(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) :
            BinaryExpr<T>(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Product<T>>(BinaryExpr<T>::_expr1->Copy(),BinaryExpr<T>::_expr2->Copy());}
         T eval(const vector<T>& aVars) const {return BinaryExpr<T>::_expr1->eval(aVars) * BinaryExpr<T>::_expr2->eval(aVars);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Product<T>* ptr = dynamic_cast<const Product<T>* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<T>::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<T>::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "(" + this->_expr1->GenerateCppCode() + " * " + this->_expr2->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Diff : public BinaryExpr<T>{
      public:
         Diff(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) :
            BinaryExpr<T>(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Diff<T>>(BinaryExpr<T>::_expr1->Copy(),BinaryExpr<T>::_expr2->Copy());}
         T eval(const vector<T>& aVars) const {return BinaryExpr<T>::_expr1->eval(aVars) - BinaryExpr<T>::_expr2->eval(aVars);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Diff<T>* ptr = dynamic_cast<const Diff<T>* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<T>::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<T>::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "(" + this->_expr1->GenerateCppCode() + " - " + this->_expr2->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class Divide : public BinaryExpr<T>{
      public:
         Divide(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) :
            BinaryExpr<T>(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Divide<T>>(BinaryExpr<T>::_expr1->Copy(),BinaryExpr<T>::_expr2->Copy());}
         T eval(const vector<T>& aVars) const {
            T temp = BinaryExpr<T>::_expr2->eval(aVars);
            if(temp == 0)
            {
               MIDASERROR("Divide by zero");
               return -1; //Will never happen
            }
            else
               return BinaryExpr<T>::_expr1->eval(aVars) / temp;
         }
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Divide<T>* ptr = dynamic_cast<const Divide<T>* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<T>::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<T>::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "(" + this->_expr1->GenerateCppCode() + " / " + this->_expr2->GenerateCppCode() + ")";
         }
   };

   template<class T,int Nvar,int Ndeg>
   class Divide<taylor<T, Nvar, Ndeg> > : public BinaryExpr<taylor<T,Nvar,Ndeg> >{
      public:
         Divide(std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg>>> e1,
                std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg>>> e2) :
                  BinaryExpr<taylor<T,Nvar,Ndeg> >(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg>>> Copy() const
         {
            return std::make_unique<Divide<taylor<T,Nvar,Ndeg>>>
                (BinaryExpr<taylor<T,Nvar,Ndeg> >::_expr1->Copy(),
                  BinaryExpr<taylor<T,Nvar,Ndeg> >::_expr2->Copy());
         }
         taylor<T,Nvar,Ndeg> eval(const vector<taylor<T,Nvar,Ndeg> >& aVars) const {
            taylor<T,Nvar,Ndeg> temp = BinaryExpr<taylor<T,Nvar,Ndeg> >::_expr2->eval(aVars);
            if(temp[0] == 0)
            {
               MIDASERROR("Divide by zero");
               return taylor<T, Nvar, Ndeg>(-1); //Will never happen
            }
            else
               return BinaryExpr<taylor<T,Nvar,Ndeg> >::_expr1->eval(aVars) / temp;
         }
         bool Compare(const std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg> >>& aPtr) const
         {
            const Divide<taylor<T,Nvar,Ndeg> >* ptr = dynamic_cast<const Divide<taylor<T,Nvar,Ndeg> >* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            MIDASERROR("");
            return "";
         }
   };

   template<class T>
   class Raise : public BinaryExpr<T>{
      public:
         Raise(std::unique_ptr<AbstractExpr<T>> e1, std::unique_ptr<AbstractExpr<T>> e2) :
            BinaryExpr<T>(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const
            {return std::make_unique<Raise<T>>(BinaryExpr<T>::_expr1->Copy(),BinaryExpr<T>::_expr2->Copy());}
         T eval(const vector<T>& aVars) const {return pow(BinaryExpr<T>::_expr1->eval(aVars), BinaryExpr<T>::_expr2->eval(aVars));}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const Raise<T>* ptr = dynamic_cast<const Raise<T>* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<T>::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<T>::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::pow(" + this->_expr1->GenerateCppCode() + ", " + this->_expr2->GenerateCppCode() + ")";
         }
   };

   template<class T>
   class RaiseInt : public UnaryExpr<T>{
      private:
         In _pow;
      public:
         RaiseInt(std::unique_ptr<AbstractExpr<T>> e1, In pow) : UnaryExpr<T>(std::move(e1)), _pow(pow) {}
         std::unique_ptr<AbstractExpr<T>> Copy() const {return std::make_unique<RaiseInt<T>>(UnaryExpr<T>::_expr->Copy(), _pow);}
         T eval(const vector<T>& aVars) const {return pow(UnaryExpr<T>::_expr->eval(aVars), _pow);}
         bool Compare(const std::unique_ptr<AbstractExpr<T>>& aPtr) const
         {
            const RaiseInt<T>* ptr = dynamic_cast<const RaiseInt<T>* >(aPtr.get());
            if(ptr)
            {
               if(ptr->_pow == _pow)
                  return UnaryExpr<T>::_expr->Compare(ptr->_expr);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            return "std::pow(" + this->_expr->GenerateCppCode() + ", " + std::to_string(_pow) + ")";
         }
   };

   template<class T,int Nvar,int Ndeg>
   class Raise<taylor<T, Nvar, Ndeg> > : public BinaryExpr<taylor<T, Nvar, Ndeg> >{
      public:
         Raise(std::unique_ptr<AbstractExpr<taylor<T, Nvar, Ndeg>>> e1,
               std::unique_ptr<AbstractExpr<taylor<T, Nvar, Ndeg>>> e2) :
            BinaryExpr<taylor<T, Nvar, Ndeg> >(std::move(e1),std::move(e2)) {}
         std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg>>> Copy()
               const {return std::make_unique<Raise<taylor<T,Nvar,Ndeg>>>
                          (BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr1->Copy(),
                            BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr2->Copy());}
         taylor<T, Nvar, Ndeg> eval(const vector<taylor<T,Nvar,Ndeg> >& aVars) const
         {
            return exp(BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr2->eval(aVars) * log(BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr1->eval(aVars)) );
         }
         bool Compare(const std::unique_ptr<AbstractExpr<taylor<T,Nvar,Ndeg>>>& aPtr) const
         {
            const Raise<taylor<T, Nvar, Ndeg> >* ptr = dynamic_cast<const Raise<taylor<T, Nvar, Ndeg> >* >(aPtr.get());
            if(ptr)
            {
               if(BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr1->Compare(ptr->_expr1))
                  return BinaryExpr<taylor<T, Nvar, Ndeg> >::_expr2->Compare(ptr->_expr2);
            }
            return false;
         }
         std::string GenerateCppCode() const
         {
            MIDASERROR("");
            return "";
         }
   };
}

#endif //ONEMODEFUNCTIONGEN_H_INCLUDED
