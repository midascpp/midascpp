/**
************************************************************************
* 
* @file                MidasFunctionWrapper.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for handling functions defined as F(Q)-F(0)
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDASFUNCTIONWRAPPER_H_INCLUDED
#define MIDASFUNCTIONWRAPPER_H_INCLUDED

// std headers
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/ShuntingYard.h"
#include "util/generalfunctions/GenericFunctions.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "util/Io.h"

/**
 *
 **/
template<class T>
class MidasFunctionWrapper 
   : public GenericFunctionWrapper<T>
{
   private:

      //! Default constructor
      MidasFunctionWrapper();

   protected:
      
      //! Value at the reference coordinates
      mutable T mZeroVal;

   public:

      //! Constructor with evaluation of the value at reference coordinate for this operator
      MidasFunctionWrapper
         (  const std::string& aFunction
         ,  const FunctionContainer<T>& aFC
         ,  const bool& aKeoTerm = false
         ,  const std::vector<std::string>& aSetOfVars = std::vector<std::string>(I_1, "Q")
         ) 
         :  GenericFunctionWrapper<T>
            (  aFunction
            ,  aFC
            ,  aSetOfVars
            )
            ,  mZeroVal(C_0) 
      {
         // If the one-mode operator is part of the kinetic energy operator, then it does not need to comply with the bar-potential property and do not have any special requirement for at the reference
         if (aKeoTerm)
         {
            mZeroVal = C_0;
         }
         // If the one-mode operator is part of the potential energy operator, then it will have to comply with the bar-potential property
         else
         {
            this->mVariables[I_0] = C_0;
            mZeroVal = this->mPtr->eval(this->mVariables);
         }

         if (gDebug && gIoLevel > I_14)
         {
            Mout << " Operator " << aFunction << ", has reference value: " << mZeroVal << std::endl;
         }
      }

      //! Constructor with evaluation of the value at reference coordinate for this operator
      MidasFunctionWrapper
         (  PrimitiveExpressions::AbstractExpr<T>* aPtr
         ,  In aCnt = I_0
         ) 
         :  GenericFunctionWrapper<T>
            (  aPtr
            ,  aCnt
            )
      {
         this->mVariables[I_0] = C_0;
         mZeroVal = this->mPtr->eval(this->mVariables);
      }
               
      //! Default constructor
      MidasFunctionWrapper
         (  const MidasFunctionWrapper& aMFW
         ) 
         :  GenericFunctionWrapper<T>(aMFW)
         ,  mZeroVal(aMFW.mZeroVal) 
      {
      }
     
      //!
      MidasFunctionWrapper operator= 
         (  const MidasFunctionWrapper<T>& aRHS
         )
      {
         if (this != &aRHS)
         {
            this->mVariables = aRHS.mVariables;
            this->mpConstants = aRHS.mpConstants;
            this->mAritOperCount = aRHS.mAritOperCount;
            this->mFunction = aRHS.mFunction;
            mZeroVal = aRHS.mZeroVal;
            
            if (this->mPtr != NULL)
            {
               delete this->mPtr;
            }
            if (aRHS.mPtr != NULL)
            {
               this->mPtr = aRHS.mPtr->Copy(this->mVariables);
            }
            else
            {
               this->mPtr = NULL;
            }
         }
         return *this;
      }
      
      //!
      void ReZero
         (
         )  const
      {
         this->mVariables[I_0] = C_0;
         mZeroVal = this->mPtr->eval(this->mVariables);
      }
      
      /**
       *
       **/
      virtual T EvaluateFunc
         (
         ) const 
      { 
         return this->mPtr->eval(this->mVariables) - mZeroVal;
      }
      
      /**
       *
       **/
      virtual T EvaluateFuncForVar
         ( const T& aValue
         ) const
      {
         this->mVariables[I_0] = aValue;
         return this->mPtr->eval(this->mVariables) - mZeroVal;
      }
     
      /**
       * Evaluates the fit-basis functions, (operator terms) at a given coordinate 
       *
       * @param aValues    Container with the values for which the function should be evaluated, i.e. this contains the coordinate and any non-linear parameters
      **/
      virtual T EvaluateFuncForVarVec
         (  const std::vector<T>& aValues
         )  const
      {
         return this->mPtr->eval(aValues) - mZeroVal;
      }

      /**
       *
       **/
      virtual T EvaluateFunction
         ( const std::vector<T>& aValues
         ) const override
      {
         this->SetVariables(aValues);
         return this->mPtr->eval(this->mVariables) - mZeroVal;
      }     

      //!
      const std::vector<T>& GetmVariables() const {return this->mVariables;} 

      //! Wrapper for EvaluateFuncForVar. For having functor interface.
      virtual T operator()(const T& aValue) const override
      {
         return EvaluateFuncForVar(aValue);
      }
      
      /**
       *
       **/
      virtual ~MidasFunctionWrapper
         (
         ) 
      {
      }
};

#endif /* MIDASFUNCTIONWRAPPER_H_INCLUDED */
