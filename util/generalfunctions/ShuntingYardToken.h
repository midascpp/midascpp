#ifndef SHUNTINGYARDTOKEN_H_INCLUDED
#define SHUNTINGYARDTOKEN_H_INCLUDED

#include <ostream>
#include <memory>
#include "util/Io.h"

/**
 * Base class for all tokens to be handled by shunting yard algorithm.
 **/
template<class T>
class ShuntingYardToken
{
   protected:
      In mPrecedence;         //< Operator precedence of token.
      bool mLeftAssociation;  //< Is the token left-associative.
      string mDescription;    //< Description of token, i.e. what does it represent.
      In mNrOfArgs;           //< How many arguments does the token take.
      bool mIsFuncToken;      //< Is the token a function.

   public:
      //! Deleted default constructor
      ShuntingYardToken() = delete;

      //! Constructor
      ShuntingYardToken
         ( In aPrecedence
         , bool aLeftAssociation
         , const string& aDesc
         , In aNrOfArgs
         , bool aIsFuncToken
         )
         : mPrecedence(aPrecedence)
         , mLeftAssociation(aLeftAssociation)
         , mDescription(aDesc)
         , mNrOfArgs(aNrOfArgs)
         , mIsFuncToken(aIsFuncToken)
      {
      }

      //!
      In GetNrArgs() const {return mNrOfArgs;}
      In GetPrecedence() const {return mPrecedence;}
      bool GetLeftAssociative() const {return mLeftAssociation;}
      string GetDescription() const {return mDescription;}
      bool IsFuncToken() const {return mIsFuncToken;}

      //! Compare token against string.
      bool Compare(const string& aStr) const {return aStr == mDescription;}

      //! Get size of description string.
      In StringSize() const {return mDescription.size();}

      //! Check if given string is in token description.
      size_t InString(const string& aStr) const {return aStr.find(mDescription);}

      // Virtual destructor for base class.
      virtual ~ShuntingYardToken() {};

      //! This applies the function to the arguments given in the vector
      virtual std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>&) = 0;

      //! Copy
      virtual std::unique_ptr<ShuntingYardToken<T>> Copy() const = 0;
};

namespace ShuntingYardAritTokens
{
   //! Function token
   template<class T>
   class FunctionToken : public ShuntingYardToken<T>
   {
      private:
         std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> mFunc;
      public:
         FunctionToken() = delete;
         FunctionToken(In aNrArgs, std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> aFunc, const string& aName)
            : ShuntingYardToken<T>(5, false, aName, aNrArgs, true)
            , mFunc(std::move(aFunc))
         {
         }
         std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>& aArgs)
         {
            return std::make_unique<PrimitiveExpressions::Function<T>>(mFunc->Copy(), aArgs, this->mNrOfArgs);
         }
         // The funcion below is never supposed to be called, so will return a nullptr
         std::unique_ptr<ShuntingYardToken<T>> Copy() const  {return nullptr;}
   };

   template<class T, class S>
   class BinaryToken : public ShuntingYardToken<T>
   {
      public:
        BinaryToken(In aPrecedence, bool aLeftAssociation, const std::string& aDesc, In aNrArgs, bool aIsFuncToken) :
          ShuntingYardToken<T>(aPrecedence, aLeftAssociation, aDesc, aNrArgs, aIsFuncToken) {}
        std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>& aArgs)
        {
            std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> tmp =
                                  std::make_unique<S>(std::move(aArgs[aArgs.size()-2]), std::move(aArgs[aArgs.size()-1]));
            aArgs.pop_back();
            aArgs.pop_back();
            return tmp;
        }
        std::unique_ptr<ShuntingYardToken<T>> Copy() const
        {
          return std::make_unique<BinaryToken<T,S>>(ShuntingYardToken<T>::mPrecedence, ShuntingYardToken<T>::mLeftAssociation,
                                                    ShuntingYardToken<T>::mDescription, ShuntingYardToken<T>::mNrOfArgs,
                                                    ShuntingYardToken<T>::mIsFuncToken);
        }
   };

   template<class T, class S>
   class UnaryToken : public ShuntingYardToken<T>
   {
      public:
        UnaryToken(In aPrecedence, bool aLeftAssociation, const std::string& aDesc, In aNrArgs, bool aIsFuncToken) :
          ShuntingYardToken<T>(aPrecedence, aLeftAssociation, aDesc, aNrArgs, aIsFuncToken) {}
        std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>& aArgs)
        {
            std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> tmp = std::make_unique<S>(std::move(aArgs[aArgs.size()-1]));
            aArgs.pop_back();
            return tmp;
        }
        std::unique_ptr<ShuntingYardToken<T>> Copy() const
        {
          return std::make_unique<UnaryToken<T,S>>(ShuntingYardToken<T>::mPrecedence, ShuntingYardToken<T>::mLeftAssociation,
                                                   ShuntingYardToken<T>::mDescription, ShuntingYardToken<T>::mNrOfArgs,
                                                   ShuntingYardToken<T>::mIsFuncToken);
        }
   };

   //!
   template<class T>
   class RaiseToken : public ShuntingYardToken<T>
   {
      public:
         RaiseToken(In aPrecedence, bool aLeftAssociation, const std::string& aDesc, In aNrArgs, bool aIsFuncToken) :
          ShuntingYardToken<T>(aPrecedence, aLeftAssociation, aDesc, aNrArgs, aIsFuncToken) {}
         std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>& aArgs)
         {
            const PrimitiveExpressions::LiteralInt<T>* ptr = dynamic_cast<const PrimitiveExpressions::LiteralInt<T>* >(aArgs[aArgs.size()-1].get());
            if(ptr)
            {
              In tmp_val = ptr->_val;
              std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> tmp = std::make_unique<PrimitiveExpressions::RaiseInt<T>>(std::move(aArgs[aArgs.size()-2]), tmp_val);
              aArgs.pop_back();
              aArgs.pop_back();
              return tmp;
            }
            std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> tmp = std::make_unique<PrimitiveExpressions::Raise<T>>(std::move(aArgs[aArgs.size()-2]), std::move(aArgs[aArgs.size()-1]));
            aArgs.pop_back();
            aArgs.pop_back();
            return tmp;
         }
         std::unique_ptr<ShuntingYardToken<T>> Copy() const
         {
           return std::make_unique<RaiseToken<T>>(ShuntingYardToken<T>::mPrecedence, ShuntingYardToken<T>::mLeftAssociation,
                                                   ShuntingYardToken<T>::mDescription, ShuntingYardToken<T>::mNrOfArgs,
                                                   ShuntingYardToken<T>::mIsFuncToken);
         }
   };

   //!
   template<class T>
   class LeftParToken : public ShuntingYardToken<T>
   {
      public:
         LeftParToken(In aPrecedence, bool aLeftAssociation, const std::string& aDesc, In aNrArgs, bool aIsFuncToken) :
          ShuntingYardToken<T>(aPrecedence, aLeftAssociation, aDesc, aNrArgs, aIsFuncToken) {}
         std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GetFunction(vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>& aArgs)
         {
            //ERROR STATE
            return nullptr; //SHOULDN'T HAPPEN
         }
         std::unique_ptr<ShuntingYardToken<T>> Copy() const
         {
           return std::make_unique<LeftParToken<T>>(ShuntingYardToken<T>::mPrecedence, ShuntingYardToken<T>::mLeftAssociation,
                                                   ShuntingYardToken<T>::mDescription, ShuntingYardToken<T>::mNrOfArgs,
                                                   ShuntingYardToken<T>::mIsFuncToken);
         }
   };

} /* namespace ShuntingYardAritTokens */

#endif /* SHUNTINGYARDTOKEN_H_INCLUDED */
