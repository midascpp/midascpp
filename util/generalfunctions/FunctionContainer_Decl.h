#ifndef MIDAS_FUNCTIONCONTAINER_DECL_H
#define MIDAS_FUNCTIONCONTAINER_DECL_H

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <memory>

#include "util/generalfunctions/GenericFunctions.h"
#include "util/generalfunctions/ShuntingYard.h"
#include "util/Io.h"

template<class T>
class FunctionContainer
{
   private:
      //! Map of functions
      std::map<std::string, std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> > mFunctionMap;
      //! Constants
      std::vector<std::string> mConstants;
      //! Functions
      std::vector<std::pair<std::string, In> > mFunctions;

      //! Insert function into function map
      void Insert(const std::string& aName, const std::string& aFunction, const std::vector<std::string>& aVars = std::vector<std::string>());
   public:
      //! Default constructor
      FunctionContainer() = default;

      //! Delete copy assignment
      FunctionContainer<T> operator=(const FunctionContainer<T>&) = delete;

      //! Copy constructor
      FunctionContainer(const FunctionContainer<T>& aFC);

      //! Find function of given name
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FindFunction(const std::string& aName) const;

      //! Insert function with name
      void InsertFunction(const std::string& aName, const std::string& aFunction);

      //! Insert constant with name
      void InsertConstant(const std::string& aName, T aConst);

      //! Tries to find a constant which is currently at the start of the string and remove the constant from the string
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FindConstant(std::string& aStr, std::string& aConstName) const;

      //! Finds a function token in the first part of the string and removes it returning a copy of the function and aNrOfArgs
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> FindFunction(std::string& aStr, In& aNrOfArgs, std::string& aFuncName) const;

      //! Clear the function container
      void Clear();

      //! Finds the function name and the variables of the function...
      void FindVarsAndName(std::string& aFuncDef, std::vector<std::string>& aVars);

      //! Write functions as C++ code
      void WriteCppCode(std::ostream& arCodeStream) const;
};

#endif /* FUNCTIONCONTAINER_DECL_H */
