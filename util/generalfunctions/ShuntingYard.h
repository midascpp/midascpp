/**
************************************************************************
*
* @file                ShuntingYard.h
*
* Created:             11-04-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class responsible for creating the function
*                        expressions using the Shunting Yard Algorithm
*
*
* Last modified: Thu May 9, 2019  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SHUNTINGYARD_H
#define SHUNTINGYARD_H

#include <vector>
#include <string>
#include <sstream>
#include "util/generalfunctions/ShuntingYardToken.h"
#include "util/generalfunctions/GenericFunctions.h"
#include "util/conversions/FromString.h"
#include "inc_gen/TypeDefs.h"

template<class T>
class FunctionContainer;

template<class T>
class ShuntingYard
{
   private:
      std::vector<std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>>  mTreeInConstruction;
      std::vector<std::unique_ptr<ShuntingYardToken<T>>>    mOperStack;
      std::vector<std::string>                              mVariables;
      std::stringstream                                     mRevPolish;
      std::vector<std::unique_ptr<ShuntingYardToken<T>>>    mAritDefs;


      bool    CheckTopOfStack(ShuntingYardToken<T>* arOperator);
      void    ApplyTopOfStack();
      void    AddOper(std::unique_ptr<ShuntingYardToken<T>> arOperator);
      void    ReadNumber(std::string& aStr);
      void    PopOffTillLeftPar(const string& aFuncStr);
      void    HandleRightPar(const string& aFuncStr);
      std::unique_ptr<ShuntingYardToken<T>>                     FindOper( std::string& aStr, bool aCanBeUMin);
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>    CheckForVar(std::string& aStr);
   public:
      ShuntingYard(const ShuntingYard<T>&) = delete;
      ShuntingYard() = delete;
      ShuntingYard operator=(const ShuntingYard<T>&) = delete;

      ShuntingYard(const std::vector<std::string>& aVariables) : mVariables(aVariables)
      {
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::BinaryToken<T, PrimitiveExpressions::Diff<T>>>(2, true, "-", 2, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Uminus<T>>>(5, false, "U-", 1, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::BinaryToken<T, PrimitiveExpressions::Sum<T>>>(2, true, "+", 2, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::BinaryToken<T, PrimitiveExpressions::Product<T>>>(3, true, "*", 2, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::BinaryToken<T, PrimitiveExpressions::Divide<T>>>(3, true, "/", 2, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Sqrt<T>>>(5, false, "SQRT", 1, true));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Tan<T>>>(5, false, "TAN", 1, true));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Cos<T>>>(5, false, "COS", 1, true));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Sin<T>>>(5, false, "SIN", 1, true));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::UnaryToken<T, PrimitiveExpressions::Exp<T>>>(5, false, "EXP", 1, true));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::LeftParToken<T>>(100, false, "(", 0, false));
           mAritDefs.emplace_back(std::make_unique<ShuntingYardAritTokens::RaiseToken<T>>(4, false, "^", 2, false));
      }
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> GenerateExpression(const std::string& aExpr, const FunctionContainer<T>& aFunctions, In& aARitOperCnt);
      string GetRevPolish() const {return mRevPolish.str();}
};

#include "util/generalfunctions/FunctionContainer.h"

/**
 * Here we do all the work of turning the function string into an evaluation tree
 *
 * @param aExpr The function we want to turn into an evalutation tree
 *
 * @param aFunctions  A function container containing any functions that we might find in the function string
 *
 * @param aARitOperCnt Counts the number of aritmatic operators
 *
 * @return a unique_ptr to the root of the evaluation tree
 **/


template<class T>
std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> ShuntingYard<T>::GenerateExpression
   (  const std::string& aExpr
   ,  const FunctionContainer<T>& aFunctions
   ,  In& aARitOperCnt
   )
{
   mRevPolish.str("");
   mTreeInConstruction.clear();
   std::string for_work(aExpr);
   //Mout << " Running shunting yard for aExpr : " << aExpr << std::endl;
   bool can_be_uminus = true;
   string const_func_name;
   while (!for_work.empty())
   {
      //Mout << "The current string is : " << for_work << endl;
      //Mout << "The current oper stack is : " << mOperStack << endl;

      /**
       * Find out if the next part of the expression is a digit and push it onto the TreeInConstruction std::vector
      **/
      if (isdigit(for_work[I_0]))
      {
         //Mout << "Found number" << std::endl;
         ReadNumber(for_work);
         can_be_uminus = false;
         continue;
      }
      /**
       * Is the current part a constant, i.e. does it figure in the "constants" list of the .mop file
      **/
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> new_constant = aFunctions.FindConstant(for_work,const_func_name);
      if (nullptr != new_constant)
      {
         //Mout << " Found constant" << std::endl;
         can_be_uminus = false;
         mTreeInConstruction.emplace_back(std::move(new_constant));
         mRevPolish << const_func_name << ", ";
         continue;
      }
      /**
       * Is the current part a variable, i.e. a coordinate Q
      **/
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> new_variable = CheckForVar(for_work);
      if (nullptr != new_variable)
      {
         //Mout << " Found variable" << std::endl;
         can_be_uminus = false;
         mTreeInConstruction.emplace_back(std::move(new_variable));
         continue;
      }
      /**
       * Is the current part a function, then we need to figure out which function and how to treat it
      **/
      In nr_args = -I_1;
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> new_function = aFunctions.FindFunction(for_work, nr_args,const_func_name);
      if (nullptr != new_function)
      {
         //Mout << " Found function" << endl;
         can_be_uminus = false;
         AddOper(std::make_unique<ShuntingYardAritTokens::FunctionToken<T>>(nr_args, std::move(new_function), const_func_name));
         continue;
      }
      /**
       * If we have a seperator then we have to pop everything off till we reach a leftpar
      **/
      if(',' == for_work[I_0])
      {
         //Mout << "Found comma" << endl;
         can_be_uminus = true;
         PopOffTillLeftPar(aExpr);
         for_work.erase(0,1);
         continue;
      }
      /*
      *     Check if we have an operator and do stuff there
      */
      std::unique_ptr<ShuntingYardToken<T>> new_oper = FindOper(for_work,can_be_uminus);//This returns zero if we do not have an operator
      if(nullptr != new_oper)
      {
         //Mout << "Found operator" << endl;
         ++aARitOperCnt;
         can_be_uminus = true;
         AddOper(std::move(new_oper));
         continue;
      }
      /*
      *     If we have a right parentesis we just need to throw everything on the stack out till we reach a left parantesis
      */
      if(')' == for_work[I_0])
      {
         //Mout << "Found ) " << endl;
         HandleRightPar(aExpr);
         for_work.erase(0,1);
         continue;
      }
      //As all things above will do a continue we will never reach this point unless nothing can be done,
      //which means that we are stuck in an infinite loop
      MIDASERROR("I got stuck on deriving a reverse polish notion expression for : "+aExpr+" The current string is : "+for_work);
   }
   while(!mOperStack.empty() && !mOperStack.back()->Compare("("))
   {
      ApplyTopOfStack();
   }
   if(mTreeInConstruction.size() != I_1 || !mOperStack.empty())
   {
      if(!mOperStack.empty() && mOperStack.back()->Compare("("))
         MIDASERROR("Unmatched ( in expression : "+aExpr);
      else
      {
         Mout << "mTreeInConstruction.size() = " << mTreeInConstruction.size() << endl;
         Mout << "mOperStack : ";
         for(auto& it : mOperStack)
            Mout << it->GetDescription() << ", ";
         Mout << std::endl;
         MIDASERROR("Unknown error in expression : "+aExpr);
      }
   }
   return std::move(mTreeInConstruction[I_0]);
}

/**
 * If we find a right parentesis we have to first pop off everything untill we reach a left parentesis
 * then we remove the left parentesis and apply any function token standing in front of the left parentesis
 *
 * @param aFuncStr   A Reference to the function string needed to generate a meaningfull error
 **/
template<class T>
void ShuntingYard<T>::HandleRightPar(const string& aFuncStr)
{
   PopOffTillLeftPar(aFuncStr);
   mOperStack.pop_back(); //Deletes the left parantesis
   //Handle any function token
   if(!mOperStack.empty() && mOperStack.back()->IsFuncToken())
      ApplyTopOfStack();
}

/**
 * Check if we have a variable as the next part of the function, if so
 * remove the variable from the string and add it to mTreeInConstruction
 *
 * @param aStr  The string we are currently working on, we will remove
 *              the part of the string corresponding to the first number
 *              in this function.
 * @return either return a nullptr or a pointer a newly constructed variable
 **/

template<class T>
std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>
    ShuntingYard<T>::CheckForVar(std::string& aStr)
{
   for(size_t i = 0; i < mVariables.size(); ++i)
   {
      In curr_size = I_0;
      In match = -I_1;
      for(In i = I_0; i <mVariables.size(); ++i)
      {
         if(mVariables[i].size() > curr_size)
         {
            size_t poss = aStr.find(mVariables[i]);
            if(I_0 == poss)
            {
               match = i;
               curr_size = mVariables[i].size();
            }
         }
      }
      if(-I_1 == match)
         return nullptr;
      mRevPolish << aStr.substr(I_0,curr_size) << ", ";
      aStr.erase(I_0, curr_size);
      return std::make_unique<PrimitiveExpressions::Variable<T>>(match);
   }
   return nullptr;
}

/**
 * Read a number from the string we are working on and save it to the tree.
 *
 * @param aStr         The string we are currently working on, we will remove
 *                     the part of the string corresponding to the first number
 *                     in this function.
 *
 **/

template<class T>
void ShuntingYard<T>::ReadNumber(std::string& aStr)
{
   size_t end = aStr.find_first_not_of("0123456789.");
   if(end != std::string::npos)                // if its not the end of string
   {                                           // then
      if('E' == aStr[end] || 'e' == aStr[end]) // we might have scientific notation
      {
         end += 1;
         if('+' == aStr[end] || '-' == aStr[end]) // if we have scientific, exponent MUST have a sign
         {
            end += 1;
            end = aStr.find_first_not_of("0123456789",end);  // search for end of exponent
         }
         else
         {
            MIDASERROR("No sign for scientific when reading string: " + aStr);
         }
      }
   }

   std::string number(aStr.substr(I_0, end));
   size_t end_2 = number.find_first_not_of("0123456789");
   if(end_2 != std::string::npos)
   {
      Nb temp = midas::util::FromString<Nb>(number);
      mTreeInConstruction.emplace_back(std::make_unique<PrimitiveExpressions::Literal<T>>(T(temp)));
   }
   else
   {
      In temp = midas::util::FromString<In>(number);
      mTreeInConstruction.emplace_back(std::make_unique<PrimitiveExpressions::LiteralInt<T>>(temp));
   }
   mRevPolish << number << ", "; // add number to the rev polish string (useful for debugging)
   aStr.erase(I_0, end); // remove the characters we just read from string
}

/**
 * Apply the top of the operstack to the elements of mTreeInConstruction needed for the given operator
 **/

template<class T>
void ShuntingYard<T>::ApplyTopOfStack()
{
   //Mout << "Doing apply, size ofmTreeInConstruction : " << mTreeInConstruction.size() << endl;
   if(mTreeInConstruction.size() < mOperStack.back()->GetNrArgs())
   {
      MIDASERROR("Error, something wrong with expression tree..");
      //ERROR STATE
   }
   auto tmp = std::move(mOperStack.back()->GetFunction(mTreeInConstruction));
   mTreeInConstruction.emplace_back(std::move(tmp));
   //Mout << "Done constructing vector, size of mTreeInConstruction : " << mTreeInConstruction.size() << endl;
   mRevPolish << mOperStack.back()->GetDescription() << ", ";
   mOperStack.pop_back(); // Remove the top of the operator stack
}

/**
 * Add an operator to the operator stack, if necessary also pop operators
 * off the stack
 *
 * @param arOperator The operator we are going to add to the stack
 *
 **/

template<class T>
void ShuntingYard<T>::AddOper(std::unique_ptr<ShuntingYardToken<T>> arOperator)
{
   //Mout << "Adding operator " << arOperator << endl;
   while(!mOperStack.empty() && CheckTopOfStack(arOperator.get()))
   {
      //Mout << "Removing token" << endl;
      ApplyTopOfStack();
   }
   mOperStack.emplace_back(std::move(arOperator));
}


/**
* We have to chek if we have to pop the operator on top of the operator stack off the stack, this happens if:
* The top operator is NOT a left parentesis AND (
* arOperator is left associative and its precedence is equal to the operator on top of the stack OR
* arOperator has precedence less than the operator on top of the stack )
*
* Note 2019 BT: This takes a raw pointer, but it should be okay since it just does comparisons and NO! copies
* If it can be fixed to work with a unique_ptr, then it probably should
*
* @param arOperator  A raw pointer to the operator we are going to add
*
* @return Returns true if we have to pop the top element of the operator stack before adding the new element
*
**/
template<class T>
bool ShuntingYard<T>::CheckTopOfStack(ShuntingYardToken<T>* arOperator)
{
   //Mout << "Checking token " << arOperator << " against " << mOperStack.back() << endl;
   //Mout << "Precedence : " << arOperator->GetPrecedence() << " vs. " << mOperStack.back()->GetPrecedence() << endl;
   if (mOperStack.back()->Compare("("))
   {
      return false;
   }
   return ( ( arOperator->GetLeftAssociative()
            && arOperator->GetPrecedence() == mOperStack.back()->GetPrecedence()
            )
            || arOperator->GetPrecedence() < mOperStack.back()->GetPrecedence()
          );
}

/**
 * Pops off all operators on the stack untill the next (
 *
 * @param aFuncStr   A Reference to the function string needed to generate a meaningfull error
**/
template<class T>
void ShuntingYard<T>::PopOffTillLeftPar(const string& aFuncStr)
{
   //Mout << "Doing PopOff..." << endl;
   while(!mOperStack.empty() && !mOperStack.back()->Compare("("))
   {
      //Mout << "Running loop " << endl;
      ApplyTopOfStack();
   }
   //Mout << "Done with mOperStack.size : " << mOperStack.size() << endl;
   if(mOperStack.empty())
   {
      MIDASERROR("Unmatched ) in expression : "+aFuncStr);
   }
}

template<class T>
std::unique_ptr<ShuntingYardToken<T>> ShuntingYard<T>::FindOper
   ( std::string& aStr
   , bool aCanBeUMin
   )
{
   In curr_size = I_0;
   In match = -I_1;
   if('-' == aStr[I_0])
   {
      curr_size = I_1;
      if(aCanBeUMin)
         match = I_1;
      else
         match = I_0;
   }
   else
   {
      for(In i = 2; i < mAritDefs.size(); ++i)
      {
         if(mAritDefs[i]->StringSize() > curr_size)
         {
            size_t poss = mAritDefs[i]->InString(aStr);
            if(I_0 == poss)
            {
               match = i;
               curr_size = mAritDefs[i]->StringSize();
            }
         }
      }
   }
   if(-I_1 == match)
      return nullptr;
   aStr.erase(I_0, curr_size); // remove the processed part
   return mAritDefs[match]->Copy();
}

#endif //SHUNTINGYARD_H
