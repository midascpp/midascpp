/**
************************************************************************
* 
* @file                GenericFunctions.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Definitions of the primitive functions for the function wrappers
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SPLINEINTERFACES_H 
#define SPLINEINTERFACES_H 
#include <math.h>
#include <vector>
#include "util/Io.h"
#include "mathlib/Taylor/taylor.hpp"
#include "pes/splines/Spline1D.h"
#include "util/generalfunctions/GenericFunctions.h"
#include "mmv/MidasVector.h"

namespace PrimitiveExpressions{
   class QubicSpline : public UnaryExpr<Nb>
   {
      private:
         std::vector<MidasVector> mData;
      public:
         QubicSpline(const AbstractExpr<Nb>* e, const std::vector<MidasVector> aData) 
            : UnaryExpr<Nb>(e), mData(aData) {;}
         AbstractExpr<Nb>* Copy() const {return new QubicSpline(UnaryExpr::_expr->Copy(), mData);}
         bool Compare(const AbstractExpr<T>* aPtr) const
         {
            const QubicSpline<T>* ptr = dynamic_cast<const QubicSpline<T>*>(aPtr);
            if(ptr)
            {
               if(mData[I_0].Size() == ptr->mData[I_0].Size())
               {
                  for(In i = I_0; i < mData.size(); ++i)
                  {
                     if(mData[i] != ptr->mData[i])
                        return false;
                  }
                  return true;
               }
            }
            return false;
         }
         Nb eval(const vector<Nb>& aVars) const 
         {
            Nb temp;
            GetYog(temp, _expr->eval(aVars), mData[I_0], mData[I_1], mData[I_2]);
            return temp;
         }
   };

   class ModifiedShepard : public UnaryExpr<Nb>
   {
      private:
         std::vector<MidasVector> mData;
      public:
         ModifiedShepard(const AbstractExpr<Nb>* e, const std::vector<MidasVector> aData) : UnaryExpr(e), mData(aData) {;}
         AbstractExpr<Nb>* Copy() const {return new ModifiedShepard(UnaryExpr::_expr->Copy(), mData);}
         bool Compare(const AbstractExpr<T>* aPtr) const
         {
            const ModifiedShepard<T>* ptr = dynamic_cast<const ModifiedShepard<T>*>(aPtr);
            if(ptr)
            {
               if(mData[I_0].Size() == ptr->mData[I_0].Size())
               {
                  for(In i = I_0; i < mData.size(); ++i)
                  {
                     if(mData[i] != ptr->mData[i])
                        return false;
                  }
                  return true;
               }
            }
            return false;
         }
         Nb eval(const vector<Nb>& aVars) const
         {
            Nb temp;
            //Calc val
            return temp;
         }
   };
}
#endif //SPLINEINTERFACES_H
