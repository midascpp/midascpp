/**
************************************************************************
*
* @file                GenericFunctionWrapper.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for wrapping a function of a single variable,
                     complete with memory dealocation and all
*
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#ifndef GENERICFUNCTIONWRAPPER_H_INCLUDED
#define GENERICFUNCTIONWRAPPER_H_INCLUDED

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/GenericFunctions.h"
#include "util/generalfunctions/ShuntingYard.h"
#include "util/Io.h"

/**
 *
 **/
template<class T>
class GenericFunctionWrapper
{
   private:
      GenericFunctionWrapper();
   protected:
      std::vector<std::string>                  mVariableNames;
      mutable std::vector<T>                    mVariables; //This vector exists to avoid realocation each time the function is used.
                                                            //It is muteable since the evaluation can change it, but we don't really
                                                            //care since this is just here for efficiency reasons
      std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>>    mPtr;
      In                                                        mAritOperCount;
      std::string mFuncStr;

      //!
      void SetFunction
         (  const FunctionContainer<T>& aFC
         ,  const std::string& aFuncStr
         )
      {
         ShuntingYard<T> shunting_yard(mVariableNames);
         mPtr = std::move(shunting_yard.GenerateExpression(aFuncStr, aFC, mAritOperCount));
         //Mout << "GetRevPolish: " << shunting_yard.GetRevPolish() << std::endl;
      }

   public:

      //!
      virtual ~GenericFunctionWrapper() {}

      //!
      GenericFunctionWrapper
         (  const std::string& aFunction
         ,  const FunctionContainer<T>& aFC
         ,  const std::vector<std::string>& aSetOfVars = std::vector<std::string>(1, "Q")
         )
         :  mVariableNames(aSetOfVars)
         ,  mVariables
            (  aSetOfVars.size()
            ,  T(C_0)
            )
         ,  mPtr(nullptr)
         ,  mAritOperCount(I_0)
         ,  mFuncStr(aFunction)
      {
         SetFunction(aFC, aFunction);
         //Mout << "aFunction is at this point set to: " << aFunction << std::endl;
      }

      //!
      GenericFunctionWrapper
         (  std::unique_ptr<PrimitiveExpressions::AbstractExpr<T>> aPtr
         ,  In aCnt = I_0
         ,  const std::vector<std::string>& aSetOfVars = std::vector<std::string>(1, "Q")
         )
         :  mVariableNames(aSetOfVars)
         ,  mVariables
            (  aSetOfVars.size()
            ,  C_0
            )
         ,  mPtr(std::move(aPtr))
         , mAritOperCount(aCnt)
      {
      }

      //!
      GenericFunctionWrapper
         (  const GenericFunctionWrapper<T>& aFW
         )
         :  mVariableNames(aFW.mVariableNames)
         ,  mVariables(aFW.mVariables)
         ,  mAritOperCount(aFW.mAritOperCount)
      {
         if (aFW.mPtr != NULL)
         {
            mPtr = aFW.mPtr->Copy();
         }
         else
         {
            mPtr = nullptr;
         }
      }

      //!
      GenericFunctionWrapper operator= (const GenericFunctionWrapper<T>& aRHS)
      {
         if (this != &aRHS)
         {
            mVariableNames = aRHS.mVariableNames;
            mVariables = aRHS.mVariables;
            mAritOperCount = aRHS.mAritOperCount;
            if (aRHS.mPtr != nullptr)
            {
               mPtr = aRHS.mPtr->Copy(mVariables);
            }
            else
            {
               mPtr = nullptr;
            }
         }
         return *this;
      }

      //!
      In GetAritOperCount() const
      {
         return mAritOperCount;
      }

      //!
      void SetVariableI(const T& aValue, In aIn) const
      {
         //Mout << "Evaluating function for : " << aIn << " aka " << mVariableNames[aIn] << " with val : " << aValue << std::endl;
         mVariables[aIn] = aValue;
      }

      //!
      void SetVariables(const std::vector<T>& aValues) const
      {
         if (aValues.size() != mVariables.size())
         {
            MIDASERROR("Something wrong in setting variables for function");
         }
         for (In i = I_0; i < mVariables.size(); ++i)
         {
            mVariables[i] = aValues[i];
         }
      }

      //!
      T EvaluateFunc() const
      {
         return mPtr->eval(mVariables);
      }

      //!
      virtual T EvaluateFunction(const std::vector<T>& aValues) const
      {
         SetVariables(aValues);
         return mPtr->eval(mVariables);
      }

      virtual T EvaluateFunctionVec(const std::vector<T>& aValues) const
      {
         return mPtr->eval(aValues);
      }

      //! Puts arg. in size-1 vector and calls EvaluateFunction. For having functor interface.
      virtual T operator()(const T& aValue) const
      {
         return EvaluateFunction(std::vector<T>{aValue});
      }

      //!
      const std::string& FuncStr() const {return mFuncStr;}

      //!
      const std::string GenerateCppCode() const
      {
         return mPtr->GenerateCppCode();
      }

      //! Returns whether the underlying wrapped functions compare equal.
      bool Compare(const GenericFunctionWrapper<T>& arOther) const
      {
         return mPtr->Compare(arOther.mPtr);
      }
};

#endif //GENERICFUNCTIONWRAPPER_H_INCLUDED
