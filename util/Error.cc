#include "Error.h"

#include <thread>
#include <mutex>
#include <chrono>
#include <ctime>

#include "util/Io.h"
#include "util/Os.h"
#include "mpi/Interface.h"

// Create two mutex objects to control writes on cout and Mout.
std::mutex cout_mutex;
std::mutex mout_mutex;

enum class ErrorBehaviour : int { KILL, NUMBER };

// Some "Error module" parameters
ErrorBehaviour error_behaviour = ErrorBehaviour::KILL;
int            error_number    = 0;
bool           error_write_stdout = true;
bool           error_write_mout   = true;

// Struct for handling an error label and message.
struct error_info
{
   std::string label;
   std::string message;

   error_info(const std::string& l, const std::string& m)
      :  label(l)
      ,  message(m)
   {
   }
};

// Object binding together an error_info vector and a mutex for accessing it.
struct 
{
   std::vector<error_info> vec;
   std::mutex              mut;
} error_info_handler;

/**
 * Function for adding information to be printed when encountering a MIDASERROR.
 *
 * @param label
 *    The message label, i.e. what gets printed between the brackets [].
 * @param message
 *    The actual info message.
 **/
void AddErrorInfo
   (  const std::string& label
   ,  const std::string& message
   )
{
   // Lock the mutex, so only a single thread can change error_info_handler at a time.
   std::lock_guard<std::mutex> lock(error_info_handler.mut);

   // Push back error info.
   error_info_handler.vec.emplace_back(label, message);
}

/**
 * Print everything that was added to error info.
 *
 * @param aOs
 *    The output stream we are printing to.
 **/
void PrintErrorInfo
   (  std::ostream& aOs
   )
{
   // Lock the mutex, so only a single thread can use error_info_handler at a time.
   std::lock_guard<std::mutex> lock(error_info_handler.mut);

   // Print the information
   for(const auto& info: error_info_handler.vec)
   {
      std::stringstream sstr;
      sstr << std::left << "[" << std::setw(10) << info.label << "] ";

      midas::stream::HeaderInserter os_buf(aOs.rdbuf(), sstr.str(), true);
      std::ostream os(&os_buf);

      os << info.message << "\n";
   }
}

/**
 * Print Midas error message. If available will print filename, 
 * line number and return address for where the error was thrown.
 *
 * @param aOs           The stream to which to print the error message.
 * @param aError        Short message explaining the error.
 * @param aFilename     The file where the error was thrown.
 * @param aLinenumber   The line number where the error was thrown.
 * @param aReturnAddr   The return address for the function where the error was thrown.
 * @param aErrorTime    The time/date the error was thrown.
 **/
void PrintError
   (  std::ostream& aOs
   ,  std::string aError
   ,  const char* aFilename
   ,  unsigned aLinenumber
   ,  void* aReturnAddr
   ,  const std::time_t& aErrorTime
   )
{
   // Print header
   aOs << "*** MIDAS ERROR *** *** MIDAS ERROR *** *** MIDAS ERROR ***\n";

   // Print timestamp
   midas::stream::HeaderInserter date_buf(aOs.rdbuf(),     "[DATE      ] ",true);
   std::ostream date(&date_buf);
   date << std::ctime(&aErrorTime) << "\n";
   
   // Print MPI/Threading stuff
   midas::stream::HeaderInserter rank_buf(aOs.rdbuf(),     "[MPI RANK  ] ",true);
   std::ostream rank(&rank_buf);
   rank << "MPI Rank        : " << midas::mpi::get_Interface().GetMpiGlobalRank() << "\n"
        << "Running on host : " << midas::os::Gethostname() << "\n"
        << "Thread ID       : " << std::this_thread::get_id() << "\n";
   
   // Print error message
   midas::stream::HeaderInserter message_buf(aOs.rdbuf(),  "[MESSAGE   ] ",true);
   std::ostream message(&message_buf);
   message << aError << "\n";
   
   // Print CWD
   midas::stream::HeaderInserter cwd_buf(aOs.rdbuf(),  "[CWD       ] ",true);
   std::ostream cwd(&cwd_buf);
   cwd << midas::os::Getcwd() << "\n";

   // Print some information on where in source
   if(aFilename)
   {
      aOs << "[FILE      ] " << aFilename << " " << aLinenumber << "\n";
   }

   if(aReturnAddr)
   {
      midas::stream::HeaderInserter return_buf(aOs.rdbuf(),"[RETURNADDR] ",true);
      std::ostream return_str(&return_buf);
      libmda::util::output_call_addr(return_str,aReturnAddr,'\n');
   }

   // Print dynamic error information
   PrintErrorInfo(aOs);
   
   // Print any warning thrown prior to error
   midas::stream::HeaderInserter warning_buf(aOs.rdbuf(),  "[WARNING   ] ",true);
   std::ostream warning(&warning_buf);
   PrintMidasWarnings(warning);

   // Print stacktrace
   midas::stream::HeaderInserter stacktrace_buf(aOs.rdbuf(),"[STACKTRACE] ",true);
   std::ostream stacktrace_stream(&stacktrace_buf);
   libmda::util::print_stacktrace(stacktrace_stream);
   
   // Print footer
   aOs << "*** *********** *** ** PROGRAM EXITS ** *** *********** ***" << std::endl;
}

/**
 * Guard PrintError with a mutex lock, such that only 1 thread can enter PrintError at a time.
 * Should make a little nicer output.
 **/
void GuardedPrintError
   (  std::ostream& aOs
   ,  std::string aError
   ,  const char* aFilename
   ,  unsigned aLinenumber
   ,  void* aReturnAddr
   ,  const std::time_t& aErrorTime
   ,  std::mutex& aMutex
   )
{
   std::lock_guard<std::mutex> lock(aMutex);
   PrintError(aOs, aError, aFilename, aLinenumber, aReturnAddr, aErrorTime);
}

/**
 * Print Midas error message and terminate execution of the program.
 * First unmutes Mout so we are sure error message will be printed,
 * then prints the error message. In the end terminate the program with exit
 * or calling MPI_ABORT if it is an MPI run
 *
 * @param aError
 * @param aFilename
 * @param aLinenumber
 * @param aReturnAddr
 * @param aErrorTime    The time/date the error was thrown.
 **/
void Error
   (  std::string aError
   ,  const char* aFilename
   ,  unsigned int aLinenumber
   ,  void* aReturnAddr
   ,  const std::time_t& aErrorTime
   ) 
{
   // unmmute if Mout is muted
   Mout.Unmute(); 
   
   // then print error message
   if (error_write_mout) 
   {
      GuardedPrintError(Mout     , aError, aFilename, aLinenumber, aReturnAddr, aErrorTime, mout_mutex);
   }

   if (error_write_stdout) 
   {
      GuardedPrintError(std::cout, aError, aFilename, aLinenumber, aReturnAddr, aErrorTime, cout_mutex);
   }

   // Exit the process
   if(error_behaviour == ErrorBehaviour::NUMBER)
   {
      error_number = -1;
   }
   else
   {
   #ifdef VAR_MPI
      //This is to kill all MPI processes, since this will not happen when exit is called...
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
      exit(1);
   #else
      // if not MPI we just exit
      exit(1);
   #endif /* VAR_MPI */
   }
}

/**
 * Set error flag for how to deal with errors or reset error number.
 **/
void ErrorControl(ErrorCtrl flag)
{
   switch(flag)
   {
      case ErrorCtrl::SETKILL:
         error_behaviour = ErrorBehaviour::KILL;
         break;
      case ErrorCtrl::SETNUMBER:
         error_behaviour = ErrorBehaviour::NUMBER;
         break;
      case ErrorCtrl::RESETNUMBER:
         error_number = 0;
         break;
      case ErrorCtrl::MUTESTDOUT:
         error_write_stdout = false;
         break;
      case ErrorCtrl::UNMUTESTDOUT:
         error_write_stdout = true;
         break;
      case ErrorCtrl::MUTEMOUT:
         error_write_mout = false;
         break;
      case ErrorCtrl::UNMUTEMOUT:
         error_write_mout = true;
         break;
   }
}

/**
 * Get last error number.
 **/
int ErrorNumber()
{
   return error_number;
}
