/**
************************************************************************
* 
* @file                FileHandler.cc
*
* Created:             10-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Classes for handling writing to .mbar, .mop and .plot files
* 
* Last modified: Thu Oct 10, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

// std headers
#include<fstream>
#include<string>

// midas headers
#include "util/FileHandler.h"
#include "util/Io.h"

// using declarations
using namespace FileHandles;

FileHandler::FileHandler() 
   : mListOfFiles(NULL)
   , mNoOfFiles(I_0)
   , mSpecOfStream(-I_1)
   , mToAll(true)
   , mToOne(false)
{
}

FileHandler::~FileHandler()
{
   Close();
}

void FileHandler::OpenFiles(string aStartOfFileName, In aNoOfFiles, string aEndOfFileName)
{
   //If we already have some files we remove them and get some new ones
   if(mNoOfFiles != I_0)
   {
      for(In i = I_0; i < mNoOfFiles; i++)
         mListOfFiles[i].close();
      delete[] mListOfFiles;
   }
      
   mNoOfFiles = aNoOfFiles;
   mListOfFiles = new ofstream[aNoOfFiles];
   for(In i = I_0; i < aNoOfFiles; i++)
   {
      string FileName = aStartOfFileName + std::to_string(i+I_1) + aEndOfFileName;
      mListOfFiles[i].open(FileName.c_str());
      mListOfFiles[i].setf(ios::scientific);
      mListOfFiles[i].setf(ios::uppercase);
      mListOfFiles[i].precision(I_22);
   }
}

void FileHandler::OpenFile(string aFileName)
{
   mNoOfFiles = I_1;
   mListOfFiles = new ofstream[I_1];
   mListOfFiles[I_0].open(aFileName.c_str());
   mListOfFiles[I_0].setf(ios::scientific);
   mListOfFiles[I_0].setf(ios::uppercase);
   mListOfFiles[I_0].precision(I_22);
}

void FileHandler::Close()
{
   if(mNoOfFiles > I_0)
   {
      for(In i = I_0; i < mNoOfFiles; i++)
         mListOfFiles[i].close();
      mNoOfFiles = I_0;
      delete[] mListOfFiles;
   }
}

void FileHandler::Width(In aIn)
{
   for(In i = I_0; i < mNoOfFiles; i++)
      mListOfFiles[i].width(aIn);
}

FileHandler& FileHandler::operator<<(MidasVector aMVector)
{
   if(mToAll)
      for(In i = I_0; i < mNoOfFiles; i++)
         mListOfFiles[i] << aMVector;
   else if(mToOne)
      mListOfFiles[mSpecOfStream] << aMVector;
   else
      if(aMVector.Size() != mNoOfFiles)
         MIDASERROR("Mismatch in size in output to dist in FileHandler");
      else
         for(In i = I_0; i < aMVector.Size(); i++)
            mListOfFiles[i] << aMVector[i] << "  ";
   
   return *this;
}
