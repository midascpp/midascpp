/**
************************************************************************
* 
* @file                FileHandler.cc
*
* Created:             10-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*                      Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Handler class for .mbar files
* 
* Last modified:       20-04-2018
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
**/

// std headers
#include <map>
#include <list>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <stdarg.h>

// midas headers
#include "util/BarInterface.h"

// using declarations
using namespace BarHandles;

/**
 *
**/
BarFileInterface::BarFileInterface
   (  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   )
   :  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo) 
   ,  mSaveDir(mPesInfo.GetMultiLevelInfo().SaveIoDir())
   ,  mBarFileStorageType(mPesCalcDef.GetmPesBarFileStorageType())
{
   // Check that the storage type is correct
   if (mBarFileStorageType != "ONFILE" && mBarFileStorageType != "INMEMORY")
   {  
      MIDASERROR("The storage type for bar file information under the fitting/interpolation routines is not set correct!");
   }
}

/**
 *
**/
BarFileInterface::~BarFileInterface()
{
}

/**
 *
**/
void BarFileInterface::Initialize()
{
   if (gPesIoLevel > I_9)
   {
      Mout << " Storage type for bar-file information is set to: " << mBarFileStorageType << std::endl;
   }

   // The bar files are read per "mode combination block" by using information obtained from the prop_bar_files_navigation.mpesinfo file, i.e. this file works as an "interface" to the bar files
   GetOffsetsForInterface();

   // Do nothing, we get the data straight from the files at each mode combination instance
   if (mBarFileStorageType == "ONFILE")
   {
   }

   // Read all available information now and store them in memory for later use
   if (mBarFileStorageType == "INMEMORY")
   {
      ReadBarFileDataIntoMem();
   }
}

/**
 * Get the offsets which interface to the reading of bar files 
**/
void BarFileInterface::GetOffsetsForInterface()
{
   std::string bar_files_offsets_filename = mSaveDir + "/" + "prop_bar_files_navigation.mpesinfo";
   if (!InquireFile(bar_files_offsets_filename))
   {
      MIDASERROR(" I was unable to find to find the interface file to the .mbar files, (prop_bar_files_navigation.mpesinfo) in the DoInterpFit function!");
   }
   ifstream BarInterface(bar_files_offsets_filename.c_str(), ios_base::in);

   std::string s;
   std::getline(BarInterface, s); // Skip the header of prop_bar_files_navigation.mpesinfo file

   while (std::getline(BarInterface, s))
   {
      mBarFilesOffsets.push_back(midas::util::TupleFromString<std::string, In, In>(s));
   }
}

/**
 * Determine which part of the .mbar file pertains to this particular MC. If we use some kind of screening we can no longer be sure of the mode combination ordering, so we need to check for the right element explicitly.
 *
 * @param aModeCombi
 * @param aReadFromLine
 * @param aReadToLine
**/
void BarFileInterface::GetOffsetsForMc
   (  const ModeCombi& aModeCombi
   ,  In& aReadFromLine
   ,  In& aReadToLine
   )  const
{
   if (mPesCalcDef.GetmDoAdgaPreScreen() || mPesCalcDef.GetmPesScreenModeCombi())
   {
      std::string curr_non_screened_mc = "(";
      for (In i = I_0; i < aModeCombi.Size(); ++i)
      {
         curr_non_screened_mc += mPesInfo.GetMolecule().GetModeLabel(aModeCombi.Mode(i));
         if (aModeCombi.Size() > I_1 && i != aModeCombi.Size() - I_1)
         {
            curr_non_screened_mc += ",";
         }
      }
      curr_non_screened_mc += ")";

      for (In i = I_0; i < mBarFilesOffsets.size(); i++)
      {
         if (std::get<I_0>(mBarFilesOffsets[i]) == curr_non_screened_mc)
         {
            aReadFromLine = std::get<I_1>(mBarFilesOffsets[i]);
            aReadToLine = std::get<I_2>(mBarFilesOffsets[i]);
         }
      }
   }
   else
   {
      aReadFromLine = std::get<I_1>(mBarFilesOffsets[aModeCombi.Address() - I_1]);
      aReadToLine = std::get<I_2>(mBarFilesOffsets[aModeCombi.Address() - I_1]);
   }

   if (gDebug || gPesIoLevel > I_8)
   {
      Mout << " Offsets used for interfacing with bar-files are read from the prop_bar_files_navigation.mpesinfo file as follows: " << std::endl << "  Read from line " << aReadFromLine  << std::endl << "  Read to line   " << aReadToLine << std::endl;
   }

   if (aReadFromLine > aReadToLine)
   {
      MIDASERROR("The lines read from the bar-file interface file (prop_bar_files_navigation.mpesinfo) do not make any sense!");
   }
}

/**
 * Get single point data pertaining to a specific mode combination 
 *
 * @param aModeCombi
 * @param aBarNumber
 * @param aInputPoints
 * @param aBarFileCoords
 * @param aBarFileCoordsUncompressed
 * @param aBarFileProperty
 * @param aBarFileDerivatives
 * @param aAvailableDerivatives
 * @param aDoMsi
**/
void BarFileInterface::GetSinglePointBarDataForMc
   (  const ModeCombi& aModeCombi
   ,  const In& aBarNumber
   ,  In& aInputPoints
   ,  std::vector<MidasVector>& aBarFileCoords
   ,  std::vector<MidasVector>& aBarFileCoordsUncompressed
   ,  MidasVector& aBarFileProperty
   ,  std::vector<MidasVector>& aBarFileDerivatives
   ,  const bool& aAvailableDerivatives
   ,  const bool& aDoMsi
   )  const
{
   // The mode combination size is a frequently used quantity in the following
   In mode_combi_size = aModeCombi.Size();
   
   // Get the correct bar file interface, i.e. the offsets for each mode combination
   In read_from_line = I_0;
   In read_to_line = I_0;
   GetOffsetsForMc(aModeCombi, read_from_line, read_to_line);

   // Read bar-file data for the specific mode-combination
   std::vector<std::vector<Nb>> bar_file_data;
   bar_file_data.assign(read_to_line - read_from_line + I_1, std::vector<Nb>());
   if (mBarFileStorageType == "ONFILE")
   {
      // Obtain the correct bar file
      std::string bar_file_name = mSaveDir + "/" + "prop_no_" + std::to_string(aBarNumber + I_1) + ".mbar";
 
      if (!InquireFile(bar_file_name))
      {
         MIDASERROR("I was unable to find the correct .mbar file, (DoInterpFit function): " + bar_file_name);
      }
 
      // Open the bar file
      ifstream bar_file(bar_file_name.c_str(), ios_base::in);
      if (!bar_file)
      {
         MIDASERROR(" I was unable to open the correct .mbar file, (DoInterpFit function): " + bar_file_name);
      }
 
      // Read the correct block of data in the bar file, (implicitly linked to a specific MC)
      std::string s;
      // First we need to navigate to the correct position in the bar-file
      for (In i_line = I_1; i_line < read_from_line; ++i_line)
      {
         std::getline(bar_file, s);
      }
      // Then we can begin reading the information that we need
      In bar_file_data_index = I_0;
      for (In j_line = read_from_line - I_1; j_line < read_to_line; ++j_line)
      {
         std::getline(bar_file, s);
         bar_file_data[bar_file_data_index] = midas::util::VectorFromString<Nb>(s);
         ++bar_file_data_index;
      }
   
      // Close the bar file again
      bar_file.close();
   }
   if (mBarFileStorageType == "INMEMORY")
   {
      In bar_file_data_index = I_0;
      for (In i_line = read_from_line - I_1; i_line < read_to_line; ++i_line)
      {
         bar_file_data[bar_file_data_index] = mBarFileData[aBarNumber][i_line];
         ++bar_file_data_index;
      }
   }

   if (gDebug)
   {
      Mout << std::endl;
      Mout << " Now using property data from (bar-)file: prop_no_" << aBarNumber + I_1 << ".mbar " << std::endl << std::endl;
      Mout << bar_file_data << std::endl; 
   }

   aBarFileCoords.assign(mode_combi_size, MidasVector());
   for (In bar_file_line = I_0; bar_file_line < bar_file_data.size(); bar_file_line++)
   {  
      for (In bar_file_col = I_0; bar_file_col < mode_combi_size; bar_file_col++)
      {
         aBarFileCoords[bar_file_col].SetNewSize(bar_file_data.size());
         aBarFileCoords[bar_file_col][bar_file_line] = bar_file_data[bar_file_line][bar_file_col];
      }
      aInputPoints += I_1;   
   }
   
   // If modified Shepard interpolation is requested then we additionally need to have the uncompressed data available 
   if (aDoMsi)
   {
      aBarFileCoordsUncompressed = aBarFileCoords; 
   }

   // There will be a number of duplicate coordinate entries in the bar files for higher order mode combination levels, which we need to sort out before going any further
   if (mode_combi_size > I_1)
   {
      for (In bar_file_col = I_0; bar_file_col < mode_combi_size; bar_file_col++)
      {
         // Access all elements which pertains to a specific coordinate
         std::list<Nb> ls_coord_tmp(aBarFileCoords[bar_file_col].begin(), aBarFileCoords[bar_file_col].end());
      
         // Sort the data into groups and remove duplicate entries
         ls_coord_tmp.sort();
         ls_coord_tmp.unique();
      
         In indx_cnt = I_0;
         MidasVector vec_coord_tmp(ls_coord_tmp.size());
         for (std::list<Nb>::const_iterator indx = ls_coord_tmp.begin(); indx != ls_coord_tmp.end(); indx++)
         {
            vec_coord_tmp[indx_cnt] = *indx;
            indx_cnt++;
         }

         // aBarFileCoords now contains the compressed data, i.e. all the sorted and unique coordinates
         aBarFileCoords[bar_file_col].SetNewSize(ls_coord_tmp.size());
         aBarFileCoords[bar_file_col] = vec_coord_tmp;
      }
   }
   
   // Second we seperate out the energy/property, (first column after the coordinates)
   aBarFileProperty.SetNewSize(bar_file_data.size());
   for (In bar_file_line = I_0; bar_file_line < bar_file_data.size(); bar_file_line++)
   {  
      aBarFileProperty[bar_file_line] = bar_file_data[bar_file_line][mode_combi_size];
   }

   // Third we retrieve the derivatives for use in the Shepard interpolation if needed, these are situated in a column that comes after the energy/property 
   if (aDoMsi && aAvailableDerivatives)
   {
      aBarFileDerivatives.resize(I_2);
      In n_derivs = aBarFileProperty.Size();
     
      if (gPesIoLevel > I_8 || gDebug)
      {
         Mout << " Nr. of derivatives for Shepard interpolation: " << n_derivs << std::endl;
      }
   
      // Note that the aBarFileDerivatives container holds the energy/property as its first element, as this is needed by the modified Shepard interpolation, while the remaining elements contain the actual derivatives
      for (In bar_file_line = I_0; bar_file_line < bar_file_data.size(); bar_file_line++)
      {
         for (In bar_file_col = I_0; bar_file_col < I_2; bar_file_col++)
         {
            aBarFileDerivatives[bar_file_col].SetNewSize(n_derivs);
            aBarFileDerivatives[bar_file_col][bar_file_line] = bar_file_data[bar_file_line][mode_combi_size + bar_file_col + I_1];
         }
      }
           
      if (gDebug || gPesIoLevel > I_14)
      {
         Mout << " SETTING SHEPARD DERIVS : " << std::endl << aBarFileDerivatives << std::endl;
      }
   }
}

/**
 * Get variance pertaining to a specific mode combination 
 *
 * @param aModeCombi
 * @param aBarNumber
 * @param aInputPoints
 * @param aVariances contains on exist the variances of the ML Potential if aDoMlPes is true
**/
void BarFileInterface::GetVarianceForMc
   (   const ModeCombi& aModeCombi
   ,   const In& aBarNumber
   ,   In& aInputPoints
   ,   std::vector<Nb>& aVariances   
   ) const
{
   aVariances.clear();
   aVariances.reserve(aInputPoints);

   std::string file_gpr_variance =  mSaveDir  + "/gpr_prop_no_" + std::to_string(aBarNumber + 1) + ".sig2";

   if (!InquireFile(file_gpr_variance))
   {
      std::fill(aVariances.begin(),aVariances.end(),1.0);
   }
   else
   {
      // Open the sig file
      ifstream sig_file(file_gpr_variance.c_str(), ios_base::in);
      if (!sig_file)
      {
         MIDASERROR(" I was unable to open the correct .sig file, (DoInterpFit function): " + file_gpr_variance);
      }

      // Get vector of modes
      auto mc_vec = aModeCombi.MCVec();

      // Read the variances for this specific mode combination
      std::string line;
      while(std::getline(sig_file, line))
      {
         std::string key;
         Nb value;
         std::istringstream iss(line);
         if(!(iss >> key >> value))
         {
            MIDASERROR(file_gpr_variance + ": \n \t Line: " + line + " could not be interpreted correctly.");
         }

         // extract mode combination from key
         CalcCode pt_calc_code(key);

         auto pt_mc_vec = pt_calc_code.GetModeVec();

         // is this the MC we are looking for?
         if (pt_mc_vec == mc_vec)
         {
            aVariances.push_back(value);  
         }

      }

      // close sig tile again
      sig_file.close();
   }
}

/**
 * Get single point data on property pertaining to a specific mode combination 
 *
 * @param aModeCombi
 * @param aBarNumber
 * @param aBarFileProperty
**/
void BarFileInterface::GetSinglePointBarPropertyForMc
   (  const ModeCombi& aModeCombi
   ,  const In& aBarNumber
   ,  MidasVector& aBarFileProperty
   )  const
{
   // The mode combination size is a frequently used quantity in the following
   In mode_combi_size = aModeCombi.Size();
   
   // Get the correct bar file interface, i.e. the offsets for each mode combination
   In read_from_line = I_0;
   In read_to_line = I_0;
   GetOffsetsForMc(aModeCombi, read_from_line, read_to_line);

   // Read bar-file data for the specific mode-combination
   std::vector<std::vector<Nb>> bar_file_data;
   bar_file_data.assign(read_to_line - read_from_line + I_1, std::vector<Nb>());
   if (mBarFileStorageType == "ONFILE")
   {
      // Obtain the correct bar file
      std::string bar_file_name = mSaveDir + "/" + "prop_no_" + std::to_string(aBarNumber + I_1) + ".mbar";
 
      if (!InquireFile(bar_file_name))
      {
         MIDASERROR("I was unable to find the correct .mbar file: " + bar_file_name);
      }
 
      // Open the bar file
      ifstream bar_file(bar_file_name.c_str(), ios_base::in);
      if (!bar_file)
      {
         MIDASERROR(" I was unable to open the correct .mbar file: " + bar_file_name);
      }
 
      // Read the correct block of data in the bar file, (implicitly linked to a specific MC)
      std::string s;
      // First we need to navigate to the correct position in the bar-file
      for (In i_line = I_1; i_line < read_from_line; ++i_line)
      {
         std::getline(bar_file, s);
      }
      // Then we can begin reading the information that we need
      In bar_file_data_index = I_0;
      for (In j_line = read_from_line - I_1; j_line < read_to_line; ++j_line)
      {
         std::getline(bar_file, s);
         bar_file_data[bar_file_data_index] = midas::util::VectorFromString<Nb>(s);
         ++bar_file_data_index;
      }
   
      // Close the bar file again
      bar_file.close();
   }
   if (mBarFileStorageType == "INMEMORY")
   {
      In bar_file_data_index = I_0;
      for (In i_line = read_from_line - I_1; i_line < read_to_line; ++i_line)
      {
         bar_file_data[bar_file_data_index] = mBarFileData[aBarNumber][i_line];
         ++bar_file_data_index;
      }
   }

   // Second we seperate out the energy/property, (first column after the coordinates)
   aBarFileProperty.SetNewSize(bar_file_data.size());
   for (In bar_file_line = I_0; bar_file_line < bar_file_data.size(); bar_file_line++)
   {  
      aBarFileProperty[bar_file_line] = bar_file_data[bar_file_line][mode_combi_size];
   }
}

/**
 * Read all bar-file information available into memory and store as member
**/
void BarFileInterface::ReadBarFileDataIntoMem()
{
   In no_property_files = mPesInfo.GetMultiLevelInfo().GetNoOfProps();
   mBarFileData.assign(no_property_files, std::vector<std::vector<Nb>>());
   
   // All information in all available bar-files are now to be read and saved to memory
   for (In bar_prop = I_0; bar_prop < no_property_files; ++bar_prop)
   {
      // Obtain the correct bar file
      std::string bar_file_name = mSaveDir + "/" + "prop_no_" + std::to_string(bar_prop + I_1) + ".mbar";
      
      if (!InquireFile(bar_file_name))
      {
         MIDASERROR("I was unable to find the correct .mbar file, (DoInterpFit function): " + bar_file_name);
      }

      // Open the bar file
      ifstream bar_file(bar_file_name.c_str(), ios_base::in);
      if (!bar_file)
      {
         MIDASERROR(" I was unable to open the correct .mbar file, (DoInterpFit function): " + bar_file_name);
      }
     
      // Now we read the data for the individual property file
      std::string s;
      Nb bar_file_length = std::get<I_2>(mBarFilesOffsets.back());
      mBarFileData[bar_prop].assign(bar_file_length, std::vector<Nb>());
     
      for (In i = I_0; i < bar_file_length; ++i)
      {
         std::getline(bar_file, s);
         mBarFileData[bar_prop][i] = midas::util::VectorFromString<Nb>(s);
      }
      
      // Close the bar file again
      bar_file.close();
   }
}

