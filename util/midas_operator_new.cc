/**
************************************************************************
* 
* @file                midas_operator_new.cc
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Midas operator new implementation
*                      Will be used if -DMIDAS_MEM_DEBUG is set
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifdef MIDAS_MEM_DEBUG

// std headers
#include <new>

// midas headers
#include "midas_operator_new.h"
#include "midas_mem_check.h"

#pragma GCC push_options
#pragma GCC optimize ("-O3")

#define RETURN_ADDRESS 0

void* operator new(std::size_t sz)
{
   return midas::mem::alloc_mem(sz,__builtin_return_address(RETURN_ADDRESS));
}

void* operator new(std::size_t sz, const std::nothrow_t& nothrow_values) noexcept
{
   return midas::mem::alloc_mem(sz,__builtin_return_address(RETURN_ADDRESS));
}

void* operator new[](std::size_t sz)
{
   return midas::mem::alloc_mem(sz,__builtin_return_address(RETURN_ADDRESS));
}

void* operator new[](std::size_t sz, const std::nothrow_t& nothrow_values) noexcept
{
   return midas::mem::alloc_mem(sz,__builtin_return_address(RETURN_ADDRESS));
}

void operator delete(void* ptr) noexcept
{
   midas::mem::dealloc_mem(ptr,__builtin_return_address(RETURN_ADDRESS));
}

void operator delete(void* ptr, const std::nothrow_t& nothrow_values) noexcept
{
   midas::mem::dealloc_mem(ptr,__builtin_return_address(RETURN_ADDRESS));
}

void operator delete[](void* ptr) noexcept
{
   midas::mem::dealloc_mem(ptr,__builtin_return_address(RETURN_ADDRESS));
}

void operator delete[](void* ptr, const std::nothrow_t& nothrow_values) noexcept
{
   midas::mem::dealloc_mem(ptr,__builtin_return_address(RETURN_ADDRESS));
}

#undef RETURN_ADDRESS

#pragma GCC pop_options

#endif /* MIDAS_MEM_DEBUG */
