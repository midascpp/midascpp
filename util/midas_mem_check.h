/**
************************************************************************
* 
* @file                midas_mem_check.h
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Class for midas memory logging
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifdef MIDAS_MEM_DEBUG

#ifndef MIDAS_MEM_CHECK_H
#define MIDAS_MEM_CHECK_H

// std headers
#include<iostream>
#include<iomanip>
#include<functional>
#include<limits>
#include<set>
#include<fstream>
#include<stdlib.h>
#include<memory> // for std::unique_ptr

#ifdef MIDAS_MEM_STACKTRACE
#include <stackd.hpp>
#endif /* MIDAS_MEM_STACKTRACE */

// midas headers
#include "mpi/Impi.h"
#include "mpi/OrderedCallback.h"
#include "inc_gen/TypeDefs.h"
#include "libmda/util/output_call_addr.h"
#include "libmda/util/stacktrace.h"

#pragma GCC push_options
#pragma GCC optimize ("-O3")
namespace midas
{
namespace mem
{

/**
 * @class ptr_data
 *    Front guard for raw memory blocks
 *    also keeps size for logging purposes
 *    stores address of first entry plus 
 *    address of the call to operator new that created it 
 **/
struct ptr_data
{
   unsigned magic;  // magic number for mem corruption detection
   size_t   size;   // size of allocated block
   //void*    this_addr;   // address of first entry
   void*    addr;   // address of first entry
   void*    call_addr;  // adress of function calling new
#ifdef MIDAS_MEM_STACKTRACE
   stackd::trace::stacktrace_struct* stacktrace = nullptr;
#endif /* MIDAS_MEM_STACKTRACE */
   unsigned magic_middle;  // magic number for mem corruption detection
};

/**
 * @function front_guard
 *    Get frontguarding ptr_data from a ptr inside calling program.
 * @param ptr  Pointer to retrieve frontguard for.
 * @return     Return pointer to frontguard ptr_data.
 **/
template<class T>
inline ptr_data* front_guard(T* ptr)
{
   return (ptr_data*)((char*)ptr-sizeof(ptr_data)); // get ptr data
}

/**
 * @function front_guard
 *    Get frontguarding ptr_data from std::unique_ptr.
 * @param ptr  Pointer to retrieve frontguard for.
 * @return     Return pointer to frontguard ptr_data.
 **/
template<class T>
inline ptr_data* front_guard(const std::unique_ptr<T>& ptr)
{
   return front_guard(ptr.get());
}

/**
 *
 **/
void print_allocated_addresses(bool = false);

/**
 * @function operator<<
 *    Overload for pointer to ptr_data.
 * @param os         OStream to output to.
 * @param aptr_data  The data to ouput.
 * @return           Returns ostream so operator<< can be chained.
 **/
inline std::ostream& operator<<(std::ostream& os, const ptr_data* const aptr_data)
{
   os << " PRINT PTR DATA "         << std::endl;
   os << "ptr_data : \t addr : "    << (void*)aptr_data
      //<< "\t this addr : "          << (void*)aptr_data->this_addr
      << "\t saved addr : "         << aptr_data->addr
      << "\t size : "               << aptr_data->size 
      << "\t call_addr_to_line : "  << libmda::util::wrap_address_to_line(aptr_data->call_addr)
      << "\t call_addr : "          << aptr_data->call_addr
      << "\t magic number : "       << aptr_data->magic;
#ifdef MIDAS_MEM_STACKTRACE
   stackd::trace::demangle(aptr_data->stacktrace);
   stackd::trace::print(aptr_data->stacktrace, os);
#endif /* MIDAS_MEM_STACKTRACE */
   return os;
}

inline std::ostream& operator<<(std::ostream& os, const ptr_data** data)
{
   os << *data;
   return os;
}

/**
 * @class ptr_tailguard
 *    Tail guard for raw memory block.
 **/
struct ptr_tailguard
{
   unsigned magic; // magic number for mem corruption detection
#if defined MIDAS_MEM_TAILBUF && MIDAS_MEM_TAILBUF > 0 
   char     buffer[MIDAS_MEM_TAILBUF];
#endif
};

/**
 * @function tail_guard
 *    Retrive tailguard from raw pointer.
 * @param ptr    Pointer to retrieve tailguard for.
 * @return       Return pointer to the tail guard.
 **/
template<class T>
inline ptr_tailguard* tail_guard(T* ptr)
{
   auto ptr_front = front_guard(ptr);
   size_t size = ptr_front->size;            // get size of pointer
   return (ptr_tailguard*)((char*)ptr+size); // get tailguard
}

/**
 * @function tail_guard
 *    Retrive tailguard from pointer wrapped in a std::unique_ptr.
 * @param ptr    Pointer to retrieve tailguard for.
 * @return       Return pointer to the tail guard.
 **/
template<class T>
inline ptr_tailguard* tail_guard(const std::unique_ptr<T>& ptr)
{
   return tail_guard(ptr.get());
}

/**
 * @class do_log_allocation
 *    Wrapper class for bool saying whether allocations should be logged or not.
 **/
struct do_log_allocation
{
   private:
      do_log_allocation() : value(false) {}
   public:
      bool value;
      friend do_log_allocation& get_do_log_allocation();
};

do_log_allocation& get_do_log_allocation();

/**
 * @class log_allocation_guard
 *    Scope-Bound-Resource-Management class for resetting do_log_allocation, after end of scope.
 **/
struct log_allocation_guard
{
   private:
      bool saved_value;
   public:
      log_allocation_guard(): saved_value(get_do_log_allocation().value) { get_do_log_allocation().value = false; }
      ~log_allocation_guard() { get_do_log_allocation().value = saved_value; }

      void release() const { get_do_log_allocation().value = saved_value; }
};

/**
 * @class mem_stat
 *    Class for logging memory allocation and deallocation information.
 **/ 
class mem_stat
{
   private:
      // mem stats
      size_t m_current_use;
      size_t m_max_use;
      size_t m_allocated;   //total amount allocated
      size_t m_deallocated; //total amount deallocated
      size_t m_times_new;
      size_t m_times_delete;
      bool   m_warning;     //bool flag for warnings, used if m_allocated overcounts 
      bool   m_mem_is_corrupted = false;
      std::set<ptr_data*>  m_curr_alloc_data;
      
      // update mem stats
      void incr_current_use(size_t sz) { m_current_use+=sz; }
      void decr_current_use(size_t sz) { m_current_use-=sz; }
      
      void update_max_usage() 
      { 
         if(m_current_use>m_max_use) m_max_use=m_current_use; 
      }

      void incr_allocated(size_t sz) 
      { 
         size_t allocated_old = m_allocated;
         m_allocated+=sz;   
         if(m_allocated<allocated_old) m_warning = true;
      }
      void incr_deallocated(size_t sz) { m_deallocated+=sz; }
      
      void incr_times_new()    { ++m_times_new;   }
      void incr_times_delete() { ++m_times_delete; }
      
      // function wrapper for setting output width
      auto width(int i=std::numeric_limits<size_t>::digits10) const -> decltype(std::setw(i)) 
      { 
         return std::setw(i); 
      }
      
      // we don't allow copy/move construction/assignment
      mem_stat(const mem_stat&) = delete;
      mem_stat(mem_stat&&)      = delete;
      mem_stat& operator=(const mem_stat&) = delete;
      mem_stat& operator=(mem_stat&&)      = delete;
      
      ///
      // default constructor is private
      // should only be constructed through get_mem_stat() friend function
      ///
      mem_stat(): m_current_use(0)
                , m_max_use(0)
                , m_allocated(0)
                , m_deallocated(0)
                , m_times_new(0)
                , m_times_delete(0)
                , m_warning(false)
                , m_curr_alloc_data()
      { 
         get_do_log_allocation().value = true;
      }

   public:
      ///
      // destructor... will output memory allocation/deallocation statistics
      ///
      ~mem_stat() { 
         get_do_log_allocation().value = false;
         // we do an ordered_callback in case we have mpi we get 
         // output from each rank in ordered form
         // sadly syntax is a little obscure because we need to pass a member function
         // for more info lookup std::mem_fn(), std::bind(), and std::placeholders
         midas::mpi::OrderedCallback(midas::mpi::CommunicatorWorld(), std::bind(mem_fn(&mem_stat::status), this, std::placeholders::_1), std::cout);
         //status(std::cout);
      }
      
      ///
      //
      ///
      bool log_alloc() const 
      {  
         return get_do_log_allocation().value;
      }
      
      ///
      //
      ///
      bool is_allocated(ptr_data* ptr)
      {
         auto it = m_curr_alloc_data.find(ptr);
         return (m_curr_alloc_data.end() != it); 
      }

      ///
      //
      ///
      void stat_allocation(ptr_data* ptr)
      {
         size_t sz = ptr->size;
         incr_current_use(sz);
         update_max_usage();
         incr_allocated(sz);
         incr_times_new();
      }

      ///
      //
      ///
      void stat_deallocation(ptr_data* ptr)
      {
         size_t sz = ptr->size;
         decr_current_use(sz);
         incr_deallocated(sz);
         incr_times_delete();
      }
      
      ///
      // log allocation information
      ///
      void log_allocation(ptr_data* ptr) 
      { 
         log_allocation_guard guard; // we do not log allocations in this function
         auto check = m_curr_alloc_data.insert(ptr);
         if(!check.second)
         {
            // if we can't insert into the map we just abort... no way to recover
            std::cout << "Undefined behaviour ... terminating\n" << __FILE__ << " " << __LINE__ << std::endl;
            midas::mpi::OrderedCallback(midas::mpi::CommunicatorWorld(), std::bind(mem_fn(&mem_stat::status),this,std::placeholders::_1), std::cout);
            std::abort();
         }
      }

      ///
      // log deallocation information
      ///
      void log_deallocation(ptr_data* ptr)
      {
         log_allocation_guard guard; // we do not log allocations in this function
         auto it = m_curr_alloc_data.find(ptr);
         if(m_curr_alloc_data.end() == it) // do we know of the pointer we try to dealloc
         {
            // if not we warn about it !
            std::cout << "WARNING!! Tried to deallocate something we don't know anything about!!" << std::endl;
            std::cout << ptr << std::endl;
         }
         else // else we just "de-log"
         {
            m_curr_alloc_data.erase(it); 
         }
      }
      
      ///
      // output mem usage status
      ///
      void status(std::ostream& a_ostream) const
      {
         a_ostream << " ---*** MEMORY STATISTICS ***---  ---*** MEMORY STATISTICS ***--- \n";
#ifdef VAR_MPI
         a_ostream << "mem output from rank " << midas::mpi::GlobalRank() << "\n";
#endif /* VAR_MPI */
         
         a_ostream << "current use      : " << width() << m_current_use  << width(0) << " bytes\n"
                   << "max use          : " << width() << m_max_use      << width(0) << " bytes\n"
                   << "total allocated  : " << width() << m_allocated    << width(0) << " bytes\n"
                   << "total deallocated: " << width() << m_deallocated  << width(0) << " bytes\n"
                   << "times new        : " << width() << m_times_new    << width(0) << "\n"
                   << "times delete     : " << width() << m_times_delete << width(0) << "\n";
         
         if(m_current_use) a_ostream << " WARNING current use != 0 " << "\n";
         
         if(m_warning) a_ostream << " WARNING !! " << "\n";
         
         if(corrupted()) a_ostream << " WARNING MEMORY CORRUPTION " << "\n";

         if(!m_curr_alloc_data.empty())
         {
            a_ostream << "List of size " << m_curr_alloc_data.size() << " of still allocated pointers : \n";
            int i = 0;
            for(auto it = m_curr_alloc_data.begin(); it != m_curr_alloc_data.end(); ++it)
            {
               a_ostream << i << " " << *it << " PTR PRINTED\n" << std::flush;
               ++i;
               if(i == 100) // when we have printed 100 leaks, we stop...
               {
                  a_ostream << " printed " << i << " leaks, stopping now... \n"
                            << " there are a total of " << m_curr_alloc_data.size() << " memory leaks "
                            << "\n" << std::flush;
                  break; // break printing loop
               }
            }
         }

         a_ostream << " ---*** ***************** ***---  ---*** ***************** ***--- \n";
         a_ostream << std::flush;
      }
   
      ///
      // check if memory is corrupted
      ///
      void mem_corruption()   { m_mem_is_corrupted = true;  }
      void reset_corruption() { m_mem_is_corrupted = false; } // don't use unless you know what you want
      bool corrupted() const  { return m_mem_is_corrupted; }

      unsigned current_use()       const { return m_current_use; }
      unsigned max_use()           const { return m_max_use; }
      unsigned total_allocated()   const { return m_allocated; }
      unsigned total_deallocated() const { return m_deallocated; }
      unsigned times_new()         const { return m_times_new; }
      unsigned times_delete()      const { return m_times_delete; }

      ///
      // print return address of ptr
      ///
      void print_ptr_data(void* ptr, std::ostream& a_ostream) const
      {
         for(auto it = m_curr_alloc_data.begin(); it != m_curr_alloc_data.end(); ++it)
         {
            if((*it)->call_addr == ptr)
            {
               a_ostream << *it << std::endl;
            }
         }
      }
      
      ///
      // interface friend function
      ///
      friend mem_stat& get_mem_stat();
};

// interface function to get the mem_stat instance
mem_stat& get_mem_stat();

// function for allocating memory
void* alloc_mem(size_t sz, void* call_addr);

// function for deallocating memory
void dealloc_mem(void* pointer, void* call_addr);

} /* namespace mem */
} /* namespace midas */

#pragma GCC pop_options

#endif /* MIDAS_MEM_CHECK_H */

#endif /* MIDAS_MEM_DEBUG */
