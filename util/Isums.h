/**
************************************************************************
* 
* @file                Isums.h
*
* Created:             16-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   inlines sum over integer results. 
* 
* Last modified: Mon Oct 30, 2006  09:26AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTSUMS_H
#define INTSUMS_H

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"
#include<vector>
#include"inc_gen/math_link.h"
#include"mmv/MidasVector.h"

/**
* Pure integer versions
* */
inline In IntSum1(In aI){In i=aI; return i*(i+1)/2;}                            ///< return 1^1+2^1+3^1+...+i^1
inline In IntSum2(In aI){In i=aI; return i*(i+1)*(2*i+1)/6;}                    ///< returns 1^2+2^2+3^2+...+i^2
inline In IntSum3(In aI){In i=aI; return i*i*(i+1)*(i+1)/4;}                    ///< returns 1^3+2^3+3^3+...+i^3
inline In IntSum4(In aI){In i=aI; return i*(i+1)*(2*i+1)*(3*i*i+3*i+1)/30;}     ///< returns 1^4+2^4+3^4+...+i^4
inline In Faculty(In aI){if (aI==I_0) aI=I_1;In fa=aI; for(In i=aI-I_1;i>I_0;i--) fa*=i; return fa;}  ///< returns aI!
inline In InNorm2(const vector<In>& aVec){In Norm = I_0; for(In i=I_0; i<aVec.size();i++) Norm += aVec[i]*aVec[i]; return Norm;} ///< return norm2 of vector<In>

/**
* Floating versions with floating returns to account for overflow!
* */
inline Nb NbIntSum1(In aI){Nb i=aI; return i*(i+1)/2;}                          ///< return 1^1+2^1+3^1+...+i^1
inline Nb NbIntSum2(In aI){Nb i=aI; return i*(i+1)*(2*i+1)/6;}                  ///< returns 1^2+2^2+3^2+...+i^2
inline Nb NbIntSum3(In aI){Nb i=aI; return i*i*(i+1)*(i+1)/4;}                  ///< returns 1^3+2^3+3^3+...+i^3
inline Nb NbIntSum4(In aI){Nb i=aI; return i*(i+1)*(2*i+1)*(3*i*i+3*i+1)/30;}   ///< returns 1^4+2^4+3^4+...+i^4
inline Nb NbFaculty(In aI){if (aI==I_0) aI=I_1;Nb fa=Nb(aI); for(In i=aI-I_1;i>I_0;i--) fa*=i; return fa;}  ///< returns aI!
inline Nb NbDoubleFacultyEven(In aI){if (aI==I_0) aI=I_1;Nb fa=Nb(aI); for(In i=aI-I_2;i>I_0;i-=I_2) fa*=i; return fa;}  ///< returns aI!! where aI is an even number

inline Nb NbDoubleFacultyOdd(In aI){if (aI==-I_1) aI=I_1;Nb fa=Nb(aI); for(In i=aI-I_2;i>I_0;i-=I_2) fa*=Nb(i); return fa;}  ///< returns aI!! where aI is an odd number

inline Nb IntDoubleFacultyOdd(In aI){if (aI==-I_1) aI=I_1;In result=aI; for(In i=aI-I_2;i>I_0;i-=I_2) result*=i; return result;}  ///< returns aI!! where aI is an odd number

/**
* Factorials and binomial coefficients.
* */
inline Nb NbFactLn(In aN)
{
   Nb sum=0;
   for (In i=1;i<=aN;i++) sum+=log(Nb(i));
   return sum;
}

/**
 *
 **/
inline Nb NbFactLnTable(In aN)
{
   static const In NTOP=100;
   static MidasVector a(NTOP);
   static bool init=true;
   
   if (init)
   {
      init=false;
      for (In i=I_0; i<NTOP; ++i) 
         a[i]=NbFactLn(i);
   }
  
   if (aN>=NTOP) 
      MIDASERROR("Requesting factorial larger than implemented by table");

   return a[aN];
}
///> NbFactorial.
inline Nb NbFact(In aN) 
{
   return exp(NbFactLn(aN));
}

//d. toffoli
inline Nb NbFactTable(In aN) {return exp(NbFactLnTable(aN));}                             ///< NbFactorial.
//sne
inline Nb NbEmFactLn(In aN)
{
   Nb sum=0;
   for (In i=aN;i>=1; i-=2) sum+=log(Nb(i));
   //debug 
   //Mout << " emi factorial for n= " << aN << "is: " << sum << endl;
   return sum;
}

inline Nb NbEmFact(In aN)
{
   //Mout << " integer aN in NbEmFact: " << aN << endl;
   //Mout << " emi-factorial: " << exp(NbEmFactLn(aN)) << endl;
   return exp(NbEmFactLn(aN));
}///< Nb Emi-Factorial.

/**
 *
 **/
inline Nb NbBinCo(In aN,In aK) 
{
   if ( (aN>=aK) && (aK>=I_0) )
   { 
      return floor(C_I_2 + exp(NbFactLn(aN)-NbFactLn(aK)-NbFactLn(aN-aK)));
   }
   return C_0;
}

/**
 *
 **/
inline Nb NbBinCoTable(In aN,In aK) 
{
   if ( (aN>=aK) && (aK>=I_0) )
   { 
      return floor(C_I_2 + exp(NbFactLnTable(aN)-NbFactLnTable(aK)-NbFactLnTable(aN-aK)));
   }
   return C_0;
}

/**
 *
 **/
inline In IntBinCoef(In aN,In aK)
{     
   if ( (aN>=aK) && (aK>=I_0) )
   {
      Nb sum = exp(NbFactLn(aN)-NbFactLn(aK)-NbFactLn(aN-aK));
      return In(sum+C_I_2);
   }
   else
   {
      return I_0;
   }
}     

/**
 *
 **/
inline In InPow(In aX, In aP)
{
  if (aP == I_0) return I_1;
  if (aP == I_1) return aX;

  In tmp= InPow(aX,aP/I_2);
  if (aP%I_2 == I_0) return tmp*tmp;
  else return aX*tmp*tmp;
}

#endif /* INTSUMS_H */
