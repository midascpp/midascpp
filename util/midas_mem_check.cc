/**
************************************************************************
* 
* @file                midas_mem_check.cc
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implemenetation of midas memory logger/checker
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifdef MIDAS_MEM_DEBUG

#include "midas_mem_check.h"

#include <iostream>
#include <mutex>

#include "mpi/Impi.h"
#include "mpi/Finalizer.h"

#pragma GCC push_options
#pragma GCC optimize ("-O3")

namespace midas
{
namespace mem
{

std::recursive_mutex& get_allocation_log_mutex()
{
   static std::recursive_mutex allocation_log_mutex;
   return allocation_log_mutex;
}

do_log_allocation& get_do_log_allocation()
{
   static do_log_allocation log;
   return log;
}

// function to control creation of mem_stat object
// this will ensure that mem_stat object is created on first use
mem_stat& get_mem_stat()
{
   get_allocation_log_mutex();
   get_do_log_allocation();
#ifdef VAR_MPI
   mpi::get_Finalizer(); // get mpi_finalizer as mem_stat needs mpi features
                        // this will ensure that mpi finalize is called after mem_stats 
                        // destructor is called
#endif /* VAR_MPI */
   static mem_stat mem;
   return mem;
}

bool print_addresses = false;

void print_allocated_addresses(bool print)
{
   print_addresses = print;
}

// define some preprocessor macros
#define PTR_DATA_SIZE      sizeof(ptr_data)
#define PTR_TAILGUARD_SIZE sizeof(ptr_tailguard)
#define MAGIC              0xDEADBEEF   // random magic number

/**
 * alloc_mem() 
 *
 * function for allocating memory and logging information
 * will also put front and tail guards on the raw memory
 * to check for corruption at deallocation
 * 
 * also saves binary address of where operator new was called in program
 **/
void* alloc_mem(size_t sz, void* call_addr)
{
   size_t size = sz + PTR_DATA_SIZE + PTR_TAILGUARD_SIZE; // calc total size
   ptr_data* ptr =  (ptr_data*)malloc(size); // allocate total ptr including data

   if(print_addresses)
   {
      std::cout << " ALLOCATED : " << ptr << std::endl;
   }
   
   if(!ptr)
   {
      std::cout << " Allocation failed! Size: " << sz << "(actual size: << " << size << ")." << std::endl;
      abort();
   }

   void* pointer = (char*)ptr + PTR_DATA_SIZE; // make pointer of size requested to return
   ptr_tailguard* ptr_tail = (ptr_tailguard*)((char*)ptr + PTR_DATA_SIZE + sz); // get tailguard
   ptr->magic=MAGIC; // set front guard to MAGIC
   ptr->magic_middle=MAGIC; // set front guard to MAGIC
   ptr->size=sz;     // save size
   //std::cout << " here " << std::endl;
   //ptr->this_addr=(void*)ptr; // save address of pointer
   //std::cout << " but not here " << std::endl;
   ptr->addr=(void*)pointer; // save address of pointer
   ptr->call_addr=call_addr; // save caller address
   ptr_tail->magic=MAGIC;    // set tail guard to MAGIC

#ifdef MIDAS_MEM_STACKTRACE
   ptr->stacktrace = (stackd::trace::stacktrace_struct*)malloc(sizeof(stackd::trace::stacktrace_struct));
   ptr->stacktrace->size       = 0;
   ptr->stacktrace->addrlen    = 0;
   ptr->stacktrace->addrlist   = nullptr;
   ptr->stacktrace->symbollist = nullptr;
   ptr->stacktrace->symbollist_demangled = nullptr;

   stackd::trace::create(ptr->stacktrace);
#endif /* MIDAS_MEM_STACKTRACE */
   
   {
   std::lock_guard<std::recursive_mutex> guard(get_allocation_log_mutex());

   if(get_mem_stat().log_alloc())
   {
      get_mem_stat().stat_allocation(ptr); // stat allocation
#ifndef MIDAS_MEM_NO_LOG_ALLOCATION
      get_mem_stat().log_allocation(ptr); // log allocation
#endif /* MIDAS_MEM_NO_LOG_ALLOCATION */
   }

   }
   
   if(!pointer)
   {
      std::cout << " Returning nullptr " << std::endl;
   }
   
   return pointer;           // return requsted pointer
}

/**
 * dealloc_mem()
 *
 * function for deallocating memory and logging information
 * will also check front and tail guards for corruption of memory
 **/
void dealloc_mem(void* pointer, void* call_addr)
{
   // Check for nullptr (it is allowed to call delete on a nullptr)
   if(!pointer)
   {
      //std::cout << " Deallocating nullptr " << std::endl;
      return;
   }

   ptr_data* ptr = (ptr_data*)((char*)pointer-PTR_DATA_SIZE); // get ptr data
   
   if(print_addresses)
   {
      std::cout << " DEALLOCATING : " << ptr << std::endl;
   }

   size_t size = ptr->size;                                   // get size of pointer
   ptr_tailguard* ptr_tail = (ptr_tailguard*)((char*)pointer+size); // get tailguard
   if(ptr->magic != MAGIC || ptr->magic_middle != MAGIC || ptr_tail->magic != MAGIC) // check front and tail guard
   {        
      get_mem_stat().mem_corruption();
      // do some output
      std::cout << " -~!| WARNING |!~- MEMORY CORRUPTION -~!| WARNING |!~- " << std::endl;
      std::cout << " ALLOCATION: \n";
#ifdef MIDAS_MEM_STACKTRACE
      stackd::trace::demangle(ptr->stacktrace);
      stackd::trace::print(ptr->stacktrace, std::cout);
#else 
      libmda::util::output_call_addr(std::cout, ptr->call_addr, '\t');
#endif /* MIDAS_MEM_STACKTRACE */
      std::cout << " DEALLOCATION: \n";
#ifdef MIDAS_MEM_STACKTRACE
      auto stacktrace = stackd::trace::create();
      stackd::trace::demangle(&stacktrace);
      stackd::trace::print(&stacktrace, std::cout);
#else
      libmda::util::output_call_addr(std::cout, call_addr, '\t');
#endif /* MIDAS_MEM_STACKTRACE */
      std::cout << " -~!| ------------------------------------------- |!~- " << std::endl;
      // ian: make this also output call address...
      //libmda::util::print_stacktrace(std::cout);
   }

#ifdef MIDAS_MEM_STACKTRACE
   stackd::trace::destroy(ptr->stacktrace);
   free(ptr->stacktrace);
#endif /* MIDAS_MEM_STACKTRACE */
   
   {
   std::lock_guard<std::recursive_mutex> guard(get_allocation_log_mutex());
   if(get_mem_stat().log_alloc())
   {
      get_mem_stat().stat_deallocation(ptr); // stat deallocation
   }
   
#ifndef MIDAS_MEM_NO_LOG_ALLOCATION
   if(get_mem_stat().log_alloc())
   {
      if(get_mem_stat().is_allocated(ptr))
      {
         free(ptr); // free pointer
      }
      else
      {
         std::cout << " ERROR ABORT! De-allocating pointer that was not allocated." << std::endl;
         abort();
      }
   }
   else
   {
#endif /* MIDAS_MEM_NO_LOG_ALLOCATION */
      free(ptr);
#ifndef MIDAS_MEM_NO_LOG_ALLOCATION
   }
   
   if(get_mem_stat().log_alloc())
   {
      get_mem_stat().log_deallocation(ptr); // log deallocation
   }
#endif /* MIDAS_MEM_NO_LOG_ALLOCATION */
   }
}

// undef preprocessor macros
#undef PTR_DATA_SIZE
#undef PTR_TAILGUARD_SIZE
#undef MAGIC

} /* namespace mem */
} /* namespace midas */

#pragma GCC pop_options

#endif /* MIDAS_MEM_DEBUG */
