/**
 *******************************************************************************
 * 
 * @file    Io_fwd.h
 * @date    06-11-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Forward declarations (only) for I/O related stuff. Necessary for some
 *    template functions/classes.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef IO_FWD_H_INCLUDED
#define IO_FWD_H_INCLUDED

#include <vector>
#include <ostream>
#include <set>
#include <utility>
#include <map>
#include <ostream>

//@{
//! Forward declarations of operator<<'s
template<typename T> 
std::ostream& operator<<(std::ostream&, const std::vector<T>&);
template<typename T>
std::ostream& operator<<(std::ostream&, const std::set<T>&);
template<typename T, typename U> 
std::ostream& operator<<(std::ostream&, const std::pair<T,U>&);
template<class T, class U>
std::ostream& operator<<(std::ostream& arOut, const std::map<T,U>& ar1);
template<typename T>
std::ostream& operator<<(std::ostream& arOut, const std::set<T>& arCont);
//@}


#endif /* IO_FWD_H_INCLUDED */
