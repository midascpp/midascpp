/**
************************************************************************
* 
* @file                InSet.cc
*
* Created:             14-01-2013
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Implement InSet class members 
* 
* Last modified:       02-06-2014
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <algorithm> 
using std::find;
using std::unique;
using std::set_union;
using std::set_difference;


#include "inc_gen/math_link.h" 
//
// My headers:
#include "inc_gen/TypeDefs.h" 
#include "input/Input.h" 
#include "util/Io.h" 
#include "util/InSet.h" 


/**
* Initialized InSet by string
* */
InSet::InSet(string aString)
{
   istringstream s(aString);
   In j;
   while (s >> j)
   {
      mMemberNrs.insert(j);
   }
}
/**
* checks whether In is contained in this group 
* */
bool InSet::InContained(const In& arIn) const
{
   if (find(mMemberNrs.begin(), mMemberNrs.end(), arIn) != mMemberNrs.end()) 
   {
      return true;
   }
   return false;
}

/**
* returns the highst entry
* */
In InSet::Highest()
{
   set<In>::iterator it(max_element(mMemberNrs.begin(),mMemberNrs.end()));
   return *it;
}
/**
* return all members as vector.
* */
vector<In> InSet::Vec() const
{
   In n_set = mMemberNrs.size();
   vector<In> set_vec;
   set_vec.clear();
   set_vec.reserve(n_set);
   copy(mMemberNrs.begin(),mMemberNrs.end(),set_vec.begin());
   for (In i = I_0; i < n_set ; i++)
      set_vec.push_back(set_vec[i]);
   return set_vec;
}
