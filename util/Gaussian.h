/**
************************************************************************
* 
* @file                Gaussian.h
*
* Created:             20-02-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Simple Class for containing Gaussians.
* 
* Last modified: man mar 21, 2005  11:39
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GAUSSIAN_H  
#define GAUSSIAN_H  

#include<vector>
using std::vector;

#include"inc_gen/math_link.h"

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"

/**
* Class Gaussian: 
* */
class Gaussian
{
   private:
      Nb mAlpha;                                                ///< G = mPreConst*exp(-mAlpha*(x-mEq)**2) 
      Nb mPreConst;                                             ///< 
      Nb mEq;                                                   ///< 
   public:
      Gaussian(Nb aAlpha = C_0,Nb aEq = C_0,Nb aConst=C_1) 
              {mAlpha=aAlpha;mEq=aEq;mPreConst=aConst;}
      Nb ValueFor(Nb aXval) {Nb sq = aXval-mEq; sq*=sq; return mPreConst*exp(-mAlpha*sq);}
      Nb Alpha(Nb aAlpha) {return mAlpha;}
      Nb PreConst(Nb aConst=C_1) {return mPreConst;}
      Nb Eq(Nb aEq = C_0) {return mEq;}
      void SetAlpha(Nb aAlpha) {mAlpha=aAlpha;}
      void SetPreConst(Nb aConst=C_1) {mPreConst=aConst;}
      void SetEq(Nb aEq = C_0) {mEq=aEq;}
      friend Nb AveOfValsForVectorOfGaussians(vector<Gaussian>& arVecOfGauss,Nb aXval);
};

#endif
