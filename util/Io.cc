/**
************************************************************************
* 
* @file                Io.cc
*
* Created:             27-02-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Output format utilities
* 
* Last modified: Wed Nov 18, 2009  01:32PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <sys/stat.h>

#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <iterator>
#include <set>
#include <unistd.h>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasSystemCaller.h"
#include "util/MidasStream.h"
#include "util/paral/run_process.h"
#include "util/FileSystem.h"
#include "util/Os.h"
#include "mmv/MidasVector.h"

// mpi
#include "mpi/Impi.h"

// using declarations
using std::vector;
using std::string;
using std::transform;
using std::istream_iterator;
using std::ostream_iterator;
using std::left;
using std::set;

typedef string::size_type Sst;

static bool DebugUtil=false;

void Out72Char(std::ostream& aOut,char aC1, char aC2, char aC3)
{
   aOut << aC1;
   for (In i=I_0;i<I_70;++i) aOut << aC2;
   aOut << aC3 << '\n';
}

/**
 * Prints a string 70 characters pr. line, with borders defined by char aC.
 * If a newline is encountered, the line is also cut.
 * All lines but the first get prepended with a whitespace.
 * All calls to this function has a whitespace at the start of the string,
 * hence a whitespace is not added to first line (this is not really that pretty,
 * but this is how the function has historically been called, if you want you 
 * can change all calls to the function to not include the whitespace, and just treat 
 * it here for the first line as well). This makes the function really ugly,
 * but it does the job.
 *
 * @param aOut
 *    The stream to print to.
 * @param aString
 *    The string to print.
 * @param aC
 *    The character used for the border.
 **/
void OneArgOut72(std::ostream& aOut, const string& aString, char aC)
{
   bool first = true;
   for(int i = 0; i < aString.size(); )
   {
      aOut << aC;
      if(!first) /* If not first line, we print a whitespace (everywhere this function is called strings passed to this function already start with a whitespace) */
         aOut << " ";
      int end = std::min(i + 69, int(aString.size()));
      if(int newline = int(aString.substr(i, aString.size()).find('\n')) != -1)
      {
         end = std::min(end, i + newline - 1);
      }
      aOut << left << (first ? std::setw(I_70) : std::setw(69)) << aString.substr(i, end);
      aOut << aC << '\n';
      i += end - i;
      if(aString[i] == '\n') /* Skip the newline if any, we already printed it explicitly above */
      {
         ++i;
      }
      first = false;
   }
}

void TwoArgOut72(std::ostream& aOut, string aString1,string aString2, char aC)
{
   aOut << aC;
   aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << aString2;
   aOut << aC << '\n';
}

void TwoArgOut72(std::ostream& aOut, string aString1, bool aBool, char aC)
{
   aOut << aC;
   if (aBool) aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << "true";
   if (!aBool) aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << "false";
   aOut << aC << '\n';
}

void TwoArgOut(std::ostream& aOut, string aString1, In aWi1,In aInt, In aWi2, char aC)
{
   aOut << aC;
   aOut << left << setw(aWi1) << aString1 << "| " << setw(aWi2) << aInt;
   aOut << aC << '\n';
}

void TwoArgOut72(std::ostream& aOut, string aString1, In aInt, char aC)
{
   aOut << aC;
   aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << aInt;
   aOut << aC << '\n';
}

void TwoArgOut72(std::ostream& aOut, string aString1, size_t aSizeT, char aC)
{
   aOut << aC;
   aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << aSizeT;
   aOut << aC << '\n';
}

void TwoArgOut72(std::ostream& aOut, string aString1, Nb aNb, char aC)
{
   aOut << aC;
   aOut << left << setw(I_30+I_4) << aString1 << "| " << setw(I_30+I_4) << aNb;
   aOut << aC << '\n';
}

void TwoArgOut(std::ostream& aOut, string aString1, In aWi1,Nb aNb, In aWi2,char aC)
{
   aOut << aC;
   aOut << left << setw(aWi1) << aString1 << "| " << setw(aWi2) << aNb;
   aOut << aC << '\n';
}

void OneArgBox72(std::ostream& aOut, string aString)
{
   aOut << '\n';
   aOut << '+';
   for (In i=I_1;i<=I_70;i++) aOut << '-';
   aOut << '+'<< '\n';
   aOut << '|' << left << setw(I_70) << aString << '|' << '\n';
   aOut << '+';
   for (In i=I_1;i<=I_70;i++) aOut << '-';
   aOut << '+'<< endl;
   aOut << '\n';
}

/**
* StringNoCaseCompare compares two string and returns true if they
* are the same, but does not distinguish between 
* upper and lower case.
* NOTE Addition!!!!!: it also does not care about blanks!!!
* */
bool StringNoCaseCompare(string s1,string s2, In aLcompare)
{
   while(s1.find(" ")!= s1.npos) s1.erase(s1.find(" "),I_1);   // Delete ALL blanks
   while(s2.find(" ")!= s2.npos) s2.erase(s2.find(" "),I_1);   // Delete ALL blanks
   transform(s1.begin(),s1.end(),s1.begin(),(In(*) (In))toupper);
   transform(s2.begin(),s2.end(),s2.begin(),(In(*) (In))toupper);
   if (aLcompare == 0 )
   {
      In l1 = s1.length();
      In l2 = s2.length();
      if (l1 != l2) return false;
      return s1==s2;
   }
   else 
   {
      string s3 = s1.substr(I_0,aLcompare);
      string s4 = s2.substr(I_0,aLcompare);
      return s3==s4;
   }
}
/**
* Takes a string and removes the unit and return instead a conversion factor.
* NOTE!! Seeks first unit! eV cm-1 will be eV
* NOTE!! If there were more characters they are converted to uppercase now.
* */
void CheckUnit(string& arS, Nb& arConv)
{
   transform(arS.begin(),arS.end(),arS.begin(),(In(*) (In))toupper);           // Transform to all upper.
   while(arS.find(" ")!= arS.npos) arS.erase(arS.find(" "),I_1);   // Delete ALL blanks

   arConv = C_1;
   Sst i_cm= arS.find("CM-1");          
   if (i_cm != arS.npos)
   {
      arConv = C_1/C_AUTKAYS;
      arS.erase(i_cm,4);
   }
   Sst i_ev= arS.find("EV");          
   if (i_ev != arS.npos)
   {
      arConv = C_1/C_AUTEV;  
      arS.erase(i_ev,2);
   }
}

/**
 * Output a set of strings 
 **/
void OutPutSetOfStrings
   (  MidasStream& arOut
   ,  const set<string>& arSetOfStrings
   ,  const std::string& aName
   )
{
  arOut << aName << endl;
  for (std::set<string>::const_iterator i=arSetOfStrings.begin();
        i!=arSetOfStrings.end();i++) arOut<< *i << endl;
}

/**
 * Output a vector of strings 
 **/
void OutPutVectorOfStrings
   (  MidasStream& arOut
   ,  const std::vector<string>& arSetOfStrings
   ,  const std::string& aName
   )
{
  arOut << aName << endl;
  for (std::vector<string>::const_iterator i=arSetOfStrings.begin();
        i!=arSetOfStrings.end();i++) arOut<< *i << endl;
}

/**
 * Wait until the file is unlocked. As far as possible.
 **/
void Delay(In aN)
{
   sleep(aN);
}

/**
 * Inquire if file exist 
 **/
bool InquireFile(const std::string& aFile1)
{
   return midas::filesystem::Exists(aFile1);
}

/**
 * Finding all file names in a directory .
 **/
void VectorOfFileNamesPath
   (  string aPathName
   ,  vector<string>& arVecString
   )
{
   string names = aPathName + "/";
   std::stringstream tmp;
   system_command_t command = {"ls", names};
   MIDASSYSTEMTOSTREAM(command,tmp);
   
   string s;
   while(tmp>>s)
   {
      arVecString.push_back(s);
   }
}
/**
* A very Simple copy file routine.
* */
void VectorOfFileNames(string aBaseName,vector<string>& arVecString)
{
   string names = aBaseName + "*";
   std::stringstream tmp;
   system_command_t command = {"bin/sh","-c","ls",names};
   MIDASSYSTEMTOSTREAM(command,tmp);
   
   string s;
   while(tmp>>s)
   {
      arVecString.push_back(s);
   }
}

/**
* A very Simple copy file routine.
* */
void CopyAllFiles(string aFile1,string aFile2)
{
   if (midas::mpi::IsMaster())
   {
      string dir = midas::os::Getcwd();
      string files1 = dir+"/"+aFile1;
      string files2 = dir+"/"+aFile2;
      string base   = files1; 
      vector<string> vector_of_file_names;
      VectorOfFileNames(files1,vector_of_file_names);
      for (In i=I_0;i<vector_of_file_names.size();i++)
      {
         string file_name = vector_of_file_names[i];
         In n=files1.size();
         In n2=vector_of_file_names[i].size();
         string final_piece=vector_of_file_names[i].substr(n,n2-n);
         string file_name2 = files2+final_piece;
         bool exist = InquireFile(file_name2);
         if (!exist)
         {
            Mout << " Copy: " << file_name 
               << "\n To:   " << file_name2 << endl;
            CopyFile(file_name,file_name2);
         }
         else
         {
            Mout << " File: " << file_name2 << " exists! " << endl;
            Mout << " File: " << file_name  << " not copied!" << endl;
         }
      }
   }
}

/**
 * A very Simple copy file routine.
 **/
void CopyFile(std::string aFile1, std::string aFile2)
{
   if (midas::mpi::IsMaster())
   {
      //system_command_t command = {"cp",aFile1,aFile2};
      //MIDASSYSTEM(command);
      In status = midas::filesystem::Copy(aFile1, aFile2);
      if(status != 0)
      {
          MIDASERROR("Could not copy file '" + aFile1 + "' to '" + aFile2 + "'.");
      }
   }
}

/*
* A very Simple copy file routine.
* */
void CopyAllFilesFullPath(string aPath1,string aPath2)
{
   if (midas::mpi::IsMaster())
   {
      vector<string> vector_of_file_names;
      VectorOfFileNamesPath(aPath1,vector_of_file_names);
      for (In i=I_0;i<vector_of_file_names.size();i++)
      {
         bool exist = InquireFile(aPath2+"/"+vector_of_file_names[i]);
         if (!exist)
         {
            CopyFile(aPath1+"/"+vector_of_file_names[i],aPath2+"/"+vector_of_file_names[i]);
         }
         else
         {
            Mout << " File: " << aPath2+"/"+vector_of_file_names[i] << " exists! " << endl;
            Mout << " File: " << aPath1+"/"+vector_of_file_names[i]  << " not copied!" << endl;
         }
      }
   }
}

/**
* A very simple mv file routine.
*
* Niels: We could also use std::remove and std::rename to avoid system calls?
* */
void MvFile(string aFile1,string aFile2)
{
   if (midas::mpi::IsMaster())
   {
      if (aFile1.find("*") != aFile1.npos && aFile1.size() == 0
        && aFile2.find("*") != aFile2.npos && aFile2.size() == 0)
      {
         Mout << " I do not allow stars/empty strings in the file names for moving" << endl;
         MIDASERROR(" Not-allowed * or empty string in MvFile");
      }
      else
      {
         //system_command_t command = {"mv",aFile1,aFile2};
         //MIDASSYSTEM(command);
         midas::filesystem::Rename(aFile1, aFile2);
      }
   }
}
/**
* A very touch on a file to create it
* */
void TouchFile(string aFile)
{
   if (aFile.find("*") != aFile.npos && aFile.size() == 0)
   {
      Mout << " I do not allow stars/empty string in the file name for touching " << endl;
      MIDASERROR(" Not-allowed * or empty string in touching File");
   }
   else
   {
      //system_command_t command = {"touch",aFile};
      //MIDASSYSTEM(command);
      midas::filesystem::Touch(aFile);
   }
}
/**
* A very Simple rm file routine.
* */
void RmFile(string aFile)
{
   if (midas::mpi::IsMaster())
   {
      if (aFile.find("*") != aFile.npos && aFile.size() == 0)
      {
         Mout << " I do not allow stars/empty string in the file name for removing" << endl;
         MIDASERROR(" Not-allowed * or empty string in RmFile");
      }
      else
      {
         //system_command_t command = {"rm",aFile};
         //MIDASSYSTEM(command);
         midas::filesystem::Remove(aFile);
      }
   }
}
/**
 * Serach in file for label 
 **/
bool SearchInFile(string arStringTest, ifstream& InFile, string arProp, string filename)
{
   InFile.clear();
   InFile.seekg (0, ios::beg);
   string s;
   getline(InFile,s);
   while( !(s.find(arStringTest) != s.npos) && !InFile.eof() )
   {
      getline(InFile,s);
   }

   if (InFile.eof())
   {
      Mout << arStringTest << " label not found on file " << filename << endl;
      return false;
   }
   else
   {
      if (gIoLevel > I_10)
      {
         Mout << " " << arProp << " read from file " << filename << std::endl;
      }
      return true;
   }
}
/**
 * Serach in file for label 
 **/
bool SearchInFile(string arStringTest, std::istringstream& InFile, string arProp, string filename)
{
   InFile.clear();
   InFile.seekg (0, ios::beg);
   string s;
   getline(InFile,s);
   while( !(s.find(arStringTest) != s.npos) && !InFile.eof() )
   {
      getline(InFile,s);
   }

   if (InFile.eof())
   {
      Mout << arStringTest << " label not found on file " << filename << endl;
      return false;
   }
   else
   {
      if (gIoLevel > I_10)
      {
         Mout << " " << arProp << " read from file " << filename << std::endl;
      }
      return true;
   }
}

/**
* Serach in file for label. If bool aCont=true then start reading from last record
* */
bool SearchInFile2(string aStringTest, ifstream& arIn, string aFilename, string& arSnum, Nb& arVal, In Iopt)
{
   arIn.clear();
   arIn.seekg (0, ios::beg); 
   arSnum = "0"; 
   arVal = C_0;
   // Eduard: next line commented (unused variable)
   //In n_char=aStringTest.size();
   string s;
   bool succes;
   getline(arIn,s);
   //Mout << " string read from file " << aFilename << " is: " << s << endl;
   if (!(aStringTest.find("*")!=aStringTest.npos))aStringTest += "*";
   //while( !(s.find(aStringTest)!=s.npos && (s.substr(s.find(aStringTest)+n_char,1)=="*") && !arIn.eof()))
   while( !(s.find(aStringTest) != s.npos) && !arIn.eof() )
   {
      getline(arIn,s);
      //Mout << " string read from file " << aFilename << " is: " << s << "  aStringTest:" << aStringTest << endl;
   }
   if (arIn.eof())
   {
       succes = false;
       return succes;
   }
   else 
   {
      if (Iopt == I_0)
      {
         istringstream line_s(s);
         line_s >> arSnum;
         succes = true;
         return succes;
      }
      else if (Iopt == I_1)
      {
         succes = true; 
         return succes;
      }
      else if (Iopt == I_2)
      {
         string s_code; 
         istringstream line_s(s);
         line_s >> arSnum >> s_code >> arVal;
         succes = true;
         return succes;
      }
      else
      {
         Mout << " Warning ! Unknownm option in SearchInFile2 " << endl;
         succes = false;
         return succes;
      }
   }
}

/**
 * Get a string out of a number .
 **/
string StringForNb(Nb aNb,In aSize)
{
   ostringstream tmp;
   tmp.setf(ios::scientific);
   tmp.setf(ios::uppercase);
   tmp.setf(ios::showpoint);
   tmp << setw(aSize) << setprecision(aSize-6) << aNb;
   return tmp.str();
}

/**
* Get a string out of a number .
* */
string StringAfterLastSlash(const string& arS)
{
   string s1=arS;
   while(s1.find("/")!= s1.npos) s1.erase(I_0,s1.find("/")+I_1);   // Delete ALL blanks
   Mout << " in:|"<<arS<<"|out:|"<<s1<<endl;
   return s1;
}
/**
* Read gaussian formatted checkpoint file
**/
void ReadFormCheck(string aFileName, string aDir) {
   string name=aDir+"/GAUSS_"+aFileName+".fchk";
   ifstream in(name.c_str());
   name=aDir+"/"+aFileName+"_prop.out";
   // seach inputfile for patterns
   string s,junk1,junk2,junk3;
   In n_prop=I_1;
   while(getline(in,s)) {
      if(s.find("SCF Energy")!=s.npos) {
         Nb scf_energy;
         istringstream iss(s);
         iss >> junk1 >> junk2 >> junk3 >> scf_energy;
         WriteDaltonProp(scf_energy,0,n_prop,"ENERGY",name);
         n_prop++;
      }
      else if(s.find("Dipole Moment")!=s.npos) {   
         getline(in,s);
         istringstream iss(s);
         Nb x_dip,y_dip,z_dip;
         iss >> x_dip >> y_dip >> z_dip;
         WriteDaltonProp(x_dip,1,n_prop,"XDIPLEN",name);
         n_prop++;
         WriteDaltonProp(y_dip,1,n_prop,"YDIPLEN",name);
         n_prop++;
         WriteDaltonProp(z_dip,1,n_prop,"ZDIPLEN",name);
         n_prop++;
      }
   }
   WriteDaltonProp(C_0,666,n_prop,"ALL_DONE",name,"THE_END");
}
/**
* Write dalton property file, i.e. the same format as WRIPRO in dalton
**/
void WriteDaltonProp(Nb aResult,In aType,In aPropNumber,string aLabel,string aOutPut,string aStrType) {
   ofstream out_stream(aOutPut.c_str(),ios_base::app);
   MidasStreamBuf out_buf(out_stream);
   MidasStream out(out_buf);
   string lab=" "+aLabel;
   for(In i=I_0;i<8-aLabel.length();i++) lab+=" ";
   string strtype=" "+aStrType;
   for(In i=I_0;i<12-aStrType.length();i++) strtype+=" ";
   out << setw(5) << aPropNumber << setw(3) << "1" << setw(4) << aType;
   out.setf(ios::scientific);
   midas::stream::ScopedPrecision(16, out);
   out << strtype << aResult << lab << lab << lab << lab;
   out << " " << C_0 << " " << C_0 << " " << C_0;
   if (aLabel.find("ENERGY")!=aLabel.npos)
      out << "   0   1   0" << endl;
   else
      out << "   0   0   0" << endl;
   out_stream.close();
}
/**
* Write dalton derivative file, i.e. the same format as WRIDER in dalton
* Cartesian Coordinates
**/
void WriteDaltonDer(In aDim, MidasVector& aGrdHess,string& aOutPut) {
   ofstream out_stream(aOutPut.c_str(),ios_base::app);
   MidasStreamBuf out_buf(out_stream);
   MidasStream out(out_buf);
   out << aDim << endl;
   out << endl;
   out.setf(ios::scientific);
   midas::stream::ScopedPrecision(16, out);
   // writes the gradient
   for(In i=I_0;i<aDim; i++)
      out << aGrdHess[i] << endl;
   out << endl;
   In i_counter=aDim;
   // writes the hessian
   for(In j=I_0;j<aDim; j++)
   {
      for(In i=I_0;i<aDim; i++)
      {
         out << aGrdHess[i_counter] << endl;
         i_counter++;
      }
   }
   out_stream.close();
}
/**
*  In: two strings
*  out: a vector of strings corresponding to the first string being split at the second string
**/
vector<string> SplitString(const string& aS, const string& aSplit) {
   vector<string> result;
   In i=I_1;
   string work = aS;
   while (i > I_0) {
      i=work.find_first_not_of(aSplit,I_0);
      work.erase(I_0,i);
      i=work.find_first_of(aSplit,I_0);
      if(work.substr(I_0,i).size()!=0)
         result.push_back(work.substr(I_0,i));
      work.erase(I_0,i);
   }
   return result;
}

string SplitString(string& aS, const string& aSplit, In& aIndex) {
   vector<string> result;
   In i=I_1;
   string work = aS;
   while (i > I_0) {
      i=work.find_first_not_of(aSplit,I_0);
      work.erase(I_0,i);
      i=work.find_first_of(aSplit,I_0);
      result.push_back(work.substr(I_0,i));
      work.erase(I_0,i);
   }
   return result[aIndex];
}

string ReverseString(string aS) {
   string result;
   if(aS.length()>I_0) {
      string s=aS.substr(I_0,aS.length()-I_1);
      result=aS.substr(aS.length()-I_1,I_1)+ReverseString(s);
   }
   return result;
}


bool CIEqual(char aCh1, char aCh2)
{
      return toupper((unsigned char)aCh1) == toupper((unsigned char)aCh2);
}

template <>
In ValFromString(string& aStr, bool& aVal)
{
   std::transform(aStr.begin(), aStr.end(), aStr.begin(), (In(*) (In))toupper);
   if (aStr == "TRUE" || aStr == "1")
   {
      aVal = true;
      return 0;
   }
   else if (aStr == "FALSE" || aStr == "0")
   {
      aVal = false;
      return 0;
   }
   else
   {
      return -3;
   }
}

