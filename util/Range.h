/**
************************************************************************
* 
* @file                util/Range.h
*
* Created:             15-01-2013
*
* Author:              Ian Godtliebsen (ian@chem.au.dk)
*
* Short Description:     Class to hold a 'bash-type' range [begin..end;step]
*                      
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef RANGE_H_INCLUDED
#define RANGE_H_INCLUDED

#include<stdlib.h>
#include<tuple>
#include<vector>
#include<iostream>

/**
 *
 **/
template<class T>
class Range: private std::tuple<T,T,T>
{
   public:
      /////
      // ctors
      /////
      Range(T aBegin, T aEnd, T aStep): std::tuple<T,T,T>(aBegin,aEnd,aStep)
      {
         // assert that we have an integer #steps
      }

      Range() = delete;
      Range(const Range&) = default;
      Range& operator=(const Range&) = default;
      
      /////
      //
      /////
      T Begin() const { return std::get<0>(*this); }
      T End() const { return std::get<1>(*this); }
      T Step() const { return std::get<2>(*this); }
      
      ///////
      // expand range to vector
      ///////
      std::vector<T> Expand() const
      {
         size_t size = (End() - Begin())/Step() + 1;
         std::vector<T> range_vec(size);
         auto value = Begin();
         for(size_t i=0; i<range_vec.size(); ++i)
         {
            range_vec[i] = value;
            value += Step();
         }
         return range_vec;
      }
};

/**
 * output range
 **/
template<class T>
std::ostream& operator<<(std::ostream& aOstream, const Range<T>& aRange)
{
   aOstream << "RANGE[" << aRange.Begin()
            << ".." << aRange.End()
            << ";" << aRange.Step()
            << "]";
   return aOstream;
}

#endif /* RANGE_H_INCLUDED */
