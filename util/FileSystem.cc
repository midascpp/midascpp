/**
************************************************************************
* 
* @file                FileSystem.h
*
* Created:             07/06-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Functions for manipulating files.
* 
* Last modified: See git...
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "util/FileSystem.h"

#include <sys/types.h>
#include <sys/stat.h>
// lstat()
// mkdir()
// chmod()
#include <stdio.h>
// remove()
// rename()
// feof()
#include <unistd.h>
// rmdir()
// link() 
// symlink()
// unlink()
// sync()
#include <ftw.h>
// nftw()
#include <dirent.h>
// opendir
// readdir
// closedir
#include <fstream>

#include <errno.h>
// errno
// strerror()

#include <iostream>
#include <cstdlib>
#include <regex>

#include "util/Error.h"
#include "util/Path.h"

namespace midas
{
namespace filesystem
{
namespace detail
{

#define BUFSIZE 2048

/**
 * Implementation of Copy.
 *
 * @param aSourceFile    The source file
 * @param aTargetFile    The target file
 *
 * @return Returns 0 on success, -1 on error.
 **/
int copy_file_impl
   (  const std::string& aSourceFile
   ,  const std::string& aTargetFile
   )
{
   // Open source and target files
   FILE *source, *target;
 
   source = fopen(aSourceFile.c_str(), "r");
 
   if( source == NULL )
   {
      return -1;
   }
 
   target = fopen(aTargetFile.c_str(), "w");
 
   if( target == NULL )
   {
      fclose(source);
      return -1;
   }
   
   // Do the copying
   char buf[BUFSIZE];
   size_t n;
   while( (n = fread(buf, sizeof(char), sizeof(buf), source)) )
   {
      //if( fwrite(buf, sizeof(char), sizeof(buf), target) != n )
      if( fwrite(buf, sizeof(char), n, target) != n )
      {
         break;
      }
   }
   
   // Check for errors
   int status = 0;
   if( ferror(source) )
   {
      std::cout << " ERROR READING " << std::endl;  
      status = -1;
   }
   
   if( ferror(target) )
   {
      std::cout << " ERROR WRITING " << std::endl;
      status = -1;
   }
 
   // Then close files 
   fclose(source);
   fclose(target);

   // Set permission of copied file
   struct stat st;
   int stat_status = stat(aSourceFile.c_str(), &st);
   Chmod(aTargetFile, st.st_mode);
   
   // And return
   return status;
}

/**
 * Functor for Rmdir.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int remove_file_impl
   (  const char* pathname
   ,  const struct stat* statbuf
   ,  int typeflag
   ,  struct FTW* ftwbuf
   )
{
   return remove(pathname);
}

#undef BUFSIZE

// Settings for unix
const struct
{
   char delimeter = '/';
   char extension = '.';
} settings;

} /* namespace detail */

/**
 * Check if a given path exists.
 *
 * @param   aPath  The path to check.
 *
 * @return  Returns true if path exists, false otherwise.
 **/
bool Exists
   (  const std::string& aPath
   )
{
   struct stat s;
   return (lstat(aPath.c_str(), &s) >= 0);
}

/**
 * Check if a given path is a regular file.
 *
 * @param   aPath  The path to check.
 *
 * @return  Returns true if path is a regular file, false otherwise.
 **/
bool IsFile
   (  const std::string& aPath
   )
{
   struct stat s;
   return (lstat(aPath.c_str(), &s) >= 0) && S_ISREG(s.st_mode);
}

/**
 * Check if a given path is a directory.
 *
 * @param   aPath  The path to check.
 *
 * @return  Returns true if path is a directory, false otherwise.
 **/
bool IsDir
   (  const std::string& aPath
   )
{
   struct stat s;
   return (lstat(aPath.c_str(), &s) >= 0) && S_ISDIR(s.st_mode);
}

/**
 * Check if a given path is a symbolic link.
 *
 * @param   aPath  The path to check.
 *
 * @return  Returns true if path is a symnbolic link, false otherwise.
 **/
bool IsSymlink
   (  const std::string& aPath
   )
{
   struct stat s;
   return (lstat(aPath.c_str(), &s) >= 0) && S_ISLNK(s.st_mode);
}

/**
 * Determines if a file is executable by the user.
 *
 * @param aPath The filename.
 *
 * @return Returns true if the file is executable and false if it is not.
 **/
bool IsExecutable
   (  const std::string& aPath
   )
{
   struct stat s;
   return (lstat(aPath.c_str(), &s) >= 0) && ((s.st_mode & S_IXUSR) == S_IXUSR);
}

/**
 * Get the permissions associated to a file.
 *
 * @param aPath The filename.
 *
 * @return Returns permissions or 0 if the aPath doesn't exist.
**/
mode_t GetFilePermissions
   (  const std::string& aPath
   )
{
   struct stat s;
   if (lstat(aPath.c_str(), &s) >= 0)
   {
      return s.st_mode;
   }
   else
   {
      return 0;
   }
}

/**
 * @param aPath
 *    The Path to check.
 * @return
 *    The size in bytes if aPath is a file, or length of the pathname (without
 *    the terminating null byte) if aPath is a symlink.
 *    Returns 0 if aPath doesn't exist.
 **/
off_t Size
   (  const std::string& aPath
   )
{
   struct stat s;
   if (lstat(aPath.c_str(), &s) >= 0)
   {
      return s.st_size;
   }
   else
   {
      return 0;
   }
}


/**
 * Copy a file.
 *
 * @param aSourceFile    The source file
 * @param aTargetFile    The target file
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Copy
   (  const std::string& aSourceFile
   ,  const std::string& aTargetFile
   )
{
   return detail::copy_file_impl(aSourceFile, aTargetFile);
}

/**
 * Rename/move a file.
 *
 * @param aOldPath    The old pathname 
 * @param aNewPath    The new pathname
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Rename
   (  const std::string& aOldPath
   ,  const std::string& aNewPath
   )
{
   return rename(aOldPath.c_str(), aNewPath.c_str());
}

/**
 * Remove a file, empty directory, or unlink a link.
 *
 * @param aPathName    The pathname of file.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Remove
   (  const std::string& aPathName
   )
{
   return remove(aPathName.c_str());
}

/**
 * Touch a file.
 *
 * @param aPathName   The pathname of file.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Touch
   (  const std::string& aPathName 
   )
{
   FILE *fp = fopen(aPathName.c_str(), "ab+");
   if(fp)
   {
      fclose(fp);
      return 0;
   }
   else
   {
      return -1;
   }
}

/**
 * Create a directory with pathname. Pathname can be either absolute or relative.
 *
 * @param aPathName  The pathname.
 * @param aRecursive Create directories recursively.
 * @param aMode      Specify permissions for directory.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Mkdir
   (  const std::string& aPathName
   ,  bool aRecursive
   ,  mode_t aMode
   ) 
{
   int status = 0;

   if (aRecursive)
   {
      auto dirname = path::DirName(aPathName);
      if (!dirname.empty())
      {
         if (!Exists(dirname))
         { 
            status = Mkdir(dirname, aRecursive, aMode);

            // The directory dirname did not exist when we did the check just before calling Mkdir.
            // However, another thread might have created it in the meantime, so that Mkdir fails.
            // In this case we ignore the non-zero status. This should not silence other errors.
            if (status != 0 && errno == EEXIST)
            { 
               status = 0;
            } 

         } 
      } 
   }
   
   if (status == 0)
   {
      status = mkdir(aPathName.c_str(), aMode);
   }

   return status;
}

/**
 * Remove a directory.
 *
 * @param aPath         Directory path.
 * @param aRecursive    Remove recursively. If false, will only delete an empty directory.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Rmdir
   (  const std::string& aPath
   ,  bool aRecursive
   )
{
   if(aRecursive)
   {
      return nftw(aPath.c_str(), detail::remove_file_impl, 30, FTW_DEPTH | FTW_MOUNT | FTW_PHYS);
   }
   else
   {
      return rmdir(aPath.c_str());
   }
}

/**
 * Chmod a file.
 *
 * @param aPathName   The filename.
 * @param aMode       The mode to set.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Chmod
   (  const std::string& aPathName
   ,  mode_t aMode
   )
{
   return chmod(aPathName.c_str(), aMode);
}

/**
 * Synchronize filesystem buffers and underlying filesystem.
 **/
void Sync
   (
   )
{
   sync();
}

/**
 * Create a hard link. This is usually no what you want to do.
 * Use instead Symlink() function to create symbolic links.
 * Links created can be unlinked again with Unlink().
 * 
 * @param aOldPath   The old filepath.
 * @param aNewPath   The new filepath.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Link
   (  const std::string& aOldPath
   ,  const std::string& aNewPath
   )
{
   return link(aOldPath.c_str(), aNewPath.c_str());
}

/**
 * Create a symbolic to file.
 * Links created can be unlinked again with Unlink().
 * 
 * @param aFilePath   The filepath.
 * @param aLinkPath   The link path.
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Symlink
   (  const std::string& aFilePath
   ,  const std::string& aLinkPath
   )
{
   return symlink(aFilePath.c_str(), aLinkPath.c_str());
}

/**
 * Unlink either a symbolic or hard link created with either Link() or Symlink().
 *  
 * @param aLinkPath
 *
 * @return Returns 0 on success, -1 on error.
 **/
int Unlink
   (  const std::string& aLinkPath
   )
{
   return unlink(aLinkPath.c_str());
}

/**
 * Grep in file for regex and return a vector of all matches.
 *
 * @param aFilePath    The file to search.
 * @param aRegex       The regex to search for.
 *
 * @return Returns a vector of all matches.
 **/
std::vector<std::string> Grep
   (  const std::string& aFilePath
   ,  const std::regex& aRegex
   )
{
   std::vector<std::string> found;

   std::ifstream inpfile(aFilePath);
   std::string str;

   while(std::getline(inpfile, str))
   {
      if(std::regex_match(str, aRegex))
      {
         found.emplace_back(str);
      }
   }

   return found;
}

/**
 * Search a directory for files matching a regex.
 *
 * @param aDirPath   The directory to search.
 * @param aRegex     The regex to match file names against.
 *
 * @return           Returns vector of strings with filenames matching the regex.
 **/
std::vector<std::string> SearchDir
   (  const std::string& aDirPath
   ,  const std::regex&  aRegex
   )
{
   std::vector<std::string> result;

   struct dirent *pDirent;
   DIR *pDir;
   
   // Try to open directory
   if ( (pDir = opendir(aDirPath.c_str())) != NULL) 
   {
      // Loop through files and check against regex
      while ((pDirent = readdir(pDir)) != NULL) 
      {
         std::string filename(pDirent->d_name);
         if(std::regex_search(filename, aRegex))
         {
            result.emplace_back(std::move(filename));
         }
      }
   
      // close directory again
      closedir (pDir);
   }
   
   return result;
}

/**
 * Check 'errno' and if set report the error and print a telling message.
 * Will throw MIDASERROR if errno is set.
 *
 * @return        Return '0' on success, and '-1' on failure (will throw MIDASERROR, so no return on failure).
 **/
int ErrorCheck
   (  const std::string& aMessage
   ,  bool aHardError
   )
{
   if(errno != 0)
   {
      std::string message = aMessage + " failed with '" + strerror(errno) + "'.";
      if (  aHardError  )
      {
         MIDASERROR(message);
      }
      else
      {
         MidasWarning(message);
      }
      return -1;
   }
   return 0;
}

} /* namespace filesystem */
} /* namespace midas */
