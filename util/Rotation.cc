#include "Rotation.h"


namespace midas
{
namespace util
{

double conversion_degrees_to_radians = C_PI / 180.0;

/**
 * Convert angle to radians.
 *
 * @param aAngle   The angle.
 * @param aUnit    The unit of aAngle.
 *
 * return   Returns converted angle.
 **/
double ConvertAngle
   (  double aAngle
   ,  Unit aUnit
   )
{
   switch(aUnit)
   {
      case Unit::Degrees:
         aAngle *= conversion_degrees_to_radians;
         break;
      case Unit::Radians:
      default:
         break;
   }

   return aAngle;
}

/**
 * Create 3D rotation matrix that rotates aAngle around the X-axis.
 *
 * @param aAngle   The angle.
 * @param aUnit    The unit of aAngle.
 *
 * return   Returns 3D rotation matrix that rotates the given angle.
 **/
MidasMatrix CreateRotationMatrixX
   (  double aAngle
   ,  Unit   aUnit
   )
{
   aAngle = ConvertAngle(aAngle, aUnit);

   MidasMatrix rot_matrix{I_3, I_3};

   auto cos_a = std::cos(aAngle);
   auto sin_a = std::sin(aAngle);
   
   rot_matrix[0][0] =  C_1; rot_matrix[0][1] =    C_0; rot_matrix[0][2] =    C_0;
   rot_matrix[1][0] =  C_0; rot_matrix[1][1] =  cos_a; rot_matrix[1][2] =  sin_a;
   rot_matrix[2][0] =  C_0; rot_matrix[2][1] = -sin_a; rot_matrix[2][2] =  cos_a;

   return rot_matrix;
}

/**
 * Create 3D rotation matrix that rotates aAngle around the Y-axis.
 *
 * @param aAngle   The angle.
 * @param aUnit    The unit of aAngle.
 *
 * return   Returns 3D rotation matrix that rotates the given angle.
 **/
MidasMatrix CreateRotationMatrixY
   (  double aAngle
   ,  Unit   aUnit
   )
{
   aAngle = ConvertAngle(aAngle, aUnit);
   
   MidasMatrix rot_matrix{I_3, I_3};

   auto cos_b = std::cos(aAngle);
   auto sin_b = std::sin(aAngle);
   
   rot_matrix[0][0] =  cos_b; rot_matrix[0][1] =  C_0; rot_matrix[0][2] = -sin_b;
   rot_matrix[1][0] =    C_0; rot_matrix[1][1] =  C_1; rot_matrix[1][2] =    C_0;
   rot_matrix[2][0] =  sin_b; rot_matrix[2][1] =  C_0; rot_matrix[2][2] =  cos_b;
   
   return rot_matrix;
}

/**
 * Create 3D rotation matrix that rotates aAngle around the Z-axis.
 *
 * @param aAngle   The angle.
 * @param aUnit    The unit of aAngle.
 *
 * return   Returns 3D rotation matrix that rotates the given angle.
 **/
MidasMatrix CreateRotationMatrixZ
   (  double aAngle
   ,  Unit   aUnit
   )
{
   aAngle = ConvertAngle(aAngle, aUnit);

   MidasMatrix rot_matrix{I_3, I_3};

   auto cos_c = std::cos(aAngle);
   auto sin_c = std::sin(aAngle);

   rot_matrix[0][0] =  cos_c; rot_matrix[0][1] =  sin_c; rot_matrix[0][2] =  C_0;
   rot_matrix[1][0] = -sin_c; rot_matrix[1][1] =  cos_c; rot_matrix[1][2] =  C_0;
   rot_matrix[2][0] =    C_0; rot_matrix[2][1] =    C_0; rot_matrix[2][2] =  C_1;

   return rot_matrix;
}

} /* namespace util */
} /* namespace midas */
