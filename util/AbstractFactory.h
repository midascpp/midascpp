/**
************************************************************************
* 
* @file                AbstractFactory.h
*
* Created:             07-02-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   A generic abstract factory class.
* 
* Last modified: Tue. Feb 07 2012
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ABSTRACTFACTORY_H_INCLUDED
#define ABSTRACTFACTORY_H_INCLUDED

#include <string>
#include <unordered_map>
#include <memory>
#include <exception>
#include <stdexcept>
#include <vector>

#include <iostream>

#include "Singleton.h"

/***********
 *
 * Abstract factory
 *
 ***********/
template<class BaseType, typename IDKey = std::string>
class AbstractFactory;

//
// Overload for BaseType* such that BaseType can be an abstract base class
//
template<class BaseType, typename IDKey, class... Ts>
class AbstractFactory<BaseType*(Ts...),IDKey> 
   :  public Singleton<AbstractFactory<BaseType*(Ts...), IDKey> >
{
   // Be-friend underlying Singleton class.
   friend class Singleton<AbstractFactory>;

   private:
      typedef std::unique_ptr<BaseType>(*typeCreator)(Ts...);
      
      //! Default constructor.
      AbstractFactory() = default;

      //! Default destructor.
      ~AbstractFactory() = default;
      
      //!@{
      //! Delete copy constructor and copy assignment.
      AbstractFactory(const AbstractFactory&)            = delete;
      AbstractFactory& operator=(const AbstractFactory&) = delete;
      //!@}
      
      //! Implementation of instance creation function.
      template<typename... Args>
      std::unique_ptr<BaseType> create_impl(const IDKey& key, Args&&... args) 
      {
         try
         {
            return (m_register_types.at(key))(std::forward<Args>(args)...);
         }
         catch(const std::out_of_range& e)
         {
            return {nullptr};
         }
      }
      
      //! Implementation of check_key function
      bool check_key_impl(const IDKey& key)
      {
         return m_register_types.find(key) != m_register_types.end();
      }

      //! Implementation of get_registered_keys.
      std::vector<IDKey> get_registered_keys_impl() const
      {
         std::vector<IDKey> keys;
         for(const auto& key_val_pair: m_register_types)
         {
            keys.emplace_back(key_val_pair.first);
         }
         return keys;
      }

   public:
      /////
      // Register class with factory
      /////
      void registerNewClass(const IDKey& key, const typeCreator& creator)
      {
         m_register_types.insert({key, creator});
      };
      
      /////
      // create instance 
      /////
      template<typename... Args>
      static std::unique_ptr<BaseType> create(const IDKey& key, Args&&... args) 
      {
         return Singleton<AbstractFactory>::instance().create_impl(key, std::forward<Args>(args)...);
      }
      
      /////
      // Check if key has been registered
      /////
      static bool check_key(const IDKey& key)
      {
         return Singleton<AbstractFactory>::instance().check_key_impl(key);
      }

      //////
      //
      //////
      static std::vector<IDKey> get_registered_keys()
      {
         return Singleton<AbstractFactory>::instance().get_registered_keys_impl();
      }

   private:
      //! Map of registered factory functions.
      std::unordered_map<IDKey, typeCreator> m_register_types;
};

/***********
 *
 * Abstract factory registration
 *
 ***********/
template<class BaseType, class RealType, typename IDKey = std::string>
class AbstractFactoryRegistration;

//
// Overload for BaseType* such that BaseType can be an abstract base class
//
template<class BaseType, class RealType, typename IDKey, class... Ts>
class AbstractFactoryRegistration<BaseType*(Ts...),RealType,IDKey>
{  
   //! Deleted default constructor.
   AbstractFactoryRegistration() = delete;

   //!@{
   //! Deleted copy constructor and copy assignment.
   AbstractFactoryRegistration(const AbstractFactoryRegistration&)            = delete;
   AbstractFactoryRegistration& operator=(const AbstractFactoryRegistration&) = delete;
   //!@}

   public:
      //! Constructor.
      AbstractFactoryRegistration(const IDKey& key)
      {
         AbstractFactory<BaseType*(Ts...),IDKey>::instance().registerNewClass(key, &instancier);
      };
      
      //! Default destructor.
      ~AbstractFactoryRegistration() = default;
      
      /////
      // register class with factory
      /////
      static void registerMe(const IDKey& key)
      {
         AbstractFactory<BaseType(Ts...),IDKey>::instance().registerNewClass(key, &instancier);
      };

      /////
      // create instance
      /////
      static std::unique_ptr<BaseType> instancier(Ts... ts)
      {
         return std::unique_ptr<BaseType>(new RealType(ts...));
      };
};

#endif /* ABSTRACTFACTORY_H_INCLUDED */
