/*
************************************************************************
*
* @file                 IsDetected.h
*
* Created:              22-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Classes for detecting functions.
*                       This is pretty much taken from std::experimental::is_detected
*                       and will probably become obsolete at some point.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef ISDETECTED_H_INCLUDED
#define ISDETECTED_H_INCLUDED

#include <type_traits>

namespace midas::util
{

namespace detail
{

/**
 * Struct for indicating detection failure
 **/
struct NoneSuch
{
   NoneSuch() = delete;
   ~NoneSuch() = delete;
   NoneSuch(const NoneSuch&) = delete;
   void operator=(const NoneSuch&) = delete;
};

/**
 * Detector class
 **/
template
   <  typename Default
   ,  typename AlwaysVoid
   ,  template<typename...> typename Op
   ,  typename... Args
   >
struct Detector
{
   using value_t = std::false_type;
   using type = Default;
};

/**
 * Specialization for successful detections
 **/
template
   <  typename Default
   ,  template<typename...> typename Op
   ,  typename... Args
   >
struct Detector
   <  Default
   ,  std::void_t<Op<Args...>>   // This only works if Op<Args...> exists. Otherwise, we fall back to default (false_type).
   ,  Op
   ,  Args...
   >
{
   using value_t = std::true_type;
   using type = Op<Args...>;
};

} /* namespace detail */

/**
 * IsDetected
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
using IsDetected = typename detail::Detector<detail::NoneSuch, void, Op, Args...>::value_t;

/**
 * Is Op<Args...> detected. True or false?
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
inline constexpr bool IsDetectedV = IsDetected<Op, Args...>::value;

/**
 * Detected type
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
using DetectedT = typename detail::Detector<detail::NoneSuch, void, Op, Args...>::type;

} /* namespace midas::util */

#endif /* ISDETECTED_H_INCLUDED */
