#ifndef READ_WRITE_BINARY_H_INCLUDED
#define READ_WRITE_BINARY_H_INCLUDED

#include<iostream>

template <class T>
std::istream& read_binary(T& thing, std::istream& input)
{
   return input.read( reinterpret_cast<char*>(&thing), sizeof(T) );
}

template <class T>
std::istream& read_binary_array(T* thing, const size_t num_elem, std::istream& input)
{
   return input.read( reinterpret_cast<char*>(thing), num_elem*sizeof(T) );
}

template <class T>
std::ostream& write_binary(const T& thing, std::ostream& output)
{
   return output.write( reinterpret_cast<const char*>(&thing), sizeof(T) );
}

template <class T>
std::ostream& write_binary_array(const T* thing, const size_t num_elem, std::ostream& output)
{
   return output.write( reinterpret_cast<const char*>(thing), num_elem*sizeof(T) );
}

#endif /* GET_WRITE_BINARY_H_INCLUDED */
