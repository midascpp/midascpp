/**
************************************************************************
* 
* @file                Fractions.h
*
* Created:             18-03-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Tools displaying fractions.
* 
* Last modified: Wed Nov 19, 2008  05:35PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FRACTIONS_H
#define FRACTIONS_H

#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

void FracFloat(Nb aF, In& aNom, In& aDen);
///< Converts aF to fraction. Puts nominator / denominator in aNom / aDen.

void FracFloat(Nb aF, std::string& aStr);
///< Converts aF to fraction. Put is aStr as "nom/den".

void FracFloatLatex(Nb aF, std::string& aStr, bool aSign=false);
///< Convert aF to fraction. Save result as Latex code in aStr.

#endif
