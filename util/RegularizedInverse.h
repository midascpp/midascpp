/*
************************************************************************
*
* @file                 RegularizedInverse.h
*
* Created:              09-06-2020
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Methods for constructing regularized inverses of matrices
*                       useful e.g. in time-dependent calculations (MCTDH and TDMVCC)
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef REGULARIZEDINVERSE_H_INCLUDED
#define REGULARIZEDINVERSE_H_INCLUDED

#include "mmv/MidasMatrix.h"
#include "lapack_interface/GESDD.h"
#include "lapack_interface/GESVD.h"
#include "util/Math.h"

namespace midas::math
{

/**
 * Types of inverses
 **/
struct inverse
{
   inline static struct svd_tikhonov_type {}       svd_tikhonov;  // Add s_max*epsilon to all singular values before inverting.
   inline static struct svd_type          {}       svd;           // Set all singular values to s_i = max(s_max*epsilon, s_i) before inverting
   inline static struct xsvd_type         {}       xsvd;          // Set all singular values to s_i = s_i + epsilon*exp(-s_i/epsilon) before inverting
//   inline static struct xevd_type      {}       xevd;          // Set all eigenvalues to e_i = e_i + epsilon*exp(-e_i/epsilon) before inverting (NB: requires symmetric, positive definite matrix).
};

namespace detail
{

/**
 * Wrapper for computing SVD
 *
 * @param aMatrix
 * @param aType
 * @param aVerbose
 * @return
 *    Svd_struct
 **/
template
   <  typename T
   >
auto ComputeSvd
   (  const GeneralMidasMatrix<T>& aMatrix
   ,  char aType = 'A'
   ,  bool aVerbose = false
   )
{
   auto svd = GESDD(aMatrix, aType);
   if (  svd.info != 0
      )
   {
      MidasWarning("GESDD failed with info = " + std::to_string(svd.info) + ". Try GESVD!");
      svd = GESVD(aMatrix, aType);
   }

   // Print singular values
   if (  aVerbose
      )
   {
      Mout  << " Singular values:  ";
      auto n = svd.Min();
      for(In i=I_0; i<n; ++i)
      {
         Mout  << svd.s[i] << "   ";
      }
      Mout << std::endl;
   }

   return svd;
}

/**
 * Check matrix dimensions
 *
 * @param aMatrix
 **/
template
   <  typename T
   >
void CheckMatrix
   (  const GeneralMidasMatrix<T>& aMatrix
   )
{
   if (  aMatrix.Nrows() == I_0
      )
   {
      MIDASERROR("Cannot compute regularized inverse of matrix with dimension 0.");
   }
   if (  !aMatrix.IsSquare()
      )
   {
      MIDASERROR("Can only compute regularized inverse of square matrices!");
   }
}

/**
 * Construct inverse matrix from SVD.
 * @param aSvd       M = U Sigma V^H
 * @param aF         Function to regularize singluar values
 * @return
 *    M^{-1} = V aF(Sigma)^{-1} U^H
 **/
template
   <  typename T
   ,  typename F
   >
auto ConstructInverse
   (  const SVD_struct<T>& aSvd
   ,  F&& aF
   )
{
   using mat_t = GeneralMidasMatrix<T>;

   const auto& svd = aSvd;

   auto n = svd.n;
   const auto& u = svd.u;
   const auto& vt = svd.vt;
   const auto& s = svd.s;

   mat_t inv(n,n);
   inv.Zero();
   
   for(In icol=I_0; icol<n; ++icol)
   {
      for(In irow=I_0; irow<n; ++irow)
      {
         for(In isv=I_0; isv<n; ++isv)
         {
            inv[irow][icol] += midas::math::Conj(vt[isv + n*irow] * u[icol + n*isv]) / aF(s[isv]);
         }
      }
   }

   return inv;
}

}; /* namespace detail */

/**
 * SVD Tikhonov inverse
 *
 * M = U Sigma V^H
 *
 * M^{-1}_reg = V (Sigma_reg)^{-1} U^H
 * with
 * [Sigma_reg]_i = s_{reg,i} = s_i + s_max*epsilon
 *
 * @param aType
 * @param aMatrix
 * @param aEpsilon
 * @param aVerbose
 * @return
 *    Inverse matrix
 **/
template
   <  typename T
   >
auto RegularizedInverse
   (  inverse::svd_tikhonov_type aType
   ,  const GeneralMidasMatrix<T>& aMatrix
   ,  midas::type_traits::RealTypeT<T> aEpsilon
   ,  bool aVerbose = false
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   detail::CheckMatrix(aMatrix);
   auto svd = detail::ComputeSvd(aMatrix, 'A', aVerbose);
   auto smax = svd.s[0];
   return detail::ConstructInverse
            (  svd
            ,  [aEpsilon, smax](real_t s) -> real_t
                  {
                     return s + smax*aEpsilon;
                  }
            );
}
/**
 * SVD inverse
 *
 * M = U Sigma V^H
 *
 * M^{-1}_reg = V (Sigma_reg)^{-1} U^H
 * with
 * [Sigma_reg]_i = s_{reg,i} = max(s_max*epsilon, s_i)
 *
 * @param aType
 * @param aMatrix
 * @param aEpsilon
 * @param aVerbose
 * @return
 *    Inverse matrix
 **/
template
   <  typename T
   >
auto RegularizedInverse
   (  inverse::svd_type aType
   ,  const GeneralMidasMatrix<T>& aMatrix
   ,  midas::type_traits::RealTypeT<T> aEpsilon
   ,  bool aVerbose = false
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   detail::CheckMatrix(aMatrix);
   auto svd = detail::ComputeSvd(aMatrix, 'A', aVerbose);
   auto smax = svd.s[0];
   return detail::ConstructInverse
            (  svd
            ,  [aEpsilon, smax](real_t s) -> real_t
                  {
                     return std::max(s, smax*aEpsilon); 
                  }
            );
}

/**
 * XSVD inverse
 *
 * M = U Sigma V^H
 *
 * M^{-1}_reg = V (Sigma_reg)^{-1} U^H
 * with
 * [Sigma_reg]_i = s_{reg,i} = s_i + epsilon*exp(-s_i / epsilon)
 *
 * @param aType
 * @param aMatrix
 * @param aEpsilon
 * @param aVerbose
 * @return
 *    Inverse matrix
 **/
template
   <  typename T
   >
auto RegularizedInverse
   (  inverse::xsvd_type aType
   ,  const GeneralMidasMatrix<T>& aMatrix
   ,  midas::type_traits::RealTypeT<T> aEpsilon
   ,  bool aVerbose = false
   )
{
   using real_t = midas::type_traits::RealTypeT<T>;
   detail::CheckMatrix(aMatrix);
   auto svd = detail::ComputeSvd(aMatrix, 'A', aVerbose);
   return detail::ConstructInverse
            (  svd
            ,  [aEpsilon](real_t s) -> real_t
                  {
                     return s + aEpsilon*std::exp(-s/aEpsilon);
                  }
            );
}

}; /* namespace midas::math */

#endif /* REGULARIZEDINVERSE_H_INCLUDED */
