/**
 *******************************************************************************
 * 
 * @file    Math.h
 * @date    07-09-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Simple math functions.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

#include <type_traits>
#include "util/NumericLimits.h"
#include "util/type_traits/Complex.h"
#include "util/IsDetected.h"
#include "inc_gen/Const.h"

#ifdef ENABLE_BOOST
#include <boost/math/special_functions/expint.hpp>
#elif ENABLE_GSL
#include <gsl/gsl_sf_expint.h>
#endif

namespace midas
{
namespace util
{

/***************************************************************************//**
 * @brief
 *    Returns greatest common divisor of two numbers of integral type.
 *
 * Uses Euclid's algorithm (fastest solution). In its recursive formulation the
 * algorithm is based on `gcd(m, n) = gcd(n, m % n)` which terminates at some
 * step `gcd(a, 0) = a`. Here it's implemented iteratively.
 * We define `gcd(0, 0) = 0`.
 * It also works for negative integers, but the return value is always taken to
 * be positive.
 *
 * @param[in] m
 *    Integer (one iteration faster if `|m| > |n|`).
 * @param[in] n
 *    Integer (one iteration faster if `|m| > |n|`).
 *
 * @return
 *    Greatest common divisor (non-negative).
 ******************************************************************************/
template<class IntType>
IntType GreatestCommonDivisor
   (  IntType m
   ,  IntType n
   )
{
   // Convert to abs. vals, since result of modulo (%) with neg. numbers may be
   // undef.
   m = std::abs(m);
   n = std::abs(n);

   // Euclid's algorithm. Iteratively, not recursively, to avoid initial
   // conversions again.
   // The cases where one is zero (should return the non-zero) or both are zero
   // (should return zero) are automatically handled.
   IntType t(0);
   while (n != 0)
   {
      t = n;
      n = m % n;
      m = t;
   }

   return m;
}

/**
 * Calculate exponential integral
 **/
template
   <  class T
   >
inline T ExpInt
   (  T x
   )
{
   static_assert(std::is_floating_point_v<T>, "Only works with floating-point numbers!");

   // If boost is enabled, we use that as default
#ifdef ENABLE_BOOST
   return boost::math::expint(x);
   // Else, if GSL is enabled, we use that
#elif ENABLE_GSL
   return static_cast<T>(gsl_sf_expint_Ei(static_cast<double>(x)));
   // As fallback, we use the standard library, but this requires c++17 and a standard library that implements it
#else
   return std::expint(x);
#endif
}

} /* namespace util */
} /* namespace midas */

namespace midas::math
{

/**
 * Calculate unnormalized sinc function defined as sinc(x) = sin(x)/x for x != 0 and sinc(x) = 1 for x = 0.
 *
 * This is done using Taylor approximation when x is small using the number of terms corresponding to roots of 
 * the numeric epsilon.
 *
 * @param x
 * @return
 *    sinc(x)
 **/
template
   <  class T
   >
T sinc
   (  T x
   )
{
   static_assert(std::is_floating_point<T>::value, "sinc only takes floating-point values!");

   T eps = midas::util::NumericPrecisionInfo<T>::Epsilon();
   T root_eps = midas::util::NumericPrecisionInfo<T>::EpsilonSquareRoot();
   T root4_eps = midas::util::NumericPrecisionInfo<T>::EpsilonFourthRoot();

   if (  std::abs(x) >= std::abs(root4_eps)
      )
   {
      return std::sin(x)/x;
   }
   else
   {
      // Taylor approximation
      auto result = static_cast<T>(1.);

      if (  std::abs(x) >= std::abs(eps)
         )
      {
         T x2 = x*x;

         // Subtract first term
         result -= x2/static_cast<T>(6.);

         if (  std::abs(x) >= std::abs(root_eps)
            )
         {
            // Add next term
            result += (x2*x2) / static_cast<T>(120.);
         }
      }

      return result;
   }
}

// Complex conjugate for complex numbers
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr T Conj
   (  const T& aNumber
   )
{
   return std::conj(aNumber);
}

// Complex conjugate for real numbers. Return real number.
template
   <  typename T
   ,  std::enable_if_t<!midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline constexpr T Conj
   (  const T& aNumber
   )
{
   return aNumber;
}

/**
 * Routines for Gauss-Legendre abscissa and weghts, taken from
 * Numerical Recipes in C++ pag. 157, (interval -1, 1)
 *
 * @param arX     Vector of abscissa
 * @param arW     Vector of weights
 * @param aX1     Beginning of interval
 * @param aX2     End of interval
 * @param aAcc    Accuracy relative to numeric epsilon
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   >
void GauLeg
   (  VECTOR<T>& arX
   ,  VECTOR<T>& arW
   ,  T aX1
   ,  T aX2
   ,  unsigned aAcc = 1
   )
{
   static_assert(std::is_floating_point_v<T>, "GauLeg only works with floating-point!");

   In n=arX.size();
   MidasAssert(arW.size() == n, "Different sizes of abscissa and weights for GauLeg. Cannot determine order!");

   T x1 = aX1;
   T x2 = aX2;
   const T eps = aAcc * midas::util::NumericPrecisionInfo<T>::Epsilon();
   In i, m, j;
   T z1, z, xm, xl, pp, p3, p2, p1;
   m=(n+I_1)/I_2;
   xm=C_I_2*(x2+x1);
   xl=C_I_2*(x2-x1);

   for(i=I_0; i<m; ++i)
   {
      z=cos(C_PI*(i+ 0.75)/(n+C_I_2));

      do
      {
         p1=C_1;
         p2=C_0; 
         for (j=I_0; j<n; ++j)
         {
            p3=p2;
            p2=p1;
            p1=((C_2*j + C_1)*z*p2-j*p3)/(j+I_1);
         }
         pp= n*(z*p1-p2)/(z*z-C_1);
         z1=z;
         z=z1-p1/pp;
      }
      while (  fabs(z-z1)> eps
            );

      arX[i] = xm-xl*z;
      arX[n-I_1-i] = xm + xl*z;
      arW[i] = C_2*xl/((C_1-z*z)*pp*pp);
      arW[n-I_1-i] = arW[i];
   }
}


} /* namespace midas::math */

#endif/*MATH_H_INCLUDED*/
