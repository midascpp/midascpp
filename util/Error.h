/**
************************************************************************
* 
* @file                Error.h 
*
* Created:             07-02-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Functions for throwing Midas Errors, that will log all sorts of information
* 
* Last modified: Tue. Feb 07 2012
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ERROR_H_INCLUDED
#define ERROR_H_INCLUDED

#include <iostream>
#include <chrono>

#include "inc_gen/Warnings.h"
#include "util/MidasStream.h"
#include "util/stream/HeaderInserter.h"
#include "libmda/util/stacktrace.h"
#include "libmda/util/output_call_addr.h"

extern MidasStream Mout;

enum class ErrorCtrl : int { SETKILL, SETNUMBER, RESETNUMBER, MUTESTDOUT, UNMUTESTDOUT, MUTEMOUT, UNMUTEMOUT };

//! Add an error info message.
void AddErrorInfo
   (  const std::string& label
   ,  const std::string& message
   );

//! Print Midas error message.
void PrintError
   (  std::ostream& aOs
   ,  std::string aError
   ,  const char* aFilename
   ,  unsigned aLinenumber
   ,  void* aReturnAddr
   );

//! Output Midas error message and terminate execution of program.
void Error
   (  std::string aError
   ,  const char* aFilename=nullptr
   ,  unsigned int aLinenumber=0
   ,  void* aReturnAddr=nullptr
   ,  const std::time_t& aErrorTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())
   );

/**
 *
 **/
inline std::string CatenateStringWithNewline()
{
   return "";
}

/**
 *
 **/
template<class T>
inline std::string CatenateStringWithNewline(T&& t)
{
   return std::string(t);
}

/**
 *
 **/
template<class T, class... Ts>
inline std::string CatenateStringWithNewline(T&& t, Ts&&... ts)
{
   return std::string(t) + "\n" + CatenateStringWithNewline(std::forward<Ts>(ts)...);
}

/**
 *
 **/
inline void MidasAssertImpl(bool test, const std::string& message, const char* file, unsigned line, void* return_addr)
{
   if(!test)
   {
      Error(message,file,line,return_addr);
   }
}

/**
 * Control error control flags.
 **/
void ErrorControl(ErrorCtrl flag);

/**
 * Get last error number.
 **/
int ErrorNumber();

/**
 * macro for calling Error function with file and line number
 **/
/*
#define MIDASERROR(a) \
   Error((a),__FILE__,__LINE__)
*/
#define MIDASERROR(...) \
   Error(CatenateStringWithNewline(__VA_ARGS__),__FILE__,__LINE__,__builtin_return_address(0))

#define MidasAssert(a,...) \
   MidasAssertImpl(a,CatenateStringWithNewline("{ASSERTION FAILED}",__VA_ARGS__),__FILE__,__LINE__,__builtin_return_address(0))

#endif /* ERROR_H_INCLUDED */
