#ifndef MIDAS_UTIL_OS_H_INCLUDED
#define MIDAS_UTIL_OS_H_INCLUDED

#include <string>

namespace midas
{
namespace os
{

//! Change the directory where the program is working/running.
int Chdir(const std::string& aDir);

//! Get the current working directory.
std::string Getcwd();

//! Get hostname of machine.
std::string Gethostname();

} /* namespace os */
} /* namespace midas */

#endif /* MIDAS_UTIL_OS_H_INCLUDED */
