/**
************************************************************************
* 
* @file                MakeTables.h 
*
* Created:             11-03-2002
*
* Author:              Ove Christiansen (Ove.Christiansen@teokem.lu.se)
*
* Short Description:   Implementing auto making of *tex files and *.ps files (by gnuplot)
* 
* Last modified: Mon Oct 30, 2006  09:27AM
*
* Copyright:
*
* Ove Christiansen, Lunds University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_UTIL_MAKETABLES_H_INCLUDED
#define MIDAS_UTIL_MAKETABLES_H_INCLUDED

// std headers
#include<string>
#include<iostream>
#include<vector>
#include<algorithm>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "util/Io.h"
//#include "util/Caller.h"
#include "util/MakeTables.h"
#include "util/MidasStream.h"

// using declarations
using std::string;
using std::ofstream;

inline Nb LogNum(Nb aX)
{
   Nb xnum = fabs(aX);
   // get sign, inelegant, studid in general case if minus comes from both x and i: -xE-i
   //if (xnum == C_0) return C_0;
   //Nb xsign = aX/xnum;
   //return xsign*log(xnum);
   return log10(xnum);
}
/**
* Make first part of tex table
* */
template<class STREAM>
void TexTableBeginDocument(STREAM& aOut)
{
   aOut << "\\documentclass[preprint,prb,aps,floatfix]{revtex4}" << endl;
   aOut << "\\usepackage{amsfonts}" << endl;
   aOut << "\\pagestyle{plain}" << endl;
   aOut << " " << endl;
   aOut << "\\begin{document}" << endl;
   aOut << " " << endl;
}
/**
* Make first part of tex table
* */
template<class STREAM>
void TexTableBegin(STREAM& aOut,string aCaption, In aNtries)
{
   aOut << "{\\squeezetable" << endl;
   aOut << "\\begin{table}[t]" << endl;
   aOut << "\\caption{" << endl;
   aOut << aCaption << endl;
   aOut << "}" << endl;
   aOut << "\\begin{tabular}" << endl;
   aOut << "{" << endl;
   aOut << "l";
   for (In i=I_0;i<aNtries-I_1;i++) aOut << "c";
   aOut << endl;
   aOut << "}" << endl;
   aOut << "\\hline\\hline" << endl;
   //aOut << "}" << endl;
}
/**
* Make last part of tex table
* */
template<class STREAM>
void TexTableEnd(STREAM& aOut,string aFooter)
{
   aOut << "\\hline\\hline" <<endl;
   aOut << "\\end{tabular}" <<endl;
   aOut << "\\end{table}" <<endl;
   aOut << "}" <<endl;
   aOut << " " <<endl;
   aOut << aFooter << endl;
   aOut << " " <<endl;
}
/**
* Make last part of tex table
* */
template<class STREAM>
void TexTableEndDocument(STREAM& aOut)
{
   aOut << "\\end{document}" <<endl;
}

#endif /* MIDAS_UTIL_MAKETABLES_H_INCLUDED */
