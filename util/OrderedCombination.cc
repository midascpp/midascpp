/**
 *******************************************************************************
 * 
 * @file    OrderedCombination.h
 * @date    27-03-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    OrderedCombination class and related stuff.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <algorithm>
#include <sstream>
#include <utility>
#include "util/Error.h"
#include "util/OrderedCombination.h"

//==============================================================================
//==============================================================================
// CONSTRUCTORS, ETC.
//==============================================================================
//==============================================================================

OrderedCombination::OrderedCombination(const OrderedCombination&) = default;
OrderedCombination::OrderedCombination(OrderedCombination&&) = default;
OrderedCombination::~OrderedCombination() = default;

/***************************************************************************//**
 * @brief 
 *    Combinations from aBeg until (but not including) aEnd elements in the set
 *    {0,...,aNumElemsTotal-1}.
 *
 * @param[in] aBeg
 *    Initial combination is the first one with this many elements.
 *
 * @param[in] aEnd
 *    First-beyond-last combination is the first one with this many elements.
 *
 * @param[in] aNumElemsTotal
 *    Combinations will be from subsets of the first aNumElemsTotal unsigned
 *    integers, i.e. {0,...,aNumElemsTotal - 1}.
 ******************************************************************************/
OrderedCombination::OrderedCombination
   (  Uin aBeg
   ,  Uin aEnd
   ,  Uin aNumElemsTotal
   )
   :  mBase(MakeBaseVector(aBeg, aEnd))
   ,  mFixed(std::vector<In>(I_0))
   ,  mNonFixed(MakeNonFixedElems(aNumElemsTotal, mFixed))
   ,  mSizeBegin(aBeg)
   ,  mSizeEnd(aEnd)
{
   CheckValidity();
}

/***************************************************************************//**
 * @brief 
 *    Combinations from aBeg until (but not including) aEnd elements in the set
 *    provided in the constructor's argument.
 *
 * @param[in] aBeg
 *    Initial combination is the first one with this many elements.
 *
 * @param[in] aEnd
 *    First-beyond-last combination is the first one with this many elements.
 *
 * @param[in] aNonFixed
 *    The elements from which to make combinations.
 ******************************************************************************/
OrderedCombination::OrderedCombination
   (  Uin aBeg
   ,  Uin aEnd
   ,  std::vector<In> aNonFixed
   )
   :  mBase(MakeBaseVector(aBeg, aEnd))
   ,  mFixed(std::vector<In>(I_0))
   ,  mNonFixed(SortRemoveDuplicates(std::move(aNonFixed)))
   ,  mSizeBegin(aBeg)
   ,  mSizeEnd(aEnd)
{
   CheckValidity();
}

/***************************************************************************//**
 * @brief 
 *    Combinations from aBeg until (but not including) aEnd nonfixed elements
 *    in the set {0,...}, where fixed elements have been excluded, and such
 *    that the total number of elements is aNumElemsTotal.
 *
 * @param[in] aBeg
 *    Initial combination is the first one with this many nonfixed elements.
 *
 * @param[in] aEnd
 *    First-beyond-last combination is the first one with this many nonfixed
 *    elements.
 *
 * @param[in] aNumElemsTotal
 *    The nonfixed elements for the combinations will be the first
 *    `aNumElemsTotal - aFixed.size()` nonnegative integers that are not in
 *    `aFixed`.
 *
 * @param[in] aFixed
 *    The fixed elements that will be part of _any_ combination retrieved from
 *    this object.
 ******************************************************************************/
OrderedCombination::OrderedCombination
   (  Uin aBeg
   ,  Uin aEnd
   ,  Uin aNumElemsTotal
   ,  std::vector<In> aFixed
   )
   :  mBase(MakeBaseVector(aBeg, aEnd))
   ,  mFixed(SortRemoveDuplicates(std::move(aFixed)))
   ,  mNonFixed(MakeNonFixedElems(aNumElemsTotal, mFixed))
   ,  mSizeBegin(aBeg)
   ,  mSizeEnd(aEnd)
{
   CheckValidity();
}

/***************************************************************************//**
 * @brief 
 *    Combinations from aBeg until (but not including) aEnd nonfixed elements
 *    from the set provided in the constructor's argument (with fixed elements
 *    removed).
 *
 * @param[in] aBeg
 *    Initial combination is the first one with this many nonfixed elements.
 *
 * @param[in] aEnd
 *    First-beyond-last combination is the first one with this many nonfixed
 *    elements.
 *
 * @param[in] aNonFixed
 *    The nonfixed elements for the combinations.
 *
 * @param[in] aFixed
 *    The fixed elements that will be part of _any_ combination retrieved from
 *    this object.
 ******************************************************************************/
OrderedCombination::OrderedCombination
   (  Uin aBeg
   ,  Uin aEnd
   ,  std::vector<In> aNonFixed
   ,  std::vector<In> aFixed
   )
   :  mBase(MakeBaseVector(aBeg, aEnd))
   ,  mFixed(SortRemoveDuplicates(std::move(aFixed)))
   ,  mNonFixed(InplaceDifferenceSortedVectors(SortRemoveDuplicates(std::move(aNonFixed)),mFixed))
   ,  mSizeBegin(aBeg)
   ,  mSizeEnd(aEnd)
{
   CheckValidity();
}



//==============================================================================
//==============================================================================
// PUBLIC METHODS
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * @return
 *    The number of nonfixed elements.
 ******************************************************************************/
Uin OrderedCombination::NumElementsNonFixed
   (
   )  const
{
   return mNonFixed.size();
}

/***************************************************************************//**
 * @return
 *    The total number of elements, both fixed and nonfixed.
 ******************************************************************************/
Uin OrderedCombination::NumElementsTotal
   (
   )  const
{
   return NumElementsNonFixed() + mFixed.size();
}

/***************************************************************************//**
 * @return
 *    The number of (nonfixed) elements in the first combination of the object.
 ******************************************************************************/
Uin OrderedCombination::SizeBegin
   (
   )  const
{
   return mSizeBegin;
}

/***************************************************************************//**
 * @return
 *    The number of (nonfixed) elements in the first-beyond-last combination of
 *    the object.
 ******************************************************************************/
Uin OrderedCombination::SizeEnd
   (
   )  const
{
   return mSizeEnd;
}

/***************************************************************************//**
 * @note
 *    Hard error if calling this if object is AtEnd().
 *
 * @return
 *    A newly constructed vector with the fixed and current combination of
 *    nonfixed elements, all sorted in ascending order.
 ******************************************************************************/
std::vector<In> OrderedCombination::GetCombination
   (
   )  const
{
   std::vector<In> vec = GetNonFixed();
   if (!mFixed.empty())
   {
      vec.reserve(vec.size() + mFixed.size());
      vec.insert(vec.end(), mFixed.begin(), mFixed.end());
      std::sort(vec.begin(), vec.end());
   }
   return vec;
}

/***************************************************************************//**
 * @return
 *    Whether the object is in its `end` state, i.e. the first-beyond-last
 *    combination.
 ******************************************************************************/
bool OrderedCombination::AtEnd
   (
   )  const
{
   return (mBase.size() >= SizeEnd());
}

/***************************************************************************//**
 * Increments the combination of the object.
 *
 * Implementation: Tries from last to first element of
 * OrderedCombination::mBase (i.e. from right to left) to increment the
 * element. If the element already has its maximum value moves on the the next
 * (to the left) to try that one. It has its maximum value if incrementing it
 * would equal it to the one on its right or to NumElementsNonFixed() if the
 * element is the rightmost one.
 * If finding an element that can be incremented, do so and reset the elements
 * to the right to the lowest values possible such that they are still in
 * strictly ascending order.
 * If no elements can be incremented, increase the size of
 * OrderedCombination::mBase by one and set it to the lowest possible
 * combination {0,1,2,...}.
 *
 * Example: Let NumElementsNonFixed() = 5.
 *    -  Incrementing {0,1,2} yields {0,1,3} since the `2` can be incremented.
 *    -  Incrementing {0,1,4} yields {0,2,3} since the `4` can't be incremented
 *       but the `1` can.
 *    -  Incrementing {2,3,4} yields {0,1,2,3} since none of them can be
 *       incremented, resulting in an increment of the size.
 *
 * @note
 *    If object is already AtEnd(), nothing happens. I.e. the object cannot be
 *    incremented past the end.
 *
 * @note
 *    There's no `OrderedCombination::operator--()` at the moment (March,
 *    2017), since it wasn't necessary. But feel free to implement it according
 *    to the same logic as for this function.
 *
 * @return
 *    Reference to the object, _after_ increment.
 ******************************************************************************/
OrderedCombination& OrderedCombination::operator++
   (
   )
{
   if (!AtEnd())
   {
      // Loop in reverse to find the first element (from the back) that can be
      // incremented.
      auto r_end = mBase.rend();
      auto r_it = mBase.rbegin();
      for(; r_it != r_end; ++r_it)
      {
         if (*r_it < MaxValueForPosition(r_it))
         {
            ++(*r_it);
            break;
         }
      }

      // If we reached the end in loop, we must go up in size.
      // (This potentially invalidates iterators, so use new calls!)
      // In any case we must reset the indices to as low as possible from the
      // changed position and onwards.
      if (r_it == r_end)
      {
         mBase.resize(mBase.size() + I_1);
         r_it = mBase.rend();
      }

      for(auto r_beg = mBase.rbegin(); r_it != r_beg; )
      {
         --r_it;
         *r_it = MinValueForPosition(r_it);
      }
   }

   return *this;
}





//==============================================================================
//==============================================================================
// PRIVATE METHODS
//==============================================================================
//==============================================================================

/***************************************************************************//**
 * Checks the following and causes a hard error if not fulfilled:
 *    -  That `SizeEnd() >= SizeBegin()` (otherwise the range doesn't make
 *       sense).
 *    -  That `SizeEnd() > NumElementsNonFixed() + 1` (otherwise there's not
 *       enough nonfixed elements to map OrderedCombination::mBase to the
 *       actual combination of the nonfixed elements).
 ******************************************************************************/
void OrderedCombination::CheckValidity
   (
   )  const
{
   bool validity = true;
   std::stringstream s_err;
   // Check that [beg;end) is a valid range.
   if (SizeBegin() > SizeEnd())
   {
      s_err << "SizeBegin (which is " << SizeBegin() << ") > "
            << "SizeEnd (which is " << SizeEnd() << ").\n";
      validity = false;
   }
   // Check that the end size is not too large.
   if (SizeEnd() > NumElementsNonFixed() + I_1)
   {
      s_err << "SizeEnd (which is " << SizeEnd() << ") more than 1 larger than "
            << "NumElementsNonFixed (which is " << NumElementsNonFixed() << ").\n";
      validity = false;
   }

   // Hard error if invalid.
   if (!validity)
   {
      MIDASERROR("Invalid OrderedCombination:\n" + s_err.str());
   }
}

/***************************************************************************//**
 * Make the initial base combination with size aBeg and space reserved for aEnd
 * elements in total (to avoid reallocations at any point in the lifetime of
 * the object).
 *
 * @param[in] aBeg
 *    Number of elements in the initial base vector.
 *
 * @param[in] aEnd
 *    Number of elements in the first-beyond-last base combination (reserves
 *    space for aEnd elements). 
 *
 * @return
 *    The first base combination vector, `{0,1,...,aBeg-1}`.
 ******************************************************************************/
std::vector<Uin> OrderedCombination::MakeBaseVector
   (  Uin aBeg
   ,  Uin aEnd
   )  const
{
   std::vector<Uin> vec;
   vec.reserve(aEnd);
   for(Uin i = I_0; i < aBeg; ++i)
   {
      vec.push_back(i);
   }
   return vec;
}

/***************************************************************************//**
 * Asserts that:
 *    -  aNumElemsTotal not larger than largest unsigned integer. To avoid
 *       overflow when casting to signed.
 *    -  Number of fixed elements not larger than aNumElemsTotal.
 *
 * Then constructs a vector of integers {0,1,...} where integers occuring in
 * arFixed have been removed, and so that the size equals the aNumElemsTotal
 * minus the number of fixed elements.
 *
 * Example: aNumElemsTotal = 5, arFixed = {-1, 2}; returns {0,1,3}.
 *
 * @param[in] aNumElemsTotal
 *    Total number of elements (fixed + nonfixed) desired in the object. Must
 *    _not_ be larger than largest signed integer.
 *
 * @param[in] arFixed
 *    Reference to the elements to be considered fixed in the combinations of
 *    the object. Must be a _sorted_ vector _without_ duplicates.
 *
 * @return
 *    Vector of nonfixed elements, {0,1,...}, with fixed elements removed and
 *    size is aNumElemsTotal - arFixed.size().
 ******************************************************************************/
std::vector<In> OrderedCombination::MakeNonFixedElems
   (  Uin aNumElemsTotal
   ,  const std::vector<In>& arFixed
   )  const
{
   // aNumElemsTotal must still be positive when casting Uin to In.
   // aNumElemsTotal must be >= num. fixed elements, otherwise illogical.
   MidasAssert(aNumElemsTotal <= C_IN_MAX, "aNumElemsTotal > C_IN_MAX");
   MidasAssert(aNumElemsTotal >= arFixed.size(), "aNumElemsTotal < num. fixed elements.");

   // Incremented fixed elements until getting to 0 or positive ones.
   auto it_f = arFixed.begin(), end_f = arFixed.end();
   while (it_f != end_f && *it_f < I_0)
   {
      ++it_f;
   }

   // Size of non fixed must be total number minus the fixed ones.
   Uin size = aNumElemsTotal - arFixed.size();
   std::vector<In> vec;
   vec.reserve(size);

   // Then push back with all the natural numbers not among the fixed elements.
   for(In i = I_0; vec.size() != size; ++i)
   {
      if (it_f != end_f)
      {
         if (i != *it_f)
         {
            vec.push_back(i);
         }
         else
         {
            ++it_f;
         }
      }
      else
      {
         vec.push_back(i);
      }
   }

   return vec;
}

/***************************************************************************//**
 * Takes the argument object, sorts it in ascending order, removes duplicates
 * and (tries to) shrink(s) the allocated memory to fit new size.
 *
 * @param[in] aVec
 *    (Copy of) vector for which to sort/remove duplicates. `std::move` when
 *    passing argument to basically do it in-place.
 *
 * @return
 *    Sorted vector with unique elements.
 ******************************************************************************/
std::vector<In> OrderedCombination::SortRemoveDuplicates
   (  std::vector<In> aVec
   )  const
{
   std::sort(aVec.begin(), aVec.end());
   aVec.erase(std::unique(aVec.begin(), aVec.end()), aVec.end());
   aVec.shrink_to_fit();
   return aVec;
}

/***************************************************************************//**
 * Given two sorted vectors, removes the elements of arRemoveThese from the
 * elements of aVec and return the latter.
 *
 * @param[in] aVec
 *    (Copy of) vector for which to remove the elements of arRemoveThese.
 *    `std::move` when passing argument to basically do it in-place.
 *    _Must_ be sorted prehand.
 *
 * @param[in] arRemoveThese
 *    Vector whose elements will be removed from aVec. _Must_ be sorted
 *    prehand.
 *    
 * @return
 *    Vector consisting of the elements that are in aVec but not in
 *    arRemoveThese.
 ******************************************************************************/
std::vector<In> OrderedCombination::InplaceDifferenceSortedVectors
   (  std::vector<In> aVec
   ,  const std::vector<In>& arRemoveThese
   )  const
{
   auto it_v = aVec.begin(), end_v = aVec.end();
   auto it_rm = arRemoveThese.begin(), end_rm = arRemoveThese.end();
   auto it_new = it_v;
   while (it_v != end_v && it_rm != end_rm)
   {
      if (*it_v < *it_rm)
      {
         *it_new = std::move(*it_v);
         ++it_new;
         ++it_v;
      }
      else if  (*it_rm < *it_v)
      {
         ++it_rm;
      }
      else
      {
         ++it_v;
         ++it_rm;
      }
   }
   while (it_v != end_v)
   {
      *it_new = std::move(*it_v);
      ++it_new;
      ++it_v;
   }
   aVec.erase(it_new, end_v);
   aVec.shrink_to_fit();
   return aVec;
}

/***************************************************************************//**
 * For use when incrementing the object. Uses reverse iterators since
 * incrementing operates from the end towards the beginning (i.e. from rbegin()
 * towards rend()).
 *
 * The max. possible value equals the element to the right (i.e. pointed to by
 * `--aRevIt`) `- 1` or `NumElementsNonFixed() - 1` if aRevIt points to the
 * rightmost element.
 *
 * The min. possible value equals the element to the left (i.e. pointed to by
 * `++aRevIt`) `+ 1` or `0` if aRevIt points to the leftmost element.
 *
 * @param[in] aRevIt
 *    A reverse iterator to an element in OrderedCombination::mBase. Must _not_
 *    be `rend()`.
 *
 * @return
 *    The min/max possible value for the current position given the current
 *    combination held by mBase.
 ******************************************************************************/
Uin OrderedCombination::MinValueForPosition
   (  std::vector<Uin>::const_reverse_iterator aRevIt
   )  const
{
   ++aRevIt;
   if (aRevIt == mBase.rend())
   {
      return I_0;
   }
   else
   {
      return *(aRevIt) + I_1;
   }
}

Uin OrderedCombination::MaxValueForPosition
   (  std::vector<Uin>::const_reverse_iterator aRevIt
   )  const
{
   if (aRevIt == mBase.rbegin())
   {
      return NumElementsNonFixed() - I_1;
   }
   else
   {
      return *(--aRevIt) - I_1;
   }
}

/***************************************************************************//**
 * @note
 *    Hard error when calling this if object is AtEnd().
 *
 * @return
 *    A newly constructed vector with the current combination of nonfixed
 *    elements, sorted in ascending order.
 ******************************************************************************/
std::vector<In> OrderedCombination::GetNonFixed
   (
   )  const
{
   if (AtEnd())
   {
      MIDASERROR("OrderedCombination object is at end position; can't get combination.");
      return std::vector<In>(I_0);
   }
   else
   {
      std::vector<In> vec;
      vec.reserve(mBase.size());
      for(const auto& i: mBase)
      {
         vec.push_back(mNonFixed[i]);
      }
      return vec;
   }
}

