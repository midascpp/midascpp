/**
************************************************************************
* 
* @file                Singleton.h
*
* Created:             07-02-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   A base class to inherit from for classes that should only have one instance in a program
* 
* Last modified: Tue. Feb 07 2012
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SINGLETON_H_INCLUDED
#define SINGLETON_H_INCLUDED

template<class A>
class Singleton
{
   private:
   
   protected:
      Singleton() = default;

   public:
      Singleton(const Singleton&) = delete;
      Singleton(Singleton&&) = delete;
      Singleton& operator=(const Singleton&) = delete;
      Singleton& operator=(Singleton&&) = delete;

      static A& instance()
      {
         static A a; 
         return a;
      }
};

#endif /* SINGLETON_H_INCLUDED */
