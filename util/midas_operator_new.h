/**
************************************************************************
* 
* @file                midas_operator_new.h
*
* Created:             24-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Midas operator new declaration
*                      Will be used if -DMIDAS_MEM_DEBUG is set
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifdef MIDAS_MEM_DEBUG

#ifndef MIDAS_OPERATOR_NEW_H
#define MIDAS_OPERATOR_NEW_H

#pragma GCC push_options
#pragma GCC optimize ("-O3")

void* operator new(std::size_t sz);

void* operator new(std::size_t sz, const std::nothrow_t& nothrow_values) noexcept;

void* operator new[](std::size_t sz);

void* operator new[](std::size_t sz, const std::nothrow_t& nothrow_values) noexcept;

void operator delete(void* ptr) noexcept;

void operator delete(void* ptr, const std::nothrow_t& nothrow_values) noexcept;

void operator delete[](void* ptr) noexcept;

void operator delete[](void* ptr, const std::nothrow_t& nothrow_values) noexcept;

#pragma GCC pop_options

#endif /* MIDAS_OPERATOR_NEW_H */

#endif /* MIDAS_MEM_DEBUG */
