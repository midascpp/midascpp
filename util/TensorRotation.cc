/**
************************************************************************
* 
* @file                TensorRotation.cc
*
* Created:             12-07-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TensorRotation datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/TensorRotation.h"

// using declarations
using std::string;
using std::vector;

TensorRotation::TensorRotation(const MidasMatrix& aM) : mRot(I_3)
{
   if(aM.Ncols()!=aM.Nrows() || aM.Ncols()!=I_3)
      MIDASERROR("Only support for cartesian rotations...");
   mRot=aM;
}

void TensorRotation::RotateMidasVector(MidasVector& aV)
{
   Nb* elem=aV.data();
   RotateCOneDArray(elem);
}

void TensorRotation::RotateCOneDArray(Nb* aV)
{
   Nb result[3];
   for(In i=0;i<3;i++) {
      result[i]=C_0;
      for(In j=0;j<3;j++) {
         result[i]+=mRot[i][j]*aV[j];
      }
   }
   for(In i=0;i<3;i++) {
      aV[i]=result[i];
   }
}

void TensorRotation::RotateMidasMatrix(MidasMatrix& aM)
{
   Nb** elem=aM.GetElements();
   RotateCTwoDArray(elem);
}

void TensorRotation::RotateCTwoDArray(Nb** aM)
{
   Nb result[3][3];
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         result[i][j]=C_0;
         for(In k=0;k<I_3;k++) {
            for(In l=0;l<I_3;l++) {
               result[i][j]+=mRot[i][k]*mRot[j][l]*aM[k][l];
            }
         }
      }
   }
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         aM[i][j]=result[i][j];
      }
   }
}

void TensorRotation::RotateCThreeDArray(Nb*** aT)
{
   Nb result[3][3][3];
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         for(In k=0;k<I_3;k++) {
            result[i][j][k]=C_0;
            for(In l=0;l<I_3;l++) {
               for(In m=0;m<I_3;m++) {
                  for(In n=0;n<I_3;n++) {
                     result[i][j][k]+=mRot[i][l]*mRot[j][m]*mRot[k][n]*aT[l][m][n];
                  }
               }
            }
         }
      }
   }
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         for(In k=0;k<I_3;k++) {
            aT[i][j][k]=result[i][j][k];
         }
      }
   }
}

void TensorRotation::RotateCFourDArray(Nb**** aT)
{
   Nb result[3][3][3][3];
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         for(In k=0;k<I_3;k++) {
            for(In l=0;l<I_3;l++) {
               result[i][j][k][l]=C_0;
               for(In m=0;m<I_3;m++) {
                  for(In n=0;n<I_3;n++) {
                     for(In o=0;o<I_3;o++) {
                        for(In p=0;p<I_3;p++) {
                           result[i][j][k][l]+=mRot[i][m]*mRot[j][n]
                              *mRot[k][o]*mRot[l][p]*aT[m][n][o][p];
                        }
                     }
                  }
               }
            }
         }
      }
   }
   for(In i=0;i<I_3;i++) {
      for(In j=0;j<I_3;j++) {
         for(In k=0;k<I_3;k++) {
            for(In l=0;l<I_3;l++) {
               aT[i][j][k][l]=result[i][j][k][l];
            }
         }
      }
   }
}
