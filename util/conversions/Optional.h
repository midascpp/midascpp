#ifndef MIDAS_UTIL_OPTIONAL_H_INCLUDED
#define MIDAS_UTIL_OPTIONAL_H_INCLUDED

template<class T>
class Optional
{
   private:
      bool mHasValue;
      T mValue;

   public:
      Optional()
         :  mHasValue(false)
         ,  mValue()
      {
      }
      
      Optional(const T& t)
         :  mHasValue(true)
         ,  mValue(t)
      {
      }

      T& operator*() { return mValue; }
      const T& operator*() const { return mValue; }

      operator bool() const { return mHasValue; }
};


#endif /* MIDAS_UTIL_OPTIONAL_H_INCLUDED */
