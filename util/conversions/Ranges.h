/**
************************************************************************
* 
* @file                Ranges.h
*
* Created:             15-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Interface for expanding expressions like [1..3] or [1..3;2] ([from..to;step]) 
*                      to vectors (In the cases presented (1,2,3) and (1,3) would be the result)
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef RANGES_H_INCLUDED
#define RANGES_H_INCLUDED

#include <string>
#include <vector>
#include <algorithm>
#include "util/conversions/FromString.h"
#include "util/Io.h"
#include "util/Range.h"

namespace midas
{
namespace util
{
namespace detail
{
   std::vector<std::string> GetVarsFromExp(const std::string& aVarStr);
   bool IsValidExpr(const std::string& aExp);
   bool IsRepExpr(const std::string& aExp);
   std::vector<std::string> GetRepExpr(const std::string& aToBeExpanded);
   std::vector<std::string> GetExpansion(const std::string& aToBeExpanded);

   template<class T>
   inline std::vector<T> GetRealExpansion(const std::string& aToBeExpanded)
   {
      std::vector<T> vars;
      vars.push_back(-1);
      vars.push_back(-1);
      vars.push_back(1);
      std::vector<std::string> var_strs = GetVarsFromExp(aToBeExpanded);
      //Mout << "var_strs : " << var_strs << endl;
      for(int i = 0; i < var_strs.size(); ++i)
      {
         vars[i] = FromString<T>(var_strs[i]);
      }
      std::vector<T> result;
      for(T i = vars[0]; i <= vars[1]; i+=vars[2])
      {
         result.emplace_back(i);
      }
      return result;
   }

   inline std::string RemoveBrackets(const std::string& str)
   {
      return str.substr(1,str.size()-2);
   }
}/*namespace detail*/

std::vector<std::string> CreateVectorFromRange(const string& aBashRange);

template<class T>
Range<T> CreateRangeFromString(const std::string& aRangeStr)
{
   auto var_strs = detail::GetVarsFromExp(detail::RemoveBrackets(aRangeStr));
   return { midas::util::FromString<T>(var_strs[0])
          , midas::util::FromString<T>(var_strs[1])
          , midas::util::FromString<T>(var_strs[2]) 
          };
          
}

} /* namespace util */
} /* namespace midas */

#endif /* RANGES_H_INCLUDED */
