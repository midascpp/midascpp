/**
************************************************************************
* 
* @file                TupleFromString.h
*
* Created:             14-04-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Converts a string into a tuple
* 
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TUPLE_FROM_STRING_H_INCLUDED
#define TUPLE_FROM_STRING_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <stdexcept>      // std::out_of_range, std::invalid_argument

// midas headers
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/conversions/Ranges.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "libmda/meta/index_list.h"
#include "mmv/MidasVector.h"

namespace midas
{
namespace util
{
namespace detail
{
/**
 *
 **/
template<class... Ts, unsigned... Is>
std::tuple<Ts...> CreateTupleFromStringImpl
   ( const std::vector<std::string>& aVec
   , libmda::meta::index_list<Is...>
   )
{
   return std::tuple<Ts...>(FromString<Ts>(aVec[Is])...);
}

/**
 *
 **/
template<class... Ts>
std::tuple<Ts...> CreateTupleFromString
   ( const std::string& aStr
   , char aSeparator = ' '
   )
{
   auto vec = StringVectorFromString(aStr, aSeparator);
   MidasAssert(vec.size() == sizeof...(Ts), "Number of input args do not match. Read = " + std::to_string(vec.size()) + ", Required = " + std::to_string(sizeof...(Ts)));
   return CreateTupleFromStringImpl<Ts...>(vec, libmda::meta::index_range<0, sizeof...(Ts)>());
}


/**
 *
 **/
template<class... Ts>
struct TupleFromStringImpl
{
   //static std::tuple<Ts...> apply(const std::string& arStr, char aSeparator)
   static auto apply(const std::string& arStr, char aSeparator)
   {
      return CreateTupleFromString<Ts...>(arStr, aSeparator);
   }
};

/**
 *
 **/
template<class... Ts>
struct TupleFromStringImpl<std::tuple<Ts...> >
{
   //static std::tuple<Ts...> apply(const std::string& arStr, char aSeparator)
   static auto apply(const std::string& arStr, char aSeparator)
   {
      return CreateTupleFromString<Ts...>(arStr, aSeparator);
   }
};

} /* namespace detail */

/**
 *
 **/
template<class... Ts>
auto TupleFromString
   ( const std::string& arStr
   , char aSeparator = ' '
   )
   //-> decltype(detail::TupleFromStringImpl<Ts...>::apply(arStr, aSeparator))
{
   return detail::TupleFromStringImpl<Ts...>::apply(arStr, aSeparator);
}

/**
 *
 **/
template<class... Ts>
auto TupleFromStringVec
   ( const std::vector<std::string>& arVec
   )
{
   MidasAssert(arVec.size() == sizeof...(Ts), "Number of input args do not match. Read = " + std::to_string(arVec.size()) + ", Required = " + std::to_string(sizeof...(Ts)));
   return detail::CreateTupleFromStringImpl<Ts...>(arVec, libmda::meta::index_range<0, sizeof...(Ts)>());
}

} /* namespace util */
} /* namespace midas */

#endif /* TUPLE_FROM_STRING_H_INCLUDED */
