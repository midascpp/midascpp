/**
************************************************************************
* 
* @file                BaseExpression.h
*
* Created:             16-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Base Class for the expressions
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <vector>
#include "util/conversions/BaseExpression.h"
#include "util/conversions/ExpandExpression.h"
#include "util/conversions/StringShuntingYard.h"

namespace midas
{
namespace util
{
namespace detail
{
   //This is a fucking initializer list...
   /*
   * format is defined as follows :
   * {name-in-code, std::shared_ptr<BaseExpression>{new ClassName}}, 
   * {name-in-code, std::shared_ptr<BaseExpression>{new ClassName}}
   */
   const std::map<std::string, std::shared_ptr<BaseExpression> > gExpressionNamesAndFuncs = 
   {
      {"EXPAND", std::shared_ptr<BaseExpression>{new ExpandExpression}}
   };
} /*namespace detail*/

std::vector<std::string> BaseExpression::EvalExpression(const std::string& aStr, char aSeperator)
{
   if(aStr.back() != ')')
   {
      //Error state
   }
   size_t first_left_par_poss = aStr.find('(');
   if(std::string::npos == first_left_par_poss)
      return std::vector<std::string>(1,aStr);
   auto it = detail::gExpressionNamesAndFuncs.find(aStr.substr(0, first_left_par_poss));
   if(detail::gExpressionNamesAndFuncs.end() == it)
      return std::vector<std::string>(1,aStr);
   std::vector<std::vector<std::string> > arguments = detail::StringShuntingYard(aStr.substr(first_left_par_poss+1, aStr.size()-first_left_par_poss-2),aSeperator);
   return it->second->Apply(arguments, aSeperator);
}

} /*namespace util*/
} /*namespace midas*/

