/**
************************************************************************
* 
* @file                Ranges.cc
*
* Created:             15-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Interface for expanding expressions like [1..3] or [1..3;2] ([from..to;step]) 
*                      to vectors (In the cases presented (1,2,3) and (1,3) would be the result)
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
#include <vector>
#include <algorithm>

#include "util/conversions/FromString.h"
#include "util/Io.h"
#include "util/conversions/Ranges.h"
#include "util/conversions/StringShuntingYard.h"

namespace midas
{
namespace util
{
namespace detail
{
   /**
    *
    **/
   std::vector<std::string> GetVarsFromExp(const std::string& aVarStr)
   {
      std::vector<std::string> result;
      size_t poss = aVarStr.find("..");
      result.emplace_back(aVarStr.substr(0,poss));
      size_t next_nr_start = poss + 2;
      poss = aVarStr.find(";");
      result.emplace_back(aVarStr.substr(next_nr_start,poss-next_nr_start));
      if(std::string::npos != poss)
         result.emplace_back(aVarStr.substr(poss+1,std::string::npos));
      return result;
   }

   /**
    *
    **/
   bool IsValidExpr(const std::string& aExp)
   {
      if(aExp.front() != '[' || aExp.back() != ']')
         return false;
      if(std::count(aExp.begin(), aExp.end(), '[') != 1)
         return false;
      if(std::count(aExp.begin(), aExp.end(), ']') != 1)
         return false;
      In nr_semi = std::count(aExp.begin(), aExp.end(), ';');
      if(nr_semi > 1)
         return false;
      std::string test("...");
      if(aExp.end() != std::search(aExp.begin(), aExp.end(), test.begin(), test.end()))
         return false;
      test.pop_back();
      auto it = std::search(aExp.begin(), aExp.end(), test.begin(), test.end());
      if(aExp.end() == it)
         return false;
      ++it;
      ++it;
      it = std::search(it, aExp.end(), test.begin(), test.end());
      if(aExp.end() != it)
         return false;
      
      return true;
   }
   
   /**
    *
    **/
   std::vector<std::string> GetRepExpr(const std::string& aToBeExpanded)
   {
      std::vector<std::string> res;
      std::vector<std::vector<std::string> > temp;
      size_t poss = aToBeExpanded.find('*');
      In rep = FromString<In>(aToBeExpanded.substr(0, poss));
      temp = StringShuntingYard(aToBeExpanded.substr(poss+1,aToBeExpanded.size() - 1 - poss));
      if(temp.size() != 1 || temp.front().size() < 1)
      {
         MIDASERROR("Could not convert "+aToBeExpanded+" to vector");
      }
      if(aToBeExpanded[poss+1] == '[')
         for(In i = 0; i < rep; ++i)
            res.insert(res.end(), temp.front().begin(), temp.front().end());
      else
      {
         for(In i = 0; i < rep; ++i)
            res.insert(res.end(), temp.front().front());
         res.insert(res.end(), temp.front().begin()+1, temp.front().end());
      }
      return res;
   }

   /**
    *
    **/
   bool IsRepExpr(const std::string& aExp)
   {
      size_t poss = aExp.find('*');
      if(std::string::npos == poss || poss != aExp.find_first_not_of("0123456789"))
         return false;
      return true;
   }

   /**
    *
    **/
   std::vector<std::string> GetExpansion(const std::string& aToBeExpanded)
   {
      bool is_float = false;
      if(std::string::npos == aToBeExpanded.find("E") || std::string::npos == aToBeExpanded.find("e"))
      {
         size_t poss = aToBeExpanded.find(".");
         while(std::string::npos != poss)
         {
            if(std::isdigit(aToBeExpanded[poss-1]) && std::isdigit(aToBeExpanded[poss+1]))
            {
               is_float = true;
               break;
            }
            poss = aToBeExpanded.find(".",poss+1);
         }
      }
      else
         is_float = true;
   
      if(is_float)
      {
         std::vector<Nb> temp = GetRealExpansion<Nb>(aToBeExpanded);
         std::vector<std::string> result;
         for(In i = 0; i < temp.size(); ++i)
            result.emplace_back(std::to_string(temp[i]));
         return result;
      }
      else
      {
         std::vector<In> temp = GetRealExpansion<In>(aToBeExpanded);
         std::vector<std::string> result;
         for(In i = 0; i < temp.size(); ++i)
            result.emplace_back(std::to_string(temp[i]));
         return result;
      }
      //Error should never happen...
      return vector<string>();
   }
} /* namespace detail */

/**
 *
 **/
std::vector<string> CreateVectorFromRange(const string& aRange)
{
   std::vector<std::string> res;
   if(detail::IsValidExpr(aRange))
   {
      //Mout << aRange << " is bash expr" << endl;
      res = detail::GetExpansion(aRange.substr(1,aRange.size() - 2)); //Here we remove the "[" and "]"
   }
   else if(detail::IsRepExpr(aRange.substr(1,aRange.size() - 2)))
   {
      //Mout << aRange << " is rep expr " << aRange.substr(1,aRange.size() - 2) << endl;
      res = detail::GetRepExpr(aRange.substr(1,aRange.size() - 2));
   }
   else
   {
      //Mout << aRange << " is vector" << endl;
      std::vector<std::vector<std::string> > temp;
      temp = detail::StringShuntingYard(aRange.substr(1,aRange.size() - 2)); //Here we remove the "[" and "]"
      if(1 != temp.size())
      {
         MIDASERROR("String : \""+aRange+"\" can't be converted to vector");
      }
      res = temp.front();
   }
   return res;
}


}/*namespace util*/
}/*namespace midas*/
