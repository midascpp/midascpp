/**
************************************************************************
* 
* @file                ExpandExpression.h
*
* Created:             16-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class for handing the EXPAND(arg1;arg2;arg3) statement
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef EXPANDEXPRESSION_H
#define EXPANDEXPRESSION_H
#include <string>
#include <vector>
#include <map>
#include <memory>

#include "util/conversions/BaseExpression.h"
namespace midas
{
namespace util
{
class ExpandExpression : public BaseExpression
{
   public:
      ExpandExpression() : BaseExpression() {}
   private:
      std::vector<std::string> Apply(const std::vector<std::vector<std::string> >& aStr, char aSeperator = ' ') const override;
      std::vector<std::string> Recursion(const std::vector<std::string>&, std::vector<std::vector<std::string> >::const_iterator, bool, 
                                   std::vector<std::string>::const_iterator, std::vector<std::string>::const_iterator) const;
};
} /*namespace util*/
} /*namespace midas*/
#endif /*EXPANDEXPRESSION_H*/
