/**
************************************************************************
* 
* @file                StringShuntingYard.h
*
* Created:             12-02-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class for generating a vector<string> using Midas's
*                      internal functions for string expandions
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef STRINGSHUNTINGYARD_H
#define STRINGSHUNTINGYARD_H
#include <string>
#include <vector>
#include <map>
#include <memory>

namespace midas
{
namespace util
{
namespace detail
{
   std::vector<std::vector<std::string> > StringShuntingYard(const std::string&, const char aSeperator = ' ');
} /*namespace detail*/
} /*namespace util*/
} /*namespace midas*/
#endif /*STRINGSHUNTINGYARD_H*/
