/**
************************************************************************
* 
* @file                StringShuntingYard.h
*
* Created:             12-02-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class for generating a vector<string> using Midas's
*                      internal functions for string expandions
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
#include <vector>
#include <map>
#include <memory>

#include "util/conversions/BaseExpression.h"
#include "util/conversions/Ranges.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

namespace midas
{
namespace util
{
namespace detail
{

std::vector<std::vector<std::string> > StringShuntingYard(const std::string& aInpString, const char aSeparator)
{
   std::string working_str(aInpString);
   std::vector<std::vector<std::string> > result;
   result.emplace_back(std::vector<std::string>());
   result.back().emplace_back(std::string());
   while(!working_str.empty())
   {
      if(working_str.front() == '\\')
      {
         working_str.erase(0,1);
         if(!working_str.empty())
         {
            result.back().back().push_back(working_str.front());
            working_str.erase(0,1);
         }
      }
      else if(working_str.front() == aSeparator)
      {
         result.back().emplace_back(std::string());
         working_str.erase(0,1);
         while(!working_str.empty() && working_str.front() == aSeparator)
            working_str.erase(0,1);
      }
      else if(working_str.front() == ',')
      {
         result.emplace_back(std::vector<std::string>());
         result.back().emplace_back(std::string());
         working_str.erase(0,1);
         //New vector to stack
      }
      else if(working_str.front() == '(')
      {
         std::string temp = result.back().back();
         result.back().pop_back(); //remove the string we have read so far, it will return later..
         In cnt = 1;
         temp.push_back(working_str.front());
         working_str.erase(0,1);
         while(cnt) //cnt != 0
         {
            if(working_str.empty())
               MIDASERROR("Unmatched ( in string : "+aInpString+" cannot safely convert to vector");
            if(working_str.front() == '(')
            {
               ++cnt;
            }
            else if(working_str.front() == ')')
            {
               --cnt;
            }
            temp.push_back(working_str.front());
            working_str.erase(0,1);
         }
         std::vector<std::string> new_vec = BaseExpression::EvalExpression(temp,aSeparator);
         result.back().insert(result.back().end(), new_vec.begin(), new_vec.end());
         //read till matching )
      }
      else if(working_str.front() == '[')
      {
         if(!result.back().back().empty())
         {
            MIDASERROR("[ given inside part of vector, this is a special character so please remove it from the string : "+aInpString);
         }
         std::string temp;
         result.back().pop_back(); //remove the string we have read so far, it will return later..
         In cnt = 1;
         temp.push_back(working_str.front());
         working_str.erase(0,1);
         while(cnt) //cnt != 0
         {
            if(working_str.empty())
               MIDASERROR("Unmatched ( in string : "+aInpString+" cannot safely convert to vector");
            if(working_str.front() == '[')
            {
               ++cnt;
               //MIDASERROR("Cannot have another [ within [..] check your string : "+aInpString);
            }
            else if(working_str.front() == ']')
            {
               --cnt;
            }
            temp.push_back(working_str.front());
            working_str.erase(0,1);
         }
         std::vector<std::string> new_vec = CreateVectorFromRange(temp);
         result.back().insert(result.back().end(), new_vec.begin(), new_vec.end());
      }
      else
      {
         result.back().back().push_back(working_str.front());
         working_str.erase(0,1);
      }
   }
   return result;
}

} /*namespace detail*/
} /*namespace util*/
} /*namespace midas*/
