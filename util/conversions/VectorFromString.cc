/**
************************************************************************
* 
* @file                VectorFromString.cc
*
* Created:             15-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Converts a comma seperated string to a vector
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
#include <vector>
#include "util/Io.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/StringShuntingYard.h"

namespace midas
{
namespace util
{
namespace detail
{

   /**
    *
    **/
   std::vector<std::string> GetIndividualPartsOfVector(const std::string& aStr, char aSeperator)
   {
      if(aStr.empty()) // if empty string, we return empty vector
         return vector<string>();
      size_t str_beg = aStr.find_first_not_of(aSeperator);
      size_t str_end = aStr.find_last_not_of(aSeperator);
      std::vector<std::vector<std::string> > result = StringShuntingYard(aStr.substr(str_beg, str_end-str_beg+1), aSeperator);
      if(1 != result.size())
      {
         Mout << "Reading string : \"" << aStr.substr(str_beg, str_end-str_beg+1) << "\""<< endl;
         Mout << "Result : " << result << endl;
         MIDASERROR("Something wrong when converting the string "+aStr+" to vector, check your input");
      }
      return result.front();
   }

} /* namespace detail */

/**
* 
**/
std::vector<std::string> SeperateLinesIntoVector(const std::string& aStr)
{
   std::stringstream tmp_s(aStr);
   std::vector<std::string> result;
   std::string tmp;
   while(std::getline(tmp_s, tmp))
   {
      result.emplace_back(tmp);
   }
   return result;
}

} /* namespace util */
} /* namspace midas */
