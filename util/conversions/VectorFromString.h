/**
************************************************************************
* 
* @file                VectorFromString.h
*
* Created:             15-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Converts a comma seperated string to a vector
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_VECTOR_FROM_STRING_H_INCLUDED
#define MIDAS_VECTOR_FROM_STRING_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <stdexcept>      // std::out_of_range, std::invalid_argument

// midas headers
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/conversions/Ranges.h"
#include "util/conversions/FromString.h"
#include "mmv/MidasVector.h"

namespace midas
{
namespace util
{
namespace detail
{
   std::vector<std::string> GetIndividualPartsOfVector(const std::string& aStr, char aSeperator);
}//namespace detail

template<class T>
std::vector<T> VectorFromString(const std::string& aStr, char aSeperator = ' ')
{
   std::vector<std::string> string_vector = detail::GetIndividualPartsOfVector(aStr, aSeperator);
   std::vector<T> res;
   for(auto it = string_vector.begin(); it != string_vector.end(); ++it)
   {
      res.emplace_back(midas::util::FromString<T>(*it));
   }
   return res;
}

template<class T>
inline GeneralMidasVector<T> GeneralMidasVectorFromString(const std::string& aStr, char aSeperator = ' ')
{
   return GeneralMidasVector<T>(VectorFromString<T>(aStr, aSeperator));
}

inline MidasVector MidasVectorFromString(const std::string& aStr, char aSeperator = ' ')
{
   return MidasVector(VectorFromString<Nb>(aStr, aSeperator));
}

inline std::vector<std::string> StringVectorFromString(const std::string& aStr, char aSeperator = ' ')
{
   return detail::GetIndividualPartsOfVector(aStr, aSeperator);
}

std::vector<std::string> SeperateLinesIntoVector(const std::string& aStr);

} //namespace util
} //namespace midas

#endif // MIDAS_VECTOR_FROM_STRING_H_INCLUDED
