/**
************************************************************************
* 
* @file                FromString.h
*
* Created:             15-01-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Function interface for turning a std::string into a "number"(double, int, etc.)
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_FOR_STRING
#define MIDAS_FOR_STRING
#include <string>
#include <stdexcept>      // std::out_of_range, std::invalid_argument
#include <limits>         // std::numeric_limits
#include <typeinfo>
#include "util/Io.h"
#include "util/conversions/Optional.h"
#include "inc_gen/Warnings.h"
#include "util/type_traits/Complex.h"


namespace midas
{
namespace util
{
namespace detail
{
   template<class T> 
   T From_String_Impl(const std::string&, size_t*);
   
   template<>
   inline int From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stoi(aArg, arEnd);
   }
   
   /**
   * This functing is a little messy since there are no stou / stoui function in C++ yet
   * If you read this and such function does exist, please change the function to mirror
   * the simple nature of the rest of these conversion functions
   **/
   template<>
   inline unsigned int From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      unsigned long result = std::stoul(aArg, arEnd);
      if(result > std::numeric_limits<unsigned>::max())
      {
         throw std::out_of_range("stou");
      }
      return result;
   }

   template<>
   inline long From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stol(aArg, arEnd);
   }
   
   template<>
   inline unsigned long From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stoul(aArg, arEnd);
   }
   
   template<>
   inline long long From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stoll(aArg, arEnd);
   }
   
   template<>
   inline unsigned long long From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stoull(aArg, arEnd);
   }
   
   template<>
   inline float From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stof(aArg, arEnd);
   }
   
   template<>
   inline double From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stod(aArg, arEnd);
   }
   
   template<>
   inline long double From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      return std::stold(aArg, arEnd);
   }

   template<>
   inline std::complex<double> From_String_Impl(const std::string& aArg, size_t* arEnd)
   {
      std::istringstream is(aArg);
      std::complex<double> ComplexDouble;
      is >> ComplexDouble;
      return ComplexDouble;
   }
} //namespace detail

/**
 *
 **/
template<class T>
T FromString(const std::string& aStr, size_t* arEnd, bool aCheckConversion=true)
{
   
   T res = 0;
   try
   {
      if constexpr (midas::type_traits::IsComplexV<T>)
      {
         res = detail::From_String_Impl<T>(aStr, arEnd);
         // Since conversion to complex is not handled as above with some std::...
         // We manually set arEnd to nullpointer to silence warnings.
         arEnd = nullptr;
      }
      {
         res = detail::From_String_Impl<T>(aStr, arEnd);
      }
   }
   catch (const std::invalid_argument& ia)
   {
      MIDASERROR("Tried to read : " + aStr
               , "Invalid Argument : " + std::string(ia.what()));
   }
   catch (const std::out_of_range& oor)
   {
      MIDASERROR("Tried to read : " + aStr
               , "Out of Range error : " + std::string(oor.what()));
   }
   if(arEnd != nullptr && *arEnd != aStr.size() && aCheckConversion)
   {
      MidasWarning("Didn't read all of the string : \""+aStr+"\" in FromString conversion to "+typeid(T).name()+
                          " "+ __FILE__+" "+std::to_string(__LINE__));
   }
   return res;
}

/**
 *
 **/
template<class T>
Optional<T> FromStringOptional(const std::string& aStr, size_t* arEnd)
{
   
   T res = 0;
   try
   {
      res = detail::From_String_Impl<T>(aStr, arEnd);
   }
   catch (...)
   {
      return {};
   }
   if(arEnd != nullptr && *arEnd != aStr.size())
   {
      MidasWarning("Didn't read all of the string : \""+aStr+"\" in FromString conversion to "+typeid(T).name()+
                          " "+ __FILE__+" "+std::to_string(__LINE__));
   }
   return {res};
}

/**
 * Interface
 **/
template<class T>
T FromString(const std::string& aStr, bool aCheckConversion=true)
{
   size_t test = 0;
   return FromString<T>(aStr, &test, aCheckConversion);
}

template<>
inline std::string FromString(const std::string& aStr, bool aCheckConversion)
{
   return aStr;
}

template<class T>
Optional<T> FromStringOptional(const std::string& aStr)
{
   size_t test;
   return FromStringOptional<T>(aStr, &test);
}

} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_FOR_STRING */
