/**
************************************************************************
* 
* @file                BaseExpression.h
*
* Created:             16-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Base Class for the expressions
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BASEEXPRESSION_H
#define BASEEXPRESSION_H
#include <string>
#include <vector>
#include <map>
#include <memory>

namespace midas
{
namespace util
{
class BaseExpression
{
   public:
      virtual ~BaseExpression() = default;
      static std::vector<std::string> EvalExpression(const std::string& aStr, char aSeperator = ' ');
   protected:
      BaseExpression()
      {
      }
   private:
      virtual std::vector<std::string> Apply(const std::vector<std::vector<std::string> >& aStr, char aSeperator = ' ') const = 0;
};
} /*namespace util*/
} /*namespace midas*/
#endif /*BASEEXPRESSION_H*/
