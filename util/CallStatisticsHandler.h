#ifndef CALLSTATISTICSHANDLER_H_INCLUDED
#define CALLSTATISTICSHANDLER_H_INCLUDED

#include <unordered_map>
#include <map>
#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>

/**
 * @class CallTimer
 **/
class CallTimer
{
   private:
      using clock_type = std::chrono::high_resolution_clock;

      std::chrono::time_point<clock_type> mStart;
      double& mTime;

   public:
      /**
       *
       **/
      CallTimer(double& aTime)
         : mTime(aTime)
      {
         mStart = clock_type::now();
      }

      /**
       *
       **/
      ~CallTimer()
      {
         auto now = clock_type::now();
         //std::cout << now.count() << std::endl;
         mTime += std::chrono::duration<double>(now - mStart).count();
      }
};

/**
 * @class CallStatisticsHandler   
 **/
class CallStatisticsHandler
{
   private:
      using count_type = std::pair<unsigned long, double>;
      using mark_type  = const char*;
      using mark_map_type = std::map<std::pair<int, const char*>, count_type>;
      using call_map_type = std::unordered_map<std::string, mark_map_type>; 
      
      call_map_type mCallMap;
      std::ostream& mOs = std::cout;
      const std::string mSep = "!SEP!";

      CallStatisticsHandler() = default;

   public:
      /**
       *
       **/
      CallStatisticsHandler(const CallStatisticsHandler&) = delete; 
      
      /**
       *
       **/
      ~CallStatisticsHandler()
      {
         PrintStatistics();
      }

      /**
       *
       **/
      CallTimer LogCall(const char* file, int line, const char* func, const char* mark)
      {
         std::string token = std::string(file) + mSep + std::string(func);
         ++(mCallMap[token][{line, mark}].first);
         return CallTimer(mCallMap[token][{line, mark}].second);
      }

      /**
       *
       **/
      void PrintStatistics() const
      {
         auto width = 30;
         auto num_col = 5;
         auto bar_width = num_col*width + 4;
         auto space = "   ";
         mOs << std::left << std::scientific << std::setprecision(3);
         mOs << " +++++ *begin*  Function call statistics *begin* +++++ " << "\n\n";
         for(auto& function : mCallMap)
         {
            auto file = function.first.substr(0, function.first.find(mSep)); 
            auto func = function.first.substr(function.first.find(mSep) + mSep.size());
            mOs << space << "+" << std::string(bar_width, '-') << "+\n";
            mOs << space << "| file: " << std::setw(num_col*width - 3) << file << "|\n";
            mOs << space << "| func: " << std::setw(num_col*width - 3) << func << "|\n";
            mOs << space << "+" << std::string(bar_width, '-') << "+\n";
            mOs << space << "| "
                << std::setw(width) << "mark" << " "
                << std::setw(width) << "line" << " "
                << std::setw(width) << "num_calls" << " "
                << std::setw(width) << "time/s"
                << std::setw(width) << "time pr. call/s"
                << "|\n";
            mOs << space << "+" << std::string(bar_width, '-') << "+\n";
            for(auto& mark : function.second)
            {
               mOs << space << "| "
                   << std::setw(width) << mark.first.second << " " 
                   << std::setw(width) << mark.first.first  << " " 
                   << std::setw(width) << mark.second.first << " "
                   << std::setw(width) << mark.second.second
                   << std::setw(width) << mark.second.second/mark.second.first
                   <<"|\n";
            }
            mOs << space << "+" << std::string(bar_width, '-') << "+\n\n";
         }
         mOs << " +++++   *end*  Function call statistics  *end*  +++++ " << "\n";
         mOs << std::flush;
         mOs << std::right;
      }
      
      /**
       *
       **/
      friend CallStatisticsHandler& get_CallStatisticsHandler();
};

/**
 *
 **/
inline CallStatisticsHandler& get_CallStatisticsHandler()
{
   static CallStatisticsHandler callstat;
   return callstat;
}

/**
 *
 **/
// define some MACROs used later
#define CONCAT_IMPL( x, y ) x##y
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )

#ifdef __COUNTER__
#define ANONYMOUS_VARIABLE(str) \
   MACRO_CONCAT(str, __COUNTER__)
#else /* __COUNTER__ */
#define ANONYMOUS_VARIABLE(str) \
   MACRO_CONCAT(str, __LINE__)
#endif /* __COUNTER__ */

/**
 * Interface MACRO
 **/
#ifdef MIDAS_CALL_STATISTICS
#define LOGCALL(A) \
   auto ANONYMOUS_VARIABLE(CALLSTATISTICSHANDLER) = get_CallStatisticsHandler().LogCall(__FILE__, __LINE__, __PRETTY_FUNCTION__, A)
#else /* MIDAS_CALL_STATISTICS */
#define LOGCALL(A) 
#endif /* MIDAS_CALL_STATISTICS */

#endif /* CALLSTATISTICSHANDLER_H_INCLUDED */
