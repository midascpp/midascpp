/**
************************************************************************
* 
* @file                MidasStream.h 
*
* Created:             04.03.2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk)
*
* Short Description:   Handle the midas output, made for solving output
*                      issues with MPI parallellization
* 
* Last modified: Thu Mar 04, 2010  08:38AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASSTREAM_H
#define MIDASSTREAM_H 

// std headers
#include<iostream>
#include<iomanip>
#include<sstream>
#include<streambuf>
#include<string>
#include<ostream>
#include<mutex>

#ifdef VAR_MPI
#include<mpi.h>
#endif /* VAR_MPI */

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "libmda/numeric/float_eq.h"
#include "util/Io_fwd.h"

extern bool gDebug;

/**
 * Stream that will pipe to Midas.out.
 **/
class MidasStreamBuf
   :  public std::stringbuf
{
   private:
      //!
      std::ostream& output;

   public:
      //!
      bool mMute;
      //!
      bool mOnlyMaster;

      //! Constructor Constructor
      MidasStreamBuf(std::ostream& str, bool aOnlyMaster = true) 
         :  output(str)
         ,  mMute(false)
         ,  mOnlyMaster(aOnlyMaster)
      {
      }
   
      ///< It should be adequate to provide our own sync method
      ///< for the derived buffer type, right?
      // 1) Output the buffer
      // 2) Reset the buffer
      // 3) flush output stream
      virtual int sync ( );
      //{
      //   ///< following two lines for avoiding
      //   ///< multiple output in Midas.out file
      //   //output << "In MidasStream, gMpiRank is: " << gMpiRank << endl;
      //   if(!mMute && (!mOnlyMaster || midas::mpi::GlobalRank() == 0)) 
      //   {
      //      output << str();
      //      output.flush();
      //   }

      //   str("");
      //   return 0;
      //}
};


/**
 * Specialized Midas stream.
 **/
class MidasStream
   : public std::ostream
{
   private:
      ///< A stream buffer that works with mpi parallelization
      ///< private class as it is probably not going to be used
      ///< anywhere else...
      using mutex_type = std::recursive_mutex;
      
      //! Stream mutex (for concurrent writing to stream)
      mutex_type mStreamMutex;
      //! Bool designating whether to lock stream before write out
      bool mLockStream = true;

   public:
      //! 
      MidasStreamBuf& mStreamBuf;
      
      //! Constructor
      MidasStream(MidasStreamBuf&);
      
      void Mute() 
      { 
         std::ostream::flush(); 
         mStreamBuf.mMute = true;
      }

      void Unmute()
      {
         mStreamBuf.mMute = false;
      }

      bool Muted() const
      {
         return mStreamBuf.mMute;
      }

      template<class T>
      MidasStream& operator<<(T&& t)
      {
         // Lock the stream of requested
         std::unique_lock<mutex_type> stream_lock(mStreamMutex, std::defer_lock);
         if(mLockStream)
         {
            stream_lock.lock();
         }
         
         // Do write out
         dynamic_cast<std::ostream&>(*this) << t;

         return *this;
      }

      MidasStream& operator<<(std::ostream& (*f)(std::ostream&))
      { 
         // Lock the stream if requested
         std::unique_lock<mutex_type> stream_lock(mStreamMutex, std::defer_lock);
         if(mLockStream)
         {
            stream_lock.lock();
         }
         
         // Do write out
         dynamic_cast<std::ostream&>(*this) << f;

         return *this;
      }
};

#endif /* MIDASSTREAM_H */
