/**
 *******************************************************************************
 * 
 * @file    MatRepUtils.cc
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#include <cassert>
#include "util/Io_fwd.h"
#include "inc_gen/TypeDefs.h"
#include "util/matrep/MatRepUtils.h"
#include "input/ModeCombiOpRange.h"

//==============================================================================
// NON-TEMPLATED DEFINITIONS
//==============================================================================
namespace midas::util::matrep
{

/***************************************************************************//**
 * Example: `std::vector<In>{3,1,2} --> std::set<Uin>{1,2,3}`.
 *
 * @note
 *    Hard error if negative integer is encountered.
 ******************************************************************************/
std::set<Uin> SetFromVec
   (  const std::vector<In>& arV
   )
{
   std::set<Uin> s;
   for(const auto& i: arV)
   {
      if (i < 0) MIDASERROR("Negative integer not allowed.");
      s.emplace_hint(s.end(), i);
   }
   return s;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *     arV    = { 2, 4, 3}
 *     return = {12, 3, 1}
 * ~~~
 *
 * @param[in] arV
 *    Vector of integers.
 * @return
 *    Cumulative product in descending/reverse order, i.e. where the `i`'th
 *    element is the product of the `i-1` last elements of arV.
 *    (The last element is 1.)
 ******************************************************************************/
std::vector<Uin> CumulativeProduct
   (  const std::vector<Uin>& arV
   )
{
   std::vector<Uin> res;
   res.reserve(arV.size());
   Uin p = Product(arV);
   for(const auto& v: arV)
   {
      p /= v;
      res.emplace_back(p);
   }
   return res;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *       arDims    = {2,3,4}
 *       aAbsIndex = 14 (which is < 2*3*4 = 24)
 *       return    = {1,0,2} because 14 = 1*(3*4) + 0*(4) + 2
 * ~~~
 *
 * @param[in] aAbsIndex
 *    An absolute index in the range `[0; Product(arDims))`.
 * @param[in] arDims
 *    The dimensions of each mode for the conversion to multiindex.
 * @return
 *    The corresponding multiindex based on the given dimensions.
 ******************************************************************************/
std::vector<Uin> MultiIndex
   (  Uin aAbsIndex
   ,  const std::vector<Uin>& arDims
   )
{
   Uin prod_dim = Product(arDims);
   if (!(aAbsIndex < prod_dim)) MIDASERROR("Index >= full dim.");
   std::vector<Uin> multi_index;
   multi_index.reserve(arDims.size());
   for(const auto& d: arDims)
   {
      prod_dim /= d;
      multi_index.emplace_back(aAbsIndex/prod_dim);
      aAbsIndex %= prod_dim;
   }
   return multi_index;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *       arDims            = {2,3,4}
 *
 *       arMultiIndex[in]  = {0,0,0}
 *       arMultiIndex[out] = {0,0,1}
 *
 *       arMultiIndex[in]  = {1,0,2}
 *       arMultiIndex[out] = {1,0,3}
 *
 *       arMultiIndex[in]  = {1,0,3}
 *       arMultiIndex[out] = {1,1,0}
 *
 *       arMultiIndex[in]  = {1,2,3}
 *       arMultiIndex[out] = {0,0,0}
 * ~~~
 *
 * @param[in,out] arMultiIndex
 *    Multiindex to be incremented. Must have same size as arDims.
 * @param[in] arDims
 *    The dimensions of each mode for the conversion to multiindex. All values
 *    are assumed to be > 0.
 ******************************************************************************/
void IncrMultiIndex
   (  std::vector<Uin>& arMultiIndex
   ,  const std::vector<Uin>& arDims
   )
{
   assert(arMultiIndex.size() == arDims.size());
   Uin i = arMultiIndex.size();
   while(i-- > 0)
   {
      ++arMultiIndex[i];
      arMultiIndex[i] %= arDims[i];
      if(arMultiIndex[i] != 0)
      {
         break;
      }
   }
}

/***************************************************************************//**
 * Example:
 * ~~~
 *       dims              = { 2,3,4,2}
 *       cumprod           = {24,8,2,1}
 *
 *       s                 = { 0,1,  3} // some indices in range [0;dims.size())
 *       s_dims            = { 2,3,  2} // corresponding subset of dims
 *       s_cumprod         = {24,8,  1} // corresponding subset of cumprod
 *       multindex         = { 0,2,  0} // multiindex within s_dims
 *
 *       IncrMultiIndex(multiindex, s_dims, s_cumprod);
 *          // multiindex: {0,2,0} 
 *          //         --> {0,2,1}
 *          // return    =  0*24 + 0*8 + 1*1 = 1
 *       IncrMultiIndex(multiindex, s_dims, s_cumprod);
 *          // multiindex: {0,2,1} 
 *          //         --> {1,0,0}
 *          // return    =  1*24 - 2*8 - 1*1 = 7
 * ~~~
 *
 * @param[in,out] arMultiIndex
 *    Multiindex to be incremented.
 * @param[in] arDims
 *    The dimensions of each mode for the conversion to multiindex. All values
 *    are assumed to be > 0. Must have same size as arMultiIndex.
 * @param[in] arCumProd
 *    The cumulative product of arDims (see CumulativeProduct()).
 *    Must have same size as arMultiIndex.
 * @return
 *    The increment in absolute index corresponding to arCumProd.
 ******************************************************************************/
In IncrMultiIndex
   (  std::vector<Uin>& arMultiIndex
   ,  const std::vector<Uin>& arDims
   ,  const std::vector<Uin>& arCumProd
   )
{
   assert(arMultiIndex.size() == arDims.size());
   assert(arMultiIndex.size() == arCumProd.size());

   In incr = 0;
   Uin i = arMultiIndex.size();
   while(i-- > 0)
   {
      ++arMultiIndex[i];
      arMultiIndex[i] %= arDims[i];
      if(arMultiIndex[i] != 0)
      {
         incr += arCumProd[i];
         break;
      }
      else
      {
         incr -= (arDims[i]-1)*arCumProd[i];
      }
   }
   return incr;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arFullMultiIndex = {5,2,3}
 *    arFullModeCombi  = {1,4,7}
 *    arSubModeCombis  = {{4},{1,7}}
 *    return           = {{2},{5,3}}
 * ~~~
 *
 * @param[in] arFullMultiIndex
 *    Full multi-index to split.
 * @param[in] arFullModeCombi
 *    The mode combination of the full index.
 * @param[in] arSubModeCombis
 *    Sub-combinations to split the full index into.
 * @return
 *    The sub-multiindices.
 ******************************************************************************/
std::vector<std::vector<Uin>> SplitMultiIndex
   (  const std::vector<Uin>& arFullMultiIndex
   ,  const std::set<Uin>& arFullModeCombi
   ,  const std::vector<std::set<Uin>>& arSubModeCombis
   )
{
   if (arFullModeCombi.size() != arFullMultiIndex.size())
   {
      std::stringstream ss;
      ss << "arFullModeCombi.size() (which is " << arFullModeCombi.size()
         << ") != arFullMultiIndex.size() (which is " << arFullMultiIndex.size()
         << ")."
         ;
      MIDASERROR(ss.str());
   }

   std::vector<std::vector<Uin>> result;
   result.reserve(arSubModeCombis.size());
   const auto beg = arFullModeCombi.begin();
   const auto end = arFullModeCombi.end();
   auto it_mc = beg;
   for(const auto& sub_mc: arSubModeCombis)
   {
      std::vector<Uin> tmp;
      tmp.reserve(sub_mc.size());
      for(const auto& i: sub_mc)
      {
         if ((it_mc = std::find(beg, end, i)) != end)
         {
            try
            {
               tmp.emplace_back(arFullMultiIndex.at(std::distance(beg,it_mc)));
            }
            catch(const std::out_of_range& oor)
            {
               MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
               return result; // To quench compiler warning.
            }
         }
         else
         {
            std::stringstream ss;
            ss << "The arFullModeCombi " << arFullModeCombi << " doesn't contain " << sub_mc << ".";
            MIDASERROR(ss.str());
         }
      }
      result.emplace_back(std::move(tmp));
   }
   return result;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *       arDims       = {3,5,2}
 *       arMultiIndex = {1,3,0}
 *       return       = 16 (computed as 1*(5*2) + 3*(2) + 0)
 * ~~~
 *
 * @param[in] arMultiIndex
 *    A multiindex `{i_0, ..., i_{M-1}}`, `M` being the number of provided
 *    dimensions, and each `i_k` being in the range `[0;dim(k))`.
 * @param[in] arDims
 *    Dimensions of each mode for converting the multiindex to an absolute one.
 * @return
 *    The absolute index in the range `[0;Product(arDims))`.
 ******************************************************************************/
Uin AbsIndex
   (  const std::vector<Uin>& arMultiIndex
   ,  const std::vector<Uin>& arDims
   )
{
   if (!(arMultiIndex.size() == arDims.size())) MIDASERROR("Bad multi-index size.");
   Uin index = 0;
   Uin prod_dim = Product(arDims);
   for(Uin m = 0; m < arDims.size(); ++m)
   {
      prod_dim /= arDims.at(m);
      if (!(arMultiIndex.at(m) < arDims.at(m))) MIDASERROR("Index of mode is larger than Dim(m).");
      index += arMultiIndex.at(m) * prod_dim;
   }
   return index;
}

/***************************************************************************//**
 * @param[in] arV
 *    Vector of non-negative integers.
 * @return
 *    Product of the integers in the vector. 1 if vector is empty.
 ******************************************************************************/
Uin Product
   (  const std::vector<Uin>& arV
   )
{
   Uin p = 1;
   for(const auto& v: arV)
   {
      p *= v;
   }
   return p;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    aNumModes = 6
 *    arModes   = {0,2}
 *    return    = {1,3,4,5}
 * ~~~
 *
 * @param[in] aNumModes
 *    Make complementary set within `[0;aNumModes)`.
 * @param[in] arModes
 *    Find modes complementary to those.
 * @return
 *    The modes from `[0;aNumModes)` _not_ in arModes.
 ******************************************************************************/
std::set<Uin> ComplementaryModes
   (  const Uin aNumModes
   ,  const std::set<Uin>& arModes
   )
{
   if (!arModes.empty())
   {
      if (!(*arModes.rbegin() < aNumModes)) MIDASERROR("A mode is too large.");
   }
   std::set<Uin> s;
   for(Uin m = 0; m < aNumModes; ++m)
   {
      if (arModes.find(m) == arModes.end())
      {
         s.emplace_hint(s.end(), m);
      }
   }
   return s;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arModes = {1,3}
 *    arDims  = {2,4,3,6,5}
 *    return  = {4,6}
 * ~~~
 *
 * @param[in] arModes
 *    The indices of arDims for which to extract elements.
 * @param[in] arDims
 *    All the integers (dimensions).
 * @return
 *    The elements of arDims that has indices arModes.
 ******************************************************************************/
std::vector<Uin> SubsetDims
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arDims
   )
{
   if (!arModes.empty())
   {
      if (!(*arModes.rbegin() < arDims.size())) MIDASERROR("A mode is too large.");
   }
   std::vector<Uin> v;
   v.reserve(arModes.size());
   for(const auto& m: arModes)
   {
      v.emplace_back(arDims.at(m));
   }
   return v;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    aVec     = {2,3,4}
 *    aAddend  = -1
 *    return   = {1,2,3}
 * ~~~
 *
 * @note
 *    Hard error if adding (a negative) aAddend would cause any of the integers
 *    of aVec to become negative.
 *
 * @param[in] aVec
 *    Values to be shifted.
 * @param[in] aAddend
 *    Add this number to all elements of aVec.
 * @return
 *    Newly constructor vector whose values equal those of aVec plus aAddend.
 ******************************************************************************/
std::vector<Uin> ShiftVals
   (  std::vector<Uin> aVec
   ,  const In aAddend
   )
{
   for(auto& d: aVec)
   {
      if (In(d) + aAddend < 0) MIDASERROR(std::to_string(d)+"+("+std::to_string(aAddend)+") < 0.");
      d += aAddend;
   }
   return aVec;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arModes = {0,2}
 *    arDims  = {3,4,5}
 *    return  = { {1,1}
 *              , {1,2}
 *              , {1,3}
 *              , {1,4}
 *              , {2,1}
 *              , {2,2}
 *              , {2,3}
 *              , {2,4}
 *              }
 * ~~~
 *
 * @note
 *    Hard error if there are more modes than dimensions, or if a mode number
 *    is out-of-range w.r.t. the number of dimensions.
 *
 * @param[in] arModes
 *    Mode combination to get excitation indices for.
 * @param[in] arDims
 *    Dimensions of direct-product space; possible excitation indices for each
 *    mode `m` are in the range `[1;arDims[m])`.
 * @return
 *    Vector containing all possible excitation indices for given mode
 *    combination and dimensions; sorted in accordance with MatRepVibOper
 *    logic.
 ******************************************************************************/
std::vector<std::vector<Uin>> ModeCombiExciIndices
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arDims
   )
{
   // Assertions.
   if (arModes.size() > arDims.size()) MIDASERROR("Num. modes > num. dims.");
   if (!arModes.empty())
   {
      if (*arModes.rbegin() >= arDims.size()) MIDASERROR("Mode too large.");
   }

   std::vector<std::vector<Uin>> v;

   const auto& exci_dims = ShiftVals(SubsetDims(arModes, arDims), -1);
   const Uin n_exci = Product(exci_dims);

   for(Uin i_exci = 0; i_exci < n_exci; ++i_exci)
   {
      v.emplace_back(ShiftVals(matrep::MultiIndex(i_exci, exci_dims), +1));
   }

   return v;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arMcr  = { {}, {2}, {0,1} }
 *    arDims = {3,4,3}
 *    return = {  ({}   ; {})
 *             ,  ({2}  ; {1}, {2}, {3})
 *             ,  ({0,1}; {1,1}, {1,2}, {2,1}, {2,2})
 *             }
 * ~~~
 *
 * @note
 *    Hard error if for some ModeCombi there are more modes than dimensions, or
 *    if a mode number is out-of-range w.r.t. the number of dimensions.
 *
 * @param[in] arMcr
 *    Range to get excitation indices for.
 * @param[in] arDims
 *    Dimensions of direct-product space; possible excitation indices for each
 *    mode `m` are in the range `[1;arDims[m])`.
 * @return
 *    Vector of pairs of mode combinations and corresponding excitation
 *    indices.
 ******************************************************************************/
std::vector<std::pair<std::set<Uin>, std::vector<std::vector<Uin>>>> McrExciIndices
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arDims
   )
{
   std::vector<std::pair<std::set<Uin>, std::vector<std::vector<Uin>>>> v;
   v.reserve(arMcr.Size());
   for(const auto& mc: arMcr)
   {
      auto s = SetFromVec(mc.MCVec());
      auto v_ind = ModeCombiExciIndices(s, arDims);
      v.emplace_back(std::make_pair(std::move(s), std::move(v_ind)));
   }
   return v;
}

} /* namespace midas::util::matrep */


//==============================================================================
// TEMPLATE INSTANTIATIONS
//==============================================================================
#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "util/matrep/MatRepUtils_Impl.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

// Define instantiation macro.
#define INSTANTIATE_GETELEMENTS(T,CONT_T) \
   namespace midas::util::matrep \
   { \
      template std::vector<T> GetElements(const CONT_T<T>&, Uin, Uin); \
   } /* namespace midas::util::matrep */

#define INSTANTIATE_ORGANIZEMCRSPACEVEC(T,CONT_T) \
   namespace midas::util::matrep \
   { \
      template std::vector<std::vector<T>> OrganizeMcrSpaceVec \
         (  const std::vector<Uin>& arDims \
         ,  const ModeCombiOpRange& arMcr \
         ,  const CONT_T<T>& arVec \
         ); \
   } /* namespace midas::util::matrep */

#define INSTANTIATE_MCRORGANIZEDTOSTACKEDVEC(T,CONT_T) \
   namespace midas::util::matrep \
   { \
      template CONT_T<T> McrOrganizedToStackedVec<T,CONT_T> \
         (  const std::vector<std::vector<T>>& arVec \
         ); \
   } /* namespace midas::util::matrep */

#define INSTANTIATE_MATREPUTILS(T, CONT_T) \
   INSTANTIATE_GETELEMENTS(T, CONT_T); \
   INSTANTIATE_ORGANIZEMCRSPACEVEC(T, CONT_T); \
   INSTANTIATE_MCRORGANIZEDTOSTACKEDVEC(T, CONT_T);

#define INSTANTIATE_EXTENDTOSECONDARYSPACE(T) \
   namespace midas::util::matrep \
   { \
      template std::vector<std::vector<T>> ExtendToSecondarySpace \
         (  const ModeCombiOpRange& arMcr \
         ,  const std::vector<Uin>& arDimsReduced \
         ,  const std::vector<Uin>& arDimsExtended \
         ,  const std::vector<std::vector<T>>& arParamsReduced \
         ); \
   } /* namespace midas::util::matrep */ \

// Instantiations.
INSTANTIATE_MATREPUTILS(Nb,GeneralMidasVector);
INSTANTIATE_MATREPUTILS(std::complex<Nb>,GeneralMidasVector);
INSTANTIATE_MATREPUTILS(Nb,GeneralDataCont);
INSTANTIATE_MATREPUTILS(std::complex<Nb>,GeneralDataCont);

INSTANTIATE_ORGANIZEMCRSPACEVEC(Nb, GeneralTensorDataCont);
INSTANTIATE_ORGANIZEMCRSPACEVEC(std::complex<Nb>, GeneralTensorDataCont);

INSTANTIATE_EXTENDTOSECONDARYSPACE(Nb);
INSTANTIATE_EXTENDTOSECONDARYSPACE(std::complex<Nb>);

#undef INSTANTIATE_MATREPUTILS

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
