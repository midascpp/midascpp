/**
 *******************************************************************************
 * 
 * @file    OperMat.cc
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "util/matrep/OperMat.h"
#include "util/matrep/OperMat_Impl.h"

#ifndef DISABLE_PRECOMPILED_TEMPLATES

// Define instantiation macro.
#define INSTANTIATE_OPERMAT(T) \
   namespace midas::util::matrep \
   { \
      template class OperMat<T>; \
      template typename OperMat<T>::vec_t OperMat<T>::MatVecMult<false>(const vec_t&) const; \
      template typename OperMat<T>::vec_t OperMat<T>::MatVecMult<true>(const vec_t&) const; \
   } /* namespace midas::util::matrep */ \

// Instantiations.
INSTANTIATE_OPERMAT(Nb);
INSTANTIATE_OPERMAT(std::complex<Nb>);

#undef INSTANTIATE_OPERMAT

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
