/**
 *******************************************************************************
 * 
 * @file    MatRepUtils_Decl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPUTILS_DECL_H_INCLUDED
#define MATREPUTILS_DECL_H_INCLUDED

// Standard headers.
#include <set>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/Error.h"

// Forward declarations.
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;

namespace midas::util::matrep
{

//! Conversion from e.g. ModeCombi::MCVec().
std::set<Uin> SetFromVec(const std::vector<In>&);

//! Returns cumulative products of elements of given vector, in descending order.
std::vector<Uin> CumulativeProduct(const std::vector<Uin>&);

//! Converts abs. index to multiindex within subspace with given dimensions.
std::vector<Uin> MultiIndex(Uin, const std::vector<Uin>&);

//! Increment multiindex by 1 within subspace with given dimensions. Sizes must match.
void IncrMultiIndex(std::vector<Uin>&, const std::vector<Uin>&);

//! Increment multiindex (like the other one); return corresponding incr. in abs. index.
In IncrMultiIndex(std::vector<Uin>&, const std::vector<Uin>&, const std::vector<Uin>&);

//! Split a multiindex into sub-multiindices according to sub-ModeCombi%s.
std::vector<std::vector<Uin>> SplitMultiIndex
   (  const std::vector<Uin>& arFullMultiIndex
   ,  const std::set<Uin>& arFullModeCombi
   ,  const std::vector<std::set<Uin>>& arSubModeCombis
   );

//! Converts multiindex to abs. index within subspace with given dimensions.
Uin AbsIndex(const std::vector<Uin>&, const std::vector<Uin>&);

//! Product of numbers in vector.
Uin Product(const std::vector<Uin>&);

//! The modes in `[0;aNumModes)` _not_ contained in argument.
std::set<Uin> ComplementaryModes
   (  const Uin aNumModes
   ,  const std::set<Uin>& arModes
   );

//! A smaller vector with just those dimensions corresponding to the given modes.
std::vector<Uin> SubsetDims
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arDims
   );

//! New vector with values shifted by same constant.
std::vector<Uin> ShiftVals
   (  std::vector<Uin> aVec
   ,  const In aAddend
   );

//! Get vector of all possible excitation indices for given modes.
std::vector<std::vector<Uin>> ModeCombiExciIndices
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arDims
   );

//! Vector of pairs of {mode combination, all mode combination excitation indices}.
std::vector<std::pair<std::set<Uin>, std::vector<std::vector<Uin>>>> McrExciIndices
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arDims
   );

//@{
//! Utility functions for OrganizeMcrSpaceVec.
template<typename T>
std::vector<T> GetElements(const GeneralMidasVector<T>&, Uin aBegin, Uin aEnd);
template<typename T>
std::vector<T> GetElements(const GeneralDataCont<T>&, Uin aBegin, Uin aEnd);
//@}

//! Organize the MCR-space ordered vector into std::vector%s per ModeCombi.
template<typename T, template<typename...> class CONT_T>
std::vector<std::vector<T>> OrganizeMcrSpaceVec
   (  const std::vector<Uin>& arDims
   ,  const ModeCombiOpRange& arMcr
   ,  const CONT_T<T>& arVec
   );

//! Inverse operation of OrganizeMcrSpaceVec().
template<typename T, template<typename...> class CONT_T>
CONT_T<T> McrOrganizedToStackedVec
   (  const std::vector<std::vector<T>>& arVec
   );

//! Extend params. in a reduced virtual space into a larger space with 0s in the secondary space.
template<typename T>
std::vector<std::vector<T>> ExtendToSecondarySpace
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arDimsReduced
   ,  const std::vector<Uin>& arDimsExtended
   ,  const std::vector<std::vector<T>>& arParamsReduced
   );

} /* namespace midas::util::matrep */

#endif/*MATREPUTILS_DECL_H_INCLUDED*/
