/**
 *******************************************************************************
 * 
 * @file    MatRepTransformers_Decl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPTRANSFORMERS_DECL_H_INCLUDED
#define MATREPTRANSFORMERS_DECL_H_INCLUDED

// Standard headers.
#include <array>
#include <set>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"

// Forward declarations.
class ModeCombiOpRange;
class OpDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;
template<typename> class GeneralMidasMatrix;
template<typename> class GeneralDataCont;
namespace midas::util::matrep
{
   class SparseClusterOper;
   class ShiftOperBraketLooper;
   template<typename> class OperMat;
}

namespace midas::util::matrep
{
   //! Fast (matrix-free) evaluation of `<bra|E^m_{rs}|ket>` expressions.
   template<bool CONJ_BRA, typename T>
   T ShiftOperBraket
      (  const GeneralMidasVector<T>& arBra
      ,  const GeneralMidasVector<T>& arKet
      ,  const std::vector<Uin>& arDims
      ,  const std::set<Uin>& arModes
      ,  const std::vector<Uin>& arCrea
      ,  const std::vector<Uin>& arAnni
      );

   //! \f$ exp(aT)|v> \f$ (or \f$ <v|exp(aT) \f$ for DEEXC) with only mat.-vec. mults.
   template<bool DEEXC = false, bool CONJ = false, typename T>
   GeneralMidasVector<T> TrfExpClusterOper
      (  const SparseClusterOper& arOper
      ,  const GeneralMidasVector<T>& arAmpls
      ,  GeneralMidasVector<T> aVec
      ,  T aCoef = T(1)
      );

   //! \f$ aT|v> \f$ (or \f$ <v|aT \f$ for DEEXC) with only mat.-vec. mults.
   template<bool DEEXC = false, bool CONJ = false, typename T>
   GeneralMidasVector<T> TrfLinClusterOper
      (  const SparseClusterOper& arOper
      ,  const GeneralMidasVector<T>& arAmpls
      ,  GeneralMidasVector<T> aVec
      ,  T aCoef = T(1)
      ,  Uin aPower = 1
      );

   //! VCC error vector with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfVccErrVec
      (  const OperMat<T>& arOperMat
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      );

   //! VCC error vector; \f$ \exp(-T) H \exp(T) \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasVector<T> TrfVccErrVec
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      );

   //! VCI transformation with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfVci
      (  const OperMat<T>& arOperMat
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arCCoefs
      );

   //! VCI transformation; \f$ H C \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasVector<T> TrfVci
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arCCoefs
      );

   //! VCC eta vector with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfVccEtaVec
      (  const OperMat<T>& arOperMat
      ,  const ShiftOperBraketLooper& arLooper
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      );

   //! VCC eta vector; \f$ \eta_\nu = \langle\Phi\vert \exp(-T)[H,\tau_\nu]\exp(T) \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasVector<T> TrfVccEtaVec
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      );

   //! Right-hand VCC-Jacobian with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfVccRJac
      (  const OperMat<T>& arOperMat
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      ,  const GeneralMidasVector<T>& arRCoefs
      ,  const bool aZeroRefElemResult = true
      );

   //! Right-hand VCC-Jacobian; \f$ \exp(-T) [H,R] \exp(T) \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasVector<T> TrfVccRJac
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      ,  const std::vector<std::vector<T>>& arRCoefs
      ,  const bool aZeroRefElemResult = true
      );

   //! Left-hand VCC-Jacobian with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfVccLJac
      (  const OperMat<T>& arOperMat
      ,  const ShiftOperBraketLooper& arLooper
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      ,  const GeneralMidasVector<T>& arLCoefs
      );

   //! Left-hand VCC-Jacobian; \f$ \langle\Phi\vert L \exp(-T)[H,\tau_\nu]\exp(T) \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasVector<T> TrfVccLJac
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      ,  const std::vector<std::vector<T>>& arLCoefs
      );

   //! Explicit VCC-Jacobian; \f$ \langle\tau_\mu\vert \exp(-T)[H,\tau_\nu]\exp(T) \vert\Phi\rangle \f$.
   template<typename T>
   GeneralMidasMatrix<T> ExplVccJac
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      ,  const bool aDisregardEmptyMc = true
      );

   //! Solve \f$ \mathbf{A}^T \mathbf{l} = -\mathbf{\eta} \f$ for precalculated matrix/vector.
   template<typename T>
   std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA
      (  const GeneralMidasMatrix<T>& arATransp
      ,  const GeneralMidasVector<T>& arMinusEta
      );

   //! Setup explicit VCC-Jacobian, then solve \f$ \mathbf{l}\mathbf{A} = -\mathbf{\eta} \f$.
   template<typename T>
   std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      );

   //! ExtVCC; H deriv. wrt. ext. ampl.; with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfExtVccHamDerExtAmp
      (  const OperMat<T>& arOperMat
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      ,  const GeneralMidasVector<T>& arSAmps
      );

   //! ExtVCC; H deriv. wrt. ext. ampl.; \f$ \exp(S)\exp(-T)H\exp(T)\vert\Phi\rangle\f$.
   template<typename T>
   GeneralMidasVector<T> TrfExtVccHamDerExtAmp
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      ,  const std::vector<std::vector<T>>& arSAmps
      );

   //! ExtVCC; H deriv. wrt. clust. ampl.; with pre-constructed operator matrix.
   template<typename T>
   GeneralMidasVector<T> TrfExtVccHamDerClustAmp
      (  const OperMat<T>& arOperMat
      ,  const ShiftOperBraketLooper& arLooper
      ,  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmps
      ,  const GeneralMidasVector<T>& arSAmps
      );

   //! ExtVCC; H deriv. wrt. clust. ampl.; \f$ \langle\Phi\vert\exp(S)\exp(-T)[H,\tau_\mu]\exp(T)\vert\Phi\rangle\f$.
   template<typename T>
   GeneralMidasVector<T> TrfExtVccHamDerClustAmp
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmps
      ,  const std::vector<std::vector<T>>& arSAmps
      );

   //! ExtVCC; the result of \f$ k^-_{\mu\nu} v_{\nu} \f$, the k being elemnents of exp(-S).
   template<typename T>
   GeneralMidasVector<T> TrfExtVccLeftExpmS
      (  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arSAmps
      ,  const GeneralMidasVector<T>& arVec
      );

   //! ExtVCC; the result of \f$ v_{\mu} k^-_{\mu\nu} \f$, the k being elemnents of exp(-S).
   template<typename T>
   GeneralMidasVector<T> TrfExtVccRightExpmS
      (  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arSAmps
      ,  const GeneralMidasVector<T>& arVec
      );

   //! The tensor direct product, given corresponding mode combinations and full space dimensions.
   template<typename T>
   std::pair<GeneralMidasVector<T>,std::set<Uin>> TensorDirectProduct
      (  const std::vector<Uin>& arDims
      ,  const std::vector<std::set<Uin>>& arSubMcs
      ,  const std::vector<GeneralMidasVector<T>>& arSubTensors
      );

   //! The result of \f$ C \vert \Phi \rangle \f$ in full space, direct implementation (no matrix).
   template<typename T>
   GeneralMidasVector<T> CRefFullSpace
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arCCoefs
      );

   //! The result of \f$ \exp(T) \vert \Phi \rangle \f$, i.e. VCC to VCI conversion.
   template<typename T>
   GeneralMidasVector<T> ExpTRef
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralMidasVector<T>& arTAmps
      ,  const Uin aMaxExciOut
      );

   //! Wrapper that just extract the T-amps as a GeneralMidasVector.
   template<typename T>
   GeneralMidasVector<T> ExpTRef
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralDataCont<T>& arTAmps
      ,  const Uin aMaxExciOut
      );

   //! Given `(1+C)|ref>`, find `T` so `exp(T)|ref> = (1+C)|ref>` (within MCR-space).
   template<typename T>
   GeneralMidasVector<T> InvExpTRef
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralMidasVector<T>& arCCoefs
      );

   //! Wrapper that just extract the C-coefs as a GeneralMidasVector.
   template<typename T>
   GeneralMidasVector<T> InvExpTRef
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralDataCont<T>& arCCoefs
      );

   //! _Inefficient_ impl. of \f$ \langle\Phi\vert (1+L)\exp(-T) \f$, i.e. VCC->VCI for Lambda state.
   template<typename T, template<typename> class CONT_TMPL>
   GeneralMidasVector<T> Ref1pLExpmT
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const CONT_TMPL<T>& arTAmps
      ,  const CONT_TMPL<T>& arLCoefs
      ,  const Uin aMaxExciOut
      );

   //! _Inefficient_ impl. of \f$ \langle\Phi\vert \exp(S)\exp(-T) \f$, i.e. VCC->VCI for EVCC bra.
   template<typename T, template<typename> class CONT_TMPL>
   GeneralMidasVector<T> RefExpSExpmT
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const CONT_TMPL<T>& arTAmps
      ,  const CONT_TMPL<T>& arSAmps
      ,  const Uin aMaxExciOut
      );

   //! VCC 1-mode dens. mats `rho_uv = <ref|(1+L)exp(-T) E_vu exp(T)|ref>`. (NB! `uv <-> vu` order)
   template<typename T>
   std::vector<GeneralMidasMatrix<T>> VccOneModeDensityMatrices
      (  const SparseClusterOper& arClustOper
      ,  const GeneralMidasVector<T>& arTAmpls
      ,  const GeneralMidasVector<T>& arLCoefs
      );

   //! VCC 1-mode dens. mats `rho_uv = <ref|(1+L)exp(-T) E_vu exp(T)|ref>`. (NB! `uv <-> vu` order)
   template<typename T>
   std::vector<GeneralMidasMatrix<T>> VccOneModeDensityMatrices
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmpls
      ,  const std::vector<std::vector<T>>& arLCoefs
      );

   //! Utility function for constructing half- and fully-transformed modal integrals.
   template<typename T>
   std::array<std::vector<std::vector<GeneralMidasMatrix<T>>>,3> TdmvccHalfAndFullyTransformedModalIntegrals
      (  const ModalIntegrals<T>& arModIntsPrimBas
      ,  const std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>>& arModalTransMats
      );

   //! Construct half-transformed mean-field matrices from TDMVCC theory.
   template<typename T>
   std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransOneModeOpers
      (  const OpDef& arOpDef
      ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfPrimTd
      ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfTdPrim
      );

   //! Construct half-transformed mean-field matrices from TDMVCC theory.
   template<typename T>
   std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransMeanFieldMatrices
      (  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModIntsTdBas
      ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfPrimTd
      ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfTdPrim
      ,  const SparseClusterOper& arClustOperTdBas
      ,  const GeneralMidasVector<T>& arTAmpls
      ,  const GeneralMidasVector<T>& arLCoefs
      ,  bool aSkipOneModeTerms
      );

   //! Construct half-transformed mean-field matrices from TDMCC theory.
   template<typename T>
   std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransMeanFieldMatrices
      (  const std::vector<Uin>& arNModalsTdBas
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<T>& arModIntsPrimBas
      ,  const std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>>& arModalTransMats
      ,  const ModeCombiOpRange& arMcr
      ,  const std::vector<std::vector<T>>& arTAmpls
      ,  const std::vector<std::vector<T>>& arLCoefs
      ,  bool aSkipOneModeTerms
      );

   //! Returns `GU + (I-P)(F^ rho^-1 - GU) = (I-P)F^ rho^-1 + Ug`.
   template<typename T>
   GeneralMidasMatrix<T> TdmvccUDotBaseExpression
      (  const GeneralMidasMatrix<T>& arU
      ,  const GeneralMidasMatrix<T>& arMeanFieldMatHalfTrfPrimTd
      ,  const GeneralMidasMatrix<T>& arProjTdActiveSpace
      ,  const GeneralMidasMatrix<T>& arDensMatInverse
      ,  const GeneralMidasMatrix<T>& arConstraintOperTdBas
      );

   //! Returns `WG + (rho^-1 F^' - WG)(I-P) = rho^-1 F^'(I-P) + gW`
   template<typename T>
   GeneralMidasMatrix<T> TdmvccWDotBaseExpression
      (  const GeneralMidasMatrix<T>& arW
      ,  const GeneralMidasMatrix<T>& arMeanFieldMatHalfTrfTdPrim
      ,  const GeneralMidasMatrix<T>& arProjTdActiveSpace
      ,  const GeneralMidasMatrix<T>& arDensMatInverse
      ,  const GeneralMidasMatrix<T>& arConstraintOperTdBas
      );

} /* namespace midas::util::matrep */

#endif/*MATREPTRANSFORMERS_DECL_H_INCLUDED*/
