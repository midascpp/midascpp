/**
 *******************************************************************************
 * 
 * @file    SparseClusterOper_Impl.h
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SPARSECLUSTEROPER_IMPL_H_INCLUDED
#define SPARSECLUSTEROPER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "util/InterfaceOpenMP.h"

namespace midas::util::matrep
{

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<bool DEEXC>
   const SparseClusterOper::index_t& SparseClusterOper::Indices
      (
      )  const
   {
      return std::get<DEEXC? 1: 0>(mIndices);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<bool DEEXC>
   const std::vector<Uin>& SparseClusterOper::MpiOmpRanges
      (
      )  const
   {
      return std::get<DEEXC? 1: 0>(mMpiOmpRanges);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<bool DEEXC, bool CONJ, typename T>
   SparseClusterOper::vec_tmpl<T> SparseClusterOper::Transform
      (  const vec_tmpl<T>& arAmpls
      ,  const vec_tmpl<T>& arVec
      )  const
   {
      // Assertions.
      if (arAmpls.Size() != this->NumAmpls() || arVec.Size() != this->FullDim())
      {
         std::stringstream ss;
         ss << "Size mismatch;\n"
            << "   arAmpls.Size() = " << arAmpls.Size() << '\n'
            << "   (expected      = " << this->NumAmpls() << ")\n"
            << "   arVec.Size()   = " << arVec.Size() << '\n'
            << "   (expected      = " << this->FullDim() << ")\n"
            ;
         MIDASERROR(ss.str());
      }

      const index_t& v_index = this->Indices<DEEXC>();
      const auto& v_ranges = MpiOmpRanges<DEEXC>();
      
      // Reserve and SetNewSizeWithinCapacity to avoid zero-initializing res;
      // the loop will anyway assign to all elements.
      vec_tmpl<T> res;
      res.Reserve(v_index.size());
      res.SetNewSizeWithinCapacity(res.Capacity());

      #pragma omp parallel
      {
         const Uin id_mpi_omp = midas::mpi::GlobalRank()*omp_get_max_threads() + omp_get_thread_num();
         const Uin i_beg = v_ranges.at(id_mpi_omp);
         const Uin i_end = v_ranges.at(id_mpi_omp + 1);
         for(Uin i = i_beg; i < i_end; ++i)
         {
            T val = 0;
            for(const auto& [ind_v, ind_ampls]: v_index[i])
            {
               if constexpr(CONJ)
               {
                  val += arVec[ind_v] * midas::math::Conj(arAmpls[ind_ampls]);
               }
               else
               {
                  val += arVec[ind_v] * arAmpls[ind_ampls];
               }
            }
            res[i] = val;
         }
      }

#ifdef VAR_MPI
      // Broadcast results for MPI.
      for(Uin r = 0; r < midas::mpi::GlobalSize(); ++r)
      {
         const Uin i_beg = v_ranges[r*omp_get_max_threads()];
         const Uin i_end = v_ranges[(r+1)*omp_get_max_threads()];
         midas::mpi::detail::WRAP_Bcast
            (  res.data() + i_beg
            ,  i_end - i_beg
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  r
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
      }
#endif /* VAR_MPI */

      return res;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<bool CONJ, typename T>
   SparseClusterOper::vec_tmpl<T> SparseClusterOper::RefToFullSpace
      (  const vec_tmpl<T>& arAmpls
      )  const
   {
      // Assertions.
      if (arAmpls.Size() != this->NumAmpls())
      {
         std::stringstream ss;
         ss << "Size mismatch;\n"
            << "   arAmpls.Size() = " << arAmpls.Size() << '\n'
            << "   (expected      = " << this->NumAmpls() << ")\n"
            ;
         MIDASERROR(ss.str());
      }

      // The 0'th vector of de-exc. indices directly give us the non-zero
      // values of Oper|ref>.
      const index_t& v_index = this->IndicesDeExc();

      // We have to zero-initialize res, because the loop might not assign to
      // all elements of it.
      vec_tmpl<T> res(v_index.size(), T(0));

      if (v_index.size() > 0)
      {
         const Uin size = v_index[0].size();
         #pragma omp parallel for
         for(Uin i = 0; i < size; ++i)
         {
            const auto& [ind,ampl] = v_index[0][i];
            if constexpr(CONJ)
            {
               res[ind] = midas::math::Conj(arAmpls[ampl]);
            }
            else
            {
               res[ind] = arAmpls[ampl];
            }
         }

         // Only bother with OpenMP, no MPI, since the communication will
         // probably take as long (or longer) as just filling in the
         // amplitudes, anyway.
      }

      return res;
   }

} /* namespace midas::util::matrep */

#endif/*SPARSECLUSTEROPER_IMPL_H_INCLUDED*/
