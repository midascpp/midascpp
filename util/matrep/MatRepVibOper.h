/**
 *******************************************************************************
 * 
 * @file    MatRepVibOper.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPVIBOPER_H_INCLUDED
#define MATREPVIBOPER_H_INCLUDED

#include "util/matrep/MatRepVibOper_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "util/matrep/MatRepVibOper_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*MATREPVIBOPER_H_INCLUDED*/
