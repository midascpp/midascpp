/**
 *******************************************************************************
 * 
 * @file    ShiftOperBraketLooper_Decl.h
 * @date    02-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SHIFTOPERBRAKETLOOPER_DECL_H_INCLUDED
#define SHIFTOPERBRAKETLOOPER_DECL_H_INCLUDED

// Standard headers.
#include <array>
#include <utility>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"

// Forward declarations.
template<typename> class GeneralMidasVector;

namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   class ShiftOperBraketLooper
   {
      public:
         template<typename U> using vec_tmpl = GeneralMidasVector<U>;
         using loop_data_tuple_t = std::tuple
            <  std::vector<std::set<Uin>>    // the ModeCombis as set<Uin>
            ,  std::vector<Uin>              // index of ModeCombi
            ,  std::vector<std::vector<Uin>> // creation indices
            ,  std::vector<std::vector<Uin>> // annihilation indices
            ,  std::vector<Uin>              // estimated costs (terms in ShiftOperBraket)
            >;
         using mcr_exci_indices_t = std::vector<std::pair<std::set<Uin>,std::vector<std::vector<Uin>>>>;

         //@{
         //! Default constructors. Assignments deleted.
         ShiftOperBraketLooper(const ShiftOperBraketLooper&) = default;
         ShiftOperBraketLooper(ShiftOperBraketLooper&&) = default;
         ShiftOperBraketLooper& operator=(const ShiftOperBraketLooper&) = delete;
         ShiftOperBraketLooper& operator=(ShiftOperBraketLooper&&) = delete;
         ~ShiftOperBraketLooper() = default;
         //@}

         //! Standard Eta-vec/LJac setup; loops tau_nu (E^m_ai) for all MCs. Anni. always reference.
         ShiftOperBraketLooper
            (  std::vector<Uin> aDims
            ,  mcr_exci_indices_t arMcrExciIndices
            );

         //! Constructor using a call to McrExciIndices, then calls other constructor.
         ShiftOperBraketLooper
            (  const std::vector<Uin>& arDims
            ,  const ModeCombiOpRange& arMcr
            );

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //!
         const std::vector<Uin>& Dims() const {return mDims;}

         //!
         Uin NumModes() const {return mDims.size();}

         //! The dimension of the full space, i.e. product of dimensions for each mode.
         Uin FullDim() const {return mFullDim;}

         //! Size of result vector.
         Uin ResultSize() const {return this->VecIndexModes().size();}

         //!@}

         /******************************************************************//**
          * @name Computations
          *********************************************************************/
         //!@{
         template<bool CONJ_BRA, typename T>
         vec_tmpl<T> Transform
            (  const std::vector<std::tuple<T,const vec_tmpl<T>*,const vec_tmpl<T>*>>& arTermsCoefBraKet
            )  const;
         //!@}

      private:
         //!
         const std::vector<Uin> mDims;

         //!
         const Uin mFullDim = 0;

         //!
         bool mZeroEmptyMc = true;

         //!
         bool mAlwaysAnnihilateInReference = true;

         /******************************************************************//**
          * Vector of different ModeCombis. (Not for each element of results.)
          * Then, for each element of result
          * - index in ModeCombiOpRange of the set of modes of the shift operator.
          * - the creation indices.
          * - the annihilation indices. If empty, use reference.
          * - estimated cost (terms in ShiftOperBraket).
          *********************************************************************/
         const loop_data_tuple_t mVecsLoopData;

         //@{
         //! Interface to mVecsLoopData.
         const std::vector<std::set<Uin>>& VecMcSets() const {return std::get<0>(mVecsLoopData);}
         const std::vector<Uin>& VecIndexModes() const {return std::get<1>(mVecsLoopData);}
         const std::vector<std::vector<Uin>>& VecCrea() const {return std::get<2>(mVecsLoopData);}
         const std::vector<std::vector<Uin>>& VecAnni() const {return std::get<3>(mVecsLoopData);}
         const std::vector<Uin>& CostWeights() const {return std::get<4>(mVecsLoopData);}
         //@}

         static loop_data_tuple_t ConstructLoopData
            (  const std::vector<Uin>& arDims
            ,  mcr_exci_indices_t aMcrExciIndices
            );

   };

} /* namespace midas::util::matrep */

#endif/*SHIFTOPERBRAKETLOOPER_DECL_H_INCLUDED*/
