/**
 *******************************************************************************
 * 
 * @file    ShiftOperBraketLooper.cc
 * @date    02-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "util/InterfaceOpenMP.h"
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "mpi/Impi.h"

//==============================================================================
// NON-TEMPLATED DEFINITIONS
//==============================================================================
namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   ShiftOperBraketLooper::ShiftOperBraketLooper
      (  std::vector<Uin> aDims
      ,  mcr_exci_indices_t arMcrExciIndices
      )
      :  mDims(std::move(aDims))
      ,  mFullDim(Product(mDims))
      ,  mZeroEmptyMc(true)
      ,  mAlwaysAnnihilateInReference(true)
      ,  mVecsLoopData(ConstructLoopData(mDims, std::move(arMcrExciIndices)))
   {
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   ShiftOperBraketLooper::ShiftOperBraketLooper
      (  const std::vector<Uin>& arDims
      ,  const ModeCombiOpRange& arMcr
      )
      :  ShiftOperBraketLooper(arDims, McrExciIndices(arMcr, arDims))
   {
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   ShiftOperBraketLooper::loop_data_tuple_t ShiftOperBraketLooper::ConstructLoopData
      (  const std::vector<Uin>& arDims
      ,  mcr_exci_indices_t aMcrExciIndices
      )
   {
      Uin size = 0;
      for(const auto& [mc_set, v_exci]: aMcrExciIndices)
      {
         size += v_exci.size();
      }

      // Setup vectors of result. Note, v_anni is left empty; always annihilate
      // reference indices in this setup.
      loop_data_tuple_t res;
      auto& [v_mc_sets, v_mc_ind, v_crea, v_anni, v_cost] = res;
      v_mc_sets.reserve(aMcrExciIndices.size());
      v_mc_ind.reserve(size);
      v_crea.reserve(size);
      v_cost.reserve(size);

      for(auto& [mc_set, v_exci]: aMcrExciIndices)
      {
         const Uin i_mc = v_mc_sets.size();
         const Uin cost = Product(SubsetDims(ComplementaryModes(arDims.size(), mc_set), arDims));
         for(auto& exci: v_exci)
         {
            v_mc_ind.emplace_back(i_mc);
            v_crea.emplace_back(std::move(exci));
            v_cost.emplace_back(cost);
         }
         v_mc_sets.emplace_back(std::move(mc_set));
      }

      return res;
   }

} /* namespace midas::util::matrep */

//==============================================================================
// TEMPLATE INSTANTIATIONS
//==============================================================================
#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "util/matrep/ShiftOperBraketLooper_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_SHIFTOPERBRAKETLOOPER(T) \
   namespace midas::util::matrep \
   { \
      template ShiftOperBraketLooper::vec_tmpl<T> ShiftOperBraketLooper::Transform<false> \
         (  const std::vector<std::tuple<T,const vec_tmpl<T>*,const vec_tmpl<T>*>>& arTermsCoefBraKet \
         )  const; \
      template ShiftOperBraketLooper::vec_tmpl<T> ShiftOperBraketLooper::Transform<true> \
         (  const std::vector<std::tuple<T,const vec_tmpl<T>*,const vec_tmpl<T>*>>& arTermsCoefBraKet \
         )  const; \
   } /* namespace midas::util::matrep */ \


// Instantiations.
INSTANTIATE_SHIFTOPERBRAKETLOOPER(Nb);
INSTANTIATE_SHIFTOPERBRAKETLOOPER(std::complex<Nb>);

#undef INSTANTIATE_SHIFTOPERBRAKETLOOPER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
