/**
 *******************************************************************************
 * 
 * @file    ShiftOperBraketLooper_Impl.h
 * @date    02-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SHIFTOPERBRAKETLOOPER_IMPL_H_INCLUDED
#define SHIFTOPERBRAKETLOOPER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "mpi/Impi.h"
#include "util/InterfaceOpenMP.h"


namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template<bool CONJ_BRA, typename T>
   ShiftOperBraketLooper::vec_tmpl<T> ShiftOperBraketLooper::Transform
      (  const std::vector<std::tuple<T,const vec_tmpl<T>*,const vec_tmpl<T>*>>& arTermsCoefBraKet
      )  const
   {
      vec_tmpl<T> res(this->ResultSize(), T(0));
      const Uin mpi_size = midas::mpi::GlobalSize();
      const Uin res_size = this->ResultSize();
      auto get_i_mpi = [mpi_size, res_size](const Uin rank) -> Uin
      {
         return (rank*res_size)/mpi_size;
      };
      const Uin i_beg = get_i_mpi(midas::mpi::GlobalRank());
      const Uin i_end = get_i_mpi(midas::mpi::GlobalRank()+1);

      #pragma omp parallel
      {
         std::vector<Uin> anni;
         anni.reserve(this->Dims().size());

         #pragma omp for
         for(Uin i = i_beg; i < i_end; ++i)
         {
            T val = 0;
            const auto& modes = this->VecMcSets().at(this->VecIndexModes().at(i));
            if (!(this->mZeroEmptyMc && modes.empty()))
            {
               if (this->mAlwaysAnnihilateInReference)
               {
                  anni.resize(modes.size(), 0);
               }
               const auto& crea = this->VecCrea().at(i);
               for(const auto& [coef, pbra, pket]: arTermsCoefBraKet)
               {
                  val += coef * ShiftOperBraket<CONJ_BRA>(*pbra, *pket, this->Dims(), modes, crea, anni);
               }
            }
            res[i] = val;
         }
      }

#ifdef VAR_MPI
      // Broadcast results for MPI.
      for(Uin r = 0; r < midas::mpi::GlobalSize(); ++r)
      {
         const Uin i_beg = get_i_mpi(r);
         const Uin i_end = get_i_mpi(r+1);
         midas::mpi::detail::WRAP_Bcast
            (  res.data() + i_beg
            ,  i_end - i_beg
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  r
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
      }
#endif /* VAR_MPI */

      return res;
   }

} /* namespace midas::util::matrep */

#endif/*SHIFTOPERBRAKETLOOPER_IMPL_H_INCLUDED*/
