/**
 *******************************************************************************
 * 
 * @file    SparseClusterOper.cc
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "util/InterfaceOpenMP.h"
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "mpi/Impi.h"

//==============================================================================
// NON-TEMPLATED DEFINITIONS
//==============================================================================
namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   SparseClusterOper::SparseClusterOper
      (  std::vector<Uin> aDims
      ,  ModeCombiOpRange aMcr
      )
      :  mDims(std::move(aDims))
      ,  mMcr(std::move(aMcr))
      ,  mFullDim(Product(mDims))
      ,  mAmplMcAddresses(CalcAmplMcAddresses(mDims, mMcr))
      ,  mIndices(CalcIndices(mDims, mMcr))
      ,  mNumNonZeroElems(CalcNumNonZeroElems(mIndices.at(0)))
      ,  mMpiOmpRanges(
            {  CalcMpiOmpRanges(Indices<false>(), omp_get_max_threads() * midas::mpi::GlobalSize())
            ,  CalcMpiOmpRanges(Indices<true>(), omp_get_max_threads() * midas::mpi::GlobalSize())
            })
   {
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   Nb SparseClusterOper::Density() const
   {
      return Nb(this->NumNonZeroElems())/this->FullDim()/this->FullDim();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   Nb SparseClusterOper::Sparsity() const
   {
      return 1. - this->Density();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   std::vector<Uin> SparseClusterOper::ExampleMpiOmpRangesExc
      (  Uin aNumMpiRanks
      )  const
   {
      return CalcMpiOmpRanges(this->IndicesExc(), aNumMpiRanks);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   std::vector<Uin> SparseClusterOper::ExampleMpiOmpRangesDeExc
      (  Uin aNumMpiRanks
      )  const
   {
      return CalcMpiOmpRanges(this->IndicesDeExc(), aNumMpiRanks);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   std::vector<Uin> SparseClusterOper::CalcAmplMcAddresses
      (  const std::vector<Uin>& arDims
      ,  const ModeCombiOpRange& arMcr
      )
   {
      std::vector<Uin> v;
      v.reserve(arMcr.Size()+1);
      Uin a = 0;
      for(const auto& mc: arMcr)
      {
         v.emplace_back(a);
         a += NumParams(mc, arDims);
      }
      v.emplace_back(a);
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   std::array<SparseClusterOper::index_t,2> SparseClusterOper::CalcIndices
      (  const std::vector<Uin>& arDims
      ,  const ModeCombiOpRange& arMcr
      )
   {
      const Uin d_full = Product(arDims);
      const Uin n_modes = arDims.size();
      index_t v_exc(d_full);
      index_t v_deexc(d_full);

      Uin addr_mc = 0;
      Uin n_exci = 0;
      for(Uin i_mc = 0; i_mc < arMcr.Size(); ++i_mc, addr_mc += n_exci)
      {
         const auto& mc = arMcr.GetModeCombi(i_mc);
         const auto mc_exci = SetFromVec(mc.MCVec());
         const auto mc_free = ComplementaryModes(n_modes, mc_exci);
         const auto exci_dims = ShiftVals(SubsetDims(mc_exci, arDims), -1);
         const auto free_dims = SubsetDims(mc_free, arDims);
         n_exci = Product(exci_dims);
         const Uin n_free = Product(free_dims);

         const std::vector<Uin> ind_mc_anni(mc_exci.size(), 0);
         for(Uin i_exci = 0; i_exci < n_exci; ++i_exci)
         {
            const Uin i_ampl = addr_mc + i_exci;
            const auto ind_mc_crea = ShiftVals(MultiIndex(i_exci,exci_dims), +1);
            for(Uin i_free = 0; i_free < n_free; ++i_free)
            {
               const auto ind_free = MultiIndex(i_free, free_dims);
               const auto ind_crea = MatRepVibOper<Nb>::CombineIndices(mc_exci, ind_mc_crea, ind_free);
               const auto ind_anni = MatRepVibOper<Nb>::CombineIndices(mc_exci, ind_mc_anni, ind_free);
               const Uin abs_crea = AbsIndex(ind_crea, arDims);
               const Uin abs_anni = AbsIndex(ind_anni, arDims);
               v_exc.at(abs_crea).emplace_back(abs_anni,i_ampl);
               v_deexc.at(abs_anni).emplace_back(abs_crea,i_ampl);
            }
         }
      }

      // Sort according to first index (index of input vector).
      for(Uin i = 0; i < v_exc.size(); ++i)
      {
         std::sort(v_exc.at(i).begin(), v_exc.at(i).end());
         std::sort(v_deexc.at(i).begin(), v_deexc.at(i).end());
      }

      return {v_exc,v_deexc};
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   Uin SparseClusterOper::CalcNumNonZeroElems
      (  const index_t& arIndices
      )
   {
      Uin n = 0;
      for(const auto& ind: arIndices)
      {
         n += ind.size();
      }
      return n;
   }

   /************************************************************************//**
    * For number of MPI ranks n, tries to divide result vector into
    * consecutive ranges such that each range comes close to containing n/N
    * dense elements, N being the total number of dense elements for the
    * operator.
    *
    * Algorithm:
    * ~~~
    *    -  n = total number of MPI ranks
    *    -  for each rank i
    *       -  N_i are the number of remaining dense elements
    *       -  rank i takes the first unassigned consecutive range such that
    *       its number of dense elements comes as close as possible to
    *       N_i/(n-i)
    *       -  in case of ties rank i is greedy
    * ~~~
    *
    * Example:
    * The excitation indices are such that the number of dense elements going
    * into each element of the result vector are:
    * ~~~
    *    v[0]:   1
    *    v[1]:   1
    *    v[2]:   2
    *    v[3]:   3
    *    v[4]:   2
    *    v[5]:   2
    *    v[6]:   4
    *    v[7]:   5
    *           20 dense elements in total
    * ~~~
    * Assume 4 MPI ranks. Then the ideal division would be each rank being
    * responsible for 5 dense elements.
    * The requirement that each rank must be responsible for a consecutive
    * range means we are a little limited on how good a result we can get.
    * Algorithm:
    * ~~~
    *    n = 4
    *    rank i = 0: N_i       = 20
    *                N_i/(n-i) = 5
    *                accumulate:
    *                    v[0]  1
    *                   +v[1]  2
    *                   +v[2]  4
    *                   +v[3]  7
    *             went >= 5
    *             |4-5| < |7-5| so we break at v[3]
    *    rank i = 1: N_i       = 20 - 4 = 16
    *                N_i/(n-i) = 16/3   = 5.333
    *                accumulate:
    *                    v[3]  3
    *                   +v[4]  5
    *                   +v[5]  7
    *             went >= 5.333
    *             |5-5.333| < |7-5.333| so we break at v[5]
    *    rank i = 2: N_i       = 16 - 5 = 11
    *                N_i/(n-i) = 11/2   = 5.5
    *                accumulate:
    *                    v[5]  2
    *                   +v[6]  6
    *             went >= 5.5
    *             |2-5.5| > |6-5.5| so we break at v[7]
    *    rank i = 3: N_i       = 11 - 6 = 5
    *                N_i/(n-i) = 5/1    = 5
    *                accumulate:
    *                    v[7]  5
    *             went >= 5 (and at end) so we break at v.end()
    *
    *    returns {0,3,5,7,8}
    * ~~~
    ***************************************************************************/
   std::vector<Uin> SparseClusterOper::CalcMpiOmpRanges
      (  const index_t& arIndices
      ,  const Uin aNumMpiRanks
      )
   {
      if (aNumMpiRanks == 0)
      {
         MIDASERROR("aNumMpiRanks must be > 0");
      }
      std::vector<Uin> v;
      v.reserve(aNumMpiRanks + 1);
      Uin n_dense_remain = CalcNumNonZeroElems(arIndices);

      // First range always begins at 0.
      v.emplace_back(0);
      Uin r_remain = aNumMpiRanks;
      for(Uin r = 0; r + 1 < aNumMpiRanks; ++r, --r_remain)
      {
         // Accumulate until reaching target.
         Uin s = 0;
         for(Uin i = v.back(); i < arIndices.size(); ++i)
         {
            Uin s_prev = s;
            s += arIndices.at(i).size();
            if (r_remain * s >= n_dense_remain)
            {
               // |s_prev - n/r_remain| < |s - n/r_remain| ?
               // --> n/r_remain - s_prev < s - n/r_remain
               // --> n - s_prev*r_remain < s*r_remain - n
               // --> 2*n < (s+s_prev)*r_remain
               // Then pick i (s_prev).
               if (2*n_dense_remain < (s + s_prev)*r_remain)
               {
                  v.emplace_back(i);
                  n_dense_remain -= s_prev;
               }
               // Otherwise pick i+1 (s).
               else
               {
                  v.emplace_back(i+1);
                  n_dense_remain -= s;
               }
               break;
            }
         }
      }

      // Last range always ends at size().
      v.emplace_back(arIndices.size());
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   void OptimizationAnalysis
      (  const SparseClusterOper& arSco
      ,  std::ostream& arOs
      ,  Uin aIndent
      )
   {
      const auto old_flags = arOs.flags();
      const std::string ind(aIndent, ' ');
      const std::string tab(3, ' ');
      const std::string eq = " = ";
      const Uin w = 20;
      arOs
         << ind << "Sparsity analysis:\n"
         << ind << tab << std::setw(w) << "FullDim()" << eq << arSco.FullDim() << '\n'
         << ind << tab << std::setw(w) << "NumNonZeroElems()" << eq << arSco.NumNonZeroElems() << '\n'
         << ind << tab << std::setw(w) << "Density()" << eq << arSco.Density() << '\n'
         << ind << tab << std::setw(w) << "Sparsity()" << eq << arSco.Sparsity() << '\n'
         ;
      const Uin n_mpi_omp = midas::mpi::GlobalSize() * omp_get_max_threads();
      if (n_mpi_omp > 1)
      {
         const std::array
            <  std::tuple
               <  std::string
               ,  const std::vector<Uin>*
               ,  const SparseClusterOper::index_t*
               >
            ,  2
            >
            arr =
            {  std::make_tuple("MpiOmpRangesExc", &arSco.MpiOmpRangesExc(), &arSco.IndicesExc())
            ,  std::make_tuple("MpiOmpRangesDeExc", &arSco.MpiOmpRangesDeExc(), &arSco.IndicesDeExc())
            };
         const Uin w_mpi = std::to_string(midas::mpi::GlobalSize()-1).size();
         const Uin w_omp = std::to_string(omp_get_max_threads()-1).size();
         const Uin w_id = std::to_string(n_mpi_omp-1).size();
         const Uin w_fd = std::to_string(arSco.FullDim()).size();
         const Uin w_nz = std::to_string(arSco.NumNonZeroElems()).size();
         for(const auto& [s,p_rng,p_ind]: arr)
         {
            arOs
               << ind << "Parallelization workload division analysis for '" << s << "'.\n"
               << ind << tab << "Ranges and num. dense elements:\n"
               ;
            Uin id_max = 0;
            Uin n_max = 0;
            arOs << std::right;
            for(Uin id = 0; id < n_mpi_omp; ++id)
            {
               const Uin r_mpi = id / omp_get_max_threads();
               const Uin t_omp = id % omp_get_max_threads();
               Uin n = 0;
               for(Uin i = p_rng->at(id); i < p_rng->at(id+1); ++i)
               {
                  n += p_ind->at(i).size();
               }
               arOs
                  << ind << tab << tab << "process " << std::setw(w_id) << id
                  << " (rank " << std::setw(w_mpi) << r_mpi
                  << ", thread " << std::setw(w_omp) << t_omp << ")"
                  << ": "
                  << "[" << std::setw(w_fd) << p_rng->at(id)
                  << ";" << std::setw(w_fd) << p_rng->at(id+1)
                  << ")," << tab << std::setw(w_nz) << n << " dense elements"
                  << '\n'
                  ;
               if (n > n_max)
               {
                  n_max = n;
                  id_max = id;
               }
            }
            const Nb est_speedup = Nb(arSco.NumNonZeroElems())/n_max;
            const Nb opt_speedup = Nb(n_mpi_omp);
            const Nb ratio = est_speedup/opt_speedup;
            arOs << std::left
               << ind << "Estimated speedup from workload division:\n"
               << ind << "(But don't trust too much; neglects communication overhead.)\n"
               << ind << tab << std::setw(w) << "id_max" << eq << id_max << '\n'
               << ind << tab << std::setw(w) << "n_max" << eq << n_max << '\n'
               << ind << tab << std::setw(w) << "est. paral. speedup" << eq << est_speedup << '\n'
               << ind << tab << std::setw(w) << "opt. paral. speedup" << eq << opt_speedup << '\n'
               << ind << tab << std::setw(w) << "speedup ratio" << eq << ratio
                      << tab << "(1 = perfect)\n"
               ;
         }
      }
      arOs << std::flush;
      arOs.flags(old_flags);
   }

} /* namespace midas::util::matrep */

//==============================================================================
// TEMPLATE INSTANTIATIONS
//==============================================================================
#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "util/matrep/SparseClusterOper_Impl.h"

// Define instantiation macro.
namespace midas::util::matrep
{
   template const SparseClusterOper::index_t& SparseClusterOper::Indices<false>() const;
   template const SparseClusterOper::index_t& SparseClusterOper::Indices<true>() const;
   template const std::vector<Uin>& SparseClusterOper::MpiOmpRanges<false>() const;
   template const std::vector<Uin>& SparseClusterOper::MpiOmpRanges<true>() const;
} /* namespace midas::util::matrep */

#define INSTANTIATE_SPARSECLUSTEROPER(T) \
   namespace midas::util::matrep \
   { \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::Transform<false,false> \
         (  const vec_tmpl<T>& arAmpls \
         ,  const vec_tmpl<T>& arVec \
         )  const; \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::Transform<false,true> \
         (  const vec_tmpl<T>& arAmpls \
         ,  const vec_tmpl<T>& arVec \
         )  const; \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::Transform<true,false> \
         (  const vec_tmpl<T>& arAmpls \
         ,  const vec_tmpl<T>& arVec \
         )  const; \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::Transform<true,true> \
         (  const vec_tmpl<T>& arAmpls \
         ,  const vec_tmpl<T>& arVec \
         )  const; \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::RefToFullSpace<false> \
         (  const vec_tmpl<T>& arAmpls \
         )  const; \
      template SparseClusterOper::vec_tmpl<T> SparseClusterOper::RefToFullSpace<true> \
         (  const vec_tmpl<T>& arAmpls \
         )  const; \
   } /* namespace midas::util::matrep */ \


// Instantiations.
INSTANTIATE_SPARSECLUSTEROPER(Nb);
INSTANTIATE_SPARSECLUSTEROPER(std::complex<Nb>);

#undef INSTANTIATE_SPARSECLUSTEROPER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
