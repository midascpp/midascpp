/**
 *******************************************************************************
 * 
 * @file    MatRepTransformers.cc
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepTransformers_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

// Define instantiation macro.
#define INSTANTIATE_MATREPTRANSFORMERS(T) \
   namespace midas::util::matrep \
   { \
      template T ShiftOperBraket<false> \
         (  const GeneralMidasVector<T>& arBra \
         ,  const GeneralMidasVector<T>& arKet \
         ,  const std::vector<Uin>& arDims \
         ,  const std::set<Uin>& arModes \
         ,  const std::vector<Uin>& arCrea \
         ,  const std::vector<Uin>& arAnni \
         ); \
      template T ShiftOperBraket<true> \
         (  const GeneralMidasVector<T>& arBra \
         ,  const GeneralMidasVector<T>& arKet \
         ,  const std::vector<Uin>& arDims \
         ,  const std::set<Uin>& arModes \
         ,  const std::vector<Uin>& arCrea \
         ,  const std::vector<Uin>& arAnni \
         ); \
      template GeneralMidasVector<T> TrfExpClusterOper<false,false> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ); \
      template GeneralMidasVector<T> TrfExpClusterOper<false,true> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ); \
      template GeneralMidasVector<T> TrfExpClusterOper<true,false> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ); \
      template GeneralMidasVector<T> TrfExpClusterOper<true,true> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ); \
      template GeneralMidasVector<T> TrfLinClusterOper<false,false> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ,  Uin aPower \
         ); \
      template GeneralMidasVector<T> TrfLinClusterOper<false,true> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ,  Uin aPower \
         ); \
      template GeneralMidasVector<T> TrfLinClusterOper<true,false> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ,  Uin aPower \
         ); \
      template GeneralMidasVector<T> TrfLinClusterOper<true,true> \
         (  const SparseClusterOper& arOper \
         ,  const GeneralMidasVector<T>& arAmpls \
         ,  GeneralMidasVector<T> aVec \
         ,  T aCoef \
         ,  Uin aPower \
         ); \
      template GeneralMidasVector<T> TrfVccErrVec \
         (  const OperMat<T>& arOperMat \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ); \
      template GeneralMidasVector<T> TrfVccErrVec \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ); \
      template GeneralMidasVector<T> TrfVci \
         (  const OperMat<T>& arOperMat \
         ,  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arCCoefs \
         ); \
      template GeneralMidasVector<T> TrfVci \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arCCoefs \
         ); \
      template GeneralMidasVector<T> TrfVccEtaVec \
         (  const OperMat<T>& arOperMat \
         ,  const ShiftOperBraketLooper& arLooper \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ); \
      template GeneralMidasVector<T> TrfVccEtaVec \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ); \
      template GeneralMidasVector<T> TrfVccRJac \
         (  const OperMat<T>& arOperMat \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arRCoefs \
         ,  const bool aZeroRefElemResult \
         ); \
      template GeneralMidasVector<T> TrfVccRJac \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ,  const std::vector<std::vector<T>>& arRCoefs \
         ,  const bool aZeroRefElemResult \
         ); \
      template GeneralMidasVector<T> TrfVccLJac \
         (  const OperMat<T>& arOperMat \
         ,  const ShiftOperBraketLooper& arLooper \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arLCoefs \
         ); \
      template GeneralMidasVector<T> TrfVccLJac \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ,  const std::vector<std::vector<T>>& arLCoefs \
         ); \
      template GeneralMidasMatrix<T> ExplVccJac \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ,  const bool aDisregardEmptyMc \
         ); \
      template std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA \
         (  const GeneralMidasMatrix<T>& arATransp \
         ,  const GeneralMidasVector<T>& arMinusEta \
         ); \
      template std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ); \
      template GeneralMidasVector<T> TrfExtVccHamDerExtAmp \
         (  const OperMat<T>& arOperMat \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arSAmps \
         ); \
      template GeneralMidasVector<T> TrfExtVccHamDerExtAmp \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ,  const std::vector<std::vector<T>>& arSAmps \
         ); \
      template GeneralMidasVector<T> TrfExtVccHamDerClustAmp \
         (  const OperMat<T>& arOperMat \
         ,  const ShiftOperBraketLooper& arLooper \
         ,  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arSAmps \
         ); \
      template GeneralMidasVector<T> TrfExtVccHamDerClustAmp \
         (  const std::vector<Uin>& arNModals \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModInts \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmps \
         ,  const std::vector<std::vector<T>>& arSAmps \
         ); \
      template GeneralMidasVector<T> TrfExtVccLeftExpmS \
         (  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arSAmps \
         ,  const GeneralMidasVector<T>& arVec \
         ); \
      template GeneralMidasVector<T> TrfExtVccRightExpmS \
         (  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arSAmps \
         ,  const GeneralMidasVector<T>& arVec \
         ); \
      template std::pair<GeneralMidasVector<T>,std::set<Uin>> TensorDirectProduct \
         (  const std::vector<Uin>& arDims \
         ,  const std::vector<std::set<Uin>>& arSubMcs \
         ,  const std::vector<GeneralMidasVector<T>>& arSubTensors \
         ); \
      template GeneralMidasVector<T> CRefFullSpace \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arCCoefs \
         ); \
      template GeneralMidasVector<T> ExpTRef \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const Uin aMaxExciOut \
         ); \
      template GeneralMidasVector<T> ExpTRef \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralDataCont<T>& arTAmps \
         ,  const Uin aMaxExciOut \
         ); \
      template GeneralMidasVector<T> InvExpTRef \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralMidasVector<T>& arCCoefs \
         ); \
      template GeneralMidasVector<T> InvExpTRef \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralDataCont<T>& arCCoefs \
         ); \
      template GeneralMidasVector<T> Ref1pLExpmT \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arLCoefs \
         ,  const Uin aMaxExciOut \
         ); \
      template GeneralMidasVector<T> Ref1pLExpmT \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralDataCont<T>& arTAmps \
         ,  const GeneralDataCont<T>& arLCoefs \
         ,  const Uin aMaxExciOut \
         ); \
      template GeneralMidasVector<T> RefExpSExpmT \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralMidasVector<T>& arTAmps \
         ,  const GeneralMidasVector<T>& arLCoefs \
         ,  const Uin aMaxExciOut \
         ); \
      template GeneralMidasVector<T> RefExpSExpmT \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const GeneralDataCont<T>& arTAmps \
         ,  const GeneralDataCont<T>& arLCoefs \
         ,  const Uin aMaxExciOut \
         ); \
      template std::vector<GeneralMidasMatrix<T>> VccOneModeDensityMatrices \
         (  const SparseClusterOper& arClustOper \
         ,  const GeneralMidasVector<T>& arTAmpls \
         ,  const GeneralMidasVector<T>& arLCoefs \
         ); \
      template std::vector<GeneralMidasMatrix<T>> VccOneModeDensityMatrices \
         (  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmpls \
         ,  const std::vector<std::vector<T>>& arLCoefs \
         ); \
      template std::array<std::vector<std::vector<GeneralMidasMatrix<T>>>,3> TdmvccHalfAndFullyTransformedModalIntegrals \
         (  const ModalIntegrals<T>& arModIntsPrimBas \
         ,  const std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>>& arModalTransMats \
         ); \
      template std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransOneModeOpers \
         (  const OpDef& arOpDef                                                                               \
         ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfPrimTd                                  \
         ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfTdPrim                                  \
         ); \
      template std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransMeanFieldMatrices \
         (  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModIntsTdBas \
         ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfPrimTd \
         ,  const std::vector<std::vector<GeneralMidasMatrix<T>>>& arModIntsHalfTrfTdPrim \
         ,  const SparseClusterOper& arClustOperTdBas \
         ,  const GeneralMidasVector<T>& arTAmpls \
         ,  const GeneralMidasVector<T>& arLCoefs \
         ,  bool aSkipOneModeTerms\
         ); \
      template std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>> TdmvccHalfTransMeanFieldMatrices \
         (  const std::vector<Uin>& arNModalsTdBas \
         ,  const OpDef& arOpDef \
         ,  const ModalIntegrals<T>& arModIntsPrimBas \
         ,  const std::vector<std::pair<GeneralMidasMatrix<T>,GeneralMidasMatrix<T>>>& arModalTransMats \
         ,  const ModeCombiOpRange& arMcr \
         ,  const std::vector<std::vector<T>>& arTAmpls \
         ,  const std::vector<std::vector<T>>& arLCoefs \
         ,  bool aSkipOneModeTerms\
         ); \
      template GeneralMidasMatrix<T> TdmvccUDotBaseExpression \
         (  const GeneralMidasMatrix<T>& arU \
         ,  const GeneralMidasMatrix<T>& arMeanFieldMatHalfTrfPrimTd \
         ,  const GeneralMidasMatrix<T>& arProjTdActiveSpace \
         ,  const GeneralMidasMatrix<T>& arDensMatInverse \
         ,  const GeneralMidasMatrix<T>& arConstraintOperTdBas \
         ); \
      template GeneralMidasMatrix<T> TdmvccWDotBaseExpression \
         (  const GeneralMidasMatrix<T>& arW \
         ,  const GeneralMidasMatrix<T>& arMeanFieldMatHalfTrfTdPrim \
         ,  const GeneralMidasMatrix<T>& arProjTdActiveSpace \
         ,  const GeneralMidasMatrix<T>& arDensMatInverse \
         ,  const GeneralMidasMatrix<T>& arConstraintOperTdBas \
         ); \
   } /* namespace midas::util::matrep */ \


// Instantiations.
INSTANTIATE_MATREPTRANSFORMERS(Nb);
INSTANTIATE_MATREPTRANSFORMERS(std::complex<Nb>);

#undef INSTANTIATE_MATREPTRANSFORMERS

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
