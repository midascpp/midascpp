/**
 *******************************************************************************
 * 
 * @file    OperMat_Impl.h
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef OPERMAT_IMPL_H_INCLUDED
#define OPERMAT_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/OpDef.h"
#include "input/Trim.h"
#include "vcc/ModalIntegrals.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/math_wrappers.h"

namespace midas::util::matrep
{

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   OperMat<T>::OperMat
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  bool aDumpToDiskAtDestructor
      )
      :  mNModals(arNModals)
      ,  mFullDim(Product(arNModals))
      ,  mMpiRankRowIndices(CalcMpiRankRowIndices(midas::mpi::GlobalSize()))
      ,  mSymType(DetermineType(arOpDef, arModInts))
      ,  mDumpFileName(GenerateDumpFileName(arNModals, arOpDef, arModInts, this->RowBeg(), this->RowEnd()))
      ,  mDumpToDisk(aDumpToDiskAtDestructor)
      ,  mMat(ConstructMat(arNModals, arOpDef, arModInts))
   {
      MidasAssert(mMat.get() != nullptr, "mMat holds nullptr");
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   OperMat<T>::OperMat
      (  const n_modals_t& arNModals
      ,  const mat_t& arMatrix
      ,  bool aDumpToDiskAtDestructor
      )
      :  mNModals(arNModals)
      ,  mFullDim(Product(arNModals))
      ,  mMpiRankRowIndices(CalcMpiRankRowIndices(midas::mpi::GlobalSize()))
      ,  mSymType(SymType::GENERAL)
      ,  mDumpFileName(GenerateDumpFileName(arNModals, this->RowBeg(), this->RowEnd()))
      ,  mDumpToDisk(aDumpToDiskAtDestructor)
      ,  mMat(ColMajPtrFromMidasMatrix2(arMatrix))
   {
      MidasAssert(arMatrix.IsSquare(), "Input matrix is not square");
      MidasAssert(arMatrix.Nrows() == this->Nrows(), "Input matrix has wrong dimensions");
      MidasAssert(mMat.get() != nullptr, "mMat holds nullptr");
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   OperMat<T>::OperMat
      (  const n_modals_t& arNModals
      ,  bool aDumpToDiskAtDestructor
      )
      :  mNModals(arNModals)
      ,  mFullDim(Product(arNModals))
      ,  mMpiRankRowIndices(CalcMpiRankRowIndices(midas::mpi::GlobalSize()))
      ,  mSymType(SymType::HERMITIAN)
      ,  mDumpFileName(GenerateDumpFileName(arNModals, this->RowBeg(), this->RowEnd()))
      ,  mDumpToDisk(aDumpToDiskAtDestructor)
      ,  mMat(ConstructMat(arNModals))
   {
      MidasAssert(mMat.get() != nullptr, "mMat holds nullptr");
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   OperMat<T>::~OperMat
      (
      )
   {
      if (this->mMat && this->mDumpToDisk)
      {
         GeneralMidasVector<T> v;
         v.SetData(this->mMat.release(), this->BlockSize(), this->BlockSize());
         GeneralDataCont<T> dc(v, "OnDisc", this->DumpFileName(), true);
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   void OperMat<T>::AddSumOneModeOpers
      (  const std::vector<mat_t>& arOneModeOperMatrices
      ,  T aCoef
      )
   {
      const auto& v_mat = arOneModeOperMatrices;
      if (v_mat.size() != this->mNModals.size())
      {
         std::stringstream ss;
         ss << "v_mat.size() (= " << v_mat.size()
            << ") != this->mNModals.size() (= " << this->mNModals.size()
            << ").";
         MIDASERROR(ss.str());
      }

      mat_t mat(this->NRowsBlock(), this->NColsBlock(), T(0));
      Uin m = 0;
      for(auto it = v_mat.begin(), end = v_mat.end(); it != end; ++it, ++m)
      {
         // Assert.
         if (  it->Nrows() != this->mNModals.at(m)
            || it->Ncols() != this->mNModals.at(m)
            )
         {
            std::stringstream ss;
            ss << "v_mat.at(m) is " << it->Nrows() << " x " << it->Ncols()
               << ", expected " << this->mNModals.at(m) << " x " << this->mNModals.at(m)
               ;
            MIDASERROR(ss.str());
         }
         
         // Add to tmp. matrix.
         MatRepVibOper<T>::DirProdOneModeOpers
            (  this->mNModals
            ,  std::set<Uin>{m}
            ,  std::vector<mat_t>{*it}
            ,  mat
            ,  T(1)
            ,  true
            ,  this->RowBeg()
            ,  0
            );
      }

      // Update matrix.
      const Uin size = mat.Nrows() * mat.Ncols();
      const mat_storage_t p = ColMajPtrFromMidasMatrix2(std::move(mat));
      for(Uin i = 0; i < size; ++i)
      {
         mMat[i] += aCoef * p[i];
      }

      // Update symmetry information.
      this->mSymType = SymType::GENERAL;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   void OperMat<T>::Zero
      (
      )
   {
      const Uin size = this->Nrows() * this->Ncols();
      for(Uin i = 0; i < size; ++i)
      {
         mMat[i] = T(0.0);
      }
      // Update symmetry information. Zero matrices are Hermitian...
      this->mSymType = SymType::HERMITIAN;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   template<bool TRANSPOSE>
   typename OperMat<T>::vec_t OperMat<T>::MatVecMult
      (  const vec_t& arVec
      )  const
   {
      vec_t v = WrapGEMV(arVec, TRANSPOSE);
#ifdef VAR_MPI
      if constexpr(TRANSPOSE)
      {
         vec_t v_tmp = v;
         midas::mpi::detail::WRAP_Allreduce
            (  v_tmp.data()
            ,  v.data()
            ,  v.Size()
            ,  midas::mpi::DataTypeTrait<T>::Get()
            ,  MPI_SUM
            ,  midas::mpi::CommunicatorWorld().GetMpiComm()
            );
      }
      else
      {
         for(Uin r = 0; r < midas::mpi::GlobalSize(); ++r)
         {
            v.MpiBcast(r, this->NRowsBlock(r), this->RowBeg(r));
         }
      }
#endif /* VAR_MPI */
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   typename OperMat<T>::vec_t OperMat<T>::GetRow
      (  Uin i
      )  const
   {
      vec_t v(this->NColsBlock(), T(0));
      const Uin r_owner = this->RankWithRow(i);
      if (r_owner == midas::mpi::GlobalRank())
      {
         v = this->GetRowLocal(i);
      }
#ifdef VAR_MPI
      v.MpiBcast(r_owner);
#endif /* VAR_MPI */
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   typename OperMat<T>::vec_t OperMat<T>::WrapGEMV
      (  const vec_t& arVec
      ,  bool aTranspose
      )  const
   {
      // Use GEMV for general matrix-vector multiplication,
      //    y := alpha*A*x    + beta*y,   or
      //    y := alpha*A**T*x + beta*y,
      // For present case, we want
      //    return A*x, or
      //    return x*A = A**T *x
      // so we have alpha = 1 and beta = 0.
      // Dimensions, A is MxN, x is N (not transposed) or M (transposed).

      char trans = aTranspose? 'T': 'N';   // Transpose or not?
      int m = this->NRowsBlock();          // Num. rows of matrix.
      int n = this->NColsBlock();          // Num. cols. of matrix.
      Uin offset = 0;
      Uin res_size = 0;
      Uin res_offset = 0;
      if (aTranspose)
      {
         res_size = this->Ncols();
         offset = this->RowBeg();
         if (arVec.Size() != this->Nrows()) MIDASERROR("arVec.Size() != Nrows()");
      }
      else
      {
         res_size = this->Nrows();
         res_offset = this->RowBeg();
         if (arVec.Size() != this->Ncols()) MIDASERROR("arVec.Size() != Ncols()");
      }
      vec_t res(res_size, T(0));

      T alpha = 1;                                    // The scalar alpha.
      T* a = mMat.get();                              // Ptr. to the matrix (col.maj.).
      int lda = std::max(1, m);                       // Leading dimension of matrix.
      T* x = const_cast<T*>(arVec.data() + offset);   // Ptr. to the x vector (RHS).
      int incx = 1;                                   // Increment/stride of data in x.
      T beta = 0;                                     // The scalar beta.
      T* y = res.data() + res_offset;                 // Ptr. to the y vector (LHS).
      int incy = 1;                                   // Increment/stride of data in y.

      midas::lapack_interface::gemv(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy);

      return res;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   Uin OperMat<T>::RankWithRow
      (  Uin aRow
      )  const
   {
      // Binary search on sorted random-access range.
      // upper_bound returns iterator to first element greater than aRow,
      // thus [*(it-1), *it) contains aRow, hence the decrement.
      auto it = std::upper_bound(mMpiRankRowIndices.begin(), mMpiRankRowIndices.end(), aRow);
      if (it == mMpiRankRowIndices.end())
      {
         MIDASERROR("aRow = "+std::to_string(aRow)+" out-of-range.");
      }
      --it;
      return std::distance(mMpiRankRowIndices.begin(), it);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   typename OperMat<T>::vec_t OperMat<T>::GetRowLocal
      (  Uin aRow
      )  const
   {
      if (aRow < this->RowBeg() || aRow >= this->RowEnd())
      {
         std::stringstream ss;
         ss << "aRow = " << aRow << " not in local row [beg;end) = [" << this->RowBeg() << ";" << this->RowEnd() << ").";
         MIDASERROR(ss.str());
      }
      vec_t v(this->NColsBlock(), T(0));
      const Uin row_local = aRow - this->RowBeg();
      for(Uin j = 0; j < v.Size(); ++j)
      {
         v[j] = this->mMat[j*this->NRowsBlock() + row_local];
      }
      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   typename OperMat<T>::SymType OperMat<T>::DetermineType
      (  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      )
   {
      using opdef_coef_t = decltype(std::declval<opdef_t>().Coef(In()));
      // At time of implementation (Jan 2020), OpDef only has _real_
      // coefficients, so we can base symmetry type on modalintegrals.
      if constexpr(!midas::type_traits::IsComplexV<opdef_coef_t>)
      {
         bool all_hermitian = true;
         bool all_anti_hermitian = true;
         for(LocalModeNr m = 0; m < arModInts.NModes(); ++m)
         {
            for(LocalOperNr o = 0; o < arModInts.NOper(m); ++o)
            {
               const auto& ints = arModInts.GetIntegrals(m, o);
               if (all_hermitian && !ints.IsHermitian())
               {
                  all_hermitian = false;
               }
               if (all_anti_hermitian && !ints.IsAntiHermitian())
               {
                  all_anti_hermitian = false;
               }
            }
         }

         if (all_hermitian)
         {
            return SymType::HERMITIAN;
         }
         else if (all_anti_hermitian)
         {
            return SymType::ANTIHERMITIAN;
         }
         else
         {
            return SymType::GENERAL;
         }
      }
      // If the else clause ever becomes relevant (complex OpDef coefs.)
      // implement accordingly. For now, the fallback option is
      // SymType::GENERAL.
      else
      {
         return SymType::GENERAL;
      }
   }

   /************************************************************************//**
    * Construct oper. mat.; tries to read in from existing file on disk, and
    * otherwise constructs from arguments:
    * 1. Generates file name based on modals, OpDef name, ModalIntegrals name.
    * (This is in itself not bulletproof.)
    * 2. Checks existence of file.
    * 3. If successful, checks value of peeked elements against current
    * constructor args.
    * 4. If match, uses data on disk.
    * 5. If not, constructs from new using FullOperator().
    *
    * @note
    *    About the peek-one-element strategy:
    *    Of course a proper hash on the OpDef/ModalIntegrals/arNModals data
    *    would be more bulletproof, but this is probably fine in normal
    *    use-cases (and the hash'ing is not trivial). Check a diagonal element,
    *    because off-diagonal ones is more likely to be generally zero if
    *    operator doesn't go high in coupling level.
    *    Also check an adjacent off-diagonal element, because diagonal
    *    elements _could_ be zero for highly symmetric operators/modal bases.
    ***************************************************************************/
   template<typename T>
   std::unique_ptr<T[]> OperMat<T>::ConstructMat
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      )  const
   {
      // Try reading in from disk.
      if (  midas::filesystem::IsFile(this->DumpFileName() + "_0")
         && this->BlockSize() > 1
         )
      {
         GeneralDataCont<T> dc;
         dc.GetFromExistingOnDisc(this->BlockSize(), this->mDumpFileName);
         dc.SaveUponDecon(false); // This will delete the file at end of scope.

         // Peek two elements:
         //    -  first, a diagonal elements, generally non-zero, whereas
         //    off-diagonal might be zero, if they correspond to higher
         //    coupling level than the given operator.
         //    -  second, the following off-diagonal element, since symmetry of
         //    matrix (anti-symmetric) might cause diagonal to be zero
         // Use elements to check that the file on disk contains data
         // corresponding to the current constructor args.

         // Find col.maj. address of first diag. element of block, (r_beg, r_beg),
         // then read that and next element, if block contains another one.
         const Uin peek_size = 2;
         Uin addr = this->RowBeg() * this->NRowsBlock();
         if (addr + 1 >= this->BlockSize() && this->BlockSize() > 1)
         {
            // If (r_beg, r_beg) is the _last_ element, read the one before that instead.
            --addr;
         }
         GeneralMidasVector<T> v_peek(peek_size);

         // File reading successful?
         if (decltype(dc)::Io::SUCCESS == dc.DataIo(IO_GET | IO_ROBUST, v_peek, v_peek.Size(), addr))
         {
            // Control value.
            bool peek_success = true;
            for(Uin i = 0, loc_addr = addr; i < v_peek.Size(); ++i, ++loc_addr)
            {
               const Uin row = this->RowBeg() + loc_addr % this->NRowsBlock();
               const Uin col = loc_addr / this->NRowsBlock();
               if (v_peek[i] != CalcSingleElementCtrl(arNModals, arOpDef, arModInts, row, col))
               {
                  peek_success = false;
               }
            }

            if (peek_success)
            {
               GeneralMidasVector<T> v(dc.Size());
               if (decltype(dc)::Io::SUCCESS == dc.DataIo(IO_GET | IO_ROBUST, v, v.Size()))
               {
                  return v.Release();
               }
            }
         }
      }

      // If falling through to here, call FullOperator.
      typename MatRepVibOper<T>::mat_t mat;
      MatRepVibOper<T>::FullOperator(arNModals, arOpDef, arModInts, mat, this->RowBeg(), this->RowEnd());
      return ColMajPtrFromMidasMatrix2(std::move(mat));
   }

   /************************************************************************//**
    * Construct zero oper. mat.
    ***************************************************************************/
   template<typename T>
   std::unique_ptr<T[]> OperMat<T>::ConstructMat
      (  const n_modals_t& arNModals
      )  const
   {
      auto size = this->Nrows() * this->Ncols();
      auto m = std::make_unique<T[]>(size);
      for(decltype(size) i = 0; i<size; ++i)
      {
         m[i] = T(0.0);
      }
      return m;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   std::vector<Uin> OperMat<T>::CalcMpiRankRowIndices
      (  Uin aMpiRanks
      )  const
   {
      if (aMpiRanks < 1)
      {
         MIDASERROR("aMpiRanks < 1");
      }

      std::vector<Uin> v;
      v.reserve(aMpiRanks + 1);
      for(Uin i = 0; i < aMpiRanks; ++i)
      {
         v.emplace_back((i*this->FullDim())/aMpiRanks);
      }
      v.emplace_back(this->FullDim());

      return v;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   std::string OperMat<T>::GenerateDumpFileName
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  Uin aRowBeg
      ,  Uin aRowEnd
      )
   {
      std::stringstream ss;
      ss << "opermat"
         << "__typeid_" << typeid(T).name()
         << "__opdef_" << arOpDef.Name()
         << "__modints_" << arModInts.Name()
         << "__nmodals"
         ;
      for(const auto& n: arNModals)
      {
         ss << "_" << n;
      }
      ss << "__rows_" << aRowBeg << "_" << aRowEnd;
      return midas::input::DeleteBlanks(ss.str());
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   std::string OperMat<T>::GenerateDumpFileName
      (  const n_modals_t& arNModals
      ,  Uin aRowBeg
      ,  Uin aRowEnd
      )
   {
      std::stringstream ss;
      ss << "opermat"
         << "__typeid_" << typeid(T).name()
         << "__nmodals"
         ;
      for(const auto& n: arNModals)
      {
         ss << "_" << n;
      }
      ss << "__rows_" << aRowBeg << "_" << aRowEnd;
      return midas::input::DeleteBlanks(ss.str());
   }

   /************************************************************************//**
    * @note
    *    Could be impl. more efficiently, but probably never a performance
    *    issue compared to calculating full matrix.
    ***************************************************************************/
   template<typename T>
   T OperMat<T>::CalcSingleElementCtrl
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  Uin aRowI
      ,  Uin aColJ
      )
   {
      typename MatRepVibOper<T>::mat_t mat(1, 1, T(0));
      MatRepVibOper<T>::FullOperator(arNModals, arOpDef, arModInts, mat, aRowI, aRowI+1, aColJ, aColJ+1);
      return mat[0][0];
   }

} /* namespace midas::util::matrep */

#endif/*OPERMAT_IMPL_H_INCLUDED*/
