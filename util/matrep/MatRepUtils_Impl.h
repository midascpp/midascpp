/**
 *******************************************************************************
 * 
 * @file    MatRepUtils_Impl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPUTILS_IMPL_H_INCLUDED
#define MATREPUTILS_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"

namespace midas::util::matrep
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   std::vector<T> GetElements
      (  const GeneralMidasVector<T>& arCont
      ,  Uin aBegin
      ,  Uin aEnd
      )
   {
      if (aEnd > arCont.Size())
      {
         MIDASERROR("aEnd goes out-of-range.");
      }
      if (aBegin > aEnd)
      {
         MIDASERROR("aBegin > aEnd");
      }
      std::vector<T> res(aEnd - aBegin);
      for(Uin i = 0; aBegin + i < aEnd; ++i)
      {
         res[i] = arCont[aBegin + i];
      }
      return res;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   std::vector<T> GetElements
      (  const GeneralDataCont<T>& arCont
      ,  Uin aBegin
      ,  Uin aEnd
      )
   {
      if (aEnd > arCont.Size())
      {
         MIDASERROR("aEnd goes out-of-range.");
      }
      if (aBegin > aEnd)
      {
         MIDASERROR("aBegin > aEnd");
      }
      GeneralMidasVector<T> mv(aEnd - aBegin);
      arCont.DataIo(IO_GET, mv, mv.Size(), aBegin);
      return std::vector<T>(mv.begin(), mv.end());
   }

   /************************************************************************//**
    * Example:
    *     arDims = {2,2,3}
    *     arMcr  = { {}   , {0}  , {1,2}      }
    *     arVec  = { e000 , e100 , e011, e012 }
    *     return = {{e000},{e100},{e011, e012}}
    *
    * @param[in] arDims
    *    Dimensions of full space.
    * @param[in] arMcr
    *    Range of ModeCombi%s:
    * @param[in] arVec
    *    The vector which contiguously stores elements of the ModeCombi%s of
    *    arMcr. Size must correspond to number of params given by arDims and
    *    arMcr.
    * @return
    *    Container where elements of each ModeCombi have been organized in a
    *    separate vector.
    ***************************************************************************/
   template<typename T, template<typename...> class CONT_T>
   std::vector<std::vector<T>> OrganizeMcrSpaceVec
      (  const std::vector<Uin>& arDims
      ,  const ModeCombiOpRange& arMcr
      ,  const CONT_T<T>& arVec
      )
   {
      if (arMcr.NumberOfModes() > 0)
      {
         if (arMcr.SmallestMode() < 0 || arMcr.LargestMode() >= arDims.size())
         {
            MIDASERROR("arMcr mode out-of-range for arDims.");
         }
      }

      std::vector<std::vector<T>> res;
      res.reserve(arMcr.Size());
      Uin addr = 0;

      if constexpr   (  std::is_same_v<CONT_T<T>, GeneralTensorDataCont<T>>
                     )
      {
         auto size = arVec.Size();
         MidasAssert(size == arMcr.Size(), "TensorDataCont and MCR should have same size!");
         for(In imc=0; imc<size; ++imc)
         {
            const auto& tens = arVec.GetModeCombiData(imc);
            const auto& dims = tens.GetDims();
            auto len = tens.TotalSize();
            std::vector<T> tmp(len);

            if (  dims.empty()
               )
            {
               tmp[0] = tens.GetScalar();
            }
            else
            {
               auto* data = tens.template StaticCast<SimpleTensor<T>>().GetData();
               for(In i=0; i<len; ++i)
               {
                  tmp[i] = data[i];
               }
            }

            res.emplace_back(std::move(tmp));
         }
      }
      else
      {
         for(const auto& mc: arMcr)
         {
            Uin mc_size = NumParams(mc, arDims);
            res.emplace_back(GetElements(arVec, addr, addr + mc_size));
            addr += mc_size;
         }
         if (addr < arVec.Size())
         {
            MIDASERROR("Didn't loop all the way through arVec.");
         }
      }

      return res;
   }

   /************************************************************************//**
    * Does the inverse of OrganizeMcrSpaceVec().
    ***************************************************************************/
   template<typename T, template<typename...> class CONT_T>
   CONT_T<T> McrOrganizedToStackedVec
      (  const std::vector<std::vector<T>>& arVec
      )
   {
      CONT_T<T> res;
      for(const auto& v: arVec)
      {
         res.Insert(res.Size(), v.begin(), v.end());
      }
      return res;
   }

   /************************************************************************//**
    * Takes parameters matching a space with "reduced" dimensions and extends
    * them to an extended space including a "secondary" space, i.e. the part of
    * the extended space that is outside the reduced subspace.
    * The returned parameters equal the input one within the reduced space and
    * are zero in the secondary space.
    *
    * A simple sketch of spaces (in a VCC context):
    * ~~~
    *       ---   ---   ---                                          +
    *       ---   ---   ---         secondary space                  |
    *       ---   ---   ---                                          |
    *    ______________________                                      |
    *       ---   ---   ---                              +           | extended
    *       ---   ---   ---         virtual space        |           | space
    *       ---   ---   ---                              | reduced   |
    *    ______________________                          | space     |
    *       -*-   -*-   -*-         occupied (reference) +           +
    *  m =   0     1     2
    * ~~~
    * 
    * Example:
    * ~~~
    *       arMcr           = {}, {0}, {1}, {0,1}
    *       arDimsReduced   = {3,2}
    *       arDimsExtended  = {4,3}
    *
    *       arParamsReduced = t_ref    for MC = {}
    *                         ---
    *                         t0_1     for MC = {0}
    *                         t0_2
    *                         ---
    *                         t1_1     for MC = {1}
    *                         ---
    *                         t01_11   for MC = {0,1}
    *                         t01_21
    *
    *       return          = t_ref    for MC = {}
    *                         ---
    *                         t0_1     for MC = {0}
    *                         t0_2
    *                         0
    *                         ---
    *                         t1_1     for MC = {1}
    *                         0
    *                         ---
    *                         t01_11   for MC = {0,1}
    *                         0
    *                         t01_21
    *                         0
    *                         0
    *                         0
    *
    * ~~~
    * 
    * @param[in] arMcr
    *    The ModeCombiOpRange used for constructing the reduced parameters,
    *    arParamsReduced.
    * @param[in] arDimsReduced
    *    The dimensions of the reduced space.
    * @param[in] arDimsExtended
    *    The dimensions of the extended space. (Must be larger than or equal to
    *    the reduced ones.)
    * @param[in] arParamsReduced
    *    The parameters in the reduced space; must fit with arMcr and
    *    arDimsReduced.
    * @return
    *    The arParamsReduced fitted into the extended space, with zeroes for
    *    new parameters that are in the secondary space of the extended space.
    ***************************************************************************/
   template<typename T>
   std::vector<std::vector<T>> ExtendToSecondarySpace
      (  const ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arDimsReduced
      ,  const std::vector<Uin>& arDimsExtended
      ,  const std::vector<std::vector<T>>& arParamsReduced
      )
   {
      // Assertions.
      if (  arMcr.Size() != arParamsReduced.size()
         || arMcr.SmallestMode() < 0
         || arMcr.LargestMode() >= arDimsReduced.size()
         || arDimsReduced.size() != arDimsExtended.size()
         )
      {
         std::stringstream ss;
         ss << "Size mismatch;\n"
            << "   arMcr.Size()           = " << arMcr.Size() << '\n'
            << "   arParamsReduced.size() = " << arParamsReduced.size() << '\n'
            << "   arMcr.SmallestMode()   = " << arMcr.SmallestMode() << '\n'
            << "   arMcr.LargestMode()    = " << arMcr.LargestMode() << '\n'
            << "   arDimsReduced.size()   = " << arDimsReduced.size() << '\n'
            << "   arDimsExtended.size()  = " << arDimsExtended.size() << '\n'
            ;
         MIDASERROR(ss.str());
      }
      for(  auto red = arDimsReduced.begin(), ext = arDimsExtended.begin()
         ;  red != arDimsReduced.end() && ext != arDimsExtended.end()
         ;  ++red, ++ext
         )
      {
         if (!(0 < *red && *red <= *ext))
         {
            std::stringstream ss;
            ss << "0 < red. dim. <= ext. dim. is not satisfied at index = " 
               << std::distance(arDimsReduced.begin(),red) << ".\n"
               << "   arDimsReduced  = " << arDimsReduced << '\n'
               << "   arDimsExtended = " << arDimsExtended << '\n'
               ;
            MIDASERROR(ss.str());
         }
      }
      {
         auto it_params = arParamsReduced.begin();
         for(auto it_mc = arMcr.begin(), end = arMcr.end(); it_mc != end; ++it_mc, ++it_params)
         {
            if (it_params->size() != NumParams(*it_mc, arDimsReduced))
            {
               std::stringstream ss;
               ss << "Wrong number of params for index = " << std::distance(arMcr.begin(), it_mc) << '\n'
                  << "   it_params->size() = " << it_params->size() << '\n'
                  << "   expected size     = " << NumParams(*it_mc, arDimsReduced) << '\n'
                  << "   ModeCombi         = " << it_mc->MCVec() << '\n'
                  << "   arDimsReduced     = " << arDimsReduced << '\n'
                  ;
               MIDASERROR(ss.str());
            }
         }
      }

      // Lambda for determining whether multiindex belongs to secondary space
      // (just a single index needs to be in secondary space for this to be the
      // case).
      // The passed index is down-shifted by 1 compared to the dimensions in
      // arDimsReduced, hence up-shift by 1 when comparing.
      auto is_sec_space =
         [&arDimsReduced](const std::set<Uin>& mc, const std::vector<Uin>& ind) -> bool
         {
            Uin i_ind = 0;
            for(auto it_m = mc.begin(), end = mc.end(); it_m != end; ++it_m, ++i_ind)
            {
               if (ind.at(i_ind) + 1 >= arDimsReduced.at(*it_m))
               {
                  return true;
               }
            }
            return false;
         };

      // The actual work.
      std::vector<std::vector<T>> v;
      v.reserve(arMcr.Size());

      for(Uin i_mc = 0; i_mc < arMcr.Size(); ++i_mc)
      {
         const auto& mc = arMcr.GetModeCombi(i_mc);
         const std::set<Uin> mc_set = SetFromVec(mc.MCVec());
         const auto& p_red = arParamsReduced.at(i_mc);
         const Uin ext_size = NumParams(mc, arDimsExtended);
         std::vector<T> v_mc;
         v_mc.reserve(ext_size);
         Uin abs_index_red = 0;
         std::vector<Uin> multiindex_ext(mc.Size(), 0);
         const auto sub_dims_ext = ShiftVals(SubsetDims(mc_set, arDimsExtended), -1);
         for(  Uin abs_index_ext = 0
            ;  abs_index_ext < ext_size
            ;  ++abs_index_ext, IncrMultiIndex(multiindex_ext, sub_dims_ext)
            )
         {
            if (is_sec_space(mc_set, multiindex_ext))
            {
               v_mc.emplace_back(0);
            }
            else
            {
               v_mc.emplace_back(p_red.at(abs_index_red));
               ++abs_index_red;
            }
         }
         v.emplace_back(std::move(v_mc));
      }

      return v;
   }

} /* namespace midas::util::matrep */

#endif/*MATREPUTILS_IMPL_H_INCLUDED*/
