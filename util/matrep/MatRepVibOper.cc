/**
 *******************************************************************************
 * 
 * @file    MatRepVibOper.cc
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepVibOper_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"

// Define instantiation macro.
#define INSTANTIATE_MATREPVIBOPER(T) \
   namespace midas::util::matrep \
   { \
      template class MatRepVibOper<T>; \
   } /* namespace midas::util::matrep */ \


// Instantiations.
INSTANTIATE_MATREPVIBOPER(Nb);
INSTANTIATE_MATREPVIBOPER(std::complex<Nb>);

#undef INSTANTIATE_MATREPVIBOPER

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
