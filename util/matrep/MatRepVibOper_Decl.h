/**
 *******************************************************************************
 * 
 * @file    MatRepVibOper_Decl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPVIBOPER_DECL_H_INCLUDED
#define MATREPVIBOPER_DECL_H_INCLUDED

// Standard headers.
#include <set>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"

// Forward declarations.
class ModeCombiOpRange;
class OpDef;
template<typename> class ModalIntegrals;

namespace midas::util::matrep
{
   /************************************************************************//**
    * @brief
    *    Full space matrix representations of typical operators in vibrational
    *    theory.
    *
    * This class is _not_ designed with effeciency in mind, so you might see
    * some computationally inefficient methods here and there; don't use it for
    * performance code. Rather it's designed for conceptual simplicity and for
    * providing a close link to vibrational theory. Specifically for casting
    * theoretical equations into a 1-to-1 matrix representation form (in the
    * _full_ vibrational state space), enabling a simple way of directly
    * calculating results for simple state spaces that can be used as controls
    * for the high-performance methods of Midas.
    *
    * The full state space is assumed to be ordered as follows; given \f$ M \f$
    * modes, each mode having a "dimension" (e.g. the number of modals) of \f$
    * m_0,\dots{},m_{M-1} \f$, the full space is a direct product space of
    * dimension \f$ m_0 \cdots{} m_{M_1} \f$. The multi-indexing increments the
    * \f$ M-1 \f$'th mode-index first and the \f$ 0 \f$'th mode-index last.
    *
    * Example:
    * 3 modes with 2, 2, 3 modals. 1-mode operators of each mode are square
    * matrices with 2, 2, 3 rows/columns respectively.
    * Consider the excitation operator \f$ \tau^{2}_{1} \f$ (from e.g. VCC
    * theory); it shifts occupation in mode 2 from modal 0 to 1. It's 1-mode
    * operator is given by the matrix representation
    * ~~~
    *       0  0  0
    *       1  0  0
    *       0  0  0
    * ~~~
    * It's full space representation is the direct product with the identity
    * operators for the other modes (because these are untouched by \f$
    * \tau^{2}_{1} \f$);
    * ~~~
    *              s0 =    0...                    1...               
    *              s1 =    0...        1...        0...        1...   
    *              s2 =    0  1  2     0  1  2     0  1  2     0  1  2
    *                                         
    *       r0  r1  r2
    *        0   0   0     0  0  0     0  0  0     0  0  0     0  0  0
    *        .   .   1     1  0  0     0  0  0     0  0  0     0  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    *
    *            1   0     0  0  0     0  0  0     0  0  0     0  0  0
    *            .   1     0  0  0     1  0  0     0  0  0     0  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    *
    *        1   0   0     0  0  0     0  0  0     0  0  0     0  0  0
    *        .   .   1     0  0  0     0  0  0     1  0  0     0  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    *
    *            1   0     0  0  0     0  0  0     0  0  0     0  0  0
    *            .   1     0  0  0     0  0  0     0  0  0     1  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    * ~~~
    *
    * Consider operator \f$ \tau^{0,2}_{1,2} \f$ (from VCC theory); it shifts
    * occupation in mode 0 from modal 0 to 1 and in mode 2 from modal 0 to 2.
    * Mode 1 is untouched.
    * It's full space representation is
    * ~~~
    *              s0 =    0...                    1...               
    *              s1 =    0...        1...        0...        1...   
    *              s2 =    0  1  2     0  1  2     0  1  2     0  1  2
    *                                         
    *       r0  r1  r2
    *        0   0   0     0  0  0     0  0  0     0  0  0     0  0  0
    *        .   .   1     0  0  0     0  0  0     0  0  0     0  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    *
    *            1   0     0  0  0     0  0  0     0  0  0     0  0  0
    *            .   1     0  0  0     0  0  0     0  0  0     0  0  0
    *                2     0  0  0     0  0  0     0  0  0     0  0  0
    *
    *        1   0   0     0  0  0     0  0  0     0  0  0     0  0  0
    *        .   .   1     0  0  0     0  0  0     0  0  0     0  0  0
    *                2     1  0  0     0  0  0     0  0  0     0  0  0
    *
    *            1   0     0  0  0     0  0  0     0  0  0     0  0  0
    *            .   1     0  0  0     0  0  0     0  0  0     0  0  0
    *                2     0  0  0     1  0  0     0  0  0     0  0  0
    * ~~~
    ***************************************************************************/
   template
      <  typename T
      >
   class MatRepVibOper
   {
      public:
         using value_t = T;
         using mat_t = GeneralMidasMatrix<value_t>;
         using vec_t = GeneralMidasVector<value_t>;

         //@{
         //! Default copies. Assignments deleted.
         MatRepVibOper(const MatRepVibOper&) = default;
         MatRepVibOper(MatRepVibOper&&) = default;
         MatRepVibOper& operator=(const MatRepVibOper&) = delete;
         MatRepVibOper& operator=(MatRepVibOper&&) = delete;
         ~MatRepVibOper() = default;
         //@}

         //! Construct zero matrix with given underlying dimensions in each mode.
         MatRepVibOper(std::vector<Uin>);

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{

         //! The dimension of the full space, i.e. product of dimensions for each mode.
         Uin FullDim() const;

         //! The number of modes.
         Uin NModes() const;

         //! The dimension of the given mode. Hard error if mode is out of range.
         Uin Dim(const Uin) const;

         //! The dimensions defining the full direct-product space.
         const std::vector<Uin>& Dims() const;

         //@{
         //! The currently stored matrix representation. (copy/move depending on *this &/&&)
         mat_t GetMatRep() const &;
         mat_t GetMatRep() &&;
         //@}
         //!@}

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{

         //! The identity matrix of given size.
         static mat_t Identity(const Uin);

         //! Converts abs. index in `[0;FullDim())` to `{i_0, ..., i_{M-1}}`.
         std::vector<Uin> MultiIndex(Uin) const;

         //! Converts multiindex like `{i_0, ..., i_{M-1}}` to abs. index in `[0;FullDim())`.
         Uin AbsIndex(const std::vector<Uin>&) const;

         //! Merges the given indices, with the given modes deciding how to "zip" them.
         static std::vector<Uin> CombineIndices
            (  const std::set<Uin>& arModesOfFirst
            ,  const std::vector<Uin>& arFirst
            ,  const std::vector<Uin>& arSecond
            );
         
         //! Extract, from a full space vector, the excitation parameters of given modes.
         static std::vector<value_t> ExtractExciParams
            (  const vec_t& arVFull
            ,  const std::set<Uin>& arModes
            ,  const std::vector<Uin>& arDims
            );

         //! Extract, from a full space vector, the exc. params. of given modes, using dims. of *this.
         std::vector<value_t> ExtractExciParams
            (  const vec_t& arVFull
            ,  const std::set<Uin>& arModes
            )  const;

         //! Converts a vector from the full space, to the excitation-from-reference space.
         static vec_t ExtractToMcrSpace
            (  const vec_t& arVFull
            ,  const ModeCombiOpRange& arMcr
            ,  const std::vector<Uin>& arDims
            );

         //! Converts a vector from the full space, to the excitation-from-reference space.
         vec_t ExtractToMcrSpace
            (  const vec_t& arVFull
            ,  const ModeCombiOpRange& arMcr
            )  const;

         //!@}

         /******************************************************************//**
          * @name Operators
          *********************************************************************/
         //!@{

         //! Direct product of 1-mode operators for each mode.
         static void DirProdOneModeOpers
            (  const std::vector<Uin>& arDims
            ,  const std::vector<mat_t>& arOpers
            ,  mat_t& arMat
            ,  T aScaleFact = T(1)
            ,  bool aAddTo = false
            ,  Uin aRowBeg = 0
            ,  Uin aColBeg = 0
            );
         //! Direct product of 1-mode operators for given modes. (Identity for rest.)
         static void DirProdOneModeOpers
            (  const std::vector<Uin>& arDims
            ,  const std::set<Uin>& arModes
            ,  const std::vector<mat_t>& arOpers
            ,  mat_t& arMat
            ,  T aScaleFact = T(1)
            ,  bool aAddTo = false
            ,  Uin aRowBeg = 0
            ,  Uin aColBeg = 0
            );
         //! Full operator (e.g. Hamiltonian) from OpDef and associated ModalIntegrals.
         static void FullOperator
            (  const std::vector<Uin>& arDims
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<value_t>& arModInts
            ,  const std::unordered_map<LocalModeNr,Uin>& arModeMap
            ,  mat_t& arMat
            ,  Uin aRowBeg = 0
            ,  Uin aRowEnd = std::numeric_limits<Uin>::max()
            ,  Uin aColBeg = 0
            ,  Uin aColEnd = std::numeric_limits<Uin>::max()
            ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode = {false,0}
            );
         //! Like the other FullOperator(), using mode-map defined by OpDef::GetGlobalModeNr().
         static void FullOperator
            (  const std::vector<Uin>& arDims
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<value_t>& arModInts
            ,  mat_t& arMat
            ,  Uin aRowBeg = 0
            ,  Uin aRowEnd = std::numeric_limits<Uin>::max()
            ,  Uin aColBeg = 0
            ,  Uin aColEnd = std::numeric_limits<Uin>::max()
            ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode = {false,0}
            );
         //! Direct product of 1-mode operators for each mode.
         void DirProdOneModeOpers
            (  const std::vector<mat_t>& arOpers
            );
         //! Direct product of 1-mode operators for given modes. (Identity for rest.)
         void DirProdOneModeOpers
            (  const std::set<Uin>& arModes
            ,  const std::vector<mat_t>& arOpers
            );
         //! Full operator (e.g. Hamiltonian) from OpDef and associated ModalIntegrals.
         void FullOperator
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<value_t>& arModInts
            ,  const std::unordered_map<LocalModeNr,Uin>& arModeMap
            ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode = {false,0}
            );
         //! Like the other FullOperator(), using mode-map defined by OpDef::GetGlobalModeNr().
         void FullOperator
            (  const OpDef& arOpDef
            ,  const ModalIntegrals<value_t>& arModInts
            ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode = {false,0}
            );
         //! \f$ E^{m,...}_{r,...;s,...} \f$. Annihilates and creates occupation for given modes. (Use ShiftOperBraket() if you can!)
         void ShiftOper
            (  const std::set<Uin>& arModes
            ,  const std::vector<Uin>& arCreate
            ,  const std::vector<Uin>& arAnnihilate
            ,  const value_t aVal = value_t(1)
            );
         //! T (cluster) operator for ModeCombi. Annihilates (0,...), creates in all excited.
         void ClusterOper
            (  const std::set<Uin>& arModes
            ,  const std::vector<value_t>& arAmps
            );
         //! A full ModeCombiOpRange cluster operator.
         void ClusterOper
            (  const ModeCombiOpRange& arMcr
            ,  const std::vector<std::vector<value_t>>& arAmps
            );
         //! (Anti-Herm.) 1-mode \f$ \kappa^m = \sum \kappa_{ai} E^m_{ai} - \kappa_{ai}^* E^m_{ia} \f$.
         void KappaOper
            (  const Uin aMode
            ,  const std::vector<value_t>& arCoefs
            );
         //! Sum of 1-mode \f$ \kappa^m \f$ operators for given modes.
         void KappaOper
            (  const std::set<Uin>& arModes
            ,  const std::vector<std::vector<value_t>>& arCoefs
            );
         //! Sum of 1-mode \f$ \kappa^m \f$ operators for all modes.
         void KappaOper
            (  const std::vector<std::vector<value_t>>& arCoefs
            );
         //! Exponential of (an already contained) cluster operator. (Use TrfExpClusterOper in stead!)
         mat_t ExpClusterOper
            (  const value_t aCoef = value_t(1)
            )  const;
         //!@}

      private:
         //! The inherent dimensions of the space.
         const std::vector<Uin> mDims;

         //! The currently stored matrix representation.
         mat_t mMat;

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{

         //! Do the sub-indices match those of the full index within the given modes?
         static bool PartialIndexMatch
            (  const std::set<Uin>& arModes
            ,  const std::vector<Uin>& arSubIndex
            ,  const std::vector<Uin>& arFullIndex
            );

         //! Add to all the matrix elements corresponding to \f$ E^{m,...}_{r,...;s,...} \f$.
         void AddToShiftOperElems
            (  const std::set<Uin>& arModes
            ,  const std::vector<Uin>& arCreate
            ,  const std::vector<Uin>& arAnnihilate
            ,  const value_t aVal = value_t(1)
            );

         //! For validating object after construction. Hard error if invalid.
         void SanityCheck() const;

         //!@}
   };

} /* namespace midas::util::matrep */

#endif/*MATREPVIBOPER_DECL_H_INCLUDED*/
