/**
 *******************************************************************************
 * 
 * @file    MatRepVibOper_Impl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPVIBOPER_IMPL_H_INCLUDED
#define MATREPVIBOPER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "util/Math.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "util/matrep/MatRepUtils.h"
#include "util/InterfaceOpenMP.h"

namespace midas::util::matrep
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
MatRepVibOper<T>::MatRepVibOper
   (  std::vector<Uin> aDims
   )
   :  mDims(std::move(aDims))
   ,  mMat(Product(mDims), value_t(0))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
Uin MatRepVibOper<T>::FullDim
   (
   )  const
{
   return Product(mDims);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
Uin MatRepVibOper<T>::NModes
   (
   )  const
{
   return mDims.size();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
Uin MatRepVibOper<T>::Dim
   (  const Uin aMode
   )  const
{
   try
   {
      return mDims.at(aMode);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Caught out-of-range: "+std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
const std::vector<Uin>& MatRepVibOper<T>::Dims
   (
   )  const
{
   return mDims;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
typename MatRepVibOper<T>::mat_t MatRepVibOper<T>::GetMatRep
   (
   )  const &
{
   return mMat;
}
template
   <  typename T
   >
typename MatRepVibOper<T>::mat_t MatRepVibOper<T>::GetMatRep
   (
   )  &&
{
   return std::move(mMat);
}

/***************************************************************************//**
 * See other DirProdOneModeOpers().
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::DirProdOneModeOpers
   (  const std::vector<Uin>& arDims
   ,  const std::vector<mat_t>& arOpers
   ,  mat_t& arMat
   ,  T aScaleFact
   ,  bool aAddTo
   ,  Uin aRowBeg
   ,  Uin aColBeg
   )
{
   const auto& dims = arDims;
   const Uin n_modes = dims.size();
   const Uin full_dim = Product(dims);
   aRowBeg = std::min(full_dim, aRowBeg);
   aColBeg = std::min(full_dim, aColBeg);
   auto& mat = arMat;
   
   // Assert dimensionalities.
   if (!(arOpers.size() == n_modes)) MIDASERROR("Wrong num. modes.");
   for(Uin m = 0; m < n_modes; ++m)
   {
      if (!(arOpers.at(m).IsSquare())) MIDASERROR("Non-square operator matrix.");
      if (!(arOpers.at(m).Nrows() == dims.at(m))) MIDASERROR("Wrong dimension.");
   }
   if (  aRowBeg + mat.Nrows() > full_dim
      || aColBeg + mat.Ncols() > full_dim
      )
   {
      MIDASERROR(std::string("Dimensions mismatch;\n")+
            "   mat      = " + std::to_string(mat.Nrows())+" x "+std::to_string(mat.Ncols()) + "\n" +
            "   aRowBeg  = " + std::to_string(aRowBeg) + "\n" +
            "   aColBeg  = " + std::to_string(aColBeg) + "\n" +
            "   full_dim = " + std::to_string(full_dim)+ "\n"
         );
   }

   if constexpr   (  MPI_DEBUG
                  )
   {
      Mout  << " Direct product of one-mode operators. Input matrix:"   << mat << std::endl;
   }

   // Form direct product.
   const std::vector<Uin> init_multi_j = ::midas::util::matrep::MultiIndex(aColBeg, dims);
   #pragma omp parallel
   {
      const Uin i_beg = omp_get_thread_num()*mat.Nrows()/omp_get_max_threads();
      const Uin i_end = (omp_get_thread_num()+1)*mat.Nrows()/omp_get_max_threads();
      std::vector<Uin> multi_i = ::midas::util::matrep::MultiIndex(aRowBeg+i_beg, dims);
      std::vector<Uin> multi_j(init_multi_j.size());
      for(Uin i = i_beg; i < i_end; ++i, IncrMultiIndex(multi_i,dims))
      {
         multi_j = init_multi_j;
         for(Uin j = 0; j < mat.Ncols(); ++j, IncrMultiIndex(multi_j,dims))
         {
            T elem = aScaleFact;
            for(Uin m = 0; m < n_modes; ++m)
            {
               elem *= arOpers[m][multi_i[m]][multi_j[m]];
            }
            if (aAddTo)
            {
               mat[i][j] += elem;
            }
            else
            {
               mat[i][j] = elem;
            }
         }
      }
   }
}

/***************************************************************************//**
 * Zeroes the current content, then constructs direct product of 1-mode
 * operators.
 *
 * Example: 2 modes, dim = 2 for both.
 * ~~~
 *    0 -1     and   2  3     =   0  0 -2 -3
 *    2  1          -1  0         0  0  1  0
 *                                4  6  2  3
 *                               -2  0 -1  0
 * ~~~
 *
 * @note
 *    Corresponds to terms of a sum-over-product operator (Hamiltonian and
 *    others).
 *
 * @param[in] arOpers
 *    Vector with one 1-mode operator matrix for each mode, which must be a
 *    square matrix of size Dim().
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::DirProdOneModeOpers
   (  const std::vector<mat_t>& arOpers
   )
{
   DirProdOneModeOpers(this->Dims(), arOpers, this->mMat);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::DirProdOneModeOpers
   (  const std::vector<Uin>& arDims
   ,  const std::set<Uin>& arModes
   ,  const std::vector<mat_t>& arOpers
   ,  mat_t& arMat
   ,  T aScaleFact
   ,  bool aAddTo
   ,  Uin aRowBeg
   ,  Uin aColBeg
   )
{
   const auto& dims = arDims;
   const Uin n_modes = dims.size();
   auto& mat = arMat;
   
   // Assertions.
   if (!(arModes.size() == arOpers.size())) MIDASERROR("Size mismatch, num. modes/opers.");
   if (!(arModes.size() <= n_modes)) MIDASERROR("Too many modes given.");
   if (!arModes.empty())
   {
      if (!(*arModes.rbegin() < n_modes)) MIDASERROR("Largest mode number is out-of-range.");
   }

   // Build a vector of the given operators and identity operators in remaining
   // modes.
   std::vector<mat_t> v_opers;
   v_opers.reserve(n_modes);
   const auto it_beg = arModes.begin();
   for(Uin m = 0; m < n_modes; ++m)
   {
      // Find mode, and emplace corresponding operator. If mode not found, at()
      // will throw, and we'll emplace with the 1-mode identity operator.
      try
      {
         v_opers.emplace_back(arOpers.at(std::distance(it_beg, arModes.find(m))));
      }
      catch(const std::out_of_range&)
      {
         v_opers.emplace_back(Identity(dims.at(m)));
      }
   }
   DirProdOneModeOpers(dims, v_opers, mat, aScaleFact, aAddTo, aRowBeg, aColBeg);
}

/***************************************************************************//**
 * Zeroes the current content, then constructs direct product of given 1-mode
 * operators for given modes. For the other modes the 1-mode identity matrix is
 * implicitly assumed.
 *
 * Example: 2 modes, dim = 2 for both. arModes = {0}.
 * ~~~
 *    0 -1     and   1  0     =   0  0 -1  0
 *    2  1           0  1         0  0  0 -1
 *   (arg.)      (implicit)       2  0  1  0
 *                                0  2  0  1
 * ~~~
 *
 * @note
 *    Corresponds to terms of a sum-over-product operator (Hamiltonian and
 *    others).
 *
 * @param[in] arModes
 *    The modes that the matrices in arOpers correspond to.
 * @param[in] arOpers
 *    Vector with one 1-mode operator matrix for arModes; must be square
 *    matrices of size Dim().
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::DirProdOneModeOpers
   (  const std::set<Uin>& arModes
   ,  const std::vector<mat_t>& arOpers
   )
{
   DirProdOneModeOpers(this->Dims(), arModes, arOpers, this->mMat);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::FullOperator
   (  const std::vector<Uin>& arDims
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<value_t>& arModInts
   ,  const std::unordered_map<LocalModeNr,Uin>& arModeMap
   ,  mat_t& arMat
   ,  Uin aRowBeg
   ,  Uin aRowEnd
   ,  Uin aColBeg
   ,  Uin aColEnd
   ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode
   )
{
   const auto& dims = arDims;
   const Uin n_modes = dims.size();
   const Uin full_dim = Product(dims);
   aRowBeg = std::min(full_dim, aRowBeg);
   aRowEnd = std::max(aRowBeg, std::min(full_dim, aRowEnd));
   aColBeg = std::min(full_dim, aColBeg);
   aColEnd = std::max(aColBeg, std::min(full_dim, aColEnd));
   auto& mat = arMat;
   const auto& [active_only, active_mode] = arOnlyActiveTermsForMode;

   // Assertions.
   if (arOpDef.NmodesInOp() > arModInts.NModes()) MIDASERROR("OpDef; more modes than ModalIntegrals.");
   Uin m_glob = 0;
   for(LocalModeNr m = 0; m < arOpDef.NmodesInOp(); ++m)
   {
      try
      {
         m_glob = arModeMap.at(m);
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("arModeMap has no LocalModeNr m = " +std::to_string(m));
      }
      if (m_glob >= n_modes) MIDASERROR("Mode m_glob = "+std::to_string(m_glob)+" (from m = "+std::to_string(m)+") out-of-range for this object.");
      if (arOpDef.NrOneModeOpers(m) != arModInts.NOper(m)) MIDASERROR("Num. opers. mismatch.");
      if (arModInts.NModals(m) != dims.at(m_glob)) MIDASERROR("NModals mismatch.");
   }
   if (active_only && active_mode >= arOpDef.NmodesInOp())
   {
      MIDASERROR("Active mode = "+std::to_string(active_mode)+" >= arOpDef.NmodesInOp() = "+std::to_string(arOpDef.NmodesInOp())+".");
   }

   // Loop through OpDef and add direct products of mod.ints. to matrix.
   mat.SetNewSize(aRowEnd - aRowBeg, aColEnd - aColBeg, false, false);
   mat.Zero();
   const Uin n_terms = active_only? arOpDef.NactiveTerms(active_mode): arOpDef.Nterms();
   for(Uin i_term = 0; i_term < n_terms; ++i_term)
   {
      const Uin t = active_only? arOpDef.TermActive(active_mode, i_term): i_term;
      std::vector<mat_t> v_opers;
      std::set<Uin> s_modes;
      for(Uin i_fac = 0; i_fac < arOpDef.NfactorsInTerm(t); ++i_fac)
      {
         const LocalModeNr& m = arOpDef.ModeForFactor(t, i_fac);
         const LocalOperNr& o = arOpDef.OperForFactor(t, i_fac);
         try
         {
            m_glob = arModeMap.at(m);
         }
         catch(const std::out_of_range& oor)
         {
            MIDASERROR("arModeMap has no LocalModeNr m = " +std::to_string(m));
         }
         s_modes.emplace_hint(s_modes.end(), m_glob);
         v_opers.emplace_back(arModInts.GetIntegrals(m,o));
      }
      DirProdOneModeOpers(dims, s_modes, v_opers, mat, arOpDef.Coef(t), true, aRowBeg, aColBeg);
   }
}

/***************************************************************************//**
 * Sets the object equal to the (full space) matrix representation
 * corresponding to the given operator (OpDef) provided with the given 1-mode
 * modal integrals (matrix elements).
 *
 * @note
 *    The OpDef sum-over-product structure is defined in terms of _local_ modes
 *    (LocalModeNr). For some cases these may not be in correspondence with the
 *    modes of the system (GlobalModeNr), nor your intention with this
 *    MatRepVibOper object.
 *    Therefore you must provide the arModeMap which defines how each OpDef
 *    _local_ mode should be mapped to the modes/degrees of freedom of this
 *    object.
 *    See MatRepVibOper<T>::FullOperator(const OpDef&, const ModalIntegrals<value_t>&),
 *    which uses the mapping defined by arOpDef's
 *    OpDef::GetGlobalModeNr(const LocalModeNr).
 *    See midas::test::matrep::viboper::FullOperatorFromOpDefB for an example
 *    of an operator where local and global modes are not equal.
 *
 * @param[in] arOpDef
 *    Defines the (sum-over-product) _structure_ of the operator, i.e.
 *    coefficients of each terms, and which (local) modes and 1-mode operators
 *    are contained in each term.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator of each (local) mode.
 * @param[in] arModeMap
 *    Defines how LocalModeNr%s of arOpDef and arModInts shall be mapped to
 *    modes/degrees of freedom of this object.
 * @param[in] arOnlyActiveTermsForMode
 *    first (bool): Whether to only use active terms.
 *    second (Uin): The active mode, if bool is true. (If bool is false, this
 *    has no effect.)
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::FullOperator
   (  const OpDef& arOpDef
   ,  const ModalIntegrals<value_t>& arModInts
   ,  const std::unordered_map<LocalModeNr,Uin>& arModeMap
   ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode
   )
{
   FullOperator(this->Dims(), arOpDef, arModInts, arModeMap, this->mMat, 0, std::numeric_limits<Uin>::max(), 0, std::numeric_limits<Uin>::max(), arOnlyActiveTermsForMode);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::FullOperator
   (  const std::vector<Uin>& arDims
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<value_t>& arModInts
   ,  mat_t& arMat
   ,  Uin aRowBeg
   ,  Uin aRowEnd
   ,  Uin aColBeg
   ,  Uin aColEnd
   ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode
   )
{
   std::unordered_map<LocalModeNr,Uin> mode_map;
   for(LocalModeNr m = 0; m < arOpDef.NmodesInOp(); ++m)
   {
      mode_map[m] = arOpDef.GetGlobalModeNr(m);
   }

   FullOperator(arDims, arOpDef, arModInts, mode_map, arMat, aRowBeg, aRowEnd, aColBeg, aColEnd, arOnlyActiveTermsForMode);
}

/***************************************************************************//**
 * Calls FullOperator(const OpDef&, const ModalIntegrals<value_t>&,
 * std::unordered_map<LocalModeNr,Uin>&) using a mode-map defined in terms of
 * the OpDef's stored LocalModeNr-to-GlobalModeNr mapping (through
 * OpDef::GetGlobalModeNr()).
 *
 * @param[in] arOpDef
 *    Defines the (sum-over-product) _structure_ of the operator, i.e.
 *    coefficients of each terms, and which (local) modes and 1-mode operators
 *    are contained in each term.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator of each (local) mode.
 * @param[in] arOnlyActiveTermsForMode
 *    first (bool): Whether to only use active terms.
 *    second (Uin): The active mode, if bool is true. (If bool is false, this
 *    has no effect.)
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::FullOperator
   (  const OpDef& arOpDef
   ,  const ModalIntegrals<value_t>& arModInts
   ,  const std::pair<bool,Uin>& arOnlyActiveTermsForMode
   )
{
   FullOperator(this->Dims(), arOpDef, arModInts, this->mMat, 0, std::numeric_limits<Uin>::max(), 0, std::numeric_limits<Uin>::max(), arOnlyActiveTermsForMode);
}

/***************************************************************************//**
 * @warning
 *    Use ShiftOperBraket() in stead if you can! Faster!
 *
 * Zeroes the current content, then constructs matrix representation of shift
 * operator \f$ E^{m,...}_{r,...;s,...} \f$, with \f$ m,... \f$ begin the
 * modes, \f$ r,... \f$ the creation indcies and \f$ s,... \f$ the annihilation
 * indices.
 *
 * Possibly multiplied by a value.
 *
 * Example: 2 modes, dim = 2 for both.
 * \f$ E^{0}_{1,0} \f$ shifts occupation from 0 to 1 in mode 0, i.e. it shifts
 *     (0,0) -> (1,0)
 *     (0,1) -> (1,1)
 * and thus has the matrix representation
 * ~~~
 *    0 0 0 0
 *    0 0 0 0
 *    1 0 0 0
 *    0 1 0 0
 * ~~~
 *
 * @param[in] arModes
 *    The modes, \f$ m,... \f$.
 * @param[in] arCreate
 *    The creation indices, \f$ r,... \f$.
 * @param[in] arAnnihilate
 *    The annihilation indices, \f$ s,... \f$.
 * @param[in] aVal
 *    Multiply \f$ E^{m,...}_{r,...;s,...} \f$ with this value.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::ShiftOper
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arCreate
   ,  const std::vector<Uin>& arAnnihilate
   ,  const value_t aVal
   )
{
   // Assertions.
   if (!(arModes.size() == arCreate.size() && arModes.size() == arAnnihilate.size()))
   {
      MIDASERROR("Size mismatch, num. modes and annihilation/creation indices.");
   }
   Uin i_m = 0;
   for(const auto& m: arModes)
   {
      if (!(arCreate.at(i_m) < Dim(m))) MIDASERROR("Creation index out of range.");
      if (!(arAnnihilate.at(i_m) < Dim(m))) MIDASERROR("Annihilation index out of range.");
      ++i_m;
   }

   // Zero matrix, then add to specifiec elements.
   mMat.Zero();
   AddToShiftOperElems(arModes, arCreate, arAnnihilate, aVal);
}

/***************************************************************************//**
 * Zeroes the matrix, then constructs a cluster operator for the given mode
 * combination, i.e.
 * \f$ T^{m,...} = \sum_{a,...} t^{m,...}_{a,...} \tau^{m,...}_{a,...} \f$
 * where the excitation operators are shift operators,
 * \f$ \tau^{m,...}_{a,...} = E^{m,...}_{a,...;i,...} \f$,
 * that always annihilate the reference indices \f$ i,... \f$ (always 0,... in
 * this class).
 *
 * @parblock
 * @note
 *    Corresponds to the cluster operators used in VCC, but the coefficient
 *    operators of VCI[n] take the same shape, in the _ansatz_
 *    \f$ \vert VCI \rangle = (C_0 + C_1 + C_2 + \dots{})\vert \Phi \rangle \f$.
 * @endparblock
 *
 * @parblock
 * @note
 *    Cluster operators commute. Any cluster operator for _one_ specific
 *    non-empty ModeCombi \f$ T^{\mathbf{m}} \f$ is, by construction, nilpotent
 *    with index 2, i.e. \f$ (T^{\mathbf{m}})^2 = 0 \f$.
 * @endparblock
 *
 * @param[in] arModes
 *    The mode combination of the cluster operator.
 * @param[in] arAmps
 *    The corresponding amplitudes. If dimensions of modes are `d0,d1,...`, the
 *    number of amplitudes must be `(d0-1)*(d1-1)*...`.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::ClusterOper
   (  const std::set<Uin>& arModes
   ,  const std::vector<value_t>& arAmps
   )
{
   const auto& exci_dims = ShiftVals(SubsetDims(arModes, mDims), -1);
   const Uin n_exci = Product(exci_dims);
   if (arAmps.size() != n_exci) MIDASERROR("Size mismatch, number of amps.");

   mMat.Zero();
   const std::vector<Uin> index_anni(arModes.size(), 0);
   for(Uin i_crea = 0; i_crea < n_exci; ++i_crea)
   {
      const auto& index_crea = ShiftVals(matrep::MultiIndex(i_crea, exci_dims), +1);
      AddToShiftOperElems(arModes, index_crea, index_anni, arAmps.at(i_crea));
   }
}

/***************************************************************************//**
 * Zeroes the matrix, then constructs a full cluster operator,
 * \f[
 *    T 
 *    =  \sum_{\mathbf{m} \in \text{MCR}} T^{\mathbf{m}}
 *    =  \sum_{\mathbf{m} \in \text{MCR}} 
 *       \sum_{a,...} t^{\mathbf{m}}_{a,...} \tau^{\mathbf{m}}_{a,...},
 * \f]
 * for the given MCR (ModeCombiOpRange).
 *
 * @parblock
 * @note
 *    Corresponds to the cluster operators used in VCC, but the coefficient
 *    operators of VCI[n] take the same shape, in the _ansatz_
 *    \f$ \vert VCI \rangle = (C_0 + C_1 + C_2 + \dots{})\vert \Phi \rangle \f$.
 * @endparblock
 *
 * @parblock
 * @note
 *    Cluster operators commute. Any cluster operator \f$ T \f$ (for any
 *    ModeCombiOpRange _not_ containing the _empty_ ModeCombi) is, by
 *    construction, nilpotent with index \f$ M + 1 \f$, \f$ M \f$ being the
 *    number of modes, i.e.  \f$ T^{M+1} = 0 \f$.
 * @endparblock
 *
 * @warning
 *    Commonly in VCC theory, there is no \f$ T_0 \f$ term in the cluster
 *    operator (this would correspond to a cluster operator for the empty
 *    ModeCombi, which would just be an overall scale/phase factor of the VCC
 *    state).
 *    Therefore take care to either erase the empty ModeCombi from the
 *    ModeCombiOpRange (ModeCombiOpRange::Erase()), or _explicitly_ provide a
 *    zero amplitude when calling this method.
 *
 * @param[in] arMcr
 *    Determines which ModeCombi%s there shall be generated excitations for.
 * @param[in] arAmps
 *    Cluster amplitudes for each ModeCombi of arMcr.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::ClusterOper
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<value_t>>& arAmps
   )
{
   if (arMcr.Size() != arAmps.size()) MIDASERROR("Num. mode combinations mismatch.");

   mMat.Zero();
   MatRepVibOper<T> matrep_tmp(*this);
   auto it_mc = arMcr.begin();
   for(auto it_amps = arAmps.begin(), end = arAmps.end(); it_amps != end; ++it_amps, ++it_mc)
   {
      matrep_tmp.ClusterOper(SetFromVec(it_mc->MCVec()), *it_amps);
      mMat += matrep_tmp.GetMatRep();
   }
}

/***************************************************************************//**
 * Constructs a 1-mode \f$ \kappa^m \f$ operator for mode \f$ m \f$ (in the
 * full space, i.e. with the identity operator implicitly assumed for the other
 * modes).
 *
 * It is defines as
 * \f[
 *    \kappa^m = \sum_{a} (\kappa^m_{ai} E^m_{ai} - (\kappa^m_{ai})^* E^m_{ia})
 * \f]
 * and is anti-Hermitian by construction.
 *
 * @note
 *    Used to parametrize unitary transformations of modals in e.g. VSCF and
 *    TDH theory (in the form of \f$ \exp(\pm \kappa^m) \f$.
 *
 * @param[in] aMode
 *    The mode on which it operates.
 * @param[in] arCoefs
 *    The \f$ \kappa^m_{ai} \f$ parameters, of which there must be 
 *    `Dim(aMode) - 1`.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::KappaOper
   (  const Uin aMode
   ,  const std::vector<value_t>& arCoefs
   )
{
   if (aMode >= NModes()) MIDASERROR("Mode out-of-range.");
   if (arCoefs.size()+1 != Dim(aMode)) MIDASERROR("There should be Dim(m)-1 kappa coefficients.");

   // Make the 1-mode kappa operator, then make direct product.
   mat_t kappa_m(Dim(aMode), Dim(aMode), value_t(0));
   const Uin i = 0;
   for(Uin a = 1; a < Dim(aMode); ++a)
   {
      const auto& kappa_ai = arCoefs.at(a-1);
      kappa_m[a][i] = kappa_ai;
      kappa_m[i][a] = -midas::math::Conj(kappa_ai);
   }
   DirProdOneModeOpers(std::set<Uin>{aMode}, std::vector<mat_t>{kappa_m});
}

/***************************************************************************//**
 * Constructs a many-mode 
 * \f$ \kappa = \sum_m \kappa^m \f$ 
 * operator summing over the given modes. (Anti-Hermitian by construction.)
 *
 * @note
 *    Used to parametrize unitary transformations of modals in e.g. VSCF and
 *    TDH theory (in the form of \f$ \exp(\pm \kappa^m) \f$.
 *
 * @param[in] arModes
 *    The modes for which a \f$ \kappa^m \f$ operator is added.
 * @param[in] arCoefs
 *    The \f$ \kappa^m_{ai} \f$ parameters of each mode; of which there must be 
 *    `Dim(aMode) - 1` for a given mode.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::KappaOper
   (  const std::set<Uin>& arModes
   ,  const std::vector<std::vector<value_t>>& arCoefs
   )
{
   if (arModes.size() != arCoefs.size()) MIDASERROR("Num. modes mismatch.");

   mMat.Zero();
   MatRepVibOper<T> matrep_tmp(*this);
   auto it_m = arModes.begin();
   for(auto it_coef = arCoefs.begin(), end = arCoefs.end(); it_coef != end; ++it_coef, ++it_m)
   {
      matrep_tmp.KappaOper(*it_m, *it_coef);
      mMat += matrep_tmp.GetMatRep();
   }
}

/***************************************************************************//**
 * Constructs a many-mode 
 * \f$ \kappa = \sum_m \kappa^m \f$ 
 * operator summing over all modes. (Anti-Hermitian by construction.)
 *
 * @note
 *    Used to parametrize unitary transformations of modals in e.g. VSCF and
 *    TDH theory (in the form of \f$ \exp(\pm \kappa^m) \f$.
 *
 * @param[in] arCoefs
 *    The \f$ \kappa^m_{ai} \f$ parameters of each mode; of which there must be 
 *    `Dim(aMode) - 1` for a given mode.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::KappaOper
   (  const std::vector<std::vector<value_t>>& arCoefs
   )
{
   if (arCoefs.size() != NModes()) MIDASERROR("Expected NModes() 1-mode kappa vectors.");

   std::set<Uin> s;
   for(Uin m = 0; m < NModes(); ++m)
   {
      s.emplace_hint(s.end(), m);
   }
   KappaOper(s, arCoefs);
}

/***************************************************************************//**
 * Computes and returns the operator exponential of a cluster operator (times a
 * scalar). Does so in the possibly naive and numerically inefficient fashion
 * of computing it as the infinite series
 * \f[
 *    \exp(\alpha T)
 *    = \sum_{k=0}^{\infty} \frac{\alpha^k}{k!} T^k
 *    = \sum_{k=0}^{M}      \frac{\alpha^k}{k!} T^k
 *    ,
 * \f]
 * _however_, utilizing the fact that any cluster operator \f$ T \f$, of a
 * ModeCombiOpRange _not_ containing the _empty_ ModeCombi, is by construction
 * nilpotent with index \f$ M + 1 \f$, i.e. the infinite series contains
 * \f$ \frac{\alpha^M}{M!} T^M \f$ as its last term.
 *
 * Corresponds to the \f$ \exp(\pm T) \f$ commonly employed in VCC theory.
 *
 * The client is assumed to only call this function after having constructed
 * such a cluster operator using one of the ClusterOper() methods. It is
 * _checked_ that the diagonal elements (which correspond to the _empty_
 * ModeCombi) are zero, and in the end that \f$ T^{M+1} = 0 \f$ as expected -
 * hard error if this isn't so.
 *
 * @parblock
 * @warning
 *    **Inefficient**, due to full matrix-matrix multiplications. Prefer
 *    TrfExpClusterOper() in stead!
 * @endparblock
 *
 * @parblock
 * @note
 *    If you want to take the exponential of a cluster operator _with_ an
 *    amplitude \f$ t \f$ for the _empty_ ModeCombi, 
 *    \f$ T_\text{full} = t + T \f$,
 *    you can compute it like
 *    \f$ \exp(\alpha T_\text{full}) = \exp(\alpha t) \exp(\alpha T) \f$
 *    the former factor being just a scalar exponential, the latter being
 *    computed by this method.
 *    This method could rather easily be implemented to take care of the above
 *    formulation, but for now I've deemed it unnecessary, given the
 *    non-typical usage. -MBH, Feb 2019.
 * @endparblock
 *
 * @parblock
 * @note
 *    So far there's no implementation for \f$ \exp(\alpha \kappa) \f$. You can
 *    _not_ use this method for computing that exponential, since \f$ \kappa \f$ 
 *    is _not_ nilpotent. I suggest making an implementation that utilizes
 *    the anti-Hermiticity of \f$ \kappa \f$, find its unitary diagonalization, 
 *    \f$ \mathbf{K} = \mathbf{U} \mathbf{D} \mathbf{U}^\dagger \f$, then
 *    computing it like
 *    \f$
 *       \exp(\alpha\mathbf{K})
 *       =  \mathbf{U} \exp(\alpha \mathbf{D}) \mathbf{U}^\dagger
 *    \f$. -MBH, Feb 2019.
 * @endparblock
 *
 * @param[in] aCoef
 *    The \f$ \alpha \f$ coefficient in description above. Set this to
 *    \f$ \pm 1 \f$ for the usual \f$ \exp(\pm T) \f$ used in VCC theory.
 *    (The object is assumed to already contain the matrix representation of
 *    the desired \f$ T \f$ operator.)
 ******************************************************************************/
template
   <  typename T
   >
typename MatRepVibOper<T>::mat_t MatRepVibOper<T>::ExpClusterOper
   (  const value_t aCoef
   )  const
{
   // It's non-standard to have something on the diagonal in a VCC cluster
   // operator; corresponds to an amplitude for the empty ModeCombi. Therefore
   // not implemented for that case (it makes T not nil-potent).
   if (mMat.NormDiagonal() != 0)
   {
      std::stringstream ss;
      ss << "Non-zero diagonal for T-operator. Do you have a non-zero amp. for the empty MC?\n"
         << "   |diag| = " << mMat.NormDiagonal() << '\n'
         << "   diag   = " << mMat.GetDiag() << '\n'
         ;
      MIDASERROR(ss.str());
   }

   // Set exp(T) and T_k to identity for starters.
   mat_t Tk(FullDim(), GeneralMidasVector<T>(FullDim(), value_t(1)));
   mat_t expT(Tk);
   for(Uin k = 1; k <= NModes(); ++k)
   {
      Tk = (aCoef/value_t(k)) * Tk * mMat;
      expT += Tk;
   }

   // Check that mMat is actually nil-potent of order NModes().
   Tk = Tk * mMat;
   if (Tk.Norm() != 0)
   {
      MIDASERROR("T^(M+1) not zero as expected. Norm = "+std::to_string(Tk.Norm()));
   }

   return expT;
}

/***************************************************************************//**
 * @param[in] aAbsIndex
 *    An absolute index in the range `[0; FullDim())`.
 * @return
 *    The corresponding multiindex based on the dimensions of this object.
 ******************************************************************************/
template
   <  typename T
   >
std::vector<Uin> MatRepVibOper<T>::MultiIndex
   (  Uin aAbsIndex
   )  const
{
   return matrep::MultiIndex(aAbsIndex, mDims);
}

/***************************************************************************//**
 * @param[in] arMultiIndex
 *    A multiindex `{i_0, ..., i_{M-1}}`, `M` being the number of modes of the
 *    object, and each `i_k` being in the range `[0;Dim(k))`.
 * @return
 *    The corresponding absolute index in the range `[0;FullDim())`.
 ******************************************************************************/
template
   <  typename T
   >
Uin MatRepVibOper<T>::AbsIndex
   (  const std::vector<Uin>& arMultiIndex
   )  const
{
   return matrep::AbsIndex(arMultiIndex, mDims);
}

/***************************************************************************//**
 * @param[in] aDim
 *    Requested dimension of identity matrix.
 * @return
 *    The identity matrix of size `aDim x aDim`.
 ******************************************************************************/
template
   <  typename T
   >
typename MatRepVibOper<T>::mat_t MatRepVibOper<T>::Identity
   (  const Uin aDim
   )
{
   mat_t identity(aDim);
   identity.Unit();
   return identity;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arModes     = {2,3}
 *    arSubIndex  = {2,0}
 *    arFullIndex = {4,1,2,0,5}
 *    return      = true
 * ~~~
 *
 * @param[in] arModes
 *    Modes for which index matching is checked.
 * @param[in] arSubIndex
 *    One index for each of the modes in arModes.
 * @param[in] arFullIndex
 *    Full index vector with at least as many modes as in arModes.
 * @return
 *    Are the indices of arSubIndex equal to those of arFullIndex for the given
 *    modes?
 ******************************************************************************/
template
   <  typename T
   >
bool MatRepVibOper<T>::PartialIndexMatch
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arSubIndex
   ,  const std::vector<Uin>& arFullIndex
   )
{
   if (!(arModes.size() == arSubIndex.size())) MIDASERROR("Modes/subindex size mismatch.");
   if (!arModes.empty())
   {
      if (!(*arModes.rbegin() < arFullIndex.size())) MIDASERROR("Modes out of range.");
   }
   Uin i_mode = 0;
   for(auto it = arModes.begin(), end = arModes.end(); it != end; ++it, ++i_mode)
   {
      if (arSubIndex.at(i_mode) != arFullIndex.at(*it))
      {
         return false;
      }
   }
   return true;
}

/***************************************************************************//**
 * Extracts and returns a vector with the values whose multiindex correspond to
 * excitation in the given modes (mode-index different from 0) and no
 * excitation in the remaining modes (mode-index equal to 0).
 *
 * Example:
 * ~~~
 *    dims    =   {2,2,3}
 *    arVFull =   { e_000, e_001, e_002
 *                , e_010, e_011, e_012
 *                , e_100, e_101, e_102
 *                , e_110, e_111, e_112
 *                }
 *    arModes =   {0,2}
 *    return  =   {e_101, e_102}
 * ~~~
 *
 * @param[in] arVFull
 *    A vector with parameters in the full space.
 * @param[in] arModes
 *    The mode combination for whose excitation parameters are sought.
 * @param[in] arDims
 *    All dimensions of direct product space.
 * @return
 *    Excitation parameters for that mode combination.
 ******************************************************************************/
template
   <  typename T
   >
std::vector<typename MatRepVibOper<T>::value_t> MatRepVibOper<T>::ExtractExciParams
   (  const vec_t& arVFull
   ,  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arDims
   )
{
   const auto full_dim = Product(arDims);
   const auto n_modes = arDims.size();
   if (arVFull.Size() != full_dim) MIDASERROR("arVFull size mismatch.");
   if (arModes.size() > n_modes) MIDASERROR("Too many modes.");
   if (!arModes.empty())
   {
      if (*arModes.rbegin() >= n_modes) MIDASERROR("Mode too large.");
   }

   const auto& v_exci_indices = ModeCombiExciIndices(arModes, arDims);
   const std::vector<Uin> index_others(n_modes - arModes.size(), 0);
   std::vector<value_t> v;
   v.reserve(v_exci_indices.size());
   for(const auto& index_exci: v_exci_indices)
   {
      const Uin abs_i = matrep::AbsIndex(CombineIndices(arModes, index_exci, index_others),arDims);
      if (abs_i >= arVFull.Size()) MIDASERROR("Abs. index out of range.");
      v.emplace_back(arVFull[abs_i]);
   }

   return v;
}

/***************************************************************************//**
 * See the static ExtractExciParams().
 *
 * @param[in] arVFull
 *    A vector with parameters in the full space.
 * @param[in] arModes
 *    The mode combination for whose excitation parameters are sought.
 * @return
 *    Excitation parameters for that mode combination.
 ******************************************************************************/
template
   <  typename T
   >
std::vector<typename MatRepVibOper<T>::value_t> MatRepVibOper<T>::ExtractExciParams
   (  const vec_t& arVFull
   ,  const std::set<Uin>& arModes
   )  const
{
   return ExtractExciParams(arVFull, arModes, this->Dims());
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    dims    =   {2,2,3}
 *    arVFull =   { e_000, e_001, e_002
 *                , e_010, e_011, e_012
 *                , e_100, e_101, e_102
 *                , e_110, e_111, e_112
 *                }
 *    arMcr   =   {}, {0}, {1}, {0,1}, {1,2}
 *    return  =   {  e_000    // For MC {}
 *                ,  e_100    // For MC {0}
 *                ,  e_010    // For MC {1}
 *                ,  e_110    // For MC {0,1}
 *                ,  e_011    // For MC {1,2}
 *                ,  e_012    // For MC {1,2} (too)
 *                }
 * ~~~
 *
 * @param[in] arMcr
 *    Will determine which parameters enter in the result.
 * @param[in] arVFull
 *    The parameters of a vector in the full space.
 * @param[in] arDims
 *    All dimensions of direct product space.
 * @return
 *    The parameters reorganized to fit with the ModeCombiOpRange and
 *    dimensions.
 ******************************************************************************/
template
   <  typename T
   >
typename MatRepVibOper<T>::vec_t MatRepVibOper<T>::ExtractToMcrSpace
   (  const vec_t& arVFull
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arDims
   )
{
   vec_t v;
   for(const auto& mc: arMcr)
   {
      v.Append(vec_t(ExtractExciParams(arVFull, SetFromVec(mc.MCVec()), arDims)));
   }
   return v;
}

/***************************************************************************//**
 * @param[in] arMcr
 *    Will determine which parameters enter in the result.
 * @param[in] arVFull
 *    The parameters of a vector in the full space.
 * @return
 *    The parameters reorganized to fit with the ModeCombiOpRange and
 *    dimensions.
 ******************************************************************************/
template
   <  typename T
   >
typename MatRepVibOper<T>::vec_t MatRepVibOper<T>::ExtractToMcrSpace
   (  const vec_t& arVFull
   ,  const ModeCombiOpRange& arMcr
   )  const
{
   return ExtractToMcrSpace(arVFull, arMcr, this->Dims());
}

/***************************************************************************//**
 * Example:
 * ~~~
 *    arModesOfFirst = {0,    3}
 *    arFirst        = {2,    1}
 *    arSecond       = {  5,4,  3}
 *    return         = {2,5,4,1,3}
 * ~~~
 *
 * @param[in] arModesOfFirst
 *    What shall be the indices of arFirst elements in the result?
 *    Largest element must be less than the sum of sizes of arFirst and
 *    arSecond.
 * @param[in] arFirst
 *    One vector of elements to be merged. Must have same size as
 *    arModesOfFirst.
 * @param[in] arSecond
 *    The other set of elements to be merged.
 * @return
 *    The combined multiindex.
 ******************************************************************************/
template
   <  typename T
   >
std::vector<Uin> MatRepVibOper<T>::CombineIndices
   (  const std::set<Uin>& arModesOfFirst
   ,  const std::vector<Uin>& arFirst
   ,  const std::vector<Uin>& arSecond
   )
{
   const Uin size = arFirst.size() + arSecond.size();
   if (!(arModesOfFirst.size() == arFirst.size())) MIDASERROR("Size mismatch, modes/first.");
   if (!arModesOfFirst.empty())
   {
      if (!(*arModesOfFirst.rbegin() < size)) MIDASERROR("A mode is out of range.");
   }

   std::vector<Uin> v;
   v.reserve(size);
   for(Uin m = 0, i1 = 0, i2 = 0; m < size; ++m)
   {
      if (arModesOfFirst.find(m) != arModesOfFirst.end())
      {
         v.emplace_back(arFirst.at(i1++));
      }
      else
      {
         v.emplace_back(arSecond.at(i2++));
      }
   }
   return v;
}

/***************************************************************************//**
 * Example: 3 modes, dim = 2 for each.
 * The shift operator \f$ E^{1}_{1,0} \f$ moves occupation from 0 to 1 in mode
 * 1, and occupations of the other modes are left untouched, i.e. it affects
 * ~~~
 *    a) (0,0,0) -> (0,1,0)
 *    b) (0,0,1) -> (0,1,1)
 *    c) (1,0,0) -> (1,1,0)
 *    d) (1,0,1) -> (1,1,1)
 * ~~~
 * Thus this method accesses and adds aVal to the elements marked with a `*` in
 * this matrix representation; the other elements are left untouched:
 * ~~~
 *    0 0 0 0 0 0 0 0
 *    0 0 0 0 0 0 0 0
 *    * 0 0 0 0 0 0 0  <-- a
 *    0 * 0 0 0 0 0 0  <-- b
 *    0 0 0 0 0 0 0 0
 *    0 0 0 0 0 0 0 0
 *    0 0 0 0 * 0 0 0  <-- c
 *    0 0 0 0 0 * 0 0  <-- d
 * ~~~
 *
 * @note
 *    The shift operator for the empty set of modes affects the entire
 *    diagonal.
 * 
 * @param[in] arModes
 *    The mode(s) operated on.
 * @param[in] arCreate
 *    Shift _to_ these occupation indices for the given modes. (\f$r,...\f$)
 * @param[in] arAnnihilate
 *    Shift _from_ these occupation indices for the given modes. (\f$s,...\f$)
 * @param[in] aVal
 *    Add this value to the affected matrix elements.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::AddToShiftOperElems
   (  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arCreate
   ,  const std::vector<Uin>& arAnnihilate
   ,  const value_t aVal
   )
{
   // Assertions.
   if (!(arModes.size() == arCreate.size() && arModes.size() == arAnnihilate.size()))
   {
      MIDASERROR("Size mismatch, num. modes and annihilation/creation indices.");
   }
   for(auto it_m = arModes.begin(), end = arModes.end(); it_m != end; ++it_m)
   {
      const auto i_mode = std::distance(arModes.begin(), it_m);
      if (!(arCreate.at(i_mode) < Dim(*it_m))) MIDASERROR("Creation index out of range.");
      if (!(arAnnihilate.at(i_mode) < Dim(*it_m))) MIDASERROR("Annihilation index out of range.");
   }

   // Loop over "free" indices (those not specified by arModes).
   // For given free index set up corresponding annihilation/creation indices.
   const auto& free_modes = ComplementaryModes(NModes(), arModes);
   const auto& free_dims = SubsetDims(free_modes, mDims);
   for(Uin j_free = 0; j_free < Product(free_dims); ++j_free)
   {
      const auto& free_indices = matrep::MultiIndex(j_free, free_dims);
      const auto& index_anni = CombineIndices(arModes, arAnnihilate, free_indices);
      const auto& index_crea = CombineIndices(arModes, arCreate, free_indices);
      mMat[AbsIndex(index_crea)][AbsIndex(index_anni)] += aVal;
   }
}

/***************************************************************************//**
 * Checks that
 *    - there is at least 1 mode
 *    - the full dimension is non-zero
 * If this is not the case, some methods may fail, so we'll cause a hard error
 * already at this point.
 ******************************************************************************/
template
   <  typename T
   >
void MatRepVibOper<T>::SanityCheck
   (
   )  const
{
   if (this->NModes() == 0) MIDASERROR("0 modes is invalid.");
   if (this->FullDim() == 0) MIDASERROR("Full dimension = 0 is invalid.");
}

} /* namespace midas::util::matrep */

#endif/*MATREPVIBOPER_IMPL_H_INCLUDED*/
