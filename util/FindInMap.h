#ifndef FINDELSEERROR_H_INCLUDED
#define FINDELSEERROR_H_INCLUDED

// std headers
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <tuple> // for pair
#include <set>
#include <type_traits>

// midas headers
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/GetLine.h"
#include "libmda/util/any_type.h"

namespace midas
{
namespace util
{

namespace detail
{

///> helper class for determining key_type of standard containers.
template<class T>
struct deduce_key_type
{
   using type        = typename T::value_type;
   using mapped_type = typename T::value_type;
};

///> deduce_key_type for std::map
template< class Key
        , class T
        , class Compare
        , class Alloc
        >
struct deduce_key_type<std::map<Key, T, Compare, Alloc> >
{
   using type        = typename std::map<Key, T, Compare, Alloc>::key_type;
   using mapped_type = typename std::map<Key, T, Compare, Alloc>::mapped_type;
};

} /* namespace detail */

/**
 * Find element in container or throw an error.
 *
 * @param key               The key to search for.
 * @param container         The container to search.
 * @param str               Error message.
 * @return                  Reference to the found object.
 **/
template< class C
        , class decayed_type = typename std::decay<C>::type
        , class T = typename C::value_type
        >
const T& FindElseError
   (  const typename detail::deduce_key_type<decayed_type>::type& key 
   ,  const C& container 
   ,  const std::string& str
   )
{
   auto it_type = container.find(key);
   if(it_type == container.end())
   {
      MIDASERROR(str);
   }
   return *it_type;
}

} /* namespace util */
} /* namespace midas */

#endif /* FINDELSEERROR_H_INCLUDED */
