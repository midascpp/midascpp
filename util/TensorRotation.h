/**
************************************************************************
* 
* @file                TensorRotation.h
*
* Created:             12-07-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TensorRotation datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TENSORROTATION_H
#define TENSORROTATION_H

// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"

// using declarations
using std::string;
using std::vector;

class TensorRotation
{
   private:
      MidasMatrix mRot;

   public:
      TensorRotation(const MidasMatrix&);
      void RotateMidasVector(MidasVector&);
      void RotateMidasMatrix(MidasMatrix&);
      //void RotateCPPVector(vector<Nb>&);
      void RotateCOneDArray(Nb*);
      void RotateCTwoDArray(Nb**);
      void RotateCThreeDArray(Nb***);
      void RotateCFourDArray(Nb****);
};

#endif /* TENSORROTATION_H */
