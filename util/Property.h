/**
************************************************************************
* 
* @file                Property.h
*
* Created:             04-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for containing a set of molecule properties.
* 
* Last modified: man mar 21, 2005  11:40
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PROPER_H
#define PROPER_H

// std headers
#include<iostream>
#include<string>
#include<vector>

// midas headers
#include"inc_gen/TypeDefs.h"

// using declarations
using std::ostream;
using std::string;
using std::vector;

/**
 *
 **/
class DaltonLabel
{
   public:
      DaltonLabel();                    ///< Create uninitialized
      DaltonLabel(char* rLabel) 
      {
         mLabel = rLabel;
      }
   private:
      char* mLabel;
};

/**
* Class Property: 
* Create input files and run dalton.
* */
class Property
{
   public:
      Property();                                          ///< Create uninitialized.
      Property(In,In,string,Nb,
            string,string,string,string,Nb,Nb,Nb,In,In,In);///< Create from data
      string PropertyModel();                              ///< Return Property Model
      bool LogicallyTheSameAs(Property& arProp) const;
      friend ostream& operator<<(ostream&, const Property&);   ///< Output overload
      void OutPut(ostream&,bool aWithValue) const;              ///< Output general
      void OutPut(string&,bool aWithValue) const;               ///< Output general
      void SetValue(Nb aNb) {mProp = aNb;}                      //< Set value 
      Nb Value() const {return mProp;}                           ///< Return Value
      In Nord() const {return mNord;}
      In ExSym()  const {return mExSym;}
      In ExSp()  const {return mExSp;}
      In ExNr() const {return mExNr;}
      bool IsThisExcita() const;
      bool IsThisOnePhotAbsStr() const;
      bool IsThisTwoPhotAbsStr() const;
      void GetIntForLabs(In& aIx,In& aIy,In& aIz, In& aIu) const;

   private:
      bool mInitialized;                                ///< Has the properties been filled in?
      In mNord;                                         ///< Order for property
      In mPropSym;                                      ///< Symmetry of property
      Nb   mProp;                                       ///< The value for the property
      Nb   mFrqY;                                       ///< Frequency 
      Nb   mFrqZ;                                       ///< Frequency 
      Nb   mFrqU;                                       ///< Frequency 
      string mLabMod;                                   ///< Label for model etc.
      string mLabX;                                     ///< Operator Label
      string mLabY;                                     ///< Operator Label
      string mLabZ;                                     ///< Operator Label
      string mLabU;                                     ///< Operator Label
      In mExSym;                                        ///< Symmetry of excited state
      In mExSp;                                         ///< Spin of excited state 
      In mExNr;                                         ///< Number of excited state in symmetry
};

bool OutPutAndCheckPropertyVector(vector<Property>& arPropVec,In aIoLevel,string& aSlabel);

#endif /* PROPER_H */
