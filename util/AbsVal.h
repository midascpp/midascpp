#ifndef ABSVAL_H_INCLUDED
#define ABSVAL_H_INCLUDED

#include <cstdlib>  // for std::abs
#include <cmath>    // for std::fabs
#include <complex>  // for std::complex

namespace midas
{
namespace util
{

/*******************************************************************
 *
 * Integer overloads
 * 
 * @note
 *    std::abs is not constexpr, thus AbsVal cannot be for integers
 *
 *******************************************************************/
inline int           AbsVal(int           i) { return std::abs(i); }
inline long int      AbsVal(long int      i) { return std::abs(i); }
inline long long int AbsVal(long long int i) { return std::abs(i); }

/*******************************************************************
 *
 * Floating point overloads
 * 
 * @note
 *    std::abs is not constexpr, thus AbsVal cannot be for floating-point numbers
 *
 *******************************************************************/
inline float       AbsVal(float       d) { return std::fabs(d); }
inline double      AbsVal(double      d) { return std::fabs(d); }
inline long double AbsVal(long double d) { return std::fabs(d); }
inline constexpr float       AbsVal2(float       d) { return d*d; }
inline constexpr double      AbsVal2(double      d) { return d*d; }
inline constexpr long double AbsVal2(long double d) { return d*d; }

/*******************************************************************
 *
 * Complex overloads
 * 
 * @note
 *    These can be declared constexpr
 *
 *******************************************************************/
template<class T>
inline constexpr T AbsVal2(const std::complex<T>& c) { return c.real()*c.real() + c.imag()*c.imag(); }
template<class T>
inline constexpr T AbsVal(const std::complex<T>& c) { return std::sqrt(AbsVal2(c)); }

/*******************************************************************
 *
 * Return type of AbsVal function for type T
 *
 *******************************************************************/
template<class T>
using AbsVal_t = decltype(AbsVal(T()));

} /* namespace util */
} /* namespace midas */

#endif /* ABSVAL_H_INCLUDED */
