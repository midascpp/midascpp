#pragma once
#ifndef MIDAS_UTIL_PATH_H_INCLUDED
#define MIDAS_UTIL_PATH_H_INCLUDED

#include <string>

namespace midas
{
namespace path
{
namespace detail
{

}

//! Check if path ends with directory delimeter
bool HasDirEnd(const std::string& aPath);
//! Check if path starts with directory delimeter
bool HasDirBegin(const std::string& aPath);
//! Remove dir end if present
std::string RemoveDirEnd(const std::string& aPath);

//! Return file name 
std::string FileName(const std::string& aPath);
//! Return base name
std::string BaseName(const std::string& aPath);
//! Return directory name
std::string DirName(const std::string& aPath);

//! Check if path is absolute.
bool IsAbsPath(const std::string& aPath);
//! Check if path is relative.
bool IsRelPath(const std::string& aPath);

//! Join two paths
std::string Join(const std::string& aPath1, const std::string& aPath2);


} /* namespace path */
} /* namespace midas */

#endif /* MIDAS_UTIL_PATH_H_INCLUDED */
