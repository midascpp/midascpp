/**
 *******************************************************************************
 * 
 * @file    NumericLimits.h
 * @date    06-08-2018
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Roots of numeric limits.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef NUMERICLIMITS_H_INCLUDED
#define NUMERICLIMITS_H_INCLUDED

#include <limits>

namespace midas::util
{

/**
 * Class for holding info on roots of numeric limits calculated at compile time.
 **/
template
   <  class T
   ,  typename std::enable_if_t<std::is_floating_point_v<T>>* = nullptr
   >
class NumericPrecisionInfo
{
   public:
      //! Epsilon
      static constexpr T Epsilon() noexcept
      {
         return std::numeric_limits<T>::epsilon();
      }

      //! Square root epsilon
      static constexpr T EpsilonSquareRoot()
      {
         return std::sqrt(midas::util::NumericPrecisionInfo<T>::Epsilon());
      }

      //! Fourth root epsilon
      static constexpr T EpsilonFourthRoot()
      {
         return std::pow(midas::util::NumericPrecisionInfo<T>::Epsilon(), 1./4.);
      }
};

} /* namespace midas::util */

#endif /* NUMERICLIMITS_H_INCLUDED */
