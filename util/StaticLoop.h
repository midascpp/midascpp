/**
************************************************************************
* 
* @file    StaticLoop.h
*
* @date    17-12-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Static for loop for iterating over e.g. std::pair and std::tuple
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef STATICLOOP_H_INCLUDED
#define STATICLOOP_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

namespace midas::util
{

namespace detail
{
//! Iteration index class allowing for passing a constexpr In to F in StaticFor
template
   <  In I
   >
struct IterationIndex
{
   constexpr operator In
      (
      )  const
   {
      return I;
   }
};
} /* namespace detail */

//! Static for loop
template
   <  In I
   ,  In N
   ,  In INCR = I_1
   >
struct StaticFor
{
   static_assert(I < N, "I must be smaller than N");
   static_assert((N - I) % INCR == 0, "I does not hit N for this choice of INCR");

   //! Call functor and go to next I
   template
      <  typename F
      ,  typename...Ts
      >
   static void Loop
      (  F&& f
      ,  Ts&&... ts
      )
   {
      // Call function in this iteration. NB: ts are not forwarded since f cannot take ownership
      f(detail::IterationIndex<I>(), ts...);

      // Call next iteration
      StaticFor<I + INCR, N, INCR>::Loop(std::forward<F>(f), std::forward<Ts>(ts)...);
   }
};

//! End clause
template
   <  In N
   ,  In INCR
   >
struct StaticFor<N, N, INCR>
{
   //! End loop by doing nothing
   template
      <  typename F
      ,  typename...Ts
      >
   static void Loop
      (  F&& f
      ,  Ts&&... ts
      )
   {
   }
};

} /* namespace midas::util */


#endif /* STATICLOOP_H_INCLUDED */
