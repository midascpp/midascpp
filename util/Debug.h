#ifndef MIDAS_DEBUG_H_INCLUDED
#define MIDAS_DEBUG_H_INCLUDED

#include <string>
#include "input/Input.h"

template<class F, class... Ts>
void MidasDebug(F&& f, Ts&&... ts)
{
   if(gDebug)
   {
      f(std::forward<Ts>(ts)...);
   }
}



class PrintIf
{
   private:
      bool mIf;
      std::string mMsgTrue;
      std::string mMsgFalse;
   public:
      PrintIf(bool aIf, const std::string& aMsgTrue, const std::string& aMsgFalse)
         : mIf(aIf)
         , mMsgTrue(aMsgTrue)
         , mMsgFalse(aMsgFalse)
      {
      }
      
      PrintIf(bool aIf, std::string&& aMsgTrue, std::string&& aMsgFalse)
         : mIf(aIf)
         , mMsgTrue(std::move(aMsgTrue))
         , mMsgFalse(std::move(aMsgFalse))
      {
      }

      void operator()() const
      {
         if(mIf)
         {
            Mout << mMsgTrue << std::endl;
         }
         else
         {
            Mout << mMsgFalse << std::endl;
         }
      }
};

#endif /* MIDAS_DEBUG_H_INCLUDED */
