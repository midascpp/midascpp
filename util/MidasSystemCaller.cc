/**
************************************************************************
* 
* @file                MidasSystemCaller.cc
*
* Created:             07-02-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of sytem caller.
* 
* Last modified: Tue. Feb 07 2012
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "util/MidasSystemCaller.h"

// std headers
#include <sys/wait.h>
#include <mutex>
#include <sstream>
#include <string>

// midas headers
#include "util/MidasStream.h"
#include "util/Io.h"
#include "util/Os.h"
#include "util/paral/run_process.h"
#include "util/paral/current_working_dir.h"
#include "util/paral/threaded_system_call.h"
#include "util/stream/HeaderInserter.h"

extern MidasStream Mout; 

std::string LogFile="midasifc.system_log." + midas::os::Gethostname(); 
ofstream systemlogfile;
MidasStreamBuf Mlog_buf(systemlogfile, false);
MidasStream Mlog_stream(Mlog_buf);
std::mutex mlog_mutex;

mutex_ostream Mlog = std::make_pair(std::ref(Mlog_stream), std::ref(mlog_mutex));

unsigned MidasSystemCaller::mN = 0;

/**
 * Writes header to system log file.
 **/
void MidasSystemCaller::Init
   (
   )
{ 
   systemlogfile.open(LogFile.c_str(), ios_base::app);
   std::lock_guard<std::mutex> guard(mlog_mutex);
   Out72Char(Mlog.first,'$','$','$');
   Out72Char(Mlog.first,'$',' ','$');
   OneArgOut72(Mlog.first," MIDAS CPP SYSTEM LOG FILE ",'$');
   Out72Char(Mlog.first,'$',' ','$');
   Out72Char(Mlog.first,'$','$','$');
   OutputDate(Mlog.first,"\n The date is: ");
   Mlog.first  << " --- Hint --- \n"
               << " use regex: \"/STATUS] \\(0\\)\\@!\" to search for return codes that are not 0.\n"
               << " use regex: \"/STATUS] \\(0\\|1\\)\\@!\" to search for return codes that are not 0 or 1, etc.\n"
               << " ------------ \n\n" 
               << std::flush;
}

/**
 *
 **/
void MidasSystemCaller::Log
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  const std::stringstream& std_out
   ,  const std::stringstream& std_err
   ,  int status
   ,  const char* file
   ,  const unsigned line
   ,  const std::string& date
   )
{
   std::lock_guard<std::mutex> guard(mlog_mutex);
   std::string n_str = " ["+std::to_string(++MidasSystemCaller::mN)+"] ~ ";
   
   // output file and line and current working dir
   Mlog.first << " ---+++ SYSTEM CALL +++--- ---+++ SYSTEM CALL +++--- \n" 
        << "[" << n_str << "DATE   ] " << date << "\n"
        << "[" << n_str << "FILE   ] " << file << " " << line << "\n"
        << "[" << n_str << "DIR    ] " << dir << "\n";
   
   // output command
   Mlog.first << "[" << n_str << "COMMAND] ";
   for(size_t i=0; i<cmd.size(); ++i)
      Mlog.first << cmd[i] << " ";
   Mlog.first << "\n";
   
   // output std out
   midas::stream::HeaderInserter file_out_buf(Mlog.first.rdbuf(),"["+n_str+"STDOUT ] ",true);
   std::ostream file_out(&file_out_buf);
   file_out << std_out.str() << "\n";
   
   // output std err
   midas::stream::HeaderInserter file_err_buf(Mlog.first.rdbuf(),"["+n_str+"STDERR ] ",true);
   std::ostream file_err(&file_err_buf);
   file_err << std_err.str() << "\n";

   // output status
   Mlog.first << "[" << n_str << "STATUS ] " << WEXITSTATUS(status) << "\n"
        << " ---+++ +++++++++++ +++--- ---+++ +++++++++++ +++--- \n\n"
        << std::flush;
}

/**
 * Make a system call and log the output.
 *
 * @param aCmd  The system command.
 * @param file  The file where the system call is created.
 * @param line  The line where the system call is created.
 **/
int MidasSystemCaller::SystemWithLog
   (  const system_command_t& aCmd
   ,  const char* file
   ,  const unsigned line
   )
{
   std::stringstream std_out;
   std::stringstream std_err;

   auto date = date_now();
   In status = run_process(aCmd, std_out, std_err);
   
   MidasSystemCaller::Log(aCmd, current_working_dir(), std_out,std_err, status,file, line, date);

   return status;
}

/**
 *
 **/
int MidasSystemCaller::SystemDirWithLog
   (  const system_command_t& aS
   ,  const std::string& dir
   ,  const char* file
   ,  const unsigned line
   )
{
   std::stringstream std_out;
   std::stringstream std_err;

   auto date = date_now();
   In status=run_process_dir(aS,dir,std_out,std_err);
   
   MidasSystemCaller::Log(aS,dir,std_out,std_err,status,file,line,date);

   return status;
}

/**
 *
 **/
int MidasSystemCaller::SystemToStreamWithLog
   (  const system_command_t& aS
   ,  std::ostream& os
   ,  const char* file
   ,  const unsigned line
   )
{
   std::stringstream std_out;
   std::stringstream std_err;

   auto date = date_now();
   In status=run_process(aS,std_out,std_err);
   
   MidasSystemCaller::Log(aS,current_working_dir(),std_out,std_err,status,file,line,date);
   
   os << std_out.str();

   return status;
}

/**
 *
 **/
int MidasSystemCaller::SystemDirToStreamWithLog
   (  const system_command_t& aS
   ,  const std::string& dir
   ,  std::ostream& os
   ,  const char* file
   ,  const unsigned line
   )
{
   std::stringstream std_out;
   std::stringstream std_err;

   auto date = date_now();
   In status=run_process_dir(aS,dir.c_str(),std_out,std_err);
   
   MidasSystemCaller::Log(aS,dir,std_out,std_err,status,file,line,date);
   
   os << std_out.str();

   return status;
}

/**
 *
 **/
int MidasSystemCaller::SystemNoLog
   (  const system_command_t& aS
   )
{
   std::stringstream std_out;
   std::stringstream std_err;
   
   In status = run_process(aS, std_out, std_err);

   return status;
}

/**
 *
 **/
int MidasSystemCaller::SystemToStreamNoLog
   (  const system_command_t& aS
   ,  std::ostream& os
   )
{
   std::stringstream std_out;
   std::stringstream std_err;

   In status=run_process(aS,std_out,std_err);
   
   os << std_out.str();

   return status;
}
