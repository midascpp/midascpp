#include "run_async.h"
   
auto run_async(const system_command_t& cmd, mutex_ostream& os)
   -> decltype(std::async(std::launch::async,threaded_system_call,cmd,std::ref(os)))
{
   return std::async(std::launch::async,threaded_system_call,cmd,std::ref(os));
}
