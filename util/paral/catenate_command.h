#ifndef CATENATE_COMMAND_H_INCLUDED
#define CATENATE_COMMAND_H_INCLUDED

#include <string>

#include "system_command_t.h"

namespace detail
{
 
template<class... Ts>
inline auto catenate_command_impl(system_command_t& cmd, const system_command_t& cmd2, Ts&&... ts);

/**
 * Stop catenation recursion.
 *
 * @param cmd    The catenated command.
 *
 * @return       Return whole command as system_command_t.
 **/
inline system_command_t catenate_command_impl(system_command_t& cmd)
{
   return cmd;
}

/**
 * Catenate with single string. All other arguments are catenated using recursion.
 *
 * @param cmd   The catenated command.
 * @param str   The string that will be added to command.
 * @param ts    The rest of the arguments to be catenated by recursion.
 *
 * @return      Return the completely catenated command as system_command_t.
 **/
template<class... Ts>
inline auto catenate_command_impl(system_command_t& cmd, const std::string& str, Ts&&... ts)
{
   cmd.emplace_back(str);
   return catenate_command_impl(cmd, std::forward<Ts>(ts)...);
}

/**
 * Catenate with other system_command_t. All other arguments are catenated using recursion.
 *
 * @param cmd   The first command.
 * @param cmd2  The second command.
 * @param ts    The rest of the arguments to be catenated by recursion.
 *
 * @return      Return the completely catenated command as system_command_t.
 **/
template<class... Ts>
inline auto catenate_command_impl(system_command_t& cmd, const system_command_t& cmd2, Ts&&... ts)
{
   for(size_t i=0; i<cmd2.size(); ++i)
   {
      cmd.emplace_back(cmd2[i]);
   }
   return catenate_command_impl(cmd, std::forward<Ts>(ts)...);
}

} /* namespace detail */

/**
 * Catenate system commands.
 *
 * @param ts   A set of arguments to be catenated.
 *
 * @return      Return the completely catenated command as system_command_type.
 **/
template<class... Ts>
auto catenate_command(Ts&&... ts)
{
   system_command_t cmd;
   return detail::catenate_command_impl(cmd, std::forward<Ts>(ts)...);
}

#endif /* CATENATE_COMMAND_H_INCLUDED */
