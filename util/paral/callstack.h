#ifndef CALLSTACK_HPP_INCLUDED
#define CALLSTACK_HPP_INCLUDED

// Disclaimer:
// Original source is Ruy Figueira callstack implementation from here : http://www.crazygaze.com/blog/2016/03/11/callstack-markers-boostasiodetailcall_stack/
// This file contains some minor modifications.

#include <string>

template <typename Key, typename Value = unsigned char>
class callstack 
{
   public:
      using value_type = Value;
      using key_type   = Key;

      class iterator;

      class context {
         public:
            context(const context&) = delete;
            context& operator=(const context&) = delete;
            
            explicit context(key_type* k)
               :  m_key(k)
               ,  m_next(callstack<key_type, value_type>::ms_top) 
            { 
               m_val = reinterpret_cast<value_type*>(this);
               callstack<key_type, value_type>::ms_top = this;
            }

            context(key_type* k, value_type& v)
               :  m_key(k)
               ,  m_val(&v)
               ,  m_next(callstack<key_type, value_type>::ms_top) 
            {
               callstack<key_type, value_type>::ms_top = this;
            }

            ~context() 
            {
               callstack<key_type, value_type>::ms_top = m_next;
            }

            key_type* get_key() 
            {
               return m_key;
            }

            value_type* get_value() 
            {
               return m_val;
            }

         private:
            friend class callstack<key_type, value_type>;
            friend class callstack<key_type, value_type>::iterator;
            key_type*   m_key;
            value_type* m_val;
            context*    m_next;
      };

      class iterator 
      {
         public:
            iterator(context* ctx) : m_ctx(ctx) {}
            
            iterator& operator++() 
            {
               if (m_ctx)
               {
                  m_ctx = m_ctx->m_next;
               }
               return *this;
            }

            bool operator!=(const iterator& other) 
            {
               return m_ctx != other.m_ctx;
            }

            context* operator*() 
            {
               return m_ctx;
            }

         private:
            context* m_ctx;
      };

      // Determine if the specified owner is on the stack
      // \return
      //  The address of the value if present, nullptr if not present
      static value_type* contains(const key_type* k) 
      {
         context* elem = ms_top;
         while (elem) 
         {
            if (elem->m_key == k)
            {
               return elem->m_val;
            }
            elem = elem->m_next;
         }
         return nullptr;
      }

      static iterator begin() 
      {
         return iterator(ms_top);
      }

      static iterator end() 
      {
         return iterator(nullptr);
      }

   private:
      static thread_local context* ms_top;
};
 
template <typename Key, typename Value>
thread_local typename callstack<Key, Value>::context*
    callstack<Key, Value>::ms_top = nullptr;

#endif /* CALLSTACK_HPP_INCLUDED */
