#ifndef CURRENT_WORKING_DIR_H_INCLUDED
#define CURRENT_WORKING_DIR_H_INCLUDED

#include <string>

int current_working_dir(std::string& dir);
std::string current_working_dir();

#endif /* CURRENT_WORKING_DIR_H_INCLUDED */
