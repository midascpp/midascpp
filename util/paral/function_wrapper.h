#ifndef MIDAS_UTIL_PARAL_FUNCTION_WRAPPER_H_INCLUDED
#define MIDAS_UTIL_PARAL_FUNCTION_WRAPPER_H_INCLUDED

namespace midas
{
namespace util
{
namespace paral
{

/**
 * function wrapper for waitable function
 **/
class function_wrapper
{
   public:
      struct impl_base
      {
         virtual void call() = 0;
         virtual ~impl_base() {};
      };

      std::unique_ptr<impl_base> impl;

      template<class F>
      struct impl_type : impl_base
      {
         F f;
         impl_type(F&& f_) : f(std::move(f_)) {}
         void call() { f(); }
      };

   public:
      template<class F>
      function_wrapper(F&& f_) : impl(new impl_type<F>(std::move(f_))) {}
      function_wrapper() = default;
      
      function_wrapper(const function_wrapper&) = delete;
      function_wrapper& operator=(const function_wrapper&) = delete;

      function_wrapper(function_wrapper&&) = default;
      function_wrapper& operator=(function_wrapper&&) = default;

      void operator() (){ impl->call(); }
};

} /* namespace paral */
} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_PARAL_FUNCTION_WRAPPER_H_INCLUDED */
