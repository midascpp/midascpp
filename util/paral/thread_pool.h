#ifndef THREAD_POOL_H_INCLUDED
#define THREAD_POOL_H_INCLUDED

#include <thread>
#include <atomic>
#include <vector>
#include <future>

#include "function_wrapper.h"
#include "work_stealing_queue.h"
#include "join_threads.h"
#include "callstack.h"
#include "semaphore.h"

/**
 * Thread pool with thread specific queues, 
 * but enables work stealing between threads.
 **/
class thread_pool
{
   private:
      using task_type  = midas::util::paral::function_wrapper;
      using queue_type = work_stealing_queue<task_type>;
      
      //! Should threads terminate
      std::atomic_bool done;
      //! 
      std::atomic_bool wait_on_task = true;
      //! Index taht shuffles through threads for assignment of work
      std::atomic_uint queue_index;
      //! Max number of running threads we allow.
      std::atomic_int m_active_threads;
      //! Vector of all work queues
      std::vector<std::unique_ptr<queue_type> > queues;
      //! Vector of threads
      std::vector<std::thread> threads;
      //!
      std::vector<std::unique_ptr<semaphore> >  semaphores;
      //! Ensures that all threads are joined (and thus correctly terminated) on pool destruction
      join_threads joiner;
      //! Pointer to thread local queue
      static thread_local queue_type* local_work_queue;
      //!
      static thread_local semaphore* local_semaphore;
      //! Thread local thread-index
      static thread_local int my_index;
      
      //! Worker thread function
      void worker_thread(unsigned my_index_)
      {
         callstack<thread_pool>::context worker_thread_ctx(this);

         my_index = my_index_;
         local_work_queue = queues[my_index].get();
         local_semaphore  = semaphores[my_index].get();

         bool success = false;
      
         while((success = run_pending_task()) || !done)
         {
            if(!success && wait_on_task)
            {
               local_semaphore->wait();
            }
         }
      }
      
      //! Get task from local queue, if available
      bool pop_task_from_local_queue(task_type& task)
      {
         return local_work_queue && local_work_queue->try_pop(task);
      }

      //! Steal task from other queue, if available
      bool pop_task_from_other_thread_queue(task_type& task)
      {
         int active_threads = m_active_threads;
         if(my_index > active_threads)
         {
            for(unsigned i = 0; i < active_threads; ++i)
            {
               unsigned const index = (my_index + i + 1) % active_threads;
               if(queues[index]->try_steal(task))
               {
                  return true;
               }
            }
         }
         return false;
      }

   public:
      //! Constructor. Will start all threads in pool or fail gracefully.
      thread_pool(unsigned num_threads = 0)
         :  done(false)
         ,  queue_index(0)
         ,  joiner(threads)
      {
         const unsigned thread_count = num_threads ? num_threads : std::thread::hardware_concurrency();
         queues.reserve(thread_count);
         semaphores.reserve(thread_count);

         m_active_threads = thread_count;

         my_index = 0;  // master thread also gets index 0 (may also run jobs, but not the standard way of doing things).

         try
         {
            for(unsigned i = 0; i < thread_count; ++i)
            {
               queues.emplace_back(new queue_type);
            }
            for(unsigned i = 0; i < thread_count; ++i)
            {
               semaphores.emplace_back(new semaphore);
            }
            for(unsigned i = 0; i < thread_count; ++i)
            {
               threads.emplace_back(&thread_pool::worker_thread, this, i);
            }
         }
         catch(...)
         {
            done = true;
            throw;
         }
      }
      
      //! Destructor. Will terminate all threads when currently running tasks finish.
      ~thread_pool()
      {
         done = true;
         this->wake_all();
      }
      
      //! Submit task to queue
      template<class F>
      std::future<typename std::result_of<F()>::type> post(F&& f)
      {
         using result_type = typename std::result_of<F()>::type;

         std::packaged_task<result_type()> task(std::move(f));
         std::future<result_type> res(task.get_future());
         
         // move task to a queue
         unsigned index = (queue_index++) % m_active_threads;
         queues[index]->push(std::move(task));
         semaphores[index]->notify();
         
         return res;
      }
      
      //! Run a pending task
      bool run_pending_task()
      {
         task_type task;
         if (  pop_task_from_local_queue(task)
            || pop_task_from_other_thread_queue(task)
            )
         {
            task();
            return true;
         }
         else
         {
            std::this_thread::yield();
         }
         return false;
      }
      
      //!
      bool can_dispatch() const
      {
         return callstack<thread_pool>::contains(this) != nullptr;
      }
      
      //!
      void wake_all(bool wait_on_task_ = true)
      {
         wait_on_task = wait_on_task_;

         for(auto& sem : semaphores)
         {
            sem->notify();
         }
      }

      //! get number of threads
      int get_nthreads() const
      {
         return queues.size();
      }

      //!
      int get_active() const
      {
         return m_active_threads;
      }
      
      //!
      void set_active(int active_threads = -1)
      {
         if((active_threads >= 0) && active_threads <= queues.size())
         {
            m_active_threads = active_threads;
         }
         else
         {
            m_active_threads = queues.size();
         }
      }
};


#endif /* THREAD_POOL_H_INCLUDED */
