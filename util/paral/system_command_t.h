#ifndef SYSTEM_COMMAND_T_H_INCLUDED
#define SYSTEM_COMMAND_T_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
#include <mutex>
#include <utility> // for std::pair
#include <sstream>

/**
 * type to make system commands
 **/ 
using system_command_t = std::vector<std::string>;

/**
 * Outut operator.
 *
 * @param os   The stream to output to.
 * @param cmd  The command to output.
 *
 * @return     Return reference to output stream.
 **/
inline std::ostream& operator<<(std::ostream& os, const system_command_t& cmd)
{
    for(int i = 0; i < cmd.size(); ++i)
    {
        os << cmd[i] << " ";
    }
    return os;
}

/**
 * Convert system_command_t to string.
 *
 * @param cmd
 *    The command to convert.
 *
 * @return
 *    Returns the command as a string.
 **/
inline std::string command_to_string(const system_command_t& cmd)
{
   std::stringstream sstr;
   sstr << cmd;
   return sstr.str();
}

/**
 * type to bind together ostream + mutex
 **/
using mutex_ostream = std::pair<std::ostream&, std::mutex&>;

#endif /* SYSTEM_COMMAND_T_H_INCLUDED */
