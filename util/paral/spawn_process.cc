#include "spawn_process.h"

#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/un.h> /* for AF_UNIX / AF_LOCAL */
#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <algorithm> // for std::transform
#include <cstring>
#include <thread>

#include "util/stream/HeaderInserter.h"
#include "util/Path.h"
#include "util/Os.h"
#include "util/Error.h"
#include "input/ProgramSettings.h"

#ifdef VAR_MPI
#include <mpi.h>
#include <mutex>
#include <string.h>
#include <chrono>
#include "util/paral/catenate_command.h"
#include "mpi/Impi.h"
#endif /* VAR_MPI */

using namespace midas;

/**
 * class to make sure that child process always terminates
 **/
class exit_guard
{
   private:
      int code = 1;

   public:
      ~exit_guard()
      {
         //abort();
         _Exit(code);
      }
};

/**
 *
 **/
int nargs_in_argv(char* const argv[])
{
   int size = 0;
   while(argv[size])
   {
      ++size;
   }
   return size;
}

/**
 * Down cast const char* to char*.
 **/
char* downcast_char(char const* c)
{
   return const_cast<char*>(c);
}

/**
 * Create a pseudo pointer to character array, from a system command.
 *
 * @param cmd   The system command to transform.
 *
 * @return      Return a pointer to character array to be used for e.g. execvp().
 **/
std::vector<char*> make_command
   (  const system_command_t& cmd
   )
{
   std::vector<char*> cmd_char( cmd.size() + 1 );
   std::transform( cmd.begin(), cmd.end(), cmd_char.begin(),
                         []( std::string const& arg ) { return downcast_char(arg.c_str()); } );
   cmd_char[cmd.size()] = nullptr;
   return cmd_char;
}

/**
 * Read from a file descriptor (fd), and append to a stringstream.
 *
 * @param fd
 *    The file desriptor to read from.
 * @param sstr
 *    The string stream to append.
 *
 * @return
 *    Return status. Returns 0 if no errors, else returns 1.
 **/
int read_from_fd
   (  int fd
   ,  std::stringstream& sstr
   )
{
   char buffer[1024];
   int count;

   while( (count = read(fd, buffer, sizeof(buffer)-1)) )
   {
      if (count >= 0) 
      {
         buffer[count] = 0;
         //printf("%s", buffer);
         sstr << buffer;
      }
      else 
      {
         printf("IO Error\n");
         return 1;
      }
   }
   return 0;
}

/**
 * Wait for a child process to finish.
 *
 * @param pid      
 *    The pid of the child process.
 * @param status   
 *    (OUTOUT) On exit will contain the status of the wait function.
 * @param options  
 *    Optional extra options for waitpid().
 **/
void wait_for_child
   (  const pid_t pid
   ,  int& status
   ,  int options
   )
{
   int waitpid_status = 0;

   // Loop over waitpid to wait for child
   while(waitpid(pid, &waitpid_status, options) < 0)
   {
      if(errno != EINTR)
      {
         status = -1;
         break;
      }
      
      std::this_thread::yield();
   }

   // After process
   if (  WIFEXITED(waitpid_status) )
   {
      status = WEXITSTATUS(waitpid_status);
   }
   else if  (  WIFSIGNALED(waitpid_status) )
   {
      std::cout << " Child stopped abnormally from recieving signal: " << WTERMSIG(waitpid_status) 
#ifdef WCOREDUMP
                << (WCOREDUMP(waitpid_status) ? " (core file generated) " : "")
#endif /* WCOREDUMP */
                << "." << std::endl;
      status = -1;
   }
   else if  (  WIFSTOPPED(waitpid_status) )
   {
      std::cout << " Child stopped from recieving stop signal: " << WSTOPSIG(status) << "." << std::endl;
      status = -1;
   }
   else if  (  WIFCONTINUED(waitpid_status) )
   {
      std::cout << " Child continued after job control stop. " << std::endl;
      status = -1;
   }
   else
   {
      std::cout << " Unknown signal from waitpid. " << std::endl;
      status = -1;
   }
}

/*
 * since pipes are unidirectional, we need two pipes.
 * one for data to flow from parent's stdout to child's
 * stdin and the other for child's stdout to flow to
 * parent's stdin 
 */
#define NUM_PIPES          3

#define PARENT_WRITE_PIPE  0
#define PARENT_READ_PIPE   1
#define PARENT_ERR_PIPE    2
 
/* always in a pipe[], pipe[0] is for read and
 * pipe[1] is for write */
#define READ_FD  0
#define WRITE_FD 1
 
#define PARENT_READ_FD  ( pipes[PARENT_READ_PIPE][READ_FD]   )
#define PARENT_WRITE_FD ( pipes[PARENT_WRITE_PIPE][WRITE_FD] )
#define PARENT_ERR_FD   ( pipes[PARENT_ERR_PIPE][READ_FD] )
 
#define CHILD_READ_FD   ( pipes[PARENT_WRITE_PIPE][READ_FD]  )
#define CHILD_WRITE_FD  ( pipes[PARENT_READ_PIPE][WRITE_FD]  )
#define CHILD_ERR_FD    ( pipes[PARENT_ERR_PIPE][WRITE_FD]  )

#define FD_ERR -1

/**
 * Spawn a process from a system command, 
 * wait for it to finish and return standard- out and error for logging.
 * This is the "serial" version (read NON-MPI).
 *
 * @param cmd     
 *    (INPUT) The command to run.
 * @param dir     
 *    (INPUT) The directory to run the command in.
 * @param out     
 *    (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     
 *    (OUTPUT) On exit will hold contents of stderr from command.
 * @param pid     
 *    (OUTPUT) On exit will hold the pid of the spawned process.
 *
 * @return  
 *    Return status. Returns 0 if exited correctly.
 **/
int spawn_process_fork
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
   auto argv_vec = make_command(cmd);
   auto argv = &argv_vec[0];

   int status = 0; 
   int pipes[NUM_PIPES][2];
                   
   // pipes for parent to write and read
   int pip0 = pipe(pipes[PARENT_READ_PIPE]);
   int pip1 = pipe(pipes[PARENT_WRITE_PIPE]);
   int pip2 = pipe(pipes[PARENT_ERR_PIPE]);
   
   if( (pid = fork()) < 0 )
   {
      status = -1;
   }
   else if(!pid) 
   {
      // child process
      exit_guard guard;

      // setup pipes
      if(dup2(CHILD_READ_FD, fileno(stdin)) == FD_ERR)
      {
         std::cout << " CHILD_READ_FD DUP ERROR " << std::endl;
         abort();
      }
      if(dup2(CHILD_WRITE_FD, fileno(stdout)) == FD_ERR)
      {
         std::cout << " CHILD_WRITE_FD DUP ERROR " << std::endl;
         abort();
      }
      if(dup2(CHILD_ERR_FD, fileno(stderr)) == FD_ERR)
      {
         std::cout << " CHILD_ERR_FD DUP ERROR " << std::endl;
         abort();
      }

      /* Close fds not required by child. Also, we don't
      want the exec'ed program to know these existed */
      close(CHILD_READ_FD);
      close(CHILD_WRITE_FD);
      close(CHILD_ERR_FD);
      close(PARENT_READ_FD);
      close(PARENT_WRITE_FD);
      close(PARENT_ERR_FD);
      
      // change directory if needed
      if(!dir.empty())
      {
         if(chdir(dir.c_str()) == -1)
         {
            midas::stream::HeaderInserter err_buf(std::cerr.rdbuf(),"*** ",true);
            std::ostream err(&err_buf);
            err << "COULD NOT CHDIR\n"
                << dir 
                << std::flush;
         }
      }

      // make call
      if(execvp(argv[0], argv) == -1)
      {
         // error handling
         midas::stream::HeaderInserter err_buf(std::cerr.rdbuf(),"*** ",true);
         std::ostream err(&err_buf);
         err << "COULD NOT RUN : " << std::strerror(errno) << "\n" << std::endl;
         int i=0;
         while(argv[i] != 0)
         {
            err << argv[i] << " ";
            ++i;
         }
         err << std::flush;
      }
   } 
   else 
   {
      // parent
      /* close fds not required by parent */       
      close(CHILD_READ_FD);
      close(CHILD_WRITE_FD);
      close(CHILD_ERR_FD);
      
      // Write to child’s stdin
      //write(PARENT_WRITE_FD, "2^32\n", 5);
      
      // Read from child’s stdout
      if(read_from_fd(PARENT_READ_FD,out))
         std::cout << " STD OUT ERROR " << std::endl;
      if(read_from_fd(PARENT_ERR_FD,err))
         std::cout << " STD ERR ERROR " << std::endl;
      
      // Wait for child to finish up
      wait_for_child(pid, status, 0);
      
      close(PARENT_READ_FD);
      close(PARENT_WRITE_FD);
      close(PARENT_ERR_FD);
   }

   return status;
}

// undef
#undef NUM_PIPES

#undef PARENT_WRITE_PIPE
#undef PARENT_READ_PIPE
#undef PARENT_ERR_PIPE
 
#undef READ_FD
#undef WRITE_FD
 
#undef PARENT_READ_FD
#undef PARENT_WRITE_FD
#undef PARENT_ERR_FD
 
#undef CHILD_READ_FD
#undef CHILD_WRITE_FD
#undef CHILD_ERR_FD

#undef FD_ERR

/**
 * Create a C string from a C++ std::string.
 * A C-string is NULL terminated.
 *
 * @param str
 *    The C++ string to convert to a C string.
 *
 * @return
 *    Return the C string in a std::unique_ptr (to control lifetime).
 **/
std::unique_ptr<char[]> string_to_char_ptr
   (  const std::string& str
   )
{
   auto size = str.size();

   std::unique_ptr<char[]> ptr{new char[size + 1]};
   std::memcpy(ptr.get(), str.c_str(), size);
   ptr[size] = '\0';

   return ptr;
}

#ifdef VAR_MPI
/**
 * Init MPI_Info struct for call to MPI_Comm_spawn.
 *
 * Sets the 'wdir' key.
 *
 * @param info
 *    The MPI_Info object to initialize.
 * @param dir
 *    The working directory for the spawned process.
 **/
void init_mpi_info
   (  MPI_Info* info
   ,  const std::string& dir
   )
{
   mpi::detail::WRAP_Info_create(info);
   
   if(!dir.empty())
   {
      mpi::detail::WRAP_Info_set   (*info, "wdir", string_to_char_ptr(dir).get());
   }
}

/**
 * Spawn a process from a system command using MPI_Comm_spawn.
 *
 * Spawns an instance of `midascpp_waiter.x`, which will do the 
 * fork() + exec() before calling MPI_Init, and starting MPI 
 * communication with PARENT.
 *
 * @param cmd     
 *    (INPUT) The command to run.
 * @param dir     
 *    (INPUT) The directory to run the command in.
 * @param out     
 *    (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     
 *    (OUTPUT) On exit will hold contents of stderr from command.
 * @param pid     
 *    (OUTPUT) On exit will hold the pid of the spawned process.
 *
 * @return  
 *    Return status. Returns 0 if exited correctly.
 **/
int spawn_process_mpi_comm_spawn
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
   int status = 0;

   // Create argv
   std::string command = "--command=";
   for(int i = 0; i < cmd.size(); ++i)
   {
      command += cmd[i] + ((i < cmd.size() - 1) ? " " : "");
   }
   std::unique_ptr<char*[]> argv{ new char*[2] };
   argv[0] = const_cast<char*>(command.c_str());
   argv[1] = nullptr;

   // Path to midascpp_waiter.x
   auto path_str = input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_PREFIX");
   path_str = path::Join(path::Join(path_str, "libexec"), "midascpp_waiter.x");

   auto path_ptr = string_to_char_ptr(path_str);

   //
   constexpr int  max_procs = 1; /* We only spawn a single process to do the system call */
   MPI_Comm       child_comm;
   int            spawn_error[max_procs];
   MPI_Info       info;
   init_mpi_info(&info, dir);
   
   // Call spawn on the wrapper
   status = mpi::detail::WRAP_Comm_spawn
      (  path_ptr.get()
      ,  argv.get()
      ,  max_procs
      ,  info
      ,  0
      ,  MPI_COMM_SELF
      ,  &child_comm
      ,  spawn_error
      );
   
   // Wait for the process to finish
   if(status == 0)
   {
      mpi::detail::WRAP_Barrier(child_comm);
   }
   
   // Clean-up
   mpi::detail::WRAP_Info_free(&info);
   
   return status;
}
#endif /* VAR_MPI */

/**
 * Connect to a UNIX socket, and return the file descriptor (fd).
 *
 * If socket cannot be created or a connection cannot be made,
 * the function will throw a MIDASERROR.
 *
 * @param filename
 *    The filename of the UNIX socket.
 *
 * @return
 *    Returns the file descriptor (fd) for the created UNIX socket.
 **/
int connect_to_un_socket(const char* filename)
{
   int sockfd;
   sockaddr_un name;

   /* Create the socket. */
   sockfd = socket(AF_LOCAL, SOCK_STREAM, 0);
   if (sockfd < 0)
   {
      MIDASERROR("Cannot create socket.");
   }

   memset(&name, 0, sizeof(name));

   name.sun_family = AF_LOCAL;
   strncpy (name.sun_path, filename, sizeof (name.sun_path));
   name.sun_path[sizeof (name.sun_path) - 1] = '\0';
      
   /* Try to connect */
   int connect_status = -1;
   if((connect_status = connect(sockfd, (sockaddr*) &name, sizeof(name))) != 0)
   {
      MIDASERROR("Connection to AF_UNIX socket failed!");
   }
   
   /* Return connected socket */
   return sockfd;
}

/**
 * Spawn process by use of the fork job server.
 *
 * @param cmd     
 *    (INPUT) The command to run.
 * @param dir     
 *    (INPUT) The directory to run the command in.
 * @param out     
 *    (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     
 *    (OUTPUT) On exit will hold contents of stderr from command.
 *             NOT IMPLEMENTED.
 * @param pid     
 *    (OUTPUT) On exit will hold the pid of the spawned process.
 *             WILL NOT RETURN PID.
 *
 * @return  
 *    Return status. Returns 0 if exited correctly.
 **/
int spawn_process_fork_server
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
   int status  = 0;

   int sockfd = connect_to_un_socket(input::gProgramSettings.GetGeneralSettings().GetUnixSocketPath().c_str());
   
   /* Create request */
   std::string message;
   for(int i = 0; i < cmd.size(); ++i)
   {
      message += cmd[i] + ((i < cmd.size() - 1) ? " " : "");
   }
   message += " ";
   message[message.size() - 1] = '\0';
   if(dir.empty())
   {
      message += os::Getcwd();
   }
   else
   {
      message += dir;
   }
      
   /* Send request */
   int write_size = write(sockfd, message.c_str(), message.size() + 1);
   
   /* Read stdout from external program (forwarded by the fork_server), and add to stringstream 
    * 
    * Buffer size can be increase, which may have an impact on performance
    */
   char buffer[1024];
   int  read_bytes;
   while((read_bytes = read(sockfd, buffer, sizeof(buffer) - 1)))
   {
      buffer[read_bytes] = '\0';
      out << buffer;
   }

   /* Clean-up */
   close(sockfd);

   return status;
}

/**
 * Create a unix socket path from hostname of current machine.
 * 
 * The socket path will be:
 *    
 *    "cwd"/socket."hostname"
 *
 * @return
 *    Returns the unix socket name as a string.
 **/
std::string unix_socket_path_from_hostname()
{
   char buffer[1024];
   
   /* Get current working directory */
   if(!getcwd(buffer, 1024))
   {
      MIDASERROR("Could not get current working directory.");
   }
   strcat(buffer, "/socket.");
   
   /* Base socket name on hostname */
   int len = strlen(buffer);
   if(gethostname(buffer + len, 1024 - len) != 0)
   {
      std::stringstream sstr;
      sstr << "Could not get hostname. Errno = " << strerror(errno);
      MIDASERROR(sstr.str());
   }
 
   return std::string{buffer};
}

/**
 * Dynamic function pointer, which can be set at runtime, 
 * to define the paradigm to use for spawning processes.
 *
 * Can be set to:
 *    spawn_process_fork
 *    spawn_process_fork_server
 *    spawn_process_mpi_comm_spawn
 *
 * These will use different ways of spawning processes for system calls
 * and calls to external programs.
 **/
int (*spawn_process_ptr) (const system_command_t&, const std::string&, std::stringstream&, std::stringstream&, pid_t&) = nullptr;

/**
 * Setup spawn_process_ptr.
 * Should be called before spawn_process wrapper is called.
 **/
void setup_spawn_process()
{
#ifdef VAR_MPI
   // Always call serial version, as MPI version is buggy.
   // CAVEAT: Serial version does not work with MPI and infiniband, as it calls fork().
   //         Fix is currently to run on ethernet network instead, when using MPI, as this should work OK!
   switch(input::gProgramSettings.GetGeneralSettings().GetSpawnParadigm())
   {
      case input::SpawnParadigm::FORK_EXEC:
         spawn_process_ptr = &spawn_process_fork;
         break;
      case input::SpawnParadigm::FORK_SERVER:
         spawn_process_ptr = &spawn_process_fork_server;
         if(input::gProgramSettings.GetGeneralSettings().GetUnixSocketPath().empty())
         {
            input::gProgramSettings.GetGeneralSettings().SetUnixSocketPath(unix_socket_path_from_hostname());
         }
         break;
      case input::SpawnParadigm::MPI_COMM_SPAWN:
         /* Actually there are some issues with mpi_comm_spawn paradigm,
          * as this will not pipe output back to MidasCpp.
          * So for now it is probably best to just deactivate it.
          */
         MIDASERROR("'--spawn-paradigm=mpi_comm_spawn' not supported.");
         spawn_process_ptr = &spawn_process_mpi_comm_spawn;
         break;
      case input::SpawnParadigm::ERROR:
         MIDASERROR("Unknown spawn paradigm.");
   }
#else
   // If not MPI we just always use spawn_process_fork
   spawn_process_ptr = &spawn_process_fork;
#endif /* VAR_MPI */
}

/**
 * Wrapper to spawn a process from a system command.
 * Will call spawn_process_ptr, which is set to one of 
 * spawn_process_<implementation> functions.
 *
 * This function cannot be called before setup_spawn_process has been called
 * (this is done in midascpp.cc as part of program setup).
 *
 * @param cmd    
 *    (INPUT) The command to run.
 * @param dir     
 *    (INPUT) The directory to run the command in.
 * @param out     
 *    (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     
 *    (OUTPUT) On exit will hold contents of stderr from command.
 * @param pid     
 *    (OUTPUT) On exit will hold the pid of the spawned process.
 *
 * @return  
 *    Return status. Returns 0 if exited correctly.
 **/
int spawn_process
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
#ifdef DEBUG
   // If DEBUG build we check that the pointer is actually set.
   assert(spawn_process_ptr);
#endif
   return spawn_process_ptr(cmd, dir, out, err, pid);
}
