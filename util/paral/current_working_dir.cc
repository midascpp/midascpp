#include "current_working_dir.h"

#include <iostream>
#include <stdio.h>  /* defines FILENAME_MAX */

#ifdef WINDOWS /* define getcwd_impl depending on OS */
    #include <direct.h>
    #define getcwd_impl _getcwd
#else
    #include <unistd.h>
    #define getcwd_impl getcwd
#endif

/**
 * getcwd_wrap - wraps low-level OS specific getcwd function
 **/
int getcwd_wrap(char* dir, size_t size)
{
   if (!getcwd_impl(dir, size))
   {
      return errno;
   }

   dir[size-1] = '\0';
   
   return 0;
}

/**
 * current_working_dir - returns current working directory
 **/
std::string current_working_dir()
{
   //std::string dir;
   char current_path[FILENAME_MAX];
   int status = getcwd_wrap(current_path,sizeof(current_path));
   if(status)
   {
      std::cerr << " COULD NOT GET CURRENT WORKING DIR " << std::endl;
   }
   return std::string(current_path);
}
