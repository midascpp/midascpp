#include "threaded_system_call.h"

#include <mutex>
#include <sstream>
#include <chrono>
#include <ctime>
#include <cstring>
#include <thread>

#include "spawn_process.h"
#include "current_working_dir.h"
#include "time.h"
#include "../stream/HeaderInserter.h"

/**
 * Return current time and date as string
 *
 * @return Current time and date.
 **/
std::string date_now
   (
   )
{
   //auto time_now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
   auto time_now = midas::paral::make_utc_tm(std::chrono::system_clock::now());
   char buffer[1024];
   std::strftime(buffer, 1024, "%D  %T", &time_now);
   buffer[strcspn(buffer, "\n")] = '\0'; // remove newline
   return std::string(buffer);
}

/**
 * Write log file (uses a mutex lock to avoid data race)
 *
 * @param cmd    system command that was run
 * @param dir    the directory the command was run in
 * @param out    output to std out
 * @param err    output to std err
 * @param date   the time and date of the call
 * @param pid    the pid of the child process
 * @param status the return status of the process
 * @param os     log output stream
 **/
void threaded_output_to_file
   (  const system_command_t& cmd
   ,  const std::string dir
   ,  const std::stringstream& out
   ,  const std::stringstream& err
   ,  const std::string& date
   ,  pid_t pid
   ,  int status
   ,  mutex_ostream& os
   )
{
   // lock file mutex
   std::lock_guard<std::mutex> guard(os.second);
   
   std::string pid_str = std::to_string(pid) + " ~ ";
   os.first << " ---~~~+++ ASYNC SYSTEM CALL +++~~~--- ---~~~+++ ASYNC SYSTEM CALL +++~~~--- \n";
   // output thread id
   os.first << "[" << pid_str << "THREAD ID] " << std::this_thread::get_id() << "\n" << std::flush;
   // output date
   os.first << "[" << pid_str << "DATE     ] " << date << "\n" << std::flush;
   // output dir
   os.first << "[" << pid_str << "DIRECTORY] " << dir << "\n" << std::flush;
   
   // output command
   os.first << "[" << pid_str << "COMMAND  ] ";
   for(size_t i=0; i<cmd.size(); ++i)
      os.first << cmd[i] << " ";
   os.first << "\n";
   
   // output std out
   midas::stream::HeaderInserter os_out_buf(os.first.rdbuf(),"["+pid_str+"STDOUT   ] ",true);
   std::ostream os_out(&os_out_buf);
   os_out << out.str() << "\n" << std::flush;
   
   // output std err
   midas::stream::HeaderInserter os_err_buf(os.first.rdbuf(),"["+pid_str+"STDERR   ] ",true);
   std::ostream os_err(&os_err_buf);
   os_err << err.str() << "\n" << std::flush;
   
   // output status
   os.first << "[" << pid_str << "STATUS   ] " << WEXITSTATUS(status) << "\n" 
            << " ---~~~+++ ----------------- +++~~~--- ---~~~+++ ----------------- +++~~~--- \n\n" << std::flush;
}

/**
 * Make a threaded system call
 * 
 * @param cmd System command to be run
 * @param os  Log output stream
 **/
threaded_system_return threaded_system_call
   (  const system_command_t& cmd
   ,  mutex_ostream& os
   )
{
   threaded_system_return ret;
   std::stringstream std_out; // catches std out
   std::stringstream std_err; // catches std err
   pid_t pid;

   // get time and date for call
   auto date = date_now();

   // spawn process and wait for it to finish
   std::chrono::time_point<std::chrono::system_clock> start, end;
   start = std::chrono::system_clock::now();
   
   ret.status = spawn_process(cmd, std::string(""), std_out, std_err, pid);
   
   end = std::chrono::system_clock::now();

   ret.duration = end-start;
   // output to log file
   threaded_output_to_file(cmd, current_working_dir(), std_out, std_err, date, pid, ret.status, os);
   
   return ret;
}

/**
 * Make a threaded system in dir
 * 
 * @param cmd System command to be run
 * @param dir Directory to run command in
 * @param os  Log output stream
 **/
threaded_system_return threaded_system_call_dir
   (  const system_command_t& cmd
   ,  std::string dir
   ,  mutex_ostream& os
   )
{
   threaded_system_return ret;
   std::stringstream std_out; // catches std out
   std::stringstream std_err; // catches std err
   pid_t pid;

   // get time and date for call
   auto date = date_now();

   // spawn process and wait for it to finish
   std::chrono::time_point<std::chrono::system_clock> start, end;
   start = std::chrono::system_clock::now();
   
   ret.status = spawn_process(cmd, dir, std_out, std_err, pid);
   
   ret.duration = end - start;

   // output to log file
   threaded_output_to_file(cmd, dir, std_out, std_err, date, pid, ret.status, os);
   
   return ret;
}
