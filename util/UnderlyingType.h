/**
 *******************************************************************************
 * 
 * @file    UnderlyingType.h
 * @date    06-10-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Conversion of scoped enum to underlying (integral) type.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef UNDERLYINGTYPE_H_INCLUDED
#define UNDERLYINGTYPE_H_INCLUDED

#include <type_traits>

namespace midas
{
   namespace util
   {

      /*********************************************************************//**
       * Converts a (scoped) enumerator variable to its underlying (integral)
       * type.
       * Shamelessly copied from _Effective Modern C++, Scott Meyers (2014)_,
       * Item 10: Prefer scoped `enum`s to unscoped `enum`s.
       *
       * Example usage: Say you have a tuple whose fields you want to access in
       * a more abstract manner than `std::get<int>`. Define an `enum`
       * (convertible to integer) to keep track of what the fields correspond
       * to. An unscoped `enum` is implicitly convertible but has other
       * drawbacks (see _Meyers_ above), so use scoped `enum` and the function
       * below.
       *
       * ~~~.cpp
       * using UserInfo = std::tuple<  std::string    // name
       *                            ,  std::string    // email
       *                            ,  std::size_t    // reputation
       *                            >;
       * UserInfo uInfo("Mads", "user@example.com", 42); // tuple object
       *
       * // Getting email, regular usage:
       * auto email = std::get<1>(uInfo);
       *
       * // Or introduce scoped enum (marked by 'class') in correspondence with
       * // tuple fields.
       * enum class UInfoFields {uiName, uiEmail, uiReputation};
       * 
       * // auto name = std::get<UInfoFields::uiName>(uInfo);
       * // ^Doesn't compile, scoped enum variable _not_ implicitly convertible
       * // to underlying integral type.
       *
       * using midas::util::ToUType;
       * auto name = std::get<ToUType(UInfoFields::uiName)>(uInfo); // Works!
       * ~~~
       *
       * To conclude:
       *    -  Using scoped `enum` (instead of unscoped) is slightly more
       *       verbose but much safer.
       *    -  Using enums for accessing tuple fields is more readable and more
       *       flexible in case the order of objects in the tuple needs changing.
       *
       * @param[in] aEnumerator
       *    (Scoped) enumerator variable.
       * @return
       *    Corresponding integer (of enumerator's underlying type).
       ************************************************************************/
      template<typename E>
      constexpr auto ToUType(E aEnumerator) noexcept
      {
         return static_cast<std::underlying_type_t<E>>(aEnumerator);
      }

   } /* namespace util */
} /* namespace midas */

#endif/*UNDERLYINGTYPE_H_INCLUDED*/
