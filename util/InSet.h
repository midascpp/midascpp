/**
************************************************************************
* 
* @file                InSet.h
*
* Created:             14-01-2014
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Class for set of Integers
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INSET_H
#define INSET_H

#include <iostream>
#include<set>
#include<vector>
#include<string>

#include"inc_gen/TypeDefs.h"

using std::vector;
using std::set;
using std::string;

class InSet
{
   private:
      set<In> mMemberNrs;     ///< Contains the mode numbers.
   public:
      InSet() {} ///< 
      InSet(string aString);

      const set<In>& GetmMemberNrs() const  {return mMemberNrs;};
      bool InContained(const In& arIn) const;   ///< check whether mode is contained
      In Size() {return mMemberNrs.size();}
      bool IsContained(const In& arIn) {return mMemberNrs.find(arIn) != mMemberNrs.end();}
             ///<checks wether arIn is contained
      void Insert(const InSet& arInSet) {set<In> new_members=arInSet.GetmMemberNrs();
             mMemberNrs.insert(new_members.begin(),new_members.end());} ///< inserts other range
      set<In>::iterator Begin() {return mMemberNrs.begin();}
      set<In>::iterator End() {return mMemberNrs.end();}
      In Highest();
      vector<In> Vec() const; ///< returns all members as a vector
};

#endif
