#ifndef DEFAULTPARAMETERPROXY
#define DEFAULTPARAMETERPROXY

template<typename T>
class DefaultParameterProxy
{
   private:
      T mProxy;

   public:
      template<typename... U>
      DefaultParameterProxy(U&&... u): mProxy(std::forward<U>(u)...) { };
      
      T& GetRef() { return mProxy; }
};

#endif /* DEFAULTPARAMETERPROXY */
