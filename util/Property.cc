/**
************************************************************************
* 
* @file                Property.cc
*
* Created:             21-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement Property class 
* 
* Last modified: Sun Feb 12, 2006  11:02PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <string> 
using std::string;
#include <vector> 
using std::vector;



// My headers:
#include "inc_gen/TypeDefs.h" 
#include "inc_gen/Const.h" 
#include "inc_gen/Warnings.h" 
#include "util/Property.h" 
#include "util/Io.h" 

/**
* Initialize the Proper class - bare version
* */
Property::Property()
{
   mInitialized = false;
}
/**
* Initialize the Property class 
* */
Property::Property(In rPropSym,In rNord,string rLabMod,Nb rProp,
      string rLabX,string rLabY,string rLabZ,string rLabU,
      Nb rFrqY,Nb rFrqZ,Nb rFrqU,In rExSym, In rExSp, In rExNr)
{
   mInitialized = true; 
   mNord        = rNord;
   mPropSym     = rPropSym;
   mProp        = rProp;
   mFrqY        = rFrqY;
   mFrqZ        = rFrqZ;
   mFrqU        = rFrqU;
   mLabMod      = rLabMod;
   mLabX        = rLabX;
   mLabY        = rLabY;
   mLabZ        = rLabZ;
   mLabU        = rLabU;
   mExSym       = rExSym;
   mExSp        = rExSp; 
   mExNr        = rExNr;
}
/**
* Return the model label 
* */
string Property::PropertyModel()
{
   return mLabMod;
}
/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const Property& arProp)
{
   arProp.OutPut(arOut,"true");
   return arOut;
}
/**
* output general 
* */
void Property::OutPut(string& arSout, bool aWithValue) const
{
//   arOut << *this << endl; 
//}
//{ 
   const Property& arProp = *this;
   //List of standard properties 
   //In case of additions for excited states, whatch our for the IsThis... functions
   In energy  = 0;
   In fop     = 1;
   In lrf     = 2;
   In qrf     = 3;
   In crf     = 4;
   In tmgx    =-1; 
   In tmgxy   =-20;
   In tmgxx   =-21;
   In excfop  =-11;
   //In exclr  =-12;
   In tmxx    = -2;
   In oscxx   = -22;
   In tp_xyzu = -220;
   // In wtp_iijj = -221;  // Seidler warning: This seems not to be used.
   // In wtp_ijij = -222;  // Seidler warning: This seems not to be used.
   In wtp_ijji = -223;
   In tp_1 = -30;
   // In tp_2 = -32;  // Seidler warning: This seems not to be used.
   In tp_3 = -33;
   In tp_4 = -35;
   In shieldiso = 401;
   In shieldtens = 402;

   string labx = arProp.mLabX;
   string laby = arProp.mLabY;
   string labz = arProp.mLabZ;
   string labu = arProp.mLabU;
   while(labx.find("DIPLEN")!= labx.npos) labx.erase(labx.find("DIPLEN"),I_6); 
   while(laby.find("DIPLEN")!= laby.npos) laby.erase(laby.find("DIPLEN"),I_6); 
   while(labz.find("DIPLEN")!= labz.npos) labz.erase(labz.find("DIPLEN"),I_6); 
   while(labu.find("DIPLEN")!= labu.npos) labu.erase(labu.find("DIPLEN"),I_6); 

   ostringstream oss;
   oss.precision(14);
   oss.unsetf(ios::scientific);
   oss.setf(ios::showpoint);
   if (arProp.mNord == energy)
   {
      if (arProp.mLabX.find("Excita") != arProp.mLabX.npos)
      {
         oss << " Exc."; 
         oss << " Sy: " << arProp.mExSym; 
         oss << " Sp: "<< arProp.mExSp;  
         oss << " Nr: " << arProp.mExNr;  
         //oss << " E_grs: " << arProp.mFrqY; 
         //oss << " E_tot: " << arProp.mFrqU;
         if (aWithValue) oss << " w ";//  << arProp.mFrqZ; 
      } 
      else if (arProp.mLabX.find("ExcTot") != arProp.mLabX.npos)
      {
         oss << " Exc."; 
         oss << " Sy: " << arProp.mExSym; 
         oss << " Sp: "<< arProp.mExSp;  
         oss << " Nr: " << arProp.mExNr;  
         if (aWithValue) oss << " E_tot ";
      } else
      {
         oss << " " << arProp.mLabX << " ground state energy ";
      }
   }
   else if (arProp.mNord == tmgx) 
   {
      //oss << " |<O|" << arProp.mLabX << "|i(" <<arProp.mFrqY << ")>| ";
      oss << " <O|" << labx << "|i(";
      oss << "Sy:" << arProp.mExSym; 
      oss << " Sp:" << arProp.mExSp;  
      oss << " Nr:" << arProp.mExNr;  
      oss << ")> ";
   }
   else if ((arProp.mNord == tmgxy) || (arProp.mNord == tmgxx))
   {
      if (arProp.mNord==tmgxx) oss << " w*<O|" << labx << "|i(";
      else oss << " <O|" << labx << "|i(";
      oss << "Sy:" << arProp.mExSym; 
      oss << " Sp:" << arProp.mExSp;  
      oss << " Nr:" << arProp.mExNr;  
      oss << ")><i(";
      oss << "Sy:" << arProp.mExSym; 
      oss << " Sp:" << arProp.mExSp;  
      oss << " Nr:" << arProp.mExNr;  
      oss << ")| ";
      oss << laby << "|O> ";
   }
   else if ((arProp.mNord <= tp_xyzu)&& (arProp.mNord >= wtp_ijji))
   {
      if (arProp.mNord == tp_xyzu) oss << " S_";
      else oss << " w_1*w_2*S_";
      oss <<labx <<","<<laby 
         <<","<< labz <<","<< labu 
         << "^(O,";
      oss << "Sy:" << arProp.mExSym; 
      oss << " Sp:" << arProp.mExSp;  
      oss << " Nr:" << arProp.mExNr;  
      oss << ") ";
      oss << "("<< arProp.mFrqY <<")";
   }
   else if ((arProp.mNord <= tp_1)&& (arProp.mNord >= tp_4))
   {
      oss <<" ";
      if ((arProp.mNord <= tp_3)&& (arProp.mNord >= tp_4))  oss << "w_1*w_2*";
      if (arProp.mNord == -30 || arProp.mNord == -33 ) oss << "Delta_||";
      if (arProp.mNord == -31 || arProp.mNord == -34 ) oss << "Delta_pe";
      if (arProp.mNord == -32 || arProp.mNord == -35 ) oss << "Delta_c ";
      oss << "(O,";
      oss << "Sy:" << arProp.mExSym; 
      oss << " Sp:" << arProp.mExSp;  
      oss << " Nr:" << arProp.mExNr;  
      oss << ") ";
      oss << "(w_1="<< arProp.mFrqY;
      oss << ",w_2="<< arProp.mFrqZ <<")";
   }
   else if (arProp.mNord == tmxx) 
   {
      oss << " |<i(" << arProp.mFrqY <<")|" 
          << labx << "|f(" <<arProp.mFrqZ << ")>| ";
   }
   else if (arProp.mNord == oscxx) 
   {
      oss << " (w_f-w_i)*|<i(" << arProp.mFrqY <<")|" 
          << labx << "|f(" <<arProp.mFrqZ << ")>|^2 ";
   }
   else if (arProp.mNord == fop) 
   {
      oss << " <" << labx << "> ";
   }
   else if (arProp.mNord == excfop) 
   {
      oss << " <" << labx << ">("
         << " " << setw(1) << arProp.mExSym
         << " " << setw(1) << arProp.mExSp  
         << " " << setw(4) << arProp.mExNr 
         << " " << setw(8) << arProp.mFrqY << ") ";
   }
   else if (arProp.mNord == shieldiso)
   {
      oss << "  " << " Shielding:  " << labx << "  " << laby << "  ";
   }
   else if (arProp.mNord == shieldtens)
   {
      oss << "  " << " Shielding tensor:  " << labx << "  " << laby << "  ";
   }
   else if (arProp.mNord == lrf) 
   {
      oss << " <<"<< labx <<","
          << laby << ">>_("<<setw(18) << arProp.mFrqY << ") ";
   }
   else if (arProp.mNord == qrf) 
   {
      oss << " <<" << labx <<","
          << laby <<"," << labz 
          << ">>_(" << setw(18) << arProp.mFrqY<<"," 
          << setw(18) << arProp.mFrqZ << ") ";
   }
   else if (arProp.mNord == crf) 
   {
      oss << " <<" << labx <<"," << laby 
         <<"," << labz <<","<< labu 
         << ">>_(" << setw(18) << arProp.mFrqY <<"," 
         << setw(18) << arProp.mFrqZ
         <<"," << setw(18) << arProp.mFrqU << ") ";
   }
   else 
   {
      oss << setw(3) << arProp.mNord;
      oss << setw(2) << arProp.mPropSym << " ";
      oss << " "  << labx;
      oss << " "  << laby;
      oss << " "  << labz;
      oss << " "  << labu;
      oss << " " << setw(24) << arProp.mFrqY; 
      oss << " " << setw(24) << arProp.mFrqZ;
      oss << " " << setw(24) << arProp.mFrqU;
      oss << " " << setw(4) << arProp.mExSym;
      oss << " " << setw(4) << arProp.mExSp;  
      oss << " " << setw(4) << arProp.mExNr; 
   }
   oss.setf(ios::scientific);
   oss.setf(ios::uppercase);
   if (aWithValue) oss << "= ";
   if (aWithValue) oss << setw(22) << arProp.mProp;
   arSout = oss.str();
}
/**
* output general 
* */
void Property::OutPut(ostream& arOut, bool aWithValue) const
{
   string s;
   OutPut(s,aWithValue);
   arOut << s; 
}

/**
* Is this an excitation energy
* */
bool Property::IsThisExcita() const
{
   In energy = I_0;
   if ((mNord == energy)&& (mLabX.find("Excita") != mLabX.npos)) return true; 
   else return false;
}
/**
* Is this a one-photon property
* */
bool Property::IsThisOnePhotAbsStr() const
{
   //In tmgx    =-1; 
   //In tmgxy   =-20;
   In tmgxx   =-21;

   //if ((mNord == tmgxy) || (mNord == tmgxx)) return true; 
   //|| (mNord == tmgx)) return true;
   //if (mNord == tmgxx) return true; 
   if (mNord == tmgxx) return true; 
   else return false;
}
/**
* Is this a two-photon property
* */
bool Property::IsThisTwoPhotAbsStr() const
{
   //In tp_xyzu = -220;
   //In wtp_iijj = -221;
   //In wtp_ijij = -222;
   //In wtp_ijji = -223;
   In tp       = -33;   // TEMPORARY VERSION 
   //In wtp_iijj = -221;
   //In wtp_ijij = -222;
   //In wtp_ijji = -223;

   //if ((mNord <= tp_xyzu)&& (mNord >= wtp_ijji)) return true;
   //if ((mNord <= wtp_iijj)&& (mNord >= wtp_ijji)) return true;
   if (mNord == tp) return true;
   else return false;
}
/**
* Get the labels as integers, x-> 0, etc.
* */
void Property::GetIntForLabs(In& aIx,In& aIy,In& aIz, In& aIu) const
{
   aIx = -I_1;
   aIy = -I_1;
   aIz = -I_1;
   aIu = -I_1;
   string labx = mLabX;
   string laby = mLabY;
   string labz = mLabZ;
   string labu = mLabU;
   if (labx.find("XDIPLEN")!= labx.npos) aIx = I_0;
   if (labx.find("YDIPLEN")!= labx.npos) aIx = I_1;
   if (labx.find("ZDIPLEN")!= labx.npos) aIx = I_2;
   if (laby.find("XDIPLEN")!= laby.npos) aIy = I_0;
   if (laby.find("YDIPLEN")!= laby.npos) aIy = I_1;
   if (laby.find("ZDIPLEN")!= laby.npos) aIy = I_2;
   if (labz.find("XDIPLEN")!= labz.npos) aIz = I_0;
   if (labz.find("YDIPLEN")!= labz.npos) aIz = I_1;
   if (labz.find("ZDIPLEN")!= labz.npos) aIz = I_2;
   if (labu.find("XDIPLEN")!= labu.npos) aIu = I_0;
   if (labu.find("YDIPLEN")!= labu.npos) aIu = I_1;
   if (labu.find("ZDIPLEN")!= labu.npos) aIu = I_2;
}
/**
* Return the value
* */
bool Property::LogicallyTheSameAs(Property& arProp) const
{
   bool test = true;
   return test;
}
/**
* Return the value
* */
bool OutPutAndCheckPropertyVector(vector<Property>& arPropVec,In aIoLevel, 
                                  string& aSlabel)
{
   string st_help;
   bool the_end = false;
   In nprpc = arPropVec.size();
   for (In i_prpc=0;i_prpc < nprpc;i_prpc++) 
   {
      if (st_help != arPropVec[i_prpc].PropertyModel())
      {
         st_help = arPropVec[i_prpc].PropertyModel();
         if (st_help == "THE_END") the_end = true;
         if(!the_end && aIoLevel > I_0) Mout << " "
              << st_help << " properties: " << endl;   
      }
      if (!the_end) 
      {
         if (aIoLevel > I_0) Mout << " #" << setw(4) << i_prpc+1;
         if (aIoLevel > I_0) Mout << arPropVec[i_prpc]<<endl;
      }
      else
      {
         if (aIoLevel > I_0) Mout 
              << " Normal termination of electronic structure "
              << " calculation " << endl;
         return true;
      }
   }
   if (!the_end) 
   { 
      string s1 = " Electronic structure calculation with abnormal ";
      s1 = s1 + "termination status: " + aSlabel;
      Mout << s1 << endl;
      MidasWarning(s1);
      return false;
   }
   return false;
}
