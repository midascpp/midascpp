#ifndef SCOPED_MANIPULATORS_H_INCLUDED
#define SCOPED_MANIPULATORS_H_INCLUDED

#include <iostream>

namespace midas
{
namespace stream
{

// define some MACROs used later
#define CONCAT_IMPL( x, y ) x##y
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )

namespace detail
{
   /**
    * @brief Class that handles setting precision of output stream in a scope-bound way.
    **/
   class ScopedPrecision_
   {
      std::ios_base& mOs; // std::ios_base is base class for all std stream classes
      const int mPrec = 0;

      public: ScopedPrecision_( int i
                              , std::ios_base& aOs
                              )
         : mOs(aOs)
         , mPrec(aOs.precision(i))
      {
      }

      public: ~ScopedPrecision_()
      {
         mOs.precision(mPrec);
      }
   };

   /**
    * @brief Class that handles setting ios flags of output stream in a scope-bound way.
    *
    * All ios flags needed must be set when called, as the setting overwrites the all ios flags.
    **/
   struct ScopedIosFlags_
   {
      std::ios_base& mOs; // std::ios_base is base class for all std stream classes
      const std::ios_base::fmtflags mFlags;

      ScopedIosFlags_( const std::ios_base::fmtflags& flags
                     , std::ios_base& aOs
                     )
         : mOs(aOs)
         , mFlags(aOs.flags(flags))
      {
      }

      ~ScopedIosFlags_()
      {
         mOs.flags(mFlags);
      }
   };


} /* namespace detail */

// define macro for 'easy' usage (__COUNTER__ increases every use)
#define ScopedPrecision(a, b) detail::ScopedPrecision_ MACRO_CONCAT(MIDAS_SCOPED_STREAM_MANIPULATOR_, __COUNTER__)(a,b)
#define ScopedIosFlags(a, b) detail::ScopedIosFlags_ MACRO_CONCAT(MIDAS_SCOPED_STREAM_MANIPULATOR_, __COUNTER__)(a,b)

} /* namespace stream */
} /* namespace midas */

#endif /* SCOPED_MANIPULATORS_H_INCLUDED */
