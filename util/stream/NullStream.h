#ifndef NULLSTREAM_H_INCLUDED
#define NULLSTREAM_H_INCLUDED

#include <iostream>

template<class Ch, class Traits = std::char_traits<Ch> >
class BasicNullBuf: public std::basic_streambuf<Ch, Traits>
{
   public:
      using base_type = std::basic_streambuf<Ch, Traits>;
      using int_type = typename base_type::int_type;
      using traits_type = typename base_type::traits_type;

      int_type overflow(int_type c) override
      {
         return traits_type::not_eof(c);
      }
};

//convenient typedefs
using NullBuf = BasicNullBuf<char>;
using WNullBuf = BasicNullBuf<wchar_t>;

// buffers and streams
// in some .h
extern std::ostream Mnull;
extern std::wostream wMnull;

#endif /* NULLSTREAM_H_INCLUDED */
