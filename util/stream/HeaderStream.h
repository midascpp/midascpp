#ifndef HEADERSTREAM_H_INCLUDED
#define HEADERSTREAM_H_INCLUDED

#include <iostream>

#include "util/stream/HeaderInserter.h"

namespace midas
{
namespace stream
{

/**
 * HeaderStream
 **/
class HeaderStream
   : public std::ostream
{
   private:
      HeaderInserter mFileBuf;

   public:
      HeaderStream(std::ostream& os, const std::string& str, bool start = false)
         : std::ostream(&mFileBuf)
         , mFileBuf(os.rdbuf(),str,start)
      {
      }
      
      HeaderStream(std::ostream& os, size_t blanks, bool start = false)
         : std::ostream(&mFileBuf)
         , mFileBuf(os.rdbuf(),blanks,start)
      {
      }

      void SkipHeader()
      {
         mFileBuf.SkipHeader();
      }
};

inline HeaderStream& operator<<(HeaderStream& hos, skipheader sh)
{
   sh(hos);
   return hos;
}

} /* namespace stream */
} /* namespace midas */

#endif /* HEADERSTREAM_H_INCLUDED */
