#ifndef HEADER_INSERTER_H_INCLUDED
#define HEADER_INSERTER_H_INCLUDED

#include<streambuf>
#include<string>

namespace midas
{
namespace stream
{

/**
 * Stream class for automatically inserting a given header at the start of each line.
 * This could e.g. be 3 spaces, for writing someting indented.
 * It could also the something more elaborate.
 **/
class HeaderInserter 
   : public std::streambuf
{
   private:
      //! The underlying streambuf we are writing output to.
      std::streambuf* myDest;   
      //! The stream header. This will be printed before each outputtet line.
      std::string myHeader;
      //!
      std::string myIndention = {"   "};
      //! Are we at the start of a line, i.e. should we print header.
      bool myIsAtStartOfLine;   
   
   protected:
      //! Overload overflow()
      int overflow( int ch ) override;
   
   public:
      //! Constructor for header of n-spaces
      HeaderInserter( std::streambuf* dest, size_t blank, bool start=false);
      
      //! Constructor for generic header.
      HeaderInserter( std::streambuf* dest, const std::string& header, bool start=false);

      //! Push
      void Push()
      {
         myHeader += myIndention;
      }

      //! Pop
      void Pop()
      {
         std::size_t found = myHeader.rfind(myIndention);
         if (found != std::string::npos)
         {
            myHeader.replace(found, myIndention.length(), "");
         }
      }

      //! Skip next header.
      void SkipHeader();
};

struct skipheader
{
   template<class S>
   void operator()(S& s)
   {
      s.SkipHeader();
   }
};

} /* namespace stream */
} /* namespace midas */

#endif /* HEADER_INSERTER_H_INCLUDED */
