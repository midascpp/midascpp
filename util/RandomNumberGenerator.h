#ifndef MIDAS_UTIL_RANDOMNUMBERGENERATOR_H_INCLUDED
#define MIDAS_UTIL_RANDOMNUMBERGENERATOR_H_INCLUDED

#include <type_traits>
#include <random>

namespace midas
{
namespace util
{
namespace detail
{

/******************************************************************************
 *
 * Implementation details
 *
 *****************************************************************************/

/*! 
 * Get mersenne twister
 */
std::mt19937& get_mersenne();

/*! Get pseudo-random integer value
 *
 */
template
   <  class T
   ,  std::enable_if_t<std::is_integral<T>::value, void*> = nullptr
   >
T rand_int_mersenne()
{
   auto& mersenne = detail::get_mersenne();
   return static_cast<T>(mersenne());
}

/*! Get pseudo-random floating point value between 0.0 and 1.0 
 *
 */
template
   <  class T
   ,  std::enable_if_t<std::is_floating_point<T>::value, void*> = nullptr
   >
T rand_float_mersenne()
{
   auto& mersenne = detail::get_mersenne();
   return static_cast<T>(mersenne())/static_cast<T>(mersenne.max());
}

/*! Get pseudo-random floating point value between -1.0 and 1.0 
 *
 */
template
   <  class T
   ,  std::enable_if_t<std::is_floating_point<T>::value, void*> = nullptr
   >
T rand_signed_float_mersenne()
{
   auto& mersenne = detail::get_mersenne();
   return static_cast<T>(2)*static_cast<T>(mersenne())/static_cast<T>(mersenne.max()) - 1.0;
}

} /* namespace detail */

/******************************************************************************
 *
 * Public interface
 *
 *****************************************************************************/

/**
 * Seed random number generator.
 **/
void SeedRng(std::vector<unsigned int> seed_vec);

/**
 * Re-seed random number generator.
 **/
void ReSeedRng(std::vector<unsigned int> seed_vec);

/**
 * Returns random integer value.
 **/
template<class T>
T RandomInt()
{
   static_assert(std::is_integral<T>::value, "Only works for integral types.");
   return detail::rand_int_mersenne<T>();
}

/**
 * Returns random floating point number between -1.0 and 1.0 of type T.
 **/
template<class T>
T RandomSignedFloat()
{
   static_assert(std::is_floating_point<T>::value,"Only works for floating point.");
   return detail::rand_signed_float_mersenne<T>();
}

/**
 * Returns random floating point number between 0.0 and 1.0 of type T
 **/
template<class T>
T RandomFloat()
{
   static_assert(std::is_floating_point<T>::value,"Only works for floating point.");
   return detail::rand_float_mersenne<T>();
}

/** 
 * Return randomly true/false.
 **/
inline bool RandomBool()
{
   return detail::rand_int_mersenne<int>() % 2 == 1;
}

} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_RANDOMNUMBERGENERATOR_H_INCLUDED */
